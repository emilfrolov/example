define ['../packages/jquery/dist/jquery.min.js'], ($)->
	require('./styles')

	$(document).ready ->
		$(document).on 'click', '.open-auth', ->
			require.ensure [], (require)->
				auth = require('./auth.coffee')
				_gaq.push(['_trackEvent', 'Открытие окна входа', 'Окно номер '+$(@).attr('data-id')])
				do auth.showAuthWin

		$(document).on 'click', '.auth-window', (e)->
			do e.stopPropagation

		$(document).on 'click', '.auth-overlay', ->
			require.ensure [], (require)->
				auth = require('./auth.coffee')
				do auth.closeAuth

		$(document).on 'click', '.close-auth', ->
			require.ensure [], (require)->
				auth = require('./auth.coffee')
				do auth.closeAuth

		$(document).on 'click', '.by-social', ->
			require.ensure [], (require)->
				auth = require('./auth.coffee')
				do auth.showAuthWin

		$(document).on 'click', '.by-num', ->
			require.ensure [], (require)->
				auth = require('./auth.coffee')
				do auth.showAuthPhoneWin

		$(document).on 'click', '.social-list > div', ->
			type = $(@).attr 'data-type'
			require.ensure [], (require)->
				width = 800
				height = 600
				auth = require('./auth.coffee')
				window.completeSocialRegister = auth.completeSocialRegister 

				window.open("#{location.origin}/social/auth/#{type}?redirectUrl=#{location.origin}/social/auth/check", 'socialWin', "top=#{(window.innerHeight-height)/2},left=#{(window.innerWidth-width)/2},width=#{width},height=#{height}")


		appsToTop = $('.apps-block').offset().top - $('.header').height() - 44
		promoToTop = $('.promo-phones').offset().top
		phoneToTop = $('.phones').offset().top
		resizeTimeout = null
		video = $('.landing-video')
		video.on 'loadeddata', ->
			do resizeHeader
		video[0].src = 'http://music.beeline.ru/public/video/landing_video.mp4'

		resizeHeader = ->
			if resizeTimeout
				clearTimeout resizeTimeout

			resizeTimeout = setTimeout ->
				wHeight = $(window).height()
				wWidth = $(window).width()

				if wHeight <= 630
					wHeight = 630

				$('.promo-win').height wHeight

				videoWidth = $('.landing-video').width()
				videoHeight = $('.landing-video').height()

				if (wHeight-videoHeight) > 0
					$('.landing-video').attr 'style', ''
						.css 
							'height': '100%'
							'width': 'auto'
				else if (wWidth-videoWidth) > 0
					$('.landing-video').attr 'style', ''
						.css 
							'width': '100%'
							'height': 'auto'

				startPadding = (wHeight - 533)/2
				# startPadding = (wHeight - $('.promo-start-block').height())/2

				$('.first-quote').css 'padding', startPadding+'px 0px'
			,100

		do resizeHeader

		$(window).on 'resize', ->
			do resizeHeader


		$(window).on 'scroll.promo', ->
			if ($(window).scrollTop() + phoneToTop - 2100) >= promoToTop
				$('.phones').addClass 'active'
				$(window).off '.promo'

		$(window).on 'scroll.shadow', ->
			if $(window).scrollTop() > 0
				if !$('.header').hasClass 'shadow'
					$('.header').addClass 'shadow'
			else
				$('.header').removeClass 'shadow'

		$('.app-button').click ->
			$.scrollTo(appsToTop,500);
