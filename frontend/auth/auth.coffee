define ['../packages/jquery/dist/jquery.min.js'], ($)->
    class Auth
        constructor: ->
            console.log
            $.fn.mask = ->
                $(@).on 'keyup', (e)->
                    res = ''
                    num = $(@).val().replace(/[^0-9]/g,'')
                    num = '9' + num.slice(1)
                    if e.keyCode is 8
                        if num.slice(-1) is '-' or num.slice(-1) is ' '
                            $(@).val num.slice 0,-1
                    else
                        for val,i in num.replace /[-\s]/gi,''
                            res += val
                            if i is 2
                                res += ' '
                            if i is 5 or i is 7
                                res += '-'
                        $(@).val res
        

        showAuthWin: ->
            self = @
            do @closeAuth
            do @openAuth
            elem = require('./templates/main-auth-win.jade')()

            $('body').append elem

        showAuthPhoneWin: ->
            self = @
            do @closeAuth
            do @openAuth
            elem = require('./templates/auth-phone-win.jade')()

            $('body').append elem

            do $('#auth .phone').mask
            $('#auth .phone').focus()

            $('#auth').submit (e)->
                phone = $('.phone').val().replace(/[-\s]/gi,'')
                $('.auth-window').addClass 'load'
                $.get '/auth/register?msisdn='+phone, (data)->
                    if data.code is 0
                        do self.showAuthConfirmWin
                        self.msisdn = phone
                    else
                        $('.auth-window').removeClass 'load'
                        $('#auth .phone').addClass 'error'
                        $('.auth-window .info-win').empty().append('<div class="error">'+data.message+'</div>');
                
                do e.preventDefault

        showAuthConfirmWin: ->
            self = @
            @actionLoginBy = 'code'
            do @closeAuth
            do @openAuth
            elem = require('./templates/auth-phone-confirm-win.jade')()

            $('body').append elem
            $('#auth .code').focus()

            $('#auth').submit (e)->
                phone = $('.code').val()
                $('.auth-window').addClass 'load'
                if self.actionLoginBy is 'code'
                    url = '/auth/login/ByCode?msisdn='+self.msisdn+'&code='+phone.replace(/[-\s]/gi,'')
                else
                    url = '/auth/login/ByPass?msisdn='+self.msisdn+'&pass='+$('.pass').val()

                $.get url, (data)->
                    if data.code is 0

                        if self.actionLoginBy is 'code'
                            _gaq.push(['_trackEvent', 'auth', 'byCode', self.msisdn])
                        else
                            _gaq.push(['_trackEvent', 'auth', 'byPassword', self.msisdn])

                        location.href = ''

                    else
                        $('.auth-window').removeClass 'load'
                        $('#auth .phone').addClass 'error'
                        $('.auth-window .container').empty().append('<div class="error">'+data.message+'</div>');
                
                do e.preventDefault

            $('.new-code').click ->
                $('.auth-window').addClass 'load'
                $.get '/registerByCodeRequest?msisdn='+self.msisdn, (data)->
                    if data.code is 0
                        do self.showAuthConfirmWin
                    else
                        $('.auth-window').removeClass 'load'
                        $('#auth .phone').addClass 'error'
                        $('.auth-window .info-win .container').empty().append('<div class="error">'+data.message+'</div>');

            $('#auth .phone-label .activate-pass').click ->
                if !$(@).hasClass 'active'
                    $('#auth .pass').focus()
                    $(@).addClass 'active'
                    $('#auth .phone-label .activate-sms').removeClass 'active'
                    self.actionLoginBy = 'pass'
                    $('#auth .by-code').hide()
                    $('#auth .by-pass').show()
                    $('.info-win .container').empty()
                    $('.info-win .new-code').hide()

            $('#auth .phone-label .activate-sms').click ->
                if !$(@).hasClass 'active'
                    $('#auth .code').focus()
                    $(@).addClass 'active'
                    $('#auth .phone-label .activate-pass').removeClass 'active'
                    self.actionLoginBy = 'code'
                    $('#auth .by-code').show()
                    $('#auth .by-pass').hide()
                    $('.info-win .container').text(locale.auth.codeSendMess)
                    $('.info-win .new-code').show()

        openAuth: ->
            window.pageScrollTop = $(window).scrollTop()

            $('.wrapper').css
                'position': 'fixed'
                'height': '100%'

            $('.wrapper-margin').css 'margin-top', -window.pageScrollTop
            $(window).scrollTop 0

        closeAuth: ->
            $('.auth-overlay').remove()
            $('.wrapper').attr 'style', ''
            $('.wrapper-margin').attr 'style', ''

            if window.pageScrollTop
                $(window).scrollTop window.pageScrollTop

        completeSocialRegister: (loginState,key,socialName,userData)->
            if loginState is true
                location.href = location.origin
            else
                self = @
                do auth.closeAuth
                do auth.openAuth

                elem = require('./templates/social-reg-win.jade')(userData);

                $('body').append elem
                do $('#auth .phone').mask
                $('#auth .phone').focus()

                $('#auth').submit (e)->
                    do e.preventDefault
                    number = $('.phone').val().replace(/[-\s]/gi,'')
                    urlData =
                        "msisdn": number,
                        "socialServiceName":socialName
                        "socialServiceToken": JSON.stringify key

                    $('.auth-window').addClass 'load'

                    $.get '/auth/registerSocial',urlData, (data)=>
                        try
                            if data.code is 0
                                elem = require('./templates/social-reg-proceed-win.jade')(userData);
                                do auth.closeAuth
                                do auth.openAuth

                                $('body').append elem
                                $('#auth .code').focus()

                                $('#auth').submit (e)->
                                    $('.auth-window').addClass 'load'
                                    do e.preventDefault
                                    code = $('.code').val()
                                    urlData =
                                        "msisdn": number,
                                        "code":code,
                                        "socialServiceName":socialName
                                        "socialServiceToken": JSON.stringify key


                                    url = '/auth/login/byCode/social'

                                    $.get url, urlData, (data)->
                                        try
                                            if data.code is 0
                                                location.href = ''
                                            else
                                                $('.auth-window').removeClass 'load'
                                                $('.auth-window .info-win').empty().append('<div class="error">'+data.message+'</div>');

                                        catch e
                                            $('.auth-window').removeClass 'load'
                                            $('.auth-window .info-win').empty().append('<div class="error">Неизвестная ошибка</div>');

                            else
                                $('.auth-window').removeClass 'load'
                                $('.auth-window .info-win').empty().append('<div class="error">'+data.message+'</div>');
                        catch e
                            console.log e
                            $('.auth-window').removeClass 'load'
                            $('.auth-window .info-win').empty().append('<div class="error">Неизвестная ошибка</div>');


    auth = new Auth

    

    return auth