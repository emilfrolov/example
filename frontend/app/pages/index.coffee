define ['../config/log','../parts/player','../parts/sidebar'], (Logger,Player,Sidebar)->
	class Router extends Marionette.AppRouter
		appRoutes:
			'track/:id': 'playedTrack'
			'': 'showRecomend'
			'new': 'showNew'
			'feed': 'showFeed'
			'recomend': 'showRecomend'
			'playlists': 'showPlaylists'
			'playlist/:id': 'showPlaylist'
			'id:id': 'showUserPage'
			'video': 'showVideo'
			'profile(/:tab)': 'showProfile'
			'user/tracks': 'showUserTracks'
			'user/albums': 'showUserAlbums'
			'album/:id': 'showAlbum'
			'artist/:id': 'showArtist'
			'search/tracks': 'searchTracks'
			'search/albums': 'searchAlbums'
			'search/artists': 'searchArtist'

	class API extends Marionette.Controller
		showRecomend: ->
			require.ensure [], (require)->
				Recomend = require('./recomend')
				$('#header h1').text app.locale.menu.popular
				do Recomend.Controller.showPage
				app.trigger 'switch:page', 'recomend'
				app.GAPageAction 'page-open','Главная'
				Logger.createNewLog app.getRequestUrlSid('/generalV1/log/openSection','?section=main')

		showFeed: ->
			require.ensure [], (require)->
				Feed = require('./feed')
				$('#header h1').text app.locale.menu.feed
				do Feed.Controller.showPage
				app.trigger 'switch:page', 'feed'
				app.GAPageAction 'page-open','Лента'
				Logger.createNewLog app.getRequestUrlSid('/generalV1/log/openSection','?section=wall')

		showUserPage: (lang, id)->
			require.ensure [], (require)->
				UserPage = require('./user_page')
				$('#header h1').text ''
				UserPage.Controller.showPage id
				app.trigger 'switch:page', 'user'
				app.GAPageAction 'page-open','Пользователь-'+id

		showNew: ->
			require.ensure [], (require)->
				New = require('./new')
				$('#header h1').text app.locale.menu.new
				do New.Controller.showPage
				app.trigger 'switch:page', 'new'
				app.GAPageAction 'page-open','Новинки'
				Logger.createNewLog app.getRequestUrlSid('/generalV1/log/openSection','?section=new')

		showPlaylists: ->
			require.ensure [], (require)->
				Playlists = require('./playlists')
				$('#header h1').text app.locale.menu.playlists
				do Playlists.Controller.showPage
				app.trigger 'switch:page', 'playlists'
				app.GAPageAction 'page-open','Плейлисты'

		showVideo: ->
			require.ensure [], (require)->
				Video = require('./video')
				$('#header h1').text app.locale.menu.clips
				do Video.Controller.showPage
				app.trigger 'switch:page', 'video'
				app.GAPageAction 'page-open','Клипы'

		showProfile: (leng,tab="main")->
			require.ensure [], (require)->
				Profile = require('./profile')
				$('#header h1').text app.locale.menu.profile
				Profile.Controller.showPage tab
				app.trigger 'switch:page', 'profile'
				app.GAPageAction 'page-open','Профиль'

		playedTrack: (leng,id)->
			app.GAPageAction 'page-open','Шаринг трека - '+id
			app.trigger 'show:recomend'
			Player.Controller.playTrack id

		showAlbum: (leng,id,data,direct)->
			if !direct
				Logger.createNewLog app.getRequestUrlSid("/generalV1/log/release","?id=#{id}&referer=directLink")
			require.ensure [], (require)->
				Album = require('./album')
				Album.Controller.showPage id,data

		showArtist: (leng,id,direct)->
			if !direct
				Logger.createNewLog app.getRequestUrlSid("/generalV1/log/artistCredit","?id=#{id}&referer=directLink")
			require.ensure [], (require)->
				Artist = require('./artist')
				Artist.Controller.showPage id

		showPlaylist: (leng,id,direct)->
			if !direct
				Logger.createNewLog app.getRequestUrlSid("/generalV1/log/publicPlaylist","?id=#{id}&referer=directLink")
			require.ensure [], (require)->
				Playlist = require('./playlist')
				Playlist.Controller.showPage id

		showUserTracks: ->
			require.ensure [], ->
				UserTracks = require('./user/tracks')
				app.GAPageAction 'page-open','Треки и плейлисты'
				$('#header h1').text app.locale.menu.tracksAndPlaylists
				do UserTracks.Controller.showPage
				app.trigger 'switch:page', 'tracks'

		showUserAlbums: ->
			require.ensure [], ->
				UserAlbums = require('./user/albums')
				app.GAPageAction 'page-open','Избранные альбомы'
				$('#header h1').text app.locale.menu.albums
				do UserAlbums.Controller.showPage
				app.trigger 'switch:page', 'albums'

		searchTracks: (leng,val)->
			app.GAPageAction 'page-open','Поиск трека по запросу - '+val
			Sidebar.Controller.showSearchTracksPage app.parsePath(val).q
			app.openSearchPageFromSite = false

		searchAlbums: (leng,val)->
			app.GAPageAction 'page-open','Поиск альбома по запросу - '+val
			Sidebar.Controller.showSearchAlbumsPage app.parsePath(val).q
			app.openSearchPageFromSite = false

		searchArtist: (leng,val)->
			app.GAPageAction 'page-open','Поиск артиста по запросу - '+val
			Sidebar.Controller.showSearchArtistsPage app.parsePath(val).q
			app.openSearchPageFromSite = false




	api = new API

	new Router
		controller: api


	app.on 'show:new', ->
		app.navigate "/new"
		do api.showNew

	app.on 'show:user:page', (id)->
		app.navigate "/id#{id}"
		api.showUserPage null, id

	app.on 'show:playlists', ->
		app.navigate "/playlists"
		do api.showPlaylists

	app.on 'show:feed', ->
		app.navigate "/feed"
		do api.showFeed

	app.on 'show:charts', ->
		app.navigate "/charts"
		do api.showCharts

	app.on 'show:recomend', ->
		app.navigate "/"
		do api.showRecomend

	app.on 'show:video', ->
		app.navigate "/video"
		do api.showVideo

	app.on 'show:profile', (tab="main")->
		app.navigate "/profile/#{tab}"
		api.showProfile null,tab

	app.on 'show:user:tracks', ->
		app.navigate "/user/tracks"
		do api.showUserTracks

	app.on 'show:user:albums', ->
		app.navigate "/user/albums"
		do api.showUserAlbums

	app.on 'show:album', (id,data)->
		Logger.createNewLog app.getRequestUrlSid("/generalV1/log/release","?id=#{id}&referer=#{app.getReferer()}")
		app.navigate "/album/#{id}"
		api.showAlbum null,id,data,true

	app.on 'show:playlist', (id,data)->
		Logger.createNewLog app.getRequestUrlSid("/generalV1/log/publicPlaylist","?id=#{id}&referer=#{app.getReferer()}")
		app.navigate "/playlist/#{id}"
		api.showPlaylist null,id,true

	app.on 'show:artist', (id)->
		Logger.createNewLog app.getRequestUrlSid("/generalV1/log/artistCredit","?id=#{id}&referer=#{app.getReferer()}")
		app.request 'stop:artist:request','artist'
		app.navigate "/artist/#{id}"
		api.showArtist null,id,true