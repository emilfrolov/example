define ['../../config/custom_elements/track','../../config/custom_elements/album'], (Track,AlbumElement)->
	Album = {}

	class Album.Page extends Marionette.LayoutView
		className: 'album-page'
		template: '#album-page-tpl'
		regions:
			'tracks': '.album-body'

		onShow: ->
			src = app.createImgLink(@model.get('img'),'1200x1200',null,true)
			image = new Image

			image.onload = =>
				@$('.album-head-wrapp').css 'background-image', 'url('+src+')'

			image.src = src

		events:
			'click .list .save': ->
				$('.list .save').removeClass('save').addClass('delete').text 'Удалить'
				AlbumElement.Controller.addAlbumToFavorite @model
				@model.set 'inFavorite', true
				return false

			'click .list .delete': ->
				$('.list .delete').removeClass('delete').addClass('save').text 'Сохранить'
				AlbumElement.Controller.removeAlbumFromFavorite @model.get 'id'
				@model.set 'inFavorite', false
				return false

			'click .list .go-to-author': ->
				app.trigger 'show:artist', @model.get 'artistid'

			'click .shuffle-track': ->
				trackContainer = @$('.tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()


	class Album.Track extends Track.Item
		className: 'track-item'
		template: '#track-tpl'

	class Album.Tracks extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Album.Track

	return Album