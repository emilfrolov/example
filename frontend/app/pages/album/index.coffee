define ['./view.coffee','../../parts/simple_views/update','../../parts/simple_views/loader'], (Album,Update,Loader)->
	require('./style.styl')

	class controller extends Marionette.Controller
		showPage: (id,model)->
			self = @
			
			checkFavorite = (model)->
				if model.has 'inFavorite'
					self.drawPage model
				else
					favorites = app.request 'get:favorite:albums'

					favorites.done (collection)=>
						item = collection.some (item)=>
							return item.get('id') is model.get('id')
						if item
							model.set 'inFavorite', true
						else
							model.set 'inFavorite', false

						self.drawPage model

					favorites.fail ->
						model.set 'inFavorite', false

						self.drawPage model

			if model
				checkFavorite model

			else
				album = app.request 'get:single:album',id

				# loader = new Loader.View
				# 	styles:
				# 		'top': $(window).height()/2


				# app.content.show loader

				album.done (model)=>
					checkFavorite model

				# album.fail ->
				# 	callback = ->
				# 		self.showPage id,model

				# 	update = new Update.View
				# 		styles:
				# 			'top': $(window).height()/2
				# 		callback: callback
				# 	app.content.show update

		drawPage: (model)->
			popup = require('../../parts/popup')
			page = popup.create {
				cover: {
					image: app.createImgLink(model.get('img'),'300x300'),
					type: "square"
				},
				title: {
					main: model.get('title'),
					second: "АЛЬБОМ 2015 • 12 ПЕСЕН • 52 МИН"
				},
				author: {
					avatar: model.get('img'),
					name: model.get('artist')
				},
				saveButton: {
					text: "Сохранить",
					saveTrigger: "save:button:click"
				},
				options: [
					{
						title: "Поделиться"
						trigger: "share:post"
					}
				]
			}

		_showTracks: (model)->
			if model.has 'tracks'
				@_drawTracks model.get 'tracks'
			else
				loadTracks = do model.fetch

				loadTracks.done =>
					@_drawTracks model.get 'tracks'

				loadTracks.fail (err)->
					console.log err

		_drawTracks: (tracks)->
			collection = new Backbone.Collection tracks

			collection.each (item)->
				item.set 'uniq', 'album'+item.id
				item.set 'optionsList', [
					{
						title: app.locale.options.addTPl
						class: 'add-to-playlist'
					},{
						title: app.locale.options.toArt
						class: 'options-open-artist'
						artist: true
					}
				]

			tracksView = new Album.Tracks
				collection: collection

			@layout.tracks.show tracksView

			do app.checkActiveTrack




		getHeaderCoeficent = (pathLength,changeLength)->
			return changeLength/(pathLength-100)


		resizeHeader: ->
			scrollHeight = 350
			if $(window).height()+$(window).scrollTop() >= $('.album-page')[0].offsetHeight
				return

			scrollTop = $(window).scrollTop()
			if scrollTop >= scrollHeight
				scrollTop = scrollHeight

			scrollTop = scrollTop - 100

			if scrollTop <= 0
				scrollTop = 0

			if @pageSize is 'large'
				$('.album-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.album-head .shuffle-track').css 'margin-top', 21-(scrollTop*getHeaderCoeficent(scrollHeight,21))
				$('.album-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.album-head .options .list').css 'margin-top', 98-(scrollTop*getHeaderCoeficent(scrollHeight,68))
				$('.album-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 190-(scrollTop*getHeaderCoeficent(scrollHeight,60))
				$('.album-head').height 478-(scrollTop*getHeaderCoeficent(scrollHeight,178))
#				$('.album-page').css 'padding-top', 485-(scrollTop*getHeaderCoeficent(scrollHeight,185))
			else
				$('.album-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.album-head .shuffle-track').css 'margin-top', 20-(scrollTop*getHeaderCoeficent(scrollHeight,20))
				$('.album-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.album-head .options .list').css 'margin-top', 30
				$('.album-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 115-(scrollTop*getHeaderCoeficent(scrollHeight,45))
				$('.album-head').height 340-(scrollTop*getHeaderCoeficent(scrollHeight,120))
#				$('.album-page').css 'padding-top', 350-(scrollTop*getHeaderCoeficent(scrollHeight,130))




	Album.Controller = new controller
	return Album