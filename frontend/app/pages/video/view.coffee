define ->
    Video = {}

    class Video.Item extends Marionette.ItemView
        template: '#video-page-item-tpl'
        className: 'video-item'
        events:
            'click': ->
                app.Parts.VideoPlayer.Controller.openVideo @model

    class Video.Items extends Marionette.CollectionView
        childView: Video.Item
        className: 'video-page page'

        onBeforeRenderCollection: ->
            @attachHtml = (collectionView, itemView, index)->
                collectionView.$el.append itemView.el
                    .append ' '
    return Video