define ['./view.coffee'], (Video)->
	require('./style.styl')
	
	class controller extends Marionette.Controller
		showPage: ->
			showVideos = (items)->
				view = new Video.Items
					collection: items
#
				app.content.show view

			videos = app.request('get:videos')

			if videos.promise
				videos.done (items)->
					showVideos items

				videos.fail ->
					#app.Notification.Controller.showNotification app.locale.notification.videoOpenError

			else
				showVideos videos

	Video.Controller = new controller
	return Video