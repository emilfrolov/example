define ['../../config/custom_elements/track','../../config/custom_elements/album','../../config/custom_elements/artist/','../../config/custom_elements/playlist','../../parts/player','../../parts/video_player'] , (CustomTrack,CustomAlbum,CustomArtist,CustomPlaylist,Player,VideoPlayer)->
	Recomend = {}

	class Recomend.Layout extends Marionette.LayoutView
		template: ->
			return """
				<div class="main">
					<div class="video">
						<video src="" loop autoplay></video>
					</div>
					<div class="content"></div>
				</div>
    			<div class="blocks"></div>
			"""
		className: 'page recomend-page'
		regions:
			main: '.main .content'
			blocks: '.blocks'

		onShow: ->
			$('body').css 'background', '#111112'
			date = new Date()
			day = date.getDay()
			day = if day > 6 then 6 else day
			$('.video video').attr 'src', 'http://music.beeline.ru/public/video/recomend/music-bg-'+day+'.mp4'

		onDestroy: ->
			$('body').attr 'style', ''


	class Recomend.MainLayout extends Marionette.LayoutView
		template: (data)->
			return require('./templates/recomend-main-layout.jade')(data)

		className: 'main-block-wrapp'
		regions:
			album: '.album-block'
			artist: '.artist-block'
			track: '.track-block'
			playlist: '.playlist-block'
			charts: '.charts'

		events:
			'click .clip-block': ->
				model = new Backbone.Model @model.get('clip')
				VideoPlayer.Controller.openVideo model

		onShow: ->
			self = @
			@player = new YT.Player 'youtube-video-bg',
				videoId: @model.get('clip').id

				events:
					'onReady': self.startVideo,
					'onStateChange': self.onPlayerStateChange

				playerVars:
					frameborder:0
					allowfullscreen: true
					controls: 0
					disablekb: 0 # отключает клавиши управления проигрывателем.
					loop: 1
					rel: 0
					iv_load_policy: 3 # отключает аннотации
					showinfo: 0 # Этот параметр определяет, будут ли воспроизводиться похожие видео после завершения показа исходного видео
					modestbranding: 0 # Этот параметр позволяет использовать проигрыватель YouTube, в котором не отображается логотип YouTube.

		onPlayerStateChange: ->
#			console.log arguments

		startVideo: (data)->
			data.target.setVolume 0
			data.target.playVideo()

	class Recomend.BlocksLayout extends Marionette.LayoutView
		template: '#recomend-blocks-layout-tpl'
		className: 'recomend-blocks-wrapp'
		regions:
			playlists: '.playlists-container'
			releases: '.releases-container'
			clips: '.clips-container'
			artist: '.artist-container'
			charts: '.charts-container'

	class Recomend.ChartsItem extends CustomTrack.Item
		template: '#recomend-charts-item-tpl'
		className: 'main-chart-item track-item'

	class Recomend.Charts extends Marionette.CompositeView
		template: '#recomend-charts-block-tpl'
		childView: Recomend.ChartsItem
		childViewContainer: '.charts-container'
		className: 'main-charts-block'


	class Recomend.Album extends CustomAlbum.Item
		template: '#recomend-album-item-tpl'
		className: 'album-item recomend-main-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link

		onShow: ->
			$clamp(@$('.title')[0], {clamp: 2});
			$clamp(@$('.artist')[0], {clamp: 2});

	class Recomend.Playlist extends CustomPlaylist.Item
		template: '#recomend-playlist-item-tpl'
		className: 'album-item recomend-main-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('image'),'300x300')
			img.src = link

		onShow: ->
			$clamp(@$('.title')[0], {clamp: 2});
			$clamp(@$('.artist')[0], {clamp: 2});

	class Recomend.Track extends Marionette.ItemView
		template: '#recomend-track-item-tpl'
		className: 'track-item recomend-main-item'
		id: ->
			return @model.get 'uniq'

		events:
			'click': ->
				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					app.playedAlbum = null
					Player.Controller.playTrack @model.get('id'),@model.toJSON(),Backbone.history.location.href
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link

		onShow: ->
			$clamp(@$('.title')[0], {clamp: 2});
			$clamp(@$('.artist')[0], {clamp: 2});

	class Recomend.Artist extends CustomArtist.Item
		template: '#recomend-artist-item-tpl'
		className: 'recomend-main-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link

		onShow: ->
			$clamp(@$('.title')[0], {clamp: 2});

	#Блок плейлистов

	class Recomend.PlaylistItem extends CustomPlaylist.Item
		template: '#playlist-item-tpl'
		className: 'playlist-item album-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('image'),'300x300')
			img.src = link
		onShow: ->
			@$el.after(' ')

	class Recomend.PlaylistsBlock extends Marionette.CompositeView
		childView: Recomend.PlaylistItem
		template: '#recomend-block-container-tpl'
		className: 'recomend-playlists-block recomend-block playlists-wrapper'
		childViewContainer: '.block-content'
		onShow: ->
			if @model.get('background_image')?
				@$el.css 'background-image', 'url("'+@model.get('background_image')+'")'
			if @model.get('background_colour')?
				@$el.css 'background-color', @model.get('background_colour')

	#Блок релизов

	class Recomend.ReleaseItem extends CustomAlbum.Item
		template: '#album-item-tpl'
		className: 'album-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link
		onShow: ->
			@$el.after(' ')

	class Recomend.ReleasesBlock extends Marionette.CompositeView
		childView: Recomend.ReleaseItem
		template: '#recomend-block-container-tpl'
		className: 'recomend-releases-block recomend-block albums-wrapper'
		childViewContainer: '.block-content'
		onShow: ->
			if @model.get('background_image')?
				@$el.css 'background-image', 'url("'+@model.get('background_image')+'")'
			if @model.get('background_colour')?
				@$el.css 'background-color', @model.get('background_colour')

	#Блок клипов

	class Recomend.ClipItem extends Marionette.ItemView
		template: '#recomend-video-item-tpl'
		className: 'recomend-video-item'
		events:
			'click': ->
				VideoPlayer.Controller.openVideo @model

		onRender: ->
			snippet = @model.get 'snippet'
			snippet.resourceId = {videoId: @model.get('id')}
			@model.set 'snippet', snippet

		onShow: ->
			@$el.after(' ')

	class Recomend.ClipsBlock extends Marionette.CompositeView
		childView: Recomend.ClipItem
		template: '#recomend-block-container-tpl'
		className: 'recomend-clips-block recomend-block clips-wrapper'
		childViewContainer: '.block-content'
		onShow: ->
			if @model.get('background_image')?
				@$el.css 'background-image', 'url("'+@model.get('background_image')+'")'
			if @model.get('background_colour')?
				@$el.css 'background-color', @model.get('background_colour')

	#Блок чартов

	class Recomend.ChartItem extends CustomTrack.Item
		template: '#recomend-chart-item-tpl'
		className: 'recomend-chart-item track-item'

	class Recomend.ChartsBlock extends Marionette.CompositeView
		childView: Recomend.ChartItem
		template: '#recomend-block-container-tpl'
		className: 'recomend-charts-block recomend-block charts-wrapper'
		childViewContainer: '.block-content'
		onShow: ->
			if @model.get('background_image')?
				@$el.css 'background-image', 'url("'+@model.get('background_image')+'")'
			if @model.get('background_colour')?
				@$el.css 'background-color', @model.get('background_colour')

	#Блок артиста

	class Recomend.ArtistItem extends Marionette.LayoutView
		template: '#recomend-artist-popular-item-tpl'
		className: ->
			className = 'recomend-artist-popular-item'
			if @model.get('type') is 'album'
				className += ' album-item'
			else if @model.get('type') is 'track'
				className += ' track-item'

			return className

		regions:
			optionsRegion: '.options'

		id: ->
			if @model.get('type') isnt 'track'
				return @model.get 'id'


		onRender: ->
			if @model.get('type') is 'track'
				@model.set 'uniq','recomenduserartist-'+@model.id

				@model.set 'optionsList', [
					{
						title: app.locale.options.addTPl
						class: 'add-to-playlist'
					},{
						title: app.locale.options.toArt
						class: 'options-open-artist'
					},{
						title: app.locale.options.toAlb
						class: 'options-open-album'
					}
				]
				@$el.attr 'id', @model.get 'uniq'


			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link

		onShow: ->
			@$el.after(' ')

		events:
			'click .play-album': (e)->
				e.stopPropagation()
				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					CustomAlbum.Controller.Play @model

			'mouseenter': ->
				favorites = app.request 'get:favorite:albums'

				if favorites.promise
					favorites.done (collection)=>
						item = collection.some (item)=>
							return item.get('id') is @model.get('id')
						if item
							@model.set 'inFavorite', true
							@$('.move').removeClass('add').addClass('delete')
						else
							@model.set 'inFavorite', false
							@$('.move').removeClass('delete').addClass('add')

					favorites.fail (err)->
						console.log arguments

				else
					item = favorites.some (item)=>
						return item.get('id') is @model.get('id')
					if item
						@model.set 'inFavorite', true
						@$('.move').removeClass('add').addClass('delete')
					else
						@model.set 'inFavorite', false
						@$('.move').removeClass('delete').addClass('add')

			'click .controll-album': ->
				app.trigger 'show:album', @model.get('id'), @model

			'click .info .title-album': ->
				app.trigger 'show:album', @model.get('id'), @model

			'click .move': (e)->
				do e.stopPropagation

			'click .options': (e)->
				do e.stopPropagation
				CustomAlbum.Controller.openOptions @

			'click .add': ->
				do @addToAlbum

			'click .delete': ->
				do @removeFromAlbum

			'click .to-artist': ->
				app.trigger 'show:artist', @model.get 'artistid'

			'click .play-track': ->
				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					app.playedAlbum = null
					Player.Controller.playTrack @model.get('id'),@model.toJSON(),Backbone.history.location.href

		addToAlbum: ->
			$('.options-wrapper .add').removeClass('add').addClass('delete').text('Удалить')
			CustomAlbum.Controller.addAlbumToFavorite @model
			@model.set 'inFavorite', true
			@$('.move').removeClass('add').addClass('delete')
			return false

		removeFromAlbum: ->
			$('.options-wrapper .delete').removeClass('delete').addClass('add').text('Сохранить')
			CustomAlbum.Controller.removeAlbumFromFavorite @model.get 'id'
			@model.set 'inFavorite', false
			@$('.move').removeClass('delete').addClass('add')
			return false

	class Recomend.ArtistBlock extends Marionette.CompositeView
		childView: Recomend.ArtistItem
		template: '#recomend-block-container-tpl'
		className: 'recomend-artist-popular-block recomend-block artist-popular-wrapper'
		childViewContainer: '.block-content'
		onShow: ->
			if @model.get('background_image')?
				@$el.css 'background-image', 'url("'+@model.get('background_image')+'")'
			if @model.get('background_colour')?
				@$el.css 'background-color', @model.get('background_colour')
	return Recomend