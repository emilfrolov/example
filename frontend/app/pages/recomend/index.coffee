define ['./view.coffee'], (Recomend)->
	require('./style.styl')
	
	class controller extends Marionette.Controller
		showPage: ->
			@layout = new Recomend.Layout

			app.content.show @layout

			do @showMainBlock
			do @showRecomendBlocks

		showMainBlock: ->
			self = @
			showMain = (model)->
				if not model.has 'isEmpty'
					layout = new Recomend.MainLayout
						model: model

					self.layout.main.show layout

					self.showMainItems layout,model

			main = app.request 'get:recomend:main'

			if main.promise
				main.done (model)->
					showMain model

				main.fail ->
					console.log arguments

			else
				showMain main

		showMainItems: (layout,model)->
			albumModel = new Backbone.Model model.get 'album'

			album = new Recomend.Album
				model: albumModel
			layout.album.show album

			playlistModel = new Backbone.Model model.get 'playlist'
			playlistTitle = playlistModel.get 'title'
			playlistTitle = playlistTitle.slice(0,1).toUpperCase()+playlistTitle.slice(1)
			playlistModel.set 'title', playlistTitle
			playlist = new Recomend.Playlist
				model: playlistModel
			layout.playlist.show playlist

			class trackModel extends Backbone.Model
				urlRoot: '/favoritesTracks'
				defaults:
					duration: ''

			trackData = model.get 'track'

			if trackData.is_deleted
				trackData.favourites.sort = +trackData.favourites.sort - 100000

			trackData.uniq = 'recomenduser-'+model.id

			trackData.optionsList = [
				{
					title: app.locale.options.addTPl
					class: 'add-to-playlist'
				},{
					title: app.locale.options.toArt
					class: 'options-open-artist'
				},{
					title: app.locale.options.toAlb
					class: 'options-open-album'
				}
			]



			trackModel = new trackModel trackData
			track = new Recomend.Track
				model: trackModel
			layout.track.show track

			artistModel = new Backbone.Model model.get 'artist'
			artist = new Recomend.Artist
				model: artistModel
			layout.artist.show artist

			chart = model.get 'chart'
			chartItems = chart['items']
			for item,i in chartItems
				item['index'] = i+1
				_.extend item, item.track

				if !item.track?
					item['is_deleted'] = true

			chart = new Backbone.Model chart
			chartItems = new Backbone.Collection chartItems

			chartView = new Recomend.Charts
				model: chart
				collection: chartItems

			layout.charts.show chartView

		showRecomendBlocks: ->
			self = @
			showPageBlocks = (items)->
				layout = new Recomend.BlocksLayout
				self.layout.blocks.show layout
				self.drawPageBlocks layout, items

			blocks = app.request 'get:recomend:blocks'
			if blocks.promise
				blocks.done (item)->
					showPageBlocks item

				blocks.fail ->
					console.log 'err'

			else
				showPageBlocks blocks

		drawPageBlocks: (layout,blocks)->
			if blocks.has 'playlists'
				playlistModel = new Backbone.Model blocks.get 'playlists'
				playlistCollection = new Backbone.Collection playlistModel.get 'playlists'

				playlists = new Recomend.PlaylistsBlock
					model: playlistModel
					collection: playlistCollection

				layout.playlists.show playlists

			if blocks.has 'releases'
				releaseModel = new Backbone.Model blocks.get 'releases'
				releaseCollection = new Backbone.Collection releaseModel.get 'releases'

				releases = new Recomend.ReleasesBlock
					model: releaseModel
					collection: releaseCollection

				layout.releases.show releases

			if blocks.has 'clips'
				clipsModel = new Backbone.Model blocks.get 'clips'
				clipsCollection = new Backbone.Collection clipsModel.get 'clips'

				clips = new Recomend.ClipsBlock
					model: clipsModel
					collection: clipsCollection

				layout.clips.show clips

			if blocks.has 'charts'
				chartsModel = new Backbone.Model blocks.get 'charts'
				collection = chartsModel.get 'charts'
				for item in collection
					item['chart_label'] = item.title
					_.extend item, item.track
					_.extend item, item.track

					if !item.track?
						item['is_deleted'] = true

				chartsCollection = new Backbone.Collection collection

				charts = new Recomend.ChartsBlock
					model: chartsModel
					collection: chartsCollection

				layout.charts.show charts

			if blocks.has 'artist'
				artistModel = new Backbone.Model blocks.get 'artist'
				artistCollection = new Backbone.Collection artistModel.get 'items'

				artist = new Recomend.ArtistBlock
					model: artistModel
					collection: artistCollection

				layout.artist.show artist

	Recomend.Controller = new controller

	return Recomend
