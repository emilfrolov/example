define ['./view.coffee','../../config/custom_elements/track','../../config/custom_elements/album','../../config/custom_elements/playlist','../../parts/simple_views/loader','../../parts/simple_views/update','../../parts/prompt','../../parts/notification'], (Feed,Track,Album,Playlist,Loader,Update,Prompt,Notification)->
	require('./style.styl')

	class controller extends Marionette.Controller
		showPage: ->
			@layout = new Feed.Layout

			@listenTo @layout, 'show', ->
				model = new Backbone.Model
				userView = new Feed.LayoutUser
					model: model
				@layout.head.show userView

				user = app.request 'get:user'
				if user.promise
					user.done (user)=>
						img = app.createImgLink user.get('avatar'),'50x50'

						userView.model.set 'img', img

				else
					img = app.createImgLink user.get('avatar'),'50x50'

					userView.model.set 'img', img

			app.content.show @layout

			do @showFeeds

		showCreatePost: ->
			@postCreatelayout = new Feed.PostCreateLayout

			@layout.popup.show @postCreatelayout

		showNoteCreatePanel: ->
			view = @postCreatelayout
			model = new Backbone.Model
			text = model = app.request('get:new:feed:model')
			panel = new Feed.PostCreatePanel
				model: text

			@listenTo panel, 'show', ->
				feedPostItems = app.request 'get:feed:post:items'
				postList = new Feed.PostCreatePanelList
					collection: feedPostItems

				postHead = new Feed.PostCreatePanelHead
					model: model

				user = app.request 'get:user'
				
				user.done (user)=>
					img = app.createImgLink user.get('avatar'),'50x50'

					postHead.model.set 'img', img

				panel.head.show postHead
				panel.content.show postList

			view.content.show panel

		showNoteEditPanel: (parentView)->
			viewModel = parentView.model
			do @showCreatePost
			view = @postCreatelayout
			model = new Backbone.Model
			text = model = app.request('get:new:feed:model')
			text.set 'message', viewModel.get('text')
			text.oldView = parentView
			panel = new Feed.PostCreatePanel
				model: text

			@listenTo panel, 'show', ->
				content = viewModel.get('content')
				if content.track
					parseContent = content.track
					type = 'track'

				else if content.album
					type = 'album'
					parseContent = content.album

				else if content.artist
					type = 'artist'
					parseContent = content.artist

				for item in parseContent
					item.type = type+'s'

				feedPostItems = app.request 'get:feed:post:items'

				feedPostItems.reset parseContent

				postList = new Feed.PostCreatePanelList
					collection: feedPostItems

				postHead = new Feed.PostCreatePanelHead
					model: model

				user = app.request 'get:user'
				
				user.done (user)=>
					img = app.createImgLink user.get('avatar'),'50x50'

					postHead.model.set 'img', img

				panel.head.show postHead
				panel.content.show postList

			view.content.show panel

		showFeeds: ->
			showFeeds = (feeds)=>
				$(window).off '.new'
				fetchFeeds = ->
					$(window).off '.new'
					lastId = feeds.at(feeds.length - 1).get('id')

					fetch = feeds.fetch
						data:
							lastShownId: lastId
							limit: 30

						remove: false

					fetch.done (newcollection)->
						do app.checkActiveAlbum
						do app.checkActiveTrack
						if newcollection.wallRecords? and newcollection.wallRecords.length
							winHeight = $('body').height();
							$(window).on 'scroll.new', ->
								scroll = $(window).scrollTop()+$(window).height()*5
								if winHeight < scroll
									fetchFeeds();

					fetch.fail (res)->
						if res.status is 422
							contacts.add(res.responseJSON);
						else
							console.log('Произошла ошибка');


				view = new Feed.List
					collection: feeds

				@layout.content.show view

				winHeight = $('body').height();
				$(window).on 'scroll.new', ->
					scroll = $(window).scrollTop()+$(window).height()*5
					if winHeight < scroll
						fetchFeeds();

			feeds = app.request 'get:feeds'
			if feeds.promise
				feeds.done (collection)=>
					showFeeds collection
				feeds.fail ->

			else
				showFeeds feeds

		showPostLikes: (view)->
			likesView = new Feed.ListItemLike
				model: view.model
			view.likes.show likesView

		showPostComments: (view)->
			comments = view.model.get('comments')
			collection = view.commentsCollection = new Backbone.Collection comments
			commentsView = new Feed.ListItemComments
				collection: collection
			view.comments.show commentsView

		showPostContent: (view)->
			content = view.model.get('content')
			if content?
				if content.track
					collection = new Track.Collection content.track

					collection.each (item)->
						item.set 'uniq', 'feed'+view.model.get('id')+item.id
						item.set 'optionsList', [
							{
								title: app.locale.options.addTPl
								class: 'add-to-playlist'
							},{
								title: app.locale.options.toArt
								class: 'options-open-artist'
							},{
								title: app.locale.options.toAlb
								class: 'options-open-album'
							}
						]

					tracksView = new Feed.Tracks
						collection: collection

					view.content.show tracksView

				else if content.album
					class album extends Album.Model
						parse: (model)->
							if model.album
								model.album.album = true
								return model.album
							else
								model.album = true
								return model

					album = new album content.album[0]
					albumView = new Feed.Album
						model: album

					view.content.show albumView

				else if content.playlist
					class playlist extends Playlist.Model
						parse: (model)->
							if model.album
								model.album.album = true
								return model.album
							else
								model.album = true
								return model

					playlist = new playlist content.playlist[0]
					playlistView = new Feed.Playlist
						model: playlist

					view.content.show playlistView

		updatePostCollection: (items,type)->
			for item in items
				item.set 'type', type

			feedPostItems = app.request 'get:feed:post:items'
			feedPostItems.reset items
			do @showNoteCreatePanel

		openFeedAdd: (title,trigger,selectedType,childView,type)->
			self = @
			openAddWin = (collection)->
				model = new Backbone.Model
					title: title
					type: type

				class collectionTemp extends Backbone.Collection[selectedType]
					model: Backbone.Model.Selected

				collection = new collectionTemp collection.toJSON()


				class viewTemp extends Feed.PostContentAddView
					childView: childView
					className: 'feed-content-add-wrapp '+type+'-wrapper'

				view = new viewTemp
					model: model
					collection: collection

				self.postCreatelayout.content.show view

			collection = app.request trigger
			if collection.promise
				collection.done (collection)->
					openAddWin collection
				collection.fail (res)->
					console.log res
			else
				openAddWin collection


		postCreateSuccess: (data)->
			self = @

			createPost = ->
				loader = new Loader.View
					styles:
						'position': 'absolute'
						'top': '50%'
						'transform': 'translateY(-50%)'
						'left': '50%'
						'transform': 'translateX(-50%)'

				self.postCreatelayout.content.show loader

				create = $.get app.getRequestUrlSid("/beelineMusicV1/wall/createRecord"), data

				create.done (data)=>
					if data.code is 0
						item = data.wallRecord
						model = new Backbone.Model item

						feeds = app.request 'get:feeds', true
						app.request('get:user').done (user)->
							model.set 'currentUser', user.toJSON()
							feeds.add model


							Notification.Controller.show({message: 'Ваш пост успешно добавлен',type: 'success',time: 3000})
							do self.postCreatelayout.destroy

					else
						Notification.Controller.show({message: data.message,type: 'error',time: 3000})
						do @showNoteCreatePanel

			app.request 'get:user'
				.done (user)->
					userName = user.get('profile_name')
					if (userName.length is 0) or (userName is null)
						params = 
							title: 'Как можно к вам<br>обратиться?',
							message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
							placeholder: 'Ваше имя'

						Prompt.Controller.show params,(name)->
							loader = new Loader.View
								styles:
									'position': 'absolute'
									'top': '50%'
									'transform': 'translateY(-50%)'
									'left': '50%'
									'transform': 'translateX(-50%)'

							self.postCreatelayout.content.show loader
							
							nameUpdate = $.ajax
								type: "GET",
								url: app.getRequestUrlSid("/compatibility/user/updateProfile")
								data: {
									name: name
								}

							nameUpdate.done ->
								user.set 'profile_name', name
								do createPost

					else
						do createPost

				.fail (err)->
					console.log err

		postEditSuccess: (data,view)->
			self = @
			loader = new Loader.View
				styles:
					'position': 'absolute'
					'top': '50%'
					'transform': 'translateY(-50%)'
					'left': '50%'
					'transform': 'translateX(-50%)'

			@postCreatelayout.content.show loader

			update = $.get app.getRequestUrlSid("/beelineMusicV1/wall/updateRecord"), data

			update.done (data)=>
				if data.code is 0
					model = new Backbone.Model data.wallRecord
					model.set 'currentUser', view.model.get('currentUser')
					view.model = model
					view.render()

					Notification.Controller.show({message: 'Ваш пост успешно обновлен',type: 'success',time: 3000})
					do self.postCreatelayout.destroy

				else
					Notification.Controller.show({message: data.message,type: 'error',time: 3000})
					do @showNoteCreatePanel

		saveComment: (view,comment)->
			data = 
				objectType: 'wall_record'
				objectId: view.model.get('id')
				trackIds: comment.trackIds
				text: comment.message

			saveComment = $.get(app.getRequestUrlSid('/beelineMusicV1/comment/saveComment'),data)

			saveComment.done (data)->
				if data.code is 0
					newComment = data.comment
					model = new Backbone.Model newComment
					view.commentsCollection.add model
					comments = view.model.get('comments')
					if comments?
						comments.push(newComment)
					else
						comments = [];
						comments[0] = newComment

					view.model.set 'comments', comments
					view.$('.add-comment textarea').val('').attr 'style', ''
					view.$('.comment-content').empty()
				else
					Notification.Controller.show({message: data.message, type: 'error', time: 3000})
			saveComment.fail (err)->
				Notification.Controller.show({message: 'Неизвестная ошибка', type: 'error', time: 3000})

		toggleCommentsOpen: (view)->
			if view.allCommentsIsShown is true
				view.moreCommentsCollection.reset()
				view.$('.more-comments').html 'Раскрыть все комментарии <span>'+(view.model.get('comments_count')-3)+'</span>'
				view.allCommentsIsShown = false
			else
				if view.model.has 'allComments'
					view.$('.more-comments').text 'Свернуть комментарии'
					view.moreCommentsCollection.reset(view.model.get('allComments'))
					view.allCommentsIsShown = true
				else
					data =
						lastShownId: view.model.get('comments')[0].id
						objectType: 'wall_record'
						limit: 100
						objectId: view.model.get('id')

					fetch = $.get(app.getRequestUrlSid('/beelineMusicV1/comment/getComments'),data)
					view.$('.more-comments').text 'Подождите...'
					fetch.done (data)->
						if data.code is 0
							view.model.set 'allComments', data.comments
							view.$('.more-comments').text 'Свернуть комментарии'

							collection = view.moreCommentsCollection = new Backbone.Collection data.comments
							commentsView = new Feed.ListItemComments
								collection: collection

							view.moreComments.show commentsView

							view.allCommentsIsShown = true

					fetch.fail (err)->

		showAddCommentFavouriteContent: (view)->
			app.request 'get:favorite:tracks'
				.done (tracks)->
					favouriteView = new Feed.CommentContentTrackList
						collection: tracks
					view.content.show favouriteView
				.fail (err)->
					console.log err

		showAddCommentSearchedContent: (val,view)->
			app.request 'search:tracks',
				data:
					q: val
					page: 1
					items_on_page: 60

			.done (tracks)->
				favouriteView = new Feed.CommentContentTrackList
					collection: tracks
				view.content.show favouriteView
			.fail (err)->
				console.log err

		showAddCommentContentWindow: (listItemView)->
			view = new Feed.CommentContentWindow
			commentContent = new Feed.CommentContent
			@openedCommentView = listItemView
			@layout.popup.show view

			view.content.show commentContent

			@showAddCommentFavouriteContent commentContent

		startListenNewFeeds: ->
#			feeds = app.request 'get:feeds'
#
#			@feedConnect = $.get('http://localhost:3000/subscribe');
#
#			@feedConnect.done =>
#				lastId = feeds.at(0).get('id')
#				newFeeds = $.get('/feed/items/new?lastId='+lastId);
#				newFeeds.done (data)->
#					try
#						data = JSON.parse data
#						feeds.add data.wallRecords
#					catch e
#						console.log e
#
#				@startListenNewFeeds()
#
#			@feedConnect.fail (err)=>
#				setTimeout =>
#					if @usersAbort
#						@usersAbort = false
#					else
#						console.log('try reconnect');
#						@startListenNewFeeds()
#				,500

		stopListenNewFeeds: ->
#			@usersAbort = true
#			@feedConnect.abort();


	Feed.Controller = new controller

	return Feed
