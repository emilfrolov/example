define ['../../config/custom_elements/track','../../config/custom_elements/album','../../config/custom_elements/playlist'], (Track,Album,Playlist)->
	Feed = {}

	class Feed.Layout extends Marionette.LayoutView
		template: '#feed-page-tpl'
		className: 'feed-page'
		regions:
			head: '.head-region'
			content: '.feed-content'
			popup: '.feed-popup'

	class Feed.LayoutUser extends Marionette.ItemView
		className: 'placeholder'
		template: '#feed-page-user-tpl'
		modelEvents:
			'change:img': ->
				do @render

		events:
			'click': ->
				do Feed.Controller.showCreatePost

			'click span': (e)->
				e.stopPropagation()

	class Feed.ListItem extends Marionette.LayoutView
		template: '#feed-list-item-tpl'
		regions:
			content: '.list-content'
			likes: '.like-block'
			comments: '.comments-list'
			moreComments: '.more-comments-list'

		className: 'feed-list-item'

		onRender: ->
			Feed.Controller.showPostContent @
			Feed.Controller.showPostLikes @
			Feed.Controller.showPostComments @

			autosize @$('.new-comment-text textarea')

		events:
			'click .more-comments': ->
				Feed.Controller.toggleCommentsOpen @
			'click .new-comment-text textarea': ->
				self = @
				if !@$('.add-comment').hasClass 'open'
					$(document).off '.comment'
					$('.send-comment').hide()
					$('.add-comment').removeClass 'open'

					@$('.send-comment').show()
					@$('.add-comment').addClass 'open'

					$(document).on 'click.comment', (e)->
						if !$(e.target).closest('.add-comment.open').length
							$(document).off '.comment'
							self.$('.send-comment').hide()
							self.$('.add-comment').removeClass 'open'

			'click .add-content': ->
				Feed.Controller.showAddCommentContentWindow @

			'click .send-comment': (e)->
				tracks = []
				@$('.comment-content .track-item').each ->
					tracks.push $(@).attr 'data-id'
				
				comment =
					message: @$('.add-comment textarea').val()
					trackIds: tracks

				Feed.Controller.saveComment @, comment

			'click .options': (e)->
				e.stopPropagation()
				@$('.options .list').show()

				$(window).on 'click.feedOptions', (e)->
					$(window).off '.feedOptions'
					$('.options .list').hide()

			'click .options .delete': (e)->
				e.preventDefault()
				do @model.destroy

			'click .options .edit': (e)->
				e.preventDefault()
				Feed.Controller.showNoteEditPanel @

			'click .info .title': ->
				app.trigger 'show:user:page', @model.get('owner').user_id

			'click .comment-content .track-item .delete': (e)->
				$(e.target).closest('.track-item').remove()

	class Feed.Track extends Track.Item
		template: '#feed-track-tpl'
		className: 'track-item feed-track'

	class Feed.ListItemComment extends Marionette.CompositeView
		childView: Feed.Track
		template: '#feed-list-item-comment-tpl'
		className: 'feed-list-comment-item'
		childViewContainer: '.comment-tracks'
		onBeforeRender: ->
			tracks = @model.get 'tracks'
			if tracks?
				for item in tracks
					item['uniq'] = 'feed'+@model.get('id')+item.id
					item['optionsList'] = [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]
				@collection = new Backbone.Collection tracks
		events:
			'click .post span': ->
				app.trigger 'show:user:page', @model.get('owner').user_id

	class Feed.ListItemComments extends Marionette.CollectionView
		childView: Feed.ListItemComment
		className: 'feed-list-comments-wrapp'
		viewComparator: (item)->
			# console.log @model.get('id')
			return item.get('id')

	class Feed.ListItemLike extends Marionette.ItemView
		template: '#feed-list-item-like-tpl'
		events:
			'click .like-icon': ->
				likes_count = @model.get('likes_count')
				if @model.has 'likedByMe'
					if @model.get('likedByMe') is true
						@model.set 'likedByMe', false
						likes_count--
						$.get app.getRequestUrlSid("/beelineMusicV1/like/deleteLike","?objectType=wall_record&objectId=#{@model.get('id')}")
					else
						@model.set 'likedByMe', true
						likes_count++
						$.get app.getRequestUrlSid("/beelineMusicV1/like/saveLike","?objectType=wall_record&objectId=#{@model.get('id')}")
				else
					@model.set 'likedByMe', true
					likes_count++
					$.get app.getRequestUrlSid("/beelineMusicV1/like/saveLike","?objectType=wall_record&objectId=#{@model.get('id')}")

				likes_count = if likes_count < 0 then 0 else likes_count
				@model.set 'likes_count', likes_count

		modelEvents:
			'change': ->
				do @render

	class Feed.List extends Marionette.CollectionView
		childView: Feed.ListItem
		className: 'feed-list'
		onShow: ->
			do Feed.Controller.startListenNewFeeds
		onDestroy: ->
			do Feed.Controller.stopListenNewFeeds

	class Feed.Album extends Album.Item
		template: '#feed-album-tpl'
		className: 'album-item feed-album-item'
		onRender: ->
			@$el.css 'background-image', "url('#{app.createImgLink(@model.get('img'),'150x150',true)}')"

			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'150x150')
			img.src = link

	class Feed.Playlist extends Playlist.Item
		template: '#feed-playlist-tpl'
		className: 'album-item playlist-item feed-playlist-item'
		onRender: ->
			@$el.css 'background-image', "url('#{app.createImgLink(@model.get('image'),'150x150',true)}')"

			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('image'),'150x150')
			img.src = link

	class Feed.Tracks extends Marionette.CollectionView
		childView: Feed.Track
		className: 'tracks-wrapper'

	class Feed.PostCreateLayout extends Marionette.LayoutView
		template: '#feed-note-create-layout-tpl'
		className: 'feed-note-create-overlay'
		regions:
			content: '.post-create-container'
		onShow: ->
			$('.feed-page-wrapp').css
				'position': 'fixed'
				'height': '100%'
				'left': '50%'
				'transform': 'translateX(-50%)'

			app.scrollTop = $(window).scrollTop()

			$('.feed-page-margin').css 'margin-top', -app.scrollTop
			$(window).scrollTop 0

			do Feed.Controller.showNoteCreatePanel

		onDestroy: ->
			$('.feed-page-wrapp').attr 'style', ''
			$('.feed-page-margin').attr 'style', ''

			$(window).scrollTop app.scrollTop

			model = app.request('get:new:feed:model')
			collection = app.request 'get:feed:post:items'

			model.set 'message', ''
			delete model.oldView
			do collection.reset

		events:
			'click': ->
				do @destroy

			'click .feed-note-create-wrapp': (e)->
				do e.stopPropagation

	class Feed.CommentContentWindow extends Feed.PostCreateLayout
		onShow: ->
			app.scrollTop = $(window).scrollTop()

			$('.feed-page-wrapp').css
				'position': 'fixed'
				'height': '100%'
				'left': '50%'
				'transform': 'translateX(-50%)'

			$('.feed-page-margin').css 'margin-top', -app.scrollTop
			$(window).scrollTop 0

		onDestroy: ->
			$('.feed-page-wrapp').attr 'style', ''
			$('.feed-page-margin').attr 'style', ''

			$(window).scrollTop app.scrollTop

	class Feed.CommentContentTrack extends Marionette.ItemView
		className: 'track-item feed-add-track'
		template: '#feed-add-track-tpl'
		events:
			'click': ->
				trackCompile = _.template($('#feed-add-track-tpl').html())
				trackView = """
					<div class="track-item feed-add-track" data-id="#{@model.get('id')}">
						#{trackCompile(@model.toJSON())}
					</div>
				"""
				Feed.Controller.openedCommentView.$('.comment-content').append(trackView)
				$('.feed-note-create-overlay').click()



	class Feed.CommentContentTrackList extends Marionette.CollectionView
		childView: Feed.CommentContentTrack
		className: 'tracks-wrapper'

	class Feed.CommentContent extends Marionette.LayoutView
		className: 'feed-content-add-wrapp searched tracks-wrapper'
		template: '#feed-comment-content-add-tpl'
		regions:
			content: '.feed-content-add-body'
		events:
			'click .search': ->
				if @$('.search input').is( ":hidden" )
					@$('.search input').val('Напишите название трека').show().select()

			'keyup .search input': ->
				val = $.trim @$('.search input').val()

				if @searchTimer
					clearTimeout @searchTimer

				@searchTimer = setTimeout =>
					if val isnt ''
						Feed.Controller.showAddCommentSearchedContent val, @
					else
						Feed.Controller.showAddCommentFavouriteContent @
						@$('.search input').hide().blur()
				,300



	class Feed.PostCreatePanelHead extends Marionette.ItemView
		template: '#feed-note-create-head-tpl'
		className: '#feed-note-create-head-wrapp'
		modelEvents:
			'change:img': ->
				do @render


	class Feed.PostCreatePanel extends Marionette.LayoutView
		template: '#feed-note-create-panel-tpl'
		className: 'feed-note-create-panel'

		onShow: ->
			autosize $('#textarea_resize')
			$('#textarea_resize').keyup()
			if @model.oldView
				@$('.create').text('Сохранить').show()

		regions:
			head: '.feed-note-create-head'
			content: '.feed-note-create-content'

		events:
			'keyup #textarea_resize': ->
				val = $('#textarea_resize').val()
				feedPostItems = app.request 'get:feed:post:items'

				@model.set 'message', val

				if (val is '') and (feedPostItems.length is 0)
					$('.feed-note-create-panel .create').hide()
				else
					$('.feed-note-create-panel .create').show()

			'change #textarea_resize': ->
				val = $('#textarea_resize').val()
				feedPostItems = app.request 'get:feed:post:items'

				@model.set 'message', val

				if (val is '') and (feedPostItems.length is 0)
					$('.feed-note-create-panel .create').hide()
				else
					$('.feed-note-create-panel .create').show()

			'click .create': (e)->
				e.stopPropagation()
				model = app.request('get:new:feed:model')
				collection = app.request 'get:feed:post:items'
				if collection.length
					type = collection.at(0).get('type').slice(0,-1)
				else
					type = ''

				data = {}
				data.contentType = type
				data.text = model.get('message')
				data.contentIds = collection.pluck 'id'

				if model.oldView
					view = model.oldView
					data.recordId = view.model.get('id')

					Feed.Controller.postEditSuccess data,view

				else
					Feed.Controller.postCreateSuccess data

	class Feed.PostCreatePanelItem extends Marionette.ItemView
		template: '#feed-note-create-item-tpl'
		className: 'feed-note-create-item'
		triggers:
			'click .delete': 'delete'

	class Feed.PostCreatePanelEmpty extends Marionette.ItemView
		template: '#feed-note-create-empty-tpl'
		className: 'feed-note-create-types'

	class Feed.PostCreatePanelList extends Marionette.CollectionView
		childView: Feed.PostCreatePanelItem
		emptyView: Feed.PostCreatePanelEmpty
		className: ->
			className = 'feed-note-create-content-wrapp'
			if @collection.length
				type = @collection.at(0).get('type')

				switch type
					when 'tracks'
						className += ' tracks-wrapper'

					when 'albums'
						className += ' content-wrapper'

			return className

		events:
			'click .item-type li': (e)->
				type = $(e.currentTarget).attr('data-type')
				switch type
					when 'tracks'
						Feed.Controller.openFeedAdd 'Трек','get:favorite:tracks','MultiSelected',Feed.AddTrack,'tracks'
					when 'playlists'
						Feed.Controller.openFeedAdd 'Плейлист','get:favorite:playlists','SingleSelect',Feed.AddPlaylist,'playlists'
					when 'albums'
						Feed.Controller.openFeedAdd 'Альбом','get:favorite:albums','SingleSelect',Feed.AddAlbum,'albums'

				do e.stopPropagation

		onChildviewDelete: (item)->
			@collection.remove item.model

		collectionEvents:
			'remove': ->
				if !@collection.length
					$('.post-content-message').remove()

					val = $('#textarea_resize').val()
					if val is ''
						$('.feed-note-create-panel .create').hide()


		onRender: ->
			if @collection.length
				type = @collection.at(0).get('type')
				switch type
					when 'tracks'
						title = 'Прикрепленные треки'
					when 'playlists'
						title = 'Прикрепленный плейлист'
					when 'albums'
						title = 'Прикрепленный альбом'

				@$el.prepend '<div class="post-content-message">'+title+'</div>'



	class Feed.AddTrack extends Marionette.ItemView
		template: '#feed-add-track-tpl'
		className: 'track-item feed-add-track'
		events:
			'click': ->
				if @model.selected
					@$el.removeClass 'selected'
					@model.deselect()
				else
					@$el.addClass 'selected'
					@model.select()


	class Feed.AddAlbum extends Marionette.ItemView
		template: '#feed-add-album-tpl'
		className: 'album-item feed-add-album'
		events:
			'click': ->
				if @model.selected
					@$el.removeClass 'selected'
					@model.deselect()
				else
					$('.album-item.selected').removeClass 'selected'
					@$el.addClass 'selected'
					@model.select()


		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'150x150')
			img.src = link

		onShow: ->
			@$el.after ' '

	class Feed.AddPlaylist extends Marionette.ItemView
		template: '#feed-add-playlist-tpl'
		className: 'playlist-item feed-add-playlist'
		events:
			'click': ->
				if @model.selected
					@$el.removeClass 'selected'
					@model.deselect()
				else
					$('.playlist-item.selected').removeClass 'selected'
					@$el.addClass 'selected'
					@model.select()


		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'playlist-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('image'),'150x150')
			img.src = link

		onShow: ->
			@$el.after ' '


	class Feed.PostContentAddView extends Marionette.CompositeView
		template: '#feed-post-content-add-tpl'
		childViewContainer: '.feed-content-add-body'

		events:
			'click': (e)->
				do e.stopPropagation

			'click .back': ->
				do Feed.Controller.showNoteCreatePanel

			'click .add': ->
				selected = @collection.filter (item)->
					return item.selected is true
				Feed.Controller.updatePostCollection selected,@model.get('type')

		collectionEvents:
			'select:some': ->
				@$('.add').show()

			'select:one': ->
				@$('.add').show()

			'select:none': ->
				@$('.add').hide()

			'deselect:one': ->
				@$('.add').hide()

	return Feed