define ['../../config/custom_elements/track'], (Track)->
	Playlist = {}

	class Playlist.Page extends Marionette.LayoutView
		className: 'playlist-page'
		template: '#playlist-page-tpl'
		regions:
			'content': '.content'

		onShow: ->
			src = app.createImgLink(@model.get('image'),'1200x1200',null,true)
			image = new Image

			image.onload = =>
				@$('.playlist-head-wrapp').css 'background-image', 'url('+src+')'

			image.src = src

		events:
			'click .shuffle-track': ->
				trackContainer = $('.tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

	class Playlist.Track extends Track.Item
		className: 'track-item'
		template: '#track-tpl'

	class Playlist.Tracks extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Playlist.Track

	return Playlist