define ['./view.coffee','../../parts/simple_views/loader','../../parts/simple_views/update'], (Playlist,Loader,Update)->
	require('./style.styl')

	class controller extends Marionette.Controller
		showPage: (id,model)->
			self = @
			playlist = app.request 'get:playlist',id
			if model
				@drawPage model
			else
				if playlist.promise
					loader = new Loader.View
						styles:
							'top': $(window).height()/2


					app.content.show loader

					playlist.done (model)=>
						@drawPage model

					playlist.fail ->
						callback = ->
							self.showPage id

						update = new Update.View
							styles:
								'top': $(window).height()/2
							callback: callback
						app.content.show update
				else
					@drawPage playlist


		drawPage: (model)->
			app.GAPageAction 'page-open', "Плейлист - #{model.get('title')}"

			author = model.get 'title'
			if author.length >= 30
				author = author.slice(0,27)+"..."

			@layout = new Playlist.Page
				model: model

			@listenTo @layout, 'show', ->
				@showTracks model

				if $(window).width() < 1200 or $(window).height() < 800
					@pageSize = 'small'
				else
					@pageSize = 'large'

				@resizeHeader()
				$('.author-wrapp').width $('.author-wrapp .author').width()+$('.author-wrapp .release').width()+40
				$(window).on 'resize.album', =>
					if $(window).width() < 1200 or $(window).height() < 800
						if @pageSize isnt 'small'
							@pageSize = 'small'
							@resizeHeader()
					else
						if @pageSize isnt 'large'
							@pageSize = 'large'
							@resizeHeader()

				$(window).on 'scroll.album', =>
					do @resizeHeader

			@listenTo @layout, 'destroy', ->
				$(window).off '.album'

			app.content.show @layout


		showTracks: (model)->
			self = @
			showTracks = (collection)->
				for item in collection
					item['uniq'] = 'track'+item.id
					item['optionsList'] = [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]

				collection = new Backbone.Collection collection
				tracksView = new Playlist.Tracks
					collection: collection

				self.layout.content.show tracksView
				do app.checkActiveTrack

			if model.has 'tracks'
				showTracks model.get 'tracks'
			else
				loader = new Loader.View
					styles:
						'margin': '100px auto'
				@layout.content.show loader

				fetch = model.fetch()

				self = @
				fetch.done ->
					showTracks model.get 'tracks'
				fetch.fail ->
					callback = ->
						self.showTracks model
					update = new Update.View
						styles:
							'margin': '100px auto'
						callback: callback
					@layout.content.show update




		getHeaderCoeficent = (pathLength,changeLength)->
			return changeLength/(pathLength-0)


		resizeHeader: ->
			scrollHeight = 550
			if $(window).height()+$(window).scrollTop() >= $('.playlist-page')[0].offsetHeight
				return

			scrollTop = $(window).scrollTop()
			if scrollTop >= scrollHeight
				scrollTop = scrollHeight


			if scrollTop <= 0
				scrollTop = 0

			if @pageSize is 'large'
				$('.playlist-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.playlist-head .shuffle-track').css 'margin-top', 21-(scrollTop*getHeaderCoeficent(scrollHeight,21))
				$('.playlist-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.playlist-head .options .list').css 'margin-top', 98-(scrollTop*getHeaderCoeficent(scrollHeight,68))
				$('.playlist-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 190-(scrollTop*getHeaderCoeficent(scrollHeight,60))
				$('.playlist-head').height 478-(scrollTop*getHeaderCoeficent(scrollHeight,178))
#				$('.playlist-page').css 'padding-top', 485-(scrollTop*getHeaderCoeficent(scrollHeight,185))
			else
				$('.playlist-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.playlist-head .shuffle-track').css 'margin-top', 20-(scrollTop*getHeaderCoeficent(scrollHeight,20))
				$('.playlist-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.playlist-head .options .list').css 'margin-top', 30
				$('.playlist-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 115-(scrollTop*getHeaderCoeficent(scrollHeight,45))
				$('.playlist-head').height 340-(scrollTop*getHeaderCoeficent(scrollHeight,120))
#				$('.playlist-page').css 'padding-top', 350-(scrollTop*getHeaderCoeficent(scrollHeight,130))




	Playlist.Controller = new controller

	return Playlist