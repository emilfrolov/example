define ['./view.coffee','../../parts/simple_views/update','../../parts/simple_views/loader','../../config/custom_elements/album'], (Artist,Update,Loader,Album)->
	require('./style.styl')

	class controller extends Marionette.Controller
		showPage: (id)->
			self = @
			artist = app.request 'get:artist',id

			if artist.promise
				loader = new Loader.View
					styles:
						'top': $(window).height()/2


				app.content.show loader

				artist.done (model)=>
					@drawPage model

				artist.fail ->
					callback = ->
						self.showPage id

					update = new Update.View
						styles:
							'top': $(window).height()/2
						callback: callback
					app.content.show update
			else
				@drawPage artist

		drawPage: (model)->
			app.GAPageAction 'page-open', "Артист - #{model.get('name')}"

			author = model.get 'name'
			if author.length >= 30
				author = author.slice(0,27)+"..."

			@layout = new Artist.Page
				model: model

			@listenTo @layout, 'show', ->
				@_showTracks model

				if $(window).width() < 1200 or $(window).height() < 800
					@pageSize = 'small'
				else
					@pageSize = 'large'


				$(window).on 'resize.album', =>
					if $(window).width() < 1200 or $(window).height() < 800
						if @pageSize isnt 'small'
							@pageSize = 'small'
							@resizeHeader()
					else
						if @pageSize isnt 'large'
							@pageSize = 'large'
							@resizeHeader()

				$(window).on 'scroll.album', =>
					do @resizeHeader

			@listenTo @layout, 'destroy', ->
				$(window).off '.album'

			app.content.show @layout


		_showTracks: (model)->
			$('.tabs .video').removeClass 'active'
			$('.tabs .tracks').addClass 'active'
			tracksLayout = new Artist.Tracks.Layout

			@listenTo tracksLayout, 'destroy', ->
				app.request 'stop:artist:request','tracks'
				app.request 'stop:artist:request','albums'

			@layout.content.show tracksLayout

			@showFavoriteTracks tracksLayout,model.get 'artistid'
			@showFavoriteAlbums tracksLayout, model.get 'artistid'
			@showCoAlbums tracksLayout, model.get 'artistid'

		showFavoriteTracks: (layout,id)->
			tracks = app.request 'get:artist:tracks',
				data: 
					'artist': id
			loader = new Loader.View
				styles:
					'margin': '100px auto'
			layout.tracks.show loader

			showTracks = (collection)->
				collection.each (item)->
					item.set 'uniq', 'artist'+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]


				tracksView = new Artist.Tracks
					collection: collection

				layout.tracks.show tracksView
				do app.checkActiveTrack


			if tracks.promise
				self = @
				tracks.done (collection)->
					showTracks collection
				tracks.fail ->
					callback = ->
						self.showFavoriteTracks layout, id
					update = new Update.View
						styles:
							'margin': '100px auto'
						callback: callback
					layout.tracks.show update

			else
				showTracks tracks


		showFavoriteAlbums: (layout,id)->
			self = @
			albums = app.request 'get:artist:albums',
				data:
					'artist': id

			showAlbums = (collection)->
				if collection.length is 0
					$('.co-albums-title').remove()
				albumsView = new Album.InlineListView
					collection: collection

				self.listenTo albumsView, 'childview:open:album:content', (album)->
					showAlbumContent album

				layout.albums.show albumsView


			if albums.promise
				albums.done (collection)->
					showAlbums collection
				albums.fail (err)->
					console.log err

			else
				showAlbums albums

		showCoAlbums: (layout,id)->
			self = @
			albums = app.request 'get:artist:albums',
				data:
					'artist': id
					'artistFilterMode': 'indirect_not_exact'

			showAlbums = (collection)->
				albumsView = new Album.InlineListView
					collection: collection

				self.listenTo albumsView, 'childview:open:album:content', (album)->
					showAlbumContent album
				layout.coAlbums.show albumsView


			if albums.promise
				albums.done (collection)->
					showAlbums collection
				albums.fail (err)->
					console.log err

			else
				showAlbums albums

		_showVideo: (model)->
			$('.tabs .tracks').removeClass 'active'
			$('.tabs .video').addClass 'active'
			loader = new Loader.View
				styles:
					'margin': '200px auto'

			@layout.content.show loader

			drawVideo = (collection)=>
				videoList = new Artist.Videos
					collection: collection

				if $('.artist-head .tabs .video').hasClass 'active'
					@layout.content.show videoList

			videoLoad =  app.request 'get:artist:videos', model.get 'name'

			if videoLoad.promise
				videoLoad.done (videos)->
					drawVideo videos

				videoLoad.fail ->
					console.log arguments

			else
				drawVideo videoLoad

		getHeaderCoeficent = (pathLength,changeLength)->
			return changeLength/(pathLength-0)


		resizeHeader: ->
			scrollHeight = 550
			if $(window).height()+$(window).scrollTop() >= $('.artist-page')[0].offsetHeight
				return

			scrollTop = $(window).scrollTop()
			if scrollTop >= scrollHeight
				scrollTop = scrollHeight


			if scrollTop <= 0
				scrollTop = 0

			if @pageSize is 'large'
				$('.artist-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.artist-head .shuffle-track').css 'margin-top', 21-(scrollTop*getHeaderCoeficent(scrollHeight,21))
				$('.artist-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.artist-head .options .list').css 'margin-top', 98-(scrollTop*getHeaderCoeficent(scrollHeight,68))
				$('.artist-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 190-(scrollTop*getHeaderCoeficent(scrollHeight,60))
				$('.artist-head').height 478-(scrollTop*getHeaderCoeficent(scrollHeight,178))
#				$('.artist-page').css 'padding-top', 485-(scrollTop*getHeaderCoeficent(scrollHeight,185))
			else
				$('.artist-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
				$('.artist-head .shuffle-track').css 'margin-top', 20-(scrollTop*getHeaderCoeficent(scrollHeight,20))
				$('.artist-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
				$('.artist-head .options .list').css 'margin-top', 30
				$('.artist-head .info > span').css
					'opacity': 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))
					'padding-top': 115-(scrollTop*getHeaderCoeficent(scrollHeight,45))
				$('.artist-head').height 340-(scrollTop*getHeaderCoeficent(scrollHeight,120))
#				$('.artist-page').css 'padding-top', 350-(scrollTop*getHeaderCoeficent(scrollHeight,130))




	Artist.Controller = new controller

	return Artist