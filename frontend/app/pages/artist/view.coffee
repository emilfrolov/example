define ['../../config/custom_elements/track'], (Track)->
	Artist = {}

	class Artist.AlbumModel extends Backbone.Model
		parse: (item)->
			console.log item
			item.uniq = 'artist-'+item.id
			return item

	class Artist.AlbumCollection extends Backbone.Collection
		model: Artist.AlbumModel

	class Artist.Page extends Marionette.LayoutView
		className: 'artist-page'
		template: '#artist-page-tpl'
		regions:
			'content': '.content'

		onShow: ->
			src = app.createImgLink(@model.get('img'),'1200x1200',null,true)
			image = new Image

			image.onload = =>
				@$('.artist-head-wrapp').css 'background-image', 'url('+src+')'

			image.src = src

		events:
			'click .tabs .tracks': ->
				if !@$('.tabs .tracks').hasClass 'active'
					Artist.Controller._showTracks @model

			'click .tabs .video': ->
				if !@$('.tabs .video').hasClass 'active'
					Artist.Controller._showVideo @model

			'click .shuffle-track': ->
				trackContainer = $('.artist-tracks-container .tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

	class Artist.Track extends Track.Item
		className: 'track-item'
		template: '#track-tpl'

	class Artist.Tracks extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Artist.Track

	class Artist.Video extends Marionette.ItemView
		className: 'video-item'
		template: '#video-item-tpl'
		events:
			'click': ->
				app.Parts.VideoPlayer.Controller.openVideo @model

	class Artist.Videos extends Marionette.CollectionView
		childView: Artist.Video
		className: 'video-wrapp'
		onBeforeRenderCollection: ->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.append itemView.el
				.append ' '

	class Artist.Tracks.Layout extends Marionette.LayoutView
		template: '#artist-tracks-layout-tpl'
		className: 'tracks-wrapp'
		regions:
			tracks: '.artist-tracks-container'
			albums: '.artist-albums-container'
			coAlbums: '.artist-co-albums-container'

	return Artist