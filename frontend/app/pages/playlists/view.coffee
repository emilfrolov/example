define ['../../config/custom_elements/playlist'], (Playlist)->
	Playlists = {}

	class Playlists.Item extends Playlist.Item
		template: '#playlist-item-tpl'
		className: 'playlist-item album-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('image'),'300x300')
			img.src = link
		onShow: ->
			@$el.after(' ')

	class Playlists.List extends Marionette.CollectionView
		childView: Playlists.Item
		className: 'playlists-wrapper playlists-page'

	return Playlists