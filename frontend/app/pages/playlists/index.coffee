define ['./view.coffee','../../parts/simple_views/loader','../../parts/simple_views/update'], (Playlists,Loader,Update)->
	require('./style.styl')

	class controller extends Marionette.Controller
		showPage: ->
			self = @
			playlists = app.request 'get:playlists'

			if playlists.promise
				loader = new Loader.View
					styles:
						'position': 'absolute'
						'top': '50%'
						'transform': 'translateY(-50%)'
						'left': '50%'
						'transform': 'translateX(-50%)'

				app.content.show loader


				playlists.done (collection)=>
					if location.href.indexOf '/playlists'
						@showPlaylistsList collection

				playlists.fail =>
					callback = ->
						app.trigger 'show:playlists'

					update = new Update.View
						container: 'slider-update'
						styles:
							'margin-top': '200px'
						callback: callback

					@layout.content.show update
			else
				@showPlaylistsList playlists

		showPlaylistsList: (collection)->
			$(window).off '.new'

			startFetch = ->
				$(window).off '.new'
				offset = playlistsList.length

				fetch = playlistsList.fetch
					data:
						offset: offset
						count: 60

					remove: false

				fetch.done (newcollection)->
					if newcollection.searched.length
						winHeight = $('body').height();
						$(window).on 'scroll.new', ->
							scroll = $(window).scrollTop()+$(window).height()*5
							if winHeight < scroll
								startFetch();

				fetch.fail (res)->
					if res.status is 422
						contacts.add(res.responseJSON);
					else
						console.log('Произошла ошибка');

			playlistsList = app.request 'get:temp:playlists', collection

			view = new Playlists.List
				collection: playlistsList

			app.content.show view

			winHeight = $('body').height();
			$(window).on 'scroll.new', ->
				scroll = $(window).scrollTop()+$(window).height()*5
				if winHeight < scroll
					startFetch();


	Playlists.Controller = new controller
	return Playlists