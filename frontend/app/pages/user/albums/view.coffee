define ['../../../config/custom_elements/track'], (Track)->
	Albums = {}

	class Albums.Layout extends Marionette.LayoutView
		template: '#user-albums-layout-tpl'
		regions:
			content: '.content'
			list: '.albums-list-container'
		className: 'user-albums-page-wrapp'
		onShow: ->
			toTop = if $('body').hasClass('notifi') then 320 else 280
			albulistContainer = $('.album-list-container')
			$('.user-albums-page-wrapp .list').css 'min-height', $(window).height()-100
			albulistContainer.height $(window).height()-toTop
			$('body').css 'background', '#111112'
			$(window).on 'resize.userTracks', ->
				toTop = if $('body').hasClass('notifi') then 320 else 280
				$('.user-albums-page-wrapp .list').css 'min-height', $(window).height()-100
				albulistContainer.height $(window).height()-toTop

		onDestroy: ->
			$('body').attr 'style', ''
			$(window).off '.userTracks'


	class Albums.Item extends Marionette.ItemView
		template: '#user-album-item-tpl'
		className: 'user-album-item list-item'
		onRender: ->
			if @model.has 'is_deleted'
				@$el.addClass 'disable'
		events:
			'click': ->
				if @model.has 'is_deleted'
					app.Parts.Notification.Controller.show({message: app.locale.notification.albumNotAvailable, type: 'error', time: 3000})
					return
				Albums.Controller.openAlbum @
				$('.list-item.active').removeClass 'active'
				@$el.addClass 'active'

			'click .delete': ->
				Albums.Controller.deleteAlbum @model.get 'id'

				albums = app.request 'get:favorite:albums'
				if albums.length is 0
					$('.user-albums-page-wrapp').css 'padding-left',0
					emptyView = new Albums.EmptyLayout
					Albums.Controller.layout.content.show emptyView
					$('.list').fadeOut(100);

				else
					hasAlbums = albums.some (item)-> return !item.get('is_deleted')

					if hasAlbums
						scrollHeight = $('.albums-list-container').height()-$('.albums-list-container .scrollable').height()-40
						if scrollHeight < 0
							$('.albums-list-container .scrollable').css 'margin-top',0
						$('.list .list-item').not('.disable').eq(0).click()

					else
						emptyView = new Albums.EmptyLayout
						Albums.Controller.layout.content.show emptyView
						$('.albums-empty-layout .message').css 'margin-left': '-140px'


				app.Parts.Notification.Controller.show({message: "Альбом #{@model.get('title')} удален",type: 'success'})


	class Albums.EmptyLayout extends Marionette.ItemView
		template: '#user-albums-empty-layout-tpl'
		className: 'albums-empty-layout'
		events:
			'click .to-new': ->
				app.trigger 'show:new'

		onShow: ->
			@$el.height do $(window).height-80

	class Albums.Items extends Marionette.CollectionView
		childView: Albums.Item
		className: 'scrollable'
		onShow: ->
			if @collection.length is 0
				$('.user-albums-page-wrapp').css 'padding-left',0

				emptyView = new Albums.EmptyLayout

				Albums.Controller.layout.content.show emptyView
			else
				hasAlbums = @collection.some (item)-> return !item.get('is_deleted')

				if !hasAlbums
					emptyView = new Albums.EmptyLayout

					Albums.Controller.layout.content.show emptyView
					$('.albums-empty-layout .message').css 'margin-left': '-140px'


				$('.list').fadeIn(100)
				$('.user-albums-page-wrapp').attr 'style', ''

		onBeforeRenderCollection: ()->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.prepend itemView.el


	class Albums.TrackItem extends Track.Item
		className: 'track-item user-track'
		template: '#track-tpl'

	class Albums.TrackItems extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Albums.TrackItem

	class Albums.PageLayout extends Marionette.LayoutView
		className: 'user-album-page'
		template: '#user-albums-page-layout-tpl'
		regions:
			'content': '.content'
			'playlistOptions': '.album-options'

		onShow: ->
			src = app.createImgLink(@model.get('img'),'1200x1200',null,true)
			image = new Image

			image.onload = =>
				@$('.album-head-container').css 'background-image', 'url('+src+')'

			image.src = src


			$clamp($('.album-head h1')[0], {clamp: 2});

		events:
			'click .album-options': ->
				Albums.Controller.openAlbumOptions @

			'click .shuffle-track': ->
				trackContainer = $('.playlist-content-wrapp .tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

	class Albums.OptionsLayout extends Marionette.ItemView
		template: '#user-album-options-tpl'
		tagName: 'ul'
		className: 'options-list'
		events:
			'mouseleave': ->
#				do @destroy

			'click .options-overlay': ->
				do @destroy

			'click': ->
				return false

			'click .delete-album': ->
				view = Marionette.Renderer.render('#user-album-delete-confirm-tpl',@model.toJSON())
				@$el.empty().append view

			'click .album-delete-confirm': ->
				return false

			'click .cancel': ->
				do @destroy

			'click .delete': ->
				Albums.Controller.deleteAlbum @model.get 'id'

				albums = app.request 'get:favorite:albums'
				if albums.length is 0
					$('.user-albums-page-wrapp').css 'padding-left',0
					emptyView = new Albums.EmptyLayout
					Albums.Controller.layout.content.show emptyView
					$('.list').removeClass 'slow'
						.css 'left', '-284px'

				else
					hasAlbums = albums.some (item)-> return !item.get('is_deleted')

					if hasAlbums
						scrollHeight = $('.albums-list-container').height()-$('.albums-list-container .scrollable').height()-40
						if scrollHeight < 0
							$('.albums-list-container .scrollable').css 'margin-top',0
						$('.list .list-item').not('.disable').eq(0).click()

					else
						emptyView = new Albums.EmptyLayout
						Albums.Controller.layout.content.show emptyView
						$('.albums-empty-layout .message').css 'margin-left': '-140px'


				app.Parts.Notification.Controller.show({message: "Альбом #{@model.get('title')} удален",type: 'success'})

	return Albums