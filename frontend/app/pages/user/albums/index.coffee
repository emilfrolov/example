define ['./view.coffee','../../../parts/simple_views/loader','../../../parts/simple_views/update'], (Albums,Loader,Update)->
	require './style.styl'

	class controller extends Marionette.Controller
		showPage: ->
			@layout = new Albums.Layout

			app.content.show @layout

			listLoader = new Loader.View
				styles:
					'top': ($(window).height()/2)-200

			@layout.list.show listLoader

			do @showUserAlbums

		showUserAlbums: ->
			self = @
			albums = app.request 'get:favorite:albums'
			showAlbums = (collection)=>
				albumsView = new Albums.Items
					collection: collection

				@layout.list.show albumsView

				$('.list .list-item').not('.disable').eq(0).click()

			if albums.promise
				loader = new Loader.View
					styles:
						'top': ($(window).height()/2)-100

				@layout.content.show loader

				albums.done (collection)->
					showAlbums collection

				albums.fail =>
					callback = ->
						do self.showUserAlbums

					update = new Update.View
						callback: callback
						styles:
							'top': ($(window).height()/2)-100

					@layout.content.show update

			else
				showAlbums albums


		openAlbum: (item)->
			self = @
			model = item.model

			layout = new Albums.PageLayout
				model: model

			@listenTo layout, 'show', ->

				@resizeHeader()

				$(window).on 'resize.album', =>
					do @resizeHeader

				$(window).on 'scroll.album', =>
					do @resizeHeader

			@listenTo layout, 'destroy', ->
				$(window).off '.album'

			@layout.content.show layout

			loader = new Loader.View
				styles:
					'top': '50px'

			layout.content.show loader

			showAlbumTracks = (tracks)->
				collection = new Backbone.Collection tracks
				collection.each (item)->
					item.set 'uniq', 'user-album'+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						},{
							title: 'Перейти к альбому'
							class: app.locale.options.toAlb
						}
					]

				tracks = new Albums.TrackItems
					collection: collection

				layout.content.show tracks
				do app.checkActiveTrack
#
#
			if model.has 'tracks'
				showAlbumTracks model.get 'tracks'
			else
				if @albumFetch
					do @albumFetch.abort

				@albumFetch = model.fetch()

				@albumFetch.done ->
					showAlbumTracks model.get 'tracks'

				@albumFetch.fail ->
					callback = ->
						self.openAlbum item

					update = new Update.View
						callback: callback
						styles:
							'top': '50px'

					layout.content.show update

		openAlbumOptions: (view)->
			optionsView = new Albums.OptionsLayout
				model: view.model

			view.playlistOptions.show optionsView

		getHeaderCoeficent = (pathLength,changeLength)->
			return changeLength/(pathLength)


		resizeHeader: ->
			scrollHeight = 550
			if $(window).height()+$(window).scrollTop() >= $('.user-album-page')[0].offsetHeight
				return

			scrollTop = $(window).scrollTop()
			if scrollTop >= scrollHeight
				scrollTop = scrollHeight

			scrollTop = scrollTop

			if scrollTop <= 0
				scrollTop = 0

			$('.album-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
			$('.album-head .shuffle-track').css 'margin-top', 20-(scrollTop*getHeaderCoeficent(scrollHeight,20))
			$('.album-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
			$('.album-head .info > span').css 'opacity', 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))

			$('.album-head').height 370-(scrollTop*getHeaderCoeficent(scrollHeight,190))
#			$('.user-playlist-page').css 'padding-top', 270-(scrollTop*getHeaderCoeficent(scrollHeight,30))


		deleteAlbum: (id)->
			albums = app.request 'get:favorite:albums'
			album = albums.findWhere
				id: id

			if album?
				do album.destroy




	Albums.Controller = new controller

	return Albums