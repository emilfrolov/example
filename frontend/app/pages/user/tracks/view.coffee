define ['../../../config/custom_elements/track','../../../parts/prompt'], (Track,Prompt)->
	Tracks = {}

	class Tracks.Layout extends Marionette.LayoutView
		template: '#user-tracks-layout-tpl'
		regions:
			content: '.content'
			list: '.play-list-container'
		className: 'user-tracks-page-wrapp'
		onShow: ->
			toTop = if $('body').hasClass('notifi') then 320 else 280
			playlistContainer = $('.play-list-container')
			$('.user-tracks-page-wrapp .list').css 'min-height', $(window).height()-100
			playlistContainer.height $(window).height()-toTop
			$('body').css 'background', '#111112'
			$(window).on 'resize.userTracks', ->
				toTop = if $('body').hasClass('notifi') then 320 else 280
				$('.user-tracks-page-wrapp .list').css 'min-height', $(window).height()-100
				playlistContainer.height $(window).height()-toTop

		onDestroy: ->
			$('body').attr 'style', ''
			$(window).off '.userTracks'

		events:
			'click .all-tracks': ->
				do Tracks.Controller.showUserTracks
				$('.list-item.active').removeClass 'active'
				@$('.all-tracks').addClass 'active'

			'click .create-playlist': ->
				$('.create-playlist p').empty().append('<input type="text" value="Новый плейлист">').find('input').select()

			'focus .create-playlist input': ->
				@$('.create-playlist').addClass 'edit'
					.parent().addClass 'edit'

			'blur .create-playlist input': ->
				@$('.create-playlist p').empty().text 'Создать плейлист'
				@$('.create-playlist').removeClass 'edit'
					.parent().removeClass 'edit'

			'keyup input': (e)->
				if e.keyCode is 13
					val = @$('input').val()
					if $.trim(val) isnt ''
						Tracks.Controller.createPlaylist val
						@$('input').blur()

				else if e.keyCode is 27
					@$('input').blur()


	class Tracks.Playlist extends Marionette.ItemView
		template: '#user-playlist-item-tpl'
		className: 'user-playlist-item list-item'
		events:
			'click': ->
				Tracks.Controller.openPlaylist @
				$('.list-item.active').removeClass 'active'
				@$el.addClass 'active'
			'focus input': ->
				@$el.addClass 'edit'
					.parent().addClass 'edit'

			'blur input': ->
				@$('p').empty().text @model.get 'title'
				@$el.removeClass 'edit'
					.parent().removeClass 'edit'

			'keyup input': (e)->
				if e.keyCode is 13
					val = @$('input').val()
					if $.trim(val) isnt ''
						@model.set 'title', @$('input').val()
						$('.playlist-head h1').text @model.get 'title'
						@model.save()

				else if e.keyCode is 27
					@$('input').blur()


		modelEvents:
			'change title': ->
				do @render

	class Tracks.Playlists extends Marionette.CollectionView
		childView: Tracks.Playlist
		className: 'container default-skin'
		onShow: ->
			$('.user-tracks-page-wrapp .list .play-list-container').customScrollbar({
				updateOnWindowResize: true
			});
		collectionEvents:
			'change': ->
				$('.user-tracks-page-wrapp .list .play-list-container').customScrollbar('resize',true)

		onBeforeRenderCollection: ()->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.prepend itemView.el

	class Tracks.ItemsLayout extends Marionette.LayoutView
		template: '#user-tracks-tpl'
		className: 'user-tracks-wrapp'
		regions:
			tracks: '.tracks-container'

		events:
			'click .shuffle': ->
				trackContainer = $('.user-tracks-wrapp')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

			'keyup .search input': ->
				Tracks.Controller.filteredTracks.filter @$('.search input').val()


	class Tracks.Item extends Track.Item
		className: 'track-item user-track'
		template: '#track-tpl'

	class Tracks.Items extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Tracks.Item

	class Tracks.EmptyPlaylist extends Marionette.ItemView
		className: 'empty-playlist-layout'
		template: '#empty-playlist-layout-tpl'
		events:
			'click .to-my-tracks': ->
				do Tracks.Controller.showUserTracks
				$('.list-item.active').removeClass 'active'
				$('.all-tracks').addClass 'active'

	class Tracks.PlaylistItems extends Marionette.CollectionView
		className: 'tracks-wrapper'
		childView: Tracks.Item
		emptyView: Tracks.EmptyPlaylist
		onRender: ->

		modelEvents:
			'change': ->
				@collection = new Backbone.Collection @model.get 'tracks'
				@collection.each (item)->
					item.set 'uniq', 'playlist'+item.id
					item.set 'playlistTrack', true
					item.set 'optionsList', [
						{
							title: 'Добавить в плейлист'
							class: 'add-to-playlist'
						},{
							title: 'Перейти к исполнителю'
							class: 'options-open-artist'
						},{
							title: 'Перейти к альбому'
							class: 'options-open-album'
						}
					]
				do @render
		onChildviewPlaylistTrackDelete: (item)->
			Track.Controller.removeTrackFromPlaylist item,@

	class Tracks.PlaylistLayout extends Marionette.LayoutView
		className: 'user-playlist-page'
		template: '#user-playlist-page-tpl'
		regions:
			'content': '.content'
			'playlistOptions': '.playlist-options'

		onShow: ->
			src = app.createImgLink(@model.get('background'),'1200x1200',null,true)
			image = new Image

			image.onload = =>
				@$('.playlist-head-container').css 'background-image', 'url('+src+')'

			image.src = src

			$clamp($('.playlist-head h1')[0], {clamp: 2});

		events:
			'click .playlist-options': ->
				Tracks.Controller.openPlaylistOptions @

			'click .shuffle-track': ->
				trackContainer = $('.playlist-content-wrapp .tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

	class Tracks.PlaylistLayoutOptions extends Marionette.ItemView
		template: '#user-playlist-options-tpl'
		tagName: 'ul'
		className: 'options-list'
		events:
			'mouseleave': ->
				do @destroy
			'click .options-overlay': ->
				do @destroy

			'click': ->
				return false

			'click .delete-pl': ->
				view = Marionette.Renderer.render('#user-playlist-delete-confirm-tpl',@model.toJSON())
				@$el.empty().append view

			'click .playlist-delete-confirm': ->
				return false

			'click .cancel': ->
				do @destroy

			'click .delete': ->
				Tracks.Controller.deletePlaylist @model.get 'id'
				do Tracks.Controller.showUserTracks
				$('.list-item.active').removeClass 'active'
				$('.all-tracks').addClass 'active'

				scrollHeight = $('.play-list-container').height()-$('.play-list-container .scrollable').height()-40
				if scrollHeight < 0
					$('.play-list-container .scrollable').css 'margin-top',0

				app.Parts.Notification.Controller.show({message: "Плейлист #{@model.get('title')} удален",type: 'success'})

			'click .rename-pl': ->
				$('.user-playlist-item.active p').empty().append('<input type="text" value="'+@model.get('title')+'">').find('input').select()
				do @destroy

			'click .is-public': ->
				self = @
				changePublicState = ->
					if $('.is-public').hasClass 'public'
						self.model.set 'is_public', false
					else
						self.model.set 'is_public', true

					$('.is-public').toggleClass 'public'

					$.get app.getRequestUrlSid("/beelineMusicV1/playlist/setPublic","?playlistId=#{self.model.get('id')}&isPublic=#{self.model.get('is_public')}");

				app.request 'get:user'
					.done (user)->
						userName = user.get('profile_name')
						if (userName.length is 0) or (userName is null)
							params = 
								title: 'Как можно к вам<br>обратиться?',
								message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
								placeholder: 'Ваше имя'

							Prompt.Controller.show params,(name)->
								nameUpdate = $.ajax
									type: "GET",
									url: app.getRequestUrlSid("/compatibility/user/updateProfile")
									data: {
										name: name
									}

								nameUpdate.done ->
									user.set 'profile_name', name
									do changePublicState

						else
							do changePublicState

					.fail (err)->
						console.log err

	return Tracks