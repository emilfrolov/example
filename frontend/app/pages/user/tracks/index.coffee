define ['./view.coffee','../../../parts/simple_views/loader','../../../parts/simple_views/update','../../../parts/prompt'], (Tracks,Loader,Update,Prompt)->
	require './style.styl'

	class controller extends Marionette.Controller
		showPage: ->
			@layout = new Tracks.Layout

			app.content.show @layout

			do @showUserPlaylist
			do @showUserTracks

		showUserPlaylist: ->
			self = @
			playLists = app.request 'get:favorite:playlists'
			showPlaylists = (collection)=>
				playlistView = new Tracks.Playlists
					collection: collection

				@layout.list.show playlistView

			if playLists.promise
				listLoader = new Loader.View
					styles:
						'top': ($(window).height()/2)-200


				@layout.list.show listLoader

				playLists.done (collection)->
					showPlaylists collection

				playLists.fail =>
					callback = ->
						do self.showUserPlaylist

					update = new Update.View
						styles:
							'top': ($(window).height()/2)-200
						callback: callback
					@layout.list.show update

			else
				showPlaylists playLists

		showUserTracks: ->
			self = @
			tracks = app.request 'get:favorite:tracks'
			showTracks = (collection)=>
				layout = new Tracks.ItemsLayout
				@layout.content.show layout

				@filteredTracks = app.Helpers.Filters.Collection.Filter
					collection: collection
					filterFunction: (filterCriterion)->
						criterion = filterCriterion.toLowerCase();
						return (contact)->
							if contact.get("artist").toLowerCase().indexOf(criterion) isnt -1 or
							contact.get("title").toLowerCase().indexOf(criterion) isnt -1
								return contact


				tracksView = new Tracks.Items
					collection: @filteredTracks

				layout.tracks.show tracksView
				do app.checkActiveTrack

				@listenTo tracksView, 'destroy', ->
					@filteredTracks.stopListenCollection()

			if tracks.promise
				loader = new Loader.View
					styles:
						'top': ($(window).height()/2)-100

				@layout.content.show loader

				tracks.done (collection)->
					showTracks collection

				tracks.fail =>
					callback = ->
						do self.showUserTracks

					update = new Update.View
						styles:
							'top': ($(window).height()/2)-100
						callback: callback
					@layout.content.show update

			else
				showTracks tracks

		openPlaylist: (item)->
			self = @
			model = item.model

			layout = new Tracks.PlaylistLayout
				model: model

			@listenTo layout, 'show', ->

				@resizeHeader()

				$(window).on 'resize.playlist', =>
					do @resizeHeader

				$(window).on 'scroll.playlist', =>
					do @resizeHeader

			@listenTo layout, 'destroy', ->
				$(window).off '.playlist'

			@layout.content.show layout

			loader = new Loader.View
				styles:
					'top': '50px'

			layout.content.show loader

			showPlaylist = (tracks)->
				collection = new Backbone.Collection tracks
				collection.each (item)->
					item.set 'uniq', 'playlist'+item.id
					item.set 'playlistTrack', true
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						},{
							title: 'Перейти к альбому'
							class: app.locale.options.toAlb
						}
					]

				tracks = new Tracks.PlaylistItems
					parentView: item
					model: item.model
					collection: collection

				layout.content.show tracks
				do app.checkActiveTrack

			if model.get('items_count') is 0
				showPlaylist []
			else
				if model.has 'tracks'
					showPlaylist model.get 'tracks'
				else
					if @playlistFetch
						do @playlistFetch.abort

					@playlistFetch = model.fetch()

					@playlistFetch.done ->
						showPlaylist model.get 'tracks'

					@playlistFetch.fail ->
						callback = ->
							self.openPlaylist item

						update = new Update.View
							callback: callback
							styles:
								'top': '50px'

						layout.content.show update

		openPlaylistOptions: (view)->
			optionsView = new Tracks.PlaylistLayoutOptions
				model: view.model

			view.playlistOptions.show optionsView

		getHeaderCoeficent = (pathLength,changeLength)->
			return changeLength/(pathLength)


		resizeHeader: ->
			scrollHeight = 550
			if $(window).height()+$(window).scrollTop() >= $('.user-playlist-page')[0].offsetHeight
				return

			scrollTop = $(window).scrollTop()
			if scrollTop >= scrollHeight
				scrollTop = scrollHeight

			scrollTop = scrollTop

			if scrollTop <= 0
				scrollTop = 0

			$('.playlist-head .info h1').css 'font-size', 60-(scrollTop*getHeaderCoeficent(scrollHeight,30))
			$('.playlist-head .shuffle-track').css 'margin-top', 20-(scrollTop*getHeaderCoeficent(scrollHeight,20))
			$('.playlist-head .author-wrapp').css 'margin-top', 10-(scrollTop*getHeaderCoeficent(scrollHeight,10))
			$('.playlist-head .info > span').css 'opacity', 1-(scrollTop*getHeaderCoeficent(scrollHeight,1))

			$('.playlist-head').height 370-(scrollTop*getHeaderCoeficent(scrollHeight,190))
#			$('.user-playlist-page').css 'padding-top', 270-(scrollTop*getHeaderCoeficent(scrollHeight,30))


		createPlaylist: (title)->
			createPlaylist = ->
				playlist = app.request 'get:favorite:playlists'
				playlist.create {'title': title}
				app.GAPageAction 'Создание плейлиста', title

			app.request 'get:user'
				.done (user)->
					userName = user.get('profile_name')
					if (userName.length is 0) or (userName is null)
						params = 
							title: 'Как можно к вам<br>обратиться?',
							message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
							placeholder: 'Ваше имя'

						Prompt.Controller.show params,(name)->
							
							nameUpdate = $.ajax
								type: "GET",
								url: app.getRequestUrlSid("/compatibility/user/updateProfile")
								data: {
									name: name
								}

							nameUpdate.done ->
								user.set 'profile_name', name
								do createPlaylist

					else
						do createPlaylist

				.fail (err)->
					console.log err

		deletePlaylist: (id)->
			playlists = app.request 'get:favorite:playlists'
			playlist = playlists.findWhere
				id: id

			if playlist?
				do playlist.destroy




	Tracks.Controller = new controller

	return Tracks