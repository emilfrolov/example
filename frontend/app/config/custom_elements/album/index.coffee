define ['./view.coffee', './model.coffee','../../../parts/player','../../../parts/notification'], (Album, Model, Player, Notification)->
	require('./style.styl')

	class controller extends Marionette.Controller
		Play: (albumModel)->
			app.playedAlbum = albumModel.get 'id'

			if albumModel.has 'tracks'
				collection = new Backbone.Collection albumModel.get 'tracks'
				collection.each (item)->
					item.set 'uniq', 'album'+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						}
					]
				Player.Controller.playByCollection collection
			else
				albumTracks = do albumModel.fetch

				albumTracks.done ->
					collection = new Backbone.Collection albumModel.get 'tracks'
					collection.each (item)->
						item.set 'uniq', 'album'+item.id
						item.set 'optionsList', [
							{
								title: app.locale.options.addTPl
								class: 'add-to-playlist'
							},{
								title: app.locale.options.toArt
								class: 'options-open-artist'
							}
						]

					href = Backbone.history.location.origin+'/'+Backbone.history.location.pathname.split('/')[1]+'/album/'+albumModel.get 'id'
					Player.Controller.playByCollection collection,null,href

				albumTracks.fail (err)->
					console.log err

		removeAlbumFromFavorite: (id)->
			favorites = app.request 'get:favorite:albums'
			album = favorites.findWhere
				id: id

			if album
				do album.destroy

				Notification.Controller.show({message: "Альбом «#{album.get 'title' }» удален из медиатеки",type: 'success',time: 3000})


		addAlbumToFavorite: (album)->
			favorites = app.request 'get:favorite:albums'
			model = album.toJSON()

			if !model.favourites or model.favourites.sort is undefined
				model.favourites = {}
				model.favourites.sort = 10000

			favorites.create model
			app.GAPageAction 'Добавление альбома в избранное', album.get('title')+' - '+location.href
			Notification.Controller.show({message: "Альбом «#{album.get('title') }» добавлен в медиатеку",type: 'success',time: 3000})

		openOptions: (item)->
			view = new Album.Options
				model: item.model
			if item.optionsRegion
				item.optionsRegion.show view


	Album.Controller = new controller

	_.extend Album, Model

	return Album