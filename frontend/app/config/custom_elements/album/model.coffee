define ->
  Album = {}

  class Album.Model extends Backbone.Model
    urlRoot: ->
      return app.getRequestUrl("/compatibility/catalogue/getAlbum","?albumid=#{@id}")
      
    parse: (model)->
      if model.album
        model.album.album = true
        return model.album
      else
        model.album = true
        return model
    defaults:
      "duration": ""
      "artist": ""
      "title": ""
      "track_count": ""
      "img": ""

  class Album.Collection extends Backbone.Collection
    model: Album.Model

  return Album