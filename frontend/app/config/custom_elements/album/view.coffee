define ['../../../parts/player','../../../parts/simple_views/update','../../../parts/simple_views/loader','../track'], (Player,Update,Loader,Track)->
	Album = {}

	class Album.Item extends Marionette.LayoutView
		template: '#album-item-tpl'
		className: 'album-item'
		onRender: ->
			img = new Image
			$(img).on 'load.img', =>

				$(img).addClass 'album-bg'
				.hide()
				.appendTo @$ '.controll'
				.fadeIn 300


				$(img).off 'load.img'
			link = app.createImgLink(@model.get('img'),'300x300')
			img.src = link

		regions:
			optionsRegion: '.options'

		id: ->
			return @model.get 'id'
		events:
			'click .player-controll': (e)->
				e.stopPropagation()
				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					Album.Controller.Play @model

			'mouseenter': ->
				favorites = app.request 'get:favorite:albums'

				if favorites.promise
					favorites.done (collection)=>
						item = collection.some (item)=>
							return item.get('id') is @model.get('id')
						if item
							@model.set 'inFavorite', true
							@$('.move').removeClass('add').addClass('delete')
						else
							@model.set 'inFavorite', false
							@$('.move').removeClass('delete').addClass('add')

					favorites.fail (err)->
						console.log arguments

				else
					item = favorites.some (item)=>
						return item.get('id') is @model.get('id')
					if item
						@model.set 'inFavorite', true
						@$('.move').removeClass('add').addClass('delete')
					else
						@model.set 'inFavorite', false
						@$('.move').removeClass('delete').addClass('add')

			'click .controll': ->
				app.trigger 'show:album', @model.get('id'), @model

			'click .info .title': ->
				app.trigger 'show:album', @model.get('id'), @model

			'click .info .artist': ->
				app.trigger 'show:artist', @model.get 'artistid'

			'click .move': (e)->
				do e.stopPropagation

			'click .options': (e)->
				do e.stopPropagation
				Album.Controller.openOptions @

			'click .add': ->
				do @addToAlbum

			'click .delete': ->
				do @removeFromAlbum

			'click .to-artist': ->
				app.trigger 'show:artist', @model.get 'artistid'

		addToAlbum: ->
			$('.options-wrapper .add').removeClass('add').addClass('delete').text('Удалить')
			Album.Controller.addAlbumToFavorite @model
			@model.set 'inFavorite', true
			@$('.move').removeClass('add').addClass('delete')
			return false

		removeFromAlbum: ->
			$('.options-wrapper .delete').removeClass('delete').addClass('add').text('Сохранить')
			Album.Controller.removeAlbumFromFavorite @model.get 'id'
			@model.set 'inFavorite', false
			@$('.move').removeClass('delete').addClass('add')
			return false

	class Album.InlineItem extends Marionette.LayoutView
		template: '#inline-album-tpl'
		className: 'album-item'
		regions:
			content: '.body .albums-tracks'
		events:
			'click .body': (e)->
				e.isPropagationStopped()

			'click .shuffle-tracks': ->
				trackContainer = @$('.tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

			'click': 'showAlbumContent'

		showAlbumContent: ->
			album = @

			openAlbum = (album,tracks)->
				album.$el.removeClass('disable').addClass('open')

				collection = new Backbone.Collection tracks

				collection.each (item)->
					item.set 'uniq', album.model.get('id')+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]

				tracksView = new Track.ListView
					collection: collection

				album.content.show tracksView
				do app.checkActiveTrack

			if !album.$el.hasClass 'disable'

				if album.$el.hasClass 'open'
					do album.content.empty
					album.$el.removeClass 'open'
					return

				album.$el.addClass 'disable'
				loader = new Loader.View
					styles:
						'margin': '20px auto'
				album.content.show loader

				if album.model.has 'tracks'
					openAlbum album,album.model.get 'tracks'
				else
					fetchTracks = album.model.fetch()

					fetchTracks.done ->
						openAlbum album, album.model.get 'tracks'

					fetchTracks.fail ->
						album.$el.removeClass('disable').addClass('open')
						callback = ->
							showAlbumContent album

						update = new Update.View
							styles:
								margin: '20px auto'
							callback: callback

						album.content.show update

	class Album.Options extends Marionette.ItemView
		template: '#album-item-options-tpl'
		className: 'options-wrapper'
		events:
			'mouseleave': 'closeOptions'
			'click .options-overlay': 'closeOptions'

		closeOptions: (e)->
			e.stopPropagation()
			do @destroy

	class Album.ListView extends Marionette.CollectionView
		childView: Album.Item
		className: 'albums-wrapper'
		onBeforeRenderCollection: ->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.append itemView.el
				.append ' '

	class Album.InlineListView extends Marionette.CollectionView
		childView: Album.InlineItem
		className: 'inline-albums-wrapper'
		onRender: ->
			if @collection.length isnt 0
				$('.artist-layout-title.albums-title').show()

	return Album