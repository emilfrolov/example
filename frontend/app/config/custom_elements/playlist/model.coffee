define ->
    Playlist = {}

    class Playlist.Model extends Backbone.Model
        urlRoot: ->
            app.getRequestUrl("/playlist/get?playlistid=#{@id}&items_on_page=1000")
        defaults:
          "duration": ""
          "artist": ""
          "title": ""

    class Playlist.Collection extends Backbone.Collection
        model: Playlist.Model

    return Playlist