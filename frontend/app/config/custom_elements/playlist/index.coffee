define ['./view.coffee','./model.coffee','../../../parts/player','../../../parts/notification'], (Playlist,Model,Player,Notification)->
	require('./style.styl')
	
	class controller extends Marionette.Controller
		Play: (playlistModel)->
			app.playedAlbum = 'playlist-'+playlistModel.get 'id'

			if playlistModel.has 'tracks'
				if playlistModel.get('tracks').length is 0
					Notification.Controller.show({message: 'Плейлист пустой',type: 'error',time: 3000})
					return false

				collection = new Backbone.Collection playlistModel.get 'tracks'
				collection.each (item)->
					item.set 'uniq', 'album'+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toArt
							class: 'options-open-artist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]
				Player.Controller.playByCollection collection
			else
				playlistTracks = do playlistModel.fetch

				playlistTracks.done ->
					collection = new Backbone.Collection playlistModel.get 'tracks'
					collection.each (item)->
						item.set 'uniq', 'album'+item.id
						item.set 'optionsList', [
							{
								title: app.locale.options.addTPl
								class: 'add-to-playlist'
							},{
								title: app.locale.options.toArt
								class: 'options-open-artist'
							},{
								title: app.locale.options.toAlb
								class: 'options-open-album'
							}
						]

					href = Backbone.history.location.origin+'/'+Backbone.history.location.pathname.split('/')[1]+'/playlist/'+playlistModel.get 'id'
					Player.Controller.playByCollection collection,null,href

				playlistTracks.fail (err)->
					console.log err


	Playlist.Controller = new controller

	_.extend Playlist, Model
	return Playlist