define ['../../../parts/player','../../../parts/simple_views/loader','../../../parts/simple_views/update','../track'], (Player,Loader,Update,Track)->
	Playlist = {}

	class Playlist.Item extends Marionette.ItemView
		id: ->
			return 'playlist-'+@model.get 'id'

		events:
			'click .player-controll': (e)->
				e.stopPropagation()
				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					Playlist.Controller.Play @model

			'click .controll': ->
				app.trigger 'show:playlist', @model.get('id'), @model

			'click .info .title': ->
#				app.trigger 'show:album', @model.get('id'), @model

			'click .info .artist': ->
#				app.trigger 'show:artist', @model.get 'artistid'


	class Playlist.InlineItem extends Marionette.LayoutView
		template: '#inline-playlist-tpl'
		className: 'playlist-item'
		regions:
			content: '.body .playlist-tracks'
		events:
			'click .body': (e)->
				e.isPropagationStopped()

			'click .shuffle-tracks': ->
				trackContainer = @$('.tracks-wrapper')
				tracks = trackContainer.find('.track-item').not('.disable')
				tracksLength = tracks.length

				random = app.RandInt(0,tracksLength-1)

				tracks.eq(random).click()

			'click': 'showContent'

		showContent: ->
			playlist = @

			openPlaylist = (playlist,tracks)->
				playlist.$el.removeClass('disable').addClass('open')

				collection = new Backbone.Collection tracks

				collection.each (item)->
					item.set 'uniq', playlist.model.get('id')+item.id
					item.set 'optionsList', [
						{
							title: app.locale.options.addTPl
							class: 'add-to-playlist'
						},{
							title: app.locale.options.toAlb
							class: 'options-open-album'
						}
					]

				tracksView = new Track.ListView
					collection: collection

				playlist.content.show tracksView
				do app.checkActiveTrack

			if !playlist.$el.hasClass 'disable'

				if playlist.$el.hasClass 'open'
					do playlist.content.empty
					playlist.$el.removeClass 'open'
					return

				playlist.$el.addClass 'disable'
				loader = new Loader.View
					styles:
						'margin': '20px auto'
				playlist.content.show loader

				if playlist.model.has 'tracks'
					openPlaylist playlist,playlist.model.get 'tracks'
				else
					fetchTracks = playlist.model.fetch()

					fetchTracks.done ->
						openPlaylist playlist, playlist.model.get 'tracks'

					fetchTracks.fail ->
						playlist.$el.removeClass('disable').addClass('open')
						callback = ->
							showAlbumContent playlist

						update = new Update.View
							styles:
								margin: '20px auto'
							callback: callback

						playlist.content.show update


	class Playlist.ListView extends Marionette.CollectionView
		childView: Playlist.Item


	class Playlist.InlineListView extends Marionette.CollectionView
		childView: Playlist.InlineItem
		className: 'inline-playlists-wrapper'

	return Playlist