define ->
    Artist = {}
    class Artist.Item extends Marionette.LayoutView
        events:
            'click': ->
                app.trigger 'show:artist', @model.get 'id'

    return Artist