define ['./view.coffee','./model.coffee','../../../parts/notification'], (Track,Model,Notification)->
	require('./style.styl')
	
	class controller extends Marionette.Controller

		openOptions: (item)->
			if item.model.has 'optionsList'
				if item.model.has 'playlistTrack'
					list = [
						{
							title: 'удалить из плейлиста'
							class: 'delete-from-playlist'
						}
					]
				else
					if item.model.get 'inFavorite'
						list = [
							{
								title: 'удалить из медиатеки'
								class: 'delete'
							}
						]
					else
						list = [
							{
								title: 'добавить в медиатеку'
								class: 'add'
							}
						]

				_.each item.model.get('optionsList'), (item)->
					list.push item

				collection = new Backbone.Collection list
			else
				collection = new Backbone.Collection

			if item.model.get 'artist_is_va'
				opts = collection.findWhere
					artist: true
				if opts
					opts.remove
						silent:true

			layout = new Track.OptionsLayout
				model: item.model
				collection: collection

			item.optionsRegion.show layout


		openPlaylistsList: (item)->

			playlist = app.request 'get:favorite:playlists'
			if !playlist.promise
				view = new Track.PlaylistItems
					model: item.model
					collection: playlist

				item.optionsRegion.show view

		selectText: (target)->
			if document.createRange
				rng = document.createRange();
				textNode = target.firstChild;

				rng.selectNode( target );

				rng.setStart( textNode, 0 );
				rng.setEnd( textNode, textNode.length );

				sel = window.getSelection();
				sel.removeAllRanges();
				sel.addRange( rng );


			else
				rng = document.body.createTextRange();
				rng.moveToElementText( target );
				rng.select();

		removeTrackFromFavorite: (id)->
			app.request 'get:favorite:tracks'
			.done (favorites)->
				track = favorites.findWhere
					id: id

				if track
					do track.destroy

					Notification.Controller.show({message: "Трек «#{track.get 'title' }» удален медиатеки",type: 'success',time: 3000})

		addTrackToFavorite: (track)->
			app.request 'get:favorite:tracks'
			.done (favorites)->
				model = track.toJSON()
				if !model.favourites or model.favourites.sort is undefined
					model.favourites = {}
					model.favourites.sort = 10000

				favorites.create model
				app.GAPageAction 'Добавление трека в избранное', track.get('title')+' - '+location.href
				Notification.Controller.show({message: "Трек «#{track.get 'title' }» добавлен медиатеку",type: 'success',time: 3000})

		createPlaylist: (title,track)->
			playlist = app.request 'get:favorite:playlists'
			playlistCreate = playlist.create {'title': title,tracks: [track.model.toJSON()]},{wait:true}
			$('.options-wrapper .back-to-options').hide()
			$('.options-wrapper .options-playlist-scroll-wrapp').hide()
			$('.options-wrapper').css('min-height', 300+'px').append('<div class="loader" style="top:130px"></div>');

			playlistCreate.once 'sync', (playlist)=>
				@addTrackToPlaylist playlist.id,track.model

		addTrackToPlaylist: (playlistId,track)->
			playlistId = +playlistId
			playlists = app.request 'get:favorite:playlists'

			playlist = playlists.findWhere
				id: playlistId

			window.test = playlist

			success = (data)->
				app.GAPageAction "Добавление трека в плейлист #{playlistId}", track.get('artist')+' - '+track.get('title')

				trackData = track.toJSON()
				trackData.playlist = data.playlist

				if playlist.has 'tracks'
					collection = _.union playlist.get('tracks'),[trackData]
					count = collection.length
					playlist.set
						tracks: collection
						items_count: count
				else
					playlist.set
						tracks: [trackData]
						items_count: 1

				$('.options-wrapper').remove()
				Notification.Controller.show({message: "Трек «#{track.get 'title' }» добавлен плейлист",type: 'success',time: 3000})

			if playlist
				$('.options-wrapper').css('min-height', 300+'px').empty().append('<div class="loader" style="top:130px"></div>');
				$.ajax
					type: "GET",
					url: app.getRequestUrlSid("/playlist/additem","?contentid=#{track.get('id')}&playlistid=#{playlistId}")
					success: success



		removeTrackFromPlaylist: (track,parent)->
			parentTrackModel = parent.options.parentTrack.model
			oldCollection = parentTrackModel.get 'tracks'
			newCollection = _.filter oldCollection, (obj)->
				return track.model.get('id') isnt obj.id

			parentTrackModel.set 'tracks', newCollection
			#			view.$el.addClass 'disable'
			#			view.$('.remove-from-fvrt').removeClass('remove-from-fvrt').addClass('add-to-fvrt')

			$.ajax
				type: "GET",
				url: app.getRequestUrlSid("/playlist/removeitem","?itemid=#{track.model.get('playlist').itemid}")

	Track.Controller = new controller

	_.extend Track, Model

	return Track