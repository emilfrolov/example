define [], ->
    Track = {}

    class Track.Model extends Backbone.Model
        urlRoot: '/favoritesTracks'
        defaults:
          "duration": ""
          "artist": ""
          "title": ""

    class Track.Collection extends Backbone.Collection
        model: Track.Model

    return Track