define ['../../../parts/player'], (Player)->
	Track = {}
	class Track.Item extends Marionette.LayoutView
		template: '#track-tpl'
		className: 'track-item'
		regions:
			optionsRegion: '.options-content'

		onShow: ->
			if @model.has 'is_deleted'
				@$el.addClass 'disable'
		id: ->
			return @model.get 'uniq'

		events:
			'click': ->
				if @model.has 'is_deleted'
					return

				if @$el.hasClass 'active'
					if @$el.hasClass 'play'
						do Player.Controller.play
					else
						do Player.Controller.pause
				else
					app.playedAlbum = null
					Player.Controller.playByCollection @model.collection,@model.get('id'),Backbone.history.location.href

			'mouseenter': ->
				favorites = app.request 'get:favorite:tracks'

				if favorites.promise
					favorites.done (collection)=>
						item = collection.some (item)=>
							return item.get('id') is @model.get('id')
						if item
							@model.set 'inFavorite', true
							@$('.track-controll').removeClass('add').addClass('delete')
						else
							@model.set 'inFavorite', false
							@$('.track-controll').removeClass('delete').addClass('add')

					favorites.fail (err)->
						console.log arguments

				else
					item = favorites.some (item)=>
						return item.get('id') is @model.get('id')
					if item
						@model.set 'inFavorite', true
						@$('.track-controll').removeClass('add').addClass('delete')
					else
						@model.set 'inFavorite', false
						@$('.track-controll').removeClass('delete').addClass('add')

			'click .playlist-track-controll': ->
				@trigger 'playlist:track:delete'
				return false

			'click .track-controll.add': ->
				Track.Controller.addTrackToFavorite @model
				@model.set 'inFavorite', true
				@$('.track-controll').removeClass('add').addClass('delete')
				app.trigger 'add:to:favorite'
				return false

			'click .track-controll.delete': ->
				Track.Controller.removeTrackFromFavorite @model.get 'id'
				@model.set 'inFavorite', false
				@$('.track-controll').removeClass('delete').addClass('add')
				app.trigger 'remove:from:favorite'
				return false

			'click .options-wrapper': ->
				return false

			'click .more-options': ->
				Track.Controller.openOptions @
				return false

			'click .add-to-playlist': ->
				Track.Controller.openPlaylistsList @
				return false

			'click .back-to-options': ->
				Track.Controller.openOptions @
				return false

			'click .options-playlist-item': (e)->
				Track.Controller.addTrackToPlaylist $(e.target).attr('data-id'),@model

			'click .options-open-album': (e)->
				e.stopPropagation()
				app.trigger 'show:album', @model.get('release_id')

			'click .options-open-artist': (e)->
				e.stopPropagation()
				app.trigger 'show:artist', @model.get('artistid')

			'keypress .new-playlist input': (e)->

				if e.keyCode is 13
					Track.Controller.createPlaylist $('.new-playlist input').val(),@

			'click .artist span': (e)->
				e.stopPropagation()
				app.trigger 'show:artist', @model.get('artistid')





	class Track.OptionsItem extends Marionette.ItemView
		template: '#track-options-list-item-tpl'
		className: 'track-options-list-item'
		tagName: 'li'
		onRender: ->
			@$el.addClass @model.get 'class'

	class Track.OptionsLayout extends Marionette.CompositeView
		template: '#options-list-tpl'
		className: 'options-wrapper'
		childView: Track.OptionsItem
		childViewContainer: '.options-list'
		onShow: ->
			target = @$('.textarea')[0]
			Track.Controller.selectText target

		events:
			'click': ->
				target = @$('.textarea')[0]
				Track.Controller.selectText target

			'click .delete': (e)->
				Track.Controller.removeTrackFromFavorite @model.get 'id'
				@model.set 'inFavorite', false, {silent: true}
				$(e.target).closest('.options').find('.track-controll').removeClass('delete').addClass('add')
				@$el.find('.delete').removeClass('delete').addClass('add').text('Добавить в медиатеку')
				app.trigger 'remove:from:favorite'
				return false

			'click .add': ->
				Track.Controller.addTrackToFavorite @model
				@model.set 'inFavorite', true, {silent: true}
				@$el.closest('.options').find('.track-controll').removeClass('add').addClass('delete')
				@$el.find('.add').removeClass('add').addClass('delete').text('Удалить из медиатеки')
				app.trigger 'add:to:favorite'
				return false

			'mouseleave': 'closeOptions'
			'click .options-overlay': 'closeOptions'

		closeOptions: (e)->
			e.stopPropagation()
			$('.options-wrapper').remove()

	class Track.PlaylistItem extends Marionette.ItemView
		template: '#options-playlist-item-tpl'
		className: 'options-playlist-item'
		tagname: 'li'
		onRender: ->
			@$el.attr 'data-id', @model.get 'id'

	class Track.PlaylistItems extends Marionette.CompositeView
		template: '#options-playlist-window-tpl'
		childView: Track.PlaylistItem
		childViewContainer: '.container'
		className: 'options-wrapper'
		events:
			'click .new-playlist input': ->
				$('.new-playlist input').select()
			'blur .new-playlist input': ->
				$('.new-playlist input').val '+ Новый плейлист'

		onShow: ->
			$('.options-wrapper .options-playlist-scroll-wrapp').customScrollbar();

	class Track.ListView extends Marionette.CollectionView
		childView: Track.Item
		className: 'tracks-wrapper'

	return Track