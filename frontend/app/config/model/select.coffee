Backbone.Model.Selected = Backbone.Model.extend({
	select: ()->
		if this.selected
			return

		this.selected = true;
		this.trigger("selected", this);

		if this.collection
			this.collection.select(this);


	deselect: ()->
		if !this.selected
			return

		this.selected = false;
		this.trigger("deselected", this);

		if this.collection
			this.collection.deselect(this);


	toggleSelected: ()->
		if this.selected
			this.deselect();
		else
			this.select();

})

Backbone.Collection.SingleSelect = Backbone.Collection.extend({
	select: (model)->
		if model && this.selected is model
			return;

		this.deselect();

		this.selected = model;
		this.selected.select();
		this.trigger("select:one", model);

	deselect: (model)->
		if !this.selected
			return

		model = model || this.selected;

		if this.selected isnt model
			return

		this.selected.deselect();
		this.trigger("deselect:one", this.selected);
		delete this.selected;

})

Backbone.Collection.MultiSelected = Backbone.Collection.extend({
	initialize: ->
		this.selected = {}

	select: (model)->
		if this.selected[model.cid]
			return
		this.selected[model.cid] = model;
		model.select();
		@calculateSelectedLength(this);

	deselect: (model)->
		if !this.selected[model.cid]
			return

		delete this.selected[model.cid];
		model.deselect();
		@calculateSelectedLength(this);


	selectAll: ()->
		this.each (model)->
			model.select();
		@calculateSelectedLength(this);


	selectNone: ()->
		if this.selectedLength is 0
			return;
		this.each (model)->
			model.deselect();

		@calculateSelectedLength(this);


	toggleSelectAll: ()->
		if this.selectedLength is this.length
			this.selectNone();
		else
			this.selectAll();


	calculateSelectedLength: (collection)->
		collection.selectedLength = _.size(collection.selected);

		selectedLength = collection.selectedLength;
		length = collection.length;

		if selectedLength is length
			collection.trigger("select:all", collection);
			return;

		if selectedLength is 0
			collection.trigger("select:none", collection);
			return;

		if selectedLength > 0 && selectedLength < length
			collection.trigger("select:some", collection);
			return;

})
