_.extend Backbone.Model.prototype, 
    url: ->
      base =
        _.result(this, 'urlRoot') ||
        _.result(this.collection, 'url') ||
        urlError();

      id = this.get(this.idAttribute);
      return base;

    sync: (method,model,options)->
      if method is 'update'
        if model.updateUrl
          model.url = model.updateUrl
        else
          throw new Error("A 'updateUrl' property or function must be specified");

      if method is 'delete'
        if model.deleteUrl
          model.url = model.deleteUrl
        else
          throw new Error("A 'deleteUrl' property or function must be specified");

      if method is 'create'
        if model.createUrl
          model.url = model.createUrl
        else
          throw new Error("A 'createUrl' property or function must be specified");

      method = 'read'
        

      Backbone.sync.apply this,[method,model,options]

