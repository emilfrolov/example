do ->
  sync = Backbone.sync
  Backbone.sync = (method,model,options)->
    data = options.data or {}
    data.sid = app.user.sid
    _.extend options,
        data: data
        
    sync.apply this, [method,model,options]