class Marionette.AppRouter extends Marionette.AppRouter
	initialize: ()->
		routes = @appRoutes
		newRoutes = {}
		BaseUrl = '(:lang)'
		for key,val of routes
			if key is ''
				newRoutes[BaseUrl] = val
				newRoutes[BaseUrl+'/'] = val
			else
				newRoutes[BaseUrl+'/'+key] = val
				newRoutes[BaseUrl+'/'+key+'/'] = val

		@appRoutes = newRoutes
