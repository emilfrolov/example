define [], ->
    require('../../entities/log.coffee')
    
    class controller extends Marionette.Controller
        initialize: ->
            @errCount = 0

        sendNewLog: ->
            console.debug '.....................'
            console.debug 'Start new log request'
            self = @
            logs = app.request('get:logs')

            if logs.length
                log = logs.at(0)
                offset = (new Date().getTime() - log.get('create'))/1000
                if offset > 10800
                    console.debug 'Destroy old log', log.toJSON()
                    log.destroy()
                    do self.sendNewLog
                    return

                req = $.get log.get('url'), {timeOffset: offset}
                console.debug 'Log request send! Url: '+log.get('url')
                req.done (res)->
                    if res.code is 0
                        console.debug 'Log request send success'
                        self.errCount = 0
                        log.destroy()
                        do self.sendNewLog
                    else
                        console.debug 'Log request send error', res
                        self.errCount++
                        do self.sendErr

                req.fail (err)->
                    console.debug 'Log request send error',err
                    self.errCount++
                    do self.sendErr
            else
                console.debug 'No log to send! Wait for new logs'

                @listenTo logs, 'add', ->
                    console.debug 'New log create! Stop listening log storage'
                    @stopListening()
                    do self.sendNewLog

        sendErr: ->
            console.debug 'Log request error! Count: '+@errCount
            self = @
            if @errCount is 1
                delay = 1
            else if 10 > @errCount > 1
                delay = 10000
            else if @errCount > 10
                delay = 300000

            setTimeout ->
                do self.sendNewLog
            ,delay
                
            

        createNewLog: (url)->
            logs = app.request('get:logs');
        
            newLog = logs.create
                url: url
                create: new Date().getTime()
                uniq: app.RandInt 100,200000000

            if newLog.validationError
                console.debug newLog.validationError
                logs.remove newLog
            else
                console.debug.apply console, ['Log create', newLog.get('id')]

    return new controller