app.module 'Parts.UserNotification', (Notification,app,Backbone,Marionette,$,_)->
	class controller extends Marionette.Controller
		showWindow: (view)->
			@layout = new Notification.View

			view.show @layout

		showSubs: ->
			notify = app.request('get:user:notification')

			view = new Notification.SubsView
				collection: notify

			@layout.content.show view

		showMessage: ->

		pushNewNotification: (message)->
			notify = app.request('get:user:notification')
			if message.a.firstPersonId?
				user = app.request 'get:user', message.a.firstPersonId
				user.done (user)->
					message['user'] = user.toJSON()
					message['new'] = true
					message['sort'] = notify.length+1
					model = new Backbone.Model message
					notify.create(model)
					app.trigger('notify:create');

			else
				message['new'] = true
				message['sort'] = notify.length+1
				model = new Backbone.Model message
				notify.create(model)
				app.trigger('notify:create');

		startListenNotify: ->
			# if app.user.sid
			# 	app.socketConn = io.connect("http://localhost:8763?userSid=#{app.user.sid}");
			# 	app.socketConn.on 'connect', ->
			# 		console.log('connect')

			# 	app.socketConn.on 'closeConn', ->
			# 		app.socketConn.close()

			# 	app.socketConn.on 'stopPlay', (data)->
			# 		player = app.Parts.Player.Controller
			# 		if player.played
			# 			player.pause();
			# 			app.Parts.Notification.Controller.show({message: 'Кто-то слушает музыку на другом устройстве',type: 'error',time: 3000})

			# 	chat.on 'broadcastEvent', (data)->
			# 		console.log(data)
			# self = @
			# connectionCount = 0

			# connect = ->
			# 	connectionCount++
			# 	vars = {}

			# 	user = app.request('get:user')
			# 	user.done (user)->
			# 		console.log 'connect...'
			# 		webSocket = new WebSocket('ws://cdn.music.beeline.ru:8765/?sid='+user.get('sid'));
			# 		webSocket.onopen = ->
			# 			console.log 'open'
			# 			vars.timeout = setTimeout ->
			# 				connectionCount = 0
			# 			,100

			# 		webSocket.onmessage = (event)->
			# 			try
			# 				data = JSON.parse(event.data);
			# 				self.pushNewNotification data
			# 			catch e
			# 				console.log e

			# 		webSocket.onclose = (event)->
			# 			console.log 'close'
			# 			clearTimeout vars.timeout

			# 			if connectionCount < 5
			# 				timer = 500
			# 			else if 5 < connectionCount < 10
			# 				timer = 1000
			# 			else if connectionCount > 10
			# 				timer = 5000

			# 			setTimeout ->
			# 				do connect
			# 			,timer

			# do connect

		remove: ->
			if @layout
				@layout.destroy()

	Notification.Controller = new controller