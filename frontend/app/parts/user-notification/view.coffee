app.module 'Parts.UserNotification', (Notification,app,Backbone,Marionette,$,_)->
	class Notification.View extends Marionette.LayoutView
		template: '#user-notify-tpl'
		className: 'user-notify-wrapp'
		regions:
			'content': '.content'
		onShow: ->
			@$('.subs').click()
		events:
			'click': (e)->
				e.stopPropagation()

			'click .close': (e)->
				e.stopPropagation()
				do @destroy

			'click .subs': (e)->
				if not $(e.target).hasClass('active')
					@$('.tabs div').removeClass 'active'
					$(e.target).addClass 'active'
					do Notification.Controller.showSubs

			'click .message': (e)->
				if not $(e.target).hasClass('active')
					@$('.tabs div').removeClass 'active'
					$(e.target).addClass 'active'
					do Notification.Controller.showMessage

	class Notification.SubsItem extends Marionette.ItemView
		template: '#user-notify-subs-item-tpl'
		className: ->
			return if @model.get('new') is true then 'new user-notify-subs-item' else 'user-notify-subs-item'
		modelEvents:
			'change': ->
				do @render
		events:
			'mouseenter': ->
				if @model.get('new') is true
					@model.set 'new', false
			'click .title span': ->
				app.trigger 'show:user:page', @model.get('user').user_id

	class Notification.SubsView extends Marionette.CollectionView
		childView: Notification.SubsItem
		filter: (child, index, collection)->
			return child.get('n') is 'userSubscribed'
		viewComparator: (item)->
			return -item.get('sort')

		className: 'default-skin content-wrapp'
		onShow: ->
			@$el.customScrollbar();
		collectionEvents:
			'change': ->
				@$('.scroll-bar').remove()
				@$('.viewport').remove()
#				@$el.removeClass 'scrollable'
				do @render
				@$el.customScrollbar()
