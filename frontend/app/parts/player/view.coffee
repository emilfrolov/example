define ->
	Player = {}

	class Player.View extends Marionette.ItemView
		template: (data)->
			data.img = app.createImgLink(data.img,'50x50')
			template = require('./templates/main-tpl.jade')
			return template(data)

		className: 'player-container'
		onShow: ->
			@$('.player-wrapp').fadeIn 300

		onRender: ->
			options = new Marionette.Region
				el: @$('.options-region')
			@optionsRegion = options

		modelEvents: ->
			'change': ->
				do @render
				do Player.Controller.setDefaultVolume
				@$('.player-wrapp').show()

		events:
			'click .player-timer-display': ->
				do @$('.time-front').toggle
				do @$('.time-end').toggle

			'click .fwd-arrow': ->
				do Player.Controller.nextTrck

			'click .back-arrow': ->
				do Player.Controller.prevTrck

			'click .play': ->
				do Player.Controller.play

			'click .pause': ->
				do Player.Controller.pause

			'click .shuffle': ->
				do Player.Controller.toggleShuffle

			'click .options-region': ->
				Player.Controller.openOptions @

			'click .artistName': ->
				app.trigger 'show:artist',@model.get('artistid')

			'click .soundName': ->
				if !@model.get('artist_is_va')
					app.trigger 'show:album',@model.get('release_id')




			#options events

			'click .track-controll.add': ->
				Player.Controller.addTrackToFavorite @model
				@model.set 'inFavorite', true, {silent: true}
				@$('.track-controll').removeClass('add').addClass('delete')
				return false

			'click .track-controll.delete': ->
				Player.Controller.removeTrackFromFavorite @model.get 'id'
				@model.set 'inFavorite', false, {silent: true}
				@$('.track-controll').removeClass('delete').addClass('add')
				return false

			'click .options-wrapper': ->
				return false

			'click .add-to-playlist': ->
				Player.Controller.openPlaylistsList @
				return false

			'click .back-to-options': ->
				Player.Controller.openOptions @
				return false

			'click .options-playlist-item': (e)->
				Player.Controller.addTrackToPlaylist $(e.target).attr('data-id'),@model

			'click .options-open-album': ->
				app.trigger 'show:album', @model.get('release_id')
				do @closeOptions
				return false

			'click .options-open-artist': ->
				app.trigger 'show:artist', @model.get('artistid')
				do @closeOptions
				return false

			'keypress .new-playlist input': (e)->

				if e.keyCode is 13
					Player.Controller.createFavouritePlaylist $('.new-playlist input').val(),@

			'mouseleave': 'closeOptions'
			'click .options-overlay': 'closeOptions'

		closeOptions: ->
			$('.options-wrapper').remove()

	class Player.OptionsItem extends Marionette.ItemView
		template: '#track-options-list-item-tpl'
		className: 'track-options-list-item'
		tagName: 'li'
		onRender: ->
			@$el.addClass @model.get 'class'

	class Player.OptionsLayout extends Marionette.CompositeView
		template: '#options-list-tpl'
		className: 'options-wrapper'
		childView: Player.OptionsItem
		childViewContainer: '.options-list'
		onShow: ->
			@$('.textarea').select()

		events:
			'click': ->
				@$('.textarea').select()

			'click .delete': (e)->
				Player.Controller.removeTrackFromFavorite @model.get 'id'
				@model.set 'inFavorite', false, {silent: true}
				$(e.target).closest('.options').find('.track-controll').removeClass('delete').addClass('add')
				@$el.find('.delete').removeClass('delete').addClass('add').text('Добавить в медиатеку')
				app.trigger 'remove:from:favorite'
				return false

			'click .add': ->
				Player.Controller.addTrackToFavorite @model
				@model.set 'inFavorite', true, {silent: true}
				@$el.closest('.options').find('.track-controll').removeClass('add').addClass('delete')
				@$el.find('.add').removeClass('add').addClass('delete').text('Удалить из медиатеки')
				app.trigger 'add:to:favorite'
				return false

			'mouseleave': 'closeOptions'
			'click .options-overlay': 'closeOptions'

		closeOptions: (e)->
			e.stopPropagation()
			$('.options-wrapper').remove()

	class Player.PlaylistItem extends Marionette.ItemView
		template: '#options-playlist-item-tpl'
		className: 'options-playlist-item'
		tagname: 'li'
		onRender: ->
			@$el.attr 'data-id', @model.get 'id'

	class Player.PlaylistItems extends Marionette.CompositeView
		template: '#options-playlist-window-tpl'
		childView: Player.PlaylistItem
		childViewContainer: '.container'
		className: 'options-wrapper'
		events:
			'click .new-playlist input': ->
				$('.new-playlist input').select()
			'blur .new-playlist input': ->
				$('.new-playlist input').val '+ Новый плейлист'

		onShow: ->
			$('.options-wrapper .options-playlist-scroll-wrapp').customScrollbar();

	return Player


