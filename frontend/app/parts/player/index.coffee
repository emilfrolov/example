define ['./view.coffee', '../../config/log','../notification'], (Player,Logger,Notification)->
	require('./style.styl')

	class controller extends Marionette.Controller
		initialize: ->
			@my_jPlayer = $("#bee-player");
			@svf_path = '/packages/jplayer_new/jquery.jplayer.swf';
			$.jPlayer.timeFormat.padMin = false;
			$.jPlayer.timeFormat.padSec = false;
			$.jPlayer.timeFormat.sepMin = " min ";
			$.jPlayer.timeFormat.sepSec = " sec";
			@firstPlay = true
			@volume = if sessionStorage.volume then sessionStorage.volume else 0.5

			@repeat = if sessionStorage.repeat then sessionStorage.repeat else false

			@shuffle = if sessionStorage.shuffle then sessionStorage.shuffle else 'off'

			do @initializePlayer

			@listenTo app, 'add:to:favorite', ->
				if @playerView
					@playerView.model.set 'inFavorite', true, {silent: true}
					$('#player .track-controll').removeClass('add').addClass('delete')

			@listenTo app, 'remove:from:favorite', ->
				if @playerView
					@playerView.model.set 'inFavorite', false, {silent: true}
					$('#player .track-controll').addClass('add').removeClass('delete')



		initializePlayer: ->
			self = @;

			@my_jPlayer.jPlayer

				loadedmetadata: (event)->
					self.trackDuration = event.jPlayer.status.duration

				play: ->
					$('.player-controll-element').removeClass 'play'
						.addClass 'pause'
					app.trigger 'play'
					self.playerClassStatus = 'pause'

				pause: ->
					$('.player-controll-element').removeClass 'pause'
						.addClass 'play'
					app.trigger 'pause'
					self.playerClassStatus = 'play'

				timeupdate: (event)->
					if self.dragIsActive isnt true
						if parseInt(event.jPlayer.status.currentPercentAbsolute) is 50
							Logger.createNewLog app.getRequestUrlSid("/generalV1/log/streamingHalf","?id=#{self.playedTrack}&referer=#{app.getReferer()}")
						$('.time-line .active-line').css 'width', event.jPlayer.status.currentPercentAbsolute+'%';
						if parseInt(event.jPlayer.status.currentTime) is 29
							Logger.createNewLog app.getRequestUrlSid("/generalV1/log/streaming30Seconds","?id=#{self.playedTrack}&referer=#{app.getReferer()}")
						self._setDuration event.jPlayer.status.duration,event.jPlayer.status.currentTime

				ended: ->
					Logger.createNewLog app.getRequestUrlSid("/generalV1/log/streamingFull","?id=#{self.playedTrack}&referer=#{app.getReferer()}")
					self.nextTrck();

				swfPath: self.svf_path
				supplied: "mp3"

		startPlayerAnimation: ->
			if @firstPlay
				console.log $('.pseudo-player')
				$('.pseudo-player').fadeOut 300, ->
					$(@).remove()

				@firstPlay = false

		streamById: (id)->
			track = @_loadTrack id

			track.done (data)=>
				if data.code? and data.code is 0
					Logger.createNewLog app.getRequestUrlSid("/generalV1/log/streamingStart","?id=#{@.playedTrack}&referer=#{app.getReferer()}")
					@play data.url
				else
					Notification.Controller.show({message: 'Ошибка воспроизведения',type: 'error',time: 3000})

			track.fail ->
				Notification.Controller.show({message: 'Ошибка воспроизведения',type: 'error',time: 3000})


		playTrack: (id,data,href)->
			do @startPlayerAnimation
			if data and href
				app.GAPageAction 'played track', data.title+" - "+href

			do @stop
			if href
				app.playedHref = href

			@playedTrack = id
			if data
				app.playedId = data.uniq
				@streamById id
				@showPlayerData data
			else
				trackData = @_loadTrackData id
				trackData.done (data)=>
					if data.code? and data.code is 0
						@streamById id
						@showPlayerData data.tracks[0]
					else
						console.log 'error'



		playByCollection: (collection,id,href)->
			normalizeCollection = collection.reject (item)->
				return item.get 'is_deleted'
			if !id
				length = normalizeCollection.length-1

				if length is 1
					id = normalizeCollection[1].get 'id'
				else
					middleOfCollection = app.RandInt 1,length-1
					id = normalizeCollection[middleOfCollection].get 'id'

			@playlist = @createPlaylist collection
			@playTrack id,@playlist[id].toJSON(),href

		showPlayerData: (data)->
			delete data.playlistTrack

			if !data.inFavorite
				favorites = app.request 'get:favorite:tracks'

				if !favorites.promise
					item = favorites.some (item)=>
						return item.get('id') is data.id
					if item
						data.inFavorite = true
					else
						data.inFavorite = false
				else
					data.inFavorite = false

			if !@playerView?
				@showPlayer data

			else
				@playerView.model.set data



		showPlayer: (data)->
			model = new Backbone.Model data

			@playerView = new Player.View
				model: model

			app.player.show @playerView

			dragPlayer = new SliderDrag '.time-line .active-zone', 'sound', @, '.time-line .active-line'
			dragPlayer.ready()

			dragSound = new SliderDrag '.volume .line .active', 'volume', @, '.volume .line .value'
			dragSound.ready()

			do @setDefaultVolume
			do @setShuffle

		setDefaultVolume: ->
			volume = +@volume
			@_setVolume(volume);
			$('.volume .line .value').width (volume*100)+'%'




	#Вспомогательные методы для плеера

	#Запрос стриминга по Id
		#Возвращает промис

		_loadTrack: (id)->
			if @trackLoad
				@trackLoad.abort()

			@trackLoad = $.get app.getRequestUrlSid("/compatibility/stream/gethttp","?itemid=#{id}")

			deferred = $.Deferred()

			@trackLoad.done (data)->
				deferred.resolveWith @, [data]

			@trackLoad.fail ->
				deferred.rejectWith @, arguments

			return deferred.promise()

	#Запрос информации о треке по Id
		#Возвращает промис
		_loadTrackData: (id)->
			if @trackDataLoad
				@trackDataLoad.abort()

			@trackDataLoad = $.get '/getTrackData/'+id

			deferred = $.Deferred()

			@trackDataLoad.done (data)->
				deferred.resolveWith @, [data]

			@trackDataLoad.fail ->
				deferred.rejectWith @, arguments

			return deferred.promise()

		_setDuration: (duration,currentTime)->
			time = @_convertTrackDataToTime duration, currentTime
			$('.player-timer-display .time-front').text(time.normal);
			$('.player-timer-display .time-end').text(time.reverse);

		_convertTrackDataToTime: (duration,currentTime)->
			endDuration = duration - currentTime;
			return {
			normal: Math.floor(currentTime/60)+':'+(if Math.floor(currentTime%60) < 10 then '0' else '')+Math.floor(currentTime % 60)
			reverse: '- '+Math.floor(endDuration/60)+':'+(if Math.floor(endDuration%60) < 10 then '0' else '')+Math.floor(endDuration % 60)
			}


		play: (url)->
			@played = true
			if url
				@my_jPlayer.jPlayer "setMedia",
					mp3: url

			@my_jPlayer.jPlayer 'play'

		seek: (time)->
			@my_jPlayer.jPlayer 'play', time

		stop: ->
			@my_jPlayer.jPlayer 'stop'

		pause: ->
			@played = false
			@my_jPlayer.jPlayer 'pause'

		nextTrck: ->
			if @playlist

				if @shuffle is 'on'
					track = @playlist[@playlist[@playedTrack]['nextShuffleTrck']]
				else
					track = @playlist[@playlist[@playedTrack]['nextTrck']]


				if track?
					@playTrack track.id, track.toJSON()

		prevTrck: ->
			if @playlist

				if @shuffle is 'on'
					track = @playlist[@playedTrack]['prevShuffleTrck']
				else
					if @playlist[@playedTrack]['prewTrck']
						track = @playlist[@playlist[@playedTrack]['prewTrck']]
					else
						track = @playlist[@playlist[@playedTrack]['lastTrack']]

				if track?
					@playTrack track.id, track.toJSON()

		_setVolume: (volume)->
			v = +volume;
			if v > 1
				v = 1
			sessionStorage.volume = v
			@volume = v
			@my_jPlayer.jPlayer "volume", v


		openOptions: (item)->
			if item.model.has 'optionsList'
				if item.model.has 'playlistTrack'
					list = [
						{
							title: 'удалить из плейлиста'
							class: 'delete-from-playlist'
						}
					]
				else
					if item.model.get 'inFavorite'
						list = [
							{
								title: 'удалить из медиатеки'
								class: 'delete'
							}
						]
					else
						list = [
							{
								title: 'добавить в медиатеку'
								class: 'add'
							}
						]

				_.each item.model.get('optionsList'), (item)->
					list.push item

				collection = new Backbone.Collection list
			else
				collection = new Backbone.Collection

			if item.model.get 'artist_is_va'
				opts = collection.findWhere
					artist: true
				if opts
					opts.remove
						silent:true

			layout = new Player.OptionsLayout
				model: item.model
				collection: collection

			item.optionsRegion.show layout


		openPlaylistsList: (item)->

			playlist = app.request 'get:favorite:playlists'
			if !playlist.promise
				view = new Player.PlaylistItems
					model: item.model
					collection: playlist

				item.optionsRegion.show view

		removeTrackFromFavorite: (id)->
			app.request 'get:favorite:tracks'
			.done (favorites)->
				track = favorites.findWhere
					id: id

				if track
					do track.destroy

					Notification.Controller.show({message: "Трек «#{track.get 'title' }» удален из избранного",type: 'success',time: 3000})

		addTrackToFavorite: (track)->
			app.request 'get:favorite:tracks'
			.done (favorites)->
				model = track.toJSON()
				if !model.favourites or model.favourites.sort is undefined
					model.favourites = {}
					model.favourites.sort = 10000

				favorites.create model
				app.GAPageAction 'Добавление трека в избранное', track.get('title')+' - '+location.href
				Notification.Controller.show({message: "Трек «#{track.get 'title' }» добавлен в медиатеку",type: 'success',time: 3000})

		createFavouritePlaylist: (title,track)->
			playlist = app.request 'get:favorite:playlists'
			playlistCreate = playlist.create {'title': title,tracks: [track.model.toJSON()]},{wait:true}
			$('.options-wrapper .back-to-options').hide()
			$('.options-wrapper .options-playlist-scroll-wrapp').hide()
			$('.options-wrapper').css('min-height', 300+'px').append('<div class="loader" style="top:130px"></div>');

			playlistCreate.once 'sync', (playlist)=>
				@addTrackToPlaylist playlist.id,track.model

		addTrackToPlaylist: (playlistId,track)->
			playlistId = +playlistId
			playlists = app.request 'get:favorite:playlists'

			playlist = playlists.findWhere
				id: playlistId

			window.test = playlist

			success = (data)->
				app.GAPageAction "Добавление трека в плейлист #{playlistId}", track.get('artist')+' - '+track.get('title')
				successWindow = '<div class="options-overlay"></div><div class="success-icon"></div><p class="success-title">Готово</p>'

				trackData = track.toJSON()
				trackData.playlist = data.playlist

				if playlist.has 'tracks'
					collection = _.union playlist.get('tracks'),[trackData]
					count = collection.length
					playlist.set
						tracks: collection
						items_count: count
				else
					playlist.set
						tracks: [trackData]
						items_count: 1

				$('.options-wrapper').empty().append(successWindow);


				setTimeout ->
					$('.options-wrapper').fadeOut 300, ->
						$(this).remove()
				,1000

			if playlist
				$('.options-wrapper').css('min-height', 300+'px').empty().append('<div class="loader" style="top:130px"></div>');
				$.ajax
					type: "GET",
					url: app.getRequestUrlSid("/playlist/additem","?contentid=#{track.get('id')}&playlistid=#{playlistId}")
					success: success

	#Вспомогательные методы для портала

		#Создание плейлиста из коллекции для последующей ее передачи в плеер
		createPlaylist: (collection)->
			normal = collection.reject (item)->
				return item.get 'is_deleted'


			shuffleId = _.shuffle _.pluck normal,'id'
			_.each normal, (item,index)->

				item.nextTrck = if normal[index+1]? then normal[index+1]['id'] else null
				item.prewTrck = if index isnt 0 then normal[index-1]['id'] else null
				item.nextShuffleTrck = if shuffleId[index]? then shuffleId[index] else normal[0].id
				item.prevShuffleTrck = if shuffleId[index-1]? then shuffleId[index-1] else normal[normal.length-1].id
				item.firstTrack = normal[0].id
				item.lastTrack = normal[normal.length-1].id

			return _.indexBy normal, 'id'



		toggleShuffle: ->
			if @shuffle is 'on'
				@shuffle = sessionStorage.shuffle = 'off'
				$('.main-player .other-buttons .shuffle').removeClass 'active'
			else
				@shuffle = sessionStorage.shuffle = 'on'
				$('.main-player .other-buttons .shuffle').addClass 'active'

		setShuffle: ->
			if @shuffle is 'on'
				$('.main-player .other-buttons .shuffle').addClass 'active'
			else
				$('.main-player .other-buttons .shuffle').removeClass 'active'









		class SliderDrag
			constructor: (@element,@type,@context,@active)->

			ready: ->
				$(document).on 'mousedown', @element, (e)=>
					@down(e)

			down: (e)->
				elToLeft = $(@element).offset().left;
				pageToLeft = e.pageX;
				widthVal = pageToLeft - elToLeft;

				$(@active).width(widthVal+'px');

				if @type is 'sound'
					@context.dragIsActive = true

				$(document).on "mousemove.drager", (e)=>
					@move(e);

				$(document).on "mouseup.drager", (e)=>
					elToLeft = $(@element).offset().left;
					pageToLeft = e.pageX;
					widthVal = pageToLeft - elToLeft;
					if @type is 'sound'
						@context.dragIsActive = false

						parcentWidth = (widthVal * 100) / $(@element).width()
						soundSeek = (@context.trackDuration * parcentWidth) / 100

						@context.seek soundSeek

					if @type is 'volume'
						@context._setVolume widthVal/$(@element).width()

					$(document).off ".drager"

			move: (e)->
				e.preventDefault();
				elToLeft = $(@element).offset().left;
				pageToLeft = e.pageX;
				widthVal = pageToLeft - elToLeft;
				parentWidth = $(@element).width();
				if widthVal <= 0
					widthVal = 0;
				if parentWidth <= widthVal
					return false;

				$(@active).width(widthVal+'px');

				if @type is 'volume'
					@context._setVolume widthVal/$(@element).width()

				if @type is 'sound'
					@soundMove e


				return false;

			soundMove: (e)->
				e.preventDefault();
				elToLeft = $(@element).offset().left;
				pageToLeft = e.pageX;
				widthVal = pageToLeft - elToLeft;
				parentWidth = $(@element).width();
				if widthVal <= 0
					widthVal = 0;
				if parentWidth <= widthVal
					return false;

				parcentWidth = (widthVal * 100) / parentWidth
				soundSeek = (@context.trackDuration * parcentWidth) / 100;

				if @context.trackDuration?
					time = @context._convertTrackDataToTime widthVal,soundSeek
				else
					time =
						normal: '--:--'
						reverse: '--:--'

				$('.player-timer-display .time-front').text(time.normal);
				$('.player-timer-display .time-end').text(time.reverse);


	Player.Controller = new controller
	return Player