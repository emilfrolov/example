define ->
    Prompt = {}

    class Prompt.View extends Marionette.ItemView
        template: (data)->
            return require('./templates/main-view.jade')(data);

        className: 'prompt-overlay'

        triggers:
            'click .submit-prompt': 'complete:prompt'
        events:
            'keyup input': (e)->
                val = $(e.target).val()

                if val.length
                    @$('.submit-prompt').show()
                else
                    @$('.submit-prompt').hide()
                if e.keyCode is 13
                    @trigger 'complete:prompt'

            'click .close': ->
                @destroy()

            'click': ->
                @destroy()

            'click .prompt-window': (e)->
                e.stopPropagation()
        onShow: ->
            @$('input').focus()

    return Prompt