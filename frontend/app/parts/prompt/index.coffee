define ['./view.coffee'], (Prompt)->
    require './style.styl'
    class controller extends Marionette.Controller
        show: (params,callback)->
            model = new Backbone.Model params
            view = new Prompt.View
                model: model

            @listenTo view, 'complete:prompt', ->
                val = view.$('input').val()

                if val.length
                    view.destroy()
                    if callback
                        callback val

            app.popups.show view

    Prompt.Controller = new controller

    return Prompt