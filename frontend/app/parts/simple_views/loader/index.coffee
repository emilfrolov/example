define ->
	require('./style.styl')
	Loader = {}

	class Loader.View extends Marionette.ItemView
		template: false
		className: ->
			return if @options.container then @options.container else 'loader'

		onRender: ->
			@options.styles = @options.styles or {}
			if @options.container
				container = $('<div class="loader"></div>')
				@$el.append container

			if @options.container
				@$('.loader').css @options.styles
			else
				@$el.css @options.styles

	return Loader