define ->
	require('./style.styl');
	Update = {}

	class Update.View extends Marionette.ItemView
		template: false
		className: ->
			return if @options.container then @options.container else 'update-element'

		onRender: ->
			@options.styles = @options.styles or {}
			if @options.container
				container = $('<div class="update-element"></div>')
				@$el.append container

			if @options.container
				@$('.update-element').css @options.styles
			else
				@$el.css @options.styles


		events:
			'click': ->
				do @options.callback

	return Update