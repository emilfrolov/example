define ['./view.coffee'], (Player)->
    require('./style.styl')
    class controller extends Marionette.Controller
        openVideo: (model)->
            view = new Player.Item
                model: model

            app.video.show view

    Player.Controller = new controller

    return Player


