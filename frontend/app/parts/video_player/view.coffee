define ['../player'], (Player)->
	Player = {}

	class Player.Item extends Marionette.ItemView
		template: '#video-frame-tpl'
		className: 'video-overlay'
		onShow: ->
			if Player.Controller.played
				do Player.Controller.pause
				Player.needStartPlay = true
			else
				Player.needStartPlay = false

		onDestroy: ->
			if Player.needStartPlay
				do Player.Controller.play

		events:
			'click': ->
				do @destroy

	return Player