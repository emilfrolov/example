define ['../../config/custom_elements/track','../../config/custom_elements/album','../../config/custom_elements/artist/','../../config/custom_elements/playlist','../player'] , (Track,Album,Artist,Playlist,Player)->
	Sidebar = {}

	class Sidebar.Layout extends Marionette.LayoutView
		template: ->
			return require('./templayts/layout-template.jade')()
		className: 'sidebar-wrapp'
		regions:
			'user': '.profile'
			'mainMenu': '.main-nav'
			'userMenu': '.user-nav'
			'searchResult': '.sidebar-search-container'

		onShow: ->
			sidebarScrollContent = $('.sidebar-content-scroll')
			sidebarScrollContent.height $(window).height()-116
			$(window).on 'resize.userTracks', ->
				sidebarScrollContent.height $(window).height()-116
			sidebarScrollContent.customScrollbar({
				updateOnWindowResize: true
			});

		events:
			'keyup .search input': ->
				val = @$('.search input').val()

				if $.trim(val) is ''
					$('.sidebar-body .search').removeClass 'active'

				else
					$('.sidebar-body .search').addClass 'active'

				if @searchTimer
					clearTimeout @searchTimer

				@searchTimer = setTimeout =>
					Sidebar.Controller.showSearchResult val
				,300

			'click .close': ->
				@$('.search input').val ''
					.keyup()

	class Sidebar.User extends Marionette.LayoutView
		template: '#sidebar-user-tpl'
		regions:
			'notify': '.notify-region'

		events:
			'click .exit': (e)->
				e.stopPropagation()
				$.get '/exit', ->
					location.href = ''

			'click': ->
				app.trigger 'show:profile'

		modelEvents:
			'change': ->
				do @render

	class Sidebar.SideMenuMainItem extends Marionette.ItemView
		template: '#sidebar-menu-item-tpl'
		className: 'sidebar-menu-item'
		tagName: 'li'
		events:
			'click': ->
				app.trigger @model.get 'trigger'
				return false

		onRender: ->
			@$el.addClass @model.get 'class'
			href = app.getCurrentRoute().split('/')[1] or ''
			route = @model.get 'href'

			if route is href
				@$el.addClass 'active'
				Sidebar.urlIsFind = true



	class Sidebar.SideMenuMainItems extends Marionette.CollectionView
		childView: Sidebar.SideMenuMainItem
		tagName: 'ul'

	class Sidebar.SideMenuUserItem extends Marionette.ItemView
		template: '#sidebar-menu-item-tpl'
		className: 'sidebar-menu-item'
		tagName: 'li'
		events:
			'click': ->
				app.trigger @model.get 'trigger'
				return false
		onRender: ->
			@$el.addClass @model.get 'class'
			currentRoute = app.getCurrentRoute().split('/')
			href = currentRoute[1]+'/'+currentRoute[2]
			route = @model.get 'href'

			if route is href
				@$el.addClass 'active'
				Sidebar.urlIsFind = true


	class Sidebar.SideMenuUserItems extends Marionette.CompositeView
		template: '#sidebar-menu-user-tpl'
		childView: Sidebar.SideMenuUserItem
		childViewContainer: '.user-menu'

	class Sidebar.SearchLayout extends Marionette.LayoutView
		template: '#sidebar-search-layout-tpl'
		className: 'scrollable'
		regions:
			tracks: '.tracks-wrapp'
			albums: '.albums-wrapp'
			artists: '.artists-wrapp'

		onShow: ->
			$('.sidebar-content-wrapp').hide()
			$('.sidebar-content-scroll').customScrollbar('resize',true)

		onDestroy: ->
			$('.sidebar-content-wrapp').show()

	class Sidebar.SearchTrack extends Marionette.ItemView
		template: '#sidebar-search-track-tpl'
		className: 'search-track'
		events:
			'click': ->
				Player.Controller.playTrack @model.get('id'),@model.toJSON()

	class Sidebar.SearchArtist extends Marionette.ItemView
		template: '#sidebar-search-artist-tpl'
		className: 'search-artist'
		events:
			'click': ->
				app.trigger 'show:artist', @model.get 'artistid'
				$('#sidebar').removeClass 'open'

	class Sidebar.SearchAlbum extends Marionette.ItemView
		template: '#sidebar-search-album-tpl'
		className: 'search-album'
		events:
			'click': ->
				app.trigger 'show:album', @model.get('id'), @model
				$('#sidebar').removeClass 'open'

	class Sidebar.SearchTracksEmpty extends Marionette.ItemView
		template: '#sidebar-search-tracks-empty-tpl'
		className: 'search-empty-message'

	class Sidebar.SearchArtistsEmpty extends Marionette.ItemView
		template: '#sidebar-search-artists-empty-tpl'
		className: 'search-empty-message'

	class Sidebar.SearchAlbumsEmpty extends Marionette.ItemView
		template: '#sidebar-search-artists-empty-tpl'
		className: 'search-empty-message'

	class Sidebar.SearchTracksLayout extends Marionette.CompositeView
		childView: Sidebar.SearchTrack
		childViewContainer: '.search-block-content'
		template: '#sidebar-search-result-layout-tpl'
		className: 'search-result'
		emptyView: Sidebar.SearchTracksEmpty
		onRender: ->
			if @collection.length is 0
				@$('.show-more').hide()
			else
				@$('.show-more').show()

		events:
		 'click .show-more': ->
			 val = $('.sidebar-body .search input').val()
			 app.navigate '/search/tracks/?q='+val
			 Sidebar.Controller.showSearchTracksPage val
			 $('#sidebar').removeClass 'open'
			 app.openSearchPageFromSite = true


	class Sidebar.SearchArtistsLayout extends Marionette.CompositeView
		childView: Sidebar.SearchArtist
		childViewContainer: '.search-block-content'
		template: '#sidebar-search-result-layout-tpl'
		className: 'search-result'
		emptyView: Sidebar.SearchArtistsEmpty
		onRender: ->
			if @collection.length is 0
				@$('.show-more').hide()
			else
				@$('.show-more').show()

		events:
			'click .show-more': ->
				val = $('.sidebar-body .search input').val()
				app.navigate '/search/artists/?q='+val
				Sidebar.Controller.showSearchArtistsPage val
				$('#sidebar').removeClass 'open'
				app.openSearchPageFromSite = true

	class Sidebar.SearchAlbumsLayout extends Marionette.CompositeView
		childView: Sidebar.SearchAlbum
		childViewContainer: '.search-block-content'
		template: '#sidebar-search-result-layout-tpl'
		className: 'search-result'
		emptyView: Sidebar.SearchAlbumsEmpty
		onRender: ->
			if @collection.length is 0
				@$('.show-more').hide()
			else
				@$('.show-more').show()

		events:
			'click .show-more': ->
				val = $('.sidebar-body .search input').val()
				app.navigate '/search/albums/?q='+val
				Sidebar.Controller.showSearchAlbumsPage val
				$('#sidebar').removeClass 'open'
				app.openSearchPageFromSite = true


	class Sidebar.SearchPageTrack extends Track.Item
		template: '#track-tpl'
		className: 'track-item'

	# class Sidebar.SearchPageArtist extends Artist.Item
	# 	template: '#artist-item-tpl'
	# 	className: 'artist-item'
	# 	onRender: ->
	# 		img = new Image
	# 		$(img).on 'load.img', =>
	# 			$(img).addClass 'artist-bg'
	# 			.hide()
	# 			.appendTo @$ '.controll'
	# 			.fadeIn 300

	# 			$(img).off 'load.img'
	# 		link = app.createImgLink(@model.get('img'),'300x300')
	# 		img.src = link

	# class Sidebar.SearchPageAlbum extends Album.Item
	# 	template: '#album-item-tpl'
	# 	className: 'album-item'
	# 	onRender: ->
	# 		img = new Image
	# 		$(img).on 'load.img', =>

	# 			$(img).addClass 'album-bg'
	# 			.hide()
	# 			.appendTo @$ '.controll'
	# 			.fadeIn 300

	# 			$(img).off 'load.img'
	# 		link = app.createImgLink(@model.get('img'),'300x300')
	# 		img.src = link


	class Sidebar.SearchPageTracks extends Marionette.CompositeView
		template: '#sidebar-search-page-layout-tpl'
		className: 'search-page tracks-wrapper'
		childView: Sidebar.SearchPageTrack
		childViewContainer: '.search-content'
		events:
			'click .close': ->
				Sidebar.Controller.closeSearchPage()

	class Sidebar.SearchPageArtists extends Marionette.CompositeView
		template: '#sidebar-search-page-layout-tpl'
		className: 'search-page artists-wrapper'
		childView: Sidebar.SearchPageArtist
		childViewContainer: '.search-content'
		events:
			'click .close': ->
				Sidebar.Controller.closeSearchPage()
		onBeforeRenderCollection: ->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.append itemView.el
				.append ' '

	class Sidebar.SearchPageAlbums extends Marionette.CompositeView
		template: '#sidebar-search-page-layout-tpl'
		className: 'search-page albums-wrapper'
		childView: Sidebar.SearchPageAlbum
		childViewContainer: '.search-content'
		events:
			'click .close': ->
				Sidebar.Controller.closeSearchPage()
		onBeforeRenderCollection: ->
			@attachHtml = (collectionView, itemView, index)->
				collectionView.$el.append itemView.el
				.append ' '

	return Sidebar