define ['./view.coffee','../simple_views/loader','../simple_views/update'], (Sidebar,Loader,Update)->
	require('./style.styl')

	class controller extends Marionette.Controller
		show: ->
			self = @;
			@layout = new Sidebar.Layout

			@listenTo @layout, 'show', ->
				$('.sidebar-wrapp').css 'min-height', $(window).height()
				$(window).on 'resize', ->
					$('.sidebar-wrapp').css 'min-height', $(window).height()

			app.sidebar.show @layout
			do @showBody

			user = app.request 'get:user'
			if user.promise
				user.done (user)->
					self.showUser.apply self,[user]

				user.fail (res)->
					if res.readyState is 0 and res.statusText is 'abort'
						console.log 'отмена'
					else
						console.log 'ошибка'
			else
				self.showUser.apply self,[user]

		showBody: ->
			mainMenuList = app.request 'get:menu'
			userMenuList = app.request 'get:tabs'

			@mainMenu = new Sidebar.SideMenuMainItems
				collection: mainMenuList

			@userMenu = new Sidebar.SideMenuUserItems
				collection: userMenuList

			@layout.mainMenu.show @mainMenu
			@layout.userMenu.show @userMenu

			if !Sidebar.urlIsFind
				if !~app.getCurrentRoute().indexOf 'profile'
					$('.sidebar-menu-item.recomend').addClass 'active'

			@listenTo app, 'switch:page', (page)->
				$('#sidebar .sidebar-menu-item.active').removeClass 'active'
				$('#sidebar .sidebar-menu-item.'+page).addClass 'active'
				# app.Parts.UserNotification.Controller.remove()
				$('#sidebar').removeClass 'open'

			$('#header .title').mouseenter ->
				$('#sidebar').addClass 'open'

			$('.sidebar-overlay').click ->
				# app.Parts.UserNotification.Controller.remove()
				$('#sidebar').removeClass 'open'

		showSearchResult: (val)->
			if $.trim(val) is ''
				if @isSearchLayoutShown
					@isSearchLayoutShown = false
					do @searchLayout.destroy
					$('.sidebar-content-scroll').customScrollbar('resize',true)

				return
			if !@isSearchLayoutShown
				@searchLayout = new Sidebar.SearchLayout
				@layout.searchResult.show @searchLayout
				@listenTo @searchLayout, 'destroy', ->
					@isSearchLayoutShown = false

			@isSearchLayoutShown = true
			@SearchTracks val
			@SearchAlbums val
			@SearchArtists val

		SearchTracks: (val)->
			fetch = app.request 'search:tracks',
				data:
					q: val
					page: 1
					items_on_page: 3

			loader = new Loader.View
				styles:
					position: 'absolute'
					top: '35%'
					left: '45%'

			@searchLayout.tracks.show loader

			fetch.done (collection)=>
				view = new Sidebar.SearchTracksLayout
					collection: collection

				@searchLayout.tracks.show view

			fetch.fail (res)->


		SearchArtists: (val)->
			fetch = app.request 'search:artists',
				data:
					q: val
					page: 1
					items_on_page: 3

			loader = new Loader.View
				styles:
					position: 'absolute'
					top: '35%'
					left: '45%'

			@searchLayout.artists.show loader

			fetch.done (collection)=>
				view = new Sidebar.SearchArtistsLayout
					collection: collection

				@searchLayout.artists.show view

			fetch.fail (res)->

		SearchAlbums: (val)->
			fetch = app.request 'search:albums',
				data:
					q: val
					page: 1
					items_on_page: 3


			loader = new Loader.View
				styles:
					position: 'absolute'
					top: '35%'
					left: '45%'

			@searchLayout.albums.show loader

			fetch.done (collection)=>
				view = new Sidebar.SearchAlbumsLayout
					collection: collection

				@searchLayout.albums.show view

			fetch.fail (res)->


		showSearchTracksPage: (val)->
			list = app.request 'search:page','track',
				data:
					q: val
					page: 1
					items_on_page: 60

			loader = new Loader.View
				styles:
					'top': $(window).height()/2

			app.content.show loader

			list.done (searchCollection)->

				$(window).off '.searchpage'
				fetchMore = ->
					$(window).off '.searchpage'
					page = Math.ceil(searchCollection.length/60)+1;

					trackFetch = searchCollection.fetch
						data:
							q: val
							page: page
							items_on_page: 90

						remove: false

					trackFetch.done (newcollection)->

						do app.checkActiveTrack
						if newcollection.searched.length
							winHeight = $('body').height();
							$(window).on 'scroll.searchpage', ->
								scroll = $(window).scrollTop()+$(window).height()*5
								if winHeight < scroll
									fetchMore();

					trackFetch.fail (res)->
						if res.status is 422
							contacts.add(res.responseJSON);
						else
							console.log('Произошла ошибка');

				model = new Backbone.Model
					type: 'trk'
					title: val

				searchView = new Sidebar.SearchPageTracks
					model: model
					collection: searchCollection

				app.content.show searchView
				do app.checkActiveTrack

				winHeight = $('body').height();
				$(window).on 'scroll.searchpage', ->
					scroll = $(window).scrollTop()+$(window).height()*5
					if winHeight < scroll
						fetchMore();

		showSearchAlbumsPage: (val)->
			list = app.request 'search:page','album',
				data:
					q: val
					page: 1
					items_on_page: 30

			loader = new Loader.View
				styles:
					'top': $(window).height()/2

			app.content.show loader

			list.done (searchCollection)->

				$(window).off '.searchpage'
				fetchMore = ->
					$(window).off '.searchpage'
					page = Math.ceil(searchCollection.length/30)+1;

					trackFetch = searchCollection.fetch
						data:
							q: val
							page: page
							items_on_page: 30

						remove: false

					trackFetch.done (newcollection)->

						if newcollection.searched.length
							winHeight = $('body').height();
							$(window).on 'scroll.searchpage', ->
								scroll = $(window).scrollTop()+$(window).height()*5
								if winHeight < scroll
									fetchMore();

					trackFetch.fail (res)->
						if res.status is 422
							contacts.add(res.responseJSON);
						else
							console.log('Произошла ошибка');

				model = new Backbone.Model
					type: 'alb'
					title: val

				searchView = new Sidebar.SearchPageAlbums
					model: model
					collection: searchCollection

				app.content.show searchView
				do app.checkActiveTrack

				winHeight = $('body').height();
				$(window).on 'scroll.searchpage', ->
					scroll = $(window).scrollTop()+$(window).height()*5
					if winHeight < scroll
						fetchMore();

		showSearchArtistsPage: (val)->
			list = app.request 'search:page','artist',
				data:
					q: val
					page: 1
					items_on_page: 30

			loader = new Loader.View
				styles:
					'top': $(window).height()/2

			app.content.show loader

			list.done (searchCollection)->

				$(window).off '.searchpage'
				fetchMore = ->
					$(window).off '.searchpage'
					page = Math.ceil(searchCollection.length/30)+1;

					trackFetch = searchCollection.fetch
						data:
							q: val
							page: page
							items_on_page: 30

						remove: false

					trackFetch.done (newcollection)->

						if newcollection.searched.length
							winHeight = $('body').height();
							$(window).on 'scroll.searchpage', ->
								scroll = $(window).scrollTop()+$(window).height()*5
								if winHeight < scroll
									fetchMore();

					trackFetch.fail (res)->
						if res.status is 422
							contacts.add(res.responseJSON);
						else
							console.log('Произошла ошибка');

				model = new Backbone.Model
					type: 'art'
					title: val

				searchView = new Sidebar.SearchPageArtists
					model: model
					collection: searchCollection

				app.content.show searchView
				do app.checkActiveTrack

				winHeight = $('body').height();
				$(window).on 'scroll.searchpage', ->
					scroll = $(window).scrollTop()+$(window).height()*5
					if winHeight < scroll
						fetchMore();


		closeSearchPage: ->
			if app.openSearchPageFromSite
				history.back()

			else
				app.trigger 'show:recomend'




	Sidebar.Controller = new controller

	_.extend Sidebar.Controller,
		showUser: (user)->
			user = new Sidebar.User
				model: user

			@layout.user.show user

	return Sidebar