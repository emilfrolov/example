define ['./view.coffee'], (Notification)->
	require('./style.styl');

	class controller extends Marionette.Controller
		show: (opts)->
			###

  			message: сообщение которое хотим написать
  			type: тип ошибки {error: красное поле, success: зеленое поле} по умолчанию желтое поле
  			closed (bool): дает возможность закрыть сообщение (по умолчанию true)
  			time: время в милисикундах через которое плашка закроется

			###
			if !opts.closed
				opts.closed = true

			model = new Backbone.Model opts
			@notifiView = new Notification.View
				model: model

			time = if opts.time then opts.time else 3000

			if Notification.Timeout
				clearTimeout Notification.Timeout

			Notification.Timeout = setTimeout =>
				do @close
			,time

			app.notification.show @notifiView

		close: ->
			if @notifiView
				do @notifiView.destroy

			if @subscribeNotifi
				do @showSubscribe

		showSubscribe: ->
			@subscribeNotifi = true
			model = new Backbone.Model
				class: 'error'
				message: app.locale.notification.subsM
				closed: false

			@notifiView = new Notification.View
				model: model

			app.notification.show @notifiView
			$(window).resize()

		closeSubscribe: ->
			@subscribeNotifi = false
			do @close

	Notification.Controller = new controller

	return Notification