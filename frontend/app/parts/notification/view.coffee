define [], ->
	Notification = {}

	class Notification.View extends Marionette.ItemView
		template: (sData)->
			html = require('./templates/notification-default.jade')
			return html(sData)
		className: ->
			return if @model.has 'type' then @model.get 'type' else 'default'

		events:
			'click .close-icon': ->
				do Notification.Controller.close

			'click .to-subscribe': ->
				app.trigger 'show:profile'

		onShow: ->
			$('body').addClass 'notifi'

		onDestroy: ->
			$('body').removeClass 'notifi'

	return Notification