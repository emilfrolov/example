define ->
	Slider = {}

	class Slider.SliderItem extends Marionette.ItemView
		tagName: 'li'
		template: '#slider-item-tpl'
		events:
			'click': ->
				if !app.slideMove
					app.trigger 'show:album', @model.get('promoId')


	class Slider.SliderItems extends Marionette.CompositeView
		className: 'slider-cut'
		template: '#slider-layout-tpl'
		childView: Slider.SliderItem
		childViewContainer: '.slider-scroll'
		onRender: ->
			if $(window).height() < 800
				width = 831
			else
				width = 1166
			@$childViewContainer.width @children.length * width

	return Slider