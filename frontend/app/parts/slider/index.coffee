define ['./view.coffee','../simple_views/loader'], (Slider,Loader)->
	require('./style.styl')

	class Controller extends Marionette.Controller


		drawSlider: (selector)->
			slides = app.request 'get:slides'
			@rm = new Marionette.RegionManager
			@sliderContainer = @rm.addRegion 'sliderContainer', selector

			showSliderOne = (collection)=>
				@sliderView = new Slider.SliderItems
					collection: collection

				@listenTo @sliderView, 'show', ->
					self = @
					@slide = {}

					@sliderContainer.$el.find('.slider-cut').mousedown (e)->
						e.preventDefault()
						self.slide.startPos = e.pageX
						self.slide.transform = self.margin
						side = null

						$(document).on 'mousemove.sliderup', (e)->
							e.preventDefault()
							posX = e.pageX - self.slide.startPos
							if posX > 10 or posX < -10
								app.slideMove = true
								if posX < 0
									side = 'left'
								else
									side = 'right'

								curPos = self.margin + posX
								$('.slider-scroll').css 'margin-left': curPos+'px'
								self.slide.transform = curPos

							return false

						$(document).on 'mouseup.sliderup', (e)->
							setTimeout ->
								app.slideMove = false
							,100

							if side is 'right'
								self.slide.listener = true
								posX = e.pageX
								curPos = self.slide.transform + posX

								state = ~Math.ceil(curPos/self.sliderWidth)
								if state
									self.prewSlide(self.slide.transform)
								else
									self.returnSlide()
							else if side is 'left'
								self.slide.listener = true
								posX = e.pageX
								curPos = self.slide.transform - posX


								state = ~Math.ceil(curPos/(self.sliderWidth-300))
								if state
									self.nextSlide()
								else
									self.returnSlide()

							else
								self.returnSlide()

							$(document).off '.sliderup'


					@sliderContainer.$el.find('.slider-cut').on 'mousedown.slider', ->
						self.stopSlider()

					@sliderContainer.$el.find('.slider-cut').on 'mouseup.slider', ->
						self.startSlider()

					$(window).on 'resize.slide', ->
						wWidth = $(window).width()
						wHeight = $(window).height()
						if wWidth <= 1024
							wWidth = 1024

						if wHeight < 800
							if self.size isnt 'small'
								do self.resizeSlider
								do self.setSliderScrollWidth
						else
							if self.size isnt 'big'
								do self.resizeSlider
								do self.setSliderScrollWidth

						self.sliderView.$el.width wWidth


				@listenTo @sliderView, 'before:show', ->
					do @resizeSlider


				@sliderContainer.show @sliderView




			if slides.promise
				loader = new Loader.View
					container: 'slider-loader'
					styles:
						top: '50%'

				@sliderContainer.show loader

				slides.done (collection)=>
					showSliderOne collection

				slides.fail =>
					callback = ->
						app.trigger 'show:new'

					update = new Update.View
						container: 'slider-update'
						styles:
							top: '50%'
						callback: callback

					@sliderContainer.show update

			else
				showSliderOne slides

		remove: ->
			@stopSlider()
			$(window).off '.slide'


		prewSlide: (slide)->
			@margin = slide - @sliderWidth
			scrollElem = $('.slider-scroll')
			scrollElem.css
				'margin-left':  @margin+'px'

			scrollElem.children('li').eq(scrollElem.children('li').length-1).prependTo scrollElem

			@margin = -@sliderWidth
			scrollElem.animate {'margin-left':  @margin+'px'},200

		returnSlide: ->
			scrollElem = $('.slider-scroll')
			scrollElem.animate {'margin-left':  @margin+'px'},200

		nextSlide: ->
			@margin -= @sliderWidth
			scrollElem = $('.slider-scroll')

			scrollElem.animate {'margin-left':  @margin+'px'},200, =>
				scrollElem.children('li').eq(0).appendTo scrollElem

				@margin += @sliderWidth
				scrollElem.css
					'margin-left':  @margin+'px'

		startSlider: ->
			@stopSlider()
			@timer = setInterval =>
				@nextSlide()
			,5000

		stopSlider: ->
			clearInterval @timer
			
			
		resizeSlider: ->
			do @stopSlider
			if $(window).height() < 800
				@sliderWidth = 825
				@size = 'small'
			else
				@sliderWidth = 1160
				@size = 'big'

			@margin = -@sliderWidth
			@sliderView.$childViewContainer.css
				'margin-left':  +@margin+'px'

			@sliderView.$el.width $(window).width()
			do @startSlider

		setSliderScrollWidth: ->
		setSliderScrollWidth: ->
			if $(window).height() < 800
				width = 831
			else
				width = 1166

			@sliderContainer.$el.find('.slider-scroll').width $('.slider-scroll li').length * width

	return Controller