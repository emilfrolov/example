define [], ->
	Menu = {}
	class Menu.Item extends Backbone.Model.Selected

	class Menu.Collection extends Backbone.Collection.SingleSelect
		model: Menu.Item

	class Menu.Genre extends Backbone.Model.Selected

	class Menu.Genries extends Backbone.Collection.MultiSelected
		model: Menu.Genre
		url: '/genries'
		parse: (collection)->
			return _.where collection.genres, 'is_parent': true

	initializeMenu = ->
		Menu.List = new Menu.Collection([
			# {
			# 	title: 'Радио',
			# 	href: 'radio',
			# 	class: 'menu-radio',
			# 	trigger: 'show:radio'
			# },
			{
				title: app.locale.menu.feed,
				href: 'feed',
				class: 'feed',
				trigger: 'show:feed'
			},
			{
				title: app.locale.menu.new,
				href: 'new',
				class: 'new',
				trigger: 'show:new'
			},
			{
				title: app.locale.menu.popular,
				href: '',
				class: 'recomend',
				trigger: 'show:recomend'
			},
			{
				title: app.locale.menu.playlists,
				href: 'playlists',
				class: 'playlists',
				trigger: 'show:playlists'
			},
#			 {
#			 	title: app.locale.menu.charts,
#			 	href: 'charts',
#			 	class: 'charts',
#			 	trigger: 'show:charts'
#			 },
			{
				title: app.locale.menu.clips,
				href: 'video',
				class: 'video',
				trigger: 'show:video'
			}
		]);






	API =
		getMenus: ->
			if Menu.List is undefined
				initializeMenu()
			return Menu.List

		getGenries: ->
			ganries = new Menu.Genries

			defer = $.Deferred()
			fetch = do ganries.fetch

			fetch.done ->
				defer.resolveWith fetch, [ganries]

			fetch.fail ->
				defer.rejectWith fetch, arguments

			return defer.promise()


	app.reqres.setHandler 'get:menu', ->
		return API.getMenus()

	app.reqres.setHandler 'get:genries', ->
		return API.getGenries()

