define ->
	Model = {}
	class Model.Item extends Backbone.Model
		urlRoot: '/agreement'

		parse: (model)->
			return model


	initializeUser = ->
		Model.Agreement = new Model.Item();
		defer = $.Deferred()
		fechModel = Model.Agreement.fetch();

		if fechModel
			fechModel.done (res)->
				defer.resolveWith fechModel,[Model.Agreement]

			fechModel.fail ->
				defer.rejectWith fechModel,arguments

			return defer.promise()





	API =
		getAgreement: ->
			if Model.Agreement is undefined
				return initializeUser()
			return Model.Agreement



	app.reqres.setHandler 'get:agreement', ->
		return API.getAgreement()

