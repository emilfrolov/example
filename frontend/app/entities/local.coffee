define ->
	Local = {}
	class Local.Genre extends Backbone.Model

	class Local.Genries extends Backbone.Collection
		model: Local.Genre
		localStorage: new Backbone.LocalStorage "Genries"
		save: ->
			@each (model)->
				Backbone.localSync "update", model

	
	initializeGenre = ->
		Local.GenriesList = new Local.Genries

		do Local.GenriesList.fetch


	API =
		getGenries: ->
			if Local.GenriesList is undefined
				initializeGenre()

			return Local.GenriesList


	app.reqres.setHandler 'get:local:genries', ->
		return API.getGenries()
