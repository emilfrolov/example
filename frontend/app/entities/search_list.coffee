define ['../config/custom_elements/track','../config/custom_elements/album'], (CustomTrack,CustomAlbum)->
	SearchList = {}
	
	class SearchList.Track extends CustomTrack.Model
		parse: (item)->
			item.uniq = 'search-'+item.id
			item.optionsList = [
				{
					title: app.locale.options.addTPl
					class: 'add-to-playlist'
				},{
					title: app.locale.options.toArt
					class: 'options-open-artist'
					artist: true
				},{
					title: app.locale.options.toAlb
					class: 'options-open-album'
				}
			]
			return item

	class SearchList.Album extends CustomAlbum.Model
		urlRoot: ->
			return app.getRequestUrl("/compatibility/catalogue/getAlbum","?albumid=#{@id}")
		parse: (model)->
			if model.album
				return model.album
			else
				return model

	class SearchList.Artist extends Backbone.Model
		urlRoot: '/artist'

	class SearchList.Playlist extends Backbone.Model
		urlRoot: '/playlist'

	class SearchList.TracksCollection extends Backbone.Collection
		model: SearchList.Track
		url: app.getRequestUrl('/search/tracks')

		parse: (collection)->
			return collection.searched


	class SearchList.AlbumsCollection extends Backbone.Collection
		model: SearchList.Album
		url: app.getRequestUrl('/search/albums')

		parse: (collection)->
			return collection.searched

	class SearchList.ArtistsCollection extends Backbone.Collection
		model: SearchList.Artist
		url: app.getRequestUrl('/search/artists')

		parse: (collection)->
			return collection.searched

	class SearchList.PlaylistsCollection extends Backbone.Collection
		model: SearchList.Playlist
		url: url: app.getRequestUrl('/search/tracks')

		parse: (collection)->
			return collection.searched


	initializeTracks = (options)->
		if SearchList.fetchTracksCollection
			SearchList.fetchTracksCollection.abort()

		SearchList.Tracks = new SearchList.TracksCollection
		options || (options = {});
		defer = $.Deferred();
		SearchList.fetchTracksCollection = SearchList.Tracks.fetch(_.omit(options,'success','error'));
		SearchList.fetchTracksCollection.done ->
			if SearchList.fetchTracksCollection.responseJSON
				defer.resolveWith SearchList.fetchTracksCollection,[SearchList.Tracks,SearchList.fetchTracksCollection.responseJSON.count]

		SearchList.fetchTracksCollection.fail ->
			defer.rejectWith SearchList.fetchTracksCollection,arguments

		return defer.promise();

	initializeAlbums = (options)->
		if SearchList.fetchAlbumsCollection
			SearchList.fetchAlbumsCollection.abort()

		SearchList.Albums = new SearchList.AlbumsCollection
		options || (options = {});
		defer = $.Deferred();
		SearchList.fetchAlbumsCollection = SearchList.Albums.fetch(_.omit(options,'success','error'));
		SearchList.fetchAlbumsCollection.done ->
			if SearchList.fetchAlbumsCollection.responseJSON
				defer.resolveWith SearchList.fetchAlbumsCollection,[SearchList.Albums,SearchList.fetchAlbumsCollection.responseJSON.count]

		SearchList.fetchAlbumsCollection.fail ->
			defer.rejectWith SearchList.fetchAlbumsCollection,arguments

		return defer.promise();

	initializeArtists = (options)->
		if SearchList.fetchArtistsCollection
			SearchList.fetchArtistsCollection.abort()

		SearchList.Artists = new SearchList.ArtistsCollection
		options || (options = {});
		defer = $.Deferred();
		SearchList.fetchArtistsCollection = SearchList.Artists.fetch(_.omit(options,'success','error'));
		SearchList.fetchArtistsCollection.done ->
			if SearchList.fetchArtistsCollection.responseJSON
				defer.resolveWith SearchList.fetchArtistsCollection,[SearchList.Artists,SearchList.fetchArtistsCollection.responseJSON.count]

		SearchList.fetchArtistsCollection.fail ->
			defer.rejectWith SearchList.fetchArtistsCollection,arguments

		return defer.promise();

	initializePlaylists = (options)->
		if SearchList.fetchPlaylistsCollection
			SearchList.fetchPlaylistsCollection.abort()

		SearchList.Playlists = new SearchList.PlaylistsCollection
		options || (options = {});
		defer = $.Deferred();
		SearchList.fetchPlaylistsCollection = SearchList.Playlists.fetch(_.omit(options,'success','error'));
		SearchList.fetchPlaylistsCollection.done ->
			if SearchList.fetchPlaylistsCollection.responseJSON
				defer.resolveWith SearchList.fetchPlaylistsCollection,[SearchList.Playlists,SearchList.fetchPlaylistsCollection.responseJSON.count]

		SearchList.fetchPlaylistsCollection.fail ->
			defer.rejectWith SearchList.fetchPlaylistsCollection,arguments

		return defer.promise();


	initializeSearchPage = (type,options)->
		if SearchList.fetchPageCollection
			SearchList.fetchPageCollection.abort()

		switch	type
			when 'artist'
				SearchList.Page = new SearchList.ArtistsCollection
			when 'album'
				SearchList.Page = new SearchList.AlbumsCollection
			when 'track'
				SearchList.Page = new SearchList.TracksCollection
			when 'playlist'
				SearchList.Page = new SearchList.PlaylistsCollection


		options || (options = {});
		defer = $.Deferred();
		SearchList.fetchPageCollection = SearchList.Page.fetch(_.omit(options,'success','error'));
		SearchList.fetchPageCollection.done ->
			if SearchList.fetchPageCollection.responseJSON
				defer.resolveWith SearchList.fetchPageCollection,[SearchList.Page,SearchList.fetchPageCollection.responseJSON.count]

		SearchList.fetchPageCollection.fail ->
			defer.rejectWith SearchList.fetchPageCollection,arguments

		return defer.promise();


	API =
		getTracksCollections: (options)->
			return initializeTracks options

		getAlbumsCollections: (options)->
			return initializeAlbums options

		getArtistsCollections: (options)->
			return initializeArtists options

		getPlaylistsCollections: (options)->
			return initializePlaylists options

		getSearchPageCollections: (type,options)->
			return initializeSearchPage type,options


	app.reqres.setHandler 'search:tracks', (options)->
		API.getTracksCollections options

	app.reqres.setHandler 'search:albums', (options)->
		API.getAlbumsCollections options

	app.reqres.setHandler 'search:artists', (options)->
		API.getArtistsCollections options

	app.reqres.setHandler 'search:playlists', (options)->
		API.getPlaylistsCollections options

	app.reqres.setHandler 'search:page', (type,options)->
		API.getSearchPageCollections type,options