define ->
  Notify = {}
  class Notify.Item extends Backbone.Model

  class Notify.Items extends Backbone.Collection
    model: Notify.Item
    localStorage: new Backbone.LocalStorage "Notify"
    save: ->
      @each (model)->
        Backbone.localSync "update", model


  initializeNotify = ->
    Notify.List = new Notify.Items

    do Notify.List.fetch


  API =
    getNotify: ->
      if Notify.List is undefined
        initializeNotify()

      return Notify.List


  app.reqres.setHandler 'get:user:notification', ->
    return API.getNotify()