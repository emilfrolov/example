define ->
    Log = {}
    class Log.Model extends Backbone.Model

        validate: (attrs, options)->
            if !attrs.url?
              return "A 'updateUrl' property or function must be specified"

        url: ->
            base =
                _.result(this, 'urlRoot') or
                _.result(this.collection, 'url') or
                urlError();

            if this.isNew()
                return base;
            id = this.get(this.idAttribute);
            return base.replace(/[^\/]$/, '$&/') + encodeURIComponent(id);

        sync: (method,model,options)->
          Backbone.sync.apply this,[method,model,options]

    class Log.Collection extends Backbone.Collection
        model: Log.Model
        localStorage: new Backbone.LocalStorage "Logs"
        save: ->
            @each (model)->
                Backbone.localSync "update", model

    
    initializeLog = ->
        Log.List = new Log.Collection

        do Log.List.fetch


    API =
        getLogs: ->
            if Log.List is undefined
                initializeLog()

            return Log.List


    app.reqres.setHandler 'get:logs', ->
        return API.getLogs()
