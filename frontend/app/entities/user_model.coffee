define ['../parts/notification'], (Notification)->
	Model = {}
	class Model.Item extends Backbone.Model
		urlRoot: app.getRequestUrl('/compatibility/user/GetBySid')
		parse: (model)->
			if model.users
				return model.users[0]
			else
				return model.user

	class Model.IdItem extends Backbone.Model
		urlRoot: ->
			return app.getRequestUrl("/beelineMusicV1/society/getPublicProfiles");
		parse: (model)->
			if model.users
				return model.users[0]
			else
				return model.user


	initializeUser = ->
		if Model.isLoad
			return Model.defer.promise()

		Model.isLoad = true

		user = new Model.Item();
		Model.defer = $.Deferred()
		fetchUser = user.fetch();
		if !Model.UserTimer
			Model.UserTimer = setInterval ->
				do API.fetchUser
			,60000

		if fetchUser
			fetchUser.done (res)->
				if res.code is 0
					Model.User = user
					Model.defer.resolveWith fetchUser,[Model.User]

					if Model.User.get('subscription') is 'DISABLED'
						do Notification.Controller.showSubscribe
					else if Model.User.get('subscription') is 'OTHER'
						do Notification.Controller.showSubscribe

				else
					if res.code is -4
						$.get '/exit', ->
							location.href = ''
					Model.defer.rejectWith fetchUser,arguments
					Model.isLoad = false

			fetchUser.fail ->
				Model.defer.rejectWith fetchUser,arguments
				Model.isLoad = false

			return Model.defer.promise()

	initializeUserById = (id)->
		if Model.isLoadById
			return Model.deferById.promise()

		Model.isLoadById = true

		user = new Model.IdItem();
		Model.deferById = $.Deferred()
		fetchUser = user.fetch
			data:
				userIds: [id]

		if fetchUser
			fetchUser.done (res)->
				if res.code is 0
					if res.users[0]['not_found']
						Model.deferById.rejectWith fetchUser,arguments
					else
						Model.UserById = user
						Model.deferById.resolveWith fetchUser,[Model.UserById]
						Model.actualUserId = id
				else
					Model.deferById.rejectWith fetchUser,arguments

				Model.isLoadById = false

			fetchUser.fail ->
				Model.deferById.rejectWith fetchUser,arguments
				Model.isLoadById = false

			return Model.deferById.promise()





	API =
		getUser: ->
			if Model.User is undefined
				return initializeUser()
			defer = $.Deferred()
			setImmediate ->
				defer.resolve Model.User

			return defer.promise();

		getUserById: (id)->
			if Model.actualUserId isnt id
				return initializeUserById id

			defer = $.Deferred()
			setImmediate ->
				defer.resolve Model.UserById

			return defer.promise();

		fetchUser: ->
			if Model.User
				fetch = Model.User.fetch()

				fetch.done ->
					status = Model.User.get('subscription')
					if status isnt 'DISABLED' and status isnt 'OTHER'
						do Notification.Controller.closeSubscribe
					else if status is 'OTHER'
						do Notification.Controller.showSubscribe
					else
						do Notification.Controller.showSubscribe



	app.reqres.setHandler 'get:user', (id)->
		if id?
			return API.getUserById id
		else
			return API.getUser()

	app.reqres.setHandler 'fetch:user', ->
		return API.fetchUser()

	return Model

