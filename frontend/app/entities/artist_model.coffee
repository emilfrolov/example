define ->
	Model = {}
	
	class Model.Item extends Backbone.Model
		urlRoot: ->
			return app.getRequestUrl("/artist/get");

		parse: (model)->
			return model.artist

	class Model.Track extends Backbone.Model
		parse: (item)->
			item.uniq = 'artist-'+item.id
			return item

	class Model.Tracks extends Backbone.Collection
		model: Model.Track
		url: app.getRequestUrl("/compatibility/catalogue/getBeelineContent","?type=tracks&sort=rating&countitems=10")
		parse: (collection)->
			return collection.items


	class Model.Album extends Backbone.Model
		urlRoot: ->
			return app.getRequestUrl("/compatibility/catalogue/getAlbum","?albumid=#{@id}")
		parse: (model)->
			if model.album
				model.album.album = true
				return model.album
			else
				model.album = true
				return model

	class Model.Albums extends Backbone.Collection
		model: Model.Album
		url: app.getRequestUrl("/beelineMusicV1/catalog/getAlbums","?sort=rating")
		parse: (collection)->
			return collection.items

	class Model.ArtistVideoModel extends Backbone.Model
		parse: (item)->
			item.id = item['id']['videoId']
			item.snippet.resourceId = {}
			item.snippet.resourceId.videoId = item.id
			return item


	class Model.ArtistVideoItems extends Backbone.Collection
		model: Model.ArtistVideoModel
		url: '/artistVideo'

	initializeArtistVideo = (q)->
		if Model.VideoLoadProcess
			Model.VideoLoadProcess.abort()

		Model.ArtistVideoList = new Model.ArtistVideoItems();
		defer = $.Deferred()
		Model.VideoProcess = fech = Model.ArtistVideoList.fetch
			data:
				q: q
		if fech
			fech.done ->
				Model.Artistq = q
				defer.resolveWith fech,[Model.ArtistVideoList]

			fech.fail ->
				defer.rejectWith fech,arguments

			return defer.promise()


	initializeArtistTracks = (options={})->
		Model.ArtistTracks = new Model.Tracks
		defer = $.Deferred()
		Model.TracksLoadProcess = fechModel = Model.ArtistTracks.fetch _.omit(options,'success','error')

		if fechModel
			fechModel.done =>
				defer.resolveWith fechModel,[Model.ArtistTracks]
				Model.artistTrackId = options.data.artist

			fechModel.fail ->
				defer.rejectWith fechModel,arguments

			return defer.promise()

	initializeArtistAlbums = (options={})->
		ArtistAlbums = new Model.Albums
		defer = $.Deferred()
		Model.AlbumsLoadProcess = fechModel = ArtistAlbums.fetch _.omit(options,'success','error')

		if fechModel
			fechModel.done =>
				defer.resolveWith fechModel,[ArtistAlbums]
				Model.artistAlbumId = options.data.artist

			fechModel.fail ->
				defer.rejectWith fechModel,arguments

			return defer.promise()

	initializeArtist = (id)->
		Model.Artist = new Model.Item
		defer = $.Deferred()
		Model.LoadProcess = fechModel = Model.Artist.fetch
			data:
				'artistid': id

		if fechModel
			fechModel.done (res)=>
				defer.resolveWith fechModel,[Model.Artist]
				Model.artistId = id

			fechModel.fail ->
				defer.rejectWith fechModel,arguments

			return defer.promise()



	API =
		getArtist: (id)->
			if Model.artistId and Model.artistId is id
				return Model.Artist
			else
				return initializeArtist id

		getArtistTracks: (options)->
			if Model.artistTrackId and Model.artistTrackId is options.data.artist
				return Model.ArtistTracks
			else
				return initializeArtistTracks options

		getArtistAlbums: (options)->
			return initializeArtistAlbums options

		getArtistVideos: (q)->
			if Model.Artistq isnt q
				return initializeArtistVideo q
			return Model.ArtistVideoList



	app.reqres.setHandler 'get:artist', (id)->
		return API.getArtist(id)

	app.reqres.setHandler 'get:artist:tracks', (options)->
		return API.getArtistTracks options

	app.reqres.setHandler 'get:artist:albums', (options)->
		return API.getArtistAlbums options

	app.reqres.setHandler 'get:artist:videos', (q)->
		return API.getArtistVideos q

	app.reqres.setHandler 'stop:artist:request', (type)->
		if type
			switch type
				when 'tracks'
					if Model.TracksLoadProcess
						Model.TracksLoadProcess.abort()

				when 'albums'
					if Model.AlbumsLoadProcess
						Model.AlbumsLoadProcess.abort()
				when 'artist'
					if Model.LoadProcess
						Model.LoadProcess.abort()
				when 'video'
					if Model.VideoLoadProcess
						Model.VideoLoadProcess.abort()
