define ->
	Tab = {}
	class Tab.Item extends Backbone.Model

		select: ->
			@.collection.each (model)->
				model.selected = false

			@.selected = true

	class Tab.Collection extends Backbone.Collection
		model: Tab.Item

	initializeTab = ->
		Tab.List = new Tab.Collection([
			{
				title: app.locale.menu.tracksAndPlaylists,
				class: 'tracks',
				selected: 'selected',
				href: 'user/tracks'
				trigger: 'show:user:tracks'
			},
			{
				title: app.locale.menu.albums,
				class: 'albums',
				href: 'user/albums'
				trigger: 'show:user:albums'
			}
			# ,{
			# 	title: 'Радио',
			# 	class: 'radio',
			# 	trigger: 'show:tracks'
			# }
		]);




	API =
		getTabs: ->
			if Tab.List is undefined
				initializeTab()
			return Tab.List



	app.reqres.setHandler 'get:tabs', ->
		return API.getTabs()

