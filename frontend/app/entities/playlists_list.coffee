define ->
    PlaylistsList = {}
    class PlaylistsList.Item extends Backbone.Model
        urlRoot: ->
            app.getRequestUrl("/playlist/get","?playlistid=#{@id}&items_on_page=1000")

        parse: (model)->
            if model.album
                model.album.album = true
                return model.album
            else
                model.album = true
                return model

    class PlaylistsList.Items extends Backbone.Collection
        model: PlaylistsList.Item
        url: app.getRequestUrl("/beelineMusicV1/playlist/top")
        parse: (collection)->
            return collection.searched


    initializePlaylists = ->
        if PlaylistsList.ListIsLoad
            return PlaylistsList.ListDefer.promise()

        PlaylistsList.ListIsLoad = true

        playlists = new PlaylistsList.Items();
        PlaylistsList.ListDefer = $.Deferred()
        fetch = playlists.fetch
            data:
                count: 60
                offset: 0

        if fetch
            fetch.done ()->
                PlaylistsList.List = playlists
                PlaylistsList.ListDefer.resolveWith fetch,[PlaylistsList.List]
            fetch.fail ->
                PlaylistsList.ListIsLoad = false
                PlaylistsList.ListDefer.rejectWith fetch,arguments

            return PlaylistsList.ListDefer.promise()


    initializeSinglePlaylist = (id)->
        PlaylistsList.SinglePlaylist = new PlaylistsList.Item
            id: id
        defer = $.Deferred()
        fetch = PlaylistsList.SinglePlaylist.fetch()

        if fetch
            fetch.done (res)->
                if res.code is 0
                    PlaylistsList.PlaylistId = id
                    defer.resolveWith fetch,[PlaylistsList.SinglePlaylist]
                else
                    defer.rejectWith fetch,[res]
            fetch.fail ->
                defer.rejectWith fetch,arguments

            return defer.promise()



    API =
        getPlaylists: ->
            if PlaylistsList.List is undefined
                return initializePlaylists()
            return PlaylistsList.List

        getPlaylist: (id)->
            if PlaylistsList.PlaylistId is id
                return PlaylistsList.SinglePlaylist

            else
                return initializeSinglePlaylist id

        getTempPlaylists: (collection)->
            return new PlaylistsList.Items collection



    app.reqres.setHandler 'get:playlists', ->
        return API.getPlaylists()

    app.reqres.setHandler 'get:playlist', (id)->
        return API.getPlaylist id

    app.reqres.setHandler 'get:temp:playlists', (collection)->
        return API.getTempPlaylists collection.toJSON()
