define ->
	Slider = {}
	
	class Slider.Item extends Backbone.Model
		urlRoot: '/slider'


	class Slider.Items extends Backbone.Collection
		model: Slider.Item
		url: app.getRequestUrl('/generalV1/cp/getBanners')
		parse: (collection)->
			return collection.banners


	initializeSlides = ->
		sliderList = new Slider.Items();
		defer = $.Deferred()
		fetch = sliderList.fetch();
		if fetch
			fetch.done (res)->
				if res.code is 0
					Slider.List = sliderList
					defer.resolveWith fetch,[Slider.List]
				else
					defer.rejectWith fetch,[res]
			fetch.fail ->
				defer.rejectWith fetch,arguments

			return defer.promise()



	API =
		getSlides: ->
			if Slider.List is undefined
				return initializeSlides()
			return Slider.List



	app.reqres.setHandler 'get:slides', ->
		return API.getSlides()
