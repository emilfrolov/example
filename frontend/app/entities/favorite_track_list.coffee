define ['../config/custom_elements/track'], (CustomTrack)->
	FavoriteTracks = {}
	class FavoriteTracks.Item extends CustomTrack.Model
		deleteUrl: ->
			app.getRequestUrl("/compatibility/favourites/removeitem","?contentid=#{@id}&contenttype=TRCK")

		updateUrl: ->
			app.getRequestUrl("/compatibility/favourites/additem","?contentid=#{@id}&contenttype=TRCK")
		defaults:
			duration: ''
		parse: (model)->
			data = model
			if data.is_deleted
				data.favourites.sort = +data.favourites.sort - 100000

			data.uniq = 'user-'+model.id

			data.optionsList = [
				{
					title: app.locale.options.addTPl
					class: 'add-to-playlist'
				},{
					title: app.locale.options.toArt
					class: 'options-open-artist'
				},{
					title: app.locale.options.toAlb
					class: 'options-open-album'
				}
			]

			return data

	class FavoriteTracks.Items extends Backbone.Collection
		model: FavoriteTracks.Item
		url: app.getRequestUrl("/compatibility/favourites/getitems","?userid=#{app.user.uid}&contenttype=TRCK&")
		parse: (collection)->
			return collection.tracks

		comparator: (item)->
			return -item.get('favourites').sort


	initializeTracks = ->
		if FavoriteTracks.isLoaded
			return FavoriteTracks.defer.promise()

		FavoriteTracks.isLoaded = true

		favoriteTL = new FavoriteTracks.Items();
		FavoriteTracks.defer = $.Deferred()
		fechTracks = favoriteTL.fetch();
		if fechTracks
			fechTracks.done (res)->
				if res.code is 0
					FavoriteTracks.List = favoriteTL
					FavoriteTracks.defer.resolveWith fechTracks,[FavoriteTracks.List]
				else
					FavoriteTracks.isLoaded = false
					FavoriteTracks.defer.rejectWith fechTracks, arguments

			fechTracks.fail ->
				FavoriteTracks.isLoaded = false
				FavoriteTracks.defer.rejectWith fechTracks,arguments

			return FavoriteTracks.defer.promise()



	API =
		getTracks: ->
			if FavoriteTracks.List is undefined
				return initializeTracks()
			defer = $.Deferred()
			setImmediate ->
				defer.resolve FavoriteTracks.List

			return defer.promise();



	app.reqres.setHandler 'get:favorite:tracks', ->
		return API.getTracks()


	do API.getTracks