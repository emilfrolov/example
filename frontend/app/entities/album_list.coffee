define ->
	AlbumList = {}

	class AlbumList.Item extends Backbone.Model
		urlRoot: ->
			return app.getRequestUrl("/compatibility/catalogue/getAlbum","?albumid=#{@id}")
		
		specifiedIdKey: 'albumid'
		parse: (model)->
			if model.album
				model.album.album = true
				return model.album
			else
				model.album = true
				return model

	class AlbumList.Items extends Backbone.Collection
		model: AlbumList.Item
		url: app.getRequestUrl("/compatibility/catalogue/getBeelineContent")
		parse: (collection)->
			return collection.items
		fetch: (opts)->
			genre = app.request 'get:local:genries'
			if genre.length
				opts.data = opts.data or {}
				opts.data.genre = genre.toJSON()

			Backbone.Collection.prototype.fetch.apply @,[opts]


	initializeNewAlbums = ->
		if AlbumList.NewListIsLoad
			return AlbumList.NewListDefer.promise()

		AlbumList.NewListIsLoad = true

		albums = new AlbumList.Items();
		AlbumList.NewListDefer = $.Deferred()
		fechAlbums = albums.fetch
			data:
				type: 'albums'
				sort: 'date'
				page: 1
				countitems: 60

		if fechAlbums
			fechAlbums.done ()->
				AlbumList.NewList = albums
				AlbumList.NewList.Type = 'new'
				AlbumList.NewListDefer.resolveWith fechAlbums,[AlbumList.NewList]
			fechAlbums.fail ->
				AlbumList.NewListIsLoad = false
				AlbumList.NewListDefer.rejectWith fechAlbums,arguments

			return AlbumList.NewListDefer.promise()

	initializeSingleAlbum = (id)->
		AlbumList.SingleAlbum = new AlbumList.Item
			id: id
		defer = $.Deferred()
		fechAlbum = AlbumList.SingleAlbum.fetch()

		if fechAlbum
			fechAlbum.done (res)->
				if res.code is 0
					AlbumList.AlbumId = id
					defer.resolveWith fechAlbum,[AlbumList.SingleAlbum]
				else
					defer.rejectWith fechAlbum,[res]
			fechAlbum.fail ->
				defer.rejectWith fechAlbum,arguments

			return defer.promise()



	API =
		getNewAlbums: ->
			if AlbumList.NewList is undefined
				return initializeNewAlbums()
			return AlbumList.NewList

		getAlbum: (id)->
			if AlbumList.AlbumId isnt id
				return initializeSingleAlbum id

			defer = $.Deferred()
			setImmediate ->
				defer.resolve AlbumList.SingleAlbum

			return defer.promise();



	app.reqres.setHandler 'get:new:albums', ->
		return API.getNewAlbums()

	app.reqres.setHandler 'get:single:album', (id)->
		return API.getAlbum id

	app.reqres.setHandler 'get:temp:albums', (collection)->
		if collection?
			AlbumList.TempAlbums = new AlbumList.Items collection.toJSON()

			if AlbumList.TempCollection?
				AlbumList.TempCollection.off 'reset'

			AlbumList.TempCollection = collection

			AlbumList.TempCollection.on 'reset', ->
				if AlbumList.TempCollection.Type is 'new'
					app.Main.Content.New.Controller.showAlbumsOne AlbumList.TempCollection
				else
					app.Main.Content.Recomend.Controller.showAlbumsOne AlbumList.TempCollection

		return AlbumList.TempAlbums

	app.reqres.setHandler 're:initialize:albums', ->
		if AlbumList.NewList
			AlbumList.NewList.fetch
				data:
					type: 'new'
					page: 1
					count: 60
				reset: true
