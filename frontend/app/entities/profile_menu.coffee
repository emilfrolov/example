define ->
    Menu = {}
    class Menu.Item extends Backbone.Model.Selected

    class Menu.Collection extends Backbone.Collection.SingleSelect
        model: Menu.Item

    initializeMenu = ->
        Menu.List = new Menu.Collection([
            {
                title: app.locale.profile.menu.main
                tab: 'main'
            },
            {
                title: app.locale.profile.menu.access
                tab: 'subscribe'
            },
            {
                title: app.locale.profile.menu.promocode
                tab: 'promo'
            },{
                title: app.locale.profile.menu.followers
                tab: 'followers'
            },{
                title: app.locale.profile.menu.followed
                tab: 'followed'
            }
        ]);




    API =
        getMenus: ->
            if Menu.List is undefined
                initializeMenu()
            return Menu.List



    app.reqres.setHandler 'get:profile:menu', ->
        return API.getMenus()

