define ['../config/custom_elements/track'], (CustomTrack)->
	TrackList = {}
	class TrackList.Item extends CustomTrack.Model
		parse: (item)->
			item.uniq = 'artist-'+item.id
			item.optionsList = [
				{
					title: app.locale.options.addTPl
					class: 'add-to-playlist'
				},{
					title: app.locale.options.toArt
					class: 'options-open-artist'
					artist: true
				},{
					title: app.locale.options.toAlb
					class: 'options-open-album'
				}
			]
			return item
		urlRoot: '/track'


	class TrackList.Items extends Backbone.Collection
		model: TrackList.Item
		url: app.getRequestUrl("/compatibility/catalogue/getBeelineContent")
		parse: (collection)->
			return collection.items

	initializeNewTracks = ->
		if TrackList.NewListIsLoad
			return TrackList.NewListDefer.promise()

		TrackList.NewListIsLoad = true

		trackList = new TrackList.Items();
		TrackList.NewListDefer = $.Deferred()
		fechTracks = trackList.fetch
			data:
				type: 'tracks'
				sort: 'date'
				page: 1
				countitems: 90

		if fechTracks
			fechTracks.done ()->
				TrackList.NewList = trackList
				TrackList.NewListDefer.resolveWith fechTracks,[TrackList.NewList]
			fechTracks.fail ->
				TrackList.NewListIsLoad = false
				TrackList.NewListDefer.rejectWith fechTracks,arguments

			return TrackList.NewListDefer.promise()



	API =
		getNewTracks: ->
			if TrackList.NewList is undefined
				return initializeNewTracks()
			return TrackList.NewList



	app.reqres.setHandler 'get:new:tracks', ->
		return API.getNewTracks()

	return TrackList
