define ->
  Charts = {}
  
  class Charts.Item extends Backbone.Model

  class Charts.List extends Backbone.Collection
    model: Charts.Item
    url: '/charts/list'
    parse: (collection)->
      return collection.charts

  initializeCharts = ->
    if Charts.ListIsLoad
      return Charts.ListDefer.promise()

    Charts.ListIsLoad = true

    feedList = new Charts.List
    Charts.ListDefer = $.Deferred()
    fetch = feedList.fetch()

    if fetch
      fetch.done ()->
        Charts.NewList = feedList
        Charts.ListDefer.resolveWith fetch,[Charts.NewList]
      fetch.fail ->
        Charts.ListIsLoad = false
        Charts.ListDefer.rejectWith fetch,arguments

      return Charts.ListDefer.promise()


  API =
    getCharts: ->
      if Charts.NewList is undefined
        return initializeCharts()
      return Charts.NewList



  app.reqres.setHandler 'get:charts', ->
    return API.getCharts()