define ->
	Feed = {}
	class Feed.Item extends Backbone.Model
		deleteUrl: ->
			return app.getRequestUrlSid("/beelineMusicV1/wall/deleteRecord","?recordId=#{@id}")
		parse: (item)->
			item.currentUser = Feed.currentUser.toJSON()
			return item

	class Feed.List extends Backbone.Collection
		model: Feed.Item
		url: app.getRequestUrl("/beelineMusicV1/wall/getAllRecords")
		parse: (collection)->
			return collection.wallRecords

		comparator: (a,b)->
			dateA = new Date(a.get('date_create')*1000)
			dateB = new Date(b.get('date_create')*1000)
			return if dateA < dateB then 1 else -1


	initializeFeed = ->
		# if Feed.ListIsLoad
		# 	return Feed.ListDefer.promise()

		Feed.ListIsLoad = true

		feedList = new Feed.List
		Feed.ListDefer = $.Deferred()
		app.request('get:user').done (user)->
			Feed.currentUser = user
			fetch = feedList.fetch
				limit: 30

			if fetch
				fetch.done ()->
					Feed.NewList = feedList
					Feed.ListDefer.resolveWith fetch,[Feed.NewList]
				fetch.fail ->
					Feed.ListIsLoad = false
					Feed.ListDefer.rejectWith fetch,arguments

		return Feed.ListDefer.promise()

	initializePostList = ->
		return Feed.PostList = new Backbone.Collection

	initializeNewModel = ->
		class model extends Backbone.Model
			defaults:
				message: ''

		return Feed.NewModel = new model



	API =
		getFeeds: (current)->
			# if Feed.NewList is undefined
			if current
				return Feed.NewList

			return initializeFeed()

		getFeedPostItems: ->
			if Feed.PostList is undefined
				return initializePostList()
			return Feed.PostList

		getNewFeedModel: ->
			if Feed.NewModel is undefined
				return initializeNewModel()
			return Feed.NewModel



	app.reqres.setHandler 'get:feeds', (current)->
		return API.getFeeds(current)

	app.reqres.setHandler 'get:feed:post:items', ->
		return API.getFeedPostItems()

	app.reqres.setHandler 'get:new:feed:model', ->
		return API.getNewFeedModel()
