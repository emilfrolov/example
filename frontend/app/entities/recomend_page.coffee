define ->
  Recomend = {}
  class Recomend.Main extends Backbone.Model
    urlRoot: app.getRequestUrl("/beelineMusicV1/main-page/getMainBlock")
    parse: (model)->
      if model.today_best?
        return model.today_best
      else
        return {
          isEmpty: true
        }
      
  class Recomend.Blocks extends Backbone.Model
    urlRoot: app.getRequestUrl("/beelineMusicV1/main-page/getAdditionalBlocks")
    parse: (model)->
      return model.blocks

  initializeMain = ->
    if Recomend.MainBlockIsLoad
      return Recomend.MainBlockDefer.promise()

    Recomend.MainBlockIsLoad = true

    main = new Recomend.Main();
    Recomend.MainBlockDefer = $.Deferred()
    fetchMain = main.fetch();

    fetchMain.done ->
      Recomend.MainBlock = main
      Recomend.MainBlockDefer.resolveWith fetchMain,[Recomend.MainBlock]


    fetchMain.fail ->
      Recomend.MainBlockDefer.rejectWith fetchMain,arguments
      Recomend.MainBlockIsLoad = false

    return Recomend.MainBlockDefer.promise()
    
  initializeBlocks = ->
    if Recomend.BlocksIsLoad
      return Recomend.BlocksDefer.promise()

    Recomend.BlocksIsLoad = true

    blocks = new Recomend.Blocks();
    Recomend.BlocksDefer = $.Deferred()
    fetch = blocks.fetch();

    fetch.done ->
      Recomend.BlocksItem = blocks
      Recomend.BlocksDefer.resolveWith fetch,[Recomend.BlocksItem]


    fetch.fail ->
      Recomend.BlocksDefer.rejectWith fetch,arguments
      Recomend.BlocksIsLoad = false

    return Recomend.BlocksDefer.promise()





  API =
    getMain: ->
      if Recomend.MainBlock is undefined
        return initializeMain()
      return Recomend.MainBlock

    getBlocks: ->
      if Recomend.BlocksItem is undefined
        return initializeBlocks()
      return Recomend.BlocksItem

  app.reqres.setHandler 'get:recomend:main', ->
    return API.getMain()

  app.reqres.setHandler 'get:recomend:blocks', ->
    return API.getBlocks()
