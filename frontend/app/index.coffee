define ['./parts/player'], (Player)->
    require('../styles/app/style.styl')
    require('./config/setting/backbone_model_methods.coffee')
    require('./config/setting/route_setting.coffee')
    require('./config/setting/sync_rewrite.coffee')
    require('./config/helpers/filtered_collection.coffee')
    require('./config/model/select.coffee')
    require('./pages')

    app.addRegions
        content: '#main'
        video: '#video-container'
        player: '#player'
        sidebar: '#sidebar'
        notification: '#notification'
        popups: '#main-popups'

    app.host = 'http://cdn.music.beeline.ru/api'

    app.getRequestUrl = (url,query='')->
        return "#{app.host}#{url}/#{query}"

    app.getRequestUrlSid = (url,query)->
        query = if query then "#{query}&sid=#{app.user.sid}" else "?sid=#{app.user.sid}"
        return "#{app.host}#{url}/#{query}"

    app.navigate = (route,options)->
        options or (options = {})
        route = "/#{app.locale.lang}"+route
        Backbone.history.navigate route,options

    app.readCookie = (name)->
        nameEQ = name + "=";
        ca = document.cookie.split(';');
        if ca.length is 0
            return null

        for i in [0..ca.length-1]
            c = ca[i];
            while c.charAt(0)==' '
                c = c.substring(1,c.length);

            if c.indexOf(nameEQ) == 0
                return c.substring(nameEQ.length,c.length);

        return null;

    app.getCurrentRoute = ()->
        return Backbone.history.fragment

    app.RandInt = (low_limit, high_limit)->
       randoma = Math.round(Math.random()*(high_limit - low_limit)) + low_limit;
       return randoma;

    app.createImgLink = (link,size,blur,ro)->
        if link
            ro = if ro then '_ro' else '_cr'
            if blur
                return link.replace(/([^\/]+)\.([^\/]+)$/,size+ro+'_b0x7.$2')
            else
                return link.replace(/([^\/]+)\.([^\/]+)$/,size+ro+'.$2')
        else
            return '/img/no-onimg.png'

    app.parsePath = (path)->
        if path?
            arr = path.split '&'
            res = {}
            for val in arr
                val = val.split '='
                res[val[0]] = val[1]
            return res
        else
            throw 'Параметр path не должен быть пустым'

    app.isInfavorite = (id)->
        favoriteTracks = app.request 'get:favorite:tracks'
        
        return favoriteTracks.some (item)->
            return item.get('id') is id

    app.checkActiveTrack = ->
        currentHref = Backbone.history.location.href
        if currentHref is app.playedHref
            $("##{app.playedId}").addClass 'active '+Player.Controller.playerClassStatus

    app.checkActiveAlbum = ->
        $("##{app.playedAlbum}").addClass 'active '+Player.Controller.playerClassStatus

    app.declination = (iNumber, aEndings)->
        # aEndings - ['яблоко', 'яблока', 'яблок']
        iNumber = iNumber % 100;
        if (iNumber>=11 && iNumber<=19)
            sEnding=aEndings[2];
        else
            i = iNumber % 10;
            switch i
                when 1 
                    sEnding = aEndings[0]; 
                    break;
                when 2
                    sEnding = aEndings[1];
                when 3
                    sEnding = aEndings[1];
                when 4 
                    sEnding = aEndings[1];
                    break;
                else
                    sEnding = aEndings[2];
        return sEnding


    app.GAPageAction = (cat,subcat)->
        user = app.request 'get:user'
        if user.promise
            user.done (user)->
                _gaq.push(['_trackEvent', cat, subcat, user.get('phone')])

        else
            _gaq.push(['_trackEvent', cat, subcat, user.get('phone')])

    app.readCookie = (name)->
        nameEQ = name + "=";
        ca = document.cookie.split(';');
        if ca.length is 0
            return null

        for i in [0..ca.length-1]
            c = ca[i];
            while c.charAt(0)==' '
                c = c.substring(1,c.length);

            if c.indexOf(nameEQ) == 0
                return c.substring(nameEQ.length,c.length);

        return null;

    app.getReferer = ->
        url = location.href

        if ~url.indexOf('/feed')
            return 'wall'
        else if ~url.indexOf('/search')
            return 'search'
        else if ~url.indexOf('/profile')
            return 'profile'
        else if ~url.indexOf('/new')
            return 'new'
        else if ~url.indexOf('/playlists')
            return 'publicPlaylists'
        else if ~url.indexOf('/tracks')
            return 'myTracks'
        else if ~url.indexOf('/artist')
            return 'artist'
        else if ~url.indexOf('/album')
            return 'release'
        if ~url.indexOf('/')
            return 'recommended'

    app.getUserInfo = ->
        return app.user

    clearPage = ->
        $('.track-item.active').removeClass 'active play pause'
        $('.album-item.active').removeClass 'active play pause'

    app.on 'play', ->
        currentHref = Backbone.history.location.href
        do clearPage
        $("##{app.playedAlbum}").addClass 'active pause'
        if currentHref is app.playedHref
            $("##{app.playedId}").addClass 'active pause'


    app.on 'pause', ->
        currentHref = Backbone.history.location.href
        do clearPage
        $("##{app.playedAlbum}").addClass 'active play'
        if currentHref is app.playedHref
            $("##{app.playedId}").addClass 'active play'



    app.on 'start', ()->
        Entities = require.context('./entities', false);

        Entities.keys().forEach (path)->
            Entities path

        app.logger = require('./config/log')
        do app.logger.sendNewLog
        app.logger.createNewLog app.getRequestUrlSid('/generalV1/log/enter')

        if Backbone.history
            Backbone.history.start {pushState: true}

        Sidebar = require('./parts/sidebar')
        Sidebar.Controller.show()
        # do app.Parts.Sidebar.Controller.show

        $(document).on 'mousewheel','.scrollable', (e)->
            e = e or window.event;
            parentHeight = $(this).parent().height();
            toTop = parseInt($(this)[0].style.marginTop) or 0;
            scrollHeight = $(this).height() - parentHeight;
            scrollStep = toTop + (e.deltaY * 30);
            if scrollHeight > 0
                if scrollStep > 0
                    scrollStep = 0;
                else if -scrollStep > scrollHeight
                    scrollStep = -scrollHeight;

                $(this).css('margin-top':scrollStep+'px');
                return false

        $(document).on 'click','.player-start-element',->
            $('.track-item').not('.disable').eq(0).click()


        if !window.setImmediate
            window.setImmediate = do =>
                head = { }
                tail = head

                ID = Math.random()

                onmessage = (e)->
                    if e.data != ID
                        return
                    head = head.next
                    func = head.func
                    delete head.func
                    func()

                if window.addEventListener
                    window.addEventListener('message', onmessage);
                else
                    window.attachEvent( 'onmessage', onmessage );


                return (func)->
                    tail = tail.next = { func: func };
                    window.postMessage(ID, "*")
        # do app.Parts.UserNotification.Controller.startListenNotify

    app.on 'error', ->
        console.log 'errror'

    return app