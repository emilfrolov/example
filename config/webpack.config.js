'use strict';
var fs = require('fs');
var nib = require('nib');
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var del = require('del');
var config = {
  assetVersioning: 'query'
}

var processEnv = process.env.NODE_ENV || "production"

var noProcessModulesRegExp = /node_modules\/(angular|prismjs)/;

function extHash(name, ext, hash) {
  if (!hash) hash = '[hash]';
  return config.assetVersioning == 'query' ? `${name}.${ext}?${hash}` :
    config.assetVersioning == 'file' ? `${name}.${hash}.${ext}` :
      `${name}.${ext}`;
}

module.exports = {
  
    output: {
      path:       path.join(process.cwd(), './public/pack'),
      // path as js sees it
      // if I use another domain here, need enable Allow-Access-.. header there
      // and add  to scripts, to let error handler track errors
      publicPath: '/pack',
      // в dev-режиме файлы будут вида [name].js, но обращения - через [name].js?[hash], т.е. версия учтена
      // в prod-режиме не можем ?, т.к. CDN его обрезают, поэтому [hash] в имени
      //  (какой-то [hash] здесь необходим, иначе к chunk'ам типа 3.js, которые генерируются require.ensure,
      //  будет обращение без хэша при загрузке внутри сборки. при изменении - барузерный кеш их не подхватит)
      filename:   extHash("/scripts/[name]", 'js'),

      chunkFilename: extHash("/scripts/chunks/[name]/[id]", 'js')
      // library:       '[name]'
    },

    cache: processEnv == 'development',

    watchOptions: {
      aggregateTimeout: 10
    },

    watch: processEnv == 'development',

    devtool: processEnv == 'development' ? "cheap-module-inline-source-map" : null,

    profile: true,

    entry: {
      auth: './frontend/auth',
      app: './frontend/app',
      promo: './frontend/promo',
      wave: './frontend/wave',
      wave_mobile: './frontend/wave_mobile',
      shazam: './frontend/shazam'
    },

    module: {
      loaders: [
        {
          test:   /\.jade$/,
          loader: "jade"
        },
        {
          test:    /\.coffee$/,
          loaders: ['coffee']
        },
        {
          test:   /\.css$/,
          // ExtractTextPlugin breaks HMR for CSS
          loader: ExtractTextPlugin.extract('css!autoprefixer?browsers=last 2 version')
          //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
        },
        {
          test:   /\.styl$/,
          // ExtractTextPlugin breaks HMR for CSS
          loader: ExtractTextPlugin.extract('css!autoprefixer?browsers=last 2 version!stylus?linenos=true&resolve url=true')
          //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
        },
        {
          test:   /\.scss$/,
          // ExtractTextPlugin breaks HMR for CSS
          // loader: 'style!css!autoprefixer?browsers=last 2 version!resolve-url!sass?sourceMap'
          loader: ExtractTextPlugin.extract('css!autoprefixer?browsers=last 2 version!resolve-url!sass?sourceMap')
          //loader: 'style!css!autoprefixer?browsers=last 2 version!stylus?linenos=true'
        },
        {
          test:   /\.(png|jpg|gif|woff|eot|otf|ttf|svg)$/,
          loader: extHash('file?name=/files/[hash]', '[ext]')
        }
      ],
      noParse: [
        // regexp gets full path with loader like
        // '/js/javascript-nodejs/node_modules/client/angular.js'
        // or even
        // '/js/javascript-nodejs/node_modules/6to5-loader/index.js?modules=commonInterop!/js/javascript-nodejs/node_modules/client/head/index.js'
        {
          test: function(path) {
            //console.log(path);
            return noProcessModulesRegExp.test(path);
          }
        }
      ]
    },

    stylus: {
      use: [nib()],
      import: ['~nib/lib/nib/index.styl',path.join(process.cwd(),'./frontend/styles/app/functions.styl')]
    },

    resolve: {
      // allow require('styles') which looks for styles/index.styl
      extensions: ['.js','.coffee', '', '.styl','controller.coffee','.scss'],
      alias:      {
      }
    },

    resolveLoader: {
      modulesDirectories: ['node_modules'],
      moduleTemplates:    ['*-loader', ''],
      extensions:         ['.js', '']
    },

    node: {
      fs: 'empty'
    },

    plugins: [
      new ExtractTextPlugin(extHash('/styles/[name]', 'css', '[contenthash]'), {allChunks: true})
    ]
  };


//if (processEnv != 'development') { // production, ebook
  if (processEnv == 'production') { // production, ebook
    module.exports.plugins.push(
      // function clearBeforeRun() {
      //   function clear(compiler, callback) {
      //     // del.sync(this.options.output.path + '/*');
      //     // callback();
      //   }

      //   // in watch mode this will clear between partial rebuilds
      //   // thus removing unchanged files
      //   // => use this plugin only in normal run
      //   this.plugin('run', clear);
      // },

      /* jshint -W106 */
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          // don't show unreachable variables etc
          warnings:     false,
          drop_console: true,
          unsafe:       true,
          screw_ie8:    true
        },
        output:   {
          indent_level: 0 // for error reporting, to see which line actually has the problem
          // source maps actually didn't work in Qbaka that's why I put it here
        }
      })
    );
  }
