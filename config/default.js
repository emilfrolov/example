module.exports = {
  // secret data can be moved to env variables
  // or a separate config
  secret: 'music-secret-tema-auth-secter',
  root: process.cwd(),
  apiHost: 'http://cdn.music.beeline.ru/api',

  server: {
    host: '0.0.0.0',
    port: 10100
  },
  redis: {
    host: 'localhost'
  },
  graylog: {
    host: '10.0.0.8',
    port: 12201
  },
  socket: {
    port: 8763
  }
};