const gulp = require('gulp');
const path = require('path');
const fs = require('fs');
const assert = require('assert');
const runSequence = require('run-sequence');

process.on('uncaughtException', function(err) {
  console.error(err.message, err.stack, err.errors);
  process.exit(255);
});


function lazyRequireTask(path) {
  var args = [].slice.call(arguments, 1);
  return function(callback) {
    var task = require(path).apply(this, args);

    return task(callback);
  };
}


gulp.task("nodemon", lazyRequireTask('./tasks/nodemon'));

gulp.task('client:webpack:build', lazyRequireTask('./tasks/webpack'));
gulp.task('client:vendor:build', lazyRequireTask('./tasks/vendor-build'));

gulp.task('client:git:push', lazyRequireTask('./tasks/git-push',process.env.cName));

gulp.task('client:webpack', function (callback) {
  runSequence("client:vendor:build","client:webpack:build",callback)
});

gulp.task('client:deploy', function (callback) {
  runSequence("client:webpack","client:git:push",callback);
})

gulp.on('err', function(gulpErr) {
  if (gulpErr.err) {
    // cause
    console.error("Gulp error details", [gulpErr.err.message, gulpErr.err.stack, gulpErr.err.errors].filter(Boolean));
  }
  mongoose.disconnect();
});

