'use strict';
var logger = require('logs');

module.exports = function*(next) {

  try {
    yield * next;
  } catch (e) {

    this.set('X-Content-Type-Options', 'nosniff');

    let preferredType = this.accepts('html', 'json');

    if (e.status) {
      this.status = e.status;

      logger.error(`${e.status}: ${e.message}`)

      // could use template methods to render error page
      if (preferredType == 'json') {
        this.body = {
          error: e.message
        };
      } else {
        this.body = e.message;
      }

    } else if (e.name == 'ValidationError') {

      this.status = 400;

      var errors = {};

      logger.error(`400: ${e.message}`);

      for (var field in e.errors) {
        errors[field] = e.errors[field].message;
      }

      if (preferredType == 'json') {
        this.body = {
          errors: errors
        };
      } else {
        this.body = "Некорректные данные.";
      }

    } else {
      logger.error(`500: ${e.message}`);
      this.body = e.stack;
      this.status = 500;
      console.error(e.message, e.stack);
    }

  }
};