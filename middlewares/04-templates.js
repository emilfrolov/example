'use strict';
var jade = require('jade');
var config = require('config');
var path = require('path');

module.exports = function* (next) {
    this.render = function (view,options) {
        return jade.renderFile(path.join(config.root,`views/${view}.jade`),options);
    }

    yield* next
}