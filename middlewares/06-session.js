// in-memory store by default (use the right module instead)
var session = require('koa-generic-session'),
    sessionRedis = require('koa-redis'),
    config = require('config'),
    log = require('logs');

module.exports = session({
    name: 'dsfldjfsdlf',
    secret: "kjds2f43h*hds3",
    rolling: true,
    saveUninitialized: true,
    errorHandler: (err,type,ctx)=>{
        log.error(err);
    },
    cookie: {
        maxAge: 100000 * 60 * 60 * 24
    },
    store: sessionRedis({
        host: config.redis.host
    })
});