var staticCache = require('koa-static-cache'),
    path = require('path'),
    config = require('config');

module.exports = staticCache(path.join(config.root, 'public'), {
  maxAge: 365 * 24 * 60 * 60
  
});