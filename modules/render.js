var jade = require('jade'),
    config = require('config'),
    path = require('path'),

    views = {};

exports.init = function (view) {
    if (!views[view]) {
        views[view] = jade.compileFile(path.join(config.root,`views/${view}.jade`));
    }
}

exports.draw  = function (view,locals) {
    if (views[view]) {
        return views[view](locals);
    } else {
        return jade.renderFile(path.join(config.root,`views/${view}.jade`),locals);
    }
}