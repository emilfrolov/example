var config = require('config'),
    path = require('path'),
    jade = require('render');
    
    jade.init('auth');
    
module.exports.checkAuth = function* (next) {
    if (!this.session.sid) {
        var locale = require('locale.json');
        var lang = locale[this.params.lang];
        var view = jade.draw('auth',{lang: lang});
        this.status = 200;
        this.body = view;
        return;
    }
    yield * next
}
module.exports.checkLocale = function* (next) {
    if (this.request.get('X-Requested-With').toLowerCase() != 'xmlhttprequest'){
        var locale = require('locale.json');
        var segments = this.request.url.split('/')

        if (segments[1] == '') {
            return this.redirect('/ru');
        } else {
            var firstSegment = segments[1];
            var redirect = true;
            for (var key in locale) {
                if (key == firstSegment) {
                    redirect = false;
                }
            }
            var redirectUrl = this.request.url.replace(/\/(\w+)/, '/ru/$1')
            
            if(redirect) {
                return this.redirect(redirectUrl);
            }    
        }
    }

    yield * next;
}