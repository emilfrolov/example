'use strict'
module.exports = function (server) {
    var Redis = require('ioredis'),
        // RedisAdapter = require('socket.io-redis'),
        config = require('config'),
        request = require('co-request'),
        io = require('socket.io')(config.socket.port),
        log = require('logs'),
        co = require('co'),
        socketClients = {};

        // adapter = redis({ host: config.redis.host });

        // adapter.pubClient.on('error', function(err){
        //     console.log(err);
        // });
        // adapter.subClient.on('error', function(err){
        //     console.log(err);
        // });

        // io.adapter(initRedisAdapter(6379,config.redis.host));
        // io.set('origins', '*:*');
        // function initRedisAdapter(port,host) {
        //   var pub = redis.createClient(port,host,{detect_buffers: true});
        //   pub.on('error',onRedisError);
        //   var sub = redis.createClient(port,host,{detect_buffers: true});
        //   sub.on('error',onRedisError);
        //   var redisAdapter = RedisAdapter({pubClient: pub,
        //                             subClient: sub,
        //                             key: 'your key'});
        //   redisAdapter.prototype.on('error',onRedisError);
        //   function onRedisError(err){
        //     log.error("Redis error: ",err);
        //   }
        //   return redisAdapter;
        // }

    io.on('connection', (socket)=>{
        let query = socket.handshake.query;
        
        if (query.userSid) {
            co(function* () {
                let user = yield request({
                    url: `${config.apiHost}/compatibility/user/getBySid?sid=${query.userSid}`,
                    json: true
                });

                if (user.body.code == 0) {
                    let userData = user.body.user,
                        usersInRoom = io.sockets.adapter.rooms[userData.user_id] ? Object.keys(io.sockets.adapter.rooms[userData.user_id]).length : 0
                    if (typeof(usersInRoom) != 'undefined' && usersInRoom < 200) {
                        socket.on('startPlay', ()=>{
                            // this.in()
                            socket.in(userData.user_id).emit('stopPlay');
                        })
                        socket.join(userData.user_id,(err)=>{
                            if(err){
                                log.error(err);
                            }
                        });

                        socket.on('disconnect', function(){
                            socket.leave(userData.user_id,(err)=>{
                                if(err){
                                    log.error(err);
                                }
                            });
                        });
                    } else {
                        socket.emit('closeConn')
                    }
                } else {
                    socket.emit('closeConn')
                }
            })
        } else {
            socket.emit('closeConn')
        }
    });

    io.on('error', (err)=>{
        log.error(err);
    })

    // List the sentinel endpoints
    var endpoints = [
        {host: '10.0.0.13', port: 26379},
        {host: '10.0.0.14', port: 26380}
    ];

    var redis = new Redis({
      sentinels: endpoints,
      name: 'mymaster'
    });

    redis.on('connect', function () {
        console.log('Redis connect success')
        redis.subscribe('frontendPush.notification.web');
    });
    redis.on('message', function (channel,message) {
        try {
            var message = JSON.parse(message),
                eventName = message[1],
                data = message[2],
                resivers = message[3];
            if (resivers) {
                for (var i = 0, leng = resivers.length; i < leng; i++) {
                    io.in(resivers[i]).emit('event', {data: data, eventName: eventName});
                }
            } else {
                io.emit('broadcastEvent', {data: data, eventName: eventName})
            }

        } catch (e) {
            log.error(e);
        }
    })

    return io;
};