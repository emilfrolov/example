'use strict';

let mongoose = require('mongoose');

// mongoose.set('debug',true);

mongoose.connect('mongodb://localhost/koa-project', {
    server: {
        soketOptions: {
            keepAlive: 1
        },
        poolSize: 5
    }
});

module.exports = mongoose;