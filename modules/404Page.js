'use strict'
var request = require('co-request'),
    config = require('config');

module.exports = function* (next) {
    let preferredType = this.accepts('html', 'json');
    if (this.status == 404) {
        if (preferredType == 'html') {
            this.status = 404;
            this.body = this.render('404');
        }
    }

    yield * next;
}