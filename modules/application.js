if (process.env.TRACE) {
    require('trace');
}

var app = require('koa')(),
    config = require('config'),
    log = require('logs'),
    keyGrip = require('keygrip'),
    fs = require('fs'),
    path = require('path'),
    middlewares = fs.readdirSync(path.join(__dirname, '../middlewares')).sort();

// if (process.env.NODE_ENV == 'production') {
    var io = require('socket-server')();
// }

middlewares.forEach(function (middleware) {
    app.use(require('../middlewares/' + middleware));
});


app.keys = new keyGrip(['im a newer music front secret of beemusic', 'i like secret turtle frontend of bee music'], 'sha256');;


app.on('error', function (err) {
    log.error(err);
});

module.exports = app;