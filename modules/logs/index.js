var winston = require('winston'),
    WinstonGraylog2 = require('winston-graylog2'),
    config = require('config'),
    options = {
      name: 'Graylog',
      level: 'debug',
      silent: false,
      handleExceptions: false,
      graylog: {
        servers: [{host: config.graylog.host, port: config.graylog.port}],
        facility: 'njs.frontend',
        bufferSize: 1400
      }
    },
    logger = new(winston.Logger)({
        exitOnError: false,
        transports: [
            new(WinstonGraylog2)(options),
            new(winston.transports.Console)(),
            new(winston.transports.File)({
                filename: './modules/logs/logs.log',
                level: 'debug'
            })
        ]
      });
    
module.exports = logger