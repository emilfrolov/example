'use strict'
var app = require('application'),
    Router = require('koa-router'),
    filters = require('filters'),
    router = new Router,
    mainRouter = new Router({
        prefix: '/:lang(.{2})?'
    }),
    controller = require('./controller'),
    jade = require('render');

jade.init('main');

var mainPagePattern = 'playlist/.*|playlists|track/.*|search/.*|new|video|charts|profile.*|artist/.*|album/.*|user/.*|feed|id.*'
    mainRouter.get('/', filters.checkLocale, filters.checkAuth, controller.showMain);
    mainRouter.get('/locale', filters.checkLocale, controller.getLocale);
    mainRouter.get('/userInfo', filters.checkLocale, controller.getUserInfo);
    mainRouter.get(`/(${mainPagePattern})`, filters.checkLocale, filters.checkAuth, controller.showMain);
    router.get('/exit', controller.exit);
    router.get('/clips',controller.getYTClips)
    router.get('/artistVideo',controller.getArtistClips)
    router.get('/wave', controller.wave)
    router.get('/cpa', controller.cpa)
    router.get('/cpa/auth', controller.cpaAuth)
    router.get('/cpa/get', controller.cpaTokenGet)
    router.get('/shazam', controller.shazam)
    router.get('/promo', controller.promo)
    router.get('/promo/auth', controller.promoStartSubscribe)
    router.get('/promo/services', controller.promoSubscribe)
    router.get('/promo/congratulation', controller.promoSubscribeSuccess)
    router.get('/social/auth/:type([A-z]{2})' ,controller.getRedirectLink)
    router.get('/social/auth/check', controller.authSocialUser);
    router.get('/faq', controller.showFaq);
    router.get('/oferta', controller.showOferta);
    router.get('/video/get/:name', controller.getStaticVideo);

app.use(router.routes());
app.use(mainRouter.routes());