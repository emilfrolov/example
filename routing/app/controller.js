'use strict'
var config = require('config'),
    path = require('path'),
    md5 = require('md5'),
    promo = require('promo'),
    jade = require('render'),
    request = require('co-request'),
    url = require('url'),
    fs = require('fs'),
    util = require('util');

module.exports = {
    showMain: function* () {
        this.status = 200;
        var locale = require('locale.json');
        var lang = locale[this.params.lang];
        var user = {
            sid: this.session.sid,
            uid: this.session.uid
        }
        
        var sid = this.session.sid ? md5(this.session.sid) : new Date
        this.body = jade.draw('main',{sid: sid, lang: lang, user: user})
    },
    getLocale: function* () {
        this.status = 200;
        var root = this.request.query.root || 'window'
        var locale = require('locale.json');
        this.body = root+'.locale = '+ JSON.stringify(locale[this.params.lang])
    },
    getUserInfo: function* () {
        this.status = 200;
        var root = this.request.query.root || 'window'
        var user = {
            sid: this.session.sid,
            uid: this.session.uid
        }
        this.body = root+'.user = '+ JSON.stringify(user)
    },
    getYTClips: function* (next) {
        this.status = 200;
        let token = this.request.query.token,
            url;
        if (token) {
            url = `https://www.googleapis.com/youtube/v3/playlistItems?part=id%2Csnippet&maxResults=48&playlistId=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&pageToken=${token}&fields=items(id%2Csnippet)%2CnextPageToken&key=AIzaSyAgjf3HwyiywzGzaIZ-I5W8672D6pIlLUs`;
        } else {
            url = `https://www.googleapis.com/youtube/v3/playlistItems?part=id%2Csnippet&maxResults=48&playlistId=PLFgquLnL59alCl_2TQvOiD5Vgm1hCaGSI&fields=items(id%2Csnippet)%2CnextPageToken&key=AIzaSyAgjf3HwyiywzGzaIZ-I5W8672D6pIlLUs`
        }
        let res = yield request({
            url: url,
            json: true
        });
        
        this.body = res.body;
        yield * next;
    },
    getArtistClips: function* (next) {
        this.status = 200;
        let q = encodeURIComponent(this.request.query.q),
            url = `https://www.googleapis.com/youtube/v3/search?safeSearch=strict&videoDefinition=high&order=viewCount&part=snippet&q=${q}&videoLicense=youtube&type=video&videoCategoryId=10&maxResults=50&key=AIzaSyAgjf3HwyiywzGzaIZ-I5W8672D6pIlLUs`;

        let res = yield request({
            url: url,
            json: true
        });

        this.body = res.body.items;
        yield * next;
    },
    exit: function* (next) {
        delete this.session.sid;
        this.status = 200;
        this.body = {
            status: 'ok',
            code: 0
        }
        yield * next;
    },
    shazam: function* () {
        var userAgent = this.get('user-agent');
        if (userAgent.match(/Android/i)) {
            this.status = 200;
            this.body = this.render('landings/shazam',{device: "android"});
        } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
            this.status = 200;
            this.body = this.render('landings/shazam',{device: "ios"});
        } else {
            return this.redirect('http://music.beeline.ru')
        }
    },
    wave: function* (next) {
        var userAgent = this.get('user-agent');
        promo.setGaEvent.call(this);
        var FromParam = parseInt(this.request.query.from || -1);

        if (FromParam == 15) {
            return this.redirect("https://itunes.apple.com/ru/app/bilajn.volna/id1024976971?mt=8&ign-mpt=uo%3D4");
        } else if (FromParam == 16) {
            return this.redirect("https://play.google.com/store/apps/details?id=com.beeline.wave");
        }

        // if (userAgent.match(/Android/i)) {
        //     let Urls = promo.waveAndroidAppUrls;
        //     let UrlsLength = Urls.length;

        //     if (Urls[FromParam-1]) {
        //         return this.redirect(Urls[FromParam-1]);
        //     } else {
        //         return this.redirect("https://play.google.com/store/apps/details?id=com.beeline.wave");
        //     }
        // } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
        //     let Urls = promo.waveIosAppUrls;
        //     let UrlsLength = Urls.length;

        //     if (Urls[FromParam-1]) {
        //         return this.redirect(Urls[FromParam-1]);
        //     } else {
        //         return this.redirect("https://itunes.apple.com/ru/app/bilajn.volna/id1024976971?mt=8&ign-mpt=uo%3D4");
        //     }
        // } else {
        //     this.body = this.render('landings/wave');
        // }



        if (userAgent.match(/Android/i)) {
            let Urls = promo.waveAndroidAppUrls;
            let UrlsLength = Urls.length;

            // if (Urls[FromParam-1]) {
            //     return this.redirect(Urls[FromParam-1]);
            // } else {
            //     return this.redirect("https://play.google.com/store/apps/details?id=com.beeline.wave");
            // }
            this.body = this.render('landings/wave_mobile');
        } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
            let Urls = promo.waveIosAppUrls;
            let UrlsLength = Urls.length;

            // if (Urls[FromParam-1]) {
            //     return this.redirect(Urls[FromParam-1]);
            // } else {
            //     return this.redirect("https://itunes.apple.com/ru/app/bilajn.volna/id1024976971?mt=8&ign-mpt=uo%3D4");
            // }
            this.body = this.render('landings/wave_mobile');
        } else {
            this.body = this.render('landings/wave');
        }
        
    },
    promo: function* (next) {
        let ip = promo.getIp.call(this),
            isBeelineUser = promo.isBeelineUser(ip);
        this.status = 200;
        this.body = this.render('landings/promo',{beelineUser: isBeelineUser,ip: promo.getIp.call(this)});
    },
    cpa: function* (next) {
        var userAgent = this.get('user-agent');
        if (!userAgent.match(/Android/i) && !userAgent.match(/iPhone|iPad|iPod/i)) {
            this.redirect('http://music.beeline.ru/promo')
        } else {
            this.status = 200;
            let reqUrl = url.parse(this.url),
                search = reqUrl.search || '',
                redirectUrl = `/cpa/auth/${search}`
            this.body = this.render('landings/cpa',{redirectUrl: redirectUrl});
        }
    },
    cpaAuth: function* (next) {
        this.status = 200;
        let campaignId = this.query.campaign_id || '',
            promoChannel = this.query.promo_channel || '',
            leadId = this.query.lead_id || '',
            userAgent = this.get('user-agent'),

        resUrl = `${config.apiHost}/beelineMusicV1/partner/getToken?campaignId=${campaignId}&promoChannel=${promoChannel}&leadId=${leadId}`,
        res = yield request({
            url: resUrl,
            json: true
        });

        if (res.body.code == 0) {
            this.session.cpaToken = res.body.token;

            var androidRUrl = 'https://play.google.com/store/apps/details?id=ru.beeonline.music&referrer=utm_content%3D'+res.body.token
        } else {
            var androidRUrl = 'https://play.google.com/store/apps/details?id=ru.beeonline.music'
        }

        if (userAgent.match(/Android/i)) {
            return this.redirect(androidRUrl);
        } else if (userAgent.match(/iPhone|iPad|iPod/i)) {
            return this.redirect("https://itunes.apple.com/ru/app/bilajn.muzyka/id702770268?mt=8");
        } else {
            return this.redirect('http://music.beeline.ru')
        }
    },
    cpaTokenGet: function* () {
        this.status = 200;
        let token = this.session.cpaToken || '';
        return this.redirect('beelinemusic://?cpa_token='+token);
    },
    promoSubscribe: function* (next) {
        this.status = 200;
        promo.subscribeStartAction.call(this);
    },
    promoStartSubscribe: function* (next) {
        this.status = 200;
        promo.servicesAction.call(this);
    },
    promoSubscribeSuccess: function* (next) {
        this.status = 200;
        this.body = this.render('landings/promo-congratulation');
    },
    getRedirectLink: function* (next) {
        var sName = this.params.type,
            redirectUrl = this.request.query.redirectUrl ? "&authProceedUrl="+this.request.query.redirectUrl : "",
            url = `${config.apiHost}/beelineMusicV1/social/auth?serviceName=${sName}&redirect=false${redirectUrl}`,
            res = yield request({
                url: url,
                json: true
            });
        if (res.body.redirect_url) {
            return this.redirect(res.body.redirect_url);
        } else {
            this.status = 404;
            this.body = {
                error: 'Redirect Url is undefined'
            }
        }
    },
    authSocialUser: function* (next) {
        if (this.request.query.serviceName && this.request.query.token) {
            var check = yield * getProceedResponse.call(this);

            if (check.code == 0) {
                this.status = 200;
                if (check.social_login == 'fail') {
                    check.token = JSON.stringify(check.token);
                    check.userData = JSON.stringify(check.userData);
                }
                this.body = this.render('authCheckPage', check);
            } else {
                this.status = 500;
                this.body = 'Произошла ошибка';
            }

        } else {
            this.status = 501;
            this.body = 'переданы не все параметры';
        }
    },
    showFaq: function* () {
        this.status = 200;
        this.body = this.render('faq');
    },
    showOferta: function* () {
        this.status = 200;
        this.body = this.render('oferta');
    },
    getStaticVideo: function* () {
        // var video = yield fs.readFile(path.join(config.root, 'public/packages/wave/images/volna_showreel.mp4'))
        // this.set('content/type', 'video/mp4')
        // video.pipe(this.response)
        // console.log(this.response)
        // this.body = video
        // this.body = this.params.name;




        // var filePath = path.join(config.root, 'public/packages/wave/images/volna_showreel.mp4');
        // var stat = fs.statSync(filePath);
        // var total = stat.size;

        // if (this.get('range')) {
        //     var range = this.get('range');
        //     var parts = range.replace(/bytes=/, "").split("-");
        //     var partialstart = parts[0];
        //     var partialend = parts[1];

        //     var start = parseInt(partialstart, 10);
        //     var end = partialend ? parseInt(partialend, 10) : total-1;
        //     var chunksize = (end-start)+1;

        //     var file = fs.createReadStream(filePath, {start: start, end: end});
        //     this.status = 206;
        //     this.set({ 'Content-Range': 'bytes ' + start + '-' + end + '/' + total, 'Accept-Ranges': 'bytes', 'Content-Length': chunksize, 'Content-Type': 'video/mp4' });
        //     this.body = file
        // } else {
        //     console.log('ALL: ' + total);
        //     this.status = 200;
        //     this.set({ 'Content-Length': total, 'Content-Type': 'video/mp4' });
        //     var file = fs.createReadStream(filePath);
        //     this.body = file;
        // }
    }
}

function* getProceedResponse() {
    var sName = this.request.query.serviceName,
        token = this.request.query.token;

    var url = `${config.apiHost}/beelineMusicV1/social/authProceed?serviceName=${sName}&token=${token}`,

        res = yield request({
            url: url,
            json: true
        });

    if (res.body.code == 0) {
        if (res.body.social_login == "fail") {
            var userData = yield * getSocialUserData.apply(this,[res.body.token.access_token,sName]);
            return {
                'status': 'ok',
                'code': 0,
                'social_login': 'fail',
                'token': res.body.token,
                'sName': sName,
                'userData': userData
            };
        } else {
            this.session.sid = res.body.user.sid; 
            this.session.uid = res.body.user.user_id;
            this.session.user = res.body.user;
            this.cookies.set('sid', res.body.user.sid,{domain: '.beeline.ru'});
            this.cookies.set('userId', res.body.user.user_id, {signed: true});
            return {
                'status' : 'ok',
                'code' : 0,
                'social_login' : 'success'
            };
        }
    } else {
        return {
            'status':'bad',
            'code':-1
        };
    }
}

function* getSocialUserData (token,sName) {
    var url = `${config.apiHost}/beelineMusicV1/social/showSocialProfile?serviceName=${sName}&token=${token}`,
        res = yield request({
            url: url,
            json: true
        });
    
    return res.body.profile;
}