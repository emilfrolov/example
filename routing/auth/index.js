'use strict'
var app = require('application'),
    Router = require('koa-router'),
    filters = require('filters'),
    router = new Router({
        prefix: '/auth'
    }),
    controller = require('./controller'),
    jade = require('render');

router.get('/register', controller.register);
router.get('/registerSocial', controller.registerSocial);
router.get('/login/byCode', controller.loginByCode);
router.get('/login/byCode/social', controller.loginByCodeSocial);
router.get('/login/byPass', controller.loginByPass);

app.use(router.routes())