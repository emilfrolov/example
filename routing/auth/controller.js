'use strict'
var config = require('config'),
    jade = require('render'),
    request = require('co-request');

module.exports = {
    register: function* (next) {
        this.status = 200;
        var phone = this.request.query.msisdn
        var res = yield request({
            url: config.apiHost+"/compatibility/beeline-user/loginByCodeRequest?msisdn="+phone,
            json: true
        });
        
        this.body = res.body;
    },
    registerSocial: function* (next) {
        this.status = 200;
        var phone = this.request.query.msisdn,
            data = {
                msisdn: this.query.msisdn,
                socialServiceName: this.query.socialServiceName,
                socialServiceToken: this.query.socialServiceToken,
            },
            url = `${config.apiHost}/compatibility/beeline-user/loginByCodeRequest`,
            res = yield request({
                url: url,
                json: true,
                qs: data
            });

            this.status = 200;
            this.body = res.body;
    },
    loginByCode: function* (next) {
        this.status = 200;
        var msisdn = this.request.query.msisdn,
            code = this.request.query.code;

        var res = yield request({
            url: config.apiHost+"/compatibility/beeline-user/loginByCodeProceed?msisdn="+msisdn+"&code="+code,
            json: true
        })
       
        if (res.body.status == 'ok') {
            this.session.sid = res.body.user.sid; 
            this.session.uid = res.body.user.user_id;
            this.session.user = res.body.user;
            this.cookies.set('sid', res.body.user.sid,{domain: '.beeline.ru'});
            this.cookies.set('userId', res.body.user.user_id, {signed: true});
        }

        this.body = res.body;
    },
    loginByCodeSocial: function* (next) {
        var url = `${config.apiHost}/compatibility/beeline-user/loginByCodeProceed`,
            data = {
                msisdn: this.query.msisdn,
                code: this.query.code,
                socialServiceName: this.query.socialServiceName,
                socialServiceToken: this.query.socialServiceToken,

            },
            res = yield request({
                url: url,
                json: true,
                qs: data
            });

        if (res.body.status == 'ok') {
            this.session.sid = res.body.user.sid; 
            this.session.uid = res.body.user.user_id;
            this.session.user = res.body.user;
            this.cookies.set('sid', res.body.user.sid,{domain: '.beeline.ru'});
            this.cookies.set('userId', res.body.user.user_id, {signed: true});
        }

        this.status = 200;
        this.body = res.body;

    },
    loginByPass: function* (next) {
        this.status = 200;
        var msisdn = this.request.query.msisdn,
            pass = this.request.query.pass;

        var res = yield request({
            url: config.apiHost+"/compatibility/beeline-user/login?phone="+msisdn+"&password="+pass,
            json: true
        })
       
        if (res.body.status == 'ok') {
            this.session.sid = res.body.user.sid; 
            this.session.uid = res.body.user.user_id;
            this.session.user = res.body.user;
            this.cookies.set('sid', res.body.user.sid,{domain: '.beeline.ru'});
            this.cookies.set('userId', res.body.user.user_id, {signed: true});
        }

        this.body = res.body;
    }
}