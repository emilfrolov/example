'use strict';
var app = require('application'),
    config = require('config'),
    fs = require('fs'),
    path = require('path'),
    log = require('logs'),
    routes = fs.readdirSync(path.join(__dirname, '../routing')).sort();

routes.forEach(function (route) {
    require('../routing/' + route);
});


app.use(require('../modules/404Page.js'));

app.listen(config.server.port,config.server.host);

process.on('message', function(msg) {
  if (msg == 'shutdown') { // PM2 sends this on graceful reload
    shutdown();
  }
});


function shutdown() {
  // The process is going to be reloaded
  // Have to close all database/socket.io/* connections
    // io.close();
    // app.close();
    log.info("Closing the app...");
}

// отслеживаем unhandled ошибки
// https://iojs.org/api/process.html#process_event_rejectionhandled
var unhandledRejections = [];
process.on('unhandledRejection', function(reason, p) {
  p.trackRejectionId = Math.random();

  setTimeout(function() { // 100 ms to catch up and handle rejection
    if (p.trackRejectionId) { // if not rejectionHandled yet, report
      unhandledRejections.push(p);
      var report = {
        err: reason,
        trackRejectionId: p.trackRejectionId,
        length: unhandledRejections.length
      };

      log.error(report, "unhandledRejection");
    }
  }, 100);

});

// если вдруг есть catch, но позже - скажем и об этом, с указанием промиса /trackRejectionId/
process.on('rejectionHandled', function(p) {
  if (~unhandledRejections.indexOf(p)) {
    // too more than 100 ms to handle
    // already in the rejection list, let's report
    unhandledRejections.splice(unhandledRejections.indexOf(p), 1);

    log.error({
      trackRejectionId: p.trackRejectionId,
      length: unhandledRejections.length
    }, "rejectionHandled");
  } else {
    // handled soon, don't track
    delete p.trackRejectionId;
  }
});