webpackJsonp([2],{

/***/ 50:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(51), __webpack_require__(22), __webpack_require__(26), __webpack_require__(37), __webpack_require__(30), __webpack_require__(28), __webpack_require__(52), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Feed, Track, Album, Playlist, Loader, Update, Prompt, Notification) {
	  var controller;
	  __webpack_require__(56);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      this.layout = new Feed.Layout;
	      this.listenTo(this.layout, 'show', function() {
	        var img, model, user, userView;
	        model = new Backbone.Model;
	        userView = new Feed.LayoutUser({
	          model: model
	        });
	        this.layout.head.show(userView);
	        user = app.request('get:user');
	        if (user.promise) {
	          return user.done((function(_this) {
	            return function(user) {
	              var img;
	              img = app.createImgLink(user.get('avatar'), '50x50');
	              return userView.model.set('img', img);
	            };
	          })(this));
	        } else {
	          img = app.createImgLink(user.get('avatar'), '50x50');
	          return userView.model.set('img', img);
	        }
	      });
	      app.content.show(this.layout);
	      return this.showFeeds();
	    };

	    controller.prototype.showCreatePost = function() {
	      this.postCreatelayout = new Feed.PostCreateLayout;
	      return this.layout.popup.show(this.postCreatelayout);
	    };

	    controller.prototype.showNoteCreatePanel = function() {
	      var model, panel, text, view;
	      view = this.postCreatelayout;
	      model = new Backbone.Model;
	      text = model = app.request('get:new:feed:model');
	      panel = new Feed.PostCreatePanel({
	        model: text
	      });
	      this.listenTo(panel, 'show', function() {
	        var feedPostItems, postHead, postList, user;
	        feedPostItems = app.request('get:feed:post:items');
	        postList = new Feed.PostCreatePanelList({
	          collection: feedPostItems
	        });
	        postHead = new Feed.PostCreatePanelHead({
	          model: model
	        });
	        user = app.request('get:user');
	        user.done((function(_this) {
	          return function(user) {
	            var img;
	            img = app.createImgLink(user.get('avatar'), '50x50');
	            return postHead.model.set('img', img);
	          };
	        })(this));
	        panel.head.show(postHead);
	        return panel.content.show(postList);
	      });
	      return view.content.show(panel);
	    };

	    controller.prototype.showNoteEditPanel = function(parentView) {
	      var model, panel, text, view, viewModel;
	      viewModel = parentView.model;
	      this.showCreatePost();
	      view = this.postCreatelayout;
	      model = new Backbone.Model;
	      text = model = app.request('get:new:feed:model');
	      text.set('message', viewModel.get('text'));
	      text.oldView = parentView;
	      panel = new Feed.PostCreatePanel({
	        model: text
	      });
	      this.listenTo(panel, 'show', function() {
	        var content, feedPostItems, i, item, len, parseContent, postHead, postList, type, user;
	        content = viewModel.get('content');
	        if (content.track) {
	          parseContent = content.track;
	          type = 'track';
	        } else if (content.album) {
	          type = 'album';
	          parseContent = content.album;
	        } else if (content.artist) {
	          type = 'artist';
	          parseContent = content.artist;
	        }
	        for (i = 0, len = parseContent.length; i < len; i++) {
	          item = parseContent[i];
	          item.type = type + 's';
	        }
	        feedPostItems = app.request('get:feed:post:items');
	        feedPostItems.reset(parseContent);
	        postList = new Feed.PostCreatePanelList({
	          collection: feedPostItems
	        });
	        postHead = new Feed.PostCreatePanelHead({
	          model: model
	        });
	        user = app.request('get:user');
	        user.done((function(_this) {
	          return function(user) {
	            var img;
	            img = app.createImgLink(user.get('avatar'), '50x50');
	            return postHead.model.set('img', img);
	          };
	        })(this));
	        panel.head.show(postHead);
	        return panel.content.show(postList);
	      });
	      return view.content.show(panel);
	    };

	    controller.prototype.showFeeds = function() {
	      var feeds, showFeeds;
	      showFeeds = (function(_this) {
	        return function(feeds) {
	          var fetchFeeds, view, winHeight;
	          $(window).off('.new');
	          fetchFeeds = function() {
	            var fetch, lastId;
	            $(window).off('.new');
	            lastId = feeds.at(feeds.length - 1).get('id');
	            fetch = feeds.fetch({
	              data: {
	                lastShownId: lastId,
	                limit: 30
	              },
	              remove: false
	            });
	            fetch.done(function(newcollection) {
	              var winHeight;
	              app.checkActiveAlbum();
	              app.checkActiveTrack();
	              if ((newcollection.wallRecords != null) && newcollection.wallRecords.length) {
	                winHeight = $('body').height();
	                return $(window).on('scroll.new', function() {
	                  var scroll;
	                  scroll = $(window).scrollTop() + $(window).height() * 5;
	                  if (winHeight < scroll) {
	                    return fetchFeeds();
	                  }
	                });
	              }
	            });
	            return fetch.fail(function(res) {
	              if (res.status === 422) {
	                return contacts.add(res.responseJSON);
	              } else {
	                return console.log('Произошла ошибка');
	              }
	            });
	          };
	          view = new Feed.List({
	            collection: feeds
	          });
	          _this.layout.content.show(view);
	          winHeight = $('body').height();
	          return $(window).on('scroll.new', function() {
	            var scroll;
	            scroll = $(window).scrollTop() + $(window).height() * 5;
	            if (winHeight < scroll) {
	              return fetchFeeds();
	            }
	          });
	        };
	      })(this);
	      feeds = app.request('get:feeds');
	      if (feeds.promise) {
	        feeds.done((function(_this) {
	          return function(collection) {
	            return showFeeds(collection);
	          };
	        })(this));
	        return feeds.fail(function() {});
	      } else {
	        return showFeeds(feeds);
	      }
	    };

	    controller.prototype.showPostLikes = function(view) {
	      var likesView;
	      likesView = new Feed.ListItemLike({
	        model: view.model
	      });
	      return view.likes.show(likesView);
	    };

	    controller.prototype.showPostComments = function(view) {
	      var collection, comments, commentsView;
	      comments = view.model.get('comments');
	      collection = view.commentsCollection = new Backbone.Collection(comments);
	      commentsView = new Feed.ListItemComments({
	        collection: collection
	      });
	      return view.comments.show(commentsView);
	    };

	    controller.prototype.showPostContent = function(view) {
	      var album, albumView, collection, content, playlist, playlistView, tracksView;
	      content = view.model.get('content');
	      if (content != null) {
	        if (content.track) {
	          collection = new Track.Collection(content.track);
	          collection.each(function(item) {
	            item.set('uniq', 'feed' + view.model.get('id') + item.id);
	            return item.set('optionsList', [
	              {
	                title: app.locale.options.addTPl,
	                "class": 'add-to-playlist'
	              }, {
	                title: app.locale.options.toArt,
	                "class": 'options-open-artist'
	              }, {
	                title: app.locale.options.toAlb,
	                "class": 'options-open-album'
	              }
	            ]);
	          });
	          tracksView = new Feed.Tracks({
	            collection: collection
	          });
	          return view.content.show(tracksView);
	        } else if (content.album) {
	          album = (function(superClass1) {
	            extend(album, superClass1);

	            function album() {
	              return album.__super__.constructor.apply(this, arguments);
	            }

	            album.prototype.parse = function(model) {
	              if (model.album) {
	                model.album.album = true;
	                return model.album;
	              } else {
	                model.album = true;
	                return model;
	              }
	            };

	            return album;

	          })(Album.Model);
	          album = new album(content.album[0]);
	          albumView = new Feed.Album({
	            model: album
	          });
	          return view.content.show(albumView);
	        } else if (content.playlist) {
	          playlist = (function(superClass1) {
	            extend(playlist, superClass1);

	            function playlist() {
	              return playlist.__super__.constructor.apply(this, arguments);
	            }

	            playlist.prototype.parse = function(model) {
	              if (model.album) {
	                model.album.album = true;
	                return model.album;
	              } else {
	                model.album = true;
	                return model;
	              }
	            };

	            return playlist;

	          })(Playlist.Model);
	          playlist = new playlist(content.playlist[0]);
	          playlistView = new Feed.Playlist({
	            model: playlist
	          });
	          return view.content.show(playlistView);
	        }
	      }
	    };

	    controller.prototype.updatePostCollection = function(items, type) {
	      var feedPostItems, i, item, len;
	      for (i = 0, len = items.length; i < len; i++) {
	        item = items[i];
	        item.set('type', type);
	      }
	      feedPostItems = app.request('get:feed:post:items');
	      feedPostItems.reset(items);
	      return this.showNoteCreatePanel();
	    };

	    controller.prototype.openFeedAdd = function(title, trigger, selectedType, childView, type) {
	      var collection, openAddWin, self;
	      self = this;
	      openAddWin = function(collection) {
	        var collectionTemp, model, view, viewTemp;
	        model = new Backbone.Model({
	          title: title,
	          type: type
	        });
	        collectionTemp = (function(superClass1) {
	          extend(collectionTemp, superClass1);

	          function collectionTemp() {
	            return collectionTemp.__super__.constructor.apply(this, arguments);
	          }

	          collectionTemp.prototype.model = Backbone.Model.Selected;

	          return collectionTemp;

	        })(Backbone.Collection[selectedType]);
	        collection = new collectionTemp(collection.toJSON());
	        viewTemp = (function(superClass1) {
	          extend(viewTemp, superClass1);

	          function viewTemp() {
	            return viewTemp.__super__.constructor.apply(this, arguments);
	          }

	          viewTemp.prototype.childView = childView;

	          viewTemp.prototype.className = 'feed-content-add-wrapp ' + type + '-wrapper';

	          return viewTemp;

	        })(Feed.PostContentAddView);
	        view = new viewTemp({
	          model: model,
	          collection: collection
	        });
	        return self.postCreatelayout.content.show(view);
	      };
	      collection = app.request(trigger);
	      if (collection.promise) {
	        collection.done(function(collection) {
	          return openAddWin(collection);
	        });
	        return collection.fail(function(res) {
	          return console.log(res);
	        });
	      } else {
	        return openAddWin(collection);
	      }
	    };

	    controller.prototype.postCreateSuccess = function(data) {
	      var createPost, self;
	      self = this;
	      createPost = function() {
	        var create, loader;
	        loader = new Loader.View({
	          styles: {
	            'position': 'absolute',
	            'top': '50%',
	            'transform': 'translateY(-50%)',
	            'left': '50%',
	            'transform': 'translateX(-50%)'
	          }
	        });
	        self.postCreatelayout.content.show(loader);
	        create = $.get(app.getRequestUrlSid("/beelineMusicV1/wall/createRecord"), data);
	        return create.done((function(_this) {
	          return function(data) {
	            var feeds, item, model;
	            if (data.code === 0) {
	              item = data.wallRecord;
	              model = new Backbone.Model(item);
	              feeds = app.request('get:feeds', true);
	              return app.request('get:user').done(function(user) {
	                model.set('currentUser', user.toJSON());
	                feeds.add(model);
	                Notification.Controller.show({
	                  message: 'Ваш пост успешно добавлен',
	                  type: 'success',
	                  time: 3000
	                });
	                return self.postCreatelayout.destroy();
	              });
	            } else {
	              Notification.Controller.show({
	                message: data.message,
	                type: 'error',
	                time: 3000
	              });
	              return _this.showNoteCreatePanel();
	            }
	          };
	        })(this));
	      };
	      return app.request('get:user').done(function(user) {
	        var params, userName;
	        userName = user.get('profile_name');
	        if ((userName.length === 0) || (userName === null)) {
	          params = {
	            title: 'Как можно к вам<br>обратиться?',
	            message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
	            placeholder: 'Ваше имя'
	          };
	          return Prompt.Controller.show(params, function(name) {
	            var loader, nameUpdate;
	            loader = new Loader.View({
	              styles: {
	                'position': 'absolute',
	                'top': '50%',
	                'transform': 'translateY(-50%)',
	                'left': '50%',
	                'transform': 'translateX(-50%)'
	              }
	            });
	            self.postCreatelayout.content.show(loader);
	            nameUpdate = $.ajax({
	              type: "GET",
	              url: app.getRequestUrlSid("/compatibility/user/updateProfile"),
	              data: {
	                name: name
	              }
	            });
	            return nameUpdate.done(function() {
	              user.set('profile_name', name);
	              return createPost();
	            });
	          });
	        } else {
	          return createPost();
	        }
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.postEditSuccess = function(data, view) {
	      var loader, self, update;
	      self = this;
	      loader = new Loader.View({
	        styles: {
	          'position': 'absolute',
	          'top': '50%',
	          'transform': 'translateY(-50%)',
	          'left': '50%',
	          'transform': 'translateX(-50%)'
	        }
	      });
	      this.postCreatelayout.content.show(loader);
	      update = $.get(app.getRequestUrlSid("/beelineMusicV1/wall/updateRecord"), data);
	      return update.done((function(_this) {
	        return function(data) {
	          var model;
	          if (data.code === 0) {
	            model = new Backbone.Model(data.wallRecord);
	            model.set('currentUser', view.model.get('currentUser'));
	            view.model = model;
	            view.render();
	            Notification.Controller.show({
	              message: 'Ваш пост успешно обновлен',
	              type: 'success',
	              time: 3000
	            });
	            return self.postCreatelayout.destroy();
	          } else {
	            Notification.Controller.show({
	              message: data.message,
	              type: 'error',
	              time: 3000
	            });
	            return _this.showNoteCreatePanel();
	          }
	        };
	      })(this));
	    };

	    controller.prototype.saveComment = function(view, comment) {
	      var data, saveComment;
	      data = {
	        objectType: 'wall_record',
	        objectId: view.model.get('id'),
	        trackIds: comment.trackIds,
	        text: comment.message
	      };
	      saveComment = $.get(app.getRequestUrlSid('/beelineMusicV1/comment/saveComment'), data);
	      saveComment.done(function(data) {
	        var comments, model, newComment;
	        if (data.code === 0) {
	          newComment = data.comment;
	          model = new Backbone.Model(newComment);
	          view.commentsCollection.add(model);
	          comments = view.model.get('comments');
	          if (comments != null) {
	            comments.push(newComment);
	          } else {
	            comments = [];
	            comments[0] = newComment;
	          }
	          view.model.set('comments', comments);
	          view.$('.add-comment textarea').val('').attr('style', '');
	          return view.$('.comment-content').empty();
	        } else {
	          return Notification.Controller.show({
	            message: data.message,
	            type: 'error',
	            time: 3000
	          });
	        }
	      });
	      return saveComment.fail(function(err) {
	        return Notification.Controller.show({
	          message: 'Неизвестная ошибка',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.toggleCommentsOpen = function(view) {
	      var data, fetch;
	      if (view.allCommentsIsShown === true) {
	        view.moreCommentsCollection.reset();
	        view.$('.more-comments').html('Раскрыть все комментарии <span>' + (view.model.get('comments_count') - 3) + '</span>');
	        return view.allCommentsIsShown = false;
	      } else {
	        if (view.model.has('allComments')) {
	          view.$('.more-comments').text('Свернуть комментарии');
	          view.moreCommentsCollection.reset(view.model.get('allComments'));
	          return view.allCommentsIsShown = true;
	        } else {
	          data = {
	            lastShownId: view.model.get('comments')[0].id,
	            objectType: 'wall_record',
	            limit: 100,
	            objectId: view.model.get('id')
	          };
	          fetch = $.get(app.getRequestUrlSid('/beelineMusicV1/comment/getComments'), data);
	          view.$('.more-comments').text('Подождите...');
	          fetch.done(function(data) {
	            var collection, commentsView;
	            if (data.code === 0) {
	              view.model.set('allComments', data.comments);
	              view.$('.more-comments').text('Свернуть комментарии');
	              collection = view.moreCommentsCollection = new Backbone.Collection(data.comments);
	              commentsView = new Feed.ListItemComments({
	                collection: collection
	              });
	              view.moreComments.show(commentsView);
	              return view.allCommentsIsShown = true;
	            }
	          });
	          return fetch.fail(function(err) {});
	        }
	      }
	    };

	    controller.prototype.showAddCommentFavouriteContent = function(view) {
	      return app.request('get:favorite:tracks').done(function(tracks) {
	        var favouriteView;
	        favouriteView = new Feed.CommentContentTrackList({
	          collection: tracks
	        });
	        return view.content.show(favouriteView);
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.showAddCommentSearchedContent = function(val, view) {
	      return app.request('search:tracks', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 60
	        }
	      }).done(function(tracks) {
	        var favouriteView;
	        favouriteView = new Feed.CommentContentTrackList({
	          collection: tracks
	        });
	        return view.content.show(favouriteView);
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.showAddCommentContentWindow = function(listItemView) {
	      var commentContent, view;
	      view = new Feed.CommentContentWindow;
	      commentContent = new Feed.CommentContent;
	      this.openedCommentView = listItemView;
	      this.layout.popup.show(view);
	      view.content.show(commentContent);
	      return this.showAddCommentFavouriteContent(commentContent);
	    };

	    controller.prototype.startListenNewFeeds = function() {};

	    controller.prototype.stopListenNewFeeds = function() {};

	    return controller;

	  })(Marionette.Controller);
	  Feed.Controller = new controller;
	  return Feed;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 51:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(26), __webpack_require__(37)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track, Album, Playlist) {
	  var Feed;
	  Feed = {};
	  Feed.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#feed-page-tpl';

	    Layout.prototype.className = 'feed-page';

	    Layout.prototype.regions = {
	      head: '.head-region',
	      content: '.feed-content',
	      popup: '.feed-popup'
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Feed.LayoutUser = (function(superClass) {
	    extend(LayoutUser, superClass);

	    function LayoutUser() {
	      return LayoutUser.__super__.constructor.apply(this, arguments);
	    }

	    LayoutUser.prototype.className = 'placeholder';

	    LayoutUser.prototype.template = '#feed-page-user-tpl';

	    LayoutUser.prototype.modelEvents = {
	      'change:img': function() {
	        return this.render();
	      }
	    };

	    LayoutUser.prototype.events = {
	      'click': function() {
	        return Feed.Controller.showCreatePost();
	      },
	      'click span': function(e) {
	        return e.stopPropagation();
	      }
	    };

	    return LayoutUser;

	  })(Marionette.ItemView);
	  Feed.ListItem = (function(superClass) {
	    extend(ListItem, superClass);

	    function ListItem() {
	      return ListItem.__super__.constructor.apply(this, arguments);
	    }

	    ListItem.prototype.template = '#feed-list-item-tpl';

	    ListItem.prototype.regions = {
	      content: '.list-content',
	      likes: '.like-block',
	      comments: '.comments-list',
	      moreComments: '.more-comments-list'
	    };

	    ListItem.prototype.className = 'feed-list-item';

	    ListItem.prototype.onRender = function() {
	      Feed.Controller.showPostContent(this);
	      Feed.Controller.showPostLikes(this);
	      Feed.Controller.showPostComments(this);
	      return autosize(this.$('.new-comment-text textarea'));
	    };

	    ListItem.prototype.events = {
	      'click .more-comments': function() {
	        return Feed.Controller.toggleCommentsOpen(this);
	      },
	      'click .new-comment-text textarea': function() {
	        var self;
	        self = this;
	        if (!this.$('.add-comment').hasClass('open')) {
	          $(document).off('.comment');
	          $('.send-comment').hide();
	          $('.add-comment').removeClass('open');
	          this.$('.send-comment').show();
	          this.$('.add-comment').addClass('open');
	          return $(document).on('click.comment', function(e) {
	            if (!$(e.target).closest('.add-comment.open').length) {
	              $(document).off('.comment');
	              self.$('.send-comment').hide();
	              return self.$('.add-comment').removeClass('open');
	            }
	          });
	        }
	      },
	      'click .add-content': function() {
	        return Feed.Controller.showAddCommentContentWindow(this);
	      },
	      'click .send-comment': function(e) {
	        var comment, tracks;
	        tracks = [];
	        this.$('.comment-content .track-item').each(function() {
	          return tracks.push($(this).attr('data-id'));
	        });
	        comment = {
	          message: this.$('.add-comment textarea').val(),
	          trackIds: tracks
	        };
	        return Feed.Controller.saveComment(this, comment);
	      },
	      'click .options': function(e) {
	        e.stopPropagation();
	        this.$('.options .list').show();
	        return $(window).on('click.feedOptions', function(e) {
	          $(window).off('.feedOptions');
	          return $('.options .list').hide();
	        });
	      },
	      'click .options .delete': function(e) {
	        e.preventDefault();
	        return this.model.destroy();
	      },
	      'click .options .edit': function(e) {
	        e.preventDefault();
	        return Feed.Controller.showNoteEditPanel(this);
	      },
	      'click .info .title': function() {
	        return app.trigger('show:user:page', this.model.get('owner').user_id);
	      },
	      'click .comment-content .track-item .delete': function(e) {
	        return $(e.target).closest('.track-item').remove();
	      }
	    };

	    return ListItem;

	  })(Marionette.LayoutView);
	  Feed.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.template = '#feed-track-tpl';

	    Track.prototype.className = 'track-item feed-track';

	    return Track;

	  })(Track.Item);
	  Feed.ListItemComment = (function(superClass) {
	    extend(ListItemComment, superClass);

	    function ListItemComment() {
	      return ListItemComment.__super__.constructor.apply(this, arguments);
	    }

	    ListItemComment.prototype.childView = Feed.Track;

	    ListItemComment.prototype.template = '#feed-list-item-comment-tpl';

	    ListItemComment.prototype.className = 'feed-list-comment-item';

	    ListItemComment.prototype.childViewContainer = '.comment-tracks';

	    ListItemComment.prototype.onBeforeRender = function() {
	      var i, item, len, tracks;
	      tracks = this.model.get('tracks');
	      if (tracks != null) {
	        for (i = 0, len = tracks.length; i < len; i++) {
	          item = tracks[i];
	          item['uniq'] = 'feed' + this.model.get('id') + item.id;
	          item['optionsList'] = [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ];
	        }
	        return this.collection = new Backbone.Collection(tracks);
	      }
	    };

	    ListItemComment.prototype.events = {
	      'click .post span': function() {
	        return app.trigger('show:user:page', this.model.get('owner').user_id);
	      }
	    };

	    return ListItemComment;

	  })(Marionette.CompositeView);
	  Feed.ListItemComments = (function(superClass) {
	    extend(ListItemComments, superClass);

	    function ListItemComments() {
	      return ListItemComments.__super__.constructor.apply(this, arguments);
	    }

	    ListItemComments.prototype.childView = Feed.ListItemComment;

	    ListItemComments.prototype.className = 'feed-list-comments-wrapp';

	    ListItemComments.prototype.viewComparator = function(item) {
	      return item.get('id');
	    };

	    return ListItemComments;

	  })(Marionette.CollectionView);
	  Feed.ListItemLike = (function(superClass) {
	    extend(ListItemLike, superClass);

	    function ListItemLike() {
	      return ListItemLike.__super__.constructor.apply(this, arguments);
	    }

	    ListItemLike.prototype.template = '#feed-list-item-like-tpl';

	    ListItemLike.prototype.events = {
	      'click .like-icon': function() {
	        var likes_count;
	        likes_count = this.model.get('likes_count');
	        if (this.model.has('likedByMe')) {
	          if (this.model.get('likedByMe') === true) {
	            this.model.set('likedByMe', false);
	            likes_count--;
	            $.get(app.getRequestUrlSid("/beelineMusicV1/like/deleteLike", "?objectType=wall_record&objectId=" + (this.model.get('id'))));
	          } else {
	            this.model.set('likedByMe', true);
	            likes_count++;
	            $.get(app.getRequestUrlSid("/beelineMusicV1/like/saveLike", "?objectType=wall_record&objectId=" + (this.model.get('id'))));
	          }
	        } else {
	          this.model.set('likedByMe', true);
	          likes_count++;
	          $.get(app.getRequestUrlSid("/beelineMusicV1/like/saveLike", "?objectType=wall_record&objectId=" + (this.model.get('id'))));
	        }
	        likes_count = likes_count < 0 ? 0 : likes_count;
	        return this.model.set('likes_count', likes_count);
	      }
	    };

	    ListItemLike.prototype.modelEvents = {
	      'change': function() {
	        return this.render();
	      }
	    };

	    return ListItemLike;

	  })(Marionette.ItemView);
	  Feed.List = (function(superClass) {
	    extend(List, superClass);

	    function List() {
	      return List.__super__.constructor.apply(this, arguments);
	    }

	    List.prototype.childView = Feed.ListItem;

	    List.prototype.className = 'feed-list';

	    List.prototype.onShow = function() {
	      return Feed.Controller.startListenNewFeeds();
	    };

	    List.prototype.onDestroy = function() {
	      return Feed.Controller.stopListenNewFeeds();
	    };

	    return List;

	  })(Marionette.CollectionView);
	  Feed.Album = (function(superClass) {
	    extend(Album, superClass);

	    function Album() {
	      return Album.__super__.constructor.apply(this, arguments);
	    }

	    Album.prototype.template = '#feed-album-tpl';

	    Album.prototype.className = 'album-item feed-album-item';

	    Album.prototype.onRender = function() {
	      var img, link;
	      this.$el.css('background-image', "url('" + (app.createImgLink(this.model.get('img'), '150x150', true)) + "')");
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '150x150');
	      return img.src = link;
	    };

	    return Album;

	  })(Album.Item);
	  Feed.Playlist = (function(superClass) {
	    extend(Playlist, superClass);

	    function Playlist() {
	      return Playlist.__super__.constructor.apply(this, arguments);
	    }

	    Playlist.prototype.template = '#feed-playlist-tpl';

	    Playlist.prototype.className = 'album-item playlist-item feed-playlist-item';

	    Playlist.prototype.onRender = function() {
	      var img, link;
	      this.$el.css('background-image', "url('" + (app.createImgLink(this.model.get('image'), '150x150', true)) + "')");
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('image'), '150x150');
	      return img.src = link;
	    };

	    return Playlist;

	  })(Playlist.Item);
	  Feed.Tracks = (function(superClass) {
	    extend(Tracks, superClass);

	    function Tracks() {
	      return Tracks.__super__.constructor.apply(this, arguments);
	    }

	    Tracks.prototype.childView = Feed.Track;

	    Tracks.prototype.className = 'tracks-wrapper';

	    return Tracks;

	  })(Marionette.CollectionView);
	  Feed.PostCreateLayout = (function(superClass) {
	    extend(PostCreateLayout, superClass);

	    function PostCreateLayout() {
	      return PostCreateLayout.__super__.constructor.apply(this, arguments);
	    }

	    PostCreateLayout.prototype.template = '#feed-note-create-layout-tpl';

	    PostCreateLayout.prototype.className = 'feed-note-create-overlay';

	    PostCreateLayout.prototype.regions = {
	      content: '.post-create-container'
	    };

	    PostCreateLayout.prototype.onShow = function() {
	      $('.feed-page-wrapp').css({
	        'position': 'fixed',
	        'height': '100%',
	        'left': '50%',
	        'transform': 'translateX(-50%)'
	      });
	      app.scrollTop = $(window).scrollTop();
	      $('.feed-page-margin').css('margin-top', -app.scrollTop);
	      $(window).scrollTop(0);
	      return Feed.Controller.showNoteCreatePanel();
	    };

	    PostCreateLayout.prototype.onDestroy = function() {
	      var collection, model;
	      $('.feed-page-wrapp').attr('style', '');
	      $('.feed-page-margin').attr('style', '');
	      $(window).scrollTop(app.scrollTop);
	      model = app.request('get:new:feed:model');
	      collection = app.request('get:feed:post:items');
	      model.set('message', '');
	      delete model.oldView;
	      return collection.reset();
	    };

	    PostCreateLayout.prototype.events = {
	      'click': function() {
	        return this.destroy();
	      },
	      'click .feed-note-create-wrapp': function(e) {
	        return e.stopPropagation();
	      }
	    };

	    return PostCreateLayout;

	  })(Marionette.LayoutView);
	  Feed.CommentContentWindow = (function(superClass) {
	    extend(CommentContentWindow, superClass);

	    function CommentContentWindow() {
	      return CommentContentWindow.__super__.constructor.apply(this, arguments);
	    }

	    CommentContentWindow.prototype.onShow = function() {
	      app.scrollTop = $(window).scrollTop();
	      $('.feed-page-wrapp').css({
	        'position': 'fixed',
	        'height': '100%',
	        'left': '50%',
	        'transform': 'translateX(-50%)'
	      });
	      $('.feed-page-margin').css('margin-top', -app.scrollTop);
	      return $(window).scrollTop(0);
	    };

	    CommentContentWindow.prototype.onDestroy = function() {
	      $('.feed-page-wrapp').attr('style', '');
	      $('.feed-page-margin').attr('style', '');
	      return $(window).scrollTop(app.scrollTop);
	    };

	    return CommentContentWindow;

	  })(Feed.PostCreateLayout);
	  Feed.CommentContentTrack = (function(superClass) {
	    extend(CommentContentTrack, superClass);

	    function CommentContentTrack() {
	      return CommentContentTrack.__super__.constructor.apply(this, arguments);
	    }

	    CommentContentTrack.prototype.className = 'track-item feed-add-track';

	    CommentContentTrack.prototype.template = '#feed-add-track-tpl';

	    CommentContentTrack.prototype.events = {
	      'click': function() {
	        var trackCompile, trackView;
	        trackCompile = _.template($('#feed-add-track-tpl').html());
	        trackView = "<div class=\"track-item feed-add-track\" data-id=\"" + (this.model.get('id')) + "\">\n	" + (trackCompile(this.model.toJSON())) + "\n</div>";
	        Feed.Controller.openedCommentView.$('.comment-content').append(trackView);
	        return $('.feed-note-create-overlay').click();
	      }
	    };

	    return CommentContentTrack;

	  })(Marionette.ItemView);
	  Feed.CommentContentTrackList = (function(superClass) {
	    extend(CommentContentTrackList, superClass);

	    function CommentContentTrackList() {
	      return CommentContentTrackList.__super__.constructor.apply(this, arguments);
	    }

	    CommentContentTrackList.prototype.childView = Feed.CommentContentTrack;

	    CommentContentTrackList.prototype.className = 'tracks-wrapper';

	    return CommentContentTrackList;

	  })(Marionette.CollectionView);
	  Feed.CommentContent = (function(superClass) {
	    extend(CommentContent, superClass);

	    function CommentContent() {
	      return CommentContent.__super__.constructor.apply(this, arguments);
	    }

	    CommentContent.prototype.className = 'feed-content-add-wrapp searched tracks-wrapper';

	    CommentContent.prototype.template = '#feed-comment-content-add-tpl';

	    CommentContent.prototype.regions = {
	      content: '.feed-content-add-body'
	    };

	    CommentContent.prototype.events = {
	      'click .search': function() {
	        if (this.$('.search input').is(":hidden")) {
	          return this.$('.search input').val('Напишите название трека').show().select();
	        }
	      },
	      'keyup .search input': function() {
	        var val;
	        val = $.trim(this.$('.search input').val());
	        if (this.searchTimer) {
	          clearTimeout(this.searchTimer);
	        }
	        return this.searchTimer = setTimeout((function(_this) {
	          return function() {
	            if (val !== '') {
	              return Feed.Controller.showAddCommentSearchedContent(val, _this);
	            } else {
	              Feed.Controller.showAddCommentFavouriteContent(_this);
	              return _this.$('.search input').hide().blur();
	            }
	          };
	        })(this), 300);
	      }
	    };

	    return CommentContent;

	  })(Marionette.LayoutView);
	  Feed.PostCreatePanelHead = (function(superClass) {
	    extend(PostCreatePanelHead, superClass);

	    function PostCreatePanelHead() {
	      return PostCreatePanelHead.__super__.constructor.apply(this, arguments);
	    }

	    PostCreatePanelHead.prototype.template = '#feed-note-create-head-tpl';

	    PostCreatePanelHead.prototype.className = '#feed-note-create-head-wrapp';

	    PostCreatePanelHead.prototype.modelEvents = {
	      'change:img': function() {
	        return this.render();
	      }
	    };

	    return PostCreatePanelHead;

	  })(Marionette.ItemView);
	  Feed.PostCreatePanel = (function(superClass) {
	    extend(PostCreatePanel, superClass);

	    function PostCreatePanel() {
	      return PostCreatePanel.__super__.constructor.apply(this, arguments);
	    }

	    PostCreatePanel.prototype.template = '#feed-note-create-panel-tpl';

	    PostCreatePanel.prototype.className = 'feed-note-create-panel';

	    PostCreatePanel.prototype.onShow = function() {
	      autosize($('#textarea_resize'));
	      $('#textarea_resize').keyup();
	      if (this.model.oldView) {
	        return this.$('.create').text('Сохранить').show();
	      }
	    };

	    PostCreatePanel.prototype.regions = {
	      head: '.feed-note-create-head',
	      content: '.feed-note-create-content'
	    };

	    PostCreatePanel.prototype.events = {
	      'keyup #textarea_resize': function() {
	        var feedPostItems, val;
	        val = $('#textarea_resize').val();
	        feedPostItems = app.request('get:feed:post:items');
	        this.model.set('message', val);
	        if ((val === '') && (feedPostItems.length === 0)) {
	          return $('.feed-note-create-panel .create').hide();
	        } else {
	          return $('.feed-note-create-panel .create').show();
	        }
	      },
	      'change #textarea_resize': function() {
	        var feedPostItems, val;
	        val = $('#textarea_resize').val();
	        feedPostItems = app.request('get:feed:post:items');
	        this.model.set('message', val);
	        if ((val === '') && (feedPostItems.length === 0)) {
	          return $('.feed-note-create-panel .create').hide();
	        } else {
	          return $('.feed-note-create-panel .create').show();
	        }
	      },
	      'click .create': function(e) {
	        var collection, data, model, type, view;
	        e.stopPropagation();
	        model = app.request('get:new:feed:model');
	        collection = app.request('get:feed:post:items');
	        if (collection.length) {
	          type = collection.at(0).get('type').slice(0, -1);
	        } else {
	          type = '';
	        }
	        data = {};
	        data.contentType = type;
	        data.text = model.get('message');
	        data.contentIds = collection.pluck('id');
	        if (model.oldView) {
	          view = model.oldView;
	          data.recordId = view.model.get('id');
	          return Feed.Controller.postEditSuccess(data, view);
	        } else {
	          return Feed.Controller.postCreateSuccess(data);
	        }
	      }
	    };

	    return PostCreatePanel;

	  })(Marionette.LayoutView);
	  Feed.PostCreatePanelItem = (function(superClass) {
	    extend(PostCreatePanelItem, superClass);

	    function PostCreatePanelItem() {
	      return PostCreatePanelItem.__super__.constructor.apply(this, arguments);
	    }

	    PostCreatePanelItem.prototype.template = '#feed-note-create-item-tpl';

	    PostCreatePanelItem.prototype.className = 'feed-note-create-item';

	    PostCreatePanelItem.prototype.triggers = {
	      'click .delete': 'delete'
	    };

	    return PostCreatePanelItem;

	  })(Marionette.ItemView);
	  Feed.PostCreatePanelEmpty = (function(superClass) {
	    extend(PostCreatePanelEmpty, superClass);

	    function PostCreatePanelEmpty() {
	      return PostCreatePanelEmpty.__super__.constructor.apply(this, arguments);
	    }

	    PostCreatePanelEmpty.prototype.template = '#feed-note-create-empty-tpl';

	    PostCreatePanelEmpty.prototype.className = 'feed-note-create-types';

	    return PostCreatePanelEmpty;

	  })(Marionette.ItemView);
	  Feed.PostCreatePanelList = (function(superClass) {
	    extend(PostCreatePanelList, superClass);

	    function PostCreatePanelList() {
	      return PostCreatePanelList.__super__.constructor.apply(this, arguments);
	    }

	    PostCreatePanelList.prototype.childView = Feed.PostCreatePanelItem;

	    PostCreatePanelList.prototype.emptyView = Feed.PostCreatePanelEmpty;

	    PostCreatePanelList.prototype.className = function() {
	      var className, type;
	      className = 'feed-note-create-content-wrapp';
	      if (this.collection.length) {
	        type = this.collection.at(0).get('type');
	        switch (type) {
	          case 'tracks':
	            className += ' tracks-wrapper';
	            break;
	          case 'albums':
	            className += ' content-wrapper';
	        }
	      }
	      return className;
	    };

	    PostCreatePanelList.prototype.events = {
	      'click .item-type li': function(e) {
	        var type;
	        type = $(e.currentTarget).attr('data-type');
	        switch (type) {
	          case 'tracks':
	            Feed.Controller.openFeedAdd('Трек', 'get:favorite:tracks', 'MultiSelected', Feed.AddTrack, 'tracks');
	            break;
	          case 'playlists':
	            Feed.Controller.openFeedAdd('Плейлист', 'get:favorite:playlists', 'SingleSelect', Feed.AddPlaylist, 'playlists');
	            break;
	          case 'albums':
	            Feed.Controller.openFeedAdd('Альбом', 'get:favorite:albums', 'SingleSelect', Feed.AddAlbum, 'albums');
	        }
	        return e.stopPropagation();
	      }
	    };

	    PostCreatePanelList.prototype.onChildviewDelete = function(item) {
	      return this.collection.remove(item.model);
	    };

	    PostCreatePanelList.prototype.collectionEvents = {
	      'remove': function() {
	        var val;
	        if (!this.collection.length) {
	          $('.post-content-message').remove();
	          val = $('#textarea_resize').val();
	          if (val === '') {
	            return $('.feed-note-create-panel .create').hide();
	          }
	        }
	      }
	    };

	    PostCreatePanelList.prototype.onRender = function() {
	      var title, type;
	      if (this.collection.length) {
	        type = this.collection.at(0).get('type');
	        switch (type) {
	          case 'tracks':
	            title = 'Прикрепленные треки';
	            break;
	          case 'playlists':
	            title = 'Прикрепленный плейлист';
	            break;
	          case 'albums':
	            title = 'Прикрепленный альбом';
	        }
	        return this.$el.prepend('<div class="post-content-message">' + title + '</div>');
	      }
	    };

	    return PostCreatePanelList;

	  })(Marionette.CollectionView);
	  Feed.AddTrack = (function(superClass) {
	    extend(AddTrack, superClass);

	    function AddTrack() {
	      return AddTrack.__super__.constructor.apply(this, arguments);
	    }

	    AddTrack.prototype.template = '#feed-add-track-tpl';

	    AddTrack.prototype.className = 'track-item feed-add-track';

	    AddTrack.prototype.events = {
	      'click': function() {
	        if (this.model.selected) {
	          this.$el.removeClass('selected');
	          return this.model.deselect();
	        } else {
	          this.$el.addClass('selected');
	          return this.model.select();
	        }
	      }
	    };

	    return AddTrack;

	  })(Marionette.ItemView);
	  Feed.AddAlbum = (function(superClass) {
	    extend(AddAlbum, superClass);

	    function AddAlbum() {
	      return AddAlbum.__super__.constructor.apply(this, arguments);
	    }

	    AddAlbum.prototype.template = '#feed-add-album-tpl';

	    AddAlbum.prototype.className = 'album-item feed-add-album';

	    AddAlbum.prototype.events = {
	      'click': function() {
	        if (this.model.selected) {
	          this.$el.removeClass('selected');
	          return this.model.deselect();
	        } else {
	          $('.album-item.selected').removeClass('selected');
	          this.$el.addClass('selected');
	          return this.model.select();
	        }
	      }
	    };

	    AddAlbum.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '150x150');
	      return img.src = link;
	    };

	    AddAlbum.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return AddAlbum;

	  })(Marionette.ItemView);
	  Feed.AddPlaylist = (function(superClass) {
	    extend(AddPlaylist, superClass);

	    function AddPlaylist() {
	      return AddPlaylist.__super__.constructor.apply(this, arguments);
	    }

	    AddPlaylist.prototype.template = '#feed-add-playlist-tpl';

	    AddPlaylist.prototype.className = 'playlist-item feed-add-playlist';

	    AddPlaylist.prototype.events = {
	      'click': function() {
	        if (this.model.selected) {
	          this.$el.removeClass('selected');
	          return this.model.deselect();
	        } else {
	          $('.playlist-item.selected').removeClass('selected');
	          this.$el.addClass('selected');
	          return this.model.select();
	        }
	      }
	    };

	    AddPlaylist.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('playlist-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('image'), '150x150');
	      return img.src = link;
	    };

	    AddPlaylist.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return AddPlaylist;

	  })(Marionette.ItemView);
	  Feed.PostContentAddView = (function(superClass) {
	    extend(PostContentAddView, superClass);

	    function PostContentAddView() {
	      return PostContentAddView.__super__.constructor.apply(this, arguments);
	    }

	    PostContentAddView.prototype.template = '#feed-post-content-add-tpl';

	    PostContentAddView.prototype.childViewContainer = '.feed-content-add-body';

	    PostContentAddView.prototype.events = {
	      'click': function(e) {
	        return e.stopPropagation();
	      },
	      'click .back': function() {
	        return Feed.Controller.showNoteCreatePanel();
	      },
	      'click .add': function() {
	        var selected;
	        selected = this.collection.filter(function(item) {
	          return item.selected === true;
	        });
	        return Feed.Controller.updatePostCollection(selected, this.model.get('type'));
	      }
	    };

	    PostContentAddView.prototype.collectionEvents = {
	      'select:some': function() {
	        return this.$('.add').show();
	      },
	      'select:one': function() {
	        return this.$('.add').show();
	      },
	      'select:none': function() {
	        return this.$('.add').hide();
	      },
	      'deselect:one': function() {
	        return this.$('.add').hide();
	      }
	    };

	    return PostContentAddView;

	  })(Marionette.CompositeView);
	  return Feed;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 52:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(53)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Prompt) {
	  var controller;
	  __webpack_require__(55);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.show = function(params, callback) {
	      var model, view;
	      model = new Backbone.Model(params);
	      view = new Prompt.View({
	        model: model
	      });
	      this.listenTo(view, 'complete:prompt', function() {
	        var val;
	        val = view.$('input').val();
	        if (val.length) {
	          view.destroy();
	          if (callback) {
	            return callback(val);
	          }
	        }
	      });
	      return app.popups.show(view);
	    };

	    return controller;

	  })(Marionette.Controller);
	  Prompt.Controller = new controller;
	  return Prompt;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 53:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Prompt;
	  Prompt = {};
	  Prompt.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = function(data) {
	      return __webpack_require__(54)(data);
	    };

	    View.prototype.className = 'prompt-overlay';

	    View.prototype.triggers = {
	      'click .submit-prompt': 'complete:prompt'
	    };

	    View.prototype.events = {
	      'keyup input': function(e) {
	        var val;
	        val = $(e.target).val();
	        if (val.length) {
	          this.$('.submit-prompt').show();
	        } else {
	          this.$('.submit-prompt').hide();
	        }
	        if (e.keyCode === 13) {
	          return this.trigger('complete:prompt');
	        }
	      },
	      'click .close': function() {
	        return this.destroy();
	      },
	      'click': function() {
	        return this.destroy();
	      },
	      'click .prompt-window': function(e) {
	        return e.stopPropagation();
	      }
	    };

	    View.prototype.onShow = function() {
	      return this.$('input').focus();
	    };

	    return View;

	  })(Marionette.ItemView);
	  return Prompt;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (message, placeholder, title) {
	buf.push("<div class=\"prompt-window\"><i class=\"close\"></i><h1>" + (((jade_interp = title) == null ? '' : jade_interp)) + "</h1><p>" + (((jade_interp = message) == null ? '' : jade_interp)) + "</p><input type=\"text\"" + (jade.attr("placeholder", "" + (placeholder) + "", true, true)) + "><div class=\"submit-prompt\">Сохранить</div></div>");}.call(this,"message" in locals_for_with?locals_for_with.message:typeof message!=="undefined"?message:undefined,"placeholder" in locals_for_with?locals_for_with.placeholder:typeof placeholder!=="undefined"?placeholder:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
	}

/***/ },

/***/ 55:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 56:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});