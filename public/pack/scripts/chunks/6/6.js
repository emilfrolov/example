webpackJsonp([6],{

/***/ 69:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(70)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Video) {
	  var controller;
	  __webpack_require__(71);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      var showVideos, videos;
	      showVideos = function(items) {
	        var view;
	        view = new Video.Items({
	          collection: items
	        });
	        return app.content.show(view);
	      };
	      videos = app.request('get:videos');
	      if (videos.promise) {
	        videos.done(function(items) {
	          return showVideos(items);
	        });
	        return videos.fail(function() {});
	      } else {
	        return showVideos(videos);
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Video.Controller = new controller;
	  return Video;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 70:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Video;
	  Video = {};
	  Video.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#video-page-item-tpl';

	    Item.prototype.className = 'video-item';

	    Item.prototype.events = {
	      'click': function() {
	        return app.Parts.VideoPlayer.Controller.openVideo(this.model);
	      }
	    };

	    return Item;

	  })(Marionette.ItemView);
	  Video.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.childView = Video.Item;

	    Items.prototype.className = 'video-page page';

	    Items.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return Items;

	  })(Marionette.CollectionView);
	  return Video;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 71:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});