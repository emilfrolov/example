webpackJsonp([5],{

/***/ 66:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(67), __webpack_require__(30), __webpack_require__(28)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Playlists, Loader, Update) {
	  var controller;
	  __webpack_require__(68);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      var loader, playlists, self;
	      self = this;
	      playlists = app.request('get:playlists');
	      if (playlists.promise) {
	        loader = new Loader.View({
	          styles: {
	            'position': 'absolute',
	            'top': '50%',
	            'transform': 'translateY(-50%)',
	            'left': '50%',
	            'transform': 'translateX(-50%)'
	          }
	        });
	        app.content.show(loader);
	        playlists.done((function(_this) {
	          return function(collection) {
	            if (location.href.indexOf('/playlists')) {
	              return _this.showPlaylistsList(collection);
	            }
	          };
	        })(this));
	        return playlists.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return app.trigger('show:playlists');
	            };
	            update = new Update.View({
	              container: 'slider-update',
	              styles: {
	                'margin-top': '200px'
	              },
	              callback: callback
	            });
	            return _this.layout.content.show(update);
	          };
	        })(this));
	      } else {
	        return this.showPlaylistsList(playlists);
	      }
	    };

	    controller.prototype.showPlaylistsList = function(collection) {
	      var playlistsList, startFetch, view, winHeight;
	      $(window).off('.new');
	      startFetch = function() {
	        var fetch, offset;
	        $(window).off('.new');
	        offset = playlistsList.length;
	        fetch = playlistsList.fetch({
	          data: {
	            offset: offset,
	            count: 60
	          },
	          remove: false
	        });
	        fetch.done(function(newcollection) {
	          var winHeight;
	          if (newcollection.searched.length) {
	            winHeight = $('body').height();
	            return $(window).on('scroll.new', function() {
	              var scroll;
	              scroll = $(window).scrollTop() + $(window).height() * 5;
	              if (winHeight < scroll) {
	                return startFetch();
	              }
	            });
	          }
	        });
	        return fetch.fail(function(res) {
	          if (res.status === 422) {
	            return contacts.add(res.responseJSON);
	          } else {
	            return console.log('Произошла ошибка');
	          }
	        });
	      };
	      playlistsList = app.request('get:temp:playlists', collection);
	      view = new Playlists.List({
	        collection: playlistsList
	      });
	      app.content.show(view);
	      winHeight = $('body').height();
	      return $(window).on('scroll.new', function() {
	        var scroll;
	        scroll = $(window).scrollTop() + $(window).height() * 5;
	        if (winHeight < scroll) {
	          return startFetch();
	        }
	      });
	    };

	    return controller;

	  })(Marionette.Controller);
	  Playlists.Controller = new controller;
	  return Playlists;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 67:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(37)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Playlist) {
	  var Playlists;
	  Playlists = {};
	  Playlists.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#playlist-item-tpl';

	    Item.prototype.className = 'playlist-item album-item';

	    Item.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('image'), '300x300');
	      return img.src = link;
	    };

	    Item.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return Item;

	  })(Playlist.Item);
	  Playlists.List = (function(superClass) {
	    extend(List, superClass);

	    function List() {
	      return List.__super__.constructor.apply(this, arguments);
	    }

	    List.prototype.childView = Playlists.Item;

	    List.prototype.className = 'playlists-wrapper playlists-page';

	    return List;

	  })(Marionette.CollectionView);
	  return Playlists;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 68:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});