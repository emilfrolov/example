webpackJsonp([7],{

/***/ 72:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(73), __webpack_require__(30), __webpack_require__(28)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Profile, Loader, Update) {
	  var controller;
	  __webpack_require__(74);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function(tab) {
	      if (tab == null) {
	        tab = "main";
	      }
	      this.layout = new Profile.Layout;
	      app.content.show(this.layout);
	      this.showMenu();
	      this.setActiveMenuTab(tab);
	      switch (tab) {
	        case "subscribe":
	          return this.showSubsPage();
	        case "followers":
	          return this.showFollowersPage();
	        case "followed":
	          return this.showFollowedPage();
	        case "promo":
	          return this.showPromoPage();
	        case "main":
	          return this.showMainTab();
	      }
	    };

	    controller.prototype.setActiveMenuTab = function(tab) {
	      var link;
	      $('.profile-menu .menu-item.active').removeClass('active');
	      $('.profile-menu .menu-item.' + tab).addClass('active');
	      link = tab === 'main' ? '' : tab;
	      return app.navigate('/profile/' + link);
	    };

	    controller.prototype.showMenu = function() {
	      var menu, view;
	      menu = app.request('get:profile:menu');
	      view = new Profile.Menu({
	        collection: menu
	      });
	      return this.layout.menu.show(view);
	    };

	    controller.prototype.showMainTab = function() {
	      var loader, self, user;
	      self = this;
	      user = app.request('get:user');
	      loader = new Loader.View({
	        styles: {
	          'top': $(window).height() / 2
	        }
	      });
	      this.layout.content.show(loader);
	      user.done(function(user) {
	        var view;
	        view = new Profile.MainTab({
	          model: user
	        });
	        return self.layout.content.show(view);
	      });
	      return user.fail(function() {
	        var callback, update;
	        callback = function() {
	          return app.trigger('show:profile', 'main');
	        };
	        update = new Update.View({
	          container: 'slider-update',
	          styles: {
	            top: $(window).height() / 2
	          },
	          callback: callback
	        });
	        return self.layout.content.show(update);
	      });
	    };

	    controller.prototype.showPromoPage = function() {
	      var view;
	      view = new Profile.PromoTab;
	      return this.layout.content.show(view);
	    };

	    controller.prototype.showFollowersPage = function() {
	      var self;
	      self = this;
	      return app.request('get:user').done(function(user) {
	        return self.showUserFollowers(user);
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.showFollowedPage = function() {
	      var self;
	      self = this;
	      return app.request('get:user').done(function(user) {
	        return self.showUserFollowed(user);
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.showUserFollowers = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(followers) {
	        var collection, view;
	        collection = new Backbone.Collection(followers);
	        view = new Profile.FollowersTab({
	          collection: collection
	        });
	        return self.layout.content.show(view);
	      };
	      if (user.has('followers')) {
	        return showContent(user.get('followers'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.layout.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getFollowers", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          var req, usersData;
	          usersData = {
	            userIds: data.user_ids
	          };
	          req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicProfiles"), usersData);
	          return req.done(function(data) {
	            showContent(data.users);
	            return user.set('followers', data.users);
	          });
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showUserFollowed = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(followed) {
	        var collection, view;
	        collection = new Backbone.Collection(followed);
	        view = new Profile.FollowedTab({
	          collection: collection
	        });
	        return self.layout.content.show(view);
	      };
	      if (user.has('followed')) {
	        return showContent(user.get('followed'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.layout.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getFollowedUsers", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          var req, usersData;
	          usersData = {
	            userIds: data.user_ids
	          };
	          req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicProfiles"), usersData);
	          return req.done(function(data) {
	            showContent(data.users);
	            return user.set('followed', data.users);
	          });
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showSubsPage = function() {
	      var loader, self, user;
	      self = this;
	      user = app.request('get:user');
	      loader = new Loader.View({
	        styles: {
	          'top': $(window).height() / 2
	        }
	      });
	      this.layout.content.show(loader);
	      user.done(function(user) {
	        var view;
	        view = new Profile.SubsTab({
	          model: user
	        });
	        return self.layout.content.show(view);
	      });
	      return user.fail(function() {
	        var callback, update;
	        callback = function() {
	          return app.trigger('show:profile', 'subscribe');
	        };
	        update = new Update.View({
	          container: 'slider-update',
	          styles: {
	            top: $(window).height() / 2
	          },
	          callback: callback
	        });
	        return self.layout.content.show(update);
	      });
	    };

	    controller.prototype.openSubscribeConfirm = function(model) {
	      var view;
	      view = new Profile.ConfirmSubscribe({
	        model: model
	      });
	      return this.layout.popup.show(view);
	    };

	    return controller;

	  })(Marionette.Controller);
	  Profile.Controller = new controller;
	  return Profile;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 73:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Profile;
	  Profile = {};
	  Profile.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#profile-layout-tpl';

	    Layout.prototype.className = 'profile-page';

	    Layout.prototype.regions = {
	      menu: '.menu-region',
	      content: '.tab-content',
	      popup: '.popup'
	    };

	    Layout.prototype.onShow = function() {
	      $('.profile-page .sidebar').height($(window).height() - 350);
	      return $(window).on('resize.profile', function() {
	        return $('.profile-page .sidebar').height($(window).height() - 350);
	      });
	    };

	    Layout.prototype.onDestroy = function() {
	      return $(window).off('resize.profile');
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Profile.MainTab = (function(superClass) {
	    extend(MainTab, superClass);

	    function MainTab() {
	      return MainTab.__super__.constructor.apply(this, arguments);
	    }

	    MainTab.prototype.template = '#profile-main-tpl';

	    MainTab.prototype.regions = {
	      cpcontent: '.change-pass-container'
	    };

	    MainTab.prototype.events = {
	      'click input': function(e) {
	        return $(e.target).select();
	      },
	      'blur .personal-block input': function(e) {
	        var input;
	        input = $(e.target);
	        return input.val(input.attr('data-val'));
	      },
	      'keyup .personal-block input': function(e) {
	        var input;
	        if (e.keyCode === 13) {
	          this.saveUserData();
	          input = $(e.target);
	          return input.blur();
	        } else if (e.keyCode === 27) {
	          input = $(e.target);
	          return input.blur();
	        }
	      },
	      'click .change-password': function() {
	        this.cpview = new Profile.ChangePass({
	          model: this.model
	        });
	        return this.cpcontent.show(this.cpview);
	      },
	      'keyup .change-pass-container .oldPass': function(e) {
	        if (e.keyCode === 13) {
	          if ($.trim($(e.target).val()) !== '') {
	            $('.change-pass-container .newPass').focus();
	          }
	          return false;
	        } else if (e.keyCode === 27) {
	          return this.closeChangePassWindow();
	        }
	      },
	      'keyup .change-pass-container .newPass': function(e) {
	        if (e.keyCode === 13) {
	          if ($.trim($(e.target).val()) !== '') {
	            return $('.change-pass-container .tryNewPass').focus();
	          }
	        } else if (e.keyCode === 27) {
	          return this.closeChangePassWindow();
	        }
	      },
	      'keyup .change-pass-container .tryNewPass': function(e) {
	        var newPass, newPassr, oldpass, self;
	        if (e.keyCode === 13) {
	          self = this;
	          e.preventDefault();
	          oldpass = $('.change-pass-container .oldPass').val();
	          newPass = this.$('.change-pass-container  .newPass').val();
	          newPassr = this.$('.change-pass-container .tryNewPass').val();
	          if (newPass !== newPassr) {
	            app.Parts.Notification.Controller.show({
	              message: app.locale.profile.pIs,
	              type: 'error',
	              time: 3000
	            });
	            return false;
	          }
	          return $.get(app.getRequestUrlSid("/compatibility/beeline-user/changePassword", "?old_password=" + oldpass + "&new_password=" + newPass), function(res) {
	            if (res.status === 'ok') {
	              self.closeChangePassWindow();
	              return app.Parts.Notification.Controller.show({
	                message: app.locale.profile.pSc,
	                type: 'success',
	                time: 3000
	              });
	            } else {
	              return app.Parts.Notification.Controller.show({
	                message: res.message,
	                type: 'error',
	                time: 3000
	              });
	            }
	          });
	        } else if (e.keyCode === 27) {
	          return this.closeChangePassWindow();
	        }
	      },
	      'change .upload-avatar input': function(e) {
	        var form, self, xhr;
	        self = this;
	        this.$('.upload-avatar').addClass('loading');
	        xhr = new XMLHttpRequest;
	        xhr.open('post', app.getRequestUrlSid('/compatibility/user/changeavatar'), true);
	        xhr.onload = xhr.onerror = function() {
	          var res;
	          res = JSON.parse(this.response);
	          if (res.code === 0) {
	            self.$('.upload-avatar').removeClass('loading');
	            return app.request('get:user').done(function(user) {
	              user.set('avatar', res.url);
	              return self.model.set('avatar', res.url);
	            });
	          } else {
	            return console.log(res);
	          }
	        };
	        form = new FormData;
	        form.append('avatar', e.target.files[0]);
	        return xhr.send(form);
	      },
	      'click .exit': function(e) {
	        e.stopPropagation();
	        return $.get('/exit', function() {
	          return location.href = '';
	        });
	      }
	    };

	    MainTab.prototype.modelEvents = {
	      'change': function() {
	        return this.render();
	      }
	    };

	    MainTab.prototype.closeChangePassWindow = function() {
	      return this.cpview.destroy();
	    };

	    MainTab.prototype.saveUserData = function() {
	      var data, email, name, tmpEmail, tmpName, userUpdate;
	      name = $('.personal-block .name');
	      email = $('.personal-block .mail');
	      tmpName = name.attr('data-val');
	      tmpEmail = email.attr('data-val');
	      data = {};
	      data.name = name.val();
	      data.email = email.val();
	      name.attr('data-val', data.name);
	      email.attr('data-val', data.email);
	      userUpdate = $.ajax({
	        type: "GET",
	        url: app.getRequestUrlSid("/compatibility/user/updateProfile"),
	        data: data
	      });
	      userUpdate.done(function() {
	        app.request('get:user').done(function(user) {
	          return user.fetch();
	        });
	        return app.Parts.Notification.Controller.show({
	          message: app.locale.profile.dSch,
	          type: 'success',
	          time: 3000
	        });
	      });
	      return userUpdate.fail(function() {
	        name.val(tmpName).attr('data-val', tmpName);
	        email.val(tmpEmail).attr('data-val', tmpEmail);
	        return app.Parts.Notification.Controller.show({
	          message: app.locale.profile.dChErr,
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    return MainTab;

	  })(Marionette.LayoutView);
	  Profile.ConfirmSubscribe = (function(superClass) {
	    extend(ConfirmSubscribe, superClass);

	    function ConfirmSubscribe() {
	      return ConfirmSubscribe.__super__.constructor.apply(this, arguments);
	    }

	    ConfirmSubscribe.prototype.template = '#subscribe-confirm-window-tpl';

	    ConfirmSubscribe.prototype.className = 'subscribe-confirm-overlay';

	    ConfirmSubscribe.prototype.events = {
	      'click .subscribe': function() {
	        $('.subscribe-page .toggleConnect').text(app.locale.profile.connect + '...').addClass('disable').removeClass('toggleConnect');
	        this.destroy();
	        return $.get(app.getRequestUrlSid('/compatibility/beeline-user/musicSubscribe', '?channel=44cb0d404ecdf6e39c52a2239cf6b3681ce7ed2e74f5136b78c91b7f599e4d0e'), function() {
	          app.Log.Controller.createNewLog(app.getRequestUrlSid("/generalV1/log/subscribe", "?referer=profile"));
	          setTimeout(function() {
	            return app.request('fetch:user');
	          }, 5000);
	          return app.GAPageAction('Подписка', 'Подключение');
	        });
	      },
	      'click .unsubscribe': function() {
	        $('.subscribe-page .toggleConnect').text(app.locale.profile.disconnect + '...').addClass('disable').removeClass('toggleConnect');
	        this.destroy();
	        return $.get(app.getRequestUrlSid('/compatibility/beeline-user/musicUnsubscribe', '?channel=44cb0d404ecdf6e39c52a2239cf6b3681ce7ed2e74f5136b78c91b7f599e4d0e'), function() {
	          app.request('fetch:user');
	          return app.GAPageAction('Подписка', 'Отключение');
	        });
	      },
	      'click .close': function() {
	        return this.destroy();
	      },
	      'click .subscribe-confirm-window': function() {
	        return false;
	      },
	      'click': function() {
	        return this.destroy();
	      }
	    };

	    return ConfirmSubscribe;

	  })(Marionette.ItemView);
	  Profile.SubsTab = (function(superClass) {
	    extend(SubsTab, superClass);

	    function SubsTab() {
	      return SubsTab.__super__.constructor.apply(this, arguments);
	    }

	    SubsTab.prototype.className = 'subscribe-page';

	    SubsTab.prototype.template = '#profile-subscribe-tpl';

	    SubsTab.prototype.events = {
	      'click .toggleConnect': function() {
	        return Profile.Controller.openSubscribeConfirm(this.model);
	      }
	    };

	    SubsTab.prototype.modelEvents = {
	      'change': function() {
	        return this.render();
	      }
	    };

	    return SubsTab;

	  })(Marionette.ItemView);
	  Profile.ChangePass = (function(superClass) {
	    extend(ChangePass, superClass);

	    function ChangePass() {
	      return ChangePass.__super__.constructor.apply(this, arguments);
	    }

	    ChangePass.prototype.template = '#change-password-tpl';

	    ChangePass.prototype.className = 'change-password-wrapp';

	    ChangePass.prototype.onShow = function() {
	      $('.change-password').hide();
	      $('.renew-password').hide();
	      return this.$('.oldPass').focus();
	    };

	    ChangePass.prototype.onDestroy = function() {
	      $('.change-password').show();
	      return $('.renew-password').show();
	    };

	    return ChangePass;

	  })(Marionette.ItemView);
	  Profile.MenuItem = (function(superClass) {
	    extend(MenuItem, superClass);

	    function MenuItem() {
	      return MenuItem.__super__.constructor.apply(this, arguments);
	    }

	    MenuItem.prototype.template = '#profile-menu-item-tpl';

	    MenuItem.prototype.tagName = 'a';

	    MenuItem.prototype.onRender = function() {
	      return this.$el.attr('href', '/profile/' + this.model.get('tab'));
	    };

	    MenuItem.prototype.className = function() {
	      return "menu-item " + (this.model.get('tab'));
	    };

	    MenuItem.prototype.events = {
	      'click': function(e) {
	        e.preventDefault();
	        if (!this.$el.hasClass('active')) {
	          return Profile.Controller.showPage(this.model.get('tab'));
	        }
	      }
	    };

	    return MenuItem;

	  })(Marionette.ItemView);
	  Profile.Menu = (function(superClass) {
	    extend(Menu, superClass);

	    function Menu() {
	      return Menu.__super__.constructor.apply(this, arguments);
	    }

	    Menu.prototype.childView = Profile.MenuItem;

	    Menu.prototype.className = 'profile-menu';

	    return Menu;

	  })(Marionette.CollectionView);
	  Profile.PromoTab = (function(superClass) {
	    extend(PromoTab, superClass);

	    function PromoTab() {
	      return PromoTab.__super__.constructor.apply(this, arguments);
	    }

	    PromoTab.prototype.template = '#profile-promo-tpl';

	    PromoTab.prototype.className = 'profile-promo';

	    PromoTab.prototype.events = {
	      'click .activate-promo-code': function() {
	        var val;
	        val = $.trim($('.promo-code input').val());
	        if (val !== '') {
	          return $.get(app.getRequestUrlSid('/beelineMusicV1/promo-code/activate', "?code=" + val)).done(function(data) {
	            if (data.code === 0) {
	              return app.Parts.Notification.Controller.show({
	                message: 'Код успешно активирован',
	                type: 'success',
	                time: 3000
	              });
	            } else {
	              return app.Parts.Notification.Controller.show({
	                message: data.message,
	                type: 'error',
	                time: 3000
	              });
	            }
	          }).fail(function(res) {
	            return app.Parts.Notification.Controller.show({
	              message: 'Ошибка активации кода',
	              type: 'error',
	              time: 3000
	            });
	          });
	        }
	      }
	    };

	    return PromoTab;

	  })(Marionette.ItemView);
	  Profile.FollowUser = (function(superClass) {
	    extend(FollowUser, superClass);

	    function FollowUser() {
	      return FollowUser.__super__.constructor.apply(this, arguments);
	    }

	    FollowUser.prototype.className = 'user-follower';

	    FollowUser.prototype.template = '#user-follower-tpl';

	    return FollowUser;

	  })(Marionette.ItemView);
	  Profile.FollowersTab = (function(superClass) {
	    extend(FollowersTab, superClass);

	    function FollowersTab() {
	      return FollowersTab.__super__.constructor.apply(this, arguments);
	    }

	    FollowersTab.prototype.childView = Profile.FollowUser;

	    FollowersTab.prototype.className = 'followers-page';

	    FollowersTab.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return FollowersTab;

	  })(Marionette.CollectionView);
	  Profile.FollowedTab = (function(superClass) {
	    extend(FollowedTab, superClass);

	    function FollowedTab() {
	      return FollowedTab.__super__.constructor.apply(this, arguments);
	    }

	    FollowedTab.prototype.childView = Profile.FollowUser;

	    FollowedTab.prototype.className = 'followers-page';

	    FollowedTab.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return FollowedTab;

	  })(Marionette.CollectionView);
	  return Profile;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 74:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});