webpackJsonp([4],{

/***/ 60:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(61), __webpack_require__(28), __webpack_require__(30), __webpack_require__(62), __webpack_require__(22), __webpack_require__(65), __webpack_require__(26)], __WEBPACK_AMD_DEFINE_RESULT__ = function(New, Update, Loader, Slider, Track, EntitiesTrack, Album) {
	  var controller;
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      var slider;
	      this.layout = new New.Layout;
	      app.content.show(this.layout);
	      slider = new Slider;
	      slider.drawSlider(this.layout.slider.el);
	      this.listenTo(this.layout, 'destroy', function() {
	        return slider.remove();
	      });
	      if (this.albumsIsShow) {
	        return this.showAlbums();
	      } else {
	        return this.showTracks();
	      }
	    };

	    controller.prototype.showTracks = function() {
	      var loader, tracks;
	      $('.content-tab .albums').removeClass('active');
	      $('.content-tab .tracks').addClass('active');
	      this.albumsIsShow = false;
	      tracks = app.request('get:new:tracks');
	      if (tracks.promise) {
	        loader = new Loader.View({
	          styles: {
	            'margin-top': '200px'
	          }
	        });
	        this.layout.content.show(loader);
	        tracks.done((function(_this) {
	          return function(collection) {
	            if ($('.new-page .content-tab .tracks').hasClass('active')) {
	              return _this.showTracksOne(collection);
	            }
	          };
	        })(this));
	        return tracks.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return app.trigger('show:new');
	            };
	            update = new Update.View({
	              container: 'slider-update',
	              styles: {
	                'margin-top': '200px'
	              },
	              callback: callback
	            });
	            return _this.layout.content.show(update);
	          };
	        })(this));
	      } else {
	        return this.showTracksOne(tracks);
	      }
	    };

	    controller.prototype.showTracksOne = function(collection) {
	      var fetchTracks, trackList, tracksView, winHeight;
	      $(window).off('.new');
	      fetchTracks = function() {
	        var page, trackFetch;
	        $(window).off('.new');
	        page = Math.ceil(trackList.length / 30) + 1;
	        trackFetch = trackList.fetch({
	          data: {
	            type: 'tracks',
	            sort: 'date',
	            page: page,
	            countitems: 90
	          },
	          remove: false
	        });
	        trackFetch.done(function(newcollection) {
	          var winHeight;
	          app.checkActiveTrack();
	          if (newcollection.items.length) {
	            winHeight = $('body').height();
	            return $(window).on('scroll.new', function() {
	              var scroll;
	              scroll = $(window).scrollTop() + $(window).height() * 5;
	              if (winHeight < scroll) {
	                return fetchTracks();
	              }
	            });
	          }
	        });
	        return trackFetch.fail(function(res) {
	          if (res.status === 422) {
	            return contacts.add(res.responseJSON);
	          } else {
	            return console.log('Произошла ошибка');
	          }
	        });
	      };
	      trackList = new EntitiesTrack.Items(collection.toJSON());
	      tracksView = new Track.ListView({
	        collection: trackList
	      });
	      this.layout.content.show(tracksView);
	      app.checkActiveTrack();
	      winHeight = $('body').height();
	      return $(window).on('scroll.new', function() {
	        var scroll;
	        scroll = $(window).scrollTop() + $(window).height() * 5;
	        if (winHeight < scroll) {
	          return fetchTracks();
	        }
	      });
	    };

	    controller.prototype.showAlbums = function() {
	      var albums, loader;
	      this.albumsIsShow = true;
	      $('.content-tab .tracks').removeClass('active');
	      $('.content-tab .albums').addClass('active');
	      albums = app.request('get:new:albums');
	      if (albums.promise) {
	        loader = new Loader.View({
	          styles: {
	            'margin-top': '200px'
	          }
	        });
	        this.layout.content.show(loader);
	        albums.done((function(_this) {
	          return function(collection) {
	            if ($('.new-page .content-tab .albums').hasClass('active')) {
	              return _this.showAlbumsOne(collection);
	            }
	          };
	        })(this));
	        return albums.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return app.trigger('show:new');
	            };
	            update = new Update.View({
	              container: 'slider-update',
	              styles: {
	                'margin-top': '200px'
	              },
	              callback: callback
	            });
	            return _this.layout.content.show(update);
	          };
	        })(this));
	      } else {
	        return this.showAlbumsOne(albums);
	      }
	    };

	    controller.prototype.showAlbumsOne = function(collection) {
	      var albumList, albumView, fetchAlbums, winHeight;
	      $(window).off('.new');
	      fetchAlbums = function() {
	        var albumFetch, page;
	        $(window).off('.new');
	        page = Math.ceil(albumList.length / 30) + 1;
	        albumFetch = albumList.fetch({
	          data: {
	            type: 'albums',
	            sort: 'date',
	            page: page,
	            countitems: 40
	          },
	          remove: false
	        });
	        albumFetch.done(function(newcollection) {
	          var winHeight;
	          app.checkActiveAlbum();
	          if (newcollection.items.length) {
	            winHeight = $('body').height();
	            return $(window).on('scroll.new', function() {
	              var scroll;
	              scroll = $(window).scrollTop() + $(window).height() * 5;
	              if (winHeight < scroll) {
	                return fetchAlbums();
	              }
	            });
	          }
	        });
	        return albumFetch.fail(function(res) {
	          if (res.status === 422) {
	            return contacts.add(res.responseJSON);
	          } else {
	            return console.log('Произошла ошибка');
	          }
	        });
	      };
	      albumList = app.request('get:temp:albums', collection);
	      albumView = new Album.ListView({
	        collection: albumList
	      });
	      this.layout.content.show(albumView);
	      app.checkActiveAlbum();
	      winHeight = $('body').height();
	      return $(window).on('scroll.new', function() {
	        var scroll;
	        scroll = $(window).scrollTop() + $(window).height() * 5;
	        if (winHeight < scroll) {
	          return fetchAlbums();
	        }
	      });
	    };

	    return controller;

	  })(Marionette.Controller);
	  New.Controller = new controller;
	  return New;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 61:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var New;
	  New = {};
	  New.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#new-content-layout';

	    Layout.prototype.className = 'page new-page';

	    Layout.prototype.regions = {
	      slider: '.slider',
	      content: '.content'
	    };

	    Layout.prototype.events = {
	      'click .content-tab .tracks': function() {
	        if (!this.$('.content-tab .tracks').hasClass('active')) {
	          return New.Controller.showTracks();
	        }
	      },
	      'click .content-tab .albums': function() {
	        if (!this.$('.content-tab .albums').hasClass('active')) {
	          return New.Controller.showAlbums();
	        }
	      }
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  return New;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 62:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(63), __webpack_require__(30)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Slider, Loader) {
	  var Controller;
	  __webpack_require__(64);
	  Controller = (function(superClass) {
	    extend(Controller, superClass);

	    function Controller() {
	      return Controller.__super__.constructor.apply(this, arguments);
	    }

	    Controller.prototype.drawSlider = function(selector) {
	      var loader, showSliderOne, slides;
	      slides = app.request('get:slides');
	      this.rm = new Marionette.RegionManager;
	      this.sliderContainer = this.rm.addRegion('sliderContainer', selector);
	      showSliderOne = (function(_this) {
	        return function(collection) {
	          _this.sliderView = new Slider.SliderItems({
	            collection: collection
	          });
	          _this.listenTo(_this.sliderView, 'show', function() {
	            var self;
	            self = this;
	            this.slide = {};
	            this.sliderContainer.$el.find('.slider-cut').mousedown(function(e) {
	              var side;
	              e.preventDefault();
	              self.slide.startPos = e.pageX;
	              self.slide.transform = self.margin;
	              side = null;
	              $(document).on('mousemove.sliderup', function(e) {
	                var curPos, posX;
	                e.preventDefault();
	                posX = e.pageX - self.slide.startPos;
	                if (posX > 10 || posX < -10) {
	                  app.slideMove = true;
	                  if (posX < 0) {
	                    side = 'left';
	                  } else {
	                    side = 'right';
	                  }
	                  curPos = self.margin + posX;
	                  $('.slider-scroll').css({
	                    'margin-left': curPos + 'px'
	                  });
	                  self.slide.transform = curPos;
	                }
	                return false;
	              });
	              return $(document).on('mouseup.sliderup', function(e) {
	                var curPos, posX, state;
	                setTimeout(function() {
	                  return app.slideMove = false;
	                }, 100);
	                if (side === 'right') {
	                  self.slide.listener = true;
	                  posX = e.pageX;
	                  curPos = self.slide.transform + posX;
	                  state = ~Math.ceil(curPos / self.sliderWidth);
	                  if (state) {
	                    self.prewSlide(self.slide.transform);
	                  } else {
	                    self.returnSlide();
	                  }
	                } else if (side === 'left') {
	                  self.slide.listener = true;
	                  posX = e.pageX;
	                  curPos = self.slide.transform - posX;
	                  state = ~Math.ceil(curPos / (self.sliderWidth - 300));
	                  if (state) {
	                    self.nextSlide();
	                  } else {
	                    self.returnSlide();
	                  }
	                } else {
	                  self.returnSlide();
	                }
	                return $(document).off('.sliderup');
	              });
	            });
	            this.sliderContainer.$el.find('.slider-cut').on('mousedown.slider', function() {
	              return self.stopSlider();
	            });
	            this.sliderContainer.$el.find('.slider-cut').on('mouseup.slider', function() {
	              return self.startSlider();
	            });
	            return $(window).on('resize.slide', function() {
	              var wHeight, wWidth;
	              wWidth = $(window).width();
	              wHeight = $(window).height();
	              if (wWidth <= 1024) {
	                wWidth = 1024;
	              }
	              if (wHeight < 800) {
	                if (self.size !== 'small') {
	                  self.resizeSlider();
	                  self.setSliderScrollWidth();
	                }
	              } else {
	                if (self.size !== 'big') {
	                  self.resizeSlider();
	                  self.setSliderScrollWidth();
	                }
	              }
	              return self.sliderView.$el.width(wWidth);
	            });
	          });
	          _this.listenTo(_this.sliderView, 'before:show', function() {
	            return this.resizeSlider();
	          });
	          return _this.sliderContainer.show(_this.sliderView);
	        };
	      })(this);
	      if (slides.promise) {
	        loader = new Loader.View({
	          container: 'slider-loader',
	          styles: {
	            top: '50%'
	          }
	        });
	        this.sliderContainer.show(loader);
	        slides.done((function(_this) {
	          return function(collection) {
	            return showSliderOne(collection);
	          };
	        })(this));
	        return slides.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return app.trigger('show:new');
	            };
	            update = new Update.View({
	              container: 'slider-update',
	              styles: {
	                top: '50%'
	              },
	              callback: callback
	            });
	            return _this.sliderContainer.show(update);
	          };
	        })(this));
	      } else {
	        return showSliderOne(slides);
	      }
	    };

	    Controller.prototype.remove = function() {
	      this.stopSlider();
	      return $(window).off('.slide');
	    };

	    Controller.prototype.prewSlide = function(slide) {
	      var scrollElem;
	      this.margin = slide - this.sliderWidth;
	      scrollElem = $('.slider-scroll');
	      scrollElem.css({
	        'margin-left': this.margin + 'px'
	      });
	      scrollElem.children('li').eq(scrollElem.children('li').length - 1).prependTo(scrollElem);
	      this.margin = -this.sliderWidth;
	      return scrollElem.animate({
	        'margin-left': this.margin + 'px'
	      }, 200);
	    };

	    Controller.prototype.returnSlide = function() {
	      var scrollElem;
	      scrollElem = $('.slider-scroll');
	      return scrollElem.animate({
	        'margin-left': this.margin + 'px'
	      }, 200);
	    };

	    Controller.prototype.nextSlide = function() {
	      var scrollElem;
	      this.margin -= this.sliderWidth;
	      scrollElem = $('.slider-scroll');
	      return scrollElem.animate({
	        'margin-left': this.margin + 'px'
	      }, 200, (function(_this) {
	        return function() {
	          scrollElem.children('li').eq(0).appendTo(scrollElem);
	          _this.margin += _this.sliderWidth;
	          return scrollElem.css({
	            'margin-left': _this.margin + 'px'
	          });
	        };
	      })(this));
	    };

	    Controller.prototype.startSlider = function() {
	      this.stopSlider();
	      return this.timer = setInterval((function(_this) {
	        return function() {
	          return _this.nextSlide();
	        };
	      })(this), 5000);
	    };

	    Controller.prototype.stopSlider = function() {
	      return clearInterval(this.timer);
	    };

	    Controller.prototype.resizeSlider = function() {
	      this.stopSlider();
	      if ($(window).height() < 800) {
	        this.sliderWidth = 825;
	        this.size = 'small';
	      } else {
	        this.sliderWidth = 1160;
	        this.size = 'big';
	      }
	      this.margin = -this.sliderWidth;
	      this.sliderView.$childViewContainer.css({
	        'margin-left': +this.margin + 'px'
	      });
	      this.sliderView.$el.width($(window).width());
	      return this.startSlider();
	    };

	    Controller.prototype.setSliderScrollWidth = function() {};

	    Controller.prototype.setSliderScrollWidth = function() {
	      var width;
	      if ($(window).height() < 800) {
	        width = 831;
	      } else {
	        width = 1166;
	      }
	      return this.sliderContainer.$el.find('.slider-scroll').width($('.slider-scroll li').length * width);
	    };

	    return Controller;

	  })(Marionette.Controller);
	  return Controller;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 63:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Slider;
	  Slider = {};
	  Slider.SliderItem = (function(superClass) {
	    extend(SliderItem, superClass);

	    function SliderItem() {
	      return SliderItem.__super__.constructor.apply(this, arguments);
	    }

	    SliderItem.prototype.tagName = 'li';

	    SliderItem.prototype.template = '#slider-item-tpl';

	    SliderItem.prototype.events = {
	      'click': function() {
	        if (!app.slideMove) {
	          return app.trigger('show:album', this.model.get('promoId'));
	        }
	      }
	    };

	    return SliderItem;

	  })(Marionette.ItemView);
	  Slider.SliderItems = (function(superClass) {
	    extend(SliderItems, superClass);

	    function SliderItems() {
	      return SliderItems.__super__.constructor.apply(this, arguments);
	    }

	    SliderItems.prototype.className = 'slider-cut';

	    SliderItems.prototype.template = '#slider-layout-tpl';

	    SliderItems.prototype.childView = Slider.SliderItem;

	    SliderItems.prototype.childViewContainer = '.slider-scroll';

	    SliderItems.prototype.onRender = function() {
	      var width;
	      if ($(window).height() < 800) {
	        width = 831;
	      } else {
	        width = 1166;
	      }
	      return this.$childViewContainer.width(this.children.length * width);
	    };

	    return SliderItems;

	  })(Marionette.CompositeView);
	  return Slider;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 64:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});