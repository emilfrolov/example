webpackJsonp([11],{

/***/ 52:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(53)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Prompt) {
	  var controller;
	  __webpack_require__(55);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.show = function(params, callback) {
	      var model, view;
	      model = new Backbone.Model(params);
	      view = new Prompt.View({
	        model: model
	      });
	      this.listenTo(view, 'complete:prompt', function() {
	        var val;
	        val = view.$('input').val();
	        if (val.length) {
	          view.destroy();
	          if (callback) {
	            return callback(val);
	          }
	        }
	      });
	      return app.popups.show(view);
	    };

	    return controller;

	  })(Marionette.Controller);
	  Prompt.Controller = new controller;
	  return Prompt;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 53:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Prompt;
	  Prompt = {};
	  Prompt.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = function(data) {
	      return __webpack_require__(54)(data);
	    };

	    View.prototype.className = 'prompt-overlay';

	    View.prototype.triggers = {
	      'click .submit-prompt': 'complete:prompt'
	    };

	    View.prototype.events = {
	      'keyup input': function(e) {
	        var val;
	        val = $(e.target).val();
	        if (val.length) {
	          this.$('.submit-prompt').show();
	        } else {
	          this.$('.submit-prompt').hide();
	        }
	        if (e.keyCode === 13) {
	          return this.trigger('complete:prompt');
	        }
	      },
	      'click .close': function() {
	        return this.destroy();
	      },
	      'click': function() {
	        return this.destroy();
	      },
	      'click .prompt-window': function(e) {
	        return e.stopPropagation();
	      }
	    };

	    View.prototype.onShow = function() {
	      return this.$('input').focus();
	    };

	    return View;

	  })(Marionette.ItemView);
	  return Prompt;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 54:
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (message, placeholder, title) {
	buf.push("<div class=\"prompt-window\"><i class=\"close\"></i><h1>" + (((jade_interp = title) == null ? '' : jade_interp)) + "</h1><p>" + (((jade_interp = message) == null ? '' : jade_interp)) + "</p><input type=\"text\"" + (jade.attr("placeholder", "" + (placeholder) + "", true, true)) + "><div class=\"submit-prompt\">Сохранить</div></div>");}.call(this,"message" in locals_for_with?locals_for_with.message:typeof message!=="undefined"?message:undefined,"placeholder" in locals_for_with?locals_for_with.placeholder:typeof placeholder!=="undefined"?placeholder:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
	}

/***/ },

/***/ 55:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 84:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(85), __webpack_require__(30), __webpack_require__(28), __webpack_require__(52)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Tracks, Loader, Update, Prompt) {
	  var controller;
	  __webpack_require__(86);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      this.layout = new Tracks.Layout;
	      app.content.show(this.layout);
	      this.showUserPlaylist();
	      return this.showUserTracks();
	    };

	    controller.prototype.showUserPlaylist = function() {
	      var listLoader, playLists, self, showPlaylists;
	      self = this;
	      playLists = app.request('get:favorite:playlists');
	      showPlaylists = (function(_this) {
	        return function(collection) {
	          var playlistView;
	          playlistView = new Tracks.Playlists({
	            collection: collection
	          });
	          return _this.layout.list.show(playlistView);
	        };
	      })(this);
	      if (playLists.promise) {
	        listLoader = new Loader.View({
	          styles: {
	            'top': ($(window).height() / 2) - 200
	          }
	        });
	        this.layout.list.show(listLoader);
	        playLists.done(function(collection) {
	          return showPlaylists(collection);
	        });
	        return playLists.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return self.showUserPlaylist();
	            };
	            update = new Update.View({
	              styles: {
	                'top': ($(window).height() / 2) - 200
	              },
	              callback: callback
	            });
	            return _this.layout.list.show(update);
	          };
	        })(this));
	      } else {
	        return showPlaylists(playLists);
	      }
	    };

	    controller.prototype.showUserTracks = function() {
	      var loader, self, showTracks, tracks;
	      self = this;
	      tracks = app.request('get:favorite:tracks');
	      showTracks = (function(_this) {
	        return function(collection) {
	          var layout, tracksView;
	          layout = new Tracks.ItemsLayout;
	          _this.layout.content.show(layout);
	          _this.filteredTracks = app.Helpers.Filters.Collection.Filter({
	            collection: collection,
	            filterFunction: function(filterCriterion) {
	              var criterion;
	              criterion = filterCriterion.toLowerCase();
	              return function(contact) {
	                if (contact.get("artist").toLowerCase().indexOf(criterion) !== -1 || contact.get("title").toLowerCase().indexOf(criterion) !== -1) {
	                  return contact;
	                }
	              };
	            }
	          });
	          tracksView = new Tracks.Items({
	            collection: _this.filteredTracks
	          });
	          layout.tracks.show(tracksView);
	          app.checkActiveTrack();
	          return _this.listenTo(tracksView, 'destroy', function() {
	            return this.filteredTracks.stopListenCollection();
	          });
	        };
	      })(this);
	      if (tracks.promise) {
	        loader = new Loader.View({
	          styles: {
	            'top': ($(window).height() / 2) - 100
	          }
	        });
	        this.layout.content.show(loader);
	        tracks.done(function(collection) {
	          return showTracks(collection);
	        });
	        return tracks.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return self.showUserTracks();
	            };
	            update = new Update.View({
	              styles: {
	                'top': ($(window).height() / 2) - 100
	              },
	              callback: callback
	            });
	            return _this.layout.content.show(update);
	          };
	        })(this));
	      } else {
	        return showTracks(tracks);
	      }
	    };

	    controller.prototype.openPlaylist = function(item) {
	      var layout, loader, model, self, showPlaylist;
	      self = this;
	      model = item.model;
	      layout = new Tracks.PlaylistLayout({
	        model: model
	      });
	      this.listenTo(layout, 'show', function() {
	        this.resizeHeader();
	        $(window).on('resize.playlist', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	        return $(window).on('scroll.playlist', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	      });
	      this.listenTo(layout, 'destroy', function() {
	        return $(window).off('.playlist');
	      });
	      this.layout.content.show(layout);
	      loader = new Loader.View({
	        styles: {
	          'top': '50px'
	        }
	      });
	      layout.content.show(loader);
	      showPlaylist = function(tracks) {
	        var collection;
	        collection = new Backbone.Collection(tracks);
	        collection.each(function(item) {
	          item.set('uniq', 'playlist' + item.id);
	          item.set('playlistTrack', true);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: 'Перейти к альбому',
	              "class": app.locale.options.toAlb
	            }
	          ]);
	        });
	        tracks = new Tracks.PlaylistItems({
	          parentView: item,
	          model: item.model,
	          collection: collection
	        });
	        layout.content.show(tracks);
	        return app.checkActiveTrack();
	      };
	      if (model.get('items_count') === 0) {
	        return showPlaylist([]);
	      } else {
	        if (model.has('tracks')) {
	          return showPlaylist(model.get('tracks'));
	        } else {
	          if (this.playlistFetch) {
	            this.playlistFetch.abort();
	          }
	          this.playlistFetch = model.fetch();
	          this.playlistFetch.done(function() {
	            return showPlaylist(model.get('tracks'));
	          });
	          return this.playlistFetch.fail(function() {
	            var callback, update;
	            callback = function() {
	              return self.openPlaylist(item);
	            };
	            update = new Update.View({
	              callback: callback,
	              styles: {
	                'top': '50px'
	              }
	            });
	            return layout.content.show(update);
	          });
	        }
	      }
	    };

	    controller.prototype.openPlaylistOptions = function(view) {
	      var optionsView;
	      optionsView = new Tracks.PlaylistLayoutOptions({
	        model: view.model
	      });
	      return view.playlistOptions.show(optionsView);
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / pathLength;
	    };

	    controller.prototype.resizeHeader = function() {
	      var scrollHeight, scrollTop;
	      scrollHeight = 550;
	      if ($(window).height() + $(window).scrollTop() >= $('.user-playlist-page')[0].offsetHeight) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      scrollTop = scrollTop;
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      $('.playlist-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	      $('.playlist-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	      $('.playlist-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	      $('.playlist-head .info > span').css('opacity', 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)));
	      return $('.playlist-head').height(370 - (scrollTop * getHeaderCoeficent(scrollHeight, 190)));
	    };

	    controller.prototype.createPlaylist = function(title) {
	      var createPlaylist;
	      createPlaylist = function() {
	        var playlist;
	        playlist = app.request('get:favorite:playlists');
	        playlist.create({
	          'title': title
	        });
	        return app.GAPageAction('Создание плейлиста', title);
	      };
	      return app.request('get:user').done(function(user) {
	        var params, userName;
	        userName = user.get('profile_name');
	        if ((userName.length === 0) || (userName === null)) {
	          params = {
	            title: 'Как можно к вам<br>обратиться?',
	            message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
	            placeholder: 'Ваше имя'
	          };
	          return Prompt.Controller.show(params, function(name) {
	            var nameUpdate;
	            nameUpdate = $.ajax({
	              type: "GET",
	              url: app.getRequestUrlSid("/compatibility/user/updateProfile"),
	              data: {
	                name: name
	              }
	            });
	            return nameUpdate.done(function() {
	              user.set('profile_name', name);
	              return createPlaylist();
	            });
	          });
	        } else {
	          return createPlaylist();
	        }
	      }).fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.deletePlaylist = function(id) {
	      var playlist, playlists;
	      playlists = app.request('get:favorite:playlists');
	      playlist = playlists.findWhere({
	        id: id
	      });
	      if (playlist != null) {
	        return playlist.destroy();
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Tracks.Controller = new controller;
	  return Tracks;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 85:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(52)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track, Prompt) {
	  var Tracks;
	  Tracks = {};
	  Tracks.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#user-tracks-layout-tpl';

	    Layout.prototype.regions = {
	      content: '.content',
	      list: '.play-list-container'
	    };

	    Layout.prototype.className = 'user-tracks-page-wrapp';

	    Layout.prototype.onShow = function() {
	      var playlistContainer, toTop;
	      toTop = $('body').hasClass('notifi') ? 320 : 280;
	      playlistContainer = $('.play-list-container');
	      $('.user-tracks-page-wrapp .list').css('min-height', $(window).height() - 100);
	      playlistContainer.height($(window).height() - toTop);
	      $('body').css('background', '#111112');
	      return $(window).on('resize.userTracks', function() {
	        toTop = $('body').hasClass('notifi') ? 320 : 280;
	        $('.user-tracks-page-wrapp .list').css('min-height', $(window).height() - 100);
	        return playlistContainer.height($(window).height() - toTop);
	      });
	    };

	    Layout.prototype.onDestroy = function() {
	      $('body').attr('style', '');
	      return $(window).off('.userTracks');
	    };

	    Layout.prototype.events = {
	      'click .all-tracks': function() {
	        Tracks.Controller.showUserTracks();
	        $('.list-item.active').removeClass('active');
	        return this.$('.all-tracks').addClass('active');
	      },
	      'click .create-playlist': function() {
	        return $('.create-playlist p').empty().append('<input type="text" value="Новый плейлист">').find('input').select();
	      },
	      'focus .create-playlist input': function() {
	        return this.$('.create-playlist').addClass('edit').parent().addClass('edit');
	      },
	      'blur .create-playlist input': function() {
	        this.$('.create-playlist p').empty().text('Создать плейлист');
	        return this.$('.create-playlist').removeClass('edit').parent().removeClass('edit');
	      },
	      'keyup input': function(e) {
	        var val;
	        if (e.keyCode === 13) {
	          val = this.$('input').val();
	          if ($.trim(val) !== '') {
	            Tracks.Controller.createPlaylist(val);
	            return this.$('input').blur();
	          }
	        } else if (e.keyCode === 27) {
	          return this.$('input').blur();
	        }
	      }
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Tracks.Playlist = (function(superClass) {
	    extend(Playlist, superClass);

	    function Playlist() {
	      return Playlist.__super__.constructor.apply(this, arguments);
	    }

	    Playlist.prototype.template = '#user-playlist-item-tpl';

	    Playlist.prototype.className = 'user-playlist-item list-item';

	    Playlist.prototype.events = {
	      'click': function() {
	        Tracks.Controller.openPlaylist(this);
	        $('.list-item.active').removeClass('active');
	        return this.$el.addClass('active');
	      },
	      'focus input': function() {
	        return this.$el.addClass('edit').parent().addClass('edit');
	      },
	      'blur input': function() {
	        this.$('p').empty().text(this.model.get('title'));
	        return this.$el.removeClass('edit').parent().removeClass('edit');
	      },
	      'keyup input': function(e) {
	        var val;
	        if (e.keyCode === 13) {
	          val = this.$('input').val();
	          if ($.trim(val) !== '') {
	            this.model.set('title', this.$('input').val());
	            $('.playlist-head h1').text(this.model.get('title'));
	            return this.model.save();
	          }
	        } else if (e.keyCode === 27) {
	          return this.$('input').blur();
	        }
	      }
	    };

	    Playlist.prototype.modelEvents = {
	      'change title': function() {
	        return this.render();
	      }
	    };

	    return Playlist;

	  })(Marionette.ItemView);
	  Tracks.Playlists = (function(superClass) {
	    extend(Playlists, superClass);

	    function Playlists() {
	      return Playlists.__super__.constructor.apply(this, arguments);
	    }

	    Playlists.prototype.childView = Tracks.Playlist;

	    Playlists.prototype.className = 'container default-skin';

	    Playlists.prototype.onShow = function() {
	      return $('.user-tracks-page-wrapp .list .play-list-container').customScrollbar({
	        updateOnWindowResize: true
	      });
	    };

	    Playlists.prototype.collectionEvents = {
	      'change': function() {
	        return $('.user-tracks-page-wrapp .list .play-list-container').customScrollbar('resize', true);
	      }
	    };

	    Playlists.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.prepend(itemView.el);
	      };
	    };

	    return Playlists;

	  })(Marionette.CollectionView);
	  Tracks.ItemsLayout = (function(superClass) {
	    extend(ItemsLayout, superClass);

	    function ItemsLayout() {
	      return ItemsLayout.__super__.constructor.apply(this, arguments);
	    }

	    ItemsLayout.prototype.template = '#user-tracks-tpl';

	    ItemsLayout.prototype.className = 'user-tracks-wrapp';

	    ItemsLayout.prototype.regions = {
	      tracks: '.tracks-container'
	    };

	    ItemsLayout.prototype.events = {
	      'click .shuffle': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = $('.user-tracks-wrapp');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      },
	      'keyup .search input': function() {
	        return Tracks.Controller.filteredTracks.filter(this.$('.search input').val());
	      }
	    };

	    return ItemsLayout;

	  })(Marionette.LayoutView);
	  Tracks.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.className = 'track-item user-track';

	    Item.prototype.template = '#track-tpl';

	    return Item;

	  })(Track.Item);
	  Tracks.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.className = 'tracks-wrapper';

	    Items.prototype.childView = Tracks.Item;

	    return Items;

	  })(Marionette.CollectionView);
	  Tracks.EmptyPlaylist = (function(superClass) {
	    extend(EmptyPlaylist, superClass);

	    function EmptyPlaylist() {
	      return EmptyPlaylist.__super__.constructor.apply(this, arguments);
	    }

	    EmptyPlaylist.prototype.className = 'empty-playlist-layout';

	    EmptyPlaylist.prototype.template = '#empty-playlist-layout-tpl';

	    EmptyPlaylist.prototype.events = {
	      'click .to-my-tracks': function() {
	        Tracks.Controller.showUserTracks();
	        $('.list-item.active').removeClass('active');
	        return $('.all-tracks').addClass('active');
	      }
	    };

	    return EmptyPlaylist;

	  })(Marionette.ItemView);
	  Tracks.PlaylistItems = (function(superClass) {
	    extend(PlaylistItems, superClass);

	    function PlaylistItems() {
	      return PlaylistItems.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItems.prototype.className = 'tracks-wrapper';

	    PlaylistItems.prototype.childView = Tracks.Item;

	    PlaylistItems.prototype.emptyView = Tracks.EmptyPlaylist;

	    PlaylistItems.prototype.onRender = function() {};

	    PlaylistItems.prototype.modelEvents = {
	      'change': function() {
	        this.collection = new Backbone.Collection(this.model.get('tracks'));
	        this.collection.each(function(item) {
	          item.set('uniq', 'playlist' + item.id);
	          item.set('playlistTrack', true);
	          return item.set('optionsList', [
	            {
	              title: 'Добавить в плейлист',
	              "class": 'add-to-playlist'
	            }, {
	              title: 'Перейти к исполнителю',
	              "class": 'options-open-artist'
	            }, {
	              title: 'Перейти к альбому',
	              "class": 'options-open-album'
	            }
	          ]);
	        });
	        return this.render();
	      }
	    };

	    PlaylistItems.prototype.onChildviewPlaylistTrackDelete = function(item) {
	      return Track.Controller.removeTrackFromPlaylist(item, this);
	    };

	    return PlaylistItems;

	  })(Marionette.CollectionView);
	  Tracks.PlaylistLayout = (function(superClass) {
	    extend(PlaylistLayout, superClass);

	    function PlaylistLayout() {
	      return PlaylistLayout.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistLayout.prototype.className = 'user-playlist-page';

	    PlaylistLayout.prototype.template = '#user-playlist-page-tpl';

	    PlaylistLayout.prototype.regions = {
	      'content': '.content',
	      'playlistOptions': '.playlist-options'
	    };

	    PlaylistLayout.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('background'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.playlist-head-container').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      image.src = src;
	      return $clamp($('.playlist-head h1')[0], {
	        clamp: 2
	      });
	    };

	    PlaylistLayout.prototype.events = {
	      'click .playlist-options': function() {
	        return Tracks.Controller.openPlaylistOptions(this);
	      },
	      'click .shuffle-track': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = $('.playlist-content-wrapp .tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      }
	    };

	    return PlaylistLayout;

	  })(Marionette.LayoutView);
	  Tracks.PlaylistLayoutOptions = (function(superClass) {
	    extend(PlaylistLayoutOptions, superClass);

	    function PlaylistLayoutOptions() {
	      return PlaylistLayoutOptions.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistLayoutOptions.prototype.template = '#user-playlist-options-tpl';

	    PlaylistLayoutOptions.prototype.tagName = 'ul';

	    PlaylistLayoutOptions.prototype.className = 'options-list';

	    PlaylistLayoutOptions.prototype.events = {
	      'mouseleave': function() {
	        return this.destroy();
	      },
	      'click .options-overlay': function() {
	        return this.destroy();
	      },
	      'click': function() {
	        return false;
	      },
	      'click .delete-pl': function() {
	        var view;
	        view = Marionette.Renderer.render('#user-playlist-delete-confirm-tpl', this.model.toJSON());
	        return this.$el.empty().append(view);
	      },
	      'click .playlist-delete-confirm': function() {
	        return false;
	      },
	      'click .cancel': function() {
	        return this.destroy();
	      },
	      'click .delete': function() {
	        var scrollHeight;
	        Tracks.Controller.deletePlaylist(this.model.get('id'));
	        Tracks.Controller.showUserTracks();
	        $('.list-item.active').removeClass('active');
	        $('.all-tracks').addClass('active');
	        scrollHeight = $('.play-list-container').height() - $('.play-list-container .scrollable').height() - 40;
	        if (scrollHeight < 0) {
	          $('.play-list-container .scrollable').css('margin-top', 0);
	        }
	        return app.Parts.Notification.Controller.show({
	          message: "Плейлист " + (this.model.get('title')) + " удален",
	          type: 'success'
	        });
	      },
	      'click .rename-pl': function() {
	        $('.user-playlist-item.active p').empty().append('<input type="text" value="' + this.model.get('title') + '">').find('input').select();
	        return this.destroy();
	      },
	      'click .is-public': function() {
	        var changePublicState, self;
	        self = this;
	        changePublicState = function() {
	          if ($('.is-public').hasClass('public')) {
	            self.model.set('is_public', false);
	          } else {
	            self.model.set('is_public', true);
	          }
	          $('.is-public').toggleClass('public');
	          return $.get(app.getRequestUrlSid("/beelineMusicV1/playlist/setPublic", "?playlistId=" + (self.model.get('id')) + "&isPublic=" + (self.model.get('is_public'))));
	        };
	        return app.request('get:user').done(function(user) {
	          var params, userName;
	          userName = user.get('profile_name');
	          if ((userName.length === 0) || (userName === null)) {
	            params = {
	              title: 'Как можно к вам<br>обратиться?',
	              message: 'К сожалению, мы не знаем вашего имени<br>и не можем подписать ваши плейлисты,<br>а также представить остальным пользователям сервиса:',
	              placeholder: 'Ваше имя'
	            };
	            return Prompt.Controller.show(params, function(name) {
	              var nameUpdate;
	              nameUpdate = $.ajax({
	                type: "GET",
	                url: app.getRequestUrlSid("/compatibility/user/updateProfile"),
	                data: {
	                  name: name
	                }
	              });
	              return nameUpdate.done(function() {
	                user.set('profile_name', name);
	                return changePublicState();
	              });
	            });
	          } else {
	            return changePublicState();
	          }
	        }).fail(function(err) {
	          return console.log(err);
	        });
	      }
	    };

	    return PlaylistLayoutOptions;

	  })(Marionette.ItemView);
	  return Tracks;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 86:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});