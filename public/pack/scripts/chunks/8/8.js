webpackJsonp([8],{

/***/ 75:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(76), __webpack_require__(28), __webpack_require__(30)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Album, Update, Loader) {
	  var controller;
	  __webpack_require__(77);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function(id, model) {
	      var album, checkFavorite, self;
	      self = this;
	      checkFavorite = function(model) {
	        var favorites;
	        if (model.has('inFavorite')) {
	          return self.drawPage(model);
	        } else {
	          favorites = app.request('get:favorite:albums');
	          favorites.done((function(_this) {
	            return function(collection) {
	              var item;
	              item = collection.some(function(item) {
	                return item.get('id') === model.get('id');
	              });
	              if (item) {
	                model.set('inFavorite', true);
	              } else {
	                model.set('inFavorite', false);
	              }
	              return self.drawPage(model);
	            };
	          })(this));
	          return favorites.fail(function() {
	            model.set('inFavorite', false);
	            return self.drawPage(model);
	          });
	        }
	      };
	      if (model) {
	        return checkFavorite(model);
	      } else {
	        album = app.request('get:single:album', id);
	        return album.done((function(_this) {
	          return function(model) {
	            return checkFavorite(model);
	          };
	        })(this));
	      }
	    };

	    controller.prototype.drawPage = function(model) {
	      var page, popup;
	      popup = __webpack_require__(125);
	      return page = popup.create({
	        cover: {
	          image: app.createImgLink(model.get('img'), '300x300'),
	          type: "square"
	        },
	        title: {
	          main: model.get('title'),
	          second: "АЛЬБОМ 2015 • 12 ПЕСЕН • 52 МИН"
	        },
	        author: {
	          avatar: model.get('img'),
	          name: model.get('artist')
	        },
	        saveButton: {
	          text: "Сохранить",
	          saveTrigger: "save:button:click"
	        },
	        options: [
	          {
	            title: "Поделиться",
	            trigger: "share:post"
	          }
	        ]
	      });
	    };

	    controller.prototype._showTracks = function(model) {
	      var loadTracks;
	      if (model.has('tracks')) {
	        return this._drawTracks(model.get('tracks'));
	      } else {
	        loadTracks = model.fetch();
	        loadTracks.done((function(_this) {
	          return function() {
	            return _this._drawTracks(model.get('tracks'));
	          };
	        })(this));
	        return loadTracks.fail(function(err) {
	          return console.log(err);
	        });
	      }
	    };

	    controller.prototype._drawTracks = function(tracks) {
	      var collection, tracksView;
	      collection = new Backbone.Collection(tracks);
	      collection.each(function(item) {
	        item.set('uniq', 'album' + item.id);
	        return item.set('optionsList', [
	          {
	            title: app.locale.options.addTPl,
	            "class": 'add-to-playlist'
	          }, {
	            title: app.locale.options.toArt,
	            "class": 'options-open-artist',
	            artist: true
	          }
	        ]);
	      });
	      tracksView = new Album.Tracks({
	        collection: collection
	      });
	      this.layout.tracks.show(tracksView);
	      return app.checkActiveTrack();
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / (pathLength - 100);
	    };

	    controller.prototype.resizeHeader = function() {
	      var scrollHeight, scrollTop;
	      scrollHeight = 350;
	      if ($(window).height() + $(window).scrollTop() >= $('.album-page')[0].offsetHeight) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      scrollTop = scrollTop - 100;
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      if (this.pageSize === 'large') {
	        $('.album-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.album-head .shuffle-track').css('margin-top', 21 - (scrollTop * getHeaderCoeficent(scrollHeight, 21)));
	        $('.album-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.album-head .options .list').css('margin-top', 98 - (scrollTop * getHeaderCoeficent(scrollHeight, 68)));
	        $('.album-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 190 - (scrollTop * getHeaderCoeficent(scrollHeight, 60))
	        });
	        return $('.album-head').height(478 - (scrollTop * getHeaderCoeficent(scrollHeight, 178)));
	      } else {
	        $('.album-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.album-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	        $('.album-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.album-head .options .list').css('margin-top', 30);
	        $('.album-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 115 - (scrollTop * getHeaderCoeficent(scrollHeight, 45))
	        });
	        return $('.album-head').height(340 - (scrollTop * getHeaderCoeficent(scrollHeight, 120)));
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Album.Controller = new controller;
	  return Album;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 76:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(26)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track, AlbumElement) {
	  var Album;
	  Album = {};
	  Album.Page = (function(superClass) {
	    extend(Page, superClass);

	    function Page() {
	      return Page.__super__.constructor.apply(this, arguments);
	    }

	    Page.prototype.className = 'album-page';

	    Page.prototype.template = '#album-page-tpl';

	    Page.prototype.regions = {
	      'tracks': '.album-body'
	    };

	    Page.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('img'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.album-head-wrapp').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      return image.src = src;
	    };

	    Page.prototype.events = {
	      'click .list .save': function() {
	        $('.list .save').removeClass('save').addClass('delete').text('Удалить');
	        AlbumElement.Controller.addAlbumToFavorite(this.model);
	        this.model.set('inFavorite', true);
	        return false;
	      },
	      'click .list .delete': function() {
	        $('.list .delete').removeClass('delete').addClass('save').text('Сохранить');
	        AlbumElement.Controller.removeAlbumFromFavorite(this.model.get('id'));
	        this.model.set('inFavorite', false);
	        return false;
	      },
	      'click .list .go-to-author': function() {
	        return app.trigger('show:artist', this.model.get('artistid'));
	      },
	      'click .shuffle-track': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = this.$('.tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      }
	    };

	    return Page;

	  })(Marionette.LayoutView);
	  Album.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.className = 'track-item';

	    Track.prototype.template = '#track-tpl';

	    return Track;

	  })(Track.Item);
	  Album.Tracks = (function(superClass) {
	    extend(Tracks, superClass);

	    function Tracks() {
	      return Tracks.__super__.constructor.apply(this, arguments);
	    }

	    Tracks.prototype.className = 'tracks-wrapper';

	    Tracks.prototype.childView = Album.Track;

	    return Tracks;

	  })(Marionette.CollectionView);
	  return Album;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 77:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 125:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(126)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Popup) {
	  var controller;
	  __webpack_require__(127);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.create = function(params) {
	      var model, view;
	      if (!params) {
	        throw "Required params isnt sending\n\nRequired:\n    {\n        cover: {\n            image: (type:Image),\n            type: round or square\n        },\n        title: {\n            main: (type:String),\n            second: (type:String)\n        },\n        (optional) author: {\n            avatar: (type:Image),\n            name: (type: String)\n        },\n        saveButton: {\n            text: (type:String),\n            saveTrigger: (type:String; exs: save:click)\n        },\n        (optional) options: [\n            {\n                title(type:String): trigger(type:string)\n            },\n            ...,\n            ...\n        ]\n    }";
	      }
	      model = new Backbone.Model(params);
	      view = new Popup.View({
	        model: model
	      });
	      return app.content.show(view);
	    };

	    return controller;

	  })(Marionette.Controller);
	  Popup.Controller = new controller;
	  return Popup.Controller;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 126:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Popup;
	  Popup = {};
	  Popup.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = function(data) {
	      return __webpack_require__(128)(data);
	    };

	    View.prototype.className = 'page-popup';

	    return View;

	  })(Marionette.LayoutView);
	  return Popup;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 127:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 128:
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (author, cover, title) {
	buf.push("<div class=\"close\"></div><div class=\"page-popup-wrapp\"><div class=\"head\"><div" + (jade.attr("style", "background-image: url(" + (cover.image) + ")", true, true)) + " class=\"cover\"><div class=\"play\"></div></div><div class=\"info-block\"><div class=\"title\"><p>" + (jade.escape((jade_interp = title.second) == null ? '' : jade_interp)) + "</p><h1>" + (jade.escape((jade_interp = title.main) == null ? '' : jade_interp)) + "</h1></div>");
	if ( author)
	{
	buf.push("<div class=\"author\"><span>автор</span><img" + (jade.attr("src", "" + (author.avatar) + "", true, true)) + "><p>" + (jade.escape((jade_interp = author.name) == null ? '' : jade_interp)) + "</p></div>");
	}
	buf.push("</div></div></div>");}.call(this,"author" in locals_for_with?locals_for_with.author:typeof author!=="undefined"?author:undefined,"cover" in locals_for_with?locals_for_with.cover:typeof cover!=="undefined"?cover:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
	}

/***/ }

});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3NjcmlwdHMvY2h1bmtzLzgvOC5qcz9iODc5YTMwMGI5MzRkM2E1OTA5MSIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYWdlcy9hbGJ1bS9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhZ2VzL2FsYnVtL3ZpZXcuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYWdlcy9hbGJ1bS9zdHlsZS5zdHlsIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9wb3B1cC9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3BvcHVwL3ZpZXcuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9wb3B1cC9zdHlsZS5zdHlsIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9wb3B1cC90ZW1wbGF0ZXMvbWFpbi5qYWRlIl0sInNvdXJjZXNDb250ZW50IjpbImRlZmluZSBbJy4vdmlldy5jb2ZmZWUnLCcuLi8uLi9wYXJ0cy9zaW1wbGVfdmlld3MvdXBkYXRlJywnLi4vLi4vcGFydHMvc2ltcGxlX3ZpZXdzL2xvYWRlciddLCAoQWxidW0sVXBkYXRlLExvYWRlciktPlxuXHRyZXF1aXJlKCcuL3N0eWxlLnN0eWwnKVxuXG5cdGNsYXNzIGNvbnRyb2xsZXIgZXh0ZW5kcyBNYXJpb25ldHRlLkNvbnRyb2xsZXJcblx0XHRzaG93UGFnZTogKGlkLG1vZGVsKS0+XG5cdFx0XHRzZWxmID0gQFxuXHRcdFx0XG5cdFx0XHRjaGVja0Zhdm9yaXRlID0gKG1vZGVsKS0+XG5cdFx0XHRcdGlmIG1vZGVsLmhhcyAnaW5GYXZvcml0ZSdcblx0XHRcdFx0XHRzZWxmLmRyYXdQYWdlIG1vZGVsXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRmYXZvcml0ZXMgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOmFsYnVtcydcblxuXHRcdFx0XHRcdGZhdm9yaXRlcy5kb25lIChjb2xsZWN0aW9uKT0+XG5cdFx0XHRcdFx0XHRpdGVtID0gY29sbGVjdGlvbi5zb21lIChpdGVtKT0+XG5cdFx0XHRcdFx0XHRcdHJldHVybiBpdGVtLmdldCgnaWQnKSBpcyBtb2RlbC5nZXQoJ2lkJylcblx0XHRcdFx0XHRcdGlmIGl0ZW1cblx0XHRcdFx0XHRcdFx0bW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZVxuXHRcdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0XHRtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCBmYWxzZVxuXG5cdFx0XHRcdFx0XHRzZWxmLmRyYXdQYWdlIG1vZGVsXG5cblx0XHRcdFx0XHRmYXZvcml0ZXMuZmFpbCAtPlxuXHRcdFx0XHRcdFx0bW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgZmFsc2VcblxuXHRcdFx0XHRcdFx0c2VsZi5kcmF3UGFnZSBtb2RlbFxuXG5cdFx0XHRpZiBtb2RlbFxuXHRcdFx0XHRjaGVja0Zhdm9yaXRlIG1vZGVsXG5cblx0XHRcdGVsc2Vcblx0XHRcdFx0YWxidW0gPSBhcHAucmVxdWVzdCAnZ2V0OnNpbmdsZTphbGJ1bScsaWRcblxuXHRcdFx0XHQjIGxvYWRlciA9IG5ldyBMb2FkZXIuVmlld1xuXHRcdFx0XHQjIFx0c3R5bGVzOlxuXHRcdFx0XHQjIFx0XHQndG9wJzogJCh3aW5kb3cpLmhlaWdodCgpLzJcblxuXG5cdFx0XHRcdCMgYXBwLmNvbnRlbnQuc2hvdyBsb2FkZXJcblxuXHRcdFx0XHRhbGJ1bS5kb25lIChtb2RlbCk9PlxuXHRcdFx0XHRcdGNoZWNrRmF2b3JpdGUgbW9kZWxcblxuXHRcdFx0XHQjIGFsYnVtLmZhaWwgLT5cblx0XHRcdFx0IyBcdGNhbGxiYWNrID0gLT5cblx0XHRcdFx0IyBcdFx0c2VsZi5zaG93UGFnZSBpZCxtb2RlbFxuXG5cdFx0XHRcdCMgXHR1cGRhdGUgPSBuZXcgVXBkYXRlLlZpZXdcblx0XHRcdFx0IyBcdFx0c3R5bGVzOlxuXHRcdFx0XHQjIFx0XHRcdCd0b3AnOiAkKHdpbmRvdykuaGVpZ2h0KCkvMlxuXHRcdFx0XHQjIFx0XHRjYWxsYmFjazogY2FsbGJhY2tcblx0XHRcdFx0IyBcdGFwcC5jb250ZW50LnNob3cgdXBkYXRlXG5cblx0XHRkcmF3UGFnZTogKG1vZGVsKS0+XG5cdFx0XHRwb3B1cCA9IHJlcXVpcmUoJy4uLy4uL3BhcnRzL3BvcHVwJylcblx0XHRcdHBhZ2UgPSBwb3B1cC5jcmVhdGUge1xuXHRcdFx0XHRjb3Zlcjoge1xuXHRcdFx0XHRcdGltYWdlOiBhcHAuY3JlYXRlSW1nTGluayhtb2RlbC5nZXQoJ2ltZycpLCczMDB4MzAwJyksXG5cdFx0XHRcdFx0dHlwZTogXCJzcXVhcmVcIlxuXHRcdFx0XHR9LFxuXHRcdFx0XHR0aXRsZToge1xuXHRcdFx0XHRcdG1haW46IG1vZGVsLmdldCgndGl0bGUnKSxcblx0XHRcdFx0XHRzZWNvbmQ6IFwi0JDQm9Cs0JHQntCcIDIwMTUg4oCiIDEyINCf0JXQodCV0J0g4oCiIDUyINCc0JjQnVwiXG5cdFx0XHRcdH0sXG5cdFx0XHRcdGF1dGhvcjoge1xuXHRcdFx0XHRcdGF2YXRhcjogbW9kZWwuZ2V0KCdpbWcnKSxcblx0XHRcdFx0XHRuYW1lOiBtb2RlbC5nZXQoJ2FydGlzdCcpXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHNhdmVCdXR0b246IHtcblx0XHRcdFx0XHR0ZXh0OiBcItCh0L7RhdGA0LDQvdC40YLRjFwiLFxuXHRcdFx0XHRcdHNhdmVUcmlnZ2VyOiBcInNhdmU6YnV0dG9uOmNsaWNrXCJcblx0XHRcdFx0fSxcblx0XHRcdFx0b3B0aW9uczogW1xuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHRpdGxlOiBcItCf0L7QtNC10LvQuNGC0YzRgdGPXCJcblx0XHRcdFx0XHRcdHRyaWdnZXI6IFwic2hhcmU6cG9zdFwiXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cdFx0XHR9XG5cblx0XHRfc2hvd1RyYWNrczogKG1vZGVsKS0+XG5cdFx0XHRpZiBtb2RlbC5oYXMgJ3RyYWNrcydcblx0XHRcdFx0QF9kcmF3VHJhY2tzIG1vZGVsLmdldCAndHJhY2tzJ1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHRsb2FkVHJhY2tzID0gZG8gbW9kZWwuZmV0Y2hcblxuXHRcdFx0XHRsb2FkVHJhY2tzLmRvbmUgPT5cblx0XHRcdFx0XHRAX2RyYXdUcmFja3MgbW9kZWwuZ2V0ICd0cmFja3MnXG5cblx0XHRcdFx0bG9hZFRyYWNrcy5mYWlsIChlcnIpLT5cblx0XHRcdFx0XHRjb25zb2xlLmxvZyBlcnJcblxuXHRcdF9kcmF3VHJhY2tzOiAodHJhY2tzKS0+XG5cdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24gdHJhY2tzXG5cblx0XHRcdGNvbGxlY3Rpb24uZWFjaCAoaXRlbSktPlxuXHRcdFx0XHRpdGVtLnNldCAndW5pcScsICdhbGJ1bScraXRlbS5pZFxuXHRcdFx0XHRpdGVtLnNldCAnb3B0aW9uc0xpc3QnLCBbXG5cdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy5hZGRUUGxcblx0XHRcdFx0XHRcdGNsYXNzOiAnYWRkLXRvLXBsYXlsaXN0J1xuXHRcdFx0XHRcdH0se1xuXHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy50b0FydFxuXHRcdFx0XHRcdFx0Y2xhc3M6ICdvcHRpb25zLW9wZW4tYXJ0aXN0J1xuXHRcdFx0XHRcdFx0YXJ0aXN0OiB0cnVlXG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRdXG5cblx0XHRcdHRyYWNrc1ZpZXcgPSBuZXcgQWxidW0uVHJhY2tzXG5cdFx0XHRcdGNvbGxlY3Rpb246IGNvbGxlY3Rpb25cblxuXHRcdFx0QGxheW91dC50cmFja3Muc2hvdyB0cmFja3NWaWV3XG5cblx0XHRcdGRvIGFwcC5jaGVja0FjdGl2ZVRyYWNrXG5cblxuXG5cblx0XHRnZXRIZWFkZXJDb2VmaWNlbnQgPSAocGF0aExlbmd0aCxjaGFuZ2VMZW5ndGgpLT5cblx0XHRcdHJldHVybiBjaGFuZ2VMZW5ndGgvKHBhdGhMZW5ndGgtMTAwKVxuXG5cblx0XHRyZXNpemVIZWFkZXI6IC0+XG5cdFx0XHRzY3JvbGxIZWlnaHQgPSAzNTBcblx0XHRcdGlmICQod2luZG93KS5oZWlnaHQoKSskKHdpbmRvdykuc2Nyb2xsVG9wKCkgPj0gJCgnLmFsYnVtLXBhZ2UnKVswXS5vZmZzZXRIZWlnaHRcblx0XHRcdFx0cmV0dXJuXG5cblx0XHRcdHNjcm9sbFRvcCA9ICQod2luZG93KS5zY3JvbGxUb3AoKVxuXHRcdFx0aWYgc2Nyb2xsVG9wID49IHNjcm9sbEhlaWdodFxuXHRcdFx0XHRzY3JvbGxUb3AgPSBzY3JvbGxIZWlnaHRcblxuXHRcdFx0c2Nyb2xsVG9wID0gc2Nyb2xsVG9wIC0gMTAwXG5cblx0XHRcdGlmIHNjcm9sbFRvcCA8PSAwXG5cdFx0XHRcdHNjcm9sbFRvcCA9IDBcblxuXHRcdFx0aWYgQHBhZ2VTaXplIGlzICdsYXJnZSdcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQgLmluZm8gaDEnKS5jc3MgJ2ZvbnQtc2l6ZScsIDYwLShzY3JvbGxUb3AqZ2V0SGVhZGVyQ29lZmljZW50KHNjcm9sbEhlaWdodCwzMCkpXG5cdFx0XHRcdCQoJy5hbGJ1bS1oZWFkIC5zaHVmZmxlLXRyYWNrJykuY3NzICdtYXJnaW4tdG9wJywgMjEtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDIxKSlcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQgLmF1dGhvci13cmFwcCcpLmNzcyAnbWFyZ2luLXRvcCcsIDEwLShzY3JvbGxUb3AqZ2V0SGVhZGVyQ29lZmljZW50KHNjcm9sbEhlaWdodCwxMCkpXG5cdFx0XHRcdCQoJy5hbGJ1bS1oZWFkIC5vcHRpb25zIC5saXN0JykuY3NzICdtYXJnaW4tdG9wJywgOTgtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDY4KSlcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQgLmluZm8gPiBzcGFuJykuY3NzXG5cdFx0XHRcdFx0J29wYWNpdHknOiAxLShzY3JvbGxUb3AqZ2V0SGVhZGVyQ29lZmljZW50KHNjcm9sbEhlaWdodCwxKSlcblx0XHRcdFx0XHQncGFkZGluZy10b3AnOiAxOTAtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDYwKSlcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQnKS5oZWlnaHQgNDc4LShzY3JvbGxUb3AqZ2V0SGVhZGVyQ29lZmljZW50KHNjcm9sbEhlaWdodCwxNzgpKVxuI1x0XHRcdFx0JCgnLmFsYnVtLXBhZ2UnKS5jc3MgJ3BhZGRpbmctdG9wJywgNDg1LShzY3JvbGxUb3AqZ2V0SGVhZGVyQ29lZmljZW50KHNjcm9sbEhlaWdodCwxODUpKVxuXHRcdFx0ZWxzZVxuXHRcdFx0XHQkKCcuYWxidW0taGVhZCAuaW5mbyBoMScpLmNzcyAnZm9udC1zaXplJywgNjAtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDMwKSlcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQgLnNodWZmbGUtdHJhY2snKS5jc3MgJ21hcmdpbi10b3AnLCAyMC0oc2Nyb2xsVG9wKmdldEhlYWRlckNvZWZpY2VudChzY3JvbGxIZWlnaHQsMjApKVxuXHRcdFx0XHQkKCcuYWxidW0taGVhZCAuYXV0aG9yLXdyYXBwJykuY3NzICdtYXJnaW4tdG9wJywgMTAtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDEwKSlcblx0XHRcdFx0JCgnLmFsYnVtLWhlYWQgLm9wdGlvbnMgLmxpc3QnKS5jc3MgJ21hcmdpbi10b3AnLCAzMFxuXHRcdFx0XHQkKCcuYWxidW0taGVhZCAuaW5mbyA+IHNwYW4nKS5jc3Ncblx0XHRcdFx0XHQnb3BhY2l0eSc6IDEtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDEpKVxuXHRcdFx0XHRcdCdwYWRkaW5nLXRvcCc6IDExNS0oc2Nyb2xsVG9wKmdldEhlYWRlckNvZWZpY2VudChzY3JvbGxIZWlnaHQsNDUpKVxuXHRcdFx0XHQkKCcuYWxidW0taGVhZCcpLmhlaWdodCAzNDAtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDEyMCkpXG4jXHRcdFx0XHQkKCcuYWxidW0tcGFnZScpLmNzcyAncGFkZGluZy10b3AnLCAzNTAtKHNjcm9sbFRvcCpnZXRIZWFkZXJDb2VmaWNlbnQoc2Nyb2xsSGVpZ2h0LDEzMCkpXG5cblxuXG5cblx0QWxidW0uQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cdHJldHVybiBBbGJ1bVxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhZ2VzL2FsYnVtL2luZGV4LmNvZmZlZVxuICoqLyIsImRlZmluZSBbJy4uLy4uL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvdHJhY2snLCcuLi8uLi9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FsYnVtJ10sIChUcmFjayxBbGJ1bUVsZW1lbnQpLT5cblx0QWxidW0gPSB7fVxuXG5cdGNsYXNzIEFsYnVtLlBhZ2UgZXh0ZW5kcyBNYXJpb25ldHRlLkxheW91dFZpZXdcblx0XHRjbGFzc05hbWU6ICdhbGJ1bS1wYWdlJ1xuXHRcdHRlbXBsYXRlOiAnI2FsYnVtLXBhZ2UtdHBsJ1xuXHRcdHJlZ2lvbnM6XG5cdFx0XHQndHJhY2tzJzogJy5hbGJ1bS1ib2R5J1xuXG5cdFx0b25TaG93OiAtPlxuXHRcdFx0c3JjID0gYXBwLmNyZWF0ZUltZ0xpbmsoQG1vZGVsLmdldCgnaW1nJyksJzEyMDB4MTIwMCcsbnVsbCx0cnVlKVxuXHRcdFx0aW1hZ2UgPSBuZXcgSW1hZ2VcblxuXHRcdFx0aW1hZ2Uub25sb2FkID0gPT5cblx0XHRcdFx0QCQoJy5hbGJ1bS1oZWFkLXdyYXBwJykuY3NzICdiYWNrZ3JvdW5kLWltYWdlJywgJ3VybCgnK3NyYysnKSdcblxuXHRcdFx0aW1hZ2Uuc3JjID0gc3JjXG5cblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2sgLmxpc3QgLnNhdmUnOiAtPlxuXHRcdFx0XHQkKCcubGlzdCAuc2F2ZScpLnJlbW92ZUNsYXNzKCdzYXZlJykuYWRkQ2xhc3MoJ2RlbGV0ZScpLnRleHQgJ9Cj0LTQsNC70LjRgtGMJ1xuXHRcdFx0XHRBbGJ1bUVsZW1lbnQuQ29udHJvbGxlci5hZGRBbGJ1bVRvRmF2b3JpdGUgQG1vZGVsXG5cdFx0XHRcdEBtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCB0cnVlXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnY2xpY2sgLmxpc3QgLmRlbGV0ZSc6IC0+XG5cdFx0XHRcdCQoJy5saXN0IC5kZWxldGUnKS5yZW1vdmVDbGFzcygnZGVsZXRlJykuYWRkQ2xhc3MoJ3NhdmUnKS50ZXh0ICfQodC+0YXRgNCw0L3QuNGC0YwnXG5cdFx0XHRcdEFsYnVtRWxlbWVudC5Db250cm9sbGVyLnJlbW92ZUFsYnVtRnJvbUZhdm9yaXRlIEBtb2RlbC5nZXQgJ2lkJ1xuXHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgZmFsc2Vcblx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRcdCdjbGljayAubGlzdCAuZ28tdG8tYXV0aG9yJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YXJ0aXN0JywgQG1vZGVsLmdldCAnYXJ0aXN0aWQnXG5cblx0XHRcdCdjbGljayAuc2h1ZmZsZS10cmFjayc6IC0+XG5cdFx0XHRcdHRyYWNrQ29udGFpbmVyID0gQCQoJy50cmFja3Mtd3JhcHBlcicpXG5cdFx0XHRcdHRyYWNrcyA9IHRyYWNrQ29udGFpbmVyLmZpbmQoJy50cmFjay1pdGVtJykubm90KCcuZGlzYWJsZScpXG5cdFx0XHRcdHRyYWNrc0xlbmd0aCA9IHRyYWNrcy5sZW5ndGhcblxuXHRcdFx0XHRyYW5kb20gPSBhcHAuUmFuZEludCgwLHRyYWNrc0xlbmd0aC0xKVxuXG5cdFx0XHRcdHRyYWNrcy5lcShyYW5kb20pLmNsaWNrKClcblxuXG5cdGNsYXNzIEFsYnVtLlRyYWNrIGV4dGVuZHMgVHJhY2suSXRlbVxuXHRcdGNsYXNzTmFtZTogJ3RyYWNrLWl0ZW0nXG5cdFx0dGVtcGxhdGU6ICcjdHJhY2stdHBsJ1xuXG5cdGNsYXNzIEFsYnVtLlRyYWNrcyBleHRlbmRzIE1hcmlvbmV0dGUuQ29sbGVjdGlvblZpZXdcblx0XHRjbGFzc05hbWU6ICd0cmFja3Mtd3JhcHBlcidcblx0XHRjaGlsZFZpZXc6IEFsYnVtLlRyYWNrXG5cblx0cmV0dXJuIEFsYnVtXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFnZXMvYWxidW0vdmlldy5jb2ZmZWVcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvcGFnZXMvYWxidW0vc3R5bGUuc3R5bFxuICoqIG1vZHVsZSBpZCA9IDc3XG4gKiogbW9kdWxlIGNodW5rcyA9IDhcbiAqKi8iLCJkZWZpbmUgWycuL3ZpZXcuY29mZmVlJ10sIChQb3B1cCktPlxuICAgIHJlcXVpcmUoJy4vc3R5bGUuc3R5bCcpXG5cbiAgICBjbGFzcyBjb250cm9sbGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db250cm9sbGVyXG4gICAgICAgIGNyZWF0ZTogKHBhcmFtcyktPlxuICAgICAgICAgICAgaWYgIXBhcmFtc1xuICAgICAgICAgICAgICAgIHRocm93IFwiXCJcIlxuICAgICAgICAgICAgICAgICAgICBSZXF1aXJlZCBwYXJhbXMgaXNudCBzZW5kaW5nXG5cbiAgICAgICAgICAgICAgICAgICAgUmVxdWlyZWQ6XG4gICAgICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY292ZXI6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1hZ2U6ICh0eXBlOkltYWdlKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogcm91bmQgb3Igc3F1YXJlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYWluOiAodHlwZTpTdHJpbmcpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWNvbmQ6ICh0eXBlOlN0cmluZylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChvcHRpb25hbCkgYXV0aG9yOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGF2YXRhcjogKHR5cGU6SW1hZ2UpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAodHlwZTogU3RyaW5nKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2F2ZUJ1dHRvbjoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0ZXh0OiAodHlwZTpTdHJpbmcpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzYXZlVHJpZ2dlcjogKHR5cGU6U3RyaW5nOyBleHM6IHNhdmU6Y2xpY2spXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAob3B0aW9uYWwpIG9wdGlvbnM6IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGUodHlwZTpTdHJpbmcpOiB0cmlnZ2VyKHR5cGU6c3RyaW5nKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi4sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF1cbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBcIlwiXCJcblxuICAgICAgICAgICAgbW9kZWwgPSBuZXcgQmFja2JvbmUuTW9kZWwgcGFyYW1zXG4gICAgICAgICAgICB2aWV3ID0gbmV3IFBvcHVwLlZpZXdcbiAgICAgICAgICAgICAgICBtb2RlbDogbW9kZWxcblxuICAgICAgICAgICAgYXBwLmNvbnRlbnQuc2hvdyB2aWV3XG5cblxuICAgIFBvcHVwLkNvbnRyb2xsZXIgPSBuZXcgY29udHJvbGxlclxuXG4gICAgcmV0dXJuIFBvcHVwLkNvbnRyb2xsZXJcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9wYXJ0cy9wb3B1cC9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgLT5cbiAgICBQb3B1cCA9IHt9XG5cbiAgICBjbGFzcyBQb3B1cC5WaWV3IGV4dGVuZHMgTWFyaW9uZXR0ZS5MYXlvdXRWaWV3XG4gICAgICAgIHRlbXBsYXRlOiAoZGF0YSktPlxuICAgICAgICAgICAgcmV0dXJuIHJlcXVpcmUoJy4vdGVtcGxhdGVzL21haW4uamFkZScpKGRhdGEpXG4gICAgICAgIGNsYXNzTmFtZTogJ3BhZ2UtcG9wdXAnXG5cblxuXG4gICAgcmV0dXJuIFBvcHVwXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvcG9wdXAvdmlldy5jb2ZmZWVcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvcG9wdXAvc3R5bGUuc3R5bFxuICoqIG1vZHVsZSBpZCA9IDEyN1xuICoqIG1vZHVsZSBjaHVua3MgPSA4XG4gKiovIiwidmFyIGphZGUgPSByZXF1aXJlKFwiL1VzZXJzL2VtaWwvd29ya19wcm9qZWN0L25vZGUubXVzaWMvbm9kZV9tb2R1bGVzL2phZGUvbGliL3J1bnRpbWUuanNcIik7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gdGVtcGxhdGUobG9jYWxzKSB7XG52YXIgYnVmID0gW107XG52YXIgamFkZV9taXhpbnMgPSB7fTtcbnZhciBqYWRlX2ludGVycDtcbjt2YXIgbG9jYWxzX2Zvcl93aXRoID0gKGxvY2FscyB8fCB7fSk7KGZ1bmN0aW9uIChhdXRob3IsIGNvdmVyLCB0aXRsZSkge1xuYnVmLnB1c2goXCI8ZGl2IGNsYXNzPVxcXCJjbG9zZVxcXCI+PC9kaXY+PGRpdiBjbGFzcz1cXFwicGFnZS1wb3B1cC13cmFwcFxcXCI+PGRpdiBjbGFzcz1cXFwiaGVhZFxcXCI+PGRpdlwiICsgKGphZGUuYXR0cihcInN0eWxlXCIsIFwiYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiICsgKGNvdmVyLmltYWdlKSArIFwiKVwiLCB0cnVlLCB0cnVlKSkgKyBcIiBjbGFzcz1cXFwiY292ZXJcXFwiPjxkaXYgY2xhc3M9XFxcInBsYXlcXFwiPjwvZGl2PjwvZGl2PjxkaXYgY2xhc3M9XFxcImluZm8tYmxvY2tcXFwiPjxkaXYgY2xhc3M9XFxcInRpdGxlXFxcIj48cD5cIiArIChqYWRlLmVzY2FwZSgoamFkZV9pbnRlcnAgPSB0aXRsZS5zZWNvbmQpID09IG51bGwgPyAnJyA6IGphZGVfaW50ZXJwKSkgKyBcIjwvcD48aDE+XCIgKyAoamFkZS5lc2NhcGUoKGphZGVfaW50ZXJwID0gdGl0bGUubWFpbikgPT0gbnVsbCA/ICcnIDogamFkZV9pbnRlcnApKSArIFwiPC9oMT48L2Rpdj5cIik7XG5pZiAoIGF1dGhvcilcbntcbmJ1Zi5wdXNoKFwiPGRpdiBjbGFzcz1cXFwiYXV0aG9yXFxcIj48c3Bhbj7QsNCy0YLQvtGAPC9zcGFuPjxpbWdcIiArIChqYWRlLmF0dHIoXCJzcmNcIiwgXCJcIiArIChhdXRob3IuYXZhdGFyKSArIFwiXCIsIHRydWUsIHRydWUpKSArIFwiPjxwPlwiICsgKGphZGUuZXNjYXBlKChqYWRlX2ludGVycCA9IGF1dGhvci5uYW1lKSA9PSBudWxsID8gJycgOiBqYWRlX2ludGVycCkpICsgXCI8L3A+PC9kaXY+XCIpO1xufVxuYnVmLnB1c2goXCI8L2Rpdj48L2Rpdj48L2Rpdj5cIik7fS5jYWxsKHRoaXMsXCJhdXRob3JcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLmF1dGhvcjp0eXBlb2YgYXV0aG9yIT09XCJ1bmRlZmluZWRcIj9hdXRob3I6dW5kZWZpbmVkLFwiY292ZXJcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLmNvdmVyOnR5cGVvZiBjb3ZlciE9PVwidW5kZWZpbmVkXCI/Y292ZXI6dW5kZWZpbmVkLFwidGl0bGVcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLnRpdGxlOnR5cGVvZiB0aXRsZSE9PVwidW5kZWZpbmVkXCI/dGl0bGU6dW5kZWZpbmVkKSk7O3JldHVybiBidWYuam9pbihcIlwiKTtcbn1cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL3BvcHVwL3RlbXBsYXRlcy9tYWluLmphZGVcbiAqKiBtb2R1bGUgaWQgPSAxMjhcbiAqKiBtb2R1bGUgY2h1bmtzID0gOFxuICoqLyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBOzs7Ozs7O0FBREE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQURBO0FBR0E7O0FBRUE7QUFSQTtBQUFBO0FBVUE7QUFDQTtBQUVBO0FBSEE7O0FBaEJBO0FBcUJBO0FBQ0E7QUFEQTtBQUlBO0FBU0E7QUFBQTtBQUNBO0FBREE7QUFBQTs7QUFyQ0E7QUFDQTtBQWlEQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBQ0E7QUFYQTtBQWFBO0FBQ0E7QUFDQTtBQWZBO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFqQkE7QUFBQTtBQUZBO0FBQ0E7QUEwQkE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUdBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUdBO0FBQ0E7QUFEQTs7QUFUQTtBQUNBO0FBV0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFQQTtBQUFBO0FBRkE7QUFhQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBckJBO0FBQ0E7QUF5QkE7QUFDQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBR0E7QUFSQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFHQTs7QUFoQ0E7QUFDQTs7O0FBekhBO0FBOEpBO0FBQ0E7QUFsS0E7Ozs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFHQTtBQVBBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBUEE7Ozs7O0FBL0JBO0FBeUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUhBO0FBSUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBSEE7QUFJQTtBQXBEQTs7Ozs7Ozs7QUNBQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOztBQStCQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBckNBO0FBQ0E7OztBQUZBO0FBeUNBO0FBRUE7QUE5Q0E7Ozs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBOzs7QUFKQTtBQU9BO0FBVkE7Ozs7Ozs7O0FDQUE7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OyIsInNvdXJjZVJvb3QiOiIifQ==