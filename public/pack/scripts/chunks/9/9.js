webpackJsonp([9],{

/***/ 78:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(79), __webpack_require__(28), __webpack_require__(30), __webpack_require__(26)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Artist, Update, Loader, Album) {
	  var controller;
	  __webpack_require__(80);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function(id) {
	      var artist, loader, self;
	      self = this;
	      artist = app.request('get:artist', id);
	      if (artist.promise) {
	        loader = new Loader.View({
	          styles: {
	            'top': $(window).height() / 2
	          }
	        });
	        app.content.show(loader);
	        artist.done((function(_this) {
	          return function(model) {
	            return _this.drawPage(model);
	          };
	        })(this));
	        return artist.fail(function() {
	          var callback, update;
	          callback = function() {
	            return self.showPage(id);
	          };
	          update = new Update.View({
	            styles: {
	              'top': $(window).height() / 2
	            },
	            callback: callback
	          });
	          return app.content.show(update);
	        });
	      } else {
	        return this.drawPage(artist);
	      }
	    };

	    controller.prototype.drawPage = function(model) {
	      var author;
	      app.GAPageAction('page-open', "Артист - " + (model.get('name')));
	      author = model.get('name');
	      if (author.length >= 30) {
	        author = author.slice(0, 27) + "...";
	      }
	      this.layout = new Artist.Page({
	        model: model
	      });
	      this.listenTo(this.layout, 'show', function() {
	        this._showTracks(model);
	        if ($(window).width() < 1200 || $(window).height() < 800) {
	          this.pageSize = 'small';
	        } else {
	          this.pageSize = 'large';
	        }
	        $(window).on('resize.album', (function(_this) {
	          return function() {
	            if ($(window).width() < 1200 || $(window).height() < 800) {
	              if (_this.pageSize !== 'small') {
	                _this.pageSize = 'small';
	                return _this.resizeHeader();
	              }
	            } else {
	              if (_this.pageSize !== 'large') {
	                _this.pageSize = 'large';
	                return _this.resizeHeader();
	              }
	            }
	          };
	        })(this));
	        return $(window).on('scroll.album', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	      });
	      this.listenTo(this.layout, 'destroy', function() {
	        return $(window).off('.album');
	      });
	      return app.content.show(this.layout);
	    };

	    controller.prototype._showTracks = function(model) {
	      var tracksLayout;
	      $('.tabs .video').removeClass('active');
	      $('.tabs .tracks').addClass('active');
	      tracksLayout = new Artist.Tracks.Layout;
	      this.listenTo(tracksLayout, 'destroy', function() {
	        app.request('stop:artist:request', 'tracks');
	        return app.request('stop:artist:request', 'albums');
	      });
	      this.layout.content.show(tracksLayout);
	      this.showFavoriteTracks(tracksLayout, model.get('artistid'));
	      this.showFavoriteAlbums(tracksLayout, model.get('artistid'));
	      return this.showCoAlbums(tracksLayout, model.get('artistid'));
	    };

	    controller.prototype.showFavoriteTracks = function(layout, id) {
	      var loader, self, showTracks, tracks;
	      tracks = app.request('get:artist:tracks', {
	        data: {
	          'artist': id
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          'margin': '100px auto'
	        }
	      });
	      layout.tracks.show(loader);
	      showTracks = function(collection) {
	        var tracksView;
	        collection.each(function(item) {
	          item.set('uniq', 'artist' + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ]);
	        });
	        tracksView = new Artist.Tracks({
	          collection: collection
	        });
	        layout.tracks.show(tracksView);
	        return app.checkActiveTrack();
	      };
	      if (tracks.promise) {
	        self = this;
	        tracks.done(function(collection) {
	          return showTracks(collection);
	        });
	        return tracks.fail(function() {
	          var callback, update;
	          callback = function() {
	            return self.showFavoriteTracks(layout, id);
	          };
	          update = new Update.View({
	            styles: {
	              'margin': '100px auto'
	            },
	            callback: callback
	          });
	          return layout.tracks.show(update);
	        });
	      } else {
	        return showTracks(tracks);
	      }
	    };

	    controller.prototype.showFavoriteAlbums = function(layout, id) {
	      var albums, self, showAlbums;
	      self = this;
	      albums = app.request('get:artist:albums', {
	        data: {
	          'artist': id
	        }
	      });
	      showAlbums = function(collection) {
	        var albumsView;
	        if (collection.length === 0) {
	          $('.co-albums-title').remove();
	        }
	        albumsView = new Album.InlineListView({
	          collection: collection
	        });
	        self.listenTo(albumsView, 'childview:open:album:content', function(album) {
	          return showAlbumContent(album);
	        });
	        return layout.albums.show(albumsView);
	      };
	      if (albums.promise) {
	        albums.done(function(collection) {
	          return showAlbums(collection);
	        });
	        return albums.fail(function(err) {
	          return console.log(err);
	        });
	      } else {
	        return showAlbums(albums);
	      }
	    };

	    controller.prototype.showCoAlbums = function(layout, id) {
	      var albums, self, showAlbums;
	      self = this;
	      albums = app.request('get:artist:albums', {
	        data: {
	          'artist': id,
	          'artistFilterMode': 'indirect_not_exact'
	        }
	      });
	      showAlbums = function(collection) {
	        var albumsView;
	        albumsView = new Album.InlineListView({
	          collection: collection
	        });
	        self.listenTo(albumsView, 'childview:open:album:content', function(album) {
	          return showAlbumContent(album);
	        });
	        return layout.coAlbums.show(albumsView);
	      };
	      if (albums.promise) {
	        albums.done(function(collection) {
	          return showAlbums(collection);
	        });
	        return albums.fail(function(err) {
	          return console.log(err);
	        });
	      } else {
	        return showAlbums(albums);
	      }
	    };

	    controller.prototype._showVideo = function(model) {
	      var drawVideo, loader, videoLoad;
	      $('.tabs .tracks').removeClass('active');
	      $('.tabs .video').addClass('active');
	      loader = new Loader.View({
	        styles: {
	          'margin': '200px auto'
	        }
	      });
	      this.layout.content.show(loader);
	      drawVideo = (function(_this) {
	        return function(collection) {
	          var videoList;
	          videoList = new Artist.Videos({
	            collection: collection
	          });
	          if ($('.artist-head .tabs .video').hasClass('active')) {
	            return _this.layout.content.show(videoList);
	          }
	        };
	      })(this);
	      videoLoad = app.request('get:artist:videos', model.get('name'));
	      if (videoLoad.promise) {
	        videoLoad.done(function(videos) {
	          return drawVideo(videos);
	        });
	        return videoLoad.fail(function() {
	          return console.log(arguments);
	        });
	      } else {
	        return drawVideo(videoLoad);
	      }
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / (pathLength - 0);
	    };

	    controller.prototype.resizeHeader = function() {
	      var scrollHeight, scrollTop;
	      scrollHeight = 550;
	      if ($(window).height() + $(window).scrollTop() >= $('.artist-page')[0].offsetHeight) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      if (this.pageSize === 'large') {
	        $('.artist-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.artist-head .shuffle-track').css('margin-top', 21 - (scrollTop * getHeaderCoeficent(scrollHeight, 21)));
	        $('.artist-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.artist-head .options .list').css('margin-top', 98 - (scrollTop * getHeaderCoeficent(scrollHeight, 68)));
	        $('.artist-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 190 - (scrollTop * getHeaderCoeficent(scrollHeight, 60))
	        });
	        return $('.artist-head').height(478 - (scrollTop * getHeaderCoeficent(scrollHeight, 178)));
	      } else {
	        $('.artist-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.artist-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	        $('.artist-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.artist-head .options .list').css('margin-top', 30);
	        $('.artist-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 115 - (scrollTop * getHeaderCoeficent(scrollHeight, 45))
	        });
	        return $('.artist-head').height(340 - (scrollTop * getHeaderCoeficent(scrollHeight, 120)));
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Artist.Controller = new controller;
	  return Artist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 79:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track) {
	  var Artist;
	  Artist = {};
	  Artist.AlbumModel = (function(superClass) {
	    extend(AlbumModel, superClass);

	    function AlbumModel() {
	      return AlbumModel.__super__.constructor.apply(this, arguments);
	    }

	    AlbumModel.prototype.parse = function(item) {
	      console.log(item);
	      item.uniq = 'artist-' + item.id;
	      return item;
	    };

	    return AlbumModel;

	  })(Backbone.Model);
	  Artist.AlbumCollection = (function(superClass) {
	    extend(AlbumCollection, superClass);

	    function AlbumCollection() {
	      return AlbumCollection.__super__.constructor.apply(this, arguments);
	    }

	    AlbumCollection.prototype.model = Artist.AlbumModel;

	    return AlbumCollection;

	  })(Backbone.Collection);
	  Artist.Page = (function(superClass) {
	    extend(Page, superClass);

	    function Page() {
	      return Page.__super__.constructor.apply(this, arguments);
	    }

	    Page.prototype.className = 'artist-page';

	    Page.prototype.template = '#artist-page-tpl';

	    Page.prototype.regions = {
	      'content': '.content'
	    };

	    Page.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('img'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.artist-head-wrapp').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      return image.src = src;
	    };

	    Page.prototype.events = {
	      'click .tabs .tracks': function() {
	        if (!this.$('.tabs .tracks').hasClass('active')) {
	          return Artist.Controller._showTracks(this.model);
	        }
	      },
	      'click .tabs .video': function() {
	        if (!this.$('.tabs .video').hasClass('active')) {
	          return Artist.Controller._showVideo(this.model);
	        }
	      },
	      'click .shuffle-track': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = $('.artist-tracks-container .tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      }
	    };

	    return Page;

	  })(Marionette.LayoutView);
	  Artist.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.className = 'track-item';

	    Track.prototype.template = '#track-tpl';

	    return Track;

	  })(Track.Item);
	  Artist.Tracks = (function(superClass) {
	    extend(Tracks, superClass);

	    function Tracks() {
	      return Tracks.__super__.constructor.apply(this, arguments);
	    }

	    Tracks.prototype.className = 'tracks-wrapper';

	    Tracks.prototype.childView = Artist.Track;

	    return Tracks;

	  })(Marionette.CollectionView);
	  Artist.Video = (function(superClass) {
	    extend(Video, superClass);

	    function Video() {
	      return Video.__super__.constructor.apply(this, arguments);
	    }

	    Video.prototype.className = 'video-item';

	    Video.prototype.template = '#video-item-tpl';

	    Video.prototype.events = {
	      'click': function() {
	        return app.Parts.VideoPlayer.Controller.openVideo(this.model);
	      }
	    };

	    return Video;

	  })(Marionette.ItemView);
	  Artist.Videos = (function(superClass) {
	    extend(Videos, superClass);

	    function Videos() {
	      return Videos.__super__.constructor.apply(this, arguments);
	    }

	    Videos.prototype.childView = Artist.Video;

	    Videos.prototype.className = 'video-wrapp';

	    Videos.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return Videos;

	  })(Marionette.CollectionView);
	  Artist.Tracks.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#artist-tracks-layout-tpl';

	    Layout.prototype.className = 'tracks-wrapp';

	    Layout.prototype.regions = {
	      tracks: '.artist-tracks-container',
	      albums: '.artist-albums-container',
	      coAlbums: '.artist-co-albums-container'
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  return Artist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 80:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});