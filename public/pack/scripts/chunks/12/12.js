webpackJsonp([12],{

/***/ 87:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(88), __webpack_require__(30), __webpack_require__(28)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Albums, Loader, Update) {
	  var controller;
	  __webpack_require__(89);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      var listLoader;
	      this.layout = new Albums.Layout;
	      app.content.show(this.layout);
	      listLoader = new Loader.View({
	        styles: {
	          'top': ($(window).height() / 2) - 200
	        }
	      });
	      this.layout.list.show(listLoader);
	      return this.showUserAlbums();
	    };

	    controller.prototype.showUserAlbums = function() {
	      var albums, loader, self, showAlbums;
	      self = this;
	      albums = app.request('get:favorite:albums');
	      showAlbums = (function(_this) {
	        return function(collection) {
	          var albumsView;
	          albumsView = new Albums.Items({
	            collection: collection
	          });
	          _this.layout.list.show(albumsView);
	          return $('.list .list-item').not('.disable').eq(0).click();
	        };
	      })(this);
	      if (albums.promise) {
	        loader = new Loader.View({
	          styles: {
	            'top': ($(window).height() / 2) - 100
	          }
	        });
	        this.layout.content.show(loader);
	        albums.done(function(collection) {
	          return showAlbums(collection);
	        });
	        return albums.fail((function(_this) {
	          return function() {
	            var callback, update;
	            callback = function() {
	              return self.showUserAlbums();
	            };
	            update = new Update.View({
	              callback: callback,
	              styles: {
	                'top': ($(window).height() / 2) - 100
	              }
	            });
	            return _this.layout.content.show(update);
	          };
	        })(this));
	      } else {
	        return showAlbums(albums);
	      }
	    };

	    controller.prototype.openAlbum = function(item) {
	      var layout, loader, model, self, showAlbumTracks;
	      self = this;
	      model = item.model;
	      layout = new Albums.PageLayout({
	        model: model
	      });
	      this.listenTo(layout, 'show', function() {
	        this.resizeHeader();
	        $(window).on('resize.album', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	        return $(window).on('scroll.album', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	      });
	      this.listenTo(layout, 'destroy', function() {
	        return $(window).off('.album');
	      });
	      this.layout.content.show(layout);
	      loader = new Loader.View({
	        styles: {
	          'top': '50px'
	        }
	      });
	      layout.content.show(loader);
	      showAlbumTracks = function(tracks) {
	        var collection;
	        collection = new Backbone.Collection(tracks);
	        collection.each(function(item) {
	          item.set('uniq', 'user-album' + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: 'Перейти к альбому',
	              "class": app.locale.options.toAlb
	            }
	          ]);
	        });
	        tracks = new Albums.TrackItems({
	          collection: collection
	        });
	        layout.content.show(tracks);
	        return app.checkActiveTrack();
	      };
	      if (model.has('tracks')) {
	        return showAlbumTracks(model.get('tracks'));
	      } else {
	        if (this.albumFetch) {
	          this.albumFetch.abort();
	        }
	        this.albumFetch = model.fetch();
	        this.albumFetch.done(function() {
	          return showAlbumTracks(model.get('tracks'));
	        });
	        return this.albumFetch.fail(function() {
	          var callback, update;
	          callback = function() {
	            return self.openAlbum(item);
	          };
	          update = new Update.View({
	            callback: callback,
	            styles: {
	              'top': '50px'
	            }
	          });
	          return layout.content.show(update);
	        });
	      }
	    };

	    controller.prototype.openAlbumOptions = function(view) {
	      var optionsView;
	      optionsView = new Albums.OptionsLayout({
	        model: view.model
	      });
	      return view.playlistOptions.show(optionsView);
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / pathLength;
	    };

	    controller.prototype.resizeHeader = function() {
	      var scrollHeight, scrollTop;
	      scrollHeight = 550;
	      if ($(window).height() + $(window).scrollTop() >= $('.user-album-page')[0].offsetHeight) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      scrollTop = scrollTop;
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      $('.album-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	      $('.album-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	      $('.album-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	      $('.album-head .info > span').css('opacity', 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)));
	      return $('.album-head').height(370 - (scrollTop * getHeaderCoeficent(scrollHeight, 190)));
	    };

	    controller.prototype.deleteAlbum = function(id) {
	      var album, albums;
	      albums = app.request('get:favorite:albums');
	      album = albums.findWhere({
	        id: id
	      });
	      if (album != null) {
	        return album.destroy();
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Albums.Controller = new controller;
	  return Albums;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 88:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track) {
	  var Albums;
	  Albums = {};
	  Albums.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = '#user-albums-layout-tpl';

	    Layout.prototype.regions = {
	      content: '.content',
	      list: '.albums-list-container'
	    };

	    Layout.prototype.className = 'user-albums-page-wrapp';

	    Layout.prototype.onShow = function() {
	      var albulistContainer, toTop;
	      toTop = $('body').hasClass('notifi') ? 320 : 280;
	      albulistContainer = $('.album-list-container');
	      $('.user-albums-page-wrapp .list').css('min-height', $(window).height() - 100);
	      albulistContainer.height($(window).height() - toTop);
	      $('body').css('background', '#111112');
	      return $(window).on('resize.userTracks', function() {
	        toTop = $('body').hasClass('notifi') ? 320 : 280;
	        $('.user-albums-page-wrapp .list').css('min-height', $(window).height() - 100);
	        return albulistContainer.height($(window).height() - toTop);
	      });
	    };

	    Layout.prototype.onDestroy = function() {
	      $('body').attr('style', '');
	      return $(window).off('.userTracks');
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Albums.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#user-album-item-tpl';

	    Item.prototype.className = 'user-album-item list-item';

	    Item.prototype.onRender = function() {
	      if (this.model.has('is_deleted')) {
	        return this.$el.addClass('disable');
	      }
	    };

	    Item.prototype.events = {
	      'click': function() {
	        if (this.model.has('is_deleted')) {
	          app.Parts.Notification.Controller.show({
	            message: app.locale.notification.albumNotAvailable,
	            type: 'error',
	            time: 3000
	          });
	          return;
	        }
	        Albums.Controller.openAlbum(this);
	        $('.list-item.active').removeClass('active');
	        return this.$el.addClass('active');
	      },
	      'click .delete': function() {
	        var albums, emptyView, hasAlbums, scrollHeight;
	        Albums.Controller.deleteAlbum(this.model.get('id'));
	        albums = app.request('get:favorite:albums');
	        if (albums.length === 0) {
	          $('.user-albums-page-wrapp').css('padding-left', 0);
	          emptyView = new Albums.EmptyLayout;
	          Albums.Controller.layout.content.show(emptyView);
	          $('.list').fadeOut(100);
	        } else {
	          hasAlbums = albums.some(function(item) {
	            return !item.get('is_deleted');
	          });
	          if (hasAlbums) {
	            scrollHeight = $('.albums-list-container').height() - $('.albums-list-container .scrollable').height() - 40;
	            if (scrollHeight < 0) {
	              $('.albums-list-container .scrollable').css('margin-top', 0);
	            }
	            $('.list .list-item').not('.disable').eq(0).click();
	          } else {
	            emptyView = new Albums.EmptyLayout;
	            Albums.Controller.layout.content.show(emptyView);
	            $('.albums-empty-layout .message').css({
	              'margin-left': '-140px'
	            });
	          }
	        }
	        return app.Parts.Notification.Controller.show({
	          message: "Альбом " + (this.model.get('title')) + " удален",
	          type: 'success'
	        });
	      }
	    };

	    return Item;

	  })(Marionette.ItemView);
	  Albums.EmptyLayout = (function(superClass) {
	    extend(EmptyLayout, superClass);

	    function EmptyLayout() {
	      return EmptyLayout.__super__.constructor.apply(this, arguments);
	    }

	    EmptyLayout.prototype.template = '#user-albums-empty-layout-tpl';

	    EmptyLayout.prototype.className = 'albums-empty-layout';

	    EmptyLayout.prototype.events = {
	      'click .to-new': function() {
	        return app.trigger('show:new');
	      }
	    };

	    EmptyLayout.prototype.onShow = function() {
	      return this.$el.height($(window).height() - 80);
	    };

	    return EmptyLayout;

	  })(Marionette.ItemView);
	  Albums.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.childView = Albums.Item;

	    Items.prototype.className = 'scrollable';

	    Items.prototype.onShow = function() {
	      var emptyView, hasAlbums;
	      if (this.collection.length === 0) {
	        $('.user-albums-page-wrapp').css('padding-left', 0);
	        emptyView = new Albums.EmptyLayout;
	        return Albums.Controller.layout.content.show(emptyView);
	      } else {
	        hasAlbums = this.collection.some(function(item) {
	          return !item.get('is_deleted');
	        });
	        if (!hasAlbums) {
	          emptyView = new Albums.EmptyLayout;
	          Albums.Controller.layout.content.show(emptyView);
	          $('.albums-empty-layout .message').css({
	            'margin-left': '-140px'
	          });
	        }
	        $('.list').fadeIn(100);
	        return $('.user-albums-page-wrapp').attr('style', '');
	      }
	    };

	    Items.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.prepend(itemView.el);
	      };
	    };

	    return Items;

	  })(Marionette.CollectionView);
	  Albums.TrackItem = (function(superClass) {
	    extend(TrackItem, superClass);

	    function TrackItem() {
	      return TrackItem.__super__.constructor.apply(this, arguments);
	    }

	    TrackItem.prototype.className = 'track-item user-track';

	    TrackItem.prototype.template = '#track-tpl';

	    return TrackItem;

	  })(Track.Item);
	  Albums.TrackItems = (function(superClass) {
	    extend(TrackItems, superClass);

	    function TrackItems() {
	      return TrackItems.__super__.constructor.apply(this, arguments);
	    }

	    TrackItems.prototype.className = 'tracks-wrapper';

	    TrackItems.prototype.childView = Albums.TrackItem;

	    return TrackItems;

	  })(Marionette.CollectionView);
	  Albums.PageLayout = (function(superClass) {
	    extend(PageLayout, superClass);

	    function PageLayout() {
	      return PageLayout.__super__.constructor.apply(this, arguments);
	    }

	    PageLayout.prototype.className = 'user-album-page';

	    PageLayout.prototype.template = '#user-albums-page-layout-tpl';

	    PageLayout.prototype.regions = {
	      'content': '.content',
	      'playlistOptions': '.album-options'
	    };

	    PageLayout.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('img'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.album-head-container').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      image.src = src;
	      return $clamp($('.album-head h1')[0], {
	        clamp: 2
	      });
	    };

	    PageLayout.prototype.events = {
	      'click .album-options': function() {
	        return Albums.Controller.openAlbumOptions(this);
	      },
	      'click .shuffle-track': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = $('.playlist-content-wrapp .tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      }
	    };

	    return PageLayout;

	  })(Marionette.LayoutView);
	  Albums.OptionsLayout = (function(superClass) {
	    extend(OptionsLayout, superClass);

	    function OptionsLayout() {
	      return OptionsLayout.__super__.constructor.apply(this, arguments);
	    }

	    OptionsLayout.prototype.template = '#user-album-options-tpl';

	    OptionsLayout.prototype.tagName = 'ul';

	    OptionsLayout.prototype.className = 'options-list';

	    OptionsLayout.prototype.events = {
	      'mouseleave': function() {},
	      'click .options-overlay': function() {
	        return this.destroy();
	      },
	      'click': function() {
	        return false;
	      },
	      'click .delete-album': function() {
	        var view;
	        view = Marionette.Renderer.render('#user-album-delete-confirm-tpl', this.model.toJSON());
	        return this.$el.empty().append(view);
	      },
	      'click .album-delete-confirm': function() {
	        return false;
	      },
	      'click .cancel': function() {
	        return this.destroy();
	      },
	      'click .delete': function() {
	        var albums, emptyView, hasAlbums, scrollHeight;
	        Albums.Controller.deleteAlbum(this.model.get('id'));
	        albums = app.request('get:favorite:albums');
	        if (albums.length === 0) {
	          $('.user-albums-page-wrapp').css('padding-left', 0);
	          emptyView = new Albums.EmptyLayout;
	          Albums.Controller.layout.content.show(emptyView);
	          $('.list').removeClass('slow').css('left', '-284px');
	        } else {
	          hasAlbums = albums.some(function(item) {
	            return !item.get('is_deleted');
	          });
	          if (hasAlbums) {
	            scrollHeight = $('.albums-list-container').height() - $('.albums-list-container .scrollable').height() - 40;
	            if (scrollHeight < 0) {
	              $('.albums-list-container .scrollable').css('margin-top', 0);
	            }
	            $('.list .list-item').not('.disable').eq(0).click();
	          } else {
	            emptyView = new Albums.EmptyLayout;
	            Albums.Controller.layout.content.show(emptyView);
	            $('.albums-empty-layout .message').css({
	              'margin-left': '-140px'
	            });
	          }
	        }
	        return app.Parts.Notification.Controller.show({
	          message: "Альбом " + (this.model.get('title')) + " удален",
	          type: 'success'
	        });
	      }
	    };

	    return OptionsLayout;

	  })(Marionette.ItemView);
	  return Albums;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 89:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});