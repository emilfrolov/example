webpackJsonp([1],{

/***/ 43:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(44)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Recomend) {
	  var controller;
	  __webpack_require__(49);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function() {
	      this.layout = new Recomend.Layout;
	      app.content.show(this.layout);
	      this.showMainBlock();
	      return this.showRecomendBlocks();
	    };

	    controller.prototype.showMainBlock = function() {
	      var main, self, showMain;
	      self = this;
	      showMain = function(model) {
	        var layout;
	        if (!model.has('isEmpty')) {
	          layout = new Recomend.MainLayout({
	            model: model
	          });
	          self.layout.main.show(layout);
	          return self.showMainItems(layout, model);
	        }
	      };
	      main = app.request('get:recomend:main');
	      if (main.promise) {
	        main.done(function(model) {
	          return showMain(model);
	        });
	        return main.fail(function() {
	          return console.log(arguments);
	        });
	      } else {
	        return showMain(main);
	      }
	    };

	    controller.prototype.showMainItems = function(layout, model) {
	      var album, albumModel, artist, artistModel, chart, chartItems, chartView, i, item, j, len, playlist, playlistModel, playlistTitle, track, trackData, trackModel;
	      albumModel = new Backbone.Model(model.get('album'));
	      album = new Recomend.Album({
	        model: albumModel
	      });
	      layout.album.show(album);
	      playlistModel = new Backbone.Model(model.get('playlist'));
	      playlistTitle = playlistModel.get('title');
	      playlistTitle = playlistTitle.slice(0, 1).toUpperCase() + playlistTitle.slice(1);
	      playlistModel.set('title', playlistTitle);
	      playlist = new Recomend.Playlist({
	        model: playlistModel
	      });
	      layout.playlist.show(playlist);
	      trackModel = (function(superClass1) {
	        extend(trackModel, superClass1);

	        function trackModel() {
	          return trackModel.__super__.constructor.apply(this, arguments);
	        }

	        trackModel.prototype.urlRoot = '/favoritesTracks';

	        trackModel.prototype.defaults = {
	          duration: ''
	        };

	        return trackModel;

	      })(Backbone.Model);
	      trackData = model.get('track');
	      if (trackData.is_deleted) {
	        trackData.favourites.sort = +trackData.favourites.sort - 100000;
	      }
	      trackData.uniq = 'recomenduser-' + model.id;
	      trackData.optionsList = [
	        {
	          title: app.locale.options.addTPl,
	          "class": 'add-to-playlist'
	        }, {
	          title: app.locale.options.toArt,
	          "class": 'options-open-artist'
	        }, {
	          title: app.locale.options.toAlb,
	          "class": 'options-open-album'
	        }
	      ];
	      trackModel = new trackModel(trackData);
	      track = new Recomend.Track({
	        model: trackModel
	      });
	      layout.track.show(track);
	      artistModel = new Backbone.Model(model.get('artist'));
	      artist = new Recomend.Artist({
	        model: artistModel
	      });
	      layout.artist.show(artist);
	      chart = model.get('chart');
	      chartItems = chart['items'];
	      for (i = j = 0, len = chartItems.length; j < len; i = ++j) {
	        item = chartItems[i];
	        item['index'] = i + 1;
	        _.extend(item, item.track);
	        if (item.track == null) {
	          item['is_deleted'] = true;
	        }
	      }
	      chart = new Backbone.Model(chart);
	      chartItems = new Backbone.Collection(chartItems);
	      chartView = new Recomend.Charts({
	        model: chart,
	        collection: chartItems
	      });
	      return layout.charts.show(chartView);
	    };

	    controller.prototype.showRecomendBlocks = function() {
	      var blocks, self, showPageBlocks;
	      self = this;
	      showPageBlocks = function(items) {
	        var layout;
	        layout = new Recomend.BlocksLayout;
	        self.layout.blocks.show(layout);
	        return self.drawPageBlocks(layout, items);
	      };
	      blocks = app.request('get:recomend:blocks');
	      if (blocks.promise) {
	        blocks.done(function(item) {
	          return showPageBlocks(item);
	        });
	        return blocks.fail(function() {
	          return console.log('err');
	        });
	      } else {
	        return showPageBlocks(blocks);
	      }
	    };

	    controller.prototype.drawPageBlocks = function(layout, blocks) {
	      var artist, artistCollection, artistModel, charts, chartsCollection, chartsModel, clips, clipsCollection, clipsModel, collection, item, j, len, playlistCollection, playlistModel, playlists, releaseCollection, releaseModel, releases;
	      if (blocks.has('playlists')) {
	        playlistModel = new Backbone.Model(blocks.get('playlists'));
	        playlistCollection = new Backbone.Collection(playlistModel.get('playlists'));
	        playlists = new Recomend.PlaylistsBlock({
	          model: playlistModel,
	          collection: playlistCollection
	        });
	        layout.playlists.show(playlists);
	      }
	      if (blocks.has('releases')) {
	        releaseModel = new Backbone.Model(blocks.get('releases'));
	        releaseCollection = new Backbone.Collection(releaseModel.get('releases'));
	        releases = new Recomend.ReleasesBlock({
	          model: releaseModel,
	          collection: releaseCollection
	        });
	        layout.releases.show(releases);
	      }
	      if (blocks.has('clips')) {
	        clipsModel = new Backbone.Model(blocks.get('clips'));
	        clipsCollection = new Backbone.Collection(clipsModel.get('clips'));
	        clips = new Recomend.ClipsBlock({
	          model: clipsModel,
	          collection: clipsCollection
	        });
	        layout.clips.show(clips);
	      }
	      if (blocks.has('charts')) {
	        chartsModel = new Backbone.Model(blocks.get('charts'));
	        collection = chartsModel.get('charts');
	        for (j = 0, len = collection.length; j < len; j++) {
	          item = collection[j];
	          item['chart_label'] = item.title;
	          _.extend(item, item.track);
	          _.extend(item, item.track);
	          if (item.track == null) {
	            item['is_deleted'] = true;
	          }
	        }
	        chartsCollection = new Backbone.Collection(collection);
	        charts = new Recomend.ChartsBlock({
	          model: chartsModel,
	          collection: chartsCollection
	        });
	        layout.charts.show(charts);
	      }
	      if (blocks.has('artist')) {
	        artistModel = new Backbone.Model(blocks.get('artist'));
	        artistCollection = new Backbone.Collection(artistModel.get('items'));
	        artist = new Recomend.ArtistBlock({
	          model: artistModel,
	          collection: artistCollection
	        });
	        return layout.artist.show(artist);
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Recomend.Controller = new controller;
	  return Recomend;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 44:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(26), __webpack_require__(34), __webpack_require__(37), __webpack_require__(1), __webpack_require__(45)], __WEBPACK_AMD_DEFINE_RESULT__ = function(CustomTrack, CustomAlbum, CustomArtist, CustomPlaylist, Player, VideoPlayer) {
	  var Recomend;
	  Recomend = {};
	  Recomend.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = function() {
	      return "<div class=\"main\">\n	<div class=\"video\">\n		<video src=\"\" loop autoplay></video>\n	</div>\n	<div class=\"content\"></div>\n</div>\n    			<div class=\"blocks\"></div>";
	    };

	    Layout.prototype.className = 'page recomend-page';

	    Layout.prototype.regions = {
	      main: '.main .content',
	      blocks: '.blocks'
	    };

	    Layout.prototype.onShow = function() {
	      var date, day;
	      $('body').css('background', '#111112');
	      date = new Date();
	      day = date.getDay();
	      day = day > 6 ? 6 : day;
	      return $('.video video').attr('src', 'http://music.beeline.ru/public/video/recomend/music-bg-' + day + '.mp4');
	    };

	    Layout.prototype.onDestroy = function() {
	      return $('body').attr('style', '');
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Recomend.MainLayout = (function(superClass) {
	    extend(MainLayout, superClass);

	    function MainLayout() {
	      return MainLayout.__super__.constructor.apply(this, arguments);
	    }

	    MainLayout.prototype.template = function(data) {
	      return __webpack_require__(48)(data);
	    };

	    MainLayout.prototype.className = 'main-block-wrapp';

	    MainLayout.prototype.regions = {
	      album: '.album-block',
	      artist: '.artist-block',
	      track: '.track-block',
	      playlist: '.playlist-block',
	      charts: '.charts'
	    };

	    MainLayout.prototype.events = {
	      'click .clip-block': function() {
	        var model;
	        model = new Backbone.Model(this.model.get('clip'));
	        return VideoPlayer.Controller.openVideo(model);
	      }
	    };

	    MainLayout.prototype.onShow = function() {
	      var self;
	      self = this;
	      return this.player = new YT.Player('youtube-video-bg', {
	        videoId: this.model.get('clip').id,
	        events: {
	          'onReady': self.startVideo,
	          'onStateChange': self.onPlayerStateChange
	        },
	        playerVars: {
	          frameborder: 0,
	          allowfullscreen: true,
	          controls: 0,
	          disablekb: 0,
	          loop: 1,
	          rel: 0,
	          iv_load_policy: 3,
	          showinfo: 0,
	          modestbranding: 0
	        }
	      });
	    };

	    MainLayout.prototype.onPlayerStateChange = function() {};

	    MainLayout.prototype.startVideo = function(data) {
	      data.target.setVolume(0);
	      return data.target.playVideo();
	    };

	    return MainLayout;

	  })(Marionette.LayoutView);
	  Recomend.BlocksLayout = (function(superClass) {
	    extend(BlocksLayout, superClass);

	    function BlocksLayout() {
	      return BlocksLayout.__super__.constructor.apply(this, arguments);
	    }

	    BlocksLayout.prototype.template = '#recomend-blocks-layout-tpl';

	    BlocksLayout.prototype.className = 'recomend-blocks-wrapp';

	    BlocksLayout.prototype.regions = {
	      playlists: '.playlists-container',
	      releases: '.releases-container',
	      clips: '.clips-container',
	      artist: '.artist-container',
	      charts: '.charts-container'
	    };

	    return BlocksLayout;

	  })(Marionette.LayoutView);
	  Recomend.ChartsItem = (function(superClass) {
	    extend(ChartsItem, superClass);

	    function ChartsItem() {
	      return ChartsItem.__super__.constructor.apply(this, arguments);
	    }

	    ChartsItem.prototype.template = '#recomend-charts-item-tpl';

	    ChartsItem.prototype.className = 'main-chart-item track-item';

	    return ChartsItem;

	  })(CustomTrack.Item);
	  Recomend.Charts = (function(superClass) {
	    extend(Charts, superClass);

	    function Charts() {
	      return Charts.__super__.constructor.apply(this, arguments);
	    }

	    Charts.prototype.template = '#recomend-charts-block-tpl';

	    Charts.prototype.childView = Recomend.ChartsItem;

	    Charts.prototype.childViewContainer = '.charts-container';

	    Charts.prototype.className = 'main-charts-block';

	    return Charts;

	  })(Marionette.CompositeView);
	  Recomend.Album = (function(superClass) {
	    extend(Album, superClass);

	    function Album() {
	      return Album.__super__.constructor.apply(this, arguments);
	    }

	    Album.prototype.template = '#recomend-album-item-tpl';

	    Album.prototype.className = 'album-item recomend-main-item';

	    Album.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    Album.prototype.onShow = function() {
	      $clamp(this.$('.title')[0], {
	        clamp: 2
	      });
	      return $clamp(this.$('.artist')[0], {
	        clamp: 2
	      });
	    };

	    return Album;

	  })(CustomAlbum.Item);
	  Recomend.Playlist = (function(superClass) {
	    extend(Playlist, superClass);

	    function Playlist() {
	      return Playlist.__super__.constructor.apply(this, arguments);
	    }

	    Playlist.prototype.template = '#recomend-playlist-item-tpl';

	    Playlist.prototype.className = 'album-item recomend-main-item';

	    Playlist.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('image'), '300x300');
	      return img.src = link;
	    };

	    Playlist.prototype.onShow = function() {
	      $clamp(this.$('.title')[0], {
	        clamp: 2
	      });
	      return $clamp(this.$('.artist')[0], {
	        clamp: 2
	      });
	    };

	    return Playlist;

	  })(CustomPlaylist.Item);
	  Recomend.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.template = '#recomend-track-item-tpl';

	    Track.prototype.className = 'track-item recomend-main-item';

	    Track.prototype.id = function() {
	      return this.model.get('uniq');
	    };

	    Track.prototype.events = {
	      'click': function() {
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          app.playedAlbum = null;
	          return Player.Controller.playTrack(this.model.get('id'), this.model.toJSON(), Backbone.history.location.href);
	        }
	      }
	    };

	    Track.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    Track.prototype.onShow = function() {
	      $clamp(this.$('.title')[0], {
	        clamp: 2
	      });
	      return $clamp(this.$('.artist')[0], {
	        clamp: 2
	      });
	    };

	    return Track;

	  })(Marionette.ItemView);
	  Recomend.Artist = (function(superClass) {
	    extend(Artist, superClass);

	    function Artist() {
	      return Artist.__super__.constructor.apply(this, arguments);
	    }

	    Artist.prototype.template = '#recomend-artist-item-tpl';

	    Artist.prototype.className = 'recomend-main-item';

	    Artist.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    Artist.prototype.onShow = function() {
	      return $clamp(this.$('.title')[0], {
	        clamp: 2
	      });
	    };

	    return Artist;

	  })(CustomArtist.Item);
	  Recomend.PlaylistItem = (function(superClass) {
	    extend(PlaylistItem, superClass);

	    function PlaylistItem() {
	      return PlaylistItem.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItem.prototype.template = '#playlist-item-tpl';

	    PlaylistItem.prototype.className = 'playlist-item album-item';

	    PlaylistItem.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('image'), '300x300');
	      return img.src = link;
	    };

	    PlaylistItem.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return PlaylistItem;

	  })(CustomPlaylist.Item);
	  Recomend.PlaylistsBlock = (function(superClass) {
	    extend(PlaylistsBlock, superClass);

	    function PlaylistsBlock() {
	      return PlaylistsBlock.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistsBlock.prototype.childView = Recomend.PlaylistItem;

	    PlaylistsBlock.prototype.template = '#recomend-block-container-tpl';

	    PlaylistsBlock.prototype.className = 'recomend-playlists-block recomend-block playlists-wrapper';

	    PlaylistsBlock.prototype.childViewContainer = '.block-content';

	    PlaylistsBlock.prototype.onShow = function() {
	      if (this.model.get('background_image') != null) {
	        this.$el.css('background-image', 'url("' + this.model.get('background_image') + '")');
	      }
	      if (this.model.get('background_colour') != null) {
	        return this.$el.css('background-color', this.model.get('background_colour'));
	      }
	    };

	    return PlaylistsBlock;

	  })(Marionette.CompositeView);
	  Recomend.ReleaseItem = (function(superClass) {
	    extend(ReleaseItem, superClass);

	    function ReleaseItem() {
	      return ReleaseItem.__super__.constructor.apply(this, arguments);
	    }

	    ReleaseItem.prototype.template = '#album-item-tpl';

	    ReleaseItem.prototype.className = 'album-item';

	    ReleaseItem.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    ReleaseItem.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return ReleaseItem;

	  })(CustomAlbum.Item);
	  Recomend.ReleasesBlock = (function(superClass) {
	    extend(ReleasesBlock, superClass);

	    function ReleasesBlock() {
	      return ReleasesBlock.__super__.constructor.apply(this, arguments);
	    }

	    ReleasesBlock.prototype.childView = Recomend.ReleaseItem;

	    ReleasesBlock.prototype.template = '#recomend-block-container-tpl';

	    ReleasesBlock.prototype.className = 'recomend-releases-block recomend-block albums-wrapper';

	    ReleasesBlock.prototype.childViewContainer = '.block-content';

	    ReleasesBlock.prototype.onShow = function() {
	      if (this.model.get('background_image') != null) {
	        this.$el.css('background-image', 'url("' + this.model.get('background_image') + '")');
	      }
	      if (this.model.get('background_colour') != null) {
	        return this.$el.css('background-color', this.model.get('background_colour'));
	      }
	    };

	    return ReleasesBlock;

	  })(Marionette.CompositeView);
	  Recomend.ClipItem = (function(superClass) {
	    extend(ClipItem, superClass);

	    function ClipItem() {
	      return ClipItem.__super__.constructor.apply(this, arguments);
	    }

	    ClipItem.prototype.template = '#recomend-video-item-tpl';

	    ClipItem.prototype.className = 'recomend-video-item';

	    ClipItem.prototype.events = {
	      'click': function() {
	        return VideoPlayer.Controller.openVideo(this.model);
	      }
	    };

	    ClipItem.prototype.onRender = function() {
	      var snippet;
	      snippet = this.model.get('snippet');
	      snippet.resourceId = {
	        videoId: this.model.get('id')
	      };
	      return this.model.set('snippet', snippet);
	    };

	    ClipItem.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    return ClipItem;

	  })(Marionette.ItemView);
	  Recomend.ClipsBlock = (function(superClass) {
	    extend(ClipsBlock, superClass);

	    function ClipsBlock() {
	      return ClipsBlock.__super__.constructor.apply(this, arguments);
	    }

	    ClipsBlock.prototype.childView = Recomend.ClipItem;

	    ClipsBlock.prototype.template = '#recomend-block-container-tpl';

	    ClipsBlock.prototype.className = 'recomend-clips-block recomend-block clips-wrapper';

	    ClipsBlock.prototype.childViewContainer = '.block-content';

	    ClipsBlock.prototype.onShow = function() {
	      if (this.model.get('background_image') != null) {
	        this.$el.css('background-image', 'url("' + this.model.get('background_image') + '")');
	      }
	      if (this.model.get('background_colour') != null) {
	        return this.$el.css('background-color', this.model.get('background_colour'));
	      }
	    };

	    return ClipsBlock;

	  })(Marionette.CompositeView);
	  Recomend.ChartItem = (function(superClass) {
	    extend(ChartItem, superClass);

	    function ChartItem() {
	      return ChartItem.__super__.constructor.apply(this, arguments);
	    }

	    ChartItem.prototype.template = '#recomend-chart-item-tpl';

	    ChartItem.prototype.className = 'recomend-chart-item track-item';

	    return ChartItem;

	  })(CustomTrack.Item);
	  Recomend.ChartsBlock = (function(superClass) {
	    extend(ChartsBlock, superClass);

	    function ChartsBlock() {
	      return ChartsBlock.__super__.constructor.apply(this, arguments);
	    }

	    ChartsBlock.prototype.childView = Recomend.ChartItem;

	    ChartsBlock.prototype.template = '#recomend-block-container-tpl';

	    ChartsBlock.prototype.className = 'recomend-charts-block recomend-block charts-wrapper';

	    ChartsBlock.prototype.childViewContainer = '.block-content';

	    ChartsBlock.prototype.onShow = function() {
	      if (this.model.get('background_image') != null) {
	        this.$el.css('background-image', 'url("' + this.model.get('background_image') + '")');
	      }
	      if (this.model.get('background_colour') != null) {
	        return this.$el.css('background-color', this.model.get('background_colour'));
	      }
	    };

	    return ChartsBlock;

	  })(Marionette.CompositeView);
	  Recomend.ArtistItem = (function(superClass) {
	    extend(ArtistItem, superClass);

	    function ArtistItem() {
	      return ArtistItem.__super__.constructor.apply(this, arguments);
	    }

	    ArtistItem.prototype.template = '#recomend-artist-popular-item-tpl';

	    ArtistItem.prototype.className = function() {
	      var className;
	      className = 'recomend-artist-popular-item';
	      if (this.model.get('type') === 'album') {
	        className += ' album-item';
	      } else if (this.model.get('type') === 'track') {
	        className += ' track-item';
	      }
	      return className;
	    };

	    ArtistItem.prototype.regions = {
	      optionsRegion: '.options'
	    };

	    ArtistItem.prototype.id = function() {
	      if (this.model.get('type') !== 'track') {
	        return this.model.get('id');
	      }
	    };

	    ArtistItem.prototype.onRender = function() {
	      var img, link;
	      if (this.model.get('type') === 'track') {
	        this.model.set('uniq', 'recomenduserartist-' + this.model.id);
	        this.model.set('optionsList', [
	          {
	            title: app.locale.options.addTPl,
	            "class": 'add-to-playlist'
	          }, {
	            title: app.locale.options.toArt,
	            "class": 'options-open-artist'
	          }, {
	            title: app.locale.options.toAlb,
	            "class": 'options-open-album'
	          }
	        ]);
	        this.$el.attr('id', this.model.get('uniq'));
	      }
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    ArtistItem.prototype.onShow = function() {
	      return this.$el.after(' ');
	    };

	    ArtistItem.prototype.events = {
	      'click .play-album': function(e) {
	        e.stopPropagation();
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          return CustomAlbum.Controller.Play(this.model);
	        }
	      },
	      'mouseenter': function() {
	        var favorites, item;
	        favorites = app.request('get:favorite:albums');
	        if (favorites.promise) {
	          favorites.done((function(_this) {
	            return function(collection) {
	              var item;
	              item = collection.some(function(item) {
	                return item.get('id') === _this.model.get('id');
	              });
	              if (item) {
	                _this.model.set('inFavorite', true);
	                return _this.$('.move').removeClass('add').addClass('delete');
	              } else {
	                _this.model.set('inFavorite', false);
	                return _this.$('.move').removeClass('delete').addClass('add');
	              }
	            };
	          })(this));
	          return favorites.fail(function(err) {
	            return console.log(arguments);
	          });
	        } else {
	          item = favorites.some((function(_this) {
	            return function(item) {
	              return item.get('id') === _this.model.get('id');
	            };
	          })(this));
	          if (item) {
	            this.model.set('inFavorite', true);
	            return this.$('.move').removeClass('add').addClass('delete');
	          } else {
	            this.model.set('inFavorite', false);
	            return this.$('.move').removeClass('delete').addClass('add');
	          }
	        }
	      },
	      'click .controll-album': function() {
	        return app.trigger('show:album', this.model.get('id'), this.model);
	      },
	      'click .info .title-album': function() {
	        return app.trigger('show:album', this.model.get('id'), this.model);
	      },
	      'click .move': function(e) {
	        return e.stopPropagation();
	      },
	      'click .options': function(e) {
	        e.stopPropagation();
	        return CustomAlbum.Controller.openOptions(this);
	      },
	      'click .add': function() {
	        return this.addToAlbum();
	      },
	      'click .delete': function() {
	        return this.removeFromAlbum();
	      },
	      'click .to-artist': function() {
	        return app.trigger('show:artist', this.model.get('artistid'));
	      },
	      'click .play-track': function() {
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          app.playedAlbum = null;
	          return Player.Controller.playTrack(this.model.get('id'), this.model.toJSON(), Backbone.history.location.href);
	        }
	      }
	    };

	    ArtistItem.prototype.addToAlbum = function() {
	      $('.options-wrapper .add').removeClass('add').addClass('delete').text('Удалить');
	      CustomAlbum.Controller.addAlbumToFavorite(this.model);
	      this.model.set('inFavorite', true);
	      this.$('.move').removeClass('add').addClass('delete');
	      return false;
	    };

	    ArtistItem.prototype.removeFromAlbum = function() {
	      $('.options-wrapper .delete').removeClass('delete').addClass('add').text('Сохранить');
	      CustomAlbum.Controller.removeAlbumFromFavorite(this.model.get('id'));
	      this.model.set('inFavorite', false);
	      this.$('.move').removeClass('delete').addClass('add');
	      return false;
	    };

	    return ArtistItem;

	  })(Marionette.LayoutView);
	  Recomend.ArtistBlock = (function(superClass) {
	    extend(ArtistBlock, superClass);

	    function ArtistBlock() {
	      return ArtistBlock.__super__.constructor.apply(this, arguments);
	    }

	    ArtistBlock.prototype.childView = Recomend.ArtistItem;

	    ArtistBlock.prototype.template = '#recomend-block-container-tpl';

	    ArtistBlock.prototype.className = 'recomend-artist-popular-block recomend-block artist-popular-wrapper';

	    ArtistBlock.prototype.childViewContainer = '.block-content';

	    ArtistBlock.prototype.onShow = function() {
	      if (this.model.get('background_image') != null) {
	        this.$el.css('background-image', 'url("' + this.model.get('background_image') + '")');
	      }
	      if (this.model.get('background_colour') != null) {
	        return this.$el.css('background-color', this.model.get('background_colour'));
	      }
	    };

	    return ArtistBlock;

	  })(Marionette.CompositeView);
	  return Recomend;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 45:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(46)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player) {
	  var controller;
	  __webpack_require__(47);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.openVideo = function(model) {
	      var view;
	      view = new Player.Item({
	        model: model
	      });
	      return app.video.show(view);
	    };

	    return controller;

	  })(Marionette.Controller);
	  Player.Controller = new controller;
	  return Player;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 46:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player) {
	  Player = {};
	  Player.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#video-frame-tpl';

	    Item.prototype.className = 'video-overlay';

	    Item.prototype.onShow = function() {
	      if (Player.Controller.played) {
	        Player.Controller.pause();
	        return Player.needStartPlay = true;
	      } else {
	        return Player.needStartPlay = false;
	      }
	    };

	    Item.prototype.onDestroy = function() {
	      if (Player.needStartPlay) {
	        return Player.Controller.play();
	      }
	    };

	    Item.prototype.events = {
	      'click': function() {
	        return this.destroy();
	      }
	    };

	    return Item;

	  })(Marionette.ItemView);
	  return Player;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 47:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },

/***/ 48:
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (clip, description, title) {
	buf.push("<div class=\"main-head\"><div class=\"title\">" + (jade.escape((jade_interp = typeof(title) == 'undefined' ? '' : title) == null ? '' : jade_interp)) + "</div><div class=\"description\">" + (jade.escape((jade_interp = typeof(description) == 'undefined' ? '' : description ) == null ? '' : jade_interp)) + "</div></div><div class=\"main-body\"><div class=\"main-left\"><div class=\"clip-block\"><h1>Клип</h1><h2>" + (jade.escape((jade_interp = clip.title) == null ? '' : jade_interp)) + "</h2><div id=\"youtube-video-bg\"></div></div><div class=\"charts\"></div></div><div class=\"main-right\"><div class=\"album-block\"></div><div class=\"playlist-block\"></div><div class=\"track-block\"></div><div class=\"artist-block\"></div></div></div>");}.call(this,"clip" in locals_for_with?locals_for_with.clip:typeof clip!=="undefined"?clip:undefined,"description" in locals_for_with?locals_for_with.description:typeof description!=="undefined"?description:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
	}

/***/ },

/***/ 49:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});