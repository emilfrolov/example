webpackJsonp([3],{

/***/ 57:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(58), __webpack_require__(22), __webpack_require__(26), __webpack_require__(37), __webpack_require__(30), __webpack_require__(28), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(User, Track, Album, Playlist, Loader, Update, Notification) {
	  var controller;
	  __webpack_require__(59);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function(id) {
	      var self, user;
	      self = this;
	      user = app.request('get:user', id);
	      user.done((function(_this) {
	        return function(user) {
	          self.publicUser = user;
	          self.pageView = new User.Page({
	            model: user
	          });
	          self.listenTo(self.pageView, 'show', function() {
	            if ($(window).width() < 1200 || $(window).height() < 800) {
	              this.pageSize = 'small';
	            } else {
	              this.pageSize = 'large';
	            }
	            $(window).on('resize.upage', (function(_this) {
	              return function() {
	                if ($(window).width() < 1200 || $(window).height() < 800) {
	                  if (_this.pageSize !== 'small') {
	                    _this.pageSize = 'small';
	                    return _this.resizeHeader();
	                  }
	                } else {
	                  if (_this.pageSize !== 'large') {
	                    _this.pageSize = 'large';
	                    return _this.resizeHeader();
	                  }
	                }
	              };
	            })(this));
	            return $(window).on('scroll.upage', (function(_this) {
	              return function() {
	                return self.resizeHeader();
	              };
	            })(this));
	          });
	          self.listenTo(self.pageView, 'destroy', function() {
	            return $(window).off('.upage');
	          });
	          app.content.show(self.pageView);
	          return $('.tabs .tracks').click();
	        };
	      })(this));
	      return user.fail(function(err) {
	        return console.log(err);
	      });
	    };

	    controller.prototype.showUserTracks = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(tracks) {
	        var collection, i, item, len, tracksView;
	        for (i = 0, len = tracks.length; i < len; i++) {
	          item = tracks[i];
	          item['uniq'] = 'user-page-' + user.get('user_id') + item.id;
	          item['optionsList'] = [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ];
	        }
	        collection = new Track.Collection(tracks);
	        tracksView = new Track.ListView({
	          collection: collection
	        });
	        self.pageView.content.show(tracksView);
	        return self.resizeHeader(true);
	      };
	      if (user.has('tracks')) {
	        return showContent(user.get('tracks'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.pageView.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicFavouriteTracks", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          showContent(data.tracks);
	          return user.set('tracks', data.tracks);
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showUserAlbums = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(tracks) {
	        var collection, view;
	        collection = new Album.Collection(tracks);
	        view = new Album.InlineListView({
	          collection: collection
	        });
	        self.pageView.content.show(view);
	        return self.resizeHeader(true);
	      };
	      if (user.has('albums')) {
	        return showContent(user.get('albums'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.pageView.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicFavouriteAlbums", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          showContent(data.albums);
	          return user.set('albums', data.albums);
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showUserPlaylists = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(playlists) {
	        var collection, view;
	        collection = new Playlist.Collection(playlists);
	        view = new Playlist.InlineListView({
	          collection: collection
	        });
	        self.pageView.content.show(view);
	        return self.resizeHeader(true);
	      };
	      if (user.has('playlists')) {
	        return showContent(user.get('playlists'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.pageView.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicPlaylists", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          showContent(data.playlists);
	          return user.set('playlists', data.playlists);
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showUserFollowers = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(followers) {
	        var collection, view;
	        collection = new Backbone.Collection(followers);
	        view = new User.Followers({
	          collection: collection
	        });
	        self.pageView.content.show(view);
	        return self.resizeHeader(true);
	      };
	      if (user.has('followers')) {
	        return showContent(user.get('followers'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.pageView.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getFollowers", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          var req, usersData;
	          usersData = {
	            userIds: data.user_ids
	          };
	          req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicProfiles"), usersData);
	          return req.done(function(data) {
	            showContent(data.users);
	            return user.set('followers', data.users);
	          });
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    controller.prototype.showUserFollowed = function(user) {
	      var fetch, loader, self, showContent;
	      self = this;
	      showContent = function(followed) {
	        var collection, view;
	        collection = new Backbone.Collection(followed);
	        view = new User.Followers({
	          collection: collection
	        });
	        self.pageView.content.show(view);
	        return self.resizeHeader(true);
	      };
	      if (user.has('followed')) {
	        return showContent(user.get('followed'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'top': '150px'
	          }
	        });
	        self.pageView.content.show(loader);
	        fetch = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getFollowedUsers", "?userId=" + (user.get('user_id'))));
	        fetch.done(function(data) {
	          var req, usersData;
	          usersData = {
	            userIds: data.user_ids
	          };
	          req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/getPublicProfiles"), usersData);
	          return req.done(function(data) {
	            showContent(data.users);
	            return user.set('followed', data.users);
	          });
	        });
	        return fetch.fail(function() {
	          return console.log(arguments);
	        });
	      }
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / (pathLength - 100);
	    };

	    controller.prototype.resizeHeader = function(force) {
	      var scrollHeight, scrollTop;
	      scrollHeight = 350;
	      if (($(window).height() + $(window).scrollTop() >= $('.user-page')[0].offsetHeight) && !force) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      scrollTop = scrollTop - 100;
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      if (this.pageSize === 'large') {
	        $('.user-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.user-head .shuffle-track').css('margin-top', 61 - (scrollTop * getHeaderCoeficent(scrollHeight, 21)));
	        $('.user-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.user-head .options .list').css('margin-top', 98 - (scrollTop * getHeaderCoeficent(scrollHeight, 68)));
	        $('.user-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 190 - (scrollTop * getHeaderCoeficent(scrollHeight, 60))
	        });
	        return $('.user-head').height(478 - (scrollTop * getHeaderCoeficent(scrollHeight, 178)));
	      } else {
	        $('.user-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.user-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	        $('.user-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.user-head .options .list').css('margin-top', 30);
	        $('.user-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 115 - (scrollTop * getHeaderCoeficent(scrollHeight, 45))
	        });
	        return $('.user-head').height(340 - (scrollTop * getHeaderCoeficent(scrollHeight, 120)));
	      }
	    };

	    controller.prototype.Subscribe = function(view) {
	      var req, userId;
	      userId = view.model.get('user_id');
	      req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/followUser", "?userId=" + userId));
	      req.done(function(data) {
	        if (data.code === 0) {
	          view.model.set('is_subscribed_to_user', true);
	          view.$('.options .save').removeClass('save').addClass('delete').text("Отписаться");
	          return Notification.Controller.show({
	            message: 'Вы успешно подписаны на пользователя',
	            type: 'success',
	            time: 3000
	          });
	        } else {
	          return Notification.Controller.show({
	            message: 'Ошибка подписки',
	            type: 'error',
	            time: 3000
	          });
	        }
	      });
	      return req.fail(function() {
	        return Notification.Controller.show({
	          message: 'Ошибка подписки',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.Unsubscribe = function(view) {
	      var req, userId;
	      userId = view.model.get('user_id');
	      req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/unfollowUser", "?userId=" + userId));
	      req.done(function(data) {
	        if (data.code === 0) {
	          view.model.set('is_subscribed_to_user', false);
	          view.$('.options .delete').removeClass('delete').addClass('save').text("Подписаться");
	          return Notification.Controller.show({
	            message: 'Вы успешно отписаны от пользователя',
	            type: 'success',
	            time: 3000
	          });
	        } else {
	          return Notification.Controller.show({
	            message: 'Ошибка отписки',
	            type: 'error',
	            time: 3000
	          });
	        }
	      });
	      return req.fail(function() {
	        return Notification.Controller.show({
	          message: 'Ошибка отписки',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.unsubscribeFollower = function(view) {
	      var req, self, userId;
	      self = this;
	      userId = view.model.get('user_id');
	      req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/unfollowUser", "?userId=" + userId));
	      req.done(function(data) {
	        var followed;
	        if (data.code === 0) {
	          view.model.set('is_subscribed_to_user', false);
	          view.$('.is-subscribe').removeClass('true').addClass('false');
	          self.publicUser.set('followers', view.model.collection.toJSON());
	          if (self.publicUser.has('followed')) {
	            followed = self.publicUser.get('followed');
	            _.map(followed, function(item) {
	              if (item['user_id'] === userId) {
	                return item['is_subscribed_to_user'] = false;
	              }
	            });
	            self.publicUser.set('followed', followed);
	          }
	          return Notification.Controller.show({
	            message: 'Вы успешно отписаны от пользователя',
	            type: 'success',
	            time: 3000
	          });
	        } else {
	          return Notification.Controller.show({
	            message: 'Ошибка отписки',
	            type: 'error',
	            time: 3000
	          });
	        }
	      });
	      return req.fail(function() {
	        return Notification.Controller.show({
	          message: 'Ошибка отписки',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.subscribeFollower = function(view) {
	      var req, self, userId;
	      self = this;
	      userId = view.model.get('user_id');
	      req = $.get(app.getRequestUrlSid("/beelineMusicV1/society/followUser", "?userId=" + userId));
	      req.done(function(data) {
	        var followed, found;
	        if (data.code === 0) {
	          view.model.set('is_subscribed_to_user', true);
	          view.$('.is-subscribe').removeClass('false').addClass('true');
	          self.publicUser.set('followers', view.model.collection.toJSON());
	          if (self.publicUser.has('followed')) {
	            followed = self.publicUser.get('followed');
	            found = 0;
	            _.map(followed, function(item) {
	              if (item['user_id'] === userId) {
	                found++;
	                return item['is_subscribed_to_user'] = true;
	              }
	            });
	            if (found === 0) {
	              followed.push(view.model.toJSON());
	            }
	            self.publicUser.set('followed', followed);
	          }
	          return Notification.Controller.show({
	            message: 'Вы успешно подписаны на пользователя',
	            type: 'success',
	            time: 3000
	          });
	        } else {
	          return Notification.Controller.show({
	            message: 'Ошибка подписки',
	            type: 'error',
	            time: 3000
	          });
	        }
	      });
	      return req.fail(function() {
	        return Notification.Controller.show({
	          message: 'Ошибка подписки',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    return controller;

	  })(Marionette.Controller);
	  User.Controller = new controller;
	  return User;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 58:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var User;
	  User = {};
	  User.Page = (function(superClass) {
	    extend(Page, superClass);

	    function Page() {
	      return Page.__super__.constructor.apply(this, arguments);
	    }

	    Page.prototype.className = 'user-page';

	    Page.prototype.template = '#user-page-tpl';

	    Page.prototype.regions = {
	      'content': '.content'
	    };

	    Page.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('avatar'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.album-head-wrapp').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      return image.src = src;
	    };

	    Page.prototype.events = {
	      'click .tabs li': function(e) {
	        this.$('.tabs li.active').removeClass('active');
	        return $(e.target).addClass('active');
	      },
	      'click .tabs .tracks': function() {
	        return User.Controller.showUserTracks(this.model);
	      },
	      'click .tabs .albums': function() {
	        return User.Controller.showUserAlbums(this.model);
	      },
	      'click .tabs .playlists': function() {
	        return User.Controller.showUserPlaylists(this.model);
	      },
	      'click .tabs .followers': function() {
	        return User.Controller.showUserFollowers(this.model);
	      },
	      'click .tabs .followed': function() {
	        return User.Controller.showUserFollowed(this.model);
	      },
	      'click .save': function() {
	        return User.Controller.Subscribe(this);
	      },
	      'click .delete': function() {
	        return User.Controller.Unsubscribe(this);
	      }
	    };

	    return Page;

	  })(Marionette.LayoutView);
	  User.Follower = (function(superClass) {
	    extend(Follower, superClass);

	    function Follower() {
	      return Follower.__super__.constructor.apply(this, arguments);
	    }

	    Follower.prototype.className = 'user-follower';

	    Follower.prototype.template = '#user-follower-tpl';

	    Follower.prototype.events = {
	      'click .is-subscribe.true': function() {
	        return User.Controller.unsubscribeFollower(this);
	      },
	      'click .is-subscribe.false': function() {
	        return User.Controller.subscribeFollower(this);
	      }
	    };

	    return Follower;

	  })(Marionette.ItemView);
	  User.Followers = (function(superClass) {
	    extend(Followers, superClass);

	    function Followers() {
	      return Followers.__super__.constructor.apply(this, arguments);
	    }

	    Followers.prototype.className = 'user-followers-wrapp';

	    Followers.prototype.childView = User.Follower;

	    return Followers;

	  })(Marionette.CollectionView);
	  return User;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 59:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});