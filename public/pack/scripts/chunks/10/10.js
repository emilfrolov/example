webpackJsonp([10],{

/***/ 81:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(82), __webpack_require__(30), __webpack_require__(28)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Playlist, Loader, Update) {
	  var controller;
	  __webpack_require__(83);
	  controller = (function(superClass) {
	    var getHeaderCoeficent;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.showPage = function(id, model) {
	      var loader, playlist, self;
	      self = this;
	      playlist = app.request('get:playlist', id);
	      if (model) {
	        return this.drawPage(model);
	      } else {
	        if (playlist.promise) {
	          loader = new Loader.View({
	            styles: {
	              'top': $(window).height() / 2
	            }
	          });
	          app.content.show(loader);
	          playlist.done((function(_this) {
	            return function(model) {
	              return _this.drawPage(model);
	            };
	          })(this));
	          return playlist.fail(function() {
	            var callback, update;
	            callback = function() {
	              return self.showPage(id);
	            };
	            update = new Update.View({
	              styles: {
	                'top': $(window).height() / 2
	              },
	              callback: callback
	            });
	            return app.content.show(update);
	          });
	        } else {
	          return this.drawPage(playlist);
	        }
	      }
	    };

	    controller.prototype.drawPage = function(model) {
	      var author;
	      app.GAPageAction('page-open', "Плейлист - " + (model.get('title')));
	      author = model.get('title');
	      if (author.length >= 30) {
	        author = author.slice(0, 27) + "...";
	      }
	      this.layout = new Playlist.Page({
	        model: model
	      });
	      this.listenTo(this.layout, 'show', function() {
	        this.showTracks(model);
	        if ($(window).width() < 1200 || $(window).height() < 800) {
	          this.pageSize = 'small';
	        } else {
	          this.pageSize = 'large';
	        }
	        this.resizeHeader();
	        $('.author-wrapp').width($('.author-wrapp .author').width() + $('.author-wrapp .release').width() + 40);
	        $(window).on('resize.album', (function(_this) {
	          return function() {
	            if ($(window).width() < 1200 || $(window).height() < 800) {
	              if (_this.pageSize !== 'small') {
	                _this.pageSize = 'small';
	                return _this.resizeHeader();
	              }
	            } else {
	              if (_this.pageSize !== 'large') {
	                _this.pageSize = 'large';
	                return _this.resizeHeader();
	              }
	            }
	          };
	        })(this));
	        return $(window).on('scroll.album', (function(_this) {
	          return function() {
	            return _this.resizeHeader();
	          };
	        })(this));
	      });
	      this.listenTo(this.layout, 'destroy', function() {
	        return $(window).off('.album');
	      });
	      return app.content.show(this.layout);
	    };

	    controller.prototype.showTracks = function(model) {
	      var fetch, loader, self, showTracks;
	      self = this;
	      showTracks = function(collection) {
	        var i, item, len, tracksView;
	        for (i = 0, len = collection.length; i < len; i++) {
	          item = collection[i];
	          item['uniq'] = 'track' + item.id;
	          item['optionsList'] = [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ];
	        }
	        collection = new Backbone.Collection(collection);
	        tracksView = new Playlist.Tracks({
	          collection: collection
	        });
	        self.layout.content.show(tracksView);
	        return app.checkActiveTrack();
	      };
	      if (model.has('tracks')) {
	        return showTracks(model.get('tracks'));
	      } else {
	        loader = new Loader.View({
	          styles: {
	            'margin': '100px auto'
	          }
	        });
	        this.layout.content.show(loader);
	        fetch = model.fetch();
	        self = this;
	        fetch.done(function() {
	          return showTracks(model.get('tracks'));
	        });
	        return fetch.fail(function() {
	          var callback, update;
	          callback = function() {
	            return self.showTracks(model);
	          };
	          update = new Update.View({
	            styles: {
	              'margin': '100px auto'
	            },
	            callback: callback
	          });
	          return this.layout.content.show(update);
	        });
	      }
	    };

	    getHeaderCoeficent = function(pathLength, changeLength) {
	      return changeLength / (pathLength - 0);
	    };

	    controller.prototype.resizeHeader = function() {
	      var scrollHeight, scrollTop;
	      scrollHeight = 550;
	      if ($(window).height() + $(window).scrollTop() >= $('.playlist-page')[0].offsetHeight) {
	        return;
	      }
	      scrollTop = $(window).scrollTop();
	      if (scrollTop >= scrollHeight) {
	        scrollTop = scrollHeight;
	      }
	      if (scrollTop <= 0) {
	        scrollTop = 0;
	      }
	      if (this.pageSize === 'large') {
	        $('.playlist-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.playlist-head .shuffle-track').css('margin-top', 21 - (scrollTop * getHeaderCoeficent(scrollHeight, 21)));
	        $('.playlist-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.playlist-head .options .list').css('margin-top', 98 - (scrollTop * getHeaderCoeficent(scrollHeight, 68)));
	        $('.playlist-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 190 - (scrollTop * getHeaderCoeficent(scrollHeight, 60))
	        });
	        return $('.playlist-head').height(478 - (scrollTop * getHeaderCoeficent(scrollHeight, 178)));
	      } else {
	        $('.playlist-head .info h1').css('font-size', 60 - (scrollTop * getHeaderCoeficent(scrollHeight, 30)));
	        $('.playlist-head .shuffle-track').css('margin-top', 20 - (scrollTop * getHeaderCoeficent(scrollHeight, 20)));
	        $('.playlist-head .author-wrapp').css('margin-top', 10 - (scrollTop * getHeaderCoeficent(scrollHeight, 10)));
	        $('.playlist-head .options .list').css('margin-top', 30);
	        $('.playlist-head .info > span').css({
	          'opacity': 1 - (scrollTop * getHeaderCoeficent(scrollHeight, 1)),
	          'padding-top': 115 - (scrollTop * getHeaderCoeficent(scrollHeight, 45))
	        });
	        return $('.playlist-head').height(340 - (scrollTop * getHeaderCoeficent(scrollHeight, 120)));
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Playlist.Controller = new controller;
	  return Playlist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 82:
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track) {
	  var Playlist;
	  Playlist = {};
	  Playlist.Page = (function(superClass) {
	    extend(Page, superClass);

	    function Page() {
	      return Page.__super__.constructor.apply(this, arguments);
	    }

	    Page.prototype.className = 'playlist-page';

	    Page.prototype.template = '#playlist-page-tpl';

	    Page.prototype.regions = {
	      'content': '.content'
	    };

	    Page.prototype.onShow = function() {
	      var image, src;
	      src = app.createImgLink(this.model.get('image'), '1200x1200', null, true);
	      image = new Image;
	      image.onload = (function(_this) {
	        return function() {
	          return _this.$('.playlist-head-wrapp').css('background-image', 'url(' + src + ')');
	        };
	      })(this);
	      return image.src = src;
	    };

	    Page.prototype.events = {
	      'click .shuffle-track': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = $('.tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      }
	    };

	    return Page;

	  })(Marionette.LayoutView);
	  Playlist.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.className = 'track-item';

	    Track.prototype.template = '#track-tpl';

	    return Track;

	  })(Track.Item);
	  Playlist.Tracks = (function(superClass) {
	    extend(Tracks, superClass);

	    function Tracks() {
	      return Tracks.__super__.constructor.apply(this, arguments);
	    }

	    Tracks.prototype.className = 'tracks-wrapper';

	    Tracks.prototype.childView = Playlist.Track;

	    return Tracks;

	  })(Marionette.CollectionView);
	  return Playlist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },

/***/ 83:
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }

});