/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	var parentJsonpFunction = window["webpackJsonp"];
/******/ 	window["webpackJsonp"] = function webpackJsonpCallback(chunkIds, moreModules) {
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, callbacks = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId])
/******/ 				callbacks.push.apply(callbacks, installedChunks[chunkId]);
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			modules[moduleId] = moreModules[moduleId];
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(chunkIds, moreModules);
/******/ 		while(callbacks.length)
/******/ 			callbacks.shift().call(null, __webpack_require__);

/******/ 	};

/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// object to store loaded and loading chunks
/******/ 	// "0" means "already loaded"
/******/ 	// Array means "loading", array contains callbacks
/******/ 	var installedChunks = {
/******/ 		0:0
/******/ 	};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}

/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId, callback) {
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] === 0)
/******/ 			return callback.call(null, __webpack_require__);

/******/ 		// an array means "currently loading".
/******/ 		if(installedChunks[chunkId] !== undefined) {
/******/ 			installedChunks[chunkId].push(callback);
/******/ 		} else {
/******/ 			// start chunk loading
/******/ 			installedChunks[chunkId] = [callback];
/******/ 			var head = document.getElementsByTagName('head')[0];
/******/ 			var script = document.createElement('script');
/******/ 			script.type = 'text/javascript';
/******/ 			script.charset = 'utf-8';
/******/ 			script.async = true;

/******/ 			script.src = __webpack_require__.p + "/scripts/chunks/" + ({}[chunkId]||chunkId) + "/" + chunkId + ".js?" + "b879a300b934d3a59091" + "";
/******/ 			head.appendChild(script);
/******/ 		}
/******/ 	};

/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/pack";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player) {
	  var clearPage;
	  __webpack_require__(13);
	  __webpack_require__(14);
	  __webpack_require__(15);
	  __webpack_require__(16);
	  __webpack_require__(17);
	  __webpack_require__(18);
	  __webpack_require__(19);
	  app.addRegions({
	    content: '#main',
	    video: '#video-container',
	    player: '#player',
	    sidebar: '#sidebar',
	    notification: '#notification',
	    popups: '#main-popups'
	  });
	  app.host = 'http://cdn.music.beeline.ru/api';
	  app.getRequestUrl = function(url, query) {
	    if (query == null) {
	      query = '';
	    }
	    return "" + app.host + url + "/" + query;
	  };
	  app.getRequestUrlSid = function(url, query) {
	    query = query ? query + "&sid=" + app.user.sid : "?sid=" + app.user.sid;
	    return "" + app.host + url + "/" + query;
	  };
	  app.navigate = function(route, options) {
	    options || (options = {});
	    route = ("/" + app.locale.lang) + route;
	    return Backbone.history.navigate(route, options);
	  };
	  app.readCookie = function(name) {
	    var c, ca, i, j, nameEQ, ref;
	    nameEQ = name + "=";
	    ca = document.cookie.split(';');
	    if (ca.length === 0) {
	      return null;
	    }
	    for (i = j = 0, ref = ca.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
	      c = ca[i];
	      while (c.charAt(0) === ' ') {
	        c = c.substring(1, c.length);
	      }
	      if (c.indexOf(nameEQ) === 0) {
	        return c.substring(nameEQ.length, c.length);
	      }
	    }
	    return null;
	  };
	  app.getCurrentRoute = function() {
	    return Backbone.history.fragment;
	  };
	  app.RandInt = function(low_limit, high_limit) {
	    var randoma;
	    randoma = Math.round(Math.random() * (high_limit - low_limit)) + low_limit;
	    return randoma;
	  };
	  app.createImgLink = function(link, size, blur, ro) {
	    if (link) {
	      ro = ro ? '_ro' : '_cr';
	      if (blur) {
	        return link.replace(/([^\/]+)\.([^\/]+)$/, size + ro + '_b0x7.$2');
	      } else {
	        return link.replace(/([^\/]+)\.([^\/]+)$/, size + ro + '.$2');
	      }
	    } else {
	      return '/img/no-onimg.png';
	    }
	  };
	  app.parsePath = function(path) {
	    var arr, j, len, res, val;
	    if (path != null) {
	      arr = path.split('&');
	      res = {};
	      for (j = 0, len = arr.length; j < len; j++) {
	        val = arr[j];
	        val = val.split('=');
	        res[val[0]] = val[1];
	      }
	      return res;
	    } else {
	      throw 'Параметр path не должен быть пустым';
	    }
	  };
	  app.isInfavorite = function(id) {
	    var favoriteTracks;
	    favoriteTracks = app.request('get:favorite:tracks');
	    return favoriteTracks.some(function(item) {
	      return item.get('id') === id;
	    });
	  };
	  app.checkActiveTrack = function() {
	    var currentHref;
	    currentHref = Backbone.history.location.href;
	    if (currentHref === app.playedHref) {
	      return $("#" + app.playedId).addClass('active ' + Player.Controller.playerClassStatus);
	    }
	  };
	  app.checkActiveAlbum = function() {
	    return $("#" + app.playedAlbum).addClass('active ' + Player.Controller.playerClassStatus);
	  };
	  app.declination = function(iNumber, aEndings) {
	    var i, sEnding;
	    iNumber = iNumber % 100;
	    if (iNumber >= 11 && iNumber <= 19) {
	      sEnding = aEndings[2];
	    } else {
	      i = iNumber % 10;
	      switch (i) {
	        case 1:
	          sEnding = aEndings[0];
	          break;
	        case 2:
	          sEnding = aEndings[1];
	          break;
	        case 3:
	          sEnding = aEndings[1];
	          break;
	        case 4:
	          sEnding = aEndings[1];
	          break;
	        default:
	          sEnding = aEndings[2];
	      }
	    }
	    return sEnding;
	  };
	  app.GAPageAction = function(cat, subcat) {
	    var user;
	    user = app.request('get:user');
	    if (user.promise) {
	      return user.done(function(user) {
	        return _gaq.push(['_trackEvent', cat, subcat, user.get('phone')]);
	      });
	    } else {
	      return _gaq.push(['_trackEvent', cat, subcat, user.get('phone')]);
	    }
	  };
	  app.readCookie = function(name) {
	    var c, ca, i, j, nameEQ, ref;
	    nameEQ = name + "=";
	    ca = document.cookie.split(';');
	    if (ca.length === 0) {
	      return null;
	    }
	    for (i = j = 0, ref = ca.length - 1; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
	      c = ca[i];
	      while (c.charAt(0) === ' ') {
	        c = c.substring(1, c.length);
	      }
	      if (c.indexOf(nameEQ) === 0) {
	        return c.substring(nameEQ.length, c.length);
	      }
	    }
	    return null;
	  };
	  app.getReferer = function() {
	    var url;
	    url = location.href;
	    if (~url.indexOf('/feed')) {
	      return 'wall';
	    } else if (~url.indexOf('/search')) {
	      return 'search';
	    } else if (~url.indexOf('/profile')) {
	      return 'profile';
	    } else if (~url.indexOf('/new')) {
	      return 'new';
	    } else if (~url.indexOf('/playlists')) {
	      return 'publicPlaylists';
	    } else if (~url.indexOf('/tracks')) {
	      return 'myTracks';
	    } else if (~url.indexOf('/artist')) {
	      return 'artist';
	    } else if (~url.indexOf('/album')) {
	      return 'release';
	    }
	    if (~url.indexOf('/')) {
	      return 'recommended';
	    }
	  };
	  app.getUserInfo = function() {
	    return app.user;
	  };
	  clearPage = function() {
	    $('.track-item.active').removeClass('active play pause');
	    return $('.album-item.active').removeClass('active play pause');
	  };
	  app.on('play', function() {
	    var currentHref;
	    currentHref = Backbone.history.location.href;
	    clearPage();
	    $("#" + app.playedAlbum).addClass('active pause');
	    if (currentHref === app.playedHref) {
	      return $("#" + app.playedId).addClass('active pause');
	    }
	  });
	  app.on('pause', function() {
	    var currentHref;
	    currentHref = Backbone.history.location.href;
	    clearPage();
	    $("#" + app.playedAlbum).addClass('active play');
	    if (currentHref === app.playedHref) {
	      return $("#" + app.playedId).addClass('active play');
	    }
	  });
	  app.on('start', function() {
	    var Entities, Sidebar;
	    Entities = __webpack_require__(90);
	    Entities.keys().forEach(function(path) {
	      return Entities(path);
	    });
	    app.logger = __webpack_require__(6);
	    app.logger.sendNewLog();
	    app.logger.createNewLog(app.getRequestUrlSid('/generalV1/log/enter'));
	    if (Backbone.history) {
	      Backbone.history.start({
	        pushState: true
	      });
	    }
	    Sidebar = __webpack_require__(20);
	    Sidebar.Controller.show();
	    $(document).on('mousewheel', '.scrollable', function(e) {
	      var parentHeight, scrollHeight, scrollStep, toTop;
	      e = e || window.event;
	      parentHeight = $(this).parent().height();
	      toTop = parseInt($(this)[0].style.marginTop) || 0;
	      scrollHeight = $(this).height() - parentHeight;
	      scrollStep = toTop + (e.deltaY * 30);
	      if (scrollHeight > 0) {
	        if (scrollStep > 0) {
	          scrollStep = 0;
	        } else if (-scrollStep > scrollHeight) {
	          scrollStep = -scrollHeight;
	        }
	        $(this).css({
	          'margin-top': scrollStep + 'px'
	        });
	        return false;
	      }
	    });
	    $(document).on('click', '.player-start-element', function() {
	      return $('.track-item').not('.disable').eq(0).click();
	    });
	    if (!window.setImmediate) {
	      return window.setImmediate = (function(_this) {
	        return function() {
	          var ID, head, onmessage, tail;
	          head = {};
	          tail = head;
	          ID = Math.random();
	          onmessage = function(e) {
	            var func;
	            if (e.data !== ID) {
	              return;
	            }
	            head = head.next;
	            func = head.func;
	            delete head.func;
	            return func();
	          };
	          if (window.addEventListener) {
	            window.addEventListener('message', onmessage);
	          } else {
	            window.attachEvent('onmessage', onmessage);
	          }
	          return function(func) {
	            tail = tail.next = {
	              func: func
	            };
	            return window.postMessage(ID, "*");
	          };
	        };
	      })(this)();
	    }
	  });
	  app.on('error', function() {
	    return console.log('errror');
	  });
	  return app;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2), __webpack_require__(6), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player, Logger, Notification) {
	  var controller;
	  __webpack_require__(12);
	  controller = (function(superClass) {
	    var SliderDrag;

	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.initialize = function() {
	      this.my_jPlayer = $("#bee-player");
	      this.svf_path = '/packages/jplayer_new/jquery.jplayer.swf';
	      $.jPlayer.timeFormat.padMin = false;
	      $.jPlayer.timeFormat.padSec = false;
	      $.jPlayer.timeFormat.sepMin = " min ";
	      $.jPlayer.timeFormat.sepSec = " sec";
	      this.firstPlay = true;
	      this.volume = sessionStorage.volume ? sessionStorage.volume : 0.5;
	      this.repeat = sessionStorage.repeat ? sessionStorage.repeat : false;
	      this.shuffle = sessionStorage.shuffle ? sessionStorage.shuffle : 'off';
	      this.initializePlayer();
	      this.listenTo(app, 'add:to:favorite', function() {
	        if (this.playerView) {
	          this.playerView.model.set('inFavorite', true, {
	            silent: true
	          });
	          return $('#player .track-controll').removeClass('add').addClass('delete');
	        }
	      });
	      return this.listenTo(app, 'remove:from:favorite', function() {
	        if (this.playerView) {
	          this.playerView.model.set('inFavorite', false, {
	            silent: true
	          });
	          return $('#player .track-controll').addClass('add').removeClass('delete');
	        }
	      });
	    };

	    controller.prototype.initializePlayer = function() {
	      var self;
	      self = this;
	      return this.my_jPlayer.jPlayer({
	        loadedmetadata: function(event) {
	          return self.trackDuration = event.jPlayer.status.duration;
	        },
	        play: function() {
	          $('.player-controll-element').removeClass('play').addClass('pause');
	          app.trigger('play');
	          return self.playerClassStatus = 'pause';
	        },
	        pause: function() {
	          $('.player-controll-element').removeClass('pause').addClass('play');
	          app.trigger('pause');
	          return self.playerClassStatus = 'play';
	        },
	        timeupdate: function(event) {
	          if (self.dragIsActive !== true) {
	            if (parseInt(event.jPlayer.status.currentPercentAbsolute) === 50) {
	              Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/streamingHalf", "?id=" + self.playedTrack + "&referer=" + (app.getReferer())));
	            }
	            $('.time-line .active-line').css('width', event.jPlayer.status.currentPercentAbsolute + '%');
	            if (parseInt(event.jPlayer.status.currentTime) === 29) {
	              Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/streaming30Seconds", "?id=" + self.playedTrack + "&referer=" + (app.getReferer())));
	            }
	            return self._setDuration(event.jPlayer.status.duration, event.jPlayer.status.currentTime);
	          }
	        },
	        ended: function() {
	          Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/streamingFull", "?id=" + self.playedTrack + "&referer=" + (app.getReferer())));
	          return self.nextTrck();
	        },
	        swfPath: self.svf_path,
	        supplied: "mp3"
	      });
	    };

	    controller.prototype.startPlayerAnimation = function() {
	      if (this.firstPlay) {
	        console.log($('.pseudo-player'));
	        $('.pseudo-player').fadeOut(300, function() {
	          return $(this).remove();
	        });
	        return this.firstPlay = false;
	      }
	    };

	    controller.prototype.streamById = function(id) {
	      var track;
	      track = this._loadTrack(id);
	      track.done((function(_this) {
	        return function(data) {
	          if ((data.code != null) && data.code === 0) {
	            Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/streamingStart", "?id=" + _this.playedTrack + "&referer=" + (app.getReferer())));
	            return _this.play(data.url);
	          } else {
	            return Notification.Controller.show({
	              message: 'Ошибка воспроизведения',
	              type: 'error',
	              time: 3000
	            });
	          }
	        };
	      })(this));
	      return track.fail(function() {
	        return Notification.Controller.show({
	          message: 'Ошибка воспроизведения',
	          type: 'error',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.playTrack = function(id, data, href) {
	      var trackData;
	      this.startPlayerAnimation();
	      if (data && href) {
	        app.GAPageAction('played track', data.title + " - " + href);
	      }
	      this.stop();
	      if (href) {
	        app.playedHref = href;
	      }
	      this.playedTrack = id;
	      if (data) {
	        app.playedId = data.uniq;
	        this.streamById(id);
	        return this.showPlayerData(data);
	      } else {
	        trackData = this._loadTrackData(id);
	        return trackData.done((function(_this) {
	          return function(data) {
	            if ((data.code != null) && data.code === 0) {
	              _this.streamById(id);
	              return _this.showPlayerData(data.tracks[0]);
	            } else {
	              return console.log('error');
	            }
	          };
	        })(this));
	      }
	    };

	    controller.prototype.playByCollection = function(collection, id, href) {
	      var length, middleOfCollection, normalizeCollection;
	      normalizeCollection = collection.reject(function(item) {
	        return item.get('is_deleted');
	      });
	      if (!id) {
	        length = normalizeCollection.length - 1;
	        if (length === 1) {
	          id = normalizeCollection[1].get('id');
	        } else {
	          middleOfCollection = app.RandInt(1, length - 1);
	          id = normalizeCollection[middleOfCollection].get('id');
	        }
	      }
	      this.playlist = this.createPlaylist(collection);
	      return this.playTrack(id, this.playlist[id].toJSON(), href);
	    };

	    controller.prototype.showPlayerData = function(data) {
	      var favorites, item;
	      delete data.playlistTrack;
	      if (!data.inFavorite) {
	        favorites = app.request('get:favorite:tracks');
	        if (!favorites.promise) {
	          item = favorites.some((function(_this) {
	            return function(item) {
	              return item.get('id') === data.id;
	            };
	          })(this));
	          if (item) {
	            data.inFavorite = true;
	          } else {
	            data.inFavorite = false;
	          }
	        } else {
	          data.inFavorite = false;
	        }
	      }
	      if (this.playerView == null) {
	        return this.showPlayer(data);
	      } else {
	        return this.playerView.model.set(data);
	      }
	    };

	    controller.prototype.showPlayer = function(data) {
	      var dragPlayer, dragSound, model;
	      model = new Backbone.Model(data);
	      this.playerView = new Player.View({
	        model: model
	      });
	      app.player.show(this.playerView);
	      dragPlayer = new SliderDrag('.time-line .active-zone', 'sound', this, '.time-line .active-line');
	      dragPlayer.ready();
	      dragSound = new SliderDrag('.volume .line .active', 'volume', this, '.volume .line .value');
	      dragSound.ready();
	      this.setDefaultVolume();
	      return this.setShuffle();
	    };

	    controller.prototype.setDefaultVolume = function() {
	      var volume;
	      volume = +this.volume;
	      this._setVolume(volume);
	      return $('.volume .line .value').width((volume * 100) + '%');
	    };

	    controller.prototype._loadTrack = function(id) {
	      var deferred;
	      if (this.trackLoad) {
	        this.trackLoad.abort();
	      }
	      this.trackLoad = $.get(app.getRequestUrlSid("/compatibility/stream/gethttp", "?itemid=" + id));
	      deferred = $.Deferred();
	      this.trackLoad.done(function(data) {
	        return deferred.resolveWith(this, [data]);
	      });
	      this.trackLoad.fail(function() {
	        return deferred.rejectWith(this, arguments);
	      });
	      return deferred.promise();
	    };

	    controller.prototype._loadTrackData = function(id) {
	      var deferred;
	      if (this.trackDataLoad) {
	        this.trackDataLoad.abort();
	      }
	      this.trackDataLoad = $.get('/getTrackData/' + id);
	      deferred = $.Deferred();
	      this.trackDataLoad.done(function(data) {
	        return deferred.resolveWith(this, [data]);
	      });
	      this.trackDataLoad.fail(function() {
	        return deferred.rejectWith(this, arguments);
	      });
	      return deferred.promise();
	    };

	    controller.prototype._setDuration = function(duration, currentTime) {
	      var time;
	      time = this._convertTrackDataToTime(duration, currentTime);
	      $('.player-timer-display .time-front').text(time.normal);
	      return $('.player-timer-display .time-end').text(time.reverse);
	    };

	    controller.prototype._convertTrackDataToTime = function(duration, currentTime) {
	      var endDuration;
	      endDuration = duration - currentTime;
	      return {
	        normal: Math.floor(currentTime / 60) + ':' + (Math.floor(currentTime % 60) < 10 ? '0' : '') + Math.floor(currentTime % 60),
	        reverse: '- ' + Math.floor(endDuration / 60) + ':' + (Math.floor(endDuration % 60) < 10 ? '0' : '') + Math.floor(endDuration % 60)
	      };
	    };

	    controller.prototype.play = function(url) {
	      this.played = true;
	      if (url) {
	        this.my_jPlayer.jPlayer("setMedia", {
	          mp3: url
	        });
	      }
	      return this.my_jPlayer.jPlayer('play');
	    };

	    controller.prototype.seek = function(time) {
	      return this.my_jPlayer.jPlayer('play', time);
	    };

	    controller.prototype.stop = function() {
	      return this.my_jPlayer.jPlayer('stop');
	    };

	    controller.prototype.pause = function() {
	      this.played = false;
	      return this.my_jPlayer.jPlayer('pause');
	    };

	    controller.prototype.nextTrck = function() {
	      var track;
	      if (this.playlist) {
	        if (this.shuffle === 'on') {
	          track = this.playlist[this.playlist[this.playedTrack]['nextShuffleTrck']];
	        } else {
	          track = this.playlist[this.playlist[this.playedTrack]['nextTrck']];
	        }
	        if (track != null) {
	          return this.playTrack(track.id, track.toJSON());
	        }
	      }
	    };

	    controller.prototype.prevTrck = function() {
	      var track;
	      if (this.playlist) {
	        if (this.shuffle === 'on') {
	          track = this.playlist[this.playedTrack]['prevShuffleTrck'];
	        } else {
	          if (this.playlist[this.playedTrack]['prewTrck']) {
	            track = this.playlist[this.playlist[this.playedTrack]['prewTrck']];
	          } else {
	            track = this.playlist[this.playlist[this.playedTrack]['lastTrack']];
	          }
	        }
	        if (track != null) {
	          return this.playTrack(track.id, track.toJSON());
	        }
	      }
	    };

	    controller.prototype._setVolume = function(volume) {
	      var v;
	      v = +volume;
	      if (v > 1) {
	        v = 1;
	      }
	      sessionStorage.volume = v;
	      this.volume = v;
	      return this.my_jPlayer.jPlayer("volume", v);
	    };

	    controller.prototype.openOptions = function(item) {
	      var collection, layout, list, opts;
	      if (item.model.has('optionsList')) {
	        if (item.model.has('playlistTrack')) {
	          list = [
	            {
	              title: 'удалить из плейлиста',
	              "class": 'delete-from-playlist'
	            }
	          ];
	        } else {
	          if (item.model.get('inFavorite')) {
	            list = [
	              {
	                title: 'удалить из медиатеки',
	                "class": 'delete'
	              }
	            ];
	          } else {
	            list = [
	              {
	                title: 'добавить в медиатеку',
	                "class": 'add'
	              }
	            ];
	          }
	        }
	        _.each(item.model.get('optionsList'), function(item) {
	          return list.push(item);
	        });
	        collection = new Backbone.Collection(list);
	      } else {
	        collection = new Backbone.Collection;
	      }
	      if (item.model.get('artist_is_va')) {
	        opts = collection.findWhere({
	          artist: true
	        });
	        if (opts) {
	          opts.remove({
	            silent: true
	          });
	        }
	      }
	      layout = new Player.OptionsLayout({
	        model: item.model,
	        collection: collection
	      });
	      return item.optionsRegion.show(layout);
	    };

	    controller.prototype.openPlaylistsList = function(item) {
	      var playlist, view;
	      playlist = app.request('get:favorite:playlists');
	      if (!playlist.promise) {
	        view = new Player.PlaylistItems({
	          model: item.model,
	          collection: playlist
	        });
	        return item.optionsRegion.show(view);
	      }
	    };

	    controller.prototype.removeTrackFromFavorite = function(id) {
	      return app.request('get:favorite:tracks').done(function(favorites) {
	        var track;
	        track = favorites.findWhere({
	          id: id
	        });
	        if (track) {
	          track.destroy();
	          return Notification.Controller.show({
	            message: "Трек «" + (track.get('title')) + "» удален из избранного",
	            type: 'success',
	            time: 3000
	          });
	        }
	      });
	    };

	    controller.prototype.addTrackToFavorite = function(track) {
	      return app.request('get:favorite:tracks').done(function(favorites) {
	        var model;
	        model = track.toJSON();
	        if (!model.favourites || model.favourites.sort === void 0) {
	          model.favourites = {};
	          model.favourites.sort = 10000;
	        }
	        favorites.create(model);
	        app.GAPageAction('Добавление трека в избранное', track.get('title') + ' - ' + location.href);
	        return Notification.Controller.show({
	          message: "Трек «" + (track.get('title')) + "» добавлен в медиатеку",
	          type: 'success',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.createFavouritePlaylist = function(title, track) {
	      var playlist, playlistCreate;
	      playlist = app.request('get:favorite:playlists');
	      playlistCreate = playlist.create({
	        'title': title,
	        tracks: [track.model.toJSON()]
	      }, {
	        wait: true
	      });
	      $('.options-wrapper .back-to-options').hide();
	      $('.options-wrapper .options-playlist-scroll-wrapp').hide();
	      $('.options-wrapper').css('min-height', 300 + 'px').append('<div class="loader" style="top:130px"></div>');
	      return playlistCreate.once('sync', (function(_this) {
	        return function(playlist) {
	          return _this.addTrackToPlaylist(playlist.id, track.model);
	        };
	      })(this));
	    };

	    controller.prototype.addTrackToPlaylist = function(playlistId, track) {
	      var playlist, playlists, success;
	      playlistId = +playlistId;
	      playlists = app.request('get:favorite:playlists');
	      playlist = playlists.findWhere({
	        id: playlistId
	      });
	      window.test = playlist;
	      success = function(data) {
	        var collection, count, successWindow, trackData;
	        app.GAPageAction("Добавление трека в плейлист " + playlistId, track.get('artist') + ' - ' + track.get('title'));
	        successWindow = '<div class="options-overlay"></div><div class="success-icon"></div><p class="success-title">Готово</p>';
	        trackData = track.toJSON();
	        trackData.playlist = data.playlist;
	        if (playlist.has('tracks')) {
	          collection = _.union(playlist.get('tracks'), [trackData]);
	          count = collection.length;
	          playlist.set({
	            tracks: collection,
	            items_count: count
	          });
	        } else {
	          playlist.set({
	            tracks: [trackData],
	            items_count: 1
	          });
	        }
	        $('.options-wrapper').empty().append(successWindow);
	        return setTimeout(function() {
	          return $('.options-wrapper').fadeOut(300, function() {
	            return $(this).remove();
	          });
	        }, 1000);
	      };
	      if (playlist) {
	        $('.options-wrapper').css('min-height', 300 + 'px').empty().append('<div class="loader" style="top:130px"></div>');
	        return $.ajax({
	          type: "GET",
	          url: app.getRequestUrlSid("/playlist/additem", "?contentid=" + (track.get('id')) + "&playlistid=" + playlistId),
	          success: success
	        });
	      }
	    };

	    controller.prototype.createPlaylist = function(collection) {
	      var normal, shuffleId;
	      normal = collection.reject(function(item) {
	        return item.get('is_deleted');
	      });
	      shuffleId = _.shuffle(_.pluck(normal, 'id'));
	      _.each(normal, function(item, index) {
	        item.nextTrck = normal[index + 1] != null ? normal[index + 1]['id'] : null;
	        item.prewTrck = index !== 0 ? normal[index - 1]['id'] : null;
	        item.nextShuffleTrck = shuffleId[index] != null ? shuffleId[index] : normal[0].id;
	        item.prevShuffleTrck = shuffleId[index - 1] != null ? shuffleId[index - 1] : normal[normal.length - 1].id;
	        item.firstTrack = normal[0].id;
	        return item.lastTrack = normal[normal.length - 1].id;
	      });
	      return _.indexBy(normal, 'id');
	    };

	    controller.prototype.toggleShuffle = function() {
	      if (this.shuffle === 'on') {
	        this.shuffle = sessionStorage.shuffle = 'off';
	        return $('.main-player .other-buttons .shuffle').removeClass('active');
	      } else {
	        this.shuffle = sessionStorage.shuffle = 'on';
	        return $('.main-player .other-buttons .shuffle').addClass('active');
	      }
	    };

	    controller.prototype.setShuffle = function() {
	      if (this.shuffle === 'on') {
	        return $('.main-player .other-buttons .shuffle').addClass('active');
	      } else {
	        return $('.main-player .other-buttons .shuffle').removeClass('active');
	      }
	    };

	    SliderDrag = (function() {
	      function SliderDrag(element, type, context, active) {
	        this.element = element;
	        this.type = type;
	        this.context = context;
	        this.active = active;
	      }

	      SliderDrag.prototype.ready = function() {
	        return $(document).on('mousedown', this.element, (function(_this) {
	          return function(e) {
	            return _this.down(e);
	          };
	        })(this));
	      };

	      SliderDrag.prototype.down = function(e) {
	        var elToLeft, pageToLeft, widthVal;
	        elToLeft = $(this.element).offset().left;
	        pageToLeft = e.pageX;
	        widthVal = pageToLeft - elToLeft;
	        $(this.active).width(widthVal + 'px');
	        if (this.type === 'sound') {
	          this.context.dragIsActive = true;
	        }
	        $(document).on("mousemove.drager", (function(_this) {
	          return function(e) {
	            return _this.move(e);
	          };
	        })(this));
	        return $(document).on("mouseup.drager", (function(_this) {
	          return function(e) {
	            var parcentWidth, soundSeek;
	            elToLeft = $(_this.element).offset().left;
	            pageToLeft = e.pageX;
	            widthVal = pageToLeft - elToLeft;
	            if (_this.type === 'sound') {
	              _this.context.dragIsActive = false;
	              parcentWidth = (widthVal * 100) / $(_this.element).width();
	              soundSeek = (_this.context.trackDuration * parcentWidth) / 100;
	              _this.context.seek(soundSeek);
	            }
	            if (_this.type === 'volume') {
	              _this.context._setVolume(widthVal / $(_this.element).width());
	            }
	            return $(document).off(".drager");
	          };
	        })(this));
	      };

	      SliderDrag.prototype.move = function(e) {
	        var elToLeft, pageToLeft, parentWidth, widthVal;
	        e.preventDefault();
	        elToLeft = $(this.element).offset().left;
	        pageToLeft = e.pageX;
	        widthVal = pageToLeft - elToLeft;
	        parentWidth = $(this.element).width();
	        if (widthVal <= 0) {
	          widthVal = 0;
	        }
	        if (parentWidth <= widthVal) {
	          return false;
	        }
	        $(this.active).width(widthVal + 'px');
	        if (this.type === 'volume') {
	          this.context._setVolume(widthVal / $(this.element).width());
	        }
	        if (this.type === 'sound') {
	          this.soundMove(e);
	        }
	        return false;
	      };

	      SliderDrag.prototype.soundMove = function(e) {
	        var elToLeft, pageToLeft, parcentWidth, parentWidth, soundSeek, time, widthVal;
	        e.preventDefault();
	        elToLeft = $(this.element).offset().left;
	        pageToLeft = e.pageX;
	        widthVal = pageToLeft - elToLeft;
	        parentWidth = $(this.element).width();
	        if (widthVal <= 0) {
	          widthVal = 0;
	        }
	        if (parentWidth <= widthVal) {
	          return false;
	        }
	        parcentWidth = (widthVal * 100) / parentWidth;
	        soundSeek = (this.context.trackDuration * parcentWidth) / 100;
	        if (this.context.trackDuration != null) {
	          time = this.context._convertTrackDataToTime(widthVal, soundSeek);
	        } else {
	          time = {
	            normal: '--:--',
	            reverse: '--:--'
	          };
	        }
	        $('.player-timer-display .time-front').text(time.normal);
	        return $('.player-timer-display .time-end').text(time.reverse);
	      };

	      return SliderDrag;

	    })();

	    return controller;

	  })(Marionette.Controller);
	  Player.Controller = new controller;
	  return Player;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Player;
	  Player = {};
	  Player.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = function(data) {
	      var template;
	      data.img = app.createImgLink(data.img, '50x50');
	      template = __webpack_require__(3);
	      return template(data);
	    };

	    View.prototype.className = 'player-container';

	    View.prototype.onShow = function() {
	      return this.$('.player-wrapp').fadeIn(300);
	    };

	    View.prototype.onRender = function() {
	      var options;
	      options = new Marionette.Region({
	        el: this.$('.options-region')
	      });
	      return this.optionsRegion = options;
	    };

	    View.prototype.modelEvents = function() {
	      return {
	        'change': function() {
	          this.render();
	          Player.Controller.setDefaultVolume();
	          return this.$('.player-wrapp').show();
	        }
	      };
	    };

	    View.prototype.events = {
	      'click .player-timer-display': function() {
	        this.$('.time-front').toggle();
	        return this.$('.time-end').toggle();
	      },
	      'click .fwd-arrow': function() {
	        return Player.Controller.nextTrck();
	      },
	      'click .back-arrow': function() {
	        return Player.Controller.prevTrck();
	      },
	      'click .play': function() {
	        return Player.Controller.play();
	      },
	      'click .pause': function() {
	        return Player.Controller.pause();
	      },
	      'click .shuffle': function() {
	        return Player.Controller.toggleShuffle();
	      },
	      'click .options-region': function() {
	        return Player.Controller.openOptions(this);
	      },
	      'click .artistName': function() {
	        return app.trigger('show:artist', this.model.get('artistid'));
	      },
	      'click .soundName': function() {
	        if (!this.model.get('artist_is_va')) {
	          return app.trigger('show:album', this.model.get('release_id'));
	        }
	      },
	      'click .track-controll.add': function() {
	        Player.Controller.addTrackToFavorite(this.model);
	        this.model.set('inFavorite', true, {
	          silent: true
	        });
	        this.$('.track-controll').removeClass('add').addClass('delete');
	        return false;
	      },
	      'click .track-controll.delete': function() {
	        Player.Controller.removeTrackFromFavorite(this.model.get('id'));
	        this.model.set('inFavorite', false, {
	          silent: true
	        });
	        this.$('.track-controll').removeClass('delete').addClass('add');
	        return false;
	      },
	      'click .options-wrapper': function() {
	        return false;
	      },
	      'click .add-to-playlist': function() {
	        Player.Controller.openPlaylistsList(this);
	        return false;
	      },
	      'click .back-to-options': function() {
	        Player.Controller.openOptions(this);
	        return false;
	      },
	      'click .options-playlist-item': function(e) {
	        return Player.Controller.addTrackToPlaylist($(e.target).attr('data-id'), this.model);
	      },
	      'click .options-open-album': function() {
	        app.trigger('show:album', this.model.get('release_id'));
	        this.closeOptions();
	        return false;
	      },
	      'click .options-open-artist': function() {
	        app.trigger('show:artist', this.model.get('artistid'));
	        this.closeOptions();
	        return false;
	      },
	      'keypress .new-playlist input': function(e) {
	        if (e.keyCode === 13) {
	          return Player.Controller.createFavouritePlaylist($('.new-playlist input').val(), this);
	        }
	      },
	      'mouseleave': 'closeOptions',
	      'click .options-overlay': 'closeOptions'
	    };

	    View.prototype.closeOptions = function() {
	      return $('.options-wrapper').remove();
	    };

	    return View;

	  })(Marionette.ItemView);
	  Player.OptionsItem = (function(superClass) {
	    extend(OptionsItem, superClass);

	    function OptionsItem() {
	      return OptionsItem.__super__.constructor.apply(this, arguments);
	    }

	    OptionsItem.prototype.template = '#track-options-list-item-tpl';

	    OptionsItem.prototype.className = 'track-options-list-item';

	    OptionsItem.prototype.tagName = 'li';

	    OptionsItem.prototype.onRender = function() {
	      return this.$el.addClass(this.model.get('class'));
	    };

	    return OptionsItem;

	  })(Marionette.ItemView);
	  Player.OptionsLayout = (function(superClass) {
	    extend(OptionsLayout, superClass);

	    function OptionsLayout() {
	      return OptionsLayout.__super__.constructor.apply(this, arguments);
	    }

	    OptionsLayout.prototype.template = '#options-list-tpl';

	    OptionsLayout.prototype.className = 'options-wrapper';

	    OptionsLayout.prototype.childView = Player.OptionsItem;

	    OptionsLayout.prototype.childViewContainer = '.options-list';

	    OptionsLayout.prototype.onShow = function() {
	      return this.$('.textarea').select();
	    };

	    OptionsLayout.prototype.events = {
	      'click': function() {
	        return this.$('.textarea').select();
	      },
	      'click .delete': function(e) {
	        Player.Controller.removeTrackFromFavorite(this.model.get('id'));
	        this.model.set('inFavorite', false, {
	          silent: true
	        });
	        $(e.target).closest('.options').find('.track-controll').removeClass('delete').addClass('add');
	        this.$el.find('.delete').removeClass('delete').addClass('add').text('Добавить в медиатеку');
	        app.trigger('remove:from:favorite');
	        return false;
	      },
	      'click .add': function() {
	        Player.Controller.addTrackToFavorite(this.model);
	        this.model.set('inFavorite', true, {
	          silent: true
	        });
	        this.$el.closest('.options').find('.track-controll').removeClass('add').addClass('delete');
	        this.$el.find('.add').removeClass('add').addClass('delete').text('Удалить из медиатеки');
	        app.trigger('add:to:favorite');
	        return false;
	      },
	      'mouseleave': 'closeOptions',
	      'click .options-overlay': 'closeOptions'
	    };

	    OptionsLayout.prototype.closeOptions = function(e) {
	      e.stopPropagation();
	      return $('.options-wrapper').remove();
	    };

	    return OptionsLayout;

	  })(Marionette.CompositeView);
	  Player.PlaylistItem = (function(superClass) {
	    extend(PlaylistItem, superClass);

	    function PlaylistItem() {
	      return PlaylistItem.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItem.prototype.template = '#options-playlist-item-tpl';

	    PlaylistItem.prototype.className = 'options-playlist-item';

	    PlaylistItem.prototype.tagname = 'li';

	    PlaylistItem.prototype.onRender = function() {
	      return this.$el.attr('data-id', this.model.get('id'));
	    };

	    return PlaylistItem;

	  })(Marionette.ItemView);
	  Player.PlaylistItems = (function(superClass) {
	    extend(PlaylistItems, superClass);

	    function PlaylistItems() {
	      return PlaylistItems.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItems.prototype.template = '#options-playlist-window-tpl';

	    PlaylistItems.prototype.childView = Player.PlaylistItem;

	    PlaylistItems.prototype.childViewContainer = '.container';

	    PlaylistItems.prototype.className = 'options-wrapper';

	    PlaylistItems.prototype.events = {
	      'click .new-playlist input': function() {
	        return $('.new-playlist input').select();
	      },
	      'blur .new-playlist input': function() {
	        return $('.new-playlist input').val('+ Новый плейлист');
	      }
	    };

	    PlaylistItems.prototype.onShow = function() {
	      return $('.options-wrapper .options-playlist-scroll-wrapp').customScrollbar();
	    };

	    return PlaylistItems;

	  })(Marionette.CompositeView);
	  return Player;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (artist, duration, img, inFavorite, title) {
	buf.push("<div class=\"player-wrapp\"><div class=\"controll-bar\"><div class=\"back-arrow\"></div><div class=\"play player-controll-element\"></div><div class=\"fwd-arrow\"></div></div><div class=\"main-player\"><div class=\"player-view\"><div class=\"artist-logo\"><img" + (jade.attr("src", "" + (img) + "", true, true)) + "></div><div class=\"track-info\"><div class=\"track-name\"><span class=\"artistName\">" + (jade.escape((jade_interp = artist) == null ? '' : jade_interp)) + "</span>—<span class=\"soundName\">" + (jade.escape((jade_interp = title) == null ? '' : jade_interp)) + "</span><div class=\"player-timer-display\"><span class=\"time-front\">00:00</span><span class=\"time-end hidden\">" + (jade.escape((jade_interp = duration) == null ? '' : jade_interp)) + "</span></div></div><div class=\"time-line\"><div class=\"active-zone\"></div><div class=\"active-line drag-active\"><div class=\"drag-ball drag\"></div></div></div></div><div class=\"other-buttons\"><div class=\"options\"><div class=\"options-region\"></div>");
	if ( inFavorite)
	{
	buf.push("<div class=\"track-controll delete\"></div>");
	}
	else
	{
	}
	buf.push("<div class=\"track-controll add\"></div></div><div class=\"volume\"><span><div class=\"line\"><div class=\"active\"></div><div class=\"value\"></div></div></span><div class=\"shuffle\"></div><div class=\"loop\"></div></div></div></div></div></div>");}.call(this,"artist" in locals_for_with?locals_for_with.artist:typeof artist!=="undefined"?artist:undefined,"duration" in locals_for_with?locals_for_with.duration:typeof duration!=="undefined"?duration:undefined,"img" in locals_for_with?locals_for_with.img:typeof img!=="undefined"?img:undefined,"inFavorite" in locals_for_with?locals_for_with.inFavorite:typeof inFavorite!=="undefined"?inFavorite:undefined,"title" in locals_for_with?locals_for_with.title:typeof title!=="undefined"?title:undefined));;return buf.join("");
	}

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	/**
	 * Merge two attribute objects giving precedence
	 * to values in object `b`. Classes are special-cased
	 * allowing for arrays and merging/joining appropriately
	 * resulting in a string.
	 *
	 * @param {Object} a
	 * @param {Object} b
	 * @return {Object} a
	 * @api private
	 */

	exports.merge = function merge(a, b) {
	  if (arguments.length === 1) {
	    var attrs = a[0];
	    for (var i = 1; i < a.length; i++) {
	      attrs = merge(attrs, a[i]);
	    }
	    return attrs;
	  }
	  var ac = a['class'];
	  var bc = b['class'];

	  if (ac || bc) {
	    ac = ac || [];
	    bc = bc || [];
	    if (!Array.isArray(ac)) ac = [ac];
	    if (!Array.isArray(bc)) bc = [bc];
	    a['class'] = ac.concat(bc).filter(nulls);
	  }

	  for (var key in b) {
	    if (key != 'class') {
	      a[key] = b[key];
	    }
	  }

	  return a;
	};

	/**
	 * Filter null `val`s.
	 *
	 * @param {*} val
	 * @return {Boolean}
	 * @api private
	 */

	function nulls(val) {
	  return val != null && val !== '';
	}

	/**
	 * join array as classes.
	 *
	 * @param {*} val
	 * @return {String}
	 */
	exports.joinClasses = joinClasses;
	function joinClasses(val) {
	  return (Array.isArray(val) ? val.map(joinClasses) :
	    (val && typeof val === 'object') ? Object.keys(val).filter(function (key) { return val[key]; }) :
	    [val]).filter(nulls).join(' ');
	}

	/**
	 * Render the given classes.
	 *
	 * @param {Array} classes
	 * @param {Array.<Boolean>} escaped
	 * @return {String}
	 */
	exports.cls = function cls(classes, escaped) {
	  var buf = [];
	  for (var i = 0; i < classes.length; i++) {
	    if (escaped && escaped[i]) {
	      buf.push(exports.escape(joinClasses([classes[i]])));
	    } else {
	      buf.push(joinClasses(classes[i]));
	    }
	  }
	  var text = joinClasses(buf);
	  if (text.length) {
	    return ' class="' + text + '"';
	  } else {
	    return '';
	  }
	};


	exports.style = function (val) {
	  if (val && typeof val === 'object') {
	    return Object.keys(val).map(function (style) {
	      return style + ':' + val[style];
	    }).join(';');
	  } else {
	    return val;
	  }
	};
	/**
	 * Render the given attribute.
	 *
	 * @param {String} key
	 * @param {String} val
	 * @param {Boolean} escaped
	 * @param {Boolean} terse
	 * @return {String}
	 */
	exports.attr = function attr(key, val, escaped, terse) {
	  if (key === 'style') {
	    val = exports.style(val);
	  }
	  if ('boolean' == typeof val || null == val) {
	    if (val) {
	      return ' ' + (terse ? key : key + '="' + key + '"');
	    } else {
	      return '';
	    }
	  } else if (0 == key.indexOf('data') && 'string' != typeof val) {
	    if (JSON.stringify(val).indexOf('&') !== -1) {
	      console.warn('Since Jade 2.0.0, ampersands (`&`) in data attributes ' +
	                   'will be escaped to `&amp;`');
	    };
	    if (val && typeof val.toISOString === 'function') {
	      console.warn('Jade will eliminate the double quotes around dates in ' +
	                   'ISO form after 2.0.0');
	    }
	    return ' ' + key + "='" + JSON.stringify(val).replace(/'/g, '&apos;') + "'";
	  } else if (escaped) {
	    if (val && typeof val.toISOString === 'function') {
	      console.warn('Jade will stringify dates in ISO form after 2.0.0');
	    }
	    return ' ' + key + '="' + exports.escape(val) + '"';
	  } else {
	    if (val && typeof val.toISOString === 'function') {
	      console.warn('Jade will stringify dates in ISO form after 2.0.0');
	    }
	    return ' ' + key + '="' + val + '"';
	  }
	};

	/**
	 * Render the given attributes object.
	 *
	 * @param {Object} obj
	 * @param {Object} escaped
	 * @return {String}
	 */
	exports.attrs = function attrs(obj, terse){
	  var buf = [];

	  var keys = Object.keys(obj);

	  if (keys.length) {
	    for (var i = 0; i < keys.length; ++i) {
	      var key = keys[i]
	        , val = obj[key];

	      if ('class' == key) {
	        if (val = joinClasses(val)) {
	          buf.push(' ' + key + '="' + val + '"');
	        }
	      } else {
	        buf.push(exports.attr(key, val, false, terse));
	      }
	    }
	  }

	  return buf.join('');
	};

	/**
	 * Escape the given string of `html`.
	 *
	 * @param {String} html
	 * @return {String}
	 * @api private
	 */

	var jade_encode_html_rules = {
	  '&': '&amp;',
	  '<': '&lt;',
	  '>': '&gt;',
	  '"': '&quot;'
	};
	var jade_match_html = /[&<>"]/g;

	function jade_encode_char(c) {
	  return jade_encode_html_rules[c] || c;
	}

	exports.escape = jade_escape;
	function jade_escape(html){
	  var result = String(html).replace(jade_match_html, jade_encode_char);
	  if (result === '' + html) return html;
	  else return result;
	};

	/**
	 * Re-throw the given `err` in context to the
	 * the jade in `filename` at the given `lineno`.
	 *
	 * @param {Error} err
	 * @param {String} filename
	 * @param {String} lineno
	 * @api private
	 */

	exports.rethrow = function rethrow(err, filename, lineno, str){
	  if (!(err instanceof Error)) throw err;
	  if ((typeof window != 'undefined' || !filename) && !str) {
	    err.message += ' on line ' + lineno;
	    throw err;
	  }
	  try {
	    str = str || __webpack_require__(5).readFileSync(filename, 'utf8')
	  } catch (ex) {
	    rethrow(err, null, lineno)
	  }
	  var context = 3
	    , lines = str.split('\n')
	    , start = Math.max(lineno - context, 0)
	    , end = Math.min(lines.length, lineno + context);

	  // Error context
	  var context = lines.slice(start, end).map(function(line, i){
	    var curr = i + start + 1;
	    return (curr == lineno ? '  > ' : '    ')
	      + curr
	      + '| '
	      + line;
	  }).join('\n');

	  // Alter exception message
	  err.path = filename;
	  err.message = (filename || 'Jade') + ':' + lineno
	    + '\n' + context + '\n\n' + err.message;
	  throw err;
	};

	exports.DebugItem = function DebugItem(lineno, filename) {
	  this.lineno = lineno;
	  this.filename = filename;
	}


/***/ },
/* 5 */
/***/ function(module, exports) {

	/* (ignored) */

/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var controller;
	  __webpack_require__(7);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.initialize = function() {
	      return this.errCount = 0;
	    };

	    controller.prototype.sendNewLog = function() {
	      var log, logs, offset, req, self;
	      console.debug('.....................');
	      console.debug('Start new log request');
	      self = this;
	      logs = app.request('get:logs');
	      if (logs.length) {
	        log = logs.at(0);
	        offset = (new Date().getTime() - log.get('create')) / 1000;
	        if (offset > 10800) {
	          console.debug('Destroy old log', log.toJSON());
	          log.destroy();
	          self.sendNewLog();
	          return;
	        }
	        req = $.get(log.get('url'), {
	          timeOffset: offset
	        });
	        console.debug('Log request send! Url: ' + log.get('url'));
	        req.done(function(res) {
	          if (res.code === 0) {
	            console.debug('Log request send success');
	            self.errCount = 0;
	            log.destroy();
	            return self.sendNewLog();
	          } else {
	            console.debug('Log request send error', res);
	            self.errCount++;
	            return self.sendErr();
	          }
	        });
	        return req.fail(function(err) {
	          console.debug('Log request send error', err);
	          self.errCount++;
	          return self.sendErr();
	        });
	      } else {
	        console.debug('No log to send! Wait for new logs');
	        return this.listenTo(logs, 'add', function() {
	          console.debug('New log create! Stop listening log storage');
	          this.stopListening();
	          return self.sendNewLog();
	        });
	      }
	    };

	    controller.prototype.sendErr = function() {
	      var delay, ref, self;
	      console.debug('Log request error! Count: ' + this.errCount);
	      self = this;
	      if (this.errCount === 1) {
	        delay = 1;
	      } else if ((10 > (ref = this.errCount) && ref > 1)) {
	        delay = 10000;
	      } else if (this.errCount > 10) {
	        delay = 300000;
	      }
	      return setTimeout(function() {
	        return self.sendNewLog();
	      }, delay);
	    };

	    controller.prototype.createNewLog = function(url) {
	      var logs, newLog;
	      logs = app.request('get:logs');
	      newLog = logs.create({
	        url: url,
	        create: new Date().getTime(),
	        uniq: app.RandInt(100, 200000000)
	      });
	      if (newLog.validationError) {
	        console.debug(newLog.validationError);
	        return logs.remove(newLog);
	      } else {
	        return console.debug.apply(console, ['Log create', newLog.get('id')]);
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  return new controller;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Log, initializeLog;
	  Log = {};
	  Log.Model = (function(superClass) {
	    extend(Model, superClass);

	    function Model() {
	      return Model.__super__.constructor.apply(this, arguments);
	    }

	    Model.prototype.validate = function(attrs, options) {
	      if (attrs.url == null) {
	        return "A 'updateUrl' property or function must be specified";
	      }
	    };

	    Model.prototype.url = function() {
	      var base, id;
	      base = _.result(this, 'urlRoot') || _.result(this.collection, 'url') || urlError();
	      if (this.isNew()) {
	        return base;
	      }
	      id = this.get(this.idAttribute);
	      return base.replace(/[^\/]$/, '$&/') + encodeURIComponent(id);
	    };

	    Model.prototype.sync = function(method, model, options) {
	      return Backbone.sync.apply(this, [method, model, options]);
	    };

	    return Model;

	  })(Backbone.Model);
	  Log.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Log.Model;

	    Collection.prototype.localStorage = new Backbone.LocalStorage("Logs");

	    Collection.prototype.save = function() {
	      return this.each(function(model) {
	        return Backbone.localSync("update", model);
	      });
	    };

	    return Collection;

	  })(Backbone.Collection);
	  initializeLog = function() {
	    Log.List = new Log.Collection;
	    return Log.List.fetch();
	  };
	  API = {
	    getLogs: function() {
	      if (Log.List === void 0) {
	        initializeLog();
	      }
	      return Log.List;
	    }
	  };
	  return app.reqres.setHandler('get:logs', function() {
	    return API.getLogs();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(9)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Notification) {
	  var controller;
	  __webpack_require__(11);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.show = function(opts) {

	      /*
	      
	        			message: сообщение которое хотим написать
	        			type: тип ошибки {error: красное поле, success: зеленое поле} по умолчанию желтое поле
	        			closed (bool): дает возможность закрыть сообщение (по умолчанию true)
	        			time: время в милисикундах через которое плашка закроется
	       */
	      var model, time;
	      if (!opts.closed) {
	        opts.closed = true;
	      }
	      model = new Backbone.Model(opts);
	      this.notifiView = new Notification.View({
	        model: model
	      });
	      time = opts.time ? opts.time : 3000;
	      if (Notification.Timeout) {
	        clearTimeout(Notification.Timeout);
	      }
	      Notification.Timeout = setTimeout((function(_this) {
	        return function() {
	          return _this.close();
	        };
	      })(this), time);
	      return app.notification.show(this.notifiView);
	    };

	    controller.prototype.close = function() {
	      if (this.notifiView) {
	        this.notifiView.destroy();
	      }
	      if (this.subscribeNotifi) {
	        return this.showSubscribe();
	      }
	    };

	    controller.prototype.showSubscribe = function() {
	      var model;
	      this.subscribeNotifi = true;
	      model = new Backbone.Model({
	        "class": 'error',
	        message: app.locale.notification.subsM,
	        closed: false
	      });
	      this.notifiView = new Notification.View({
	        model: model
	      });
	      app.notification.show(this.notifiView);
	      return $(window).resize();
	    };

	    controller.prototype.closeSubscribe = function() {
	      this.subscribeNotifi = false;
	      return this.close();
	    };

	    return controller;

	  })(Marionette.Controller);
	  Notification.Controller = new controller;
	  return Notification;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Notification;
	  Notification = {};
	  Notification.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = function(sData) {
	      var html;
	      html = __webpack_require__(10);
	      return html(sData);
	    };

	    View.prototype.className = function() {
	      if (this.model.has('type')) {
	        return this.model.get('type');
	      } else {
	        return 'default';
	      }
	    };

	    View.prototype.events = {
	      'click .close-icon': function() {
	        return Notification.Controller.close();
	      },
	      'click .to-subscribe': function() {
	        return app.trigger('show:profile');
	      }
	    };

	    View.prototype.onShow = function() {
	      return $('body').addClass('notifi');
	    };

	    View.prototype.onDestroy = function() {
	      return $('body').removeClass('notifi');
	    };

	    return View;

	  })(Marionette.ItemView);
	  return Notification;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (closed, message) {
	buf.push("" + (((jade_interp = message) == null ? '' : jade_interp)) + "");
	if ( closed)
	{
	buf.push("<div class=\"close-icon\"></div>");
	}}.call(this,"closed" in locals_for_with?locals_for_with.closed:typeof closed!=="undefined"?closed:undefined,"message" in locals_for_with?locals_for_with.message:typeof message!=="undefined"?message:undefined));;return buf.join("");
	}

/***/ },
/* 11 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 12 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 13 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 14 */
/***/ function(module, exports) {

	_.extend(Backbone.Model.prototype, {
	  url: function() {
	    var base, id;
	    base = _.result(this, 'urlRoot') || _.result(this.collection, 'url') || urlError();
	    id = this.get(this.idAttribute);
	    return base;
	  },
	  sync: function(method, model, options) {
	    if (method === 'update') {
	      if (model.updateUrl) {
	        model.url = model.updateUrl;
	      } else {
	        throw new Error("A 'updateUrl' property or function must be specified");
	      }
	    }
	    if (method === 'delete') {
	      if (model.deleteUrl) {
	        model.url = model.deleteUrl;
	      } else {
	        throw new Error("A 'deleteUrl' property or function must be specified");
	      }
	    }
	    if (method === 'create') {
	      if (model.createUrl) {
	        model.url = model.createUrl;
	      } else {
	        throw new Error("A 'createUrl' property or function must be specified");
	      }
	    }
	    method = 'read';
	    return Backbone.sync.apply(this, [method, model, options]);
	  }
	});
	

/***/ },
/* 15 */
/***/ function(module, exports) {

	var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	Marionette.AppRouter = (function(superClass) {
	  extend(AppRouter, superClass);

	  function AppRouter() {
	    return AppRouter.__super__.constructor.apply(this, arguments);
	  }

	  AppRouter.prototype.initialize = function() {
	    var BaseUrl, key, newRoutes, routes, val;
	    routes = this.appRoutes;
	    newRoutes = {};
	    BaseUrl = '(:lang)';
	    for (key in routes) {
	      val = routes[key];
	      if (key === '') {
	        newRoutes[BaseUrl] = val;
	        newRoutes[BaseUrl + '/'] = val;
	      } else {
	        newRoutes[BaseUrl + '/' + key] = val;
	        newRoutes[BaseUrl + '/' + key + '/'] = val;
	      }
	    }
	    return this.appRoutes = newRoutes;
	  };

	  return AppRouter;

	})(Marionette.AppRouter);
	

/***/ },
/* 16 */
/***/ function(module, exports) {

	(function() {
	  var sync;
	  sync = Backbone.sync;
	  return Backbone.sync = function(method, model, options) {
	    var data;
	    data = options.data || {};
	    data.sid = app.user.sid;
	    _.extend(options, {
	      data: data
	    });
	    return sync.apply(this, [method, model, options]);
	  };
	})();
	

/***/ },
/* 17 */
/***/ function(module, exports) {

	var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	app.module("Helpers.Filters", function(Filters, app, Backbone, Marionette, $, _) {
	  var collection;
	  collection = (function(superClass) {
	    extend(collection, superClass);

	    function collection() {
	      return collection.__super__.constructor.apply(this, arguments);
	    }

	    collection.prototype.Filter = function(options) {
	      var applyFilter, filtered, original;
	      original = options.collection;
	      filtered = new original.constructor();
	      filtered.add(original.models);
	      filtered.filterFunction = options.filterFunction;
	      applyFilter = function(filterCriterion, filterStrategy, collection) {
	        var criterion, filterFunction, items;
	        collection = collection || original;
	        criterion;
	        if (filterStrategy === "filter") {
	          criterion = filterCriterion.trim();
	        } else {
	          criterion = filterCriterion;
	        }
	        items = [];
	        if (criterion) {
	          if (filterStrategy === "filter") {
	            if (!filtered.filterFunction) {
	              throw "Attempted to use 'filter' function, but none was defined";
	            }
	            filterFunction = filtered.filterFunction(criterion);
	            items = collection.filter(filterFunction);
	          } else {
	            items = collection.where(criterion);
	          }
	        } else {
	          items = collection.models;
	        }
	        filtered._currentCriterion = criterion;
	        return items;
	      };
	      filtered.filter = function(filterCriterion) {
	        var items;
	        filtered._currentFilter = "filter";
	        items = applyFilter(filterCriterion, "filter");
	        filtered.reset(items);
	        return filtered;
	      };
	      filtered.where = function(filterCriterion) {
	        var items;
	        filtered._currentFilter = "where";
	        items = applyFilter(filterCriterion, "where");
	        filtered.reset(items);
	        return filtered;
	      };
	      filtered.stopListenCollection = function() {
	        original.off('add');
	        return original.off('reset');
	      };
	      original.on("reset", function() {
	        var items;
	        items = applyFilter(filtered._currentCriterion, filtered._currentFilter);
	        return filtered.reset(items);
	      });
	      original.on("add", function(models) {
	        var coll, items;
	        coll = new original.constructor();
	        coll.add(models);
	        items = applyFilter(filtered._currentCriterion, filtered._currentFilter, coll);
	        return filtered.add(items);
	      });
	      return filtered;
	    };

	    return collection;

	  })(Marionette.Controller);
	  return Filters.Collection = new collection;
	});
	

/***/ },
/* 18 */
/***/ function(module, exports) {

	Backbone.Model.Selected = Backbone.Model.extend({
	  select: function() {
	    if (this.selected) {
	      return;
	    }
	    this.selected = true;
	    this.trigger("selected", this);
	    if (this.collection) {
	      return this.collection.select(this);
	    }
	  },
	  deselect: function() {
	    if (!this.selected) {
	      return;
	    }
	    this.selected = false;
	    this.trigger("deselected", this);
	    if (this.collection) {
	      return this.collection.deselect(this);
	    }
	  },
	  toggleSelected: function() {
	    if (this.selected) {
	      return this.deselect();
	    } else {
	      return this.select();
	    }
	  }
	});

	Backbone.Collection.SingleSelect = Backbone.Collection.extend({
	  select: function(model) {
	    if (model && this.selected === model) {
	      return;
	    }
	    this.deselect();
	    this.selected = model;
	    this.selected.select();
	    return this.trigger("select:one", model);
	  },
	  deselect: function(model) {
	    if (!this.selected) {
	      return;
	    }
	    model = model || this.selected;
	    if (this.selected !== model) {
	      return;
	    }
	    this.selected.deselect();
	    this.trigger("deselect:one", this.selected);
	    return delete this.selected;
	  }
	});

	Backbone.Collection.MultiSelected = Backbone.Collection.extend({
	  initialize: function() {
	    return this.selected = {};
	  },
	  select: function(model) {
	    if (this.selected[model.cid]) {
	      return;
	    }
	    this.selected[model.cid] = model;
	    model.select();
	    return this.calculateSelectedLength(this);
	  },
	  deselect: function(model) {
	    if (!this.selected[model.cid]) {
	      return;
	    }
	    delete this.selected[model.cid];
	    model.deselect();
	    return this.calculateSelectedLength(this);
	  },
	  selectAll: function() {
	    this.each(function(model) {
	      return model.select();
	    });
	    return this.calculateSelectedLength(this);
	  },
	  selectNone: function() {
	    if (this.selectedLength === 0) {
	      return;
	    }
	    this.each(function(model) {
	      return model.deselect();
	    });
	    return this.calculateSelectedLength(this);
	  },
	  toggleSelectAll: function() {
	    if (this.selectedLength === this.length) {
	      return this.selectNone();
	    } else {
	      return this.selectAll();
	    }
	  },
	  calculateSelectedLength: function(collection) {
	    var length, selectedLength;
	    collection.selectedLength = _.size(collection.selected);
	    selectedLength = collection.selectedLength;
	    length = collection.length;
	    if (selectedLength === length) {
	      collection.trigger("select:all", collection);
	      return;
	    }
	    if (selectedLength === 0) {
	      collection.trigger("select:none", collection);
	      return;
	    }
	    if (selectedLength > 0 && selectedLength < length) {
	      collection.trigger("select:some", collection);
	    }
	  }
	});
	

/***/ },
/* 19 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(6), __webpack_require__(1), __webpack_require__(20)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Logger, Player, Sidebar) {
	  var API, Router, api;
	  Router = (function(superClass) {
	    extend(Router, superClass);

	    function Router() {
	      return Router.__super__.constructor.apply(this, arguments);
	    }

	    Router.prototype.appRoutes = {
	      'track/:id': 'playedTrack',
	      '': 'showRecomend',
	      'new': 'showNew',
	      'feed': 'showFeed',
	      'recomend': 'showRecomend',
	      'playlists': 'showPlaylists',
	      'playlist/:id': 'showPlaylist',
	      'id:id': 'showUserPage',
	      'video': 'showVideo',
	      'profile(/:tab)': 'showProfile',
	      'user/tracks': 'showUserTracks',
	      'user/albums': 'showUserAlbums',
	      'album/:id': 'showAlbum',
	      'artist/:id': 'showArtist',
	      'search/tracks': 'searchTracks',
	      'search/albums': 'searchAlbums',
	      'search/artists': 'searchArtist'
	    };

	    return Router;

	  })(Marionette.AppRouter);
	  API = (function(superClass) {
	    extend(API, superClass);

	    function API() {
	      return API.__super__.constructor.apply(this, arguments);
	    }

	    API.prototype.showRecomend = function() {
	      return __webpack_require__.e/* nsure */(1, function(require) {
	        var Recomend;
	        Recomend = __webpack_require__(43);
	        $('#header h1').text(app.locale.menu.popular);
	        Recomend.Controller.showPage();
	        app.trigger('switch:page', 'recomend');
	        app.GAPageAction('page-open', 'Главная');
	        return Logger.createNewLog(app.getRequestUrlSid('/generalV1/log/openSection', '?section=main'));
	      });
	    };

	    API.prototype.showFeed = function() {
	      return __webpack_require__.e/* nsure */(2, function(require) {
	        var Feed;
	        Feed = __webpack_require__(50);
	        $('#header h1').text(app.locale.menu.feed);
	        Feed.Controller.showPage();
	        app.trigger('switch:page', 'feed');
	        app.GAPageAction('page-open', 'Лента');
	        return Logger.createNewLog(app.getRequestUrlSid('/generalV1/log/openSection', '?section=wall'));
	      });
	    };

	    API.prototype.showUserPage = function(lang, id) {
	      return __webpack_require__.e/* nsure */(3, function(require) {
	        var UserPage;
	        UserPage = __webpack_require__(57);
	        $('#header h1').text('');
	        UserPage.Controller.showPage(id);
	        app.trigger('switch:page', 'user');
	        return app.GAPageAction('page-open', 'Пользователь-' + id);
	      });
	    };

	    API.prototype.showNew = function() {
	      return __webpack_require__.e/* nsure */(4, function(require) {
	        var New;
	        New = __webpack_require__(60);
	        $('#header h1').text(app.locale.menu["new"]);
	        New.Controller.showPage();
	        app.trigger('switch:page', 'new');
	        app.GAPageAction('page-open', 'Новинки');
	        return Logger.createNewLog(app.getRequestUrlSid('/generalV1/log/openSection', '?section=new'));
	      });
	    };

	    API.prototype.showPlaylists = function() {
	      return __webpack_require__.e/* nsure */(5, function(require) {
	        var Playlists;
	        Playlists = __webpack_require__(66);
	        $('#header h1').text(app.locale.menu.playlists);
	        Playlists.Controller.showPage();
	        app.trigger('switch:page', 'playlists');
	        return app.GAPageAction('page-open', 'Плейлисты');
	      });
	    };

	    API.prototype.showVideo = function() {
	      return __webpack_require__.e/* nsure */(6, function(require) {
	        var Video;
	        Video = __webpack_require__(69);
	        $('#header h1').text(app.locale.menu.clips);
	        Video.Controller.showPage();
	        app.trigger('switch:page', 'video');
	        return app.GAPageAction('page-open', 'Клипы');
	      });
	    };

	    API.prototype.showProfile = function(leng, tab) {
	      if (tab == null) {
	        tab = "main";
	      }
	      return __webpack_require__.e/* nsure */(7, function(require) {
	        var Profile;
	        Profile = __webpack_require__(72);
	        $('#header h1').text(app.locale.menu.profile);
	        Profile.Controller.showPage(tab);
	        app.trigger('switch:page', 'profile');
	        return app.GAPageAction('page-open', 'Профиль');
	      });
	    };

	    API.prototype.playedTrack = function(leng, id) {
	      app.GAPageAction('page-open', 'Шаринг трека - ' + id);
	      app.trigger('show:recomend');
	      return Player.Controller.playTrack(id);
	    };

	    API.prototype.showAlbum = function(leng, id, data, direct) {
	      if (!direct) {
	        Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/release", "?id=" + id + "&referer=directLink"));
	      }
	      return __webpack_require__.e/* nsure */(8/* empty */, function(require) {
	        var Album;
	        Album = __webpack_require__(75);
	        return Album.Controller.showPage(id, data);
	      });
	    };

	    API.prototype.showArtist = function(leng, id, direct) {
	      if (!direct) {
	        Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/artistCredit", "?id=" + id + "&referer=directLink"));
	      }
	      return __webpack_require__.e/* nsure */(9, function(require) {
	        var Artist;
	        Artist = __webpack_require__(78);
	        return Artist.Controller.showPage(id);
	      });
	    };

	    API.prototype.showPlaylist = function(leng, id, direct) {
	      if (!direct) {
	        Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/publicPlaylist", "?id=" + id + "&referer=directLink"));
	      }
	      return __webpack_require__.e/* nsure */(10, function(require) {
	        var Playlist;
	        Playlist = __webpack_require__(81);
	        return Playlist.Controller.showPage(id);
	      });
	    };

	    API.prototype.showUserTracks = function() {
	      return __webpack_require__.e/* nsure */(11, function() {
	        var UserTracks;
	        UserTracks = __webpack_require__(84);
	        app.GAPageAction('page-open', 'Треки и плейлисты');
	        $('#header h1').text(app.locale.menu.tracksAndPlaylists);
	        UserTracks.Controller.showPage();
	        return app.trigger('switch:page', 'tracks');
	      });
	    };

	    API.prototype.showUserAlbums = function() {
	      return __webpack_require__.e/* nsure */(12, function() {
	        var UserAlbums;
	        UserAlbums = __webpack_require__(87);
	        app.GAPageAction('page-open', 'Избранные альбомы');
	        $('#header h1').text(app.locale.menu.albums);
	        UserAlbums.Controller.showPage();
	        return app.trigger('switch:page', 'albums');
	      });
	    };

	    API.prototype.searchTracks = function(leng, val) {
	      app.GAPageAction('page-open', 'Поиск трека по запросу - ' + val);
	      Sidebar.Controller.showSearchTracksPage(app.parsePath(val).q);
	      return app.openSearchPageFromSite = false;
	    };

	    API.prototype.searchAlbums = function(leng, val) {
	      app.GAPageAction('page-open', 'Поиск альбома по запросу - ' + val);
	      Sidebar.Controller.showSearchAlbumsPage(app.parsePath(val).q);
	      return app.openSearchPageFromSite = false;
	    };

	    API.prototype.searchArtist = function(leng, val) {
	      app.GAPageAction('page-open', 'Поиск артиста по запросу - ' + val);
	      Sidebar.Controller.showSearchArtistsPage(app.parsePath(val).q);
	      return app.openSearchPageFromSite = false;
	    };

	    return API;

	  })(Marionette.Controller);
	  api = new API;
	  new Router({
	    controller: api
	  });
	  app.on('show:new', function() {
	    app.navigate("/new");
	    return api.showNew();
	  });
	  app.on('show:user:page', function(id) {
	    app.navigate("/id" + id);
	    return api.showUserPage(null, id);
	  });
	  app.on('show:playlists', function() {
	    app.navigate("/playlists");
	    return api.showPlaylists();
	  });
	  app.on('show:feed', function() {
	    app.navigate("/feed");
	    return api.showFeed();
	  });
	  app.on('show:charts', function() {
	    app.navigate("/charts");
	    return api.showCharts();
	  });
	  app.on('show:recomend', function() {
	    app.navigate("/");
	    return api.showRecomend();
	  });
	  app.on('show:video', function() {
	    app.navigate("/video");
	    return api.showVideo();
	  });
	  app.on('show:profile', function(tab) {
	    if (tab == null) {
	      tab = "main";
	    }
	    app.navigate("/profile/" + tab);
	    return api.showProfile(null, tab);
	  });
	  app.on('show:user:tracks', function() {
	    app.navigate("/user/tracks");
	    return api.showUserTracks();
	  });
	  app.on('show:user:albums', function() {
	    app.navigate("/user/albums");
	    return api.showUserAlbums();
	  });
	  app.on('show:album', function(id, data) {
	    Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/release", "?id=" + id + "&referer=" + (app.getReferer())));
	    app.navigate("/album/" + id);
	    return api.showAlbum(null, id, data, true);
	  });
	  app.on('show:playlist', function(id, data) {
	    Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/publicPlaylist", "?id=" + id + "&referer=" + (app.getReferer())));
	    app.navigate("/playlist/" + id);
	    return api.showPlaylist(null, id, true);
	  });
	  return app.on('show:artist', function(id) {
	    Logger.createNewLog(app.getRequestUrlSid("/generalV1/log/artistCredit", "?id=" + id + "&referer=" + (app.getReferer())));
	    app.request('stop:artist:request', 'artist');
	    app.navigate("/artist/" + id);
	    return api.showArtist(null, id, true);
	  });
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 20 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(21), __webpack_require__(30), __webpack_require__(28)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Sidebar, Loader, Update) {
	  var controller;
	  __webpack_require__(42);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.show = function() {
	      var self, user;
	      self = this;
	      this.layout = new Sidebar.Layout;
	      this.listenTo(this.layout, 'show', function() {
	        $('.sidebar-wrapp').css('min-height', $(window).height());
	        return $(window).on('resize', function() {
	          return $('.sidebar-wrapp').css('min-height', $(window).height());
	        });
	      });
	      app.sidebar.show(this.layout);
	      this.showBody();
	      user = app.request('get:user');
	      if (user.promise) {
	        user.done(function(user) {
	          return self.showUser.apply(self, [user]);
	        });
	        return user.fail(function(res) {
	          if (res.readyState === 0 && res.statusText === 'abort') {
	            return console.log('отмена');
	          } else {
	            return console.log('ошибка');
	          }
	        });
	      } else {
	        return self.showUser.apply(self, [user]);
	      }
	    };

	    controller.prototype.showBody = function() {
	      var mainMenuList, userMenuList;
	      mainMenuList = app.request('get:menu');
	      userMenuList = app.request('get:tabs');
	      this.mainMenu = new Sidebar.SideMenuMainItems({
	        collection: mainMenuList
	      });
	      this.userMenu = new Sidebar.SideMenuUserItems({
	        collection: userMenuList
	      });
	      this.layout.mainMenu.show(this.mainMenu);
	      this.layout.userMenu.show(this.userMenu);
	      if (!Sidebar.urlIsFind) {
	        if (!~app.getCurrentRoute().indexOf('profile')) {
	          $('.sidebar-menu-item.recomend').addClass('active');
	        }
	      }
	      this.listenTo(app, 'switch:page', function(page) {
	        $('#sidebar .sidebar-menu-item.active').removeClass('active');
	        $('#sidebar .sidebar-menu-item.' + page).addClass('active');
	        return $('#sidebar').removeClass('open');
	      });
	      $('#header .title').mouseenter(function() {
	        return $('#sidebar').addClass('open');
	      });
	      return $('.sidebar-overlay').click(function() {
	        return $('#sidebar').removeClass('open');
	      });
	    };

	    controller.prototype.showSearchResult = function(val) {
	      if ($.trim(val) === '') {
	        if (this.isSearchLayoutShown) {
	          this.isSearchLayoutShown = false;
	          this.searchLayout.destroy();
	          $('.sidebar-content-scroll').customScrollbar('resize', true);
	        }
	        return;
	      }
	      if (!this.isSearchLayoutShown) {
	        this.searchLayout = new Sidebar.SearchLayout;
	        this.layout.searchResult.show(this.searchLayout);
	        this.listenTo(this.searchLayout, 'destroy', function() {
	          return this.isSearchLayoutShown = false;
	        });
	      }
	      this.isSearchLayoutShown = true;
	      this.SearchTracks(val);
	      this.SearchAlbums(val);
	      return this.SearchArtists(val);
	    };

	    controller.prototype.SearchTracks = function(val) {
	      var fetch, loader;
	      fetch = app.request('search:tracks', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 3
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          position: 'absolute',
	          top: '35%',
	          left: '45%'
	        }
	      });
	      this.searchLayout.tracks.show(loader);
	      fetch.done((function(_this) {
	        return function(collection) {
	          var view;
	          view = new Sidebar.SearchTracksLayout({
	            collection: collection
	          });
	          return _this.searchLayout.tracks.show(view);
	        };
	      })(this));
	      return fetch.fail(function(res) {});
	    };

	    controller.prototype.SearchArtists = function(val) {
	      var fetch, loader;
	      fetch = app.request('search:artists', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 3
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          position: 'absolute',
	          top: '35%',
	          left: '45%'
	        }
	      });
	      this.searchLayout.artists.show(loader);
	      fetch.done((function(_this) {
	        return function(collection) {
	          var view;
	          view = new Sidebar.SearchArtistsLayout({
	            collection: collection
	          });
	          return _this.searchLayout.artists.show(view);
	        };
	      })(this));
	      return fetch.fail(function(res) {});
	    };

	    controller.prototype.SearchAlbums = function(val) {
	      var fetch, loader;
	      fetch = app.request('search:albums', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 3
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          position: 'absolute',
	          top: '35%',
	          left: '45%'
	        }
	      });
	      this.searchLayout.albums.show(loader);
	      fetch.done((function(_this) {
	        return function(collection) {
	          var view;
	          view = new Sidebar.SearchAlbumsLayout({
	            collection: collection
	          });
	          return _this.searchLayout.albums.show(view);
	        };
	      })(this));
	      return fetch.fail(function(res) {});
	    };

	    controller.prototype.showSearchTracksPage = function(val) {
	      var list, loader;
	      list = app.request('search:page', 'track', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 60
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          'top': $(window).height() / 2
	        }
	      });
	      app.content.show(loader);
	      return list.done(function(searchCollection) {
	        var fetchMore, model, searchView, winHeight;
	        $(window).off('.searchpage');
	        fetchMore = function() {
	          var page, trackFetch;
	          $(window).off('.searchpage');
	          page = Math.ceil(searchCollection.length / 60) + 1;
	          trackFetch = searchCollection.fetch({
	            data: {
	              q: val,
	              page: page,
	              items_on_page: 90
	            },
	            remove: false
	          });
	          trackFetch.done(function(newcollection) {
	            var winHeight;
	            app.checkActiveTrack();
	            if (newcollection.searched.length) {
	              winHeight = $('body').height();
	              return $(window).on('scroll.searchpage', function() {
	                var scroll;
	                scroll = $(window).scrollTop() + $(window).height() * 5;
	                if (winHeight < scroll) {
	                  return fetchMore();
	                }
	              });
	            }
	          });
	          return trackFetch.fail(function(res) {
	            if (res.status === 422) {
	              return contacts.add(res.responseJSON);
	            } else {
	              return console.log('Произошла ошибка');
	            }
	          });
	        };
	        model = new Backbone.Model({
	          type: 'trk',
	          title: val
	        });
	        searchView = new Sidebar.SearchPageTracks({
	          model: model,
	          collection: searchCollection
	        });
	        app.content.show(searchView);
	        app.checkActiveTrack();
	        winHeight = $('body').height();
	        return $(window).on('scroll.searchpage', function() {
	          var scroll;
	          scroll = $(window).scrollTop() + $(window).height() * 5;
	          if (winHeight < scroll) {
	            return fetchMore();
	          }
	        });
	      });
	    };

	    controller.prototype.showSearchAlbumsPage = function(val) {
	      var list, loader;
	      list = app.request('search:page', 'album', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 30
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          'top': $(window).height() / 2
	        }
	      });
	      app.content.show(loader);
	      return list.done(function(searchCollection) {
	        var fetchMore, model, searchView, winHeight;
	        $(window).off('.searchpage');
	        fetchMore = function() {
	          var page, trackFetch;
	          $(window).off('.searchpage');
	          page = Math.ceil(searchCollection.length / 30) + 1;
	          trackFetch = searchCollection.fetch({
	            data: {
	              q: val,
	              page: page,
	              items_on_page: 30
	            },
	            remove: false
	          });
	          trackFetch.done(function(newcollection) {
	            var winHeight;
	            if (newcollection.searched.length) {
	              winHeight = $('body').height();
	              return $(window).on('scroll.searchpage', function() {
	                var scroll;
	                scroll = $(window).scrollTop() + $(window).height() * 5;
	                if (winHeight < scroll) {
	                  return fetchMore();
	                }
	              });
	            }
	          });
	          return trackFetch.fail(function(res) {
	            if (res.status === 422) {
	              return contacts.add(res.responseJSON);
	            } else {
	              return console.log('Произошла ошибка');
	            }
	          });
	        };
	        model = new Backbone.Model({
	          type: 'alb',
	          title: val
	        });
	        searchView = new Sidebar.SearchPageAlbums({
	          model: model,
	          collection: searchCollection
	        });
	        app.content.show(searchView);
	        app.checkActiveTrack();
	        winHeight = $('body').height();
	        return $(window).on('scroll.searchpage', function() {
	          var scroll;
	          scroll = $(window).scrollTop() + $(window).height() * 5;
	          if (winHeight < scroll) {
	            return fetchMore();
	          }
	        });
	      });
	    };

	    controller.prototype.showSearchArtistsPage = function(val) {
	      var list, loader;
	      list = app.request('search:page', 'artist', {
	        data: {
	          q: val,
	          page: 1,
	          items_on_page: 30
	        }
	      });
	      loader = new Loader.View({
	        styles: {
	          'top': $(window).height() / 2
	        }
	      });
	      app.content.show(loader);
	      return list.done(function(searchCollection) {
	        var fetchMore, model, searchView, winHeight;
	        $(window).off('.searchpage');
	        fetchMore = function() {
	          var page, trackFetch;
	          $(window).off('.searchpage');
	          page = Math.ceil(searchCollection.length / 30) + 1;
	          trackFetch = searchCollection.fetch({
	            data: {
	              q: val,
	              page: page,
	              items_on_page: 30
	            },
	            remove: false
	          });
	          trackFetch.done(function(newcollection) {
	            var winHeight;
	            if (newcollection.searched.length) {
	              winHeight = $('body').height();
	              return $(window).on('scroll.searchpage', function() {
	                var scroll;
	                scroll = $(window).scrollTop() + $(window).height() * 5;
	                if (winHeight < scroll) {
	                  return fetchMore();
	                }
	              });
	            }
	          });
	          return trackFetch.fail(function(res) {
	            if (res.status === 422) {
	              return contacts.add(res.responseJSON);
	            } else {
	              return console.log('Произошла ошибка');
	            }
	          });
	        };
	        model = new Backbone.Model({
	          type: 'art',
	          title: val
	        });
	        searchView = new Sidebar.SearchPageArtists({
	          model: model,
	          collection: searchCollection
	        });
	        app.content.show(searchView);
	        app.checkActiveTrack();
	        winHeight = $('body').height();
	        return $(window).on('scroll.searchpage', function() {
	          var scroll;
	          scroll = $(window).scrollTop() + $(window).height() * 5;
	          if (winHeight < scroll) {
	            return fetchMore();
	          }
	        });
	      });
	    };

	    controller.prototype.closeSearchPage = function() {
	      if (app.openSearchPageFromSite) {
	        return history.back();
	      } else {
	        return app.trigger('show:recomend');
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Sidebar.Controller = new controller;
	  _.extend(Sidebar.Controller, {
	    showUser: function(user) {
	      user = new Sidebar.User({
	        model: user
	      });
	      return this.layout.user.show(user);
	    }
	  });
	  return Sidebar;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 21 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(26), __webpack_require__(34), __webpack_require__(37), __webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track, Album, Artist, Playlist, Player) {
	  var Sidebar;
	  Sidebar = {};
	  Sidebar.Layout = (function(superClass) {
	    extend(Layout, superClass);

	    function Layout() {
	      return Layout.__super__.constructor.apply(this, arguments);
	    }

	    Layout.prototype.template = function() {
	      return __webpack_require__(41)();
	    };

	    Layout.prototype.className = 'sidebar-wrapp';

	    Layout.prototype.regions = {
	      'user': '.profile',
	      'mainMenu': '.main-nav',
	      'userMenu': '.user-nav',
	      'searchResult': '.sidebar-search-container'
	    };

	    Layout.prototype.onShow = function() {
	      var sidebarScrollContent;
	      sidebarScrollContent = $('.sidebar-content-scroll');
	      sidebarScrollContent.height($(window).height() - 116);
	      $(window).on('resize.userTracks', function() {
	        return sidebarScrollContent.height($(window).height() - 116);
	      });
	      return sidebarScrollContent.customScrollbar({
	        updateOnWindowResize: true
	      });
	    };

	    Layout.prototype.events = {
	      'keyup .search input': function() {
	        var val;
	        val = this.$('.search input').val();
	        if ($.trim(val) === '') {
	          $('.sidebar-body .search').removeClass('active');
	        } else {
	          $('.sidebar-body .search').addClass('active');
	        }
	        if (this.searchTimer) {
	          clearTimeout(this.searchTimer);
	        }
	        return this.searchTimer = setTimeout((function(_this) {
	          return function() {
	            return Sidebar.Controller.showSearchResult(val);
	          };
	        })(this), 300);
	      },
	      'click .close': function() {
	        return this.$('.search input').val('').keyup();
	      }
	    };

	    return Layout;

	  })(Marionette.LayoutView);
	  Sidebar.User = (function(superClass) {
	    extend(User, superClass);

	    function User() {
	      return User.__super__.constructor.apply(this, arguments);
	    }

	    User.prototype.template = '#sidebar-user-tpl';

	    User.prototype.regions = {
	      'notify': '.notify-region'
	    };

	    User.prototype.events = {
	      'click .exit': function(e) {
	        e.stopPropagation();
	        return $.get('/exit', function() {
	          return location.href = '';
	        });
	      },
	      'click': function() {
	        return app.trigger('show:profile');
	      }
	    };

	    User.prototype.modelEvents = {
	      'change': function() {
	        return this.render();
	      }
	    };

	    return User;

	  })(Marionette.LayoutView);
	  Sidebar.SideMenuMainItem = (function(superClass) {
	    extend(SideMenuMainItem, superClass);

	    function SideMenuMainItem() {
	      return SideMenuMainItem.__super__.constructor.apply(this, arguments);
	    }

	    SideMenuMainItem.prototype.template = '#sidebar-menu-item-tpl';

	    SideMenuMainItem.prototype.className = 'sidebar-menu-item';

	    SideMenuMainItem.prototype.tagName = 'li';

	    SideMenuMainItem.prototype.events = {
	      'click': function() {
	        app.trigger(this.model.get('trigger'));
	        return false;
	      }
	    };

	    SideMenuMainItem.prototype.onRender = function() {
	      var href, route;
	      this.$el.addClass(this.model.get('class'));
	      href = app.getCurrentRoute().split('/')[1] || '';
	      route = this.model.get('href');
	      if (route === href) {
	        this.$el.addClass('active');
	        return Sidebar.urlIsFind = true;
	      }
	    };

	    return SideMenuMainItem;

	  })(Marionette.ItemView);
	  Sidebar.SideMenuMainItems = (function(superClass) {
	    extend(SideMenuMainItems, superClass);

	    function SideMenuMainItems() {
	      return SideMenuMainItems.__super__.constructor.apply(this, arguments);
	    }

	    SideMenuMainItems.prototype.childView = Sidebar.SideMenuMainItem;

	    SideMenuMainItems.prototype.tagName = 'ul';

	    return SideMenuMainItems;

	  })(Marionette.CollectionView);
	  Sidebar.SideMenuUserItem = (function(superClass) {
	    extend(SideMenuUserItem, superClass);

	    function SideMenuUserItem() {
	      return SideMenuUserItem.__super__.constructor.apply(this, arguments);
	    }

	    SideMenuUserItem.prototype.template = '#sidebar-menu-item-tpl';

	    SideMenuUserItem.prototype.className = 'sidebar-menu-item';

	    SideMenuUserItem.prototype.tagName = 'li';

	    SideMenuUserItem.prototype.events = {
	      'click': function() {
	        app.trigger(this.model.get('trigger'));
	        return false;
	      }
	    };

	    SideMenuUserItem.prototype.onRender = function() {
	      var currentRoute, href, route;
	      this.$el.addClass(this.model.get('class'));
	      currentRoute = app.getCurrentRoute().split('/');
	      href = currentRoute[1] + '/' + currentRoute[2];
	      route = this.model.get('href');
	      if (route === href) {
	        this.$el.addClass('active');
	        return Sidebar.urlIsFind = true;
	      }
	    };

	    return SideMenuUserItem;

	  })(Marionette.ItemView);
	  Sidebar.SideMenuUserItems = (function(superClass) {
	    extend(SideMenuUserItems, superClass);

	    function SideMenuUserItems() {
	      return SideMenuUserItems.__super__.constructor.apply(this, arguments);
	    }

	    SideMenuUserItems.prototype.template = '#sidebar-menu-user-tpl';

	    SideMenuUserItems.prototype.childView = Sidebar.SideMenuUserItem;

	    SideMenuUserItems.prototype.childViewContainer = '.user-menu';

	    return SideMenuUserItems;

	  })(Marionette.CompositeView);
	  Sidebar.SearchLayout = (function(superClass) {
	    extend(SearchLayout, superClass);

	    function SearchLayout() {
	      return SearchLayout.__super__.constructor.apply(this, arguments);
	    }

	    SearchLayout.prototype.template = '#sidebar-search-layout-tpl';

	    SearchLayout.prototype.className = 'scrollable';

	    SearchLayout.prototype.regions = {
	      tracks: '.tracks-wrapp',
	      albums: '.albums-wrapp',
	      artists: '.artists-wrapp'
	    };

	    SearchLayout.prototype.onShow = function() {
	      $('.sidebar-content-wrapp').hide();
	      return $('.sidebar-content-scroll').customScrollbar('resize', true);
	    };

	    SearchLayout.prototype.onDestroy = function() {
	      return $('.sidebar-content-wrapp').show();
	    };

	    return SearchLayout;

	  })(Marionette.LayoutView);
	  Sidebar.SearchTrack = (function(superClass) {
	    extend(SearchTrack, superClass);

	    function SearchTrack() {
	      return SearchTrack.__super__.constructor.apply(this, arguments);
	    }

	    SearchTrack.prototype.template = '#sidebar-search-track-tpl';

	    SearchTrack.prototype.className = 'search-track';

	    SearchTrack.prototype.events = {
	      'click': function() {
	        return Player.Controller.playTrack(this.model.get('id'), this.model.toJSON());
	      }
	    };

	    return SearchTrack;

	  })(Marionette.ItemView);
	  Sidebar.SearchArtist = (function(superClass) {
	    extend(SearchArtist, superClass);

	    function SearchArtist() {
	      return SearchArtist.__super__.constructor.apply(this, arguments);
	    }

	    SearchArtist.prototype.template = '#sidebar-search-artist-tpl';

	    SearchArtist.prototype.className = 'search-artist';

	    SearchArtist.prototype.events = {
	      'click': function() {
	        app.trigger('show:artist', this.model.get('artistid'));
	        return $('#sidebar').removeClass('open');
	      }
	    };

	    return SearchArtist;

	  })(Marionette.ItemView);
	  Sidebar.SearchAlbum = (function(superClass) {
	    extend(SearchAlbum, superClass);

	    function SearchAlbum() {
	      return SearchAlbum.__super__.constructor.apply(this, arguments);
	    }

	    SearchAlbum.prototype.template = '#sidebar-search-album-tpl';

	    SearchAlbum.prototype.className = 'search-album';

	    SearchAlbum.prototype.events = {
	      'click': function() {
	        app.trigger('show:album', this.model.get('id'), this.model);
	        return $('#sidebar').removeClass('open');
	      }
	    };

	    return SearchAlbum;

	  })(Marionette.ItemView);
	  Sidebar.SearchTracksEmpty = (function(superClass) {
	    extend(SearchTracksEmpty, superClass);

	    function SearchTracksEmpty() {
	      return SearchTracksEmpty.__super__.constructor.apply(this, arguments);
	    }

	    SearchTracksEmpty.prototype.template = '#sidebar-search-tracks-empty-tpl';

	    SearchTracksEmpty.prototype.className = 'search-empty-message';

	    return SearchTracksEmpty;

	  })(Marionette.ItemView);
	  Sidebar.SearchArtistsEmpty = (function(superClass) {
	    extend(SearchArtistsEmpty, superClass);

	    function SearchArtistsEmpty() {
	      return SearchArtistsEmpty.__super__.constructor.apply(this, arguments);
	    }

	    SearchArtistsEmpty.prototype.template = '#sidebar-search-artists-empty-tpl';

	    SearchArtistsEmpty.prototype.className = 'search-empty-message';

	    return SearchArtistsEmpty;

	  })(Marionette.ItemView);
	  Sidebar.SearchAlbumsEmpty = (function(superClass) {
	    extend(SearchAlbumsEmpty, superClass);

	    function SearchAlbumsEmpty() {
	      return SearchAlbumsEmpty.__super__.constructor.apply(this, arguments);
	    }

	    SearchAlbumsEmpty.prototype.template = '#sidebar-search-artists-empty-tpl';

	    SearchAlbumsEmpty.prototype.className = 'search-empty-message';

	    return SearchAlbumsEmpty;

	  })(Marionette.ItemView);
	  Sidebar.SearchTracksLayout = (function(superClass) {
	    extend(SearchTracksLayout, superClass);

	    function SearchTracksLayout() {
	      return SearchTracksLayout.__super__.constructor.apply(this, arguments);
	    }

	    SearchTracksLayout.prototype.childView = Sidebar.SearchTrack;

	    SearchTracksLayout.prototype.childViewContainer = '.search-block-content';

	    SearchTracksLayout.prototype.template = '#sidebar-search-result-layout-tpl';

	    SearchTracksLayout.prototype.className = 'search-result';

	    SearchTracksLayout.prototype.emptyView = Sidebar.SearchTracksEmpty;

	    SearchTracksLayout.prototype.onRender = function() {
	      if (this.collection.length === 0) {
	        return this.$('.show-more').hide();
	      } else {
	        return this.$('.show-more').show();
	      }
	    };

	    SearchTracksLayout.prototype.events = {
	      'click .show-more': function() {
	        var val;
	        val = $('.sidebar-body .search input').val();
	        app.navigate('/search/tracks/?q=' + val);
	        Sidebar.Controller.showSearchTracksPage(val);
	        $('#sidebar').removeClass('open');
	        return app.openSearchPageFromSite = true;
	      }
	    };

	    return SearchTracksLayout;

	  })(Marionette.CompositeView);
	  Sidebar.SearchArtistsLayout = (function(superClass) {
	    extend(SearchArtistsLayout, superClass);

	    function SearchArtistsLayout() {
	      return SearchArtistsLayout.__super__.constructor.apply(this, arguments);
	    }

	    SearchArtistsLayout.prototype.childView = Sidebar.SearchArtist;

	    SearchArtistsLayout.prototype.childViewContainer = '.search-block-content';

	    SearchArtistsLayout.prototype.template = '#sidebar-search-result-layout-tpl';

	    SearchArtistsLayout.prototype.className = 'search-result';

	    SearchArtistsLayout.prototype.emptyView = Sidebar.SearchArtistsEmpty;

	    SearchArtistsLayout.prototype.onRender = function() {
	      if (this.collection.length === 0) {
	        return this.$('.show-more').hide();
	      } else {
	        return this.$('.show-more').show();
	      }
	    };

	    SearchArtistsLayout.prototype.events = {
	      'click .show-more': function() {
	        var val;
	        val = $('.sidebar-body .search input').val();
	        app.navigate('/search/artists/?q=' + val);
	        Sidebar.Controller.showSearchArtistsPage(val);
	        $('#sidebar').removeClass('open');
	        return app.openSearchPageFromSite = true;
	      }
	    };

	    return SearchArtistsLayout;

	  })(Marionette.CompositeView);
	  Sidebar.SearchAlbumsLayout = (function(superClass) {
	    extend(SearchAlbumsLayout, superClass);

	    function SearchAlbumsLayout() {
	      return SearchAlbumsLayout.__super__.constructor.apply(this, arguments);
	    }

	    SearchAlbumsLayout.prototype.childView = Sidebar.SearchAlbum;

	    SearchAlbumsLayout.prototype.childViewContainer = '.search-block-content';

	    SearchAlbumsLayout.prototype.template = '#sidebar-search-result-layout-tpl';

	    SearchAlbumsLayout.prototype.className = 'search-result';

	    SearchAlbumsLayout.prototype.emptyView = Sidebar.SearchAlbumsEmpty;

	    SearchAlbumsLayout.prototype.onRender = function() {
	      if (this.collection.length === 0) {
	        return this.$('.show-more').hide();
	      } else {
	        return this.$('.show-more').show();
	      }
	    };

	    SearchAlbumsLayout.prototype.events = {
	      'click .show-more': function() {
	        var val;
	        val = $('.sidebar-body .search input').val();
	        app.navigate('/search/albums/?q=' + val);
	        Sidebar.Controller.showSearchAlbumsPage(val);
	        $('#sidebar').removeClass('open');
	        return app.openSearchPageFromSite = true;
	      }
	    };

	    return SearchAlbumsLayout;

	  })(Marionette.CompositeView);
	  Sidebar.SearchPageTrack = (function(superClass) {
	    extend(SearchPageTrack, superClass);

	    function SearchPageTrack() {
	      return SearchPageTrack.__super__.constructor.apply(this, arguments);
	    }

	    SearchPageTrack.prototype.template = '#track-tpl';

	    SearchPageTrack.prototype.className = 'track-item';

	    return SearchPageTrack;

	  })(Track.Item);
	  Sidebar.SearchPageTracks = (function(superClass) {
	    extend(SearchPageTracks, superClass);

	    function SearchPageTracks() {
	      return SearchPageTracks.__super__.constructor.apply(this, arguments);
	    }

	    SearchPageTracks.prototype.template = '#sidebar-search-page-layout-tpl';

	    SearchPageTracks.prototype.className = 'search-page tracks-wrapper';

	    SearchPageTracks.prototype.childView = Sidebar.SearchPageTrack;

	    SearchPageTracks.prototype.childViewContainer = '.search-content';

	    SearchPageTracks.prototype.events = {
	      'click .close': function() {
	        return Sidebar.Controller.closeSearchPage();
	      }
	    };

	    return SearchPageTracks;

	  })(Marionette.CompositeView);
	  Sidebar.SearchPageArtists = (function(superClass) {
	    extend(SearchPageArtists, superClass);

	    function SearchPageArtists() {
	      return SearchPageArtists.__super__.constructor.apply(this, arguments);
	    }

	    SearchPageArtists.prototype.template = '#sidebar-search-page-layout-tpl';

	    SearchPageArtists.prototype.className = 'search-page artists-wrapper';

	    SearchPageArtists.prototype.childView = Sidebar.SearchPageArtist;

	    SearchPageArtists.prototype.childViewContainer = '.search-content';

	    SearchPageArtists.prototype.events = {
	      'click .close': function() {
	        return Sidebar.Controller.closeSearchPage();
	      }
	    };

	    SearchPageArtists.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return SearchPageArtists;

	  })(Marionette.CompositeView);
	  Sidebar.SearchPageAlbums = (function(superClass) {
	    extend(SearchPageAlbums, superClass);

	    function SearchPageAlbums() {
	      return SearchPageAlbums.__super__.constructor.apply(this, arguments);
	    }

	    SearchPageAlbums.prototype.template = '#sidebar-search-page-layout-tpl';

	    SearchPageAlbums.prototype.className = 'search-page albums-wrapper';

	    SearchPageAlbums.prototype.childView = Sidebar.SearchPageAlbum;

	    SearchPageAlbums.prototype.childViewContainer = '.search-content';

	    SearchPageAlbums.prototype.events = {
	      'click .close': function() {
	        return Sidebar.Controller.closeSearchPage();
	      }
	    };

	    SearchPageAlbums.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return SearchPageAlbums;

	  })(Marionette.CompositeView);
	  return Sidebar;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 22 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(23), __webpack_require__(24), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Track, Model, Notification) {
	  var controller;
	  __webpack_require__(25);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.openOptions = function(item) {
	      var collection, layout, list, opts;
	      if (item.model.has('optionsList')) {
	        if (item.model.has('playlistTrack')) {
	          list = [
	            {
	              title: 'удалить из плейлиста',
	              "class": 'delete-from-playlist'
	            }
	          ];
	        } else {
	          if (item.model.get('inFavorite')) {
	            list = [
	              {
	                title: 'удалить из медиатеки',
	                "class": 'delete'
	              }
	            ];
	          } else {
	            list = [
	              {
	                title: 'добавить в медиатеку',
	                "class": 'add'
	              }
	            ];
	          }
	        }
	        _.each(item.model.get('optionsList'), function(item) {
	          return list.push(item);
	        });
	        collection = new Backbone.Collection(list);
	      } else {
	        collection = new Backbone.Collection;
	      }
	      if (item.model.get('artist_is_va')) {
	        opts = collection.findWhere({
	          artist: true
	        });
	        if (opts) {
	          opts.remove({
	            silent: true
	          });
	        }
	      }
	      layout = new Track.OptionsLayout({
	        model: item.model,
	        collection: collection
	      });
	      return item.optionsRegion.show(layout);
	    };

	    controller.prototype.openPlaylistsList = function(item) {
	      var playlist, view;
	      playlist = app.request('get:favorite:playlists');
	      if (!playlist.promise) {
	        view = new Track.PlaylistItems({
	          model: item.model,
	          collection: playlist
	        });
	        return item.optionsRegion.show(view);
	      }
	    };

	    controller.prototype.selectText = function(target) {
	      var rng, sel, textNode;
	      if (document.createRange) {
	        rng = document.createRange();
	        textNode = target.firstChild;
	        rng.selectNode(target);
	        rng.setStart(textNode, 0);
	        rng.setEnd(textNode, textNode.length);
	        sel = window.getSelection();
	        sel.removeAllRanges();
	        return sel.addRange(rng);
	      } else {
	        rng = document.body.createTextRange();
	        rng.moveToElementText(target);
	        return rng.select();
	      }
	    };

	    controller.prototype.removeTrackFromFavorite = function(id) {
	      return app.request('get:favorite:tracks').done(function(favorites) {
	        var track;
	        track = favorites.findWhere({
	          id: id
	        });
	        if (track) {
	          track.destroy();
	          return Notification.Controller.show({
	            message: "Трек «" + (track.get('title')) + "» удален медиатеки",
	            type: 'success',
	            time: 3000
	          });
	        }
	      });
	    };

	    controller.prototype.addTrackToFavorite = function(track) {
	      return app.request('get:favorite:tracks').done(function(favorites) {
	        var model;
	        model = track.toJSON();
	        if (!model.favourites || model.favourites.sort === void 0) {
	          model.favourites = {};
	          model.favourites.sort = 10000;
	        }
	        favorites.create(model);
	        app.GAPageAction('Добавление трека в избранное', track.get('title') + ' - ' + location.href);
	        return Notification.Controller.show({
	          message: "Трек «" + (track.get('title')) + "» добавлен медиатеку",
	          type: 'success',
	          time: 3000
	        });
	      });
	    };

	    controller.prototype.createPlaylist = function(title, track) {
	      var playlist, playlistCreate;
	      playlist = app.request('get:favorite:playlists');
	      playlistCreate = playlist.create({
	        'title': title,
	        tracks: [track.model.toJSON()]
	      }, {
	        wait: true
	      });
	      $('.options-wrapper .back-to-options').hide();
	      $('.options-wrapper .options-playlist-scroll-wrapp').hide();
	      $('.options-wrapper').css('min-height', 300 + 'px').append('<div class="loader" style="top:130px"></div>');
	      return playlistCreate.once('sync', (function(_this) {
	        return function(playlist) {
	          return _this.addTrackToPlaylist(playlist.id, track.model);
	        };
	      })(this));
	    };

	    controller.prototype.addTrackToPlaylist = function(playlistId, track) {
	      var playlist, playlists, success;
	      playlistId = +playlistId;
	      playlists = app.request('get:favorite:playlists');
	      playlist = playlists.findWhere({
	        id: playlistId
	      });
	      window.test = playlist;
	      success = function(data) {
	        var collection, count, trackData;
	        app.GAPageAction("Добавление трека в плейлист " + playlistId, track.get('artist') + ' - ' + track.get('title'));
	        trackData = track.toJSON();
	        trackData.playlist = data.playlist;
	        if (playlist.has('tracks')) {
	          collection = _.union(playlist.get('tracks'), [trackData]);
	          count = collection.length;
	          playlist.set({
	            tracks: collection,
	            items_count: count
	          });
	        } else {
	          playlist.set({
	            tracks: [trackData],
	            items_count: 1
	          });
	        }
	        $('.options-wrapper').remove();
	        return Notification.Controller.show({
	          message: "Трек «" + (track.get('title')) + "» добавлен плейлист",
	          type: 'success',
	          time: 3000
	        });
	      };
	      if (playlist) {
	        $('.options-wrapper').css('min-height', 300 + 'px').empty().append('<div class="loader" style="top:130px"></div>');
	        return $.ajax({
	          type: "GET",
	          url: app.getRequestUrlSid("/playlist/additem", "?contentid=" + (track.get('id')) + "&playlistid=" + playlistId),
	          success: success
	        });
	      }
	    };

	    controller.prototype.removeTrackFromPlaylist = function(track, parent) {
	      var newCollection, oldCollection, parentTrackModel;
	      parentTrackModel = parent.options.parentTrack.model;
	      oldCollection = parentTrackModel.get('tracks');
	      newCollection = _.filter(oldCollection, function(obj) {
	        return track.model.get('id') !== obj.id;
	      });
	      parentTrackModel.set('tracks', newCollection);
	      return $.ajax({
	        type: "GET",
	        url: app.getRequestUrlSid("/playlist/removeitem", "?itemid=" + (track.model.get('playlist').itemid))
	      });
	    };

	    return controller;

	  })(Marionette.Controller);
	  Track.Controller = new controller;
	  _.extend(Track, Model);
	  return Track;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 23 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player) {
	  var Track;
	  Track = {};
	  Track.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#track-tpl';

	    Item.prototype.className = 'track-item';

	    Item.prototype.regions = {
	      optionsRegion: '.options-content'
	    };

	    Item.prototype.onShow = function() {
	      if (this.model.has('is_deleted')) {
	        return this.$el.addClass('disable');
	      }
	    };

	    Item.prototype.id = function() {
	      return this.model.get('uniq');
	    };

	    Item.prototype.events = {
	      'click': function() {
	        if (this.model.has('is_deleted')) {
	          return;
	        }
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          app.playedAlbum = null;
	          return Player.Controller.playByCollection(this.model.collection, this.model.get('id'), Backbone.history.location.href);
	        }
	      },
	      'mouseenter': function() {
	        var favorites, item;
	        favorites = app.request('get:favorite:tracks');
	        if (favorites.promise) {
	          favorites.done((function(_this) {
	            return function(collection) {
	              var item;
	              item = collection.some(function(item) {
	                return item.get('id') === _this.model.get('id');
	              });
	              if (item) {
	                _this.model.set('inFavorite', true);
	                return _this.$('.track-controll').removeClass('add').addClass('delete');
	              } else {
	                _this.model.set('inFavorite', false);
	                return _this.$('.track-controll').removeClass('delete').addClass('add');
	              }
	            };
	          })(this));
	          return favorites.fail(function(err) {
	            return console.log(arguments);
	          });
	        } else {
	          item = favorites.some((function(_this) {
	            return function(item) {
	              return item.get('id') === _this.model.get('id');
	            };
	          })(this));
	          if (item) {
	            this.model.set('inFavorite', true);
	            return this.$('.track-controll').removeClass('add').addClass('delete');
	          } else {
	            this.model.set('inFavorite', false);
	            return this.$('.track-controll').removeClass('delete').addClass('add');
	          }
	        }
	      },
	      'click .playlist-track-controll': function() {
	        this.trigger('playlist:track:delete');
	        return false;
	      },
	      'click .track-controll.add': function() {
	        Track.Controller.addTrackToFavorite(this.model);
	        this.model.set('inFavorite', true);
	        this.$('.track-controll').removeClass('add').addClass('delete');
	        app.trigger('add:to:favorite');
	        return false;
	      },
	      'click .track-controll.delete': function() {
	        Track.Controller.removeTrackFromFavorite(this.model.get('id'));
	        this.model.set('inFavorite', false);
	        this.$('.track-controll').removeClass('delete').addClass('add');
	        app.trigger('remove:from:favorite');
	        return false;
	      },
	      'click .options-wrapper': function() {
	        return false;
	      },
	      'click .more-options': function() {
	        Track.Controller.openOptions(this);
	        return false;
	      },
	      'click .add-to-playlist': function() {
	        Track.Controller.openPlaylistsList(this);
	        return false;
	      },
	      'click .back-to-options': function() {
	        Track.Controller.openOptions(this);
	        return false;
	      },
	      'click .options-playlist-item': function(e) {
	        return Track.Controller.addTrackToPlaylist($(e.target).attr('data-id'), this.model);
	      },
	      'click .options-open-album': function(e) {
	        e.stopPropagation();
	        return app.trigger('show:album', this.model.get('release_id'));
	      },
	      'click .options-open-artist': function(e) {
	        e.stopPropagation();
	        return app.trigger('show:artist', this.model.get('artistid'));
	      },
	      'keypress .new-playlist input': function(e) {
	        if (e.keyCode === 13) {
	          return Track.Controller.createPlaylist($('.new-playlist input').val(), this);
	        }
	      },
	      'click .artist span': function(e) {
	        e.stopPropagation();
	        return app.trigger('show:artist', this.model.get('artistid'));
	      }
	    };

	    return Item;

	  })(Marionette.LayoutView);
	  Track.OptionsItem = (function(superClass) {
	    extend(OptionsItem, superClass);

	    function OptionsItem() {
	      return OptionsItem.__super__.constructor.apply(this, arguments);
	    }

	    OptionsItem.prototype.template = '#track-options-list-item-tpl';

	    OptionsItem.prototype.className = 'track-options-list-item';

	    OptionsItem.prototype.tagName = 'li';

	    OptionsItem.prototype.onRender = function() {
	      return this.$el.addClass(this.model.get('class'));
	    };

	    return OptionsItem;

	  })(Marionette.ItemView);
	  Track.OptionsLayout = (function(superClass) {
	    extend(OptionsLayout, superClass);

	    function OptionsLayout() {
	      return OptionsLayout.__super__.constructor.apply(this, arguments);
	    }

	    OptionsLayout.prototype.template = '#options-list-tpl';

	    OptionsLayout.prototype.className = 'options-wrapper';

	    OptionsLayout.prototype.childView = Track.OptionsItem;

	    OptionsLayout.prototype.childViewContainer = '.options-list';

	    OptionsLayout.prototype.onShow = function() {
	      var target;
	      target = this.$('.textarea')[0];
	      return Track.Controller.selectText(target);
	    };

	    OptionsLayout.prototype.events = {
	      'click': function() {
	        var target;
	        target = this.$('.textarea')[0];
	        return Track.Controller.selectText(target);
	      },
	      'click .delete': function(e) {
	        Track.Controller.removeTrackFromFavorite(this.model.get('id'));
	        this.model.set('inFavorite', false, {
	          silent: true
	        });
	        $(e.target).closest('.options').find('.track-controll').removeClass('delete').addClass('add');
	        this.$el.find('.delete').removeClass('delete').addClass('add').text('Добавить в медиатеку');
	        app.trigger('remove:from:favorite');
	        return false;
	      },
	      'click .add': function() {
	        Track.Controller.addTrackToFavorite(this.model);
	        this.model.set('inFavorite', true, {
	          silent: true
	        });
	        this.$el.closest('.options').find('.track-controll').removeClass('add').addClass('delete');
	        this.$el.find('.add').removeClass('add').addClass('delete').text('Удалить из медиатеки');
	        app.trigger('add:to:favorite');
	        return false;
	      },
	      'mouseleave': 'closeOptions',
	      'click .options-overlay': 'closeOptions'
	    };

	    OptionsLayout.prototype.closeOptions = function(e) {
	      e.stopPropagation();
	      return $('.options-wrapper').remove();
	    };

	    return OptionsLayout;

	  })(Marionette.CompositeView);
	  Track.PlaylistItem = (function(superClass) {
	    extend(PlaylistItem, superClass);

	    function PlaylistItem() {
	      return PlaylistItem.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItem.prototype.template = '#options-playlist-item-tpl';

	    PlaylistItem.prototype.className = 'options-playlist-item';

	    PlaylistItem.prototype.tagname = 'li';

	    PlaylistItem.prototype.onRender = function() {
	      return this.$el.attr('data-id', this.model.get('id'));
	    };

	    return PlaylistItem;

	  })(Marionette.ItemView);
	  Track.PlaylistItems = (function(superClass) {
	    extend(PlaylistItems, superClass);

	    function PlaylistItems() {
	      return PlaylistItems.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistItems.prototype.template = '#options-playlist-window-tpl';

	    PlaylistItems.prototype.childView = Track.PlaylistItem;

	    PlaylistItems.prototype.childViewContainer = '.container';

	    PlaylistItems.prototype.className = 'options-wrapper';

	    PlaylistItems.prototype.events = {
	      'click .new-playlist input': function() {
	        return $('.new-playlist input').select();
	      },
	      'blur .new-playlist input': function() {
	        return $('.new-playlist input').val('+ Новый плейлист');
	      }
	    };

	    PlaylistItems.prototype.onShow = function() {
	      return $('.options-wrapper .options-playlist-scroll-wrapp').customScrollbar();
	    };

	    return PlaylistItems;

	  })(Marionette.CompositeView);
	  Track.ListView = (function(superClass) {
	    extend(ListView, superClass);

	    function ListView() {
	      return ListView.__super__.constructor.apply(this, arguments);
	    }

	    ListView.prototype.childView = Track.Item;

	    ListView.prototype.className = 'tracks-wrapper';

	    return ListView;

	  })(Marionette.CollectionView);
	  return Track;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 24 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Track;
	  Track = {};
	  Track.Model = (function(superClass) {
	    extend(Model, superClass);

	    function Model() {
	      return Model.__super__.constructor.apply(this, arguments);
	    }

	    Model.prototype.urlRoot = '/favoritesTracks';

	    Model.prototype.defaults = {
	      "duration": "",
	      "artist": "",
	      "title": ""
	    };

	    return Model;

	  })(Backbone.Model);
	  Track.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Track.Model;

	    return Collection;

	  })(Backbone.Collection);
	  return Track;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 25 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(27), __webpack_require__(32), __webpack_require__(1), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Album, Model, Player, Notification) {
	  var controller;
	  __webpack_require__(33);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.Play = function(albumModel) {
	      var albumTracks, collection;
	      app.playedAlbum = albumModel.get('id');
	      if (albumModel.has('tracks')) {
	        collection = new Backbone.Collection(albumModel.get('tracks'));
	        collection.each(function(item) {
	          item.set('uniq', 'album' + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }
	          ]);
	        });
	        return Player.Controller.playByCollection(collection);
	      } else {
	        albumTracks = albumModel.fetch();
	        albumTracks.done(function() {
	          var href;
	          collection = new Backbone.Collection(albumModel.get('tracks'));
	          collection.each(function(item) {
	            item.set('uniq', 'album' + item.id);
	            return item.set('optionsList', [
	              {
	                title: app.locale.options.addTPl,
	                "class": 'add-to-playlist'
	              }, {
	                title: app.locale.options.toArt,
	                "class": 'options-open-artist'
	              }
	            ]);
	          });
	          href = Backbone.history.location.origin + '/' + Backbone.history.location.pathname.split('/')[1] + '/album/' + albumModel.get('id');
	          return Player.Controller.playByCollection(collection, null, href);
	        });
	        return albumTracks.fail(function(err) {
	          return console.log(err);
	        });
	      }
	    };

	    controller.prototype.removeAlbumFromFavorite = function(id) {
	      var album, favorites;
	      favorites = app.request('get:favorite:albums');
	      album = favorites.findWhere({
	        id: id
	      });
	      if (album) {
	        album.destroy();
	        return Notification.Controller.show({
	          message: "Альбом «" + (album.get('title')) + "» удален из медиатеки",
	          type: 'success',
	          time: 3000
	        });
	      }
	    };

	    controller.prototype.addAlbumToFavorite = function(album) {
	      var favorites, model;
	      favorites = app.request('get:favorite:albums');
	      model = album.toJSON();
	      if (!model.favourites || model.favourites.sort === void 0) {
	        model.favourites = {};
	        model.favourites.sort = 10000;
	      }
	      favorites.create(model);
	      app.GAPageAction('Добавление альбома в избранное', album.get('title') + ' - ' + location.href);
	      return Notification.Controller.show({
	        message: "Альбом «" + (album.get('title')) + "» добавлен в медиатеку",
	        type: 'success',
	        time: 3000
	      });
	    };

	    controller.prototype.openOptions = function(item) {
	      var view;
	      view = new Album.Options({
	        model: item.model
	      });
	      if (item.optionsRegion) {
	        return item.optionsRegion.show(view);
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Album.Controller = new controller;
	  _.extend(Album, Model);
	  return Album;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(28), __webpack_require__(30), __webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player, Update, Loader, Track) {
	  var Album;
	  Album = {};
	  Album.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.template = '#album-item-tpl';

	    Item.prototype.className = 'album-item';

	    Item.prototype.onRender = function() {
	      var img, link;
	      img = new Image;
	      $(img).on('load.img', (function(_this) {
	        return function() {
	          $(img).addClass('album-bg').hide().appendTo(_this.$('.controll')).fadeIn(300);
	          return $(img).off('load.img');
	        };
	      })(this));
	      link = app.createImgLink(this.model.get('img'), '300x300');
	      return img.src = link;
	    };

	    Item.prototype.regions = {
	      optionsRegion: '.options'
	    };

	    Item.prototype.id = function() {
	      return this.model.get('id');
	    };

	    Item.prototype.events = {
	      'click .player-controll': function(e) {
	        e.stopPropagation();
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          return Album.Controller.Play(this.model);
	        }
	      },
	      'mouseenter': function() {
	        var favorites, item;
	        favorites = app.request('get:favorite:albums');
	        if (favorites.promise) {
	          favorites.done((function(_this) {
	            return function(collection) {
	              var item;
	              item = collection.some(function(item) {
	                return item.get('id') === _this.model.get('id');
	              });
	              if (item) {
	                _this.model.set('inFavorite', true);
	                return _this.$('.move').removeClass('add').addClass('delete');
	              } else {
	                _this.model.set('inFavorite', false);
	                return _this.$('.move').removeClass('delete').addClass('add');
	              }
	            };
	          })(this));
	          return favorites.fail(function(err) {
	            return console.log(arguments);
	          });
	        } else {
	          item = favorites.some((function(_this) {
	            return function(item) {
	              return item.get('id') === _this.model.get('id');
	            };
	          })(this));
	          if (item) {
	            this.model.set('inFavorite', true);
	            return this.$('.move').removeClass('add').addClass('delete');
	          } else {
	            this.model.set('inFavorite', false);
	            return this.$('.move').removeClass('delete').addClass('add');
	          }
	        }
	      },
	      'click .controll': function() {
	        return app.trigger('show:album', this.model.get('id'), this.model);
	      },
	      'click .info .title': function() {
	        return app.trigger('show:album', this.model.get('id'), this.model);
	      },
	      'click .info .artist': function() {
	        return app.trigger('show:artist', this.model.get('artistid'));
	      },
	      'click .move': function(e) {
	        return e.stopPropagation();
	      },
	      'click .options': function(e) {
	        e.stopPropagation();
	        return Album.Controller.openOptions(this);
	      },
	      'click .add': function() {
	        return this.addToAlbum();
	      },
	      'click .delete': function() {
	        return this.removeFromAlbum();
	      },
	      'click .to-artist': function() {
	        return app.trigger('show:artist', this.model.get('artistid'));
	      }
	    };

	    Item.prototype.addToAlbum = function() {
	      $('.options-wrapper .add').removeClass('add').addClass('delete').text('Удалить');
	      Album.Controller.addAlbumToFavorite(this.model);
	      this.model.set('inFavorite', true);
	      this.$('.move').removeClass('add').addClass('delete');
	      return false;
	    };

	    Item.prototype.removeFromAlbum = function() {
	      $('.options-wrapper .delete').removeClass('delete').addClass('add').text('Сохранить');
	      Album.Controller.removeAlbumFromFavorite(this.model.get('id'));
	      this.model.set('inFavorite', false);
	      this.$('.move').removeClass('delete').addClass('add');
	      return false;
	    };

	    return Item;

	  })(Marionette.LayoutView);
	  Album.InlineItem = (function(superClass) {
	    extend(InlineItem, superClass);

	    function InlineItem() {
	      return InlineItem.__super__.constructor.apply(this, arguments);
	    }

	    InlineItem.prototype.template = '#inline-album-tpl';

	    InlineItem.prototype.className = 'album-item';

	    InlineItem.prototype.regions = {
	      content: '.body .albums-tracks'
	    };

	    InlineItem.prototype.events = {
	      'click .body': function(e) {
	        return e.isPropagationStopped();
	      },
	      'click .shuffle-tracks': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = this.$('.tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      },
	      'click': 'showAlbumContent'
	    };

	    InlineItem.prototype.showAlbumContent = function() {
	      var album, fetchTracks, loader, openAlbum;
	      album = this;
	      openAlbum = function(album, tracks) {
	        var collection, tracksView;
	        album.$el.removeClass('disable').addClass('open');
	        collection = new Backbone.Collection(tracks);
	        collection.each(function(item) {
	          item.set('uniq', album.model.get('id') + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ]);
	        });
	        tracksView = new Track.ListView({
	          collection: collection
	        });
	        album.content.show(tracksView);
	        return app.checkActiveTrack();
	      };
	      if (!album.$el.hasClass('disable')) {
	        if (album.$el.hasClass('open')) {
	          album.content.empty();
	          album.$el.removeClass('open');
	          return;
	        }
	        album.$el.addClass('disable');
	        loader = new Loader.View({
	          styles: {
	            'margin': '20px auto'
	          }
	        });
	        album.content.show(loader);
	        if (album.model.has('tracks')) {
	          return openAlbum(album, album.model.get('tracks'));
	        } else {
	          fetchTracks = album.model.fetch();
	          fetchTracks.done(function() {
	            return openAlbum(album, album.model.get('tracks'));
	          });
	          return fetchTracks.fail(function() {
	            var callback, update;
	            album.$el.removeClass('disable').addClass('open');
	            callback = function() {
	              return showAlbumContent(album);
	            };
	            update = new Update.View({
	              styles: {
	                margin: '20px auto'
	              },
	              callback: callback
	            });
	            return album.content.show(update);
	          });
	        }
	      }
	    };

	    return InlineItem;

	  })(Marionette.LayoutView);
	  Album.Options = (function(superClass) {
	    extend(Options, superClass);

	    function Options() {
	      return Options.__super__.constructor.apply(this, arguments);
	    }

	    Options.prototype.template = '#album-item-options-tpl';

	    Options.prototype.className = 'options-wrapper';

	    Options.prototype.events = {
	      'mouseleave': 'closeOptions',
	      'click .options-overlay': 'closeOptions'
	    };

	    Options.prototype.closeOptions = function(e) {
	      e.stopPropagation();
	      return this.destroy();
	    };

	    return Options;

	  })(Marionette.ItemView);
	  Album.ListView = (function(superClass) {
	    extend(ListView, superClass);

	    function ListView() {
	      return ListView.__super__.constructor.apply(this, arguments);
	    }

	    ListView.prototype.childView = Album.Item;

	    ListView.prototype.className = 'albums-wrapper';

	    ListView.prototype.onBeforeRenderCollection = function() {
	      return this.attachHtml = function(collectionView, itemView, index) {
	        return collectionView.$el.append(itemView.el).append(' ');
	      };
	    };

	    return ListView;

	  })(Marionette.CollectionView);
	  Album.InlineListView = (function(superClass) {
	    extend(InlineListView, superClass);

	    function InlineListView() {
	      return InlineListView.__super__.constructor.apply(this, arguments);
	    }

	    InlineListView.prototype.childView = Album.InlineItem;

	    InlineListView.prototype.className = 'inline-albums-wrapper';

	    InlineListView.prototype.onRender = function() {
	      if (this.collection.length !== 0) {
	        return $('.artist-layout-title.albums-title').show();
	      }
	    };

	    return InlineListView;

	  })(Marionette.CollectionView);
	  return Album;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Update;
	  __webpack_require__(29);
	  Update = {};
	  Update.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = false;

	    View.prototype.className = function() {
	      if (this.options.container) {
	        return this.options.container;
	      } else {
	        return 'update-element';
	      }
	    };

	    View.prototype.onRender = function() {
	      var container;
	      this.options.styles = this.options.styles || {};
	      if (this.options.container) {
	        container = $('<div class="update-element"></div>');
	        this.$el.append(container);
	      }
	      if (this.options.container) {
	        return this.$('.update-element').css(this.options.styles);
	      } else {
	        return this.$el.css(this.options.styles);
	      }
	    };

	    View.prototype.events = {
	      'click': function() {
	        return this.options.callback();
	      }
	    };

	    return View;

	  })(Marionette.ItemView);
	  return Update;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 29 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Loader;
	  __webpack_require__(31);
	  Loader = {};
	  Loader.View = (function(superClass) {
	    extend(View, superClass);

	    function View() {
	      return View.__super__.constructor.apply(this, arguments);
	    }

	    View.prototype.template = false;

	    View.prototype.className = function() {
	      if (this.options.container) {
	        return this.options.container;
	      } else {
	        return 'loader';
	      }
	    };

	    View.prototype.onRender = function() {
	      var container;
	      this.options.styles = this.options.styles || {};
	      if (this.options.container) {
	        container = $('<div class="loader"></div>');
	        this.$el.append(container);
	      }
	      if (this.options.container) {
	        return this.$('.loader').css(this.options.styles);
	      } else {
	        return this.$el.css(this.options.styles);
	      }
	    };

	    return View;

	  })(Marionette.ItemView);
	  return Loader;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 31 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Album;
	  Album = {};
	  Album.Model = (function(superClass) {
	    extend(Model, superClass);

	    function Model() {
	      return Model.__super__.constructor.apply(this, arguments);
	    }

	    Model.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/catalogue/getAlbum", "?albumid=" + this.id);
	    };

	    Model.prototype.parse = function(model) {
	      if (model.album) {
	        model.album.album = true;
	        return model.album;
	      } else {
	        model.album = true;
	        return model;
	      }
	    };

	    Model.prototype.defaults = {
	      "duration": "",
	      "artist": "",
	      "title": "",
	      "track_count": "",
	      "img": ""
	    };

	    return Model;

	  })(Backbone.Model);
	  Album.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Album.Model;

	    return Collection;

	  })(Backbone.Collection);
	  return Album;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 33 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(35)], __WEBPACK_AMD_DEFINE_RESULT__ = function(View) {
	  var controller;
	  __webpack_require__(36);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    return controller;

	  })(Marionette.Controller);
	  View.Controller = new controller;
	  return View;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Artist;
	  Artist = {};
	  Artist.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.events = {
	      'click': function() {
	        return app.trigger('show:artist', this.model.get('id'));
	      }
	    };

	    return Item;

	  })(Marionette.LayoutView);
	  return Artist;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 36 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(38), __webpack_require__(39), __webpack_require__(1), __webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Playlist, Model, Player, Notification) {
	  var controller;
	  __webpack_require__(40);
	  controller = (function(superClass) {
	    extend(controller, superClass);

	    function controller() {
	      return controller.__super__.constructor.apply(this, arguments);
	    }

	    controller.prototype.Play = function(playlistModel) {
	      var collection, playlistTracks;
	      app.playedAlbum = 'playlist-' + playlistModel.get('id');
	      if (playlistModel.has('tracks')) {
	        if (playlistModel.get('tracks').length === 0) {
	          Notification.Controller.show({
	            message: 'Плейлист пустой',
	            type: 'error',
	            time: 3000
	          });
	          return false;
	        }
	        collection = new Backbone.Collection(playlistModel.get('tracks'));
	        collection.each(function(item) {
	          item.set('uniq', 'album' + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toArt,
	              "class": 'options-open-artist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ]);
	        });
	        return Player.Controller.playByCollection(collection);
	      } else {
	        playlistTracks = playlistModel.fetch();
	        playlistTracks.done(function() {
	          var href;
	          collection = new Backbone.Collection(playlistModel.get('tracks'));
	          collection.each(function(item) {
	            item.set('uniq', 'album' + item.id);
	            return item.set('optionsList', [
	              {
	                title: app.locale.options.addTPl,
	                "class": 'add-to-playlist'
	              }, {
	                title: app.locale.options.toArt,
	                "class": 'options-open-artist'
	              }, {
	                title: app.locale.options.toAlb,
	                "class": 'options-open-album'
	              }
	            ]);
	          });
	          href = Backbone.history.location.origin + '/' + Backbone.history.location.pathname.split('/')[1] + '/playlist/' + playlistModel.get('id');
	          return Player.Controller.playByCollection(collection, null, href);
	        });
	        return playlistTracks.fail(function(err) {
	          return console.log(err);
	        });
	      }
	    };

	    return controller;

	  })(Marionette.Controller);
	  Playlist.Controller = new controller;
	  _.extend(Playlist, Model);
	  return Playlist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 38 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(1), __webpack_require__(30), __webpack_require__(28), __webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Player, Loader, Update, Track) {
	  var Playlist;
	  Playlist = {};
	  Playlist.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.id = function() {
	      return 'playlist-' + this.model.get('id');
	    };

	    Item.prototype.events = {
	      'click .player-controll': function(e) {
	        e.stopPropagation();
	        if (this.$el.hasClass('active')) {
	          if (this.$el.hasClass('play')) {
	            return Player.Controller.play();
	          } else {
	            return Player.Controller.pause();
	          }
	        } else {
	          return Playlist.Controller.Play(this.model);
	        }
	      },
	      'click .controll': function() {
	        return app.trigger('show:playlist', this.model.get('id'), this.model);
	      },
	      'click .info .title': function() {},
	      'click .info .artist': function() {}
	    };

	    return Item;

	  })(Marionette.ItemView);
	  Playlist.InlineItem = (function(superClass) {
	    extend(InlineItem, superClass);

	    function InlineItem() {
	      return InlineItem.__super__.constructor.apply(this, arguments);
	    }

	    InlineItem.prototype.template = '#inline-playlist-tpl';

	    InlineItem.prototype.className = 'playlist-item';

	    InlineItem.prototype.regions = {
	      content: '.body .playlist-tracks'
	    };

	    InlineItem.prototype.events = {
	      'click .body': function(e) {
	        return e.isPropagationStopped();
	      },
	      'click .shuffle-tracks': function() {
	        var random, trackContainer, tracks, tracksLength;
	        trackContainer = this.$('.tracks-wrapper');
	        tracks = trackContainer.find('.track-item').not('.disable');
	        tracksLength = tracks.length;
	        random = app.RandInt(0, tracksLength - 1);
	        return tracks.eq(random).click();
	      },
	      'click': 'showContent'
	    };

	    InlineItem.prototype.showContent = function() {
	      var fetchTracks, loader, openPlaylist, playlist;
	      playlist = this;
	      openPlaylist = function(playlist, tracks) {
	        var collection, tracksView;
	        playlist.$el.removeClass('disable').addClass('open');
	        collection = new Backbone.Collection(tracks);
	        collection.each(function(item) {
	          item.set('uniq', playlist.model.get('id') + item.id);
	          return item.set('optionsList', [
	            {
	              title: app.locale.options.addTPl,
	              "class": 'add-to-playlist'
	            }, {
	              title: app.locale.options.toAlb,
	              "class": 'options-open-album'
	            }
	          ]);
	        });
	        tracksView = new Track.ListView({
	          collection: collection
	        });
	        playlist.content.show(tracksView);
	        return app.checkActiveTrack();
	      };
	      if (!playlist.$el.hasClass('disable')) {
	        if (playlist.$el.hasClass('open')) {
	          playlist.content.empty();
	          playlist.$el.removeClass('open');
	          return;
	        }
	        playlist.$el.addClass('disable');
	        loader = new Loader.View({
	          styles: {
	            'margin': '20px auto'
	          }
	        });
	        playlist.content.show(loader);
	        if (playlist.model.has('tracks')) {
	          return openPlaylist(playlist, playlist.model.get('tracks'));
	        } else {
	          fetchTracks = playlist.model.fetch();
	          fetchTracks.done(function() {
	            return openPlaylist(playlist, playlist.model.get('tracks'));
	          });
	          return fetchTracks.fail(function() {
	            var callback, update;
	            playlist.$el.removeClass('disable').addClass('open');
	            callback = function() {
	              return showAlbumContent(playlist);
	            };
	            update = new Update.View({
	              styles: {
	                margin: '20px auto'
	              },
	              callback: callback
	            });
	            return playlist.content.show(update);
	          });
	        }
	      }
	    };

	    return InlineItem;

	  })(Marionette.LayoutView);
	  Playlist.ListView = (function(superClass) {
	    extend(ListView, superClass);

	    function ListView() {
	      return ListView.__super__.constructor.apply(this, arguments);
	    }

	    ListView.prototype.childView = Playlist.Item;

	    return ListView;

	  })(Marionette.CollectionView);
	  Playlist.InlineListView = (function(superClass) {
	    extend(InlineListView, superClass);

	    function InlineListView() {
	      return InlineListView.__super__.constructor.apply(this, arguments);
	    }

	    InlineListView.prototype.childView = Playlist.InlineItem;

	    InlineListView.prototype.className = 'inline-playlists-wrapper';

	    return InlineListView;

	  })(Marionette.CollectionView);
	  return Playlist;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 39 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var Playlist;
	  Playlist = {};
	  Playlist.Model = (function(superClass) {
	    extend(Model, superClass);

	    function Model() {
	      return Model.__super__.constructor.apply(this, arguments);
	    }

	    Model.prototype.urlRoot = function() {
	      return app.getRequestUrl("/playlist/get?playlistid=" + this.id + "&items_on_page=1000");
	    };

	    Model.prototype.defaults = {
	      "duration": "",
	      "artist": "",
	      "title": ""
	    };

	    return Model;

	  })(Backbone.Model);
	  Playlist.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Playlist.Model;

	    return Collection;

	  })(Backbone.Collection);
	  return Playlist;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 40 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 41 */
/***/ function(module, exports, __webpack_require__) {

	var jade = __webpack_require__(4);

	module.exports = function template(locals) {
	var buf = [];
	var jade_mixins = {};
	var jade_interp;
	;var locals_for_with = (locals || {});(function (app) {
	buf.push("<div class=\"profile\"></div><div class=\"sidebar-body\"><div class=\"search\"><input type=\"text\"" + (jade.attr("placeholder", "" + (app.locale.search.search) + "", true, true)) + "><i class=\"icon\"></i><i class=\"close\">отмена</i></div><div class=\"sidebar-content-scroll\"><div class=\"sidebar-search-container\"></div><div class=\"sidebar-content-wrapp\"><div class=\"main-nav\"></div><div class=\"user-nav\"></div><div class=\"apps\"><p class=\"title\">" + (jade.escape((jade_interp = app.locale.menu.apps) == null ? '' : jade_interp)) + "</p><ul><li class=\"app-link\"><a target=\"_blank\" href=\"https://itunes.apple.com/ru/app/bilajn.muzyka/id702770268?mt=8\">IOS</a></li><li class=\"app-link\"><a target=\"_blank\" href=\"https://play.google.com/store/apps/details?id=ru.beeonline.music&amp;hl=ru\">Android</a></li></ul></div></div></div></div><div class=\"sidebar-overlay\"></div>");}.call(this,"app" in locals_for_with?locals_for_with.app:typeof app!=="undefined"?app:undefined));;return buf.join("");
	}

/***/ },
/* 42 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ },
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */,
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(CustomTrack) {
	  var API, TrackList, initializeNewTracks;
	  TrackList = {};
	  TrackList.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.parse = function(item) {
	      item.uniq = 'artist-' + item.id;
	      item.optionsList = [
	        {
	          title: app.locale.options.addTPl,
	          "class": 'add-to-playlist'
	        }, {
	          title: app.locale.options.toArt,
	          "class": 'options-open-artist',
	          artist: true
	        }, {
	          title: app.locale.options.toAlb,
	          "class": 'options-open-album'
	        }
	      ];
	      return item;
	    };

	    Item.prototype.urlRoot = '/track';

	    return Item;

	  })(CustomTrack.Model);
	  TrackList.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = TrackList.Item;

	    Items.prototype.url = app.getRequestUrl("/compatibility/catalogue/getBeelineContent");

	    Items.prototype.parse = function(collection) {
	      return collection.items;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeNewTracks = function() {
	    var fechTracks, trackList;
	    if (TrackList.NewListIsLoad) {
	      return TrackList.NewListDefer.promise();
	    }
	    TrackList.NewListIsLoad = true;
	    trackList = new TrackList.Items();
	    TrackList.NewListDefer = $.Deferred();
	    fechTracks = trackList.fetch({
	      data: {
	        type: 'tracks',
	        sort: 'date',
	        page: 1,
	        countitems: 90
	      }
	    });
	    if (fechTracks) {
	      fechTracks.done(function() {
	        TrackList.NewList = trackList;
	        return TrackList.NewListDefer.resolveWith(fechTracks, [TrackList.NewList]);
	      });
	      fechTracks.fail(function() {
	        TrackList.NewListIsLoad = false;
	        return TrackList.NewListDefer.rejectWith(fechTracks, arguments);
	      });
	      return TrackList.NewListDefer.promise();
	    }
	  };
	  API = {
	    getNewTracks: function() {
	      if (TrackList.NewList === void 0) {
	        return initializeNewTracks();
	      }
	      return TrackList.NewList;
	    }
	  };
	  app.reqres.setHandler('get:new:tracks', function() {
	    return API.getNewTracks();
	  });
	  return TrackList;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */
/***/ function(module, exports, __webpack_require__) {

	var map = {
		"./agreement_model": 91,
		"./agreement_model.coffee": 91,
		"./album_list": 92,
		"./album_list.coffee": 92,
		"./artist_model": 93,
		"./artist_model.coffee": 93,
		"./charts_list": 94,
		"./charts_list.coffee": 94,
		"./clipList": 95,
		"./clipList.coffee": 95,
		"./favorite_albums": 96,
		"./favorite_albums.coffee": 96,
		"./favorite_play_list": 97,
		"./favorite_play_list.coffee": 97,
		"./favorite_track_list": 98,
		"./favorite_track_list.coffee": 98,
		"./feed": 101,
		"./feed.coffee": 101,
		"./local": 102,
		"./local.coffee": 102,
		"./log": 7,
		"./log.coffee": 7,
		"./menu_list": 103,
		"./menu_list.coffee": 103,
		"./playlists_list": 104,
		"./playlists_list.coffee": 104,
		"./profile_menu": 105,
		"./profile_menu.coffee": 105,
		"./recomend_page": 106,
		"./recomend_page.coffee": 106,
		"./search_list": 107,
		"./search_list.coffee": 107,
		"./slider_list": 108,
		"./slider_list.coffee": 108,
		"./tab_list": 109,
		"./tab_list.coffee": 109,
		"./track_list": 65,
		"./track_list.coffee": 65,
		"./user-notify": 110,
		"./user-notify.coffee": 110,
		"./user_model": 111,
		"./user_model.coffee": 111
	};
	function webpackContext(req) {
		return __webpack_require__(webpackContextResolve(req));
	};
	function webpackContextResolve(req) {
		return map[req] || (function() { throw new Error("Cannot find module '" + req + "'.") }());
	};
	webpackContext.keys = function webpackContextKeys() {
		return Object.keys(map);
	};
	webpackContext.resolve = webpackContextResolve;
	module.exports = webpackContext;
	webpackContext.id = 90;


/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Model, initializeUser;
	  Model = {};
	  Model.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = '/agreement';

	    Item.prototype.parse = function(model) {
	      return model;
	    };

	    return Item;

	  })(Backbone.Model);
	  initializeUser = function() {
	    var defer, fechModel;
	    Model.Agreement = new Model.Item();
	    defer = $.Deferred();
	    fechModel = Model.Agreement.fetch();
	    if (fechModel) {
	      fechModel.done(function(res) {
	        return defer.resolveWith(fechModel, [Model.Agreement]);
	      });
	      fechModel.fail(function() {
	        return defer.rejectWith(fechModel, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  API = {
	    getAgreement: function() {
	      if (Model.Agreement === void 0) {
	        return initializeUser();
	      }
	      return Model.Agreement;
	    }
	  };
	  return app.reqres.setHandler('get:agreement', function() {
	    return API.getAgreement();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 92 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(setImmediate) {var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, AlbumList, initializeNewAlbums, initializeSingleAlbum;
	  AlbumList = {};
	  AlbumList.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/catalogue/getAlbum", "?albumid=" + this.id);
	    };

	    Item.prototype.specifiedIdKey = 'albumid';

	    Item.prototype.parse = function(model) {
	      if (model.album) {
	        model.album.album = true;
	        return model.album;
	      } else {
	        model.album = true;
	        return model;
	      }
	    };

	    return Item;

	  })(Backbone.Model);
	  AlbumList.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = AlbumList.Item;

	    Items.prototype.url = app.getRequestUrl("/compatibility/catalogue/getBeelineContent");

	    Items.prototype.parse = function(collection) {
	      return collection.items;
	    };

	    Items.prototype.fetch = function(opts) {
	      var genre;
	      genre = app.request('get:local:genries');
	      if (genre.length) {
	        opts.data = opts.data || {};
	        opts.data.genre = genre.toJSON();
	      }
	      return Backbone.Collection.prototype.fetch.apply(this, [opts]);
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeNewAlbums = function() {
	    var albums, fechAlbums;
	    if (AlbumList.NewListIsLoad) {
	      return AlbumList.NewListDefer.promise();
	    }
	    AlbumList.NewListIsLoad = true;
	    albums = new AlbumList.Items();
	    AlbumList.NewListDefer = $.Deferred();
	    fechAlbums = albums.fetch({
	      data: {
	        type: 'albums',
	        sort: 'date',
	        page: 1,
	        countitems: 60
	      }
	    });
	    if (fechAlbums) {
	      fechAlbums.done(function() {
	        AlbumList.NewList = albums;
	        AlbumList.NewList.Type = 'new';
	        return AlbumList.NewListDefer.resolveWith(fechAlbums, [AlbumList.NewList]);
	      });
	      fechAlbums.fail(function() {
	        AlbumList.NewListIsLoad = false;
	        return AlbumList.NewListDefer.rejectWith(fechAlbums, arguments);
	      });
	      return AlbumList.NewListDefer.promise();
	    }
	  };
	  initializeSingleAlbum = function(id) {
	    var defer, fechAlbum;
	    AlbumList.SingleAlbum = new AlbumList.Item({
	      id: id
	    });
	    defer = $.Deferred();
	    fechAlbum = AlbumList.SingleAlbum.fetch();
	    if (fechAlbum) {
	      fechAlbum.done(function(res) {
	        if (res.code === 0) {
	          AlbumList.AlbumId = id;
	          return defer.resolveWith(fechAlbum, [AlbumList.SingleAlbum]);
	        } else {
	          return defer.rejectWith(fechAlbum, [res]);
	        }
	      });
	      fechAlbum.fail(function() {
	        return defer.rejectWith(fechAlbum, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  API = {
	    getNewAlbums: function() {
	      if (AlbumList.NewList === void 0) {
	        return initializeNewAlbums();
	      }
	      return AlbumList.NewList;
	    },
	    getAlbum: function(id) {
	      var defer;
	      if (AlbumList.AlbumId !== id) {
	        return initializeSingleAlbum(id);
	      }
	      defer = $.Deferred();
	      setImmediate(function() {
	        return defer.resolve(AlbumList.SingleAlbum);
	      });
	      return defer.promise();
	    }
	  };
	  app.reqres.setHandler('get:new:albums', function() {
	    return API.getNewAlbums();
	  });
	  app.reqres.setHandler('get:single:album', function(id) {
	    return API.getAlbum(id);
	  });
	  app.reqres.setHandler('get:temp:albums', function(collection) {
	    if (collection != null) {
	      AlbumList.TempAlbums = new AlbumList.Items(collection.toJSON());
	      if (AlbumList.TempCollection != null) {
	        AlbumList.TempCollection.off('reset');
	      }
	      AlbumList.TempCollection = collection;
	      AlbumList.TempCollection.on('reset', function() {
	        if (AlbumList.TempCollection.Type === 'new') {
	          return app.Main.Content.New.Controller.showAlbumsOne(AlbumList.TempCollection);
	        } else {
	          return app.Main.Content.Recomend.Controller.showAlbumsOne(AlbumList.TempCollection);
	        }
	      });
	    }
	    return AlbumList.TempAlbums;
	  });
	  return app.reqres.setHandler('re:initialize:albums', function() {
	    if (AlbumList.NewList) {
	      return AlbumList.NewList.fetch({
	        data: {
	          type: 'new',
	          page: 1,
	          count: 60
	        },
	        reset: true
	      });
	    }
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(99).setImmediate))

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Model, initializeArtist, initializeArtistAlbums, initializeArtistTracks, initializeArtistVideo;
	  Model = {};
	  Model.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = function() {
	      return app.getRequestUrl("/artist/get");
	    };

	    Item.prototype.parse = function(model) {
	      return model.artist;
	    };

	    return Item;

	  })(Backbone.Model);
	  Model.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.parse = function(item) {
	      item.uniq = 'artist-' + item.id;
	      return item;
	    };

	    return Track;

	  })(Backbone.Model);
	  Model.Tracks = (function(superClass) {
	    extend(Tracks, superClass);

	    function Tracks() {
	      return Tracks.__super__.constructor.apply(this, arguments);
	    }

	    Tracks.prototype.model = Model.Track;

	    Tracks.prototype.url = app.getRequestUrl("/compatibility/catalogue/getBeelineContent", "?type=tracks&sort=rating&countitems=10");

	    Tracks.prototype.parse = function(collection) {
	      return collection.items;
	    };

	    return Tracks;

	  })(Backbone.Collection);
	  Model.Album = (function(superClass) {
	    extend(Album, superClass);

	    function Album() {
	      return Album.__super__.constructor.apply(this, arguments);
	    }

	    Album.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/catalogue/getAlbum", "?albumid=" + this.id);
	    };

	    Album.prototype.parse = function(model) {
	      if (model.album) {
	        model.album.album = true;
	        return model.album;
	      } else {
	        model.album = true;
	        return model;
	      }
	    };

	    return Album;

	  })(Backbone.Model);
	  Model.Albums = (function(superClass) {
	    extend(Albums, superClass);

	    function Albums() {
	      return Albums.__super__.constructor.apply(this, arguments);
	    }

	    Albums.prototype.model = Model.Album;

	    Albums.prototype.url = app.getRequestUrl("/beelineMusicV1/catalog/getAlbums", "?sort=rating");

	    Albums.prototype.parse = function(collection) {
	      return collection.items;
	    };

	    return Albums;

	  })(Backbone.Collection);
	  Model.ArtistVideoModel = (function(superClass) {
	    extend(ArtistVideoModel, superClass);

	    function ArtistVideoModel() {
	      return ArtistVideoModel.__super__.constructor.apply(this, arguments);
	    }

	    ArtistVideoModel.prototype.parse = function(item) {
	      item.id = item['id']['videoId'];
	      item.snippet.resourceId = {};
	      item.snippet.resourceId.videoId = item.id;
	      return item;
	    };

	    return ArtistVideoModel;

	  })(Backbone.Model);
	  Model.ArtistVideoItems = (function(superClass) {
	    extend(ArtistVideoItems, superClass);

	    function ArtistVideoItems() {
	      return ArtistVideoItems.__super__.constructor.apply(this, arguments);
	    }

	    ArtistVideoItems.prototype.model = Model.ArtistVideoModel;

	    ArtistVideoItems.prototype.url = '/artistVideo';

	    return ArtistVideoItems;

	  })(Backbone.Collection);
	  initializeArtistVideo = function(q) {
	    var defer, fech;
	    if (Model.VideoLoadProcess) {
	      Model.VideoLoadProcess.abort();
	    }
	    Model.ArtistVideoList = new Model.ArtistVideoItems();
	    defer = $.Deferred();
	    Model.VideoProcess = fech = Model.ArtistVideoList.fetch({
	      data: {
	        q: q
	      }
	    });
	    if (fech) {
	      fech.done(function() {
	        Model.Artistq = q;
	        return defer.resolveWith(fech, [Model.ArtistVideoList]);
	      });
	      fech.fail(function() {
	        return defer.rejectWith(fech, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  initializeArtistTracks = function(options) {
	    var defer, fechModel;
	    if (options == null) {
	      options = {};
	    }
	    Model.ArtistTracks = new Model.Tracks;
	    defer = $.Deferred();
	    Model.TracksLoadProcess = fechModel = Model.ArtistTracks.fetch(_.omit(options, 'success', 'error'));
	    if (fechModel) {
	      fechModel.done((function(_this) {
	        return function() {
	          defer.resolveWith(fechModel, [Model.ArtistTracks]);
	          return Model.artistTrackId = options.data.artist;
	        };
	      })(this));
	      fechModel.fail(function() {
	        return defer.rejectWith(fechModel, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  initializeArtistAlbums = function(options) {
	    var ArtistAlbums, defer, fechModel;
	    if (options == null) {
	      options = {};
	    }
	    ArtistAlbums = new Model.Albums;
	    defer = $.Deferred();
	    Model.AlbumsLoadProcess = fechModel = ArtistAlbums.fetch(_.omit(options, 'success', 'error'));
	    if (fechModel) {
	      fechModel.done((function(_this) {
	        return function() {
	          defer.resolveWith(fechModel, [ArtistAlbums]);
	          return Model.artistAlbumId = options.data.artist;
	        };
	      })(this));
	      fechModel.fail(function() {
	        return defer.rejectWith(fechModel, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  initializeArtist = function(id) {
	    var defer, fechModel;
	    Model.Artist = new Model.Item;
	    defer = $.Deferred();
	    Model.LoadProcess = fechModel = Model.Artist.fetch({
	      data: {
	        'artistid': id
	      }
	    });
	    if (fechModel) {
	      fechModel.done((function(_this) {
	        return function(res) {
	          defer.resolveWith(fechModel, [Model.Artist]);
	          return Model.artistId = id;
	        };
	      })(this));
	      fechModel.fail(function() {
	        return defer.rejectWith(fechModel, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  API = {
	    getArtist: function(id) {
	      if (Model.artistId && Model.artistId === id) {
	        return Model.Artist;
	      } else {
	        return initializeArtist(id);
	      }
	    },
	    getArtistTracks: function(options) {
	      if (Model.artistTrackId && Model.artistTrackId === options.data.artist) {
	        return Model.ArtistTracks;
	      } else {
	        return initializeArtistTracks(options);
	      }
	    },
	    getArtistAlbums: function(options) {
	      return initializeArtistAlbums(options);
	    },
	    getArtistVideos: function(q) {
	      if (Model.Artistq !== q) {
	        return initializeArtistVideo(q);
	      }
	      return Model.ArtistVideoList;
	    }
	  };
	  app.reqres.setHandler('get:artist', function(id) {
	    return API.getArtist(id);
	  });
	  app.reqres.setHandler('get:artist:tracks', function(options) {
	    return API.getArtistTracks(options);
	  });
	  app.reqres.setHandler('get:artist:albums', function(options) {
	    return API.getArtistAlbums(options);
	  });
	  app.reqres.setHandler('get:artist:videos', function(q) {
	    return API.getArtistVideos(q);
	  });
	  return app.reqres.setHandler('stop:artist:request', function(type) {
	    if (type) {
	      switch (type) {
	        case 'tracks':
	          if (Model.TracksLoadProcess) {
	            return Model.TracksLoadProcess.abort();
	          }
	          break;
	        case 'albums':
	          if (Model.AlbumsLoadProcess) {
	            return Model.AlbumsLoadProcess.abort();
	          }
	          break;
	        case 'artist':
	          if (Model.LoadProcess) {
	            return Model.LoadProcess.abort();
	          }
	          break;
	        case 'video':
	          if (Model.VideoLoadProcess) {
	            return Model.VideoLoadProcess.abort();
	          }
	      }
	    }
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 94 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Charts, initializeCharts;
	  Charts = {};
	  Charts.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    return Item;

	  })(Backbone.Model);
	  Charts.List = (function(superClass) {
	    extend(List, superClass);

	    function List() {
	      return List.__super__.constructor.apply(this, arguments);
	    }

	    List.prototype.model = Charts.Item;

	    List.prototype.url = '/charts/list';

	    List.prototype.parse = function(collection) {
	      return collection.charts;
	    };

	    return List;

	  })(Backbone.Collection);
	  initializeCharts = function() {
	    var feedList, fetch;
	    if (Charts.ListIsLoad) {
	      return Charts.ListDefer.promise();
	    }
	    Charts.ListIsLoad = true;
	    feedList = new Charts.List;
	    Charts.ListDefer = $.Deferred();
	    fetch = feedList.fetch();
	    if (fetch) {
	      fetch.done(function() {
	        Charts.NewList = feedList;
	        return Charts.ListDefer.resolveWith(fetch, [Charts.NewList]);
	      });
	      fetch.fail(function() {
	        Charts.ListIsLoad = false;
	        return Charts.ListDefer.rejectWith(fetch, arguments);
	      });
	      return Charts.ListDefer.promise();
	    }
	  };
	  API = {
	    getCharts: function() {
	      if (Charts.NewList === void 0) {
	        return initializeCharts();
	      }
	      return Charts.NewList;
	    }
	  };
	  return app.reqres.setHandler('get:charts', function() {
	    return API.getCharts();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 95 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Video, initializeVideos;
	  Video = {};
	  Video.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.url = '/clips';

	    Items.prototype.parse = function(collection) {
	      return collection.items;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeVideos = function() {
	    var fech, videoList;
	    if (Video.isLoad) {
	      return Video.defer.promise();
	    }
	    videoList = new Video.Items();
	    Video.defer = $.Deferred();
	    fech = videoList.fetch();
	    if (fech) {
	      fech.done(function(res) {
	        Video.List = videoList;
	        Video.defer.resolveWith(fech, [Video.List]);
	        if (res.nextPageToken) {
	          return Video.List.fetch({
	            data: {
	              token: res.nextPageToken
	            },
	            remove: false
	          });
	        }
	      });
	      fech.fail(function() {
	        return Video.defer.rejectWith(fech, arguments);
	      });
	      return Video.defer.promise();
	    }
	  };
	  API = {
	    getVideos: function() {
	      if (Video.List === void 0) {
	        return initializeVideos();
	      }
	      return Video.List;
	    }
	  };
	  app.reqres.setHandler('get:videos', function() {
	    return API.getVideos();
	  });
	  return API.getVideos();
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 96 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(setImmediate) {var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, FavoriteAlbums, initializeAlbums;
	  FavoriteAlbums = {};
	  FavoriteAlbums.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/catalogue/getAlbum", "?albumid=" + this.id);
	    };

	    Item.prototype.updateUrl = function() {
	      return app.getRequestUrl("/compatibility/favourites/addItem", "?contenttype=ALBM&contentid=" + this.id);
	    };

	    Item.prototype.deleteUrl = function() {
	      return app.getRequestUrl("/compatibility/favourites/removeItem", "?contenttype=ALBM&contentid=" + this.id);
	    };

	    Item.prototype.defaults = {
	      "title": "",
	      "img": ""
	    };

	    Item.prototype.parse = function(model) {
	      if (model.is_deleted) {
	        model.favourites.sort = +model.favourites.sort - 100000;
	      }
	      if (model.album) {
	        return model.album;
	      } else {
	        return model;
	      }
	    };

	    return Item;

	  })(Backbone.Model);
	  FavoriteAlbums.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = FavoriteAlbums.Item;

	    Items.prototype.url = app.getRequestUrl("/favourites/getitems", "?userid=" + app.user.uid + "&contenttype=ALBM");

	    Items.prototype.parse = function(collection) {
	      return collection.albums;
	    };

	    Items.prototype.comparator = function(item) {
	      return item.get('favourites').sort;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeAlbums = function() {
	    var favoriteAlbum, fechAlbums;
	    if (FavoriteAlbums.isLoad) {
	      return FavoriteAlbums.defer.promise();
	    }
	    FavoriteAlbums.isLoad = true;
	    favoriteAlbum = new FavoriteAlbums.Items();
	    FavoriteAlbums.defer = $.Deferred();
	    fechAlbums = favoriteAlbum.fetch();
	    if (fechAlbums) {
	      fechAlbums.done(function(res) {
	        FavoriteAlbums.List = favoriteAlbum;
	        if (res.code === 0) {
	          return FavoriteAlbums.defer.resolveWith(fechAlbums, [FavoriteAlbums.List]);
	        } else {
	          FavoriteAlbums.isLoad = false;
	          return FavoriteAlbums.defer.rejectWith(fechAlbums, arguments);
	        }
	      });
	      fechAlbums.fail(function() {
	        FavoriteAlbums.isLoad = false;
	        return FavoriteAlbums.defer.rejectWith(fechAlbums, arguments);
	      });
	      return FavoriteAlbums.defer.promise();
	    }
	  };
	  API = {
	    getAlbums: function() {
	      var defer;
	      if (FavoriteAlbums.List === void 0) {
	        return initializeAlbums();
	      }
	      defer = $.Deferred();
	      setImmediate(function() {
	        return defer.resolve(FavoriteAlbums.List);
	      });
	      return defer.promise();
	    }
	  };
	  app.reqres.setHandler('get:favorite:albums', function() {
	    return API.getAlbums();
	  });
	  return API.getAlbums();
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(99).setImmediate))

/***/ },
/* 97 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, FavoritePlaylist, initializePlaylists;
	  FavoritePlaylist = {};
	  FavoritePlaylist.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/playlist/get", "?playlistid=" + this.id + "&items_on_page=1000");
	    };

	    Item.prototype.createUrl = function() {
	      return app.getRequestUrl("/compatibility/playlist/create", "?title=" + (this.get('title')));
	    };

	    Item.prototype.deleteUrl = function() {
	      return app.getRequestUrl("/compatibility/playlist/remove", "?playlistid=" + this.id);
	    };

	    Item.prototype.defaults = {
	      "title": "",
	      "image": "",
	      "image_black": ""
	    };

	    Item.prototype.parse = function(item) {
	      if (item.playlistData) {
	        return item.playlistData;
	      }
	      return item;
	    };

	    Item.prototype.validate = function(attrs, options) {
	      if ($.trim(attrs.title) === "") {
	        return "title";
	      }
	      if (_.isArray(attrs.tracks) && attrs.tracks.length === 0) {
	        return "Нет треков";
	      }
	    };

	    return Item;

	  })(Backbone.Model);
	  FavoritePlaylist.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = FavoritePlaylist.Item;

	    Items.prototype.url = app.getRequestUrl("/compatibility/playlists/getbyusersid");

	    Items.prototype.parse = function(collection) {
	      return collection.playlists;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializePlaylists = function() {
	    var favoritePl, fechPl;
	    if (FavoritePlaylist.isLoad) {
	      return FavoritePlaylist.defer.promise();
	    }
	    FavoritePlaylist.isLoad = true;
	    favoritePl = new FavoritePlaylist.Items();
	    FavoritePlaylist.defer = $.Deferred();
	    fechPl = favoritePl.fetch();
	    fechPl.done(function(res) {
	      if (res.code === 0) {
	        FavoritePlaylist.List = favoritePl;
	        return FavoritePlaylist.defer.resolveWith(fechPl, [FavoritePlaylist.List]);
	      } else {
	        FavoritePlaylist.isLoad = false;
	        return FavoritePlaylist.defer.rejectWith(fechPl, arguments);
	      }
	    });
	    fechPl.fail(function() {
	      FavoritePlaylist.isLoad = false;
	      return FavoritePlaylist.defer.rejectWith(fechPl, arguments);
	    });
	    return FavoritePlaylist.defer.promise();
	  };
	  API = {
	    getPlaylist: function() {
	      if (FavoritePlaylist.List === void 0) {
	        return initializePlaylists();
	      }
	      return FavoritePlaylist.List;
	    }
	  };
	  app.reqres.setHandler('get:favorite:playlists', function() {
	    return API.getPlaylist();
	  });
	  return API.getPlaylist();
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 98 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(setImmediate) {var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22)], __WEBPACK_AMD_DEFINE_RESULT__ = function(CustomTrack) {
	  var API, FavoriteTracks, initializeTracks;
	  FavoriteTracks = {};
	  FavoriteTracks.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.deleteUrl = function() {
	      return app.getRequestUrl("/compatibility/favourites/removeitem", "?contentid=" + this.id + "&contenttype=TRCK");
	    };

	    Item.prototype.updateUrl = function() {
	      return app.getRequestUrl("/compatibility/favourites/additem", "?contentid=" + this.id + "&contenttype=TRCK");
	    };

	    Item.prototype.defaults = {
	      duration: ''
	    };

	    Item.prototype.parse = function(model) {
	      var data;
	      data = model;
	      if (data.is_deleted) {
	        data.favourites.sort = +data.favourites.sort - 100000;
	      }
	      data.uniq = 'user-' + model.id;
	      data.optionsList = [
	        {
	          title: app.locale.options.addTPl,
	          "class": 'add-to-playlist'
	        }, {
	          title: app.locale.options.toArt,
	          "class": 'options-open-artist'
	        }, {
	          title: app.locale.options.toAlb,
	          "class": 'options-open-album'
	        }
	      ];
	      return data;
	    };

	    return Item;

	  })(CustomTrack.Model);
	  FavoriteTracks.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = FavoriteTracks.Item;

	    Items.prototype.url = app.getRequestUrl("/compatibility/favourites/getitems", "?userid=" + app.user.uid + "&contenttype=TRCK&");

	    Items.prototype.parse = function(collection) {
	      return collection.tracks;
	    };

	    Items.prototype.comparator = function(item) {
	      return -item.get('favourites').sort;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeTracks = function() {
	    var favoriteTL, fechTracks;
	    if (FavoriteTracks.isLoaded) {
	      return FavoriteTracks.defer.promise();
	    }
	    FavoriteTracks.isLoaded = true;
	    favoriteTL = new FavoriteTracks.Items();
	    FavoriteTracks.defer = $.Deferred();
	    fechTracks = favoriteTL.fetch();
	    if (fechTracks) {
	      fechTracks.done(function(res) {
	        if (res.code === 0) {
	          FavoriteTracks.List = favoriteTL;
	          return FavoriteTracks.defer.resolveWith(fechTracks, [FavoriteTracks.List]);
	        } else {
	          FavoriteTracks.isLoaded = false;
	          return FavoriteTracks.defer.rejectWith(fechTracks, arguments);
	        }
	      });
	      fechTracks.fail(function() {
	        FavoriteTracks.isLoaded = false;
	        return FavoriteTracks.defer.rejectWith(fechTracks, arguments);
	      });
	      return FavoriteTracks.defer.promise();
	    }
	  };
	  API = {
	    getTracks: function() {
	      var defer;
	      if (FavoriteTracks.List === void 0) {
	        return initializeTracks();
	      }
	      defer = $.Deferred();
	      setImmediate(function() {
	        return defer.resolve(FavoriteTracks.List);
	      });
	      return defer.promise();
	    }
	  };
	  app.reqres.setHandler('get:favorite:tracks', function() {
	    return API.getTracks();
	  });
	  return API.getTracks();
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(99).setImmediate))

/***/ },
/* 99 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(setImmediate, clearImmediate) {var nextTick = __webpack_require__(100).nextTick;
	var apply = Function.prototype.apply;
	var slice = Array.prototype.slice;
	var immediateIds = {};
	var nextImmediateId = 0;

	// DOM APIs, for completeness

	exports.setTimeout = function() {
	  return new Timeout(apply.call(setTimeout, window, arguments), clearTimeout);
	};
	exports.setInterval = function() {
	  return new Timeout(apply.call(setInterval, window, arguments), clearInterval);
	};
	exports.clearTimeout =
	exports.clearInterval = function(timeout) { timeout.close(); };

	function Timeout(id, clearFn) {
	  this._id = id;
	  this._clearFn = clearFn;
	}
	Timeout.prototype.unref = Timeout.prototype.ref = function() {};
	Timeout.prototype.close = function() {
	  this._clearFn.call(window, this._id);
	};

	// Does not start the time, just sets up the members needed.
	exports.enroll = function(item, msecs) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = msecs;
	};

	exports.unenroll = function(item) {
	  clearTimeout(item._idleTimeoutId);
	  item._idleTimeout = -1;
	};

	exports._unrefActive = exports.active = function(item) {
	  clearTimeout(item._idleTimeoutId);

	  var msecs = item._idleTimeout;
	  if (msecs >= 0) {
	    item._idleTimeoutId = setTimeout(function onTimeout() {
	      if (item._onTimeout)
	        item._onTimeout();
	    }, msecs);
	  }
	};

	// That's not how node.js implements it but the exposed api is the same.
	exports.setImmediate = typeof setImmediate === "function" ? setImmediate : function(fn) {
	  var id = nextImmediateId++;
	  var args = arguments.length < 2 ? false : slice.call(arguments, 1);

	  immediateIds[id] = true;

	  nextTick(function onNextTick() {
	    if (immediateIds[id]) {
	      // fn.call() is faster so we optimize for the common use-case
	      // @see http://jsperf.com/call-apply-segu
	      if (args) {
	        fn.apply(null, args);
	      } else {
	        fn.call(null);
	      }
	      // Prevent ids from leaking
	      exports.clearImmediate(id);
	    }
	  });

	  return id;
	};

	exports.clearImmediate = typeof clearImmediate === "function" ? clearImmediate : function(id) {
	  delete immediateIds[id];
	};
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(99).setImmediate, __webpack_require__(99).clearImmediate))

/***/ },
/* 100 */
/***/ function(module, exports) {

	// shim for using process in browser

	var process = module.exports = {};
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
	    draining = false;
	    if (currentQueue.length) {
	        queue = currentQueue.concat(queue);
	    } else {
	        queueIndex = -1;
	    }
	    if (queue.length) {
	        drainQueue();
	    }
	}

	function drainQueue() {
	    if (draining) {
	        return;
	    }
	    var timeout = setTimeout(cleanUpNextTick);
	    draining = true;

	    var len = queue.length;
	    while(len) {
	        currentQueue = queue;
	        queue = [];
	        while (++queueIndex < len) {
	            if (currentQueue) {
	                currentQueue[queueIndex].run();
	            }
	        }
	        queueIndex = -1;
	        len = queue.length;
	    }
	    currentQueue = null;
	    draining = false;
	    clearTimeout(timeout);
	}

	process.nextTick = function (fun) {
	    var args = new Array(arguments.length - 1);
	    if (arguments.length > 1) {
	        for (var i = 1; i < arguments.length; i++) {
	            args[i - 1] = arguments[i];
	        }
	    }
	    queue.push(new Item(fun, args));
	    if (queue.length === 1 && !draining) {
	        setTimeout(drainQueue, 0);
	    }
	};

	// v8 likes predictible objects
	function Item(fun, array) {
	    this.fun = fun;
	    this.array = array;
	}
	Item.prototype.run = function () {
	    this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;

	process.binding = function (name) {
	    throw new Error('process.binding is not supported');
	};

	process.cwd = function () { return '/' };
	process.chdir = function (dir) {
	    throw new Error('process.chdir is not supported');
	};
	process.umask = function() { return 0; };


/***/ },
/* 101 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Feed, initializeFeed, initializeNewModel, initializePostList;
	  Feed = {};
	  Feed.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.deleteUrl = function() {
	      return app.getRequestUrlSid("/beelineMusicV1/wall/deleteRecord", "?recordId=" + this.id);
	    };

	    Item.prototype.parse = function(item) {
	      item.currentUser = Feed.currentUser.toJSON();
	      return item;
	    };

	    return Item;

	  })(Backbone.Model);
	  Feed.List = (function(superClass) {
	    extend(List, superClass);

	    function List() {
	      return List.__super__.constructor.apply(this, arguments);
	    }

	    List.prototype.model = Feed.Item;

	    List.prototype.url = app.getRequestUrl("/beelineMusicV1/wall/getAllRecords");

	    List.prototype.parse = function(collection) {
	      return collection.wallRecords;
	    };

	    List.prototype.comparator = function(a, b) {
	      var dateA, dateB;
	      dateA = new Date(a.get('date_create') * 1000);
	      dateB = new Date(b.get('date_create') * 1000);
	      if (dateA < dateB) {
	        return 1;
	      } else {
	        return -1;
	      }
	    };

	    return List;

	  })(Backbone.Collection);
	  initializeFeed = function() {
	    var feedList;
	    Feed.ListIsLoad = true;
	    feedList = new Feed.List;
	    Feed.ListDefer = $.Deferred();
	    app.request('get:user').done(function(user) {
	      var fetch;
	      Feed.currentUser = user;
	      fetch = feedList.fetch({
	        limit: 30
	      });
	      if (fetch) {
	        fetch.done(function() {
	          Feed.NewList = feedList;
	          return Feed.ListDefer.resolveWith(fetch, [Feed.NewList]);
	        });
	        return fetch.fail(function() {
	          Feed.ListIsLoad = false;
	          return Feed.ListDefer.rejectWith(fetch, arguments);
	        });
	      }
	    });
	    return Feed.ListDefer.promise();
	  };
	  initializePostList = function() {
	    return Feed.PostList = new Backbone.Collection;
	  };
	  initializeNewModel = function() {
	    var model;
	    model = (function(superClass) {
	      extend(model, superClass);

	      function model() {
	        return model.__super__.constructor.apply(this, arguments);
	      }

	      model.prototype.defaults = {
	        message: ''
	      };

	      return model;

	    })(Backbone.Model);
	    return Feed.NewModel = new model;
	  };
	  API = {
	    getFeeds: function(current) {
	      if (current) {
	        return Feed.NewList;
	      }
	      return initializeFeed();
	    },
	    getFeedPostItems: function() {
	      if (Feed.PostList === void 0) {
	        return initializePostList();
	      }
	      return Feed.PostList;
	    },
	    getNewFeedModel: function() {
	      if (Feed.NewModel === void 0) {
	        return initializeNewModel();
	      }
	      return Feed.NewModel;
	    }
	  };
	  app.reqres.setHandler('get:feeds', function(current) {
	    return API.getFeeds(current);
	  });
	  app.reqres.setHandler('get:feed:post:items', function() {
	    return API.getFeedPostItems();
	  });
	  return app.reqres.setHandler('get:new:feed:model', function() {
	    return API.getNewFeedModel();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 102 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Local, initializeGenre;
	  Local = {};
	  Local.Genre = (function(superClass) {
	    extend(Genre, superClass);

	    function Genre() {
	      return Genre.__super__.constructor.apply(this, arguments);
	    }

	    return Genre;

	  })(Backbone.Model);
	  Local.Genries = (function(superClass) {
	    extend(Genries, superClass);

	    function Genries() {
	      return Genries.__super__.constructor.apply(this, arguments);
	    }

	    Genries.prototype.model = Local.Genre;

	    Genries.prototype.localStorage = new Backbone.LocalStorage("Genries");

	    Genries.prototype.save = function() {
	      return this.each(function(model) {
	        return Backbone.localSync("update", model);
	      });
	    };

	    return Genries;

	  })(Backbone.Collection);
	  initializeGenre = function() {
	    Local.GenriesList = new Local.Genries;
	    return Local.GenriesList.fetch();
	  };
	  API = {
	    getGenries: function() {
	      if (Local.GenriesList === void 0) {
	        initializeGenre();
	      }
	      return Local.GenriesList;
	    }
	  };
	  return app.reqres.setHandler('get:local:genries', function() {
	    return API.getGenries();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 103 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Menu, initializeMenu;
	  Menu = {};
	  Menu.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    return Item;

	  })(Backbone.Model.Selected);
	  Menu.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Menu.Item;

	    return Collection;

	  })(Backbone.Collection.SingleSelect);
	  Menu.Genre = (function(superClass) {
	    extend(Genre, superClass);

	    function Genre() {
	      return Genre.__super__.constructor.apply(this, arguments);
	    }

	    return Genre;

	  })(Backbone.Model.Selected);
	  Menu.Genries = (function(superClass) {
	    extend(Genries, superClass);

	    function Genries() {
	      return Genries.__super__.constructor.apply(this, arguments);
	    }

	    Genries.prototype.model = Menu.Genre;

	    Genries.prototype.url = '/genries';

	    Genries.prototype.parse = function(collection) {
	      return _.where(collection.genres, {
	        'is_parent': true
	      });
	    };

	    return Genries;

	  })(Backbone.Collection.MultiSelected);
	  initializeMenu = function() {
	    return Menu.List = new Menu.Collection([
	      {
	        title: app.locale.menu.feed,
	        href: 'feed',
	        "class": 'feed',
	        trigger: 'show:feed'
	      }, {
	        title: app.locale.menu["new"],
	        href: 'new',
	        "class": 'new',
	        trigger: 'show:new'
	      }, {
	        title: app.locale.menu.popular,
	        href: '',
	        "class": 'recomend',
	        trigger: 'show:recomend'
	      }, {
	        title: app.locale.menu.playlists,
	        href: 'playlists',
	        "class": 'playlists',
	        trigger: 'show:playlists'
	      }, {
	        title: app.locale.menu.clips,
	        href: 'video',
	        "class": 'video',
	        trigger: 'show:video'
	      }
	    ]);
	  };
	  API = {
	    getMenus: function() {
	      if (Menu.List === void 0) {
	        initializeMenu();
	      }
	      return Menu.List;
	    },
	    getGenries: function() {
	      var defer, fetch, ganries;
	      ganries = new Menu.Genries;
	      defer = $.Deferred();
	      fetch = ganries.fetch();
	      fetch.done(function() {
	        return defer.resolveWith(fetch, [ganries]);
	      });
	      fetch.fail(function() {
	        return defer.rejectWith(fetch, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  app.reqres.setHandler('get:menu', function() {
	    return API.getMenus();
	  });
	  return app.reqres.setHandler('get:genries', function() {
	    return API.getGenries();
	  });
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 104 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, PlaylistsList, initializePlaylists, initializeSinglePlaylist;
	  PlaylistsList = {};
	  PlaylistsList.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = function() {
	      return app.getRequestUrl("/playlist/get", "?playlistid=" + this.id + "&items_on_page=1000");
	    };

	    Item.prototype.parse = function(model) {
	      if (model.album) {
	        model.album.album = true;
	        return model.album;
	      } else {
	        model.album = true;
	        return model;
	      }
	    };

	    return Item;

	  })(Backbone.Model);
	  PlaylistsList.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = PlaylistsList.Item;

	    Items.prototype.url = app.getRequestUrl("/beelineMusicV1/playlist/top");

	    Items.prototype.parse = function(collection) {
	      return collection.searched;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializePlaylists = function() {
	    var fetch, playlists;
	    if (PlaylistsList.ListIsLoad) {
	      return PlaylistsList.ListDefer.promise();
	    }
	    PlaylistsList.ListIsLoad = true;
	    playlists = new PlaylistsList.Items();
	    PlaylistsList.ListDefer = $.Deferred();
	    fetch = playlists.fetch({
	      data: {
	        count: 60,
	        offset: 0
	      }
	    });
	    if (fetch) {
	      fetch.done(function() {
	        PlaylistsList.List = playlists;
	        return PlaylistsList.ListDefer.resolveWith(fetch, [PlaylistsList.List]);
	      });
	      fetch.fail(function() {
	        PlaylistsList.ListIsLoad = false;
	        return PlaylistsList.ListDefer.rejectWith(fetch, arguments);
	      });
	      return PlaylistsList.ListDefer.promise();
	    }
	  };
	  initializeSinglePlaylist = function(id) {
	    var defer, fetch;
	    PlaylistsList.SinglePlaylist = new PlaylistsList.Item({
	      id: id
	    });
	    defer = $.Deferred();
	    fetch = PlaylistsList.SinglePlaylist.fetch();
	    if (fetch) {
	      fetch.done(function(res) {
	        if (res.code === 0) {
	          PlaylistsList.PlaylistId = id;
	          return defer.resolveWith(fetch, [PlaylistsList.SinglePlaylist]);
	        } else {
	          return defer.rejectWith(fetch, [res]);
	        }
	      });
	      fetch.fail(function() {
	        return defer.rejectWith(fetch, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  API = {
	    getPlaylists: function() {
	      if (PlaylistsList.List === void 0) {
	        return initializePlaylists();
	      }
	      return PlaylistsList.List;
	    },
	    getPlaylist: function(id) {
	      if (PlaylistsList.PlaylistId === id) {
	        return PlaylistsList.SinglePlaylist;
	      } else {
	        return initializeSinglePlaylist(id);
	      }
	    },
	    getTempPlaylists: function(collection) {
	      return new PlaylistsList.Items(collection);
	    }
	  };
	  app.reqres.setHandler('get:playlists', function() {
	    return API.getPlaylists();
	  });
	  app.reqres.setHandler('get:playlist', function(id) {
	    return API.getPlaylist(id);
	  });
	  return app.reqres.setHandler('get:temp:playlists', function(collection) {
	    return API.getTempPlaylists(collection.toJSON());
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 105 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Menu, initializeMenu;
	  Menu = {};
	  Menu.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    return Item;

	  })(Backbone.Model.Selected);
	  Menu.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Menu.Item;

	    return Collection;

	  })(Backbone.Collection.SingleSelect);
	  initializeMenu = function() {
	    return Menu.List = new Menu.Collection([
	      {
	        title: app.locale.profile.menu.main,
	        tab: 'main'
	      }, {
	        title: app.locale.profile.menu.access,
	        tab: 'subscribe'
	      }, {
	        title: app.locale.profile.menu.promocode,
	        tab: 'promo'
	      }, {
	        title: app.locale.profile.menu.followers,
	        tab: 'followers'
	      }, {
	        title: app.locale.profile.menu.followed,
	        tab: 'followed'
	      }
	    ]);
	  };
	  API = {
	    getMenus: function() {
	      if (Menu.List === void 0) {
	        initializeMenu();
	      }
	      return Menu.List;
	    }
	  };
	  return app.reqres.setHandler('get:profile:menu', function() {
	    return API.getMenus();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 106 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Recomend, initializeBlocks, initializeMain;
	  Recomend = {};
	  Recomend.Main = (function(superClass) {
	    extend(Main, superClass);

	    function Main() {
	      return Main.__super__.constructor.apply(this, arguments);
	    }

	    Main.prototype.urlRoot = app.getRequestUrl("/beelineMusicV1/main-page/getMainBlock");

	    Main.prototype.parse = function(model) {
	      if (model.today_best != null) {
	        return model.today_best;
	      } else {
	        return {
	          isEmpty: true
	        };
	      }
	    };

	    return Main;

	  })(Backbone.Model);
	  Recomend.Blocks = (function(superClass) {
	    extend(Blocks, superClass);

	    function Blocks() {
	      return Blocks.__super__.constructor.apply(this, arguments);
	    }

	    Blocks.prototype.urlRoot = app.getRequestUrl("/beelineMusicV1/main-page/getAdditionalBlocks");

	    Blocks.prototype.parse = function(model) {
	      return model.blocks;
	    };

	    return Blocks;

	  })(Backbone.Model);
	  initializeMain = function() {
	    var fetchMain, main;
	    if (Recomend.MainBlockIsLoad) {
	      return Recomend.MainBlockDefer.promise();
	    }
	    Recomend.MainBlockIsLoad = true;
	    main = new Recomend.Main();
	    Recomend.MainBlockDefer = $.Deferred();
	    fetchMain = main.fetch();
	    fetchMain.done(function() {
	      Recomend.MainBlock = main;
	      return Recomend.MainBlockDefer.resolveWith(fetchMain, [Recomend.MainBlock]);
	    });
	    fetchMain.fail(function() {
	      Recomend.MainBlockDefer.rejectWith(fetchMain, arguments);
	      return Recomend.MainBlockIsLoad = false;
	    });
	    return Recomend.MainBlockDefer.promise();
	  };
	  initializeBlocks = function() {
	    var blocks, fetch;
	    if (Recomend.BlocksIsLoad) {
	      return Recomend.BlocksDefer.promise();
	    }
	    Recomend.BlocksIsLoad = true;
	    blocks = new Recomend.Blocks();
	    Recomend.BlocksDefer = $.Deferred();
	    fetch = blocks.fetch();
	    fetch.done(function() {
	      Recomend.BlocksItem = blocks;
	      return Recomend.BlocksDefer.resolveWith(fetch, [Recomend.BlocksItem]);
	    });
	    fetch.fail(function() {
	      Recomend.BlocksDefer.rejectWith(fetch, arguments);
	      return Recomend.BlocksIsLoad = false;
	    });
	    return Recomend.BlocksDefer.promise();
	  };
	  API = {
	    getMain: function() {
	      if (Recomend.MainBlock === void 0) {
	        return initializeMain();
	      }
	      return Recomend.MainBlock;
	    },
	    getBlocks: function() {
	      if (Recomend.BlocksItem === void 0) {
	        return initializeBlocks();
	      }
	      return Recomend.BlocksItem;
	    }
	  };
	  app.reqres.setHandler('get:recomend:main', function() {
	    return API.getMain();
	  });
	  return app.reqres.setHandler('get:recomend:blocks', function() {
	    return API.getBlocks();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 107 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(22), __webpack_require__(26)], __WEBPACK_AMD_DEFINE_RESULT__ = function(CustomTrack, CustomAlbum) {
	  var API, SearchList, initializeAlbums, initializeArtists, initializePlaylists, initializeSearchPage, initializeTracks;
	  SearchList = {};
	  SearchList.Track = (function(superClass) {
	    extend(Track, superClass);

	    function Track() {
	      return Track.__super__.constructor.apply(this, arguments);
	    }

	    Track.prototype.parse = function(item) {
	      item.uniq = 'search-' + item.id;
	      item.optionsList = [
	        {
	          title: app.locale.options.addTPl,
	          "class": 'add-to-playlist'
	        }, {
	          title: app.locale.options.toArt,
	          "class": 'options-open-artist',
	          artist: true
	        }, {
	          title: app.locale.options.toAlb,
	          "class": 'options-open-album'
	        }
	      ];
	      return item;
	    };

	    return Track;

	  })(CustomTrack.Model);
	  SearchList.Album = (function(superClass) {
	    extend(Album, superClass);

	    function Album() {
	      return Album.__super__.constructor.apply(this, arguments);
	    }

	    Album.prototype.urlRoot = function() {
	      return app.getRequestUrl("/compatibility/catalogue/getAlbum", "?albumid=" + this.id);
	    };

	    Album.prototype.parse = function(model) {
	      if (model.album) {
	        return model.album;
	      } else {
	        return model;
	      }
	    };

	    return Album;

	  })(CustomAlbum.Model);
	  SearchList.Artist = (function(superClass) {
	    extend(Artist, superClass);

	    function Artist() {
	      return Artist.__super__.constructor.apply(this, arguments);
	    }

	    Artist.prototype.urlRoot = '/artist';

	    return Artist;

	  })(Backbone.Model);
	  SearchList.Playlist = (function(superClass) {
	    extend(Playlist, superClass);

	    function Playlist() {
	      return Playlist.__super__.constructor.apply(this, arguments);
	    }

	    Playlist.prototype.urlRoot = '/playlist';

	    return Playlist;

	  })(Backbone.Model);
	  SearchList.TracksCollection = (function(superClass) {
	    extend(TracksCollection, superClass);

	    function TracksCollection() {
	      return TracksCollection.__super__.constructor.apply(this, arguments);
	    }

	    TracksCollection.prototype.model = SearchList.Track;

	    TracksCollection.prototype.url = app.getRequestUrl('/search/tracks');

	    TracksCollection.prototype.parse = function(collection) {
	      return collection.searched;
	    };

	    return TracksCollection;

	  })(Backbone.Collection);
	  SearchList.AlbumsCollection = (function(superClass) {
	    extend(AlbumsCollection, superClass);

	    function AlbumsCollection() {
	      return AlbumsCollection.__super__.constructor.apply(this, arguments);
	    }

	    AlbumsCollection.prototype.model = SearchList.Album;

	    AlbumsCollection.prototype.url = app.getRequestUrl('/search/albums');

	    AlbumsCollection.prototype.parse = function(collection) {
	      return collection.searched;
	    };

	    return AlbumsCollection;

	  })(Backbone.Collection);
	  SearchList.ArtistsCollection = (function(superClass) {
	    extend(ArtistsCollection, superClass);

	    function ArtistsCollection() {
	      return ArtistsCollection.__super__.constructor.apply(this, arguments);
	    }

	    ArtistsCollection.prototype.model = SearchList.Artist;

	    ArtistsCollection.prototype.url = app.getRequestUrl('/search/artists');

	    ArtistsCollection.prototype.parse = function(collection) {
	      return collection.searched;
	    };

	    return ArtistsCollection;

	  })(Backbone.Collection);
	  SearchList.PlaylistsCollection = (function(superClass) {
	    extend(PlaylistsCollection, superClass);

	    function PlaylistsCollection() {
	      return PlaylistsCollection.__super__.constructor.apply(this, arguments);
	    }

	    PlaylistsCollection.prototype.model = SearchList.Playlist;

	    PlaylistsCollection.prototype.url = {
	      url: app.getRequestUrl('/search/tracks')
	    };

	    PlaylistsCollection.prototype.parse = function(collection) {
	      return collection.searched;
	    };

	    return PlaylistsCollection;

	  })(Backbone.Collection);
	  initializeTracks = function(options) {
	    var defer;
	    if (SearchList.fetchTracksCollection) {
	      SearchList.fetchTracksCollection.abort();
	    }
	    SearchList.Tracks = new SearchList.TracksCollection;
	    options || (options = {});
	    defer = $.Deferred();
	    SearchList.fetchTracksCollection = SearchList.Tracks.fetch(_.omit(options, 'success', 'error'));
	    SearchList.fetchTracksCollection.done(function() {
	      if (SearchList.fetchTracksCollection.responseJSON) {
	        return defer.resolveWith(SearchList.fetchTracksCollection, [SearchList.Tracks, SearchList.fetchTracksCollection.responseJSON.count]);
	      }
	    });
	    SearchList.fetchTracksCollection.fail(function() {
	      return defer.rejectWith(SearchList.fetchTracksCollection, arguments);
	    });
	    return defer.promise();
	  };
	  initializeAlbums = function(options) {
	    var defer;
	    if (SearchList.fetchAlbumsCollection) {
	      SearchList.fetchAlbumsCollection.abort();
	    }
	    SearchList.Albums = new SearchList.AlbumsCollection;
	    options || (options = {});
	    defer = $.Deferred();
	    SearchList.fetchAlbumsCollection = SearchList.Albums.fetch(_.omit(options, 'success', 'error'));
	    SearchList.fetchAlbumsCollection.done(function() {
	      if (SearchList.fetchAlbumsCollection.responseJSON) {
	        return defer.resolveWith(SearchList.fetchAlbumsCollection, [SearchList.Albums, SearchList.fetchAlbumsCollection.responseJSON.count]);
	      }
	    });
	    SearchList.fetchAlbumsCollection.fail(function() {
	      return defer.rejectWith(SearchList.fetchAlbumsCollection, arguments);
	    });
	    return defer.promise();
	  };
	  initializeArtists = function(options) {
	    var defer;
	    if (SearchList.fetchArtistsCollection) {
	      SearchList.fetchArtistsCollection.abort();
	    }
	    SearchList.Artists = new SearchList.ArtistsCollection;
	    options || (options = {});
	    defer = $.Deferred();
	    SearchList.fetchArtistsCollection = SearchList.Artists.fetch(_.omit(options, 'success', 'error'));
	    SearchList.fetchArtistsCollection.done(function() {
	      if (SearchList.fetchArtistsCollection.responseJSON) {
	        return defer.resolveWith(SearchList.fetchArtistsCollection, [SearchList.Artists, SearchList.fetchArtistsCollection.responseJSON.count]);
	      }
	    });
	    SearchList.fetchArtistsCollection.fail(function() {
	      return defer.rejectWith(SearchList.fetchArtistsCollection, arguments);
	    });
	    return defer.promise();
	  };
	  initializePlaylists = function(options) {
	    var defer;
	    if (SearchList.fetchPlaylistsCollection) {
	      SearchList.fetchPlaylistsCollection.abort();
	    }
	    SearchList.Playlists = new SearchList.PlaylistsCollection;
	    options || (options = {});
	    defer = $.Deferred();
	    SearchList.fetchPlaylistsCollection = SearchList.Playlists.fetch(_.omit(options, 'success', 'error'));
	    SearchList.fetchPlaylistsCollection.done(function() {
	      if (SearchList.fetchPlaylistsCollection.responseJSON) {
	        return defer.resolveWith(SearchList.fetchPlaylistsCollection, [SearchList.Playlists, SearchList.fetchPlaylistsCollection.responseJSON.count]);
	      }
	    });
	    SearchList.fetchPlaylistsCollection.fail(function() {
	      return defer.rejectWith(SearchList.fetchPlaylistsCollection, arguments);
	    });
	    return defer.promise();
	  };
	  initializeSearchPage = function(type, options) {
	    var defer;
	    if (SearchList.fetchPageCollection) {
	      SearchList.fetchPageCollection.abort();
	    }
	    switch (type) {
	      case 'artist':
	        SearchList.Page = new SearchList.ArtistsCollection;
	        break;
	      case 'album':
	        SearchList.Page = new SearchList.AlbumsCollection;
	        break;
	      case 'track':
	        SearchList.Page = new SearchList.TracksCollection;
	        break;
	      case 'playlist':
	        SearchList.Page = new SearchList.PlaylistsCollection;
	    }
	    options || (options = {});
	    defer = $.Deferred();
	    SearchList.fetchPageCollection = SearchList.Page.fetch(_.omit(options, 'success', 'error'));
	    SearchList.fetchPageCollection.done(function() {
	      if (SearchList.fetchPageCollection.responseJSON) {
	        return defer.resolveWith(SearchList.fetchPageCollection, [SearchList.Page, SearchList.fetchPageCollection.responseJSON.count]);
	      }
	    });
	    SearchList.fetchPageCollection.fail(function() {
	      return defer.rejectWith(SearchList.fetchPageCollection, arguments);
	    });
	    return defer.promise();
	  };
	  API = {
	    getTracksCollections: function(options) {
	      return initializeTracks(options);
	    },
	    getAlbumsCollections: function(options) {
	      return initializeAlbums(options);
	    },
	    getArtistsCollections: function(options) {
	      return initializeArtists(options);
	    },
	    getPlaylistsCollections: function(options) {
	      return initializePlaylists(options);
	    },
	    getSearchPageCollections: function(type, options) {
	      return initializeSearchPage(type, options);
	    }
	  };
	  app.reqres.setHandler('search:tracks', function(options) {
	    return API.getTracksCollections(options);
	  });
	  app.reqres.setHandler('search:albums', function(options) {
	    return API.getAlbumsCollections(options);
	  });
	  app.reqres.setHandler('search:artists', function(options) {
	    return API.getArtistsCollections(options);
	  });
	  app.reqres.setHandler('search:playlists', function(options) {
	    return API.getPlaylistsCollections(options);
	  });
	  return app.reqres.setHandler('search:page', function(type, options) {
	    return API.getSearchPageCollections(type, options);
	  });
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 108 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Slider, initializeSlides;
	  Slider = {};
	  Slider.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = '/slider';

	    return Item;

	  })(Backbone.Model);
	  Slider.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = Slider.Item;

	    Items.prototype.url = app.getRequestUrl('/generalV1/cp/getBanners');

	    Items.prototype.parse = function(collection) {
	      return collection.banners;
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeSlides = function() {
	    var defer, fetch, sliderList;
	    sliderList = new Slider.Items();
	    defer = $.Deferred();
	    fetch = sliderList.fetch();
	    if (fetch) {
	      fetch.done(function(res) {
	        if (res.code === 0) {
	          Slider.List = sliderList;
	          return defer.resolveWith(fetch, [Slider.List]);
	        } else {
	          return defer.rejectWith(fetch, [res]);
	        }
	      });
	      fetch.fail(function() {
	        return defer.rejectWith(fetch, arguments);
	      });
	      return defer.promise();
	    }
	  };
	  API = {
	    getSlides: function() {
	      if (Slider.List === void 0) {
	        return initializeSlides();
	      }
	      return Slider.List;
	    }
	  };
	  return app.reqres.setHandler('get:slides', function() {
	    return API.getSlides();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 109 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Tab, initializeTab;
	  Tab = {};
	  Tab.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.select = function() {
	      this.collection.each(function(model) {
	        return model.selected = false;
	      });
	      return this.selected = true;
	    };

	    return Item;

	  })(Backbone.Model);
	  Tab.Collection = (function(superClass) {
	    extend(Collection, superClass);

	    function Collection() {
	      return Collection.__super__.constructor.apply(this, arguments);
	    }

	    Collection.prototype.model = Tab.Item;

	    return Collection;

	  })(Backbone.Collection);
	  initializeTab = function() {
	    return Tab.List = new Tab.Collection([
	      {
	        title: app.locale.menu.tracksAndPlaylists,
	        "class": 'tracks',
	        selected: 'selected',
	        href: 'user/tracks',
	        trigger: 'show:user:tracks'
	      }, {
	        title: app.locale.menu.albums,
	        "class": 'albums',
	        href: 'user/albums',
	        trigger: 'show:user:albums'
	      }
	    ]);
	  };
	  API = {
	    getTabs: function() {
	      if (Tab.List === void 0) {
	        initializeTab();
	      }
	      return Tab.List;
	    }
	  };
	  return app.reqres.setHandler('get:tabs', function() {
	    return API.getTabs();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 110 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_RESULT__ = function() {
	  var API, Notify, initializeNotify;
	  Notify = {};
	  Notify.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    return Item;

	  })(Backbone.Model);
	  Notify.Items = (function(superClass) {
	    extend(Items, superClass);

	    function Items() {
	      return Items.__super__.constructor.apply(this, arguments);
	    }

	    Items.prototype.model = Notify.Item;

	    Items.prototype.localStorage = new Backbone.LocalStorage("Notify");

	    Items.prototype.save = function() {
	      return this.each(function(model) {
	        return Backbone.localSync("update", model);
	      });
	    };

	    return Items;

	  })(Backbone.Collection);
	  initializeNotify = function() {
	    Notify.List = new Notify.Items;
	    return Notify.List.fetch();
	  };
	  API = {
	    getNotify: function() {
	      if (Notify.List === void 0) {
	        initializeNotify();
	      }
	      return Notify.List;
	    }
	  };
	  return app.reqres.setHandler('get:user:notification', function() {
	    return API.getNotify();
	  });
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	

/***/ },
/* 111 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(setImmediate) {var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
	  hasProp = {}.hasOwnProperty;

	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(8)], __WEBPACK_AMD_DEFINE_RESULT__ = function(Notification) {
	  var API, Model, initializeUser, initializeUserById;
	  Model = {};
	  Model.Item = (function(superClass) {
	    extend(Item, superClass);

	    function Item() {
	      return Item.__super__.constructor.apply(this, arguments);
	    }

	    Item.prototype.urlRoot = app.getRequestUrl('/compatibility/user/GetBySid');

	    Item.prototype.parse = function(model) {
	      if (model.users) {
	        return model.users[0];
	      } else {
	        return model.user;
	      }
	    };

	    return Item;

	  })(Backbone.Model);
	  Model.IdItem = (function(superClass) {
	    extend(IdItem, superClass);

	    function IdItem() {
	      return IdItem.__super__.constructor.apply(this, arguments);
	    }

	    IdItem.prototype.urlRoot = function() {
	      return app.getRequestUrl("/beelineMusicV1/society/getPublicProfiles");
	    };

	    IdItem.prototype.parse = function(model) {
	      if (model.users) {
	        return model.users[0];
	      } else {
	        return model.user;
	      }
	    };

	    return IdItem;

	  })(Backbone.Model);
	  initializeUser = function() {
	    var fetchUser, user;
	    if (Model.isLoad) {
	      return Model.defer.promise();
	    }
	    Model.isLoad = true;
	    user = new Model.Item();
	    Model.defer = $.Deferred();
	    fetchUser = user.fetch();
	    if (!Model.UserTimer) {
	      Model.UserTimer = setInterval(function() {
	        return API.fetchUser();
	      }, 60000);
	    }
	    if (fetchUser) {
	      fetchUser.done(function(res) {
	        if (res.code === 0) {
	          Model.User = user;
	          Model.defer.resolveWith(fetchUser, [Model.User]);
	          if (Model.User.get('subscription') === 'DISABLED') {
	            return Notification.Controller.showSubscribe();
	          } else if (Model.User.get('subscription') === 'OTHER') {
	            return Notification.Controller.showSubscribe();
	          }
	        } else {
	          if (res.code === -4) {
	            $.get('/exit', function() {
	              return location.href = '';
	            });
	          }
	          Model.defer.rejectWith(fetchUser, arguments);
	          return Model.isLoad = false;
	        }
	      });
	      fetchUser.fail(function() {
	        Model.defer.rejectWith(fetchUser, arguments);
	        return Model.isLoad = false;
	      });
	      return Model.defer.promise();
	    }
	  };
	  initializeUserById = function(id) {
	    var fetchUser, user;
	    if (Model.isLoadById) {
	      return Model.deferById.promise();
	    }
	    Model.isLoadById = true;
	    user = new Model.IdItem();
	    Model.deferById = $.Deferred();
	    fetchUser = user.fetch({
	      data: {
	        userIds: [id]
	      }
	    });
	    if (fetchUser) {
	      fetchUser.done(function(res) {
	        if (res.code === 0) {
	          if (res.users[0]['not_found']) {
	            Model.deferById.rejectWith(fetchUser, arguments);
	          } else {
	            Model.UserById = user;
	            Model.deferById.resolveWith(fetchUser, [Model.UserById]);
	            Model.actualUserId = id;
	          }
	        } else {
	          Model.deferById.rejectWith(fetchUser, arguments);
	        }
	        return Model.isLoadById = false;
	      });
	      fetchUser.fail(function() {
	        Model.deferById.rejectWith(fetchUser, arguments);
	        return Model.isLoadById = false;
	      });
	      return Model.deferById.promise();
	    }
	  };
	  API = {
	    getUser: function() {
	      var defer;
	      if (Model.User === void 0) {
	        return initializeUser();
	      }
	      defer = $.Deferred();
	      setImmediate(function() {
	        return defer.resolve(Model.User);
	      });
	      return defer.promise();
	    },
	    getUserById: function(id) {
	      var defer;
	      if (Model.actualUserId !== id) {
	        return initializeUserById(id);
	      }
	      defer = $.Deferred();
	      setImmediate(function() {
	        return defer.resolve(Model.UserById);
	      });
	      return defer.promise();
	    },
	    fetchUser: function() {
	      var fetch;
	      if (Model.User) {
	        fetch = Model.User.fetch();
	        return fetch.done(function() {
	          var status;
	          status = Model.User.get('subscription');
	          if (status !== 'DISABLED' && status !== 'OTHER') {
	            return Notification.Controller.closeSubscribe();
	          } else if (status === 'OTHER') {
	            return Notification.Controller.showSubscribe();
	          } else {
	            return Notification.Controller.showSubscribe();
	          }
	        });
	      }
	    }
	  };
	  app.reqres.setHandler('get:user', function(id) {
	    if (id != null) {
	      return API.getUserById(id);
	    } else {
	      return API.getUser();
	    }
	  });
	  app.reqres.setHandler('fetch:user', function() {
	    return API.fetchUser();
	  });
	  return Model;
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(99).setImmediate))

/***/ }
/******/ ]);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL3NjcmlwdHMvYXBwLmpzP2I4NzlhMzAwYjkzNGQzYTU5MDkxIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIGI4NzlhMzAwYjkzNGQzYTU5MDkxIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3BsYXllci9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3BsYXllci92aWV3LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvcGFydHMvcGxheWVyL3RlbXBsYXRlcy9tYWluLXRwbC5qYWRlIiwid2VicGFjazovLy8uL34vamFkZS9saWIvcnVudGltZS5qcyIsIndlYnBhY2s6Ly8vZnMgKGlnbm9yZWQpIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvbG9nL2luZGV4LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvbG9nLmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvcGFydHMvbm90aWZpY2F0aW9uL2luZGV4LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvcGFydHMvbm90aWZpY2F0aW9uL3ZpZXcuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9ub3RpZmljYXRpb24vdGVtcGxhdGVzL25vdGlmaWNhdGlvbi1kZWZhdWx0LmphZGUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL25vdGlmaWNhdGlvbi9zdHlsZS5zdHlsIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9wbGF5ZXIvc3R5bGUuc3R5bCIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9zdHlsZXMvYXBwL3N0eWxlLnN0eWwiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9zZXR0aW5nL2JhY2tib25lX21vZGVsX21ldGhvZHMuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvc2V0dGluZy9yb3V0ZV9zZXR0aW5nLmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL3NldHRpbmcvc3luY19yZXdyaXRlLmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2hlbHBlcnMvZmlsdGVyZWRfY29sbGVjdGlvbi5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9tb2RlbC9zZWxlY3QuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYWdlcy9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3NpZGViYXIvaW5kZXguY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaWRlYmFyL3ZpZXcuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3RyYWNrL2luZGV4LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjay92aWV3LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjay9tb2RlbC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvdHJhY2svc3R5bGUuc3R5bCIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9hbGJ1bS9pbmRleC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYWxidW0vdmlldy5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3NpbXBsZV92aWV3cy91cGRhdGUvaW5kZXguY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaW1wbGVfdmlld3MvdXBkYXRlL3N0eWxlLnN0eWwiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL3BhcnRzL3NpbXBsZV92aWV3cy9sb2FkZXIvaW5kZXguY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaW1wbGVfdmlld3MvbG9hZGVyL3N0eWxlLnN0eWwiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYWxidW0vbW9kZWwuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FsYnVtL3N0eWxlLnN0eWwiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYXJ0aXN0L2luZGV4LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9hcnRpc3Qvdmlldy5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYXJ0aXN0L3N0eWxlLnN0eWwiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvcGxheWxpc3QvaW5kZXguY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3BsYXlsaXN0L3ZpZXcuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3BsYXlsaXN0L21vZGVsLmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9wbGF5bGlzdC9zdHlsZS5zdHlsIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaWRlYmFyL3RlbXBsYXl0cy9sYXlvdXQtdGVtcGxhdGUuamFkZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvcGFydHMvc2lkZWJhci9zdHlsZS5zdHlsIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9lbnRpdGllcy90cmFja19saXN0LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMgbm9ucmVjdXJzaXZlIF5cXC5cXC8uKiQiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2FncmVlbWVudF9tb2RlbC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2FsYnVtX2xpc3QuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9hcnRpc3RfbW9kZWwuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9jaGFydHNfbGlzdC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2NsaXBMaXN0LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvZmF2b3JpdGVfYWxidW1zLmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvZmF2b3JpdGVfcGxheV9saXN0LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvZmF2b3JpdGVfdHJhY2tfbGlzdC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLyh3ZWJwYWNrKS9+L25vZGUtbGlicy1icm93c2VyL34vdGltZXJzLWJyb3dzZXJpZnkvbWFpbi5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL34vbm9kZS1saWJzLWJyb3dzZXIvfi9wcm9jZXNzL2Jyb3dzZXIuanMiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2ZlZWQuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9sb2NhbC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL21lbnVfbGlzdC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3BsYXlsaXN0c19saXN0LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvcHJvZmlsZV9tZW51LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvcmVjb21lbmRfcGFnZS5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3NlYXJjaF9saXN0LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvc2xpZGVyX2xpc3QuY29mZmVlIiwid2VicGFjazovLy8uL2Zyb250ZW5kL2FwcC9lbnRpdGllcy90YWJfbGlzdC5jb2ZmZWUiLCJ3ZWJwYWNrOi8vLy4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3VzZXItbm90aWZ5LmNvZmZlZSIsIndlYnBhY2s6Ly8vLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvdXNlcl9tb2RlbC5jb2ZmZWUiXSwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXTtcbiBcdHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGNodW5rSWRzLCBtb3JlTW9kdWxlcykge1xuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIGNhbGxiYWNrcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pXG4gXHRcdFx0XHRjYWxsYmFja3MucHVzaC5hcHBseShjYWxsYmFja3MsIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSk7XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuIFx0XHRpZihwYXJlbnRKc29ucEZ1bmN0aW9uKSBwYXJlbnRKc29ucEZ1bmN0aW9uKGNodW5rSWRzLCBtb3JlTW9kdWxlcyk7XG4gXHRcdHdoaWxlKGNhbGxiYWNrcy5sZW5ndGgpXG4gXHRcdFx0Y2FsbGJhY2tzLnNoaWZ0KCkuY2FsbChudWxsLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0fTtcblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIFwiMFwiIG1lYW5zIFwiYWxyZWFkeSBsb2FkZWRcIlxuIFx0Ly8gQXJyYXkgbWVhbnMgXCJsb2FkaW5nXCIsIGFycmF5IGNvbnRhaW5zIGNhbGxiYWNrc1xuIFx0dmFyIGluc3RhbGxlZENodW5rcyA9IHtcbiBcdFx0MDowXG4gXHR9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcblxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0ZXhwb3J0czoge30sXG4gXHRcdFx0aWQ6IG1vZHVsZUlkLFxuIFx0XHRcdGxvYWRlZDogZmFsc2VcbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubG9hZGVkID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cbiBcdC8vIFRoaXMgZmlsZSBjb250YWlucyBvbmx5IHRoZSBlbnRyeSBjaHVuay5cbiBcdC8vIFRoZSBjaHVuayBsb2FkaW5nIGZ1bmN0aW9uIGZvciBhZGRpdGlvbmFsIGNodW5rc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5lID0gZnVuY3Rpb24gcmVxdWlyZUVuc3VyZShjaHVua0lkLCBjYWxsYmFjaykge1xuIFx0XHQvLyBcIjBcIiBpcyB0aGUgc2lnbmFsIGZvciBcImFscmVhZHkgbG9hZGVkXCJcbiBcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID09PSAwKVxuIFx0XHRcdHJldHVybiBjYWxsYmFjay5jYWxsKG51bGwsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIGFuIGFycmF5IG1lYW5zIFwiY3VycmVudGx5IGxvYWRpbmdcIi5cbiBcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdICE9PSB1bmRlZmluZWQpIHtcbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0ucHVzaChjYWxsYmFjayk7XG4gXHRcdH0gZWxzZSB7XG4gXHRcdFx0Ly8gc3RhcnQgY2h1bmsgbG9hZGluZ1xuIFx0XHRcdGluc3RhbGxlZENodW5rc1tjaHVua0lkXSA9IFtjYWxsYmFja107XG4gXHRcdFx0dmFyIGhlYWQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdO1xuIFx0XHRcdHZhciBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiBcdFx0XHRzY3JpcHQudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnO1xuIFx0XHRcdHNjcmlwdC5jaGFyc2V0ID0gJ3V0Zi04JztcbiBcdFx0XHRzY3JpcHQuYXN5bmMgPSB0cnVlO1xuXG4gXHRcdFx0c2NyaXB0LnNyYyA9IF9fd2VicGFja19yZXF1aXJlX18ucCArIFwiL3NjcmlwdHMvY2h1bmtzL1wiICsgKHt9W2NodW5rSWRdfHxjaHVua0lkKSArIFwiL1wiICsgY2h1bmtJZCArIFwiLmpzP1wiICsgXCJiODc5YTMwMGI5MzRkM2E1OTA5MVwiICsgXCJcIjtcbiBcdFx0XHRoZWFkLmFwcGVuZENoaWxkKHNjcmlwdCk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL3BhY2tcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIHdlYnBhY2svYm9vdHN0cmFwIGI4NzlhMzAwYjkzNGQzYTU5MDkxXG4gKiovIiwiZGVmaW5lIFsnLi9wYXJ0cy9wbGF5ZXInXSwgKFBsYXllciktPlxuICAgIHJlcXVpcmUoJy4uL3N0eWxlcy9hcHAvc3R5bGUuc3R5bCcpXG4gICAgcmVxdWlyZSgnLi9jb25maWcvc2V0dGluZy9iYWNrYm9uZV9tb2RlbF9tZXRob2RzLmNvZmZlZScpXG4gICAgcmVxdWlyZSgnLi9jb25maWcvc2V0dGluZy9yb3V0ZV9zZXR0aW5nLmNvZmZlZScpXG4gICAgcmVxdWlyZSgnLi9jb25maWcvc2V0dGluZy9zeW5jX3Jld3JpdGUuY29mZmVlJylcbiAgICByZXF1aXJlKCcuL2NvbmZpZy9oZWxwZXJzL2ZpbHRlcmVkX2NvbGxlY3Rpb24uY29mZmVlJylcbiAgICByZXF1aXJlKCcuL2NvbmZpZy9tb2RlbC9zZWxlY3QuY29mZmVlJylcbiAgICByZXF1aXJlKCcuL3BhZ2VzJylcblxuICAgIGFwcC5hZGRSZWdpb25zXG4gICAgICAgIGNvbnRlbnQ6ICcjbWFpbidcbiAgICAgICAgdmlkZW86ICcjdmlkZW8tY29udGFpbmVyJ1xuICAgICAgICBwbGF5ZXI6ICcjcGxheWVyJ1xuICAgICAgICBzaWRlYmFyOiAnI3NpZGViYXInXG4gICAgICAgIG5vdGlmaWNhdGlvbjogJyNub3RpZmljYXRpb24nXG4gICAgICAgIHBvcHVwczogJyNtYWluLXBvcHVwcydcblxuICAgIGFwcC5ob3N0ID0gJ2h0dHA6Ly9jZG4ubXVzaWMuYmVlbGluZS5ydS9hcGknXG5cbiAgICBhcHAuZ2V0UmVxdWVzdFVybCA9ICh1cmwscXVlcnk9JycpLT5cbiAgICAgICAgcmV0dXJuIFwiI3thcHAuaG9zdH0je3VybH0vI3txdWVyeX1cIlxuXG4gICAgYXBwLmdldFJlcXVlc3RVcmxTaWQgPSAodXJsLHF1ZXJ5KS0+XG4gICAgICAgIHF1ZXJ5ID0gaWYgcXVlcnkgdGhlbiBcIiN7cXVlcnl9JnNpZD0je2FwcC51c2VyLnNpZH1cIiBlbHNlIFwiP3NpZD0je2FwcC51c2VyLnNpZH1cIlxuICAgICAgICByZXR1cm4gXCIje2FwcC5ob3N0fSN7dXJsfS8je3F1ZXJ5fVwiXG5cbiAgICBhcHAubmF2aWdhdGUgPSAocm91dGUsb3B0aW9ucyktPlxuICAgICAgICBvcHRpb25zIG9yIChvcHRpb25zID0ge30pXG4gICAgICAgIHJvdXRlID0gXCIvI3thcHAubG9jYWxlLmxhbmd9XCIrcm91dGVcbiAgICAgICAgQmFja2JvbmUuaGlzdG9yeS5uYXZpZ2F0ZSByb3V0ZSxvcHRpb25zXG5cbiAgICBhcHAucmVhZENvb2tpZSA9IChuYW1lKS0+XG4gICAgICAgIG5hbWVFUSA9IG5hbWUgKyBcIj1cIjtcbiAgICAgICAgY2EgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoJzsnKTtcbiAgICAgICAgaWYgY2EubGVuZ3RoIGlzIDBcbiAgICAgICAgICAgIHJldHVybiBudWxsXG5cbiAgICAgICAgZm9yIGkgaW4gWzAuLmNhLmxlbmd0aC0xXVxuICAgICAgICAgICAgYyA9IGNhW2ldO1xuICAgICAgICAgICAgd2hpbGUgYy5jaGFyQXQoMCk9PScgJ1xuICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxLGMubGVuZ3RoKTtcblxuICAgICAgICAgICAgaWYgYy5pbmRleE9mKG5hbWVFUSkgPT0gMFxuICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lRVEubGVuZ3RoLGMubGVuZ3RoKTtcblxuICAgICAgICByZXR1cm4gbnVsbDtcblxuICAgIGFwcC5nZXRDdXJyZW50Um91dGUgPSAoKS0+XG4gICAgICAgIHJldHVybiBCYWNrYm9uZS5oaXN0b3J5LmZyYWdtZW50XG5cbiAgICBhcHAuUmFuZEludCA9IChsb3dfbGltaXQsIGhpZ2hfbGltaXQpLT5cbiAgICAgICByYW5kb21hID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKihoaWdoX2xpbWl0IC0gbG93X2xpbWl0KSkgKyBsb3dfbGltaXQ7XG4gICAgICAgcmV0dXJuIHJhbmRvbWE7XG5cbiAgICBhcHAuY3JlYXRlSW1nTGluayA9IChsaW5rLHNpemUsYmx1cixybyktPlxuICAgICAgICBpZiBsaW5rXG4gICAgICAgICAgICBybyA9IGlmIHJvIHRoZW4gJ19ybycgZWxzZSAnX2NyJ1xuICAgICAgICAgICAgaWYgYmx1clxuICAgICAgICAgICAgICAgIHJldHVybiBsaW5rLnJlcGxhY2UoLyhbXlxcL10rKVxcLihbXlxcL10rKSQvLHNpemUrcm8rJ19iMHg3LiQyJylcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICByZXR1cm4gbGluay5yZXBsYWNlKC8oW15cXC9dKylcXC4oW15cXC9dKykkLyxzaXplK3JvKycuJDInKVxuICAgICAgICBlbHNlXG4gICAgICAgICAgICByZXR1cm4gJy9pbWcvbm8tb25pbWcucG5nJ1xuXG4gICAgYXBwLnBhcnNlUGF0aCA9IChwYXRoKS0+XG4gICAgICAgIGlmIHBhdGg/XG4gICAgICAgICAgICBhcnIgPSBwYXRoLnNwbGl0ICcmJ1xuICAgICAgICAgICAgcmVzID0ge31cbiAgICAgICAgICAgIGZvciB2YWwgaW4gYXJyXG4gICAgICAgICAgICAgICAgdmFsID0gdmFsLnNwbGl0ICc9J1xuICAgICAgICAgICAgICAgIHJlc1t2YWxbMF1dID0gdmFsWzFdXG4gICAgICAgICAgICByZXR1cm4gcmVzXG4gICAgICAgIGVsc2VcbiAgICAgICAgICAgIHRocm93ICfQn9Cw0YDQsNC80LXRgtGAIHBhdGgg0L3QtSDQtNC+0LvQttC10L0g0LHRi9GC0Ywg0L/Rg9GB0YLRi9C8J1xuXG4gICAgYXBwLmlzSW5mYXZvcml0ZSA9IChpZCktPlxuICAgICAgICBmYXZvcml0ZVRyYWNrcyA9IGFwcC5yZXF1ZXN0ICdnZXQ6ZmF2b3JpdGU6dHJhY2tzJ1xuICAgICAgICBcbiAgICAgICAgcmV0dXJuIGZhdm9yaXRlVHJhY2tzLnNvbWUgKGl0ZW0pLT5cbiAgICAgICAgICAgIHJldHVybiBpdGVtLmdldCgnaWQnKSBpcyBpZFxuXG4gICAgYXBwLmNoZWNrQWN0aXZlVHJhY2sgPSAtPlxuICAgICAgICBjdXJyZW50SHJlZiA9IEJhY2tib25lLmhpc3RvcnkubG9jYXRpb24uaHJlZlxuICAgICAgICBpZiBjdXJyZW50SHJlZiBpcyBhcHAucGxheWVkSHJlZlxuICAgICAgICAgICAgJChcIiMje2FwcC5wbGF5ZWRJZH1cIikuYWRkQ2xhc3MgJ2FjdGl2ZSAnK1BsYXllci5Db250cm9sbGVyLnBsYXllckNsYXNzU3RhdHVzXG5cbiAgICBhcHAuY2hlY2tBY3RpdmVBbGJ1bSA9IC0+XG4gICAgICAgICQoXCIjI3thcHAucGxheWVkQWxidW19XCIpLmFkZENsYXNzICdhY3RpdmUgJytQbGF5ZXIuQ29udHJvbGxlci5wbGF5ZXJDbGFzc1N0YXR1c1xuXG4gICAgYXBwLmRlY2xpbmF0aW9uID0gKGlOdW1iZXIsIGFFbmRpbmdzKS0+XG4gICAgICAgICMgYUVuZGluZ3MgLSBbJ9GP0LHQu9C+0LrQvicsICfRj9Cx0LvQvtC60LAnLCAn0Y/QsdC70L7QuiddXG4gICAgICAgIGlOdW1iZXIgPSBpTnVtYmVyICUgMTAwO1xuICAgICAgICBpZiAoaU51bWJlcj49MTEgJiYgaU51bWJlcjw9MTkpXG4gICAgICAgICAgICBzRW5kaW5nPWFFbmRpbmdzWzJdO1xuICAgICAgICBlbHNlXG4gICAgICAgICAgICBpID0gaU51bWJlciAlIDEwO1xuICAgICAgICAgICAgc3dpdGNoIGlcbiAgICAgICAgICAgICAgICB3aGVuIDEgXG4gICAgICAgICAgICAgICAgICAgIHNFbmRpbmcgPSBhRW5kaW5nc1swXTsgXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIHdoZW4gMlxuICAgICAgICAgICAgICAgICAgICBzRW5kaW5nID0gYUVuZGluZ3NbMV07XG4gICAgICAgICAgICAgICAgd2hlbiAzXG4gICAgICAgICAgICAgICAgICAgIHNFbmRpbmcgPSBhRW5kaW5nc1sxXTtcbiAgICAgICAgICAgICAgICB3aGVuIDQgXG4gICAgICAgICAgICAgICAgICAgIHNFbmRpbmcgPSBhRW5kaW5nc1sxXTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICBzRW5kaW5nID0gYUVuZGluZ3NbMl07XG4gICAgICAgIHJldHVybiBzRW5kaW5nXG5cblxuICAgIGFwcC5HQVBhZ2VBY3Rpb24gPSAoY2F0LHN1YmNhdCktPlxuICAgICAgICB1c2VyID0gYXBwLnJlcXVlc3QgJ2dldDp1c2VyJ1xuICAgICAgICBpZiB1c2VyLnByb21pc2VcbiAgICAgICAgICAgIHVzZXIuZG9uZSAodXNlciktPlxuICAgICAgICAgICAgICAgIF9nYXEucHVzaChbJ190cmFja0V2ZW50JywgY2F0LCBzdWJjYXQsIHVzZXIuZ2V0KCdwaG9uZScpXSlcblxuICAgICAgICBlbHNlXG4gICAgICAgICAgICBfZ2FxLnB1c2goWydfdHJhY2tFdmVudCcsIGNhdCwgc3ViY2F0LCB1c2VyLmdldCgncGhvbmUnKV0pXG5cbiAgICBhcHAucmVhZENvb2tpZSA9IChuYW1lKS0+XG4gICAgICAgIG5hbWVFUSA9IG5hbWUgKyBcIj1cIjtcbiAgICAgICAgY2EgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoJzsnKTtcbiAgICAgICAgaWYgY2EubGVuZ3RoIGlzIDBcbiAgICAgICAgICAgIHJldHVybiBudWxsXG5cbiAgICAgICAgZm9yIGkgaW4gWzAuLmNhLmxlbmd0aC0xXVxuICAgICAgICAgICAgYyA9IGNhW2ldO1xuICAgICAgICAgICAgd2hpbGUgYy5jaGFyQXQoMCk9PScgJ1xuICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxLGMubGVuZ3RoKTtcblxuICAgICAgICAgICAgaWYgYy5pbmRleE9mKG5hbWVFUSkgPT0gMFxuICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lRVEubGVuZ3RoLGMubGVuZ3RoKTtcblxuICAgICAgICByZXR1cm4gbnVsbDtcblxuICAgIGFwcC5nZXRSZWZlcmVyID0gLT5cbiAgICAgICAgdXJsID0gbG9jYXRpb24uaHJlZlxuXG4gICAgICAgIGlmIH51cmwuaW5kZXhPZignL2ZlZWQnKVxuICAgICAgICAgICAgcmV0dXJuICd3YWxsJ1xuICAgICAgICBlbHNlIGlmIH51cmwuaW5kZXhPZignL3NlYXJjaCcpXG4gICAgICAgICAgICByZXR1cm4gJ3NlYXJjaCdcbiAgICAgICAgZWxzZSBpZiB+dXJsLmluZGV4T2YoJy9wcm9maWxlJylcbiAgICAgICAgICAgIHJldHVybiAncHJvZmlsZSdcbiAgICAgICAgZWxzZSBpZiB+dXJsLmluZGV4T2YoJy9uZXcnKVxuICAgICAgICAgICAgcmV0dXJuICduZXcnXG4gICAgICAgIGVsc2UgaWYgfnVybC5pbmRleE9mKCcvcGxheWxpc3RzJylcbiAgICAgICAgICAgIHJldHVybiAncHVibGljUGxheWxpc3RzJ1xuICAgICAgICBlbHNlIGlmIH51cmwuaW5kZXhPZignL3RyYWNrcycpXG4gICAgICAgICAgICByZXR1cm4gJ215VHJhY2tzJ1xuICAgICAgICBlbHNlIGlmIH51cmwuaW5kZXhPZignL2FydGlzdCcpXG4gICAgICAgICAgICByZXR1cm4gJ2FydGlzdCdcbiAgICAgICAgZWxzZSBpZiB+dXJsLmluZGV4T2YoJy9hbGJ1bScpXG4gICAgICAgICAgICByZXR1cm4gJ3JlbGVhc2UnXG4gICAgICAgIGlmIH51cmwuaW5kZXhPZignLycpXG4gICAgICAgICAgICByZXR1cm4gJ3JlY29tbWVuZGVkJ1xuXG4gICAgYXBwLmdldFVzZXJJbmZvID0gLT5cbiAgICAgICAgcmV0dXJuIGFwcC51c2VyXG5cbiAgICBjbGVhclBhZ2UgPSAtPlxuICAgICAgICAkKCcudHJhY2staXRlbS5hY3RpdmUnKS5yZW1vdmVDbGFzcyAnYWN0aXZlIHBsYXkgcGF1c2UnXG4gICAgICAgICQoJy5hbGJ1bS1pdGVtLmFjdGl2ZScpLnJlbW92ZUNsYXNzICdhY3RpdmUgcGxheSBwYXVzZSdcblxuICAgIGFwcC5vbiAncGxheScsIC0+XG4gICAgICAgIGN1cnJlbnRIcmVmID0gQmFja2JvbmUuaGlzdG9yeS5sb2NhdGlvbi5ocmVmXG4gICAgICAgIGRvIGNsZWFyUGFnZVxuICAgICAgICAkKFwiIyN7YXBwLnBsYXllZEFsYnVtfVwiKS5hZGRDbGFzcyAnYWN0aXZlIHBhdXNlJ1xuICAgICAgICBpZiBjdXJyZW50SHJlZiBpcyBhcHAucGxheWVkSHJlZlxuICAgICAgICAgICAgJChcIiMje2FwcC5wbGF5ZWRJZH1cIikuYWRkQ2xhc3MgJ2FjdGl2ZSBwYXVzZSdcblxuXG4gICAgYXBwLm9uICdwYXVzZScsIC0+XG4gICAgICAgIGN1cnJlbnRIcmVmID0gQmFja2JvbmUuaGlzdG9yeS5sb2NhdGlvbi5ocmVmXG4gICAgICAgIGRvIGNsZWFyUGFnZVxuICAgICAgICAkKFwiIyN7YXBwLnBsYXllZEFsYnVtfVwiKS5hZGRDbGFzcyAnYWN0aXZlIHBsYXknXG4gICAgICAgIGlmIGN1cnJlbnRIcmVmIGlzIGFwcC5wbGF5ZWRIcmVmXG4gICAgICAgICAgICAkKFwiIyN7YXBwLnBsYXllZElkfVwiKS5hZGRDbGFzcyAnYWN0aXZlIHBsYXknXG5cblxuXG4gICAgYXBwLm9uICdzdGFydCcsICgpLT5cbiAgICAgICAgRW50aXRpZXMgPSByZXF1aXJlLmNvbnRleHQoJy4vZW50aXRpZXMnLCBmYWxzZSk7XG5cbiAgICAgICAgRW50aXRpZXMua2V5cygpLmZvckVhY2ggKHBhdGgpLT5cbiAgICAgICAgICAgIEVudGl0aWVzIHBhdGhcblxuICAgICAgICBhcHAubG9nZ2VyID0gcmVxdWlyZSgnLi9jb25maWcvbG9nJylcbiAgICAgICAgZG8gYXBwLmxvZ2dlci5zZW5kTmV3TG9nXG4gICAgICAgIGFwcC5sb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKCcvZ2VuZXJhbFYxL2xvZy9lbnRlcicpXG5cbiAgICAgICAgaWYgQmFja2JvbmUuaGlzdG9yeVxuICAgICAgICAgICAgQmFja2JvbmUuaGlzdG9yeS5zdGFydCB7cHVzaFN0YXRlOiB0cnVlfVxuXG4gICAgICAgIFNpZGViYXIgPSByZXF1aXJlKCcuL3BhcnRzL3NpZGViYXInKVxuICAgICAgICBTaWRlYmFyLkNvbnRyb2xsZXIuc2hvdygpXG4gICAgICAgICMgZG8gYXBwLlBhcnRzLlNpZGViYXIuQ29udHJvbGxlci5zaG93XG5cbiAgICAgICAgJChkb2N1bWVudCkub24gJ21vdXNld2hlZWwnLCcuc2Nyb2xsYWJsZScsIChlKS0+XG4gICAgICAgICAgICBlID0gZSBvciB3aW5kb3cuZXZlbnQ7XG4gICAgICAgICAgICBwYXJlbnRIZWlnaHQgPSAkKHRoaXMpLnBhcmVudCgpLmhlaWdodCgpO1xuICAgICAgICAgICAgdG9Ub3AgPSBwYXJzZUludCgkKHRoaXMpWzBdLnN0eWxlLm1hcmdpblRvcCkgb3IgMDtcbiAgICAgICAgICAgIHNjcm9sbEhlaWdodCA9ICQodGhpcykuaGVpZ2h0KCkgLSBwYXJlbnRIZWlnaHQ7XG4gICAgICAgICAgICBzY3JvbGxTdGVwID0gdG9Ub3AgKyAoZS5kZWx0YVkgKiAzMCk7XG4gICAgICAgICAgICBpZiBzY3JvbGxIZWlnaHQgPiAwXG4gICAgICAgICAgICAgICAgaWYgc2Nyb2xsU3RlcCA+IDBcbiAgICAgICAgICAgICAgICAgICAgc2Nyb2xsU3RlcCA9IDA7XG4gICAgICAgICAgICAgICAgZWxzZSBpZiAtc2Nyb2xsU3RlcCA+IHNjcm9sbEhlaWdodFxuICAgICAgICAgICAgICAgICAgICBzY3JvbGxTdGVwID0gLXNjcm9sbEhlaWdodDtcblxuICAgICAgICAgICAgICAgICQodGhpcykuY3NzKCdtYXJnaW4tdG9wJzpzY3JvbGxTdGVwKydweCcpO1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZVxuXG4gICAgICAgICQoZG9jdW1lbnQpLm9uICdjbGljaycsJy5wbGF5ZXItc3RhcnQtZWxlbWVudCcsLT5cbiAgICAgICAgICAgICQoJy50cmFjay1pdGVtJykubm90KCcuZGlzYWJsZScpLmVxKDApLmNsaWNrKClcblxuXG4gICAgICAgIGlmICF3aW5kb3cuc2V0SW1tZWRpYXRlXG4gICAgICAgICAgICB3aW5kb3cuc2V0SW1tZWRpYXRlID0gZG8gPT5cbiAgICAgICAgICAgICAgICBoZWFkID0geyB9XG4gICAgICAgICAgICAgICAgdGFpbCA9IGhlYWRcblxuICAgICAgICAgICAgICAgIElEID0gTWF0aC5yYW5kb20oKVxuXG4gICAgICAgICAgICAgICAgb25tZXNzYWdlID0gKGUpLT5cbiAgICAgICAgICAgICAgICAgICAgaWYgZS5kYXRhICE9IElEXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm5cbiAgICAgICAgICAgICAgICAgICAgaGVhZCA9IGhlYWQubmV4dFxuICAgICAgICAgICAgICAgICAgICBmdW5jID0gaGVhZC5mdW5jXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBoZWFkLmZ1bmNcbiAgICAgICAgICAgICAgICAgICAgZnVuYygpXG5cbiAgICAgICAgICAgICAgICBpZiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignbWVzc2FnZScsIG9ubWVzc2FnZSk7XG4gICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuYXR0YWNoRXZlbnQoICdvbm1lc3NhZ2UnLCBvbm1lc3NhZ2UgKTtcblxuXG4gICAgICAgICAgICAgICAgcmV0dXJuIChmdW5jKS0+XG4gICAgICAgICAgICAgICAgICAgIHRhaWwgPSB0YWlsLm5leHQgPSB7IGZ1bmM6IGZ1bmMgfTtcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LnBvc3RNZXNzYWdlKElELCBcIipcIilcbiAgICAgICAgIyBkbyBhcHAuUGFydHMuVXNlck5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnN0YXJ0TGlzdGVuTm90aWZ5XG5cbiAgICBhcHAub24gJ2Vycm9yJywgLT5cbiAgICAgICAgY29uc29sZS5sb2cgJ2VycnJvcidcblxuICAgIHJldHVybiBhcHBcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuL3ZpZXcuY29mZmVlJywgJy4uLy4uL2NvbmZpZy9sb2cnLCcuLi9ub3RpZmljYXRpb24nXSwgKFBsYXllcixMb2dnZXIsTm90aWZpY2F0aW9uKS0+XG5cdHJlcXVpcmUoJy4vc3R5bGUuc3R5bCcpXG5cblx0Y2xhc3MgY29udHJvbGxlciBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXHRcdGluaXRpYWxpemU6IC0+XG5cdFx0XHRAbXlfalBsYXllciA9ICQoXCIjYmVlLXBsYXllclwiKTtcblx0XHRcdEBzdmZfcGF0aCA9ICcvcGFja2FnZXMvanBsYXllcl9uZXcvanF1ZXJ5LmpwbGF5ZXIuc3dmJztcblx0XHRcdCQualBsYXllci50aW1lRm9ybWF0LnBhZE1pbiA9IGZhbHNlO1xuXHRcdFx0JC5qUGxheWVyLnRpbWVGb3JtYXQucGFkU2VjID0gZmFsc2U7XG5cdFx0XHQkLmpQbGF5ZXIudGltZUZvcm1hdC5zZXBNaW4gPSBcIiBtaW4gXCI7XG5cdFx0XHQkLmpQbGF5ZXIudGltZUZvcm1hdC5zZXBTZWMgPSBcIiBzZWNcIjtcblx0XHRcdEBmaXJzdFBsYXkgPSB0cnVlXG5cdFx0XHRAdm9sdW1lID0gaWYgc2Vzc2lvblN0b3JhZ2Uudm9sdW1lIHRoZW4gc2Vzc2lvblN0b3JhZ2Uudm9sdW1lIGVsc2UgMC41XG5cblx0XHRcdEByZXBlYXQgPSBpZiBzZXNzaW9uU3RvcmFnZS5yZXBlYXQgdGhlbiBzZXNzaW9uU3RvcmFnZS5yZXBlYXQgZWxzZSBmYWxzZVxuXG5cdFx0XHRAc2h1ZmZsZSA9IGlmIHNlc3Npb25TdG9yYWdlLnNodWZmbGUgdGhlbiBzZXNzaW9uU3RvcmFnZS5zaHVmZmxlIGVsc2UgJ29mZidcblxuXHRcdFx0ZG8gQGluaXRpYWxpemVQbGF5ZXJcblxuXHRcdFx0QGxpc3RlblRvIGFwcCwgJ2FkZDp0bzpmYXZvcml0ZScsIC0+XG5cdFx0XHRcdGlmIEBwbGF5ZXJWaWV3XG5cdFx0XHRcdFx0QHBsYXllclZpZXcubW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZSwge3NpbGVudDogdHJ1ZX1cblx0XHRcdFx0XHQkKCcjcGxheWVyIC50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdhZGQnKS5hZGRDbGFzcygnZGVsZXRlJylcblxuXHRcdFx0QGxpc3RlblRvIGFwcCwgJ3JlbW92ZTpmcm9tOmZhdm9yaXRlJywgLT5cblx0XHRcdFx0aWYgQHBsYXllclZpZXdcblx0XHRcdFx0XHRAcGxheWVyVmlldy5tb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCBmYWxzZSwge3NpbGVudDogdHJ1ZX1cblx0XHRcdFx0XHQkKCcjcGxheWVyIC50cmFjay1jb250cm9sbCcpLmFkZENsYXNzKCdhZGQnKS5yZW1vdmVDbGFzcygnZGVsZXRlJylcblxuXG5cblx0XHRpbml0aWFsaXplUGxheWVyOiAtPlxuXHRcdFx0c2VsZiA9IEA7XG5cblx0XHRcdEBteV9qUGxheWVyLmpQbGF5ZXJcblxuXHRcdFx0XHRsb2FkZWRtZXRhZGF0YTogKGV2ZW50KS0+XG5cdFx0XHRcdFx0c2VsZi50cmFja0R1cmF0aW9uID0gZXZlbnQualBsYXllci5zdGF0dXMuZHVyYXRpb25cblxuXHRcdFx0XHRwbGF5OiAtPlxuXHRcdFx0XHRcdCQoJy5wbGF5ZXItY29udHJvbGwtZWxlbWVudCcpLnJlbW92ZUNsYXNzICdwbGF5J1xuXHRcdFx0XHRcdFx0LmFkZENsYXNzICdwYXVzZSdcblx0XHRcdFx0XHRhcHAudHJpZ2dlciAncGxheSdcblx0XHRcdFx0XHRzZWxmLnBsYXllckNsYXNzU3RhdHVzID0gJ3BhdXNlJ1xuXG5cdFx0XHRcdHBhdXNlOiAtPlxuXHRcdFx0XHRcdCQoJy5wbGF5ZXItY29udHJvbGwtZWxlbWVudCcpLnJlbW92ZUNsYXNzICdwYXVzZSdcblx0XHRcdFx0XHRcdC5hZGRDbGFzcyAncGxheSdcblx0XHRcdFx0XHRhcHAudHJpZ2dlciAncGF1c2UnXG5cdFx0XHRcdFx0c2VsZi5wbGF5ZXJDbGFzc1N0YXR1cyA9ICdwbGF5J1xuXG5cdFx0XHRcdHRpbWV1cGRhdGU6IChldmVudCktPlxuXHRcdFx0XHRcdGlmIHNlbGYuZHJhZ0lzQWN0aXZlIGlzbnQgdHJ1ZVxuXHRcdFx0XHRcdFx0aWYgcGFyc2VJbnQoZXZlbnQualBsYXllci5zdGF0dXMuY3VycmVudFBlcmNlbnRBYnNvbHV0ZSkgaXMgNTBcblx0XHRcdFx0XHRcdFx0TG9nZ2VyLmNyZWF0ZU5ld0xvZyBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9nZW5lcmFsVjEvbG9nL3N0cmVhbWluZ0hhbGZcIixcIj9pZD0je3NlbGYucGxheWVkVHJhY2t9JnJlZmVyZXI9I3thcHAuZ2V0UmVmZXJlcigpfVwiKVxuXHRcdFx0XHRcdFx0JCgnLnRpbWUtbGluZSAuYWN0aXZlLWxpbmUnKS5jc3MgJ3dpZHRoJywgZXZlbnQualBsYXllci5zdGF0dXMuY3VycmVudFBlcmNlbnRBYnNvbHV0ZSsnJSc7XG5cdFx0XHRcdFx0XHRpZiBwYXJzZUludChldmVudC5qUGxheWVyLnN0YXR1cy5jdXJyZW50VGltZSkgaXMgMjlcblx0XHRcdFx0XHRcdFx0TG9nZ2VyLmNyZWF0ZU5ld0xvZyBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9nZW5lcmFsVjEvbG9nL3N0cmVhbWluZzMwU2Vjb25kc1wiLFwiP2lkPSN7c2VsZi5wbGF5ZWRUcmFja30mcmVmZXJlcj0je2FwcC5nZXRSZWZlcmVyKCl9XCIpXG5cdFx0XHRcdFx0XHRzZWxmLl9zZXREdXJhdGlvbiBldmVudC5qUGxheWVyLnN0YXR1cy5kdXJhdGlvbixldmVudC5qUGxheWVyLnN0YXR1cy5jdXJyZW50VGltZVxuXG5cdFx0XHRcdGVuZGVkOiAtPlxuXHRcdFx0XHRcdExvZ2dlci5jcmVhdGVOZXdMb2cgYXBwLmdldFJlcXVlc3RVcmxTaWQoXCIvZ2VuZXJhbFYxL2xvZy9zdHJlYW1pbmdGdWxsXCIsXCI/aWQ9I3tzZWxmLnBsYXllZFRyYWNrfSZyZWZlcmVyPSN7YXBwLmdldFJlZmVyZXIoKX1cIilcblx0XHRcdFx0XHRzZWxmLm5leHRUcmNrKCk7XG5cblx0XHRcdFx0c3dmUGF0aDogc2VsZi5zdmZfcGF0aFxuXHRcdFx0XHRzdXBwbGllZDogXCJtcDNcIlxuXG5cdFx0c3RhcnRQbGF5ZXJBbmltYXRpb246IC0+XG5cdFx0XHRpZiBAZmlyc3RQbGF5XG5cdFx0XHRcdGNvbnNvbGUubG9nICQoJy5wc2V1ZG8tcGxheWVyJylcblx0XHRcdFx0JCgnLnBzZXVkby1wbGF5ZXInKS5mYWRlT3V0IDMwMCwgLT5cblx0XHRcdFx0XHQkKEApLnJlbW92ZSgpXG5cblx0XHRcdFx0QGZpcnN0UGxheSA9IGZhbHNlXG5cblx0XHRzdHJlYW1CeUlkOiAoaWQpLT5cblx0XHRcdHRyYWNrID0gQF9sb2FkVHJhY2sgaWRcblxuXHRcdFx0dHJhY2suZG9uZSAoZGF0YSk9PlxuXHRcdFx0XHRpZiBkYXRhLmNvZGU/IGFuZCBkYXRhLmNvZGUgaXMgMFxuXHRcdFx0XHRcdExvZ2dlci5jcmVhdGVOZXdMb2cgYXBwLmdldFJlcXVlc3RVcmxTaWQoXCIvZ2VuZXJhbFYxL2xvZy9zdHJlYW1pbmdTdGFydFwiLFwiP2lkPSN7QC5wbGF5ZWRUcmFja30mcmVmZXJlcj0je2FwcC5nZXRSZWZlcmVyKCl9XCIpXG5cdFx0XHRcdFx0QHBsYXkgZGF0YS51cmxcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3coe21lc3NhZ2U6ICfQntGI0LjQsdC60LAg0LLQvtGB0L/RgNC+0LjQt9Cy0LXQtNC10L3QuNGPJyx0eXBlOiAnZXJyb3InLHRpbWU6IDMwMDB9KVxuXG5cdFx0XHR0cmFjay5mYWlsIC0+XG5cdFx0XHRcdE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3coe21lc3NhZ2U6ICfQntGI0LjQsdC60LAg0LLQvtGB0L/RgNC+0LjQt9Cy0LXQtNC10L3QuNGPJyx0eXBlOiAnZXJyb3InLHRpbWU6IDMwMDB9KVxuXG5cblx0XHRwbGF5VHJhY2s6IChpZCxkYXRhLGhyZWYpLT5cblx0XHRcdGRvIEBzdGFydFBsYXllckFuaW1hdGlvblxuXHRcdFx0aWYgZGF0YSBhbmQgaHJlZlxuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwbGF5ZWQgdHJhY2snLCBkYXRhLnRpdGxlK1wiIC0gXCIraHJlZlxuXG5cdFx0XHRkbyBAc3RvcFxuXHRcdFx0aWYgaHJlZlxuXHRcdFx0XHRhcHAucGxheWVkSHJlZiA9IGhyZWZcblxuXHRcdFx0QHBsYXllZFRyYWNrID0gaWRcblx0XHRcdGlmIGRhdGFcblx0XHRcdFx0YXBwLnBsYXllZElkID0gZGF0YS51bmlxXG5cdFx0XHRcdEBzdHJlYW1CeUlkIGlkXG5cdFx0XHRcdEBzaG93UGxheWVyRGF0YSBkYXRhXG5cdFx0XHRlbHNlXG5cdFx0XHRcdHRyYWNrRGF0YSA9IEBfbG9hZFRyYWNrRGF0YSBpZFxuXHRcdFx0XHR0cmFja0RhdGEuZG9uZSAoZGF0YSk9PlxuXHRcdFx0XHRcdGlmIGRhdGEuY29kZT8gYW5kIGRhdGEuY29kZSBpcyAwXG5cdFx0XHRcdFx0XHRAc3RyZWFtQnlJZCBpZFxuXHRcdFx0XHRcdFx0QHNob3dQbGF5ZXJEYXRhIGRhdGEudHJhY2tzWzBdXG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2cgJ2Vycm9yJ1xuXG5cblxuXHRcdHBsYXlCeUNvbGxlY3Rpb246IChjb2xsZWN0aW9uLGlkLGhyZWYpLT5cblx0XHRcdG5vcm1hbGl6ZUNvbGxlY3Rpb24gPSBjb2xsZWN0aW9uLnJlamVjdCAoaXRlbSktPlxuXHRcdFx0XHRyZXR1cm4gaXRlbS5nZXQgJ2lzX2RlbGV0ZWQnXG5cdFx0XHRpZiAhaWRcblx0XHRcdFx0bGVuZ3RoID0gbm9ybWFsaXplQ29sbGVjdGlvbi5sZW5ndGgtMVxuXG5cdFx0XHRcdGlmIGxlbmd0aCBpcyAxXG5cdFx0XHRcdFx0aWQgPSBub3JtYWxpemVDb2xsZWN0aW9uWzFdLmdldCAnaWQnXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRtaWRkbGVPZkNvbGxlY3Rpb24gPSBhcHAuUmFuZEludCAxLGxlbmd0aC0xXG5cdFx0XHRcdFx0aWQgPSBub3JtYWxpemVDb2xsZWN0aW9uW21pZGRsZU9mQ29sbGVjdGlvbl0uZ2V0ICdpZCdcblxuXHRcdFx0QHBsYXlsaXN0ID0gQGNyZWF0ZVBsYXlsaXN0IGNvbGxlY3Rpb25cblx0XHRcdEBwbGF5VHJhY2sgaWQsQHBsYXlsaXN0W2lkXS50b0pTT04oKSxocmVmXG5cblx0XHRzaG93UGxheWVyRGF0YTogKGRhdGEpLT5cblx0XHRcdGRlbGV0ZSBkYXRhLnBsYXlsaXN0VHJhY2tcblxuXHRcdFx0aWYgIWRhdGEuaW5GYXZvcml0ZVxuXHRcdFx0XHRmYXZvcml0ZXMgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnRyYWNrcydcblxuXHRcdFx0XHRpZiAhZmF2b3JpdGVzLnByb21pc2Vcblx0XHRcdFx0XHRpdGVtID0gZmF2b3JpdGVzLnNvbWUgKGl0ZW0pPT5cblx0XHRcdFx0XHRcdHJldHVybiBpdGVtLmdldCgnaWQnKSBpcyBkYXRhLmlkXG5cdFx0XHRcdFx0aWYgaXRlbVxuXHRcdFx0XHRcdFx0ZGF0YS5pbkZhdm9yaXRlID0gdHJ1ZVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGRhdGEuaW5GYXZvcml0ZSA9IGZhbHNlXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRkYXRhLmluRmF2b3JpdGUgPSBmYWxzZVxuXG5cdFx0XHRpZiAhQHBsYXllclZpZXc/XG5cdFx0XHRcdEBzaG93UGxheWVyIGRhdGFcblxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRAcGxheWVyVmlldy5tb2RlbC5zZXQgZGF0YVxuXG5cblxuXHRcdHNob3dQbGF5ZXI6IChkYXRhKS0+XG5cdFx0XHRtb2RlbCA9IG5ldyBCYWNrYm9uZS5Nb2RlbCBkYXRhXG5cblx0XHRcdEBwbGF5ZXJWaWV3ID0gbmV3IFBsYXllci5WaWV3XG5cdFx0XHRcdG1vZGVsOiBtb2RlbFxuXG5cdFx0XHRhcHAucGxheWVyLnNob3cgQHBsYXllclZpZXdcblxuXHRcdFx0ZHJhZ1BsYXllciA9IG5ldyBTbGlkZXJEcmFnICcudGltZS1saW5lIC5hY3RpdmUtem9uZScsICdzb3VuZCcsIEAsICcudGltZS1saW5lIC5hY3RpdmUtbGluZSdcblx0XHRcdGRyYWdQbGF5ZXIucmVhZHkoKVxuXG5cdFx0XHRkcmFnU291bmQgPSBuZXcgU2xpZGVyRHJhZyAnLnZvbHVtZSAubGluZSAuYWN0aXZlJywgJ3ZvbHVtZScsIEAsICcudm9sdW1lIC5saW5lIC52YWx1ZSdcblx0XHRcdGRyYWdTb3VuZC5yZWFkeSgpXG5cblx0XHRcdGRvIEBzZXREZWZhdWx0Vm9sdW1lXG5cdFx0XHRkbyBAc2V0U2h1ZmZsZVxuXG5cdFx0c2V0RGVmYXVsdFZvbHVtZTogLT5cblx0XHRcdHZvbHVtZSA9ICtAdm9sdW1lXG5cdFx0XHRAX3NldFZvbHVtZSh2b2x1bWUpO1xuXHRcdFx0JCgnLnZvbHVtZSAubGluZSAudmFsdWUnKS53aWR0aCAodm9sdW1lKjEwMCkrJyUnXG5cblxuXG5cblx0I9CS0YHQv9C+0LzQvtCz0LDRgtC10LvRjNC90YvQtSDQvNC10YLQvtC00Ysg0LTQu9GPINC/0LvQtdC10YDQsFxuXG5cdCPQl9Cw0L/RgNC+0YEg0YHRgtGA0LjQvNC40L3Qs9CwINC/0L4gSWRcblx0XHQj0JLQvtC30LLRgNCw0YnQsNC10YIg0L/RgNC+0LzQuNGBXG5cblx0XHRfbG9hZFRyYWNrOiAoaWQpLT5cblx0XHRcdGlmIEB0cmFja0xvYWRcblx0XHRcdFx0QHRyYWNrTG9hZC5hYm9ydCgpXG5cblx0XHRcdEB0cmFja0xvYWQgPSAkLmdldCBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9jb21wYXRpYmlsaXR5L3N0cmVhbS9nZXRodHRwXCIsXCI/aXRlbWlkPSN7aWR9XCIpXG5cblx0XHRcdGRlZmVycmVkID0gJC5EZWZlcnJlZCgpXG5cblx0XHRcdEB0cmFja0xvYWQuZG9uZSAoZGF0YSktPlxuXHRcdFx0XHRkZWZlcnJlZC5yZXNvbHZlV2l0aCBALCBbZGF0YV1cblxuXHRcdFx0QHRyYWNrTG9hZC5mYWlsIC0+XG5cdFx0XHRcdGRlZmVycmVkLnJlamVjdFdpdGggQCwgYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBkZWZlcnJlZC5wcm9taXNlKClcblxuXHQj0JfQsNC/0YDQvtGBINC40L3RhNC+0YDQvNCw0YbQuNC4INC+INGC0YDQtdC60LUg0L/QviBJZFxuXHRcdCPQktC+0LfQstGA0LDRidCw0LXRgiDQv9GA0L7QvNC40YFcblx0XHRfbG9hZFRyYWNrRGF0YTogKGlkKS0+XG5cdFx0XHRpZiBAdHJhY2tEYXRhTG9hZFxuXHRcdFx0XHRAdHJhY2tEYXRhTG9hZC5hYm9ydCgpXG5cblx0XHRcdEB0cmFja0RhdGFMb2FkID0gJC5nZXQgJy9nZXRUcmFja0RhdGEvJytpZFxuXG5cdFx0XHRkZWZlcnJlZCA9ICQuRGVmZXJyZWQoKVxuXG5cdFx0XHRAdHJhY2tEYXRhTG9hZC5kb25lIChkYXRhKS0+XG5cdFx0XHRcdGRlZmVycmVkLnJlc29sdmVXaXRoIEAsIFtkYXRhXVxuXG5cdFx0XHRAdHJhY2tEYXRhTG9hZC5mYWlsIC0+XG5cdFx0XHRcdGRlZmVycmVkLnJlamVjdFdpdGggQCwgYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBkZWZlcnJlZC5wcm9taXNlKClcblxuXHRcdF9zZXREdXJhdGlvbjogKGR1cmF0aW9uLGN1cnJlbnRUaW1lKS0+XG5cdFx0XHR0aW1lID0gQF9jb252ZXJ0VHJhY2tEYXRhVG9UaW1lIGR1cmF0aW9uLCBjdXJyZW50VGltZVxuXHRcdFx0JCgnLnBsYXllci10aW1lci1kaXNwbGF5IC50aW1lLWZyb250JykudGV4dCh0aW1lLm5vcm1hbCk7XG5cdFx0XHQkKCcucGxheWVyLXRpbWVyLWRpc3BsYXkgLnRpbWUtZW5kJykudGV4dCh0aW1lLnJldmVyc2UpO1xuXG5cdFx0X2NvbnZlcnRUcmFja0RhdGFUb1RpbWU6IChkdXJhdGlvbixjdXJyZW50VGltZSktPlxuXHRcdFx0ZW5kRHVyYXRpb24gPSBkdXJhdGlvbiAtIGN1cnJlbnRUaW1lO1xuXHRcdFx0cmV0dXJuIHtcblx0XHRcdG5vcm1hbDogTWF0aC5mbG9vcihjdXJyZW50VGltZS82MCkrJzonKyhpZiBNYXRoLmZsb29yKGN1cnJlbnRUaW1lJTYwKSA8IDEwIHRoZW4gJzAnIGVsc2UgJycpK01hdGguZmxvb3IoY3VycmVudFRpbWUgJSA2MClcblx0XHRcdHJldmVyc2U6ICctICcrTWF0aC5mbG9vcihlbmREdXJhdGlvbi82MCkrJzonKyhpZiBNYXRoLmZsb29yKGVuZER1cmF0aW9uJTYwKSA8IDEwIHRoZW4gJzAnIGVsc2UgJycpK01hdGguZmxvb3IoZW5kRHVyYXRpb24gJSA2MClcblx0XHRcdH1cblxuXG5cdFx0cGxheTogKHVybCktPlxuXHRcdFx0QHBsYXllZCA9IHRydWVcblx0XHRcdGlmIHVybFxuXHRcdFx0XHRAbXlfalBsYXllci5qUGxheWVyIFwic2V0TWVkaWFcIixcblx0XHRcdFx0XHRtcDM6IHVybFxuXG5cdFx0XHRAbXlfalBsYXllci5qUGxheWVyICdwbGF5J1xuXG5cdFx0c2VlazogKHRpbWUpLT5cblx0XHRcdEBteV9qUGxheWVyLmpQbGF5ZXIgJ3BsYXknLCB0aW1lXG5cblx0XHRzdG9wOiAtPlxuXHRcdFx0QG15X2pQbGF5ZXIualBsYXllciAnc3RvcCdcblxuXHRcdHBhdXNlOiAtPlxuXHRcdFx0QHBsYXllZCA9IGZhbHNlXG5cdFx0XHRAbXlfalBsYXllci5qUGxheWVyICdwYXVzZSdcblxuXHRcdG5leHRUcmNrOiAtPlxuXHRcdFx0aWYgQHBsYXlsaXN0XG5cblx0XHRcdFx0aWYgQHNodWZmbGUgaXMgJ29uJ1xuXHRcdFx0XHRcdHRyYWNrID0gQHBsYXlsaXN0W0BwbGF5bGlzdFtAcGxheWVkVHJhY2tdWyduZXh0U2h1ZmZsZVRyY2snXV1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHRyYWNrID0gQHBsYXlsaXN0W0BwbGF5bGlzdFtAcGxheWVkVHJhY2tdWyduZXh0VHJjayddXVxuXG5cblx0XHRcdFx0aWYgdHJhY2s/XG5cdFx0XHRcdFx0QHBsYXlUcmFjayB0cmFjay5pZCwgdHJhY2sudG9KU09OKClcblxuXHRcdHByZXZUcmNrOiAtPlxuXHRcdFx0aWYgQHBsYXlsaXN0XG5cblx0XHRcdFx0aWYgQHNodWZmbGUgaXMgJ29uJ1xuXHRcdFx0XHRcdHRyYWNrID0gQHBsYXlsaXN0W0BwbGF5ZWRUcmFja11bJ3ByZXZTaHVmZmxlVHJjayddXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRpZiBAcGxheWxpc3RbQHBsYXllZFRyYWNrXVsncHJld1RyY2snXVxuXHRcdFx0XHRcdFx0dHJhY2sgPSBAcGxheWxpc3RbQHBsYXlsaXN0W0BwbGF5ZWRUcmFja11bJ3ByZXdUcmNrJ11dXG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0dHJhY2sgPSBAcGxheWxpc3RbQHBsYXlsaXN0W0BwbGF5ZWRUcmFja11bJ2xhc3RUcmFjayddXVxuXG5cdFx0XHRcdGlmIHRyYWNrP1xuXHRcdFx0XHRcdEBwbGF5VHJhY2sgdHJhY2suaWQsIHRyYWNrLnRvSlNPTigpXG5cblx0XHRfc2V0Vm9sdW1lOiAodm9sdW1lKS0+XG5cdFx0XHR2ID0gK3ZvbHVtZTtcblx0XHRcdGlmIHYgPiAxXG5cdFx0XHRcdHYgPSAxXG5cdFx0XHRzZXNzaW9uU3RvcmFnZS52b2x1bWUgPSB2XG5cdFx0XHRAdm9sdW1lID0gdlxuXHRcdFx0QG15X2pQbGF5ZXIualBsYXllciBcInZvbHVtZVwiLCB2XG5cblxuXHRcdG9wZW5PcHRpb25zOiAoaXRlbSktPlxuXHRcdFx0aWYgaXRlbS5tb2RlbC5oYXMgJ29wdGlvbnNMaXN0J1xuXHRcdFx0XHRpZiBpdGVtLm1vZGVsLmhhcyAncGxheWxpc3RUcmFjaydcblx0XHRcdFx0XHRsaXN0ID0gW1xuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHR0aXRsZTogJ9GD0LTQsNC70LjRgtGMINC40Lcg0L/Qu9C10LnQu9C40YHRgtCwJ1xuXHRcdFx0XHRcdFx0XHRjbGFzczogJ2RlbGV0ZS1mcm9tLXBsYXlsaXN0J1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdF1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdGlmIGl0ZW0ubW9kZWwuZ2V0ICdpbkZhdm9yaXRlJ1xuXHRcdFx0XHRcdFx0bGlzdCA9IFtcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHRpdGxlOiAn0YPQtNCw0LvQuNGC0Ywg0LjQtyDQvNC10LTQuNCw0YLQtdC60LgnXG5cdFx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdkZWxldGUnXG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdF1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRsaXN0ID0gW1xuXHRcdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdFx0dGl0bGU6ICfQtNC+0LHQsNCy0LjRgtGMINCyINC80LXQtNC40LDRgtC10LrRgydcblx0XHRcdFx0XHRcdFx0XHRjbGFzczogJ2FkZCdcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XVxuXG5cdFx0XHRcdF8uZWFjaCBpdGVtLm1vZGVsLmdldCgnb3B0aW9uc0xpc3QnKSwgKGl0ZW0pLT5cblx0XHRcdFx0XHRsaXN0LnB1c2ggaXRlbVxuXG5cdFx0XHRcdGNvbGxlY3Rpb24gPSBuZXcgQmFja2JvbmUuQ29sbGVjdGlvbiBsaXN0XG5cdFx0XHRlbHNlXG5cdFx0XHRcdGNvbGxlY3Rpb24gPSBuZXcgQmFja2JvbmUuQ29sbGVjdGlvblxuXG5cdFx0XHRpZiBpdGVtLm1vZGVsLmdldCAnYXJ0aXN0X2lzX3ZhJ1xuXHRcdFx0XHRvcHRzID0gY29sbGVjdGlvbi5maW5kV2hlcmVcblx0XHRcdFx0XHRhcnRpc3Q6IHRydWVcblx0XHRcdFx0aWYgb3B0c1xuXHRcdFx0XHRcdG9wdHMucmVtb3ZlXG5cdFx0XHRcdFx0XHRzaWxlbnQ6dHJ1ZVxuXG5cdFx0XHRsYXlvdXQgPSBuZXcgUGxheWVyLk9wdGlvbnNMYXlvdXRcblx0XHRcdFx0bW9kZWw6IGl0ZW0ubW9kZWxcblx0XHRcdFx0Y29sbGVjdGlvbjogY29sbGVjdGlvblxuXG5cdFx0XHRpdGVtLm9wdGlvbnNSZWdpb24uc2hvdyBsYXlvdXRcblxuXG5cdFx0b3BlblBsYXlsaXN0c0xpc3Q6IChpdGVtKS0+XG5cblx0XHRcdHBsYXlsaXN0ID0gYXBwLnJlcXVlc3QgJ2dldDpmYXZvcml0ZTpwbGF5bGlzdHMnXG5cdFx0XHRpZiAhcGxheWxpc3QucHJvbWlzZVxuXHRcdFx0XHR2aWV3ID0gbmV3IFBsYXllci5QbGF5bGlzdEl0ZW1zXG5cdFx0XHRcdFx0bW9kZWw6IGl0ZW0ubW9kZWxcblx0XHRcdFx0XHRjb2xsZWN0aW9uOiBwbGF5bGlzdFxuXG5cdFx0XHRcdGl0ZW0ub3B0aW9uc1JlZ2lvbi5zaG93IHZpZXdcblxuXHRcdHJlbW92ZVRyYWNrRnJvbUZhdm9yaXRlOiAoaWQpLT5cblx0XHRcdGFwcC5yZXF1ZXN0ICdnZXQ6ZmF2b3JpdGU6dHJhY2tzJ1xuXHRcdFx0LmRvbmUgKGZhdm9yaXRlcyktPlxuXHRcdFx0XHR0cmFjayA9IGZhdm9yaXRlcy5maW5kV2hlcmVcblx0XHRcdFx0XHRpZDogaWRcblxuXHRcdFx0XHRpZiB0cmFja1xuXHRcdFx0XHRcdGRvIHRyYWNrLmRlc3Ryb3lcblxuXHRcdFx0XHRcdE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3coe21lc3NhZ2U6IFwi0KLRgNC10Logwqsje3RyYWNrLmdldCAndGl0bGUnIH3CuyDRg9C00LDQu9C10L0g0LjQtyDQuNC30LHRgNCw0L3QvdC+0LPQvlwiLHR5cGU6ICdzdWNjZXNzJyx0aW1lOiAzMDAwfSlcblxuXHRcdGFkZFRyYWNrVG9GYXZvcml0ZTogKHRyYWNrKS0+XG5cdFx0XHRhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnRyYWNrcydcblx0XHRcdC5kb25lIChmYXZvcml0ZXMpLT5cblx0XHRcdFx0bW9kZWwgPSB0cmFjay50b0pTT04oKVxuXHRcdFx0XHRpZiAhbW9kZWwuZmF2b3VyaXRlcyBvciBtb2RlbC5mYXZvdXJpdGVzLnNvcnQgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdFx0bW9kZWwuZmF2b3VyaXRlcyA9IHt9XG5cdFx0XHRcdFx0bW9kZWwuZmF2b3VyaXRlcy5zb3J0ID0gMTAwMDBcblxuXHRcdFx0XHRmYXZvcml0ZXMuY3JlYXRlIG1vZGVsXG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ9CU0L7QsdCw0LLQu9C10L3QuNC1INGC0YDQtdC60LAg0LIg0LjQt9Cx0YDQsNC90L3QvtC1JywgdHJhY2suZ2V0KCd0aXRsZScpKycgLSAnK2xvY2F0aW9uLmhyZWZcblx0XHRcdFx0Tm90aWZpY2F0aW9uLkNvbnRyb2xsZXIuc2hvdyh7bWVzc2FnZTogXCLQotGA0LXQuiDCqyN7dHJhY2suZ2V0ICd0aXRsZScgfcK7INC00L7QsdCw0LLQu9C10L0g0LIg0LzQtdC00LjQsNGC0LXQutGDXCIsdHlwZTogJ3N1Y2Nlc3MnLHRpbWU6IDMwMDB9KVxuXG5cdFx0Y3JlYXRlRmF2b3VyaXRlUGxheWxpc3Q6ICh0aXRsZSx0cmFjayktPlxuXHRcdFx0cGxheWxpc3QgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnBsYXlsaXN0cydcblx0XHRcdHBsYXlsaXN0Q3JlYXRlID0gcGxheWxpc3QuY3JlYXRlIHsndGl0bGUnOiB0aXRsZSx0cmFja3M6IFt0cmFjay5tb2RlbC50b0pTT04oKV19LHt3YWl0OnRydWV9XG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyIC5iYWNrLXRvLW9wdGlvbnMnKS5oaWRlKClcblx0XHRcdCQoJy5vcHRpb25zLXdyYXBwZXIgLm9wdGlvbnMtcGxheWxpc3Qtc2Nyb2xsLXdyYXBwJykuaGlkZSgpXG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyJykuY3NzKCdtaW4taGVpZ2h0JywgMzAwKydweCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxvYWRlclwiIHN0eWxlPVwidG9wOjEzMHB4XCI+PC9kaXY+Jyk7XG5cblx0XHRcdHBsYXlsaXN0Q3JlYXRlLm9uY2UgJ3N5bmMnLCAocGxheWxpc3QpPT5cblx0XHRcdFx0QGFkZFRyYWNrVG9QbGF5bGlzdCBwbGF5bGlzdC5pZCx0cmFjay5tb2RlbFxuXG5cdFx0YWRkVHJhY2tUb1BsYXlsaXN0OiAocGxheWxpc3RJZCx0cmFjayktPlxuXHRcdFx0cGxheWxpc3RJZCA9ICtwbGF5bGlzdElkXG5cdFx0XHRwbGF5bGlzdHMgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnBsYXlsaXN0cydcblxuXHRcdFx0cGxheWxpc3QgPSBwbGF5bGlzdHMuZmluZFdoZXJlXG5cdFx0XHRcdGlkOiBwbGF5bGlzdElkXG5cblx0XHRcdHdpbmRvdy50ZXN0ID0gcGxheWxpc3RcblxuXHRcdFx0c3VjY2VzcyA9IChkYXRhKS0+XG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gXCLQlNC+0LHQsNCy0LvQtdC90LjQtSDRgtGA0LXQutCwINCyINC/0LvQtdC50LvQuNGB0YIgI3twbGF5bGlzdElkfVwiLCB0cmFjay5nZXQoJ2FydGlzdCcpKycgLSAnK3RyYWNrLmdldCgndGl0bGUnKVxuXHRcdFx0XHRzdWNjZXNzV2luZG93ID0gJzxkaXYgY2xhc3M9XCJvcHRpb25zLW92ZXJsYXlcIj48L2Rpdj48ZGl2IGNsYXNzPVwic3VjY2Vzcy1pY29uXCI+PC9kaXY+PHAgY2xhc3M9XCJzdWNjZXNzLXRpdGxlXCI+0JPQvtGC0L7QstC+PC9wPidcblxuXHRcdFx0XHR0cmFja0RhdGEgPSB0cmFjay50b0pTT04oKVxuXHRcdFx0XHR0cmFja0RhdGEucGxheWxpc3QgPSBkYXRhLnBsYXlsaXN0XG5cblx0XHRcdFx0aWYgcGxheWxpc3QuaGFzICd0cmFja3MnXG5cdFx0XHRcdFx0Y29sbGVjdGlvbiA9IF8udW5pb24gcGxheWxpc3QuZ2V0KCd0cmFja3MnKSxbdHJhY2tEYXRhXVxuXHRcdFx0XHRcdGNvdW50ID0gY29sbGVjdGlvbi5sZW5ndGhcblx0XHRcdFx0XHRwbGF5bGlzdC5zZXRcblx0XHRcdFx0XHRcdHRyYWNrczogY29sbGVjdGlvblxuXHRcdFx0XHRcdFx0aXRlbXNfY291bnQ6IGNvdW50XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRwbGF5bGlzdC5zZXRcblx0XHRcdFx0XHRcdHRyYWNrczogW3RyYWNrRGF0YV1cblx0XHRcdFx0XHRcdGl0ZW1zX2NvdW50OiAxXG5cblx0XHRcdFx0JCgnLm9wdGlvbnMtd3JhcHBlcicpLmVtcHR5KCkuYXBwZW5kKHN1Y2Nlc3NXaW5kb3cpO1xuXG5cblx0XHRcdFx0c2V0VGltZW91dCAtPlxuXHRcdFx0XHRcdCQoJy5vcHRpb25zLXdyYXBwZXInKS5mYWRlT3V0IDMwMCwgLT5cblx0XHRcdFx0XHRcdCQodGhpcykucmVtb3ZlKClcblx0XHRcdFx0LDEwMDBcblxuXHRcdFx0aWYgcGxheWxpc3Rcblx0XHRcdFx0JCgnLm9wdGlvbnMtd3JhcHBlcicpLmNzcygnbWluLWhlaWdodCcsIDMwMCsncHgnKS5lbXB0eSgpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxvYWRlclwiIHN0eWxlPVwidG9wOjEzMHB4XCI+PC9kaXY+Jyk7XG5cdFx0XHRcdCQuYWpheFxuXHRcdFx0XHRcdHR5cGU6IFwiR0VUXCIsXG5cdFx0XHRcdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9wbGF5bGlzdC9hZGRpdGVtXCIsXCI/Y29udGVudGlkPSN7dHJhY2suZ2V0KCdpZCcpfSZwbGF5bGlzdGlkPSN7cGxheWxpc3RJZH1cIilcblx0XHRcdFx0XHRzdWNjZXNzOiBzdWNjZXNzXG5cblx0I9CS0YHQv9C+0LzQvtCz0LDRgtC10LvRjNC90YvQtSDQvNC10YLQvtC00Ysg0LTQu9GPINC/0L7RgNGC0LDQu9CwXG5cblx0XHQj0KHQvtC30LTQsNC90LjQtSDQv9C70LXQudC70LjRgdGC0LAg0LjQtyDQutC+0LvQu9C10LrRhtC40Lgg0LTQu9GPINC/0L7RgdC70LXQtNGD0Y7RidC10Lkg0LXQtSDQv9C10YDQtdC00LDRh9C4INCyINC/0LvQtdC10YBcblx0XHRjcmVhdGVQbGF5bGlzdDogKGNvbGxlY3Rpb24pLT5cblx0XHRcdG5vcm1hbCA9IGNvbGxlY3Rpb24ucmVqZWN0IChpdGVtKS0+XG5cdFx0XHRcdHJldHVybiBpdGVtLmdldCAnaXNfZGVsZXRlZCdcblxuXG5cdFx0XHRzaHVmZmxlSWQgPSBfLnNodWZmbGUgXy5wbHVjayBub3JtYWwsJ2lkJ1xuXHRcdFx0Xy5lYWNoIG5vcm1hbCwgKGl0ZW0saW5kZXgpLT5cblxuXHRcdFx0XHRpdGVtLm5leHRUcmNrID0gaWYgbm9ybWFsW2luZGV4KzFdPyB0aGVuIG5vcm1hbFtpbmRleCsxXVsnaWQnXSBlbHNlIG51bGxcblx0XHRcdFx0aXRlbS5wcmV3VHJjayA9IGlmIGluZGV4IGlzbnQgMCB0aGVuIG5vcm1hbFtpbmRleC0xXVsnaWQnXSBlbHNlIG51bGxcblx0XHRcdFx0aXRlbS5uZXh0U2h1ZmZsZVRyY2sgPSBpZiBzaHVmZmxlSWRbaW5kZXhdPyB0aGVuIHNodWZmbGVJZFtpbmRleF0gZWxzZSBub3JtYWxbMF0uaWRcblx0XHRcdFx0aXRlbS5wcmV2U2h1ZmZsZVRyY2sgPSBpZiBzaHVmZmxlSWRbaW5kZXgtMV0/IHRoZW4gc2h1ZmZsZUlkW2luZGV4LTFdIGVsc2Ugbm9ybWFsW25vcm1hbC5sZW5ndGgtMV0uaWRcblx0XHRcdFx0aXRlbS5maXJzdFRyYWNrID0gbm9ybWFsWzBdLmlkXG5cdFx0XHRcdGl0ZW0ubGFzdFRyYWNrID0gbm9ybWFsW25vcm1hbC5sZW5ndGgtMV0uaWRcblxuXHRcdFx0cmV0dXJuIF8uaW5kZXhCeSBub3JtYWwsICdpZCdcblxuXG5cblx0XHR0b2dnbGVTaHVmZmxlOiAtPlxuXHRcdFx0aWYgQHNodWZmbGUgaXMgJ29uJ1xuXHRcdFx0XHRAc2h1ZmZsZSA9IHNlc3Npb25TdG9yYWdlLnNodWZmbGUgPSAnb2ZmJ1xuXHRcdFx0XHQkKCcubWFpbi1wbGF5ZXIgLm90aGVyLWJ1dHRvbnMgLnNodWZmbGUnKS5yZW1vdmVDbGFzcyAnYWN0aXZlJ1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHRAc2h1ZmZsZSA9IHNlc3Npb25TdG9yYWdlLnNodWZmbGUgPSAnb24nXG5cdFx0XHRcdCQoJy5tYWluLXBsYXllciAub3RoZXItYnV0dG9ucyAuc2h1ZmZsZScpLmFkZENsYXNzICdhY3RpdmUnXG5cblx0XHRzZXRTaHVmZmxlOiAtPlxuXHRcdFx0aWYgQHNodWZmbGUgaXMgJ29uJ1xuXHRcdFx0XHQkKCcubWFpbi1wbGF5ZXIgLm90aGVyLWJ1dHRvbnMgLnNodWZmbGUnKS5hZGRDbGFzcyAnYWN0aXZlJ1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHQkKCcubWFpbi1wbGF5ZXIgLm90aGVyLWJ1dHRvbnMgLnNodWZmbGUnKS5yZW1vdmVDbGFzcyAnYWN0aXZlJ1xuXG5cblxuXG5cblxuXG5cblxuXHRcdGNsYXNzIFNsaWRlckRyYWdcblx0XHRcdGNvbnN0cnVjdG9yOiAoQGVsZW1lbnQsQHR5cGUsQGNvbnRleHQsQGFjdGl2ZSktPlxuXG5cdFx0XHRyZWFkeTogLT5cblx0XHRcdFx0JChkb2N1bWVudCkub24gJ21vdXNlZG93bicsIEBlbGVtZW50LCAoZSk9PlxuXHRcdFx0XHRcdEBkb3duKGUpXG5cblx0XHRcdGRvd246IChlKS0+XG5cdFx0XHRcdGVsVG9MZWZ0ID0gJChAZWxlbWVudCkub2Zmc2V0KCkubGVmdDtcblx0XHRcdFx0cGFnZVRvTGVmdCA9IGUucGFnZVg7XG5cdFx0XHRcdHdpZHRoVmFsID0gcGFnZVRvTGVmdCAtIGVsVG9MZWZ0O1xuXG5cdFx0XHRcdCQoQGFjdGl2ZSkud2lkdGgod2lkdGhWYWwrJ3B4Jyk7XG5cblx0XHRcdFx0aWYgQHR5cGUgaXMgJ3NvdW5kJ1xuXHRcdFx0XHRcdEBjb250ZXh0LmRyYWdJc0FjdGl2ZSA9IHRydWVcblxuXHRcdFx0XHQkKGRvY3VtZW50KS5vbiBcIm1vdXNlbW92ZS5kcmFnZXJcIiwgKGUpPT5cblx0XHRcdFx0XHRAbW92ZShlKTtcblxuXHRcdFx0XHQkKGRvY3VtZW50KS5vbiBcIm1vdXNldXAuZHJhZ2VyXCIsIChlKT0+XG5cdFx0XHRcdFx0ZWxUb0xlZnQgPSAkKEBlbGVtZW50KS5vZmZzZXQoKS5sZWZ0O1xuXHRcdFx0XHRcdHBhZ2VUb0xlZnQgPSBlLnBhZ2VYO1xuXHRcdFx0XHRcdHdpZHRoVmFsID0gcGFnZVRvTGVmdCAtIGVsVG9MZWZ0O1xuXHRcdFx0XHRcdGlmIEB0eXBlIGlzICdzb3VuZCdcblx0XHRcdFx0XHRcdEBjb250ZXh0LmRyYWdJc0FjdGl2ZSA9IGZhbHNlXG5cblx0XHRcdFx0XHRcdHBhcmNlbnRXaWR0aCA9ICh3aWR0aFZhbCAqIDEwMCkgLyAkKEBlbGVtZW50KS53aWR0aCgpXG5cdFx0XHRcdFx0XHRzb3VuZFNlZWsgPSAoQGNvbnRleHQudHJhY2tEdXJhdGlvbiAqIHBhcmNlbnRXaWR0aCkgLyAxMDBcblxuXHRcdFx0XHRcdFx0QGNvbnRleHQuc2VlayBzb3VuZFNlZWtcblxuXHRcdFx0XHRcdGlmIEB0eXBlIGlzICd2b2x1bWUnXG5cdFx0XHRcdFx0XHRAY29udGV4dC5fc2V0Vm9sdW1lIHdpZHRoVmFsLyQoQGVsZW1lbnQpLndpZHRoKClcblxuXHRcdFx0XHRcdCQoZG9jdW1lbnQpLm9mZiBcIi5kcmFnZXJcIlxuXG5cdFx0XHRtb3ZlOiAoZSktPlxuXHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdGVsVG9MZWZ0ID0gJChAZWxlbWVudCkub2Zmc2V0KCkubGVmdDtcblx0XHRcdFx0cGFnZVRvTGVmdCA9IGUucGFnZVg7XG5cdFx0XHRcdHdpZHRoVmFsID0gcGFnZVRvTGVmdCAtIGVsVG9MZWZ0O1xuXHRcdFx0XHRwYXJlbnRXaWR0aCA9ICQoQGVsZW1lbnQpLndpZHRoKCk7XG5cdFx0XHRcdGlmIHdpZHRoVmFsIDw9IDBcblx0XHRcdFx0XHR3aWR0aFZhbCA9IDA7XG5cdFx0XHRcdGlmIHBhcmVudFdpZHRoIDw9IHdpZHRoVmFsXG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXG5cdFx0XHRcdCQoQGFjdGl2ZSkud2lkdGgod2lkdGhWYWwrJ3B4Jyk7XG5cblx0XHRcdFx0aWYgQHR5cGUgaXMgJ3ZvbHVtZSdcblx0XHRcdFx0XHRAY29udGV4dC5fc2V0Vm9sdW1lIHdpZHRoVmFsLyQoQGVsZW1lbnQpLndpZHRoKClcblxuXHRcdFx0XHRpZiBAdHlwZSBpcyAnc291bmQnXG5cdFx0XHRcdFx0QHNvdW5kTW92ZSBlXG5cblxuXHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cblx0XHRcdHNvdW5kTW92ZTogKGUpLT5cblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRlbFRvTGVmdCA9ICQoQGVsZW1lbnQpLm9mZnNldCgpLmxlZnQ7XG5cdFx0XHRcdHBhZ2VUb0xlZnQgPSBlLnBhZ2VYO1xuXHRcdFx0XHR3aWR0aFZhbCA9IHBhZ2VUb0xlZnQgLSBlbFRvTGVmdDtcblx0XHRcdFx0cGFyZW50V2lkdGggPSAkKEBlbGVtZW50KS53aWR0aCgpO1xuXHRcdFx0XHRpZiB3aWR0aFZhbCA8PSAwXG5cdFx0XHRcdFx0d2lkdGhWYWwgPSAwO1xuXHRcdFx0XHRpZiBwYXJlbnRXaWR0aCA8PSB3aWR0aFZhbFxuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblxuXHRcdFx0XHRwYXJjZW50V2lkdGggPSAod2lkdGhWYWwgKiAxMDApIC8gcGFyZW50V2lkdGhcblx0XHRcdFx0c291bmRTZWVrID0gKEBjb250ZXh0LnRyYWNrRHVyYXRpb24gKiBwYXJjZW50V2lkdGgpIC8gMTAwO1xuXG5cdFx0XHRcdGlmIEBjb250ZXh0LnRyYWNrRHVyYXRpb24/XG5cdFx0XHRcdFx0dGltZSA9IEBjb250ZXh0Ll9jb252ZXJ0VHJhY2tEYXRhVG9UaW1lIHdpZHRoVmFsLHNvdW5kU2Vla1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0dGltZSA9XG5cdFx0XHRcdFx0XHRub3JtYWw6ICctLTotLSdcblx0XHRcdFx0XHRcdHJldmVyc2U6ICctLTotLSdcblxuXHRcdFx0XHQkKCcucGxheWVyLXRpbWVyLWRpc3BsYXkgLnRpbWUtZnJvbnQnKS50ZXh0KHRpbWUubm9ybWFsKTtcblx0XHRcdFx0JCgnLnBsYXllci10aW1lci1kaXNwbGF5IC50aW1lLWVuZCcpLnRleHQodGltZS5yZXZlcnNlKTtcblxuXG5cdFBsYXllci5Db250cm9sbGVyID0gbmV3IGNvbnRyb2xsZXJcblx0cmV0dXJuIFBsYXllclxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL3BsYXllci9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgLT5cblx0UGxheWVyID0ge31cblxuXHRjbGFzcyBQbGF5ZXIuVmlldyBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogKGRhdGEpLT5cblx0XHRcdGRhdGEuaW1nID0gYXBwLmNyZWF0ZUltZ0xpbmsoZGF0YS5pbWcsJzUweDUwJylcblx0XHRcdHRlbXBsYXRlID0gcmVxdWlyZSgnLi90ZW1wbGF0ZXMvbWFpbi10cGwuamFkZScpXG5cdFx0XHRyZXR1cm4gdGVtcGxhdGUoZGF0YSlcblxuXHRcdGNsYXNzTmFtZTogJ3BsYXllci1jb250YWluZXInXG5cdFx0b25TaG93OiAtPlxuXHRcdFx0QCQoJy5wbGF5ZXItd3JhcHAnKS5mYWRlSW4gMzAwXG5cblx0XHRvblJlbmRlcjogLT5cblx0XHRcdG9wdGlvbnMgPSBuZXcgTWFyaW9uZXR0ZS5SZWdpb25cblx0XHRcdFx0ZWw6IEAkKCcub3B0aW9ucy1yZWdpb24nKVxuXHRcdFx0QG9wdGlvbnNSZWdpb24gPSBvcHRpb25zXG5cblx0XHRtb2RlbEV2ZW50czogLT5cblx0XHRcdCdjaGFuZ2UnOiAtPlxuXHRcdFx0XHRkbyBAcmVuZGVyXG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnNldERlZmF1bHRWb2x1bWVcblx0XHRcdFx0QCQoJy5wbGF5ZXItd3JhcHAnKS5zaG93KClcblxuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAucGxheWVyLXRpbWVyLWRpc3BsYXknOiAtPlxuXHRcdFx0XHRkbyBAJCgnLnRpbWUtZnJvbnQnKS50b2dnbGVcblx0XHRcdFx0ZG8gQCQoJy50aW1lLWVuZCcpLnRvZ2dsZVxuXG5cdFx0XHQnY2xpY2sgLmZ3ZC1hcnJvdyc6IC0+XG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLm5leHRUcmNrXG5cblx0XHRcdCdjbGljayAuYmFjay1hcnJvdyc6IC0+XG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnByZXZUcmNrXG5cblx0XHRcdCdjbGljayAucGxheSc6IC0+XG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnBsYXlcblxuXHRcdFx0J2NsaWNrIC5wYXVzZSc6IC0+XG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnBhdXNlXG5cblx0XHRcdCdjbGljayAuc2h1ZmZsZSc6IC0+XG5cdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnRvZ2dsZVNodWZmbGVcblxuXHRcdFx0J2NsaWNrIC5vcHRpb25zLXJlZ2lvbic6IC0+XG5cdFx0XHRcdFBsYXllci5Db250cm9sbGVyLm9wZW5PcHRpb25zIEBcblxuXHRcdFx0J2NsaWNrIC5hcnRpc3ROYW1lJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YXJ0aXN0JyxAbW9kZWwuZ2V0KCdhcnRpc3RpZCcpXG5cblx0XHRcdCdjbGljayAuc291bmROYW1lJzogLT5cblx0XHRcdFx0aWYgIUBtb2RlbC5nZXQoJ2FydGlzdF9pc192YScpXG5cdFx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YWxidW0nLEBtb2RlbC5nZXQoJ3JlbGVhc2VfaWQnKVxuXG5cblxuXG5cdFx0XHQjb3B0aW9ucyBldmVudHNcblxuXHRcdFx0J2NsaWNrIC50cmFjay1jb250cm9sbC5hZGQnOiAtPlxuXHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5hZGRUcmFja1RvRmF2b3JpdGUgQG1vZGVsXG5cdFx0XHRcdEBtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCB0cnVlLCB7c2lsZW50OiB0cnVlfVxuXHRcdFx0XHRAJCgnLnRyYWNrLWNvbnRyb2xsJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC50cmFjay1jb250cm9sbC5kZWxldGUnOiAtPlxuXHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5yZW1vdmVUcmFja0Zyb21GYXZvcml0ZSBAbW9kZWwuZ2V0ICdpZCdcblx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIGZhbHNlLCB7c2lsZW50OiB0cnVlfVxuXHRcdFx0XHRAJCgnLnRyYWNrLWNvbnRyb2xsJykucmVtb3ZlQ2xhc3MoJ2RlbGV0ZScpLmFkZENsYXNzKCdhZGQnKVxuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5vcHRpb25zLXdyYXBwZXInOiAtPlxuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5hZGQtdG8tcGxheWxpc3QnOiAtPlxuXHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5vcGVuUGxheWxpc3RzTGlzdCBAXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnY2xpY2sgLmJhY2stdG8tb3B0aW9ucyc6IC0+XG5cdFx0XHRcdFBsYXllci5Db250cm9sbGVyLm9wZW5PcHRpb25zIEBcblx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRcdCdjbGljayAub3B0aW9ucy1wbGF5bGlzdC1pdGVtJzogKGUpLT5cblx0XHRcdFx0UGxheWVyLkNvbnRyb2xsZXIuYWRkVHJhY2tUb1BsYXlsaXN0ICQoZS50YXJnZXQpLmF0dHIoJ2RhdGEtaWQnKSxAbW9kZWxcblxuXHRcdFx0J2NsaWNrIC5vcHRpb25zLW9wZW4tYWxidW0nOiAtPlxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphbGJ1bScsIEBtb2RlbC5nZXQoJ3JlbGVhc2VfaWQnKVxuXHRcdFx0XHRkbyBAY2xvc2VPcHRpb25zXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnY2xpY2sgLm9wdGlvbnMtb3Blbi1hcnRpc3QnOiAtPlxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphcnRpc3QnLCBAbW9kZWwuZ2V0KCdhcnRpc3RpZCcpXG5cdFx0XHRcdGRvIEBjbG9zZU9wdGlvbnNcblx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRcdCdrZXlwcmVzcyAubmV3LXBsYXlsaXN0IGlucHV0JzogKGUpLT5cblxuXHRcdFx0XHRpZiBlLmtleUNvZGUgaXMgMTNcblx0XHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5jcmVhdGVGYXZvdXJpdGVQbGF5bGlzdCAkKCcubmV3LXBsYXlsaXN0IGlucHV0JykudmFsKCksQFxuXG5cdFx0XHQnbW91c2VsZWF2ZSc6ICdjbG9zZU9wdGlvbnMnXG5cdFx0XHQnY2xpY2sgLm9wdGlvbnMtb3ZlcmxheSc6ICdjbG9zZU9wdGlvbnMnXG5cblx0XHRjbG9zZU9wdGlvbnM6IC0+XG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyJykucmVtb3ZlKClcblxuXHRjbGFzcyBQbGF5ZXIuT3B0aW9uc0l0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6ICcjdHJhY2stb3B0aW9ucy1saXN0LWl0ZW0tdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3RyYWNrLW9wdGlvbnMtbGlzdC1pdGVtJ1xuXHRcdHRhZ05hbWU6ICdsaSdcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEAkZWwuYWRkQ2xhc3MgQG1vZGVsLmdldCAnY2xhc3MnXG5cblx0Y2xhc3MgUGxheWVyLk9wdGlvbnNMYXlvdXQgZXh0ZW5kcyBNYXJpb25ldHRlLkNvbXBvc2l0ZVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNvcHRpb25zLWxpc3QtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ29wdGlvbnMtd3JhcHBlcidcblx0XHRjaGlsZFZpZXc6IFBsYXllci5PcHRpb25zSXRlbVxuXHRcdGNoaWxkVmlld0NvbnRhaW5lcjogJy5vcHRpb25zLWxpc3QnXG5cdFx0b25TaG93OiAtPlxuXHRcdFx0QCQoJy50ZXh0YXJlYScpLnNlbGVjdCgpXG5cblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRAJCgnLnRleHRhcmVhJykuc2VsZWN0KClcblxuXHRcdFx0J2NsaWNrIC5kZWxldGUnOiAoZSktPlxuXHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5yZW1vdmVUcmFja0Zyb21GYXZvcml0ZSBAbW9kZWwuZ2V0ICdpZCdcblx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIGZhbHNlLCB7c2lsZW50OiB0cnVlfVxuXHRcdFx0XHQkKGUudGFyZ2V0KS5jbG9zZXN0KCcub3B0aW9ucycpLmZpbmQoJy50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdkZWxldGUnKS5hZGRDbGFzcygnYWRkJylcblx0XHRcdFx0QCRlbC5maW5kKCcuZGVsZXRlJykucmVtb3ZlQ2xhc3MoJ2RlbGV0ZScpLmFkZENsYXNzKCdhZGQnKS50ZXh0KCfQlNC+0LHQsNCy0LjRgtGMINCyINC80LXQtNC40LDRgtC10LrRgycpXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdyZW1vdmU6ZnJvbTpmYXZvcml0ZSdcblx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRcdCdjbGljayAuYWRkJzogLT5cblx0XHRcdFx0UGxheWVyLkNvbnRyb2xsZXIuYWRkVHJhY2tUb0Zhdm9yaXRlIEBtb2RlbFxuXHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZSwge3NpbGVudDogdHJ1ZX1cblx0XHRcdFx0QCRlbC5jbG9zZXN0KCcub3B0aW9ucycpLmZpbmQoJy50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdhZGQnKS5hZGRDbGFzcygnZGVsZXRlJylcblx0XHRcdFx0QCRlbC5maW5kKCcuYWRkJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKS50ZXh0KCfQo9C00LDQu9C40YLRjCDQuNC3INC80LXQtNC40LDRgtC10LrQuCcpXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdhZGQ6dG86ZmF2b3JpdGUnXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnbW91c2VsZWF2ZSc6ICdjbG9zZU9wdGlvbnMnXG5cdFx0XHQnY2xpY2sgLm9wdGlvbnMtb3ZlcmxheSc6ICdjbG9zZU9wdGlvbnMnXG5cblx0XHRjbG9zZU9wdGlvbnM6IChlKS0+XG5cdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpXG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyJykucmVtb3ZlKClcblxuXHRjbGFzcyBQbGF5ZXIuUGxheWxpc3RJdGVtIGV4dGVuZHMgTWFyaW9uZXR0ZS5JdGVtVmlld1xuXHRcdHRlbXBsYXRlOiAnI29wdGlvbnMtcGxheWxpc3QtaXRlbS10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnb3B0aW9ucy1wbGF5bGlzdC1pdGVtJ1xuXHRcdHRhZ25hbWU6ICdsaSdcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEAkZWwuYXR0ciAnZGF0YS1pZCcsIEBtb2RlbC5nZXQgJ2lkJ1xuXG5cdGNsYXNzIFBsYXllci5QbGF5bGlzdEl0ZW1zIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db21wb3NpdGVWaWV3XG5cdFx0dGVtcGxhdGU6ICcjb3B0aW9ucy1wbGF5bGlzdC13aW5kb3ctdHBsJ1xuXHRcdGNoaWxkVmlldzogUGxheWVyLlBsYXlsaXN0SXRlbVxuXHRcdGNoaWxkVmlld0NvbnRhaW5lcjogJy5jb250YWluZXInXG5cdFx0Y2xhc3NOYW1lOiAnb3B0aW9ucy13cmFwcGVyJ1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAubmV3LXBsYXlsaXN0IGlucHV0JzogLT5cblx0XHRcdFx0JCgnLm5ldy1wbGF5bGlzdCBpbnB1dCcpLnNlbGVjdCgpXG5cdFx0XHQnYmx1ciAubmV3LXBsYXlsaXN0IGlucHV0JzogLT5cblx0XHRcdFx0JCgnLm5ldy1wbGF5bGlzdCBpbnB1dCcpLnZhbCAnKyDQndC+0LLRi9C5INC/0LvQtdC50LvQuNGB0YInXG5cblx0XHRvblNob3c6IC0+XG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyIC5vcHRpb25zLXBsYXlsaXN0LXNjcm9sbC13cmFwcCcpLmN1c3RvbVNjcm9sbGJhcigpO1xuXG5cdHJldHVybiBQbGF5ZXJcblxuXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9wYXJ0cy9wbGF5ZXIvdmlldy5jb2ZmZWVcbiAqKi8iLCJ2YXIgamFkZSA9IHJlcXVpcmUoXCIvVXNlcnMvZW1pbC93b3JrX3Byb2plY3Qvbm9kZS5tdXNpYy9ub2RlX21vZHVsZXMvamFkZS9saWIvcnVudGltZS5qc1wiKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0ZW1wbGF0ZShsb2NhbHMpIHtcbnZhciBidWYgPSBbXTtcbnZhciBqYWRlX21peGlucyA9IHt9O1xudmFyIGphZGVfaW50ZXJwO1xuO3ZhciBsb2NhbHNfZm9yX3dpdGggPSAobG9jYWxzIHx8IHt9KTsoZnVuY3Rpb24gKGFydGlzdCwgZHVyYXRpb24sIGltZywgaW5GYXZvcml0ZSwgdGl0bGUpIHtcbmJ1Zi5wdXNoKFwiPGRpdiBjbGFzcz1cXFwicGxheWVyLXdyYXBwXFxcIj48ZGl2IGNsYXNzPVxcXCJjb250cm9sbC1iYXJcXFwiPjxkaXYgY2xhc3M9XFxcImJhY2stYXJyb3dcXFwiPjwvZGl2PjxkaXYgY2xhc3M9XFxcInBsYXkgcGxheWVyLWNvbnRyb2xsLWVsZW1lbnRcXFwiPjwvZGl2PjxkaXYgY2xhc3M9XFxcImZ3ZC1hcnJvd1xcXCI+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1cXFwibWFpbi1wbGF5ZXJcXFwiPjxkaXYgY2xhc3M9XFxcInBsYXllci12aWV3XFxcIj48ZGl2IGNsYXNzPVxcXCJhcnRpc3QtbG9nb1xcXCI+PGltZ1wiICsgKGphZGUuYXR0cihcInNyY1wiLCBcIlwiICsgKGltZykgKyBcIlwiLCB0cnVlLCB0cnVlKSkgKyBcIj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJ0cmFjay1pbmZvXFxcIj48ZGl2IGNsYXNzPVxcXCJ0cmFjay1uYW1lXFxcIj48c3BhbiBjbGFzcz1cXFwiYXJ0aXN0TmFtZVxcXCI+XCIgKyAoamFkZS5lc2NhcGUoKGphZGVfaW50ZXJwID0gYXJ0aXN0KSA9PSBudWxsID8gJycgOiBqYWRlX2ludGVycCkpICsgXCI8L3NwYW4+4oCUPHNwYW4gY2xhc3M9XFxcInNvdW5kTmFtZVxcXCI+XCIgKyAoamFkZS5lc2NhcGUoKGphZGVfaW50ZXJwID0gdGl0bGUpID09IG51bGwgPyAnJyA6IGphZGVfaW50ZXJwKSkgKyBcIjwvc3Bhbj48ZGl2IGNsYXNzPVxcXCJwbGF5ZXItdGltZXItZGlzcGxheVxcXCI+PHNwYW4gY2xhc3M9XFxcInRpbWUtZnJvbnRcXFwiPjAwOjAwPC9zcGFuPjxzcGFuIGNsYXNzPVxcXCJ0aW1lLWVuZCBoaWRkZW5cXFwiPlwiICsgKGphZGUuZXNjYXBlKChqYWRlX2ludGVycCA9IGR1cmF0aW9uKSA9PSBudWxsID8gJycgOiBqYWRlX2ludGVycCkpICsgXCI8L3NwYW4+PC9kaXY+PC9kaXY+PGRpdiBjbGFzcz1cXFwidGltZS1saW5lXFxcIj48ZGl2IGNsYXNzPVxcXCJhY3RpdmUtem9uZVxcXCI+PC9kaXY+PGRpdiBjbGFzcz1cXFwiYWN0aXZlLWxpbmUgZHJhZy1hY3RpdmVcXFwiPjxkaXYgY2xhc3M9XFxcImRyYWctYmFsbCBkcmFnXFxcIj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJvdGhlci1idXR0b25zXFxcIj48ZGl2IGNsYXNzPVxcXCJvcHRpb25zXFxcIj48ZGl2IGNsYXNzPVxcXCJvcHRpb25zLXJlZ2lvblxcXCI+PC9kaXY+XCIpO1xuaWYgKCBpbkZhdm9yaXRlKVxue1xuYnVmLnB1c2goXCI8ZGl2IGNsYXNzPVxcXCJ0cmFjay1jb250cm9sbCBkZWxldGVcXFwiPjwvZGl2PlwiKTtcbn1cbmVsc2Vcbntcbn1cbmJ1Zi5wdXNoKFwiPGRpdiBjbGFzcz1cXFwidHJhY2stY29udHJvbGwgYWRkXFxcIj48L2Rpdj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJ2b2x1bWVcXFwiPjxzcGFuPjxkaXYgY2xhc3M9XFxcImxpbmVcXFwiPjxkaXYgY2xhc3M9XFxcImFjdGl2ZVxcXCI+PC9kaXY+PGRpdiBjbGFzcz1cXFwidmFsdWVcXFwiPjwvZGl2PjwvZGl2Pjwvc3Bhbj48ZGl2IGNsYXNzPVxcXCJzaHVmZmxlXFxcIj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJsb29wXFxcIj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj5cIik7fS5jYWxsKHRoaXMsXCJhcnRpc3RcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLmFydGlzdDp0eXBlb2YgYXJ0aXN0IT09XCJ1bmRlZmluZWRcIj9hcnRpc3Q6dW5kZWZpbmVkLFwiZHVyYXRpb25cIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLmR1cmF0aW9uOnR5cGVvZiBkdXJhdGlvbiE9PVwidW5kZWZpbmVkXCI/ZHVyYXRpb246dW5kZWZpbmVkLFwiaW1nXCIgaW4gbG9jYWxzX2Zvcl93aXRoP2xvY2Fsc19mb3Jfd2l0aC5pbWc6dHlwZW9mIGltZyE9PVwidW5kZWZpbmVkXCI/aW1nOnVuZGVmaW5lZCxcImluRmF2b3JpdGVcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLmluRmF2b3JpdGU6dHlwZW9mIGluRmF2b3JpdGUhPT1cInVuZGVmaW5lZFwiP2luRmF2b3JpdGU6dW5kZWZpbmVkLFwidGl0bGVcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLnRpdGxlOnR5cGVvZiB0aXRsZSE9PVwidW5kZWZpbmVkXCI/dGl0bGU6dW5kZWZpbmVkKSk7O3JldHVybiBidWYuam9pbihcIlwiKTtcbn1cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL3BsYXllci90ZW1wbGF0ZXMvbWFpbi10cGwuamFkZVxuICoqIG1vZHVsZSBpZCA9IDNcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIid1c2Ugc3RyaWN0JztcblxuLyoqXG4gKiBNZXJnZSB0d28gYXR0cmlidXRlIG9iamVjdHMgZ2l2aW5nIHByZWNlZGVuY2VcbiAqIHRvIHZhbHVlcyBpbiBvYmplY3QgYGJgLiBDbGFzc2VzIGFyZSBzcGVjaWFsLWNhc2VkXG4gKiBhbGxvd2luZyBmb3IgYXJyYXlzIGFuZCBtZXJnaW5nL2pvaW5pbmcgYXBwcm9wcmlhdGVseVxuICogcmVzdWx0aW5nIGluIGEgc3RyaW5nLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBhXG4gKiBAcGFyYW0ge09iamVjdH0gYlxuICogQHJldHVybiB7T2JqZWN0fSBhXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG5leHBvcnRzLm1lcmdlID0gZnVuY3Rpb24gbWVyZ2UoYSwgYikge1xuICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMSkge1xuICAgIHZhciBhdHRycyA9IGFbMF07XG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhLmxlbmd0aDsgaSsrKSB7XG4gICAgICBhdHRycyA9IG1lcmdlKGF0dHJzLCBhW2ldKTtcbiAgICB9XG4gICAgcmV0dXJuIGF0dHJzO1xuICB9XG4gIHZhciBhYyA9IGFbJ2NsYXNzJ107XG4gIHZhciBiYyA9IGJbJ2NsYXNzJ107XG5cbiAgaWYgKGFjIHx8IGJjKSB7XG4gICAgYWMgPSBhYyB8fCBbXTtcbiAgICBiYyA9IGJjIHx8IFtdO1xuICAgIGlmICghQXJyYXkuaXNBcnJheShhYykpIGFjID0gW2FjXTtcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkoYmMpKSBiYyA9IFtiY107XG4gICAgYVsnY2xhc3MnXSA9IGFjLmNvbmNhdChiYykuZmlsdGVyKG51bGxzKTtcbiAgfVxuXG4gIGZvciAodmFyIGtleSBpbiBiKSB7XG4gICAgaWYgKGtleSAhPSAnY2xhc3MnKSB7XG4gICAgICBhW2tleV0gPSBiW2tleV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGE7XG59O1xuXG4vKipcbiAqIEZpbHRlciBudWxsIGB2YWxgcy5cbiAqXG4gKiBAcGFyYW0geyp9IHZhbFxuICogQHJldHVybiB7Qm9vbGVhbn1cbiAqIEBhcGkgcHJpdmF0ZVxuICovXG5cbmZ1bmN0aW9uIG51bGxzKHZhbCkge1xuICByZXR1cm4gdmFsICE9IG51bGwgJiYgdmFsICE9PSAnJztcbn1cblxuLyoqXG4gKiBqb2luIGFycmF5IGFzIGNsYXNzZXMuXG4gKlxuICogQHBhcmFtIHsqfSB2YWxcbiAqIEByZXR1cm4ge1N0cmluZ31cbiAqL1xuZXhwb3J0cy5qb2luQ2xhc3NlcyA9IGpvaW5DbGFzc2VzO1xuZnVuY3Rpb24gam9pbkNsYXNzZXModmFsKSB7XG4gIHJldHVybiAoQXJyYXkuaXNBcnJheSh2YWwpID8gdmFsLm1hcChqb2luQ2xhc3NlcykgOlxuICAgICh2YWwgJiYgdHlwZW9mIHZhbCA9PT0gJ29iamVjdCcpID8gT2JqZWN0LmtleXModmFsKS5maWx0ZXIoZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gdmFsW2tleV07IH0pIDpcbiAgICBbdmFsXSkuZmlsdGVyKG51bGxzKS5qb2luKCcgJyk7XG59XG5cbi8qKlxuICogUmVuZGVyIHRoZSBnaXZlbiBjbGFzc2VzLlxuICpcbiAqIEBwYXJhbSB7QXJyYXl9IGNsYXNzZXNcbiAqIEBwYXJhbSB7QXJyYXkuPEJvb2xlYW4+fSBlc2NhcGVkXG4gKiBAcmV0dXJuIHtTdHJpbmd9XG4gKi9cbmV4cG9ydHMuY2xzID0gZnVuY3Rpb24gY2xzKGNsYXNzZXMsIGVzY2FwZWQpIHtcbiAgdmFyIGJ1ZiA9IFtdO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IGNsYXNzZXMubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoZXNjYXBlZCAmJiBlc2NhcGVkW2ldKSB7XG4gICAgICBidWYucHVzaChleHBvcnRzLmVzY2FwZShqb2luQ2xhc3NlcyhbY2xhc3Nlc1tpXV0pKSk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGJ1Zi5wdXNoKGpvaW5DbGFzc2VzKGNsYXNzZXNbaV0pKTtcbiAgICB9XG4gIH1cbiAgdmFyIHRleHQgPSBqb2luQ2xhc3NlcyhidWYpO1xuICBpZiAodGV4dC5sZW5ndGgpIHtcbiAgICByZXR1cm4gJyBjbGFzcz1cIicgKyB0ZXh0ICsgJ1wiJztcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gJyc7XG4gIH1cbn07XG5cblxuZXhwb3J0cy5zdHlsZSA9IGZ1bmN0aW9uICh2YWwpIHtcbiAgaWYgKHZhbCAmJiB0eXBlb2YgdmFsID09PSAnb2JqZWN0Jykge1xuICAgIHJldHVybiBPYmplY3Qua2V5cyh2YWwpLm1hcChmdW5jdGlvbiAoc3R5bGUpIHtcbiAgICAgIHJldHVybiBzdHlsZSArICc6JyArIHZhbFtzdHlsZV07XG4gICAgfSkuam9pbignOycpO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiB2YWw7XG4gIH1cbn07XG4vKipcbiAqIFJlbmRlciB0aGUgZ2l2ZW4gYXR0cmlidXRlLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBrZXlcbiAqIEBwYXJhbSB7U3RyaW5nfSB2YWxcbiAqIEBwYXJhbSB7Qm9vbGVhbn0gZXNjYXBlZFxuICogQHBhcmFtIHtCb29sZWFufSB0ZXJzZVxuICogQHJldHVybiB7U3RyaW5nfVxuICovXG5leHBvcnRzLmF0dHIgPSBmdW5jdGlvbiBhdHRyKGtleSwgdmFsLCBlc2NhcGVkLCB0ZXJzZSkge1xuICBpZiAoa2V5ID09PSAnc3R5bGUnKSB7XG4gICAgdmFsID0gZXhwb3J0cy5zdHlsZSh2YWwpO1xuICB9XG4gIGlmICgnYm9vbGVhbicgPT0gdHlwZW9mIHZhbCB8fCBudWxsID09IHZhbCkge1xuICAgIGlmICh2YWwpIHtcbiAgICAgIHJldHVybiAnICcgKyAodGVyc2UgPyBrZXkgOiBrZXkgKyAnPVwiJyArIGtleSArICdcIicpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJyc7XG4gICAgfVxuICB9IGVsc2UgaWYgKDAgPT0ga2V5LmluZGV4T2YoJ2RhdGEnKSAmJiAnc3RyaW5nJyAhPSB0eXBlb2YgdmFsKSB7XG4gICAgaWYgKEpTT04uc3RyaW5naWZ5KHZhbCkuaW5kZXhPZignJicpICE9PSAtMSkge1xuICAgICAgY29uc29sZS53YXJuKCdTaW5jZSBKYWRlIDIuMC4wLCBhbXBlcnNhbmRzIChgJmApIGluIGRhdGEgYXR0cmlidXRlcyAnICtcbiAgICAgICAgICAgICAgICAgICAnd2lsbCBiZSBlc2NhcGVkIHRvIGAmYW1wO2AnKTtcbiAgICB9O1xuICAgIGlmICh2YWwgJiYgdHlwZW9mIHZhbC50b0lTT1N0cmluZyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgY29uc29sZS53YXJuKCdKYWRlIHdpbGwgZWxpbWluYXRlIHRoZSBkb3VibGUgcXVvdGVzIGFyb3VuZCBkYXRlcyBpbiAnICtcbiAgICAgICAgICAgICAgICAgICAnSVNPIGZvcm0gYWZ0ZXIgMi4wLjAnKTtcbiAgICB9XG4gICAgcmV0dXJuICcgJyArIGtleSArIFwiPSdcIiArIEpTT04uc3RyaW5naWZ5KHZhbCkucmVwbGFjZSgvJy9nLCAnJmFwb3M7JykgKyBcIidcIjtcbiAgfSBlbHNlIGlmIChlc2NhcGVkKSB7XG4gICAgaWYgKHZhbCAmJiB0eXBlb2YgdmFsLnRvSVNPU3RyaW5nID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0phZGUgd2lsbCBzdHJpbmdpZnkgZGF0ZXMgaW4gSVNPIGZvcm0gYWZ0ZXIgMi4wLjAnKTtcbiAgICB9XG4gICAgcmV0dXJuICcgJyArIGtleSArICc9XCInICsgZXhwb3J0cy5lc2NhcGUodmFsKSArICdcIic7XG4gIH0gZWxzZSB7XG4gICAgaWYgKHZhbCAmJiB0eXBlb2YgdmFsLnRvSVNPU3RyaW5nID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBjb25zb2xlLndhcm4oJ0phZGUgd2lsbCBzdHJpbmdpZnkgZGF0ZXMgaW4gSVNPIGZvcm0gYWZ0ZXIgMi4wLjAnKTtcbiAgICB9XG4gICAgcmV0dXJuICcgJyArIGtleSArICc9XCInICsgdmFsICsgJ1wiJztcbiAgfVxufTtcblxuLyoqXG4gKiBSZW5kZXIgdGhlIGdpdmVuIGF0dHJpYnV0ZXMgb2JqZWN0LlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmpcbiAqIEBwYXJhbSB7T2JqZWN0fSBlc2NhcGVkXG4gKiBAcmV0dXJuIHtTdHJpbmd9XG4gKi9cbmV4cG9ydHMuYXR0cnMgPSBmdW5jdGlvbiBhdHRycyhvYmosIHRlcnNlKXtcbiAgdmFyIGJ1ZiA9IFtdO1xuXG4gIHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqKTtcblxuICBpZiAoa2V5cy5sZW5ndGgpIHtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGtleXMubGVuZ3RoOyArK2kpIHtcbiAgICAgIHZhciBrZXkgPSBrZXlzW2ldXG4gICAgICAgICwgdmFsID0gb2JqW2tleV07XG5cbiAgICAgIGlmICgnY2xhc3MnID09IGtleSkge1xuICAgICAgICBpZiAodmFsID0gam9pbkNsYXNzZXModmFsKSkge1xuICAgICAgICAgIGJ1Zi5wdXNoKCcgJyArIGtleSArICc9XCInICsgdmFsICsgJ1wiJyk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJ1Zi5wdXNoKGV4cG9ydHMuYXR0cihrZXksIHZhbCwgZmFsc2UsIHRlcnNlKSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGJ1Zi5qb2luKCcnKTtcbn07XG5cbi8qKlxuICogRXNjYXBlIHRoZSBnaXZlbiBzdHJpbmcgb2YgYGh0bWxgLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBodG1sXG4gKiBAcmV0dXJuIHtTdHJpbmd9XG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuXG52YXIgamFkZV9lbmNvZGVfaHRtbF9ydWxlcyA9IHtcbiAgJyYnOiAnJmFtcDsnLFxuICAnPCc6ICcmbHQ7JyxcbiAgJz4nOiAnJmd0OycsXG4gICdcIic6ICcmcXVvdDsnXG59O1xudmFyIGphZGVfbWF0Y2hfaHRtbCA9IC9bJjw+XCJdL2c7XG5cbmZ1bmN0aW9uIGphZGVfZW5jb2RlX2NoYXIoYykge1xuICByZXR1cm4gamFkZV9lbmNvZGVfaHRtbF9ydWxlc1tjXSB8fCBjO1xufVxuXG5leHBvcnRzLmVzY2FwZSA9IGphZGVfZXNjYXBlO1xuZnVuY3Rpb24gamFkZV9lc2NhcGUoaHRtbCl7XG4gIHZhciByZXN1bHQgPSBTdHJpbmcoaHRtbCkucmVwbGFjZShqYWRlX21hdGNoX2h0bWwsIGphZGVfZW5jb2RlX2NoYXIpO1xuICBpZiAocmVzdWx0ID09PSAnJyArIGh0bWwpIHJldHVybiBodG1sO1xuICBlbHNlIHJldHVybiByZXN1bHQ7XG59O1xuXG4vKipcbiAqIFJlLXRocm93IHRoZSBnaXZlbiBgZXJyYCBpbiBjb250ZXh0IHRvIHRoZVxuICogdGhlIGphZGUgaW4gYGZpbGVuYW1lYCBhdCB0aGUgZ2l2ZW4gYGxpbmVub2AuXG4gKlxuICogQHBhcmFtIHtFcnJvcn0gZXJyXG4gKiBAcGFyYW0ge1N0cmluZ30gZmlsZW5hbWVcbiAqIEBwYXJhbSB7U3RyaW5nfSBsaW5lbm9cbiAqIEBhcGkgcHJpdmF0ZVxuICovXG5cbmV4cG9ydHMucmV0aHJvdyA9IGZ1bmN0aW9uIHJldGhyb3coZXJyLCBmaWxlbmFtZSwgbGluZW5vLCBzdHIpe1xuICBpZiAoIShlcnIgaW5zdGFuY2VvZiBFcnJvcikpIHRocm93IGVycjtcbiAgaWYgKCh0eXBlb2Ygd2luZG93ICE9ICd1bmRlZmluZWQnIHx8ICFmaWxlbmFtZSkgJiYgIXN0cikge1xuICAgIGVyci5tZXNzYWdlICs9ICcgb24gbGluZSAnICsgbGluZW5vO1xuICAgIHRocm93IGVycjtcbiAgfVxuICB0cnkge1xuICAgIHN0ciA9IHN0ciB8fCByZXF1aXJlKCdmcycpLnJlYWRGaWxlU3luYyhmaWxlbmFtZSwgJ3V0ZjgnKVxuICB9IGNhdGNoIChleCkge1xuICAgIHJldGhyb3coZXJyLCBudWxsLCBsaW5lbm8pXG4gIH1cbiAgdmFyIGNvbnRleHQgPSAzXG4gICAgLCBsaW5lcyA9IHN0ci5zcGxpdCgnXFxuJylcbiAgICAsIHN0YXJ0ID0gTWF0aC5tYXgobGluZW5vIC0gY29udGV4dCwgMClcbiAgICAsIGVuZCA9IE1hdGgubWluKGxpbmVzLmxlbmd0aCwgbGluZW5vICsgY29udGV4dCk7XG5cbiAgLy8gRXJyb3IgY29udGV4dFxuICB2YXIgY29udGV4dCA9IGxpbmVzLnNsaWNlKHN0YXJ0LCBlbmQpLm1hcChmdW5jdGlvbihsaW5lLCBpKXtcbiAgICB2YXIgY3VyciA9IGkgKyBzdGFydCArIDE7XG4gICAgcmV0dXJuIChjdXJyID09IGxpbmVubyA/ICcgID4gJyA6ICcgICAgJylcbiAgICAgICsgY3VyclxuICAgICAgKyAnfCAnXG4gICAgICArIGxpbmU7XG4gIH0pLmpvaW4oJ1xcbicpO1xuXG4gIC8vIEFsdGVyIGV4Y2VwdGlvbiBtZXNzYWdlXG4gIGVyci5wYXRoID0gZmlsZW5hbWU7XG4gIGVyci5tZXNzYWdlID0gKGZpbGVuYW1lIHx8ICdKYWRlJykgKyAnOicgKyBsaW5lbm9cbiAgICArICdcXG4nICsgY29udGV4dCArICdcXG5cXG4nICsgZXJyLm1lc3NhZ2U7XG4gIHRocm93IGVycjtcbn07XG5cbmV4cG9ydHMuRGVidWdJdGVtID0gZnVuY3Rpb24gRGVidWdJdGVtKGxpbmVubywgZmlsZW5hbWUpIHtcbiAgdGhpcy5saW5lbm8gPSBsaW5lbm87XG4gIHRoaXMuZmlsZW5hbWUgPSBmaWxlbmFtZTtcbn1cblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9+L2phZGUvbGliL3J1bnRpbWUuanNcbiAqKiBtb2R1bGUgaWQgPSA0XG4gKiogbW9kdWxlIGNodW5rcyA9IDAgMTRcbiAqKi8iLCIvKiAoaWdub3JlZCkgKi9cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIGZzIChpZ25vcmVkKVxuICoqIG1vZHVsZSBpZCA9IDVcbiAqKiBtb2R1bGUgY2h1bmtzID0gMCAxNFxuICoqLyIsImRlZmluZSBbXSwgLT5cbiAgICByZXF1aXJlKCcuLi8uLi9lbnRpdGllcy9sb2cuY29mZmVlJylcbiAgICBcbiAgICBjbGFzcyBjb250cm9sbGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db250cm9sbGVyXG4gICAgICAgIGluaXRpYWxpemU6IC0+XG4gICAgICAgICAgICBAZXJyQ291bnQgPSAwXG5cbiAgICAgICAgc2VuZE5ld0xvZzogLT5cbiAgICAgICAgICAgIGNvbnNvbGUuZGVidWcgJy4uLi4uLi4uLi4uLi4uLi4uLi4uLidcbiAgICAgICAgICAgIGNvbnNvbGUuZGVidWcgJ1N0YXJ0IG5ldyBsb2cgcmVxdWVzdCdcbiAgICAgICAgICAgIHNlbGYgPSBAXG4gICAgICAgICAgICBsb2dzID0gYXBwLnJlcXVlc3QoJ2dldDpsb2dzJylcblxuICAgICAgICAgICAgaWYgbG9ncy5sZW5ndGhcbiAgICAgICAgICAgICAgICBsb2cgPSBsb2dzLmF0KDApXG4gICAgICAgICAgICAgICAgb2Zmc2V0ID0gKG5ldyBEYXRlKCkuZ2V0VGltZSgpIC0gbG9nLmdldCgnY3JlYXRlJykpLzEwMDBcbiAgICAgICAgICAgICAgICBpZiBvZmZzZXQgPiAxMDgwMFxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmRlYnVnICdEZXN0cm95IG9sZCBsb2cnLCBsb2cudG9KU09OKClcbiAgICAgICAgICAgICAgICAgICAgbG9nLmRlc3Ryb3koKVxuICAgICAgICAgICAgICAgICAgICBkbyBzZWxmLnNlbmROZXdMb2dcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuXG5cbiAgICAgICAgICAgICAgICByZXEgPSAkLmdldCBsb2cuZ2V0KCd1cmwnKSwge3RpbWVPZmZzZXQ6IG9mZnNldH1cbiAgICAgICAgICAgICAgICBjb25zb2xlLmRlYnVnICdMb2cgcmVxdWVzdCBzZW5kISBVcmw6ICcrbG9nLmdldCgndXJsJylcbiAgICAgICAgICAgICAgICByZXEuZG9uZSAocmVzKS0+XG4gICAgICAgICAgICAgICAgICAgIGlmIHJlcy5jb2RlIGlzIDBcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZGVidWcgJ0xvZyByZXF1ZXN0IHNlbmQgc3VjY2VzcydcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuZXJyQ291bnQgPSAwXG4gICAgICAgICAgICAgICAgICAgICAgICBsb2cuZGVzdHJveSgpXG4gICAgICAgICAgICAgICAgICAgICAgICBkbyBzZWxmLnNlbmROZXdMb2dcbiAgICAgICAgICAgICAgICAgICAgZWxzZVxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5kZWJ1ZyAnTG9nIHJlcXVlc3Qgc2VuZCBlcnJvcicsIHJlc1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5lcnJDb3VudCsrXG4gICAgICAgICAgICAgICAgICAgICAgICBkbyBzZWxmLnNlbmRFcnJcblxuICAgICAgICAgICAgICAgIHJlcS5mYWlsIChlcnIpLT5cbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5kZWJ1ZyAnTG9nIHJlcXVlc3Qgc2VuZCBlcnJvcicsZXJyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuZXJyQ291bnQrK1xuICAgICAgICAgICAgICAgICAgICBkbyBzZWxmLnNlbmRFcnJcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBjb25zb2xlLmRlYnVnICdObyBsb2cgdG8gc2VuZCEgV2FpdCBmb3IgbmV3IGxvZ3MnXG5cbiAgICAgICAgICAgICAgICBAbGlzdGVuVG8gbG9ncywgJ2FkZCcsIC0+XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZGVidWcgJ05ldyBsb2cgY3JlYXRlISBTdG9wIGxpc3RlbmluZyBsb2cgc3RvcmFnZSdcbiAgICAgICAgICAgICAgICAgICAgQHN0b3BMaXN0ZW5pbmcoKVxuICAgICAgICAgICAgICAgICAgICBkbyBzZWxmLnNlbmROZXdMb2dcblxuICAgICAgICBzZW5kRXJyOiAtPlxuICAgICAgICAgICAgY29uc29sZS5kZWJ1ZyAnTG9nIHJlcXVlc3QgZXJyb3IhIENvdW50OiAnK0BlcnJDb3VudFxuICAgICAgICAgICAgc2VsZiA9IEBcbiAgICAgICAgICAgIGlmIEBlcnJDb3VudCBpcyAxXG4gICAgICAgICAgICAgICAgZGVsYXkgPSAxXG4gICAgICAgICAgICBlbHNlIGlmIDEwID4gQGVyckNvdW50ID4gMVxuICAgICAgICAgICAgICAgIGRlbGF5ID0gMTAwMDBcbiAgICAgICAgICAgIGVsc2UgaWYgQGVyckNvdW50ID4gMTBcbiAgICAgICAgICAgICAgICBkZWxheSA9IDMwMDAwMFxuXG4gICAgICAgICAgICBzZXRUaW1lb3V0IC0+XG4gICAgICAgICAgICAgICAgZG8gc2VsZi5zZW5kTmV3TG9nXG4gICAgICAgICAgICAsZGVsYXlcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgIFxuXG4gICAgICAgIGNyZWF0ZU5ld0xvZzogKHVybCktPlxuICAgICAgICAgICAgbG9ncyA9IGFwcC5yZXF1ZXN0KCdnZXQ6bG9ncycpO1xuICAgICAgICBcbiAgICAgICAgICAgIG5ld0xvZyA9IGxvZ3MuY3JlYXRlXG4gICAgICAgICAgICAgICAgdXJsOiB1cmxcbiAgICAgICAgICAgICAgICBjcmVhdGU6IG5ldyBEYXRlKCkuZ2V0VGltZSgpXG4gICAgICAgICAgICAgICAgdW5pcTogYXBwLlJhbmRJbnQgMTAwLDIwMDAwMDAwMFxuXG4gICAgICAgICAgICBpZiBuZXdMb2cudmFsaWRhdGlvbkVycm9yXG4gICAgICAgICAgICAgICAgY29uc29sZS5kZWJ1ZyBuZXdMb2cudmFsaWRhdGlvbkVycm9yXG4gICAgICAgICAgICAgICAgbG9ncy5yZW1vdmUgbmV3TG9nXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgY29uc29sZS5kZWJ1Zy5hcHBseSBjb25zb2xlLCBbJ0xvZyBjcmVhdGUnLCBuZXdMb2cuZ2V0KCdpZCcpXVxuXG4gICAgcmV0dXJuIG5ldyBjb250cm9sbGVyXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2xvZy9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgLT5cbiAgICBMb2cgPSB7fVxuICAgIGNsYXNzIExvZy5Nb2RlbCBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cbiAgICAgICAgdmFsaWRhdGU6IChhdHRycywgb3B0aW9ucyktPlxuICAgICAgICAgICAgaWYgIWF0dHJzLnVybD9cbiAgICAgICAgICAgICAgcmV0dXJuIFwiQSAndXBkYXRlVXJsJyBwcm9wZXJ0eSBvciBmdW5jdGlvbiBtdXN0IGJlIHNwZWNpZmllZFwiXG5cbiAgICAgICAgdXJsOiAtPlxuICAgICAgICAgICAgYmFzZSA9XG4gICAgICAgICAgICAgICAgXy5yZXN1bHQodGhpcywgJ3VybFJvb3QnKSBvclxuICAgICAgICAgICAgICAgIF8ucmVzdWx0KHRoaXMuY29sbGVjdGlvbiwgJ3VybCcpIG9yXG4gICAgICAgICAgICAgICAgdXJsRXJyb3IoKTtcblxuICAgICAgICAgICAgaWYgdGhpcy5pc05ldygpXG4gICAgICAgICAgICAgICAgcmV0dXJuIGJhc2U7XG4gICAgICAgICAgICBpZCA9IHRoaXMuZ2V0KHRoaXMuaWRBdHRyaWJ1dGUpO1xuICAgICAgICAgICAgcmV0dXJuIGJhc2UucmVwbGFjZSgvW15cXC9dJC8sICckJi8nKSArIGVuY29kZVVSSUNvbXBvbmVudChpZCk7XG5cbiAgICAgICAgc3luYzogKG1ldGhvZCxtb2RlbCxvcHRpb25zKS0+XG4gICAgICAgICAgQmFja2JvbmUuc3luYy5hcHBseSB0aGlzLFttZXRob2QsbW9kZWwsb3B0aW9uc11cblxuICAgIGNsYXNzIExvZy5Db2xsZWN0aW9uIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuICAgICAgICBtb2RlbDogTG9nLk1vZGVsXG4gICAgICAgIGxvY2FsU3RvcmFnZTogbmV3IEJhY2tib25lLkxvY2FsU3RvcmFnZSBcIkxvZ3NcIlxuICAgICAgICBzYXZlOiAtPlxuICAgICAgICAgICAgQGVhY2ggKG1vZGVsKS0+XG4gICAgICAgICAgICAgICAgQmFja2JvbmUubG9jYWxTeW5jIFwidXBkYXRlXCIsIG1vZGVsXG5cbiAgICBcbiAgICBpbml0aWFsaXplTG9nID0gLT5cbiAgICAgICAgTG9nLkxpc3QgPSBuZXcgTG9nLkNvbGxlY3Rpb25cblxuICAgICAgICBkbyBMb2cuTGlzdC5mZXRjaFxuXG5cbiAgICBBUEkgPVxuICAgICAgICBnZXRMb2dzOiAtPlxuICAgICAgICAgICAgaWYgTG9nLkxpc3QgaXMgdW5kZWZpbmVkXG4gICAgICAgICAgICAgICAgaW5pdGlhbGl6ZUxvZygpXG5cbiAgICAgICAgICAgIHJldHVybiBMb2cuTGlzdFxuXG5cbiAgICBhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpsb2dzJywgLT5cbiAgICAgICAgcmV0dXJuIEFQSS5nZXRMb2dzKClcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2xvZy5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuL3ZpZXcuY29mZmVlJ10sIChOb3RpZmljYXRpb24pLT5cblx0cmVxdWlyZSgnLi9zdHlsZS5zdHlsJyk7XG5cblx0Y2xhc3MgY29udHJvbGxlciBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXHRcdHNob3c6IChvcHRzKS0+XG5cdFx0XHQjIyNcblxuICBcdFx0XHRtZXNzYWdlOiDRgdC+0L7QsdGJ0LXQvdC40LUg0LrQvtGC0L7RgNC+0LUg0YXQvtGC0LjQvCDQvdCw0L/QuNGB0LDRgtGMXG4gIFx0XHRcdHR5cGU6INGC0LjQvyDQvtGI0LjQsdC60Lgge2Vycm9yOiDQutGA0LDRgdC90L7QtSDQv9C+0LvQtSwgc3VjY2Vzczog0LfQtdC70LXQvdC+0LUg0L/QvtC70LV9INC/0L4g0YPQvNC+0LvRh9Cw0L3QuNGOINC20LXQu9GC0L7QtSDQv9C+0LvQtVxuICBcdFx0XHRjbG9zZWQgKGJvb2wpOiDQtNCw0LXRgiDQstC+0LfQvNC+0LbQvdC+0YHRgtGMINC30LDQutGA0YvRgtGMINGB0L7QvtCx0YnQtdC90LjQtSAo0L/QviDRg9C80L7Qu9GH0LDQvdC40Y4gdHJ1ZSlcbiAgXHRcdFx0dGltZTog0LLRgNC10LzRjyDQsiDQvNC40LvQuNGB0LjQutGD0L3QtNCw0YUg0YfQtdGA0LXQtyDQutC+0YLQvtGA0L7QtSDQv9C70LDRiNC60LAg0LfQsNC60YDQvtC10YLRgdGPXG5cblx0XHRcdCMjI1xuXHRcdFx0aWYgIW9wdHMuY2xvc2VkXG5cdFx0XHRcdG9wdHMuY2xvc2VkID0gdHJ1ZVxuXG5cdFx0XHRtb2RlbCA9IG5ldyBCYWNrYm9uZS5Nb2RlbCBvcHRzXG5cdFx0XHRAbm90aWZpVmlldyA9IG5ldyBOb3RpZmljYXRpb24uVmlld1xuXHRcdFx0XHRtb2RlbDogbW9kZWxcblxuXHRcdFx0dGltZSA9IGlmIG9wdHMudGltZSB0aGVuIG9wdHMudGltZSBlbHNlIDMwMDBcblxuXHRcdFx0aWYgTm90aWZpY2F0aW9uLlRpbWVvdXRcblx0XHRcdFx0Y2xlYXJUaW1lb3V0IE5vdGlmaWNhdGlvbi5UaW1lb3V0XG5cblx0XHRcdE5vdGlmaWNhdGlvbi5UaW1lb3V0ID0gc2V0VGltZW91dCA9PlxuXHRcdFx0XHRkbyBAY2xvc2Vcblx0XHRcdCx0aW1lXG5cblx0XHRcdGFwcC5ub3RpZmljYXRpb24uc2hvdyBAbm90aWZpVmlld1xuXG5cdFx0Y2xvc2U6IC0+XG5cdFx0XHRpZiBAbm90aWZpVmlld1xuXHRcdFx0XHRkbyBAbm90aWZpVmlldy5kZXN0cm95XG5cblx0XHRcdGlmIEBzdWJzY3JpYmVOb3RpZmlcblx0XHRcdFx0ZG8gQHNob3dTdWJzY3JpYmVcblxuXHRcdHNob3dTdWJzY3JpYmU6IC0+XG5cdFx0XHRAc3Vic2NyaWJlTm90aWZpID0gdHJ1ZVxuXHRcdFx0bW9kZWwgPSBuZXcgQmFja2JvbmUuTW9kZWxcblx0XHRcdFx0Y2xhc3M6ICdlcnJvcidcblx0XHRcdFx0bWVzc2FnZTogYXBwLmxvY2FsZS5ub3RpZmljYXRpb24uc3Vic01cblx0XHRcdFx0Y2xvc2VkOiBmYWxzZVxuXG5cdFx0XHRAbm90aWZpVmlldyA9IG5ldyBOb3RpZmljYXRpb24uVmlld1xuXHRcdFx0XHRtb2RlbDogbW9kZWxcblxuXHRcdFx0YXBwLm5vdGlmaWNhdGlvbi5zaG93IEBub3RpZmlWaWV3XG5cdFx0XHQkKHdpbmRvdykucmVzaXplKClcblxuXHRcdGNsb3NlU3Vic2NyaWJlOiAtPlxuXHRcdFx0QHN1YnNjcmliZU5vdGlmaSA9IGZhbHNlXG5cdFx0XHRkbyBAY2xvc2VcblxuXHROb3RpZmljYXRpb24uQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cblx0cmV0dXJuIE5vdGlmaWNhdGlvblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL25vdGlmaWNhdGlvbi9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgW10sIC0+XG5cdE5vdGlmaWNhdGlvbiA9IHt9XG5cblx0Y2xhc3MgTm90aWZpY2F0aW9uLlZpZXcgZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6IChzRGF0YSktPlxuXHRcdFx0aHRtbCA9IHJlcXVpcmUoJy4vdGVtcGxhdGVzL25vdGlmaWNhdGlvbi1kZWZhdWx0LmphZGUnKVxuXHRcdFx0cmV0dXJuIGh0bWwoc0RhdGEpXG5cdFx0Y2xhc3NOYW1lOiAtPlxuXHRcdFx0cmV0dXJuIGlmIEBtb2RlbC5oYXMgJ3R5cGUnIHRoZW4gQG1vZGVsLmdldCAndHlwZScgZWxzZSAnZGVmYXVsdCdcblxuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAuY2xvc2UtaWNvbic6IC0+XG5cdFx0XHRcdGRvIE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLmNsb3NlXG5cblx0XHRcdCdjbGljayAudG8tc3Vic2NyaWJlJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6cHJvZmlsZSdcblxuXHRcdG9uU2hvdzogLT5cblx0XHRcdCQoJ2JvZHknKS5hZGRDbGFzcyAnbm90aWZpJ1xuXG5cdFx0b25EZXN0cm95OiAtPlxuXHRcdFx0JCgnYm9keScpLnJlbW92ZUNsYXNzICdub3RpZmknXG5cblx0cmV0dXJuIE5vdGlmaWNhdGlvblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL25vdGlmaWNhdGlvbi92aWV3LmNvZmZlZVxuICoqLyIsInZhciBqYWRlID0gcmVxdWlyZShcIi9Vc2Vycy9lbWlsL3dvcmtfcHJvamVjdC9ub2RlLm11c2ljL25vZGVfbW9kdWxlcy9qYWRlL2xpYi9ydW50aW1lLmpzXCIpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHRlbXBsYXRlKGxvY2Fscykge1xudmFyIGJ1ZiA9IFtdO1xudmFyIGphZGVfbWl4aW5zID0ge307XG52YXIgamFkZV9pbnRlcnA7XG47dmFyIGxvY2Fsc19mb3Jfd2l0aCA9IChsb2NhbHMgfHwge30pOyhmdW5jdGlvbiAoY2xvc2VkLCBtZXNzYWdlKSB7XG5idWYucHVzaChcIlwiICsgKCgoamFkZV9pbnRlcnAgPSBtZXNzYWdlKSA9PSBudWxsID8gJycgOiBqYWRlX2ludGVycCkpICsgXCJcIik7XG5pZiAoIGNsb3NlZClcbntcbmJ1Zi5wdXNoKFwiPGRpdiBjbGFzcz1cXFwiY2xvc2UtaWNvblxcXCI+PC9kaXY+XCIpO1xufX0uY2FsbCh0aGlzLFwiY2xvc2VkXCIgaW4gbG9jYWxzX2Zvcl93aXRoP2xvY2Fsc19mb3Jfd2l0aC5jbG9zZWQ6dHlwZW9mIGNsb3NlZCE9PVwidW5kZWZpbmVkXCI/Y2xvc2VkOnVuZGVmaW5lZCxcIm1lc3NhZ2VcIiBpbiBsb2NhbHNfZm9yX3dpdGg/bG9jYWxzX2Zvcl93aXRoLm1lc3NhZ2U6dHlwZW9mIG1lc3NhZ2UhPT1cInVuZGVmaW5lZFwiP21lc3NhZ2U6dW5kZWZpbmVkKSk7O3JldHVybiBidWYuam9pbihcIlwiKTtcbn1cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL25vdGlmaWNhdGlvbi90ZW1wbGF0ZXMvbm90aWZpY2F0aW9uLWRlZmF1bHQuamFkZVxuICoqIG1vZHVsZSBpZCA9IDEwXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvbm90aWZpY2F0aW9uL3N0eWxlLnN0eWxcbiAqKiBtb2R1bGUgaWQgPSAxMVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL3BsYXllci9zdHlsZS5zdHlsXG4gKiogbW9kdWxlIGlkID0gMTJcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Zyb250ZW5kL3N0eWxlcy9hcHAvc3R5bGUuc3R5bFxuICoqIG1vZHVsZSBpZCA9IDEzXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCJfLmV4dGVuZCBCYWNrYm9uZS5Nb2RlbC5wcm90b3R5cGUsIFxuICAgIHVybDogLT5cbiAgICAgIGJhc2UgPVxuICAgICAgICBfLnJlc3VsdCh0aGlzLCAndXJsUm9vdCcpIHx8XG4gICAgICAgIF8ucmVzdWx0KHRoaXMuY29sbGVjdGlvbiwgJ3VybCcpIHx8XG4gICAgICAgIHVybEVycm9yKCk7XG5cbiAgICAgIGlkID0gdGhpcy5nZXQodGhpcy5pZEF0dHJpYnV0ZSk7XG4gICAgICByZXR1cm4gYmFzZTtcblxuICAgIHN5bmM6IChtZXRob2QsbW9kZWwsb3B0aW9ucyktPlxuICAgICAgaWYgbWV0aG9kIGlzICd1cGRhdGUnXG4gICAgICAgIGlmIG1vZGVsLnVwZGF0ZVVybFxuICAgICAgICAgIG1vZGVsLnVybCA9IG1vZGVsLnVwZGF0ZVVybFxuICAgICAgICBlbHNlXG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQSAndXBkYXRlVXJsJyBwcm9wZXJ0eSBvciBmdW5jdGlvbiBtdXN0IGJlIHNwZWNpZmllZFwiKTtcblxuICAgICAgaWYgbWV0aG9kIGlzICdkZWxldGUnXG4gICAgICAgIGlmIG1vZGVsLmRlbGV0ZVVybFxuICAgICAgICAgIG1vZGVsLnVybCA9IG1vZGVsLmRlbGV0ZVVybFxuICAgICAgICBlbHNlXG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQSAnZGVsZXRlVXJsJyBwcm9wZXJ0eSBvciBmdW5jdGlvbiBtdXN0IGJlIHNwZWNpZmllZFwiKTtcblxuICAgICAgaWYgbWV0aG9kIGlzICdjcmVhdGUnXG4gICAgICAgIGlmIG1vZGVsLmNyZWF0ZVVybFxuICAgICAgICAgIG1vZGVsLnVybCA9IG1vZGVsLmNyZWF0ZVVybFxuICAgICAgICBlbHNlXG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQSAnY3JlYXRlVXJsJyBwcm9wZXJ0eSBvciBmdW5jdGlvbiBtdXN0IGJlIHNwZWNpZmllZFwiKTtcblxuICAgICAgbWV0aG9kID0gJ3JlYWQnXG4gICAgICAgIFxuXG4gICAgICBCYWNrYm9uZS5zeW5jLmFwcGx5IHRoaXMsW21ldGhvZCxtb2RlbCxvcHRpb25zXVxuXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvc2V0dGluZy9iYWNrYm9uZV9tb2RlbF9tZXRob2RzLmNvZmZlZVxuICoqLyIsImNsYXNzIE1hcmlvbmV0dGUuQXBwUm91dGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5BcHBSb3V0ZXJcblx0aW5pdGlhbGl6ZTogKCktPlxuXHRcdHJvdXRlcyA9IEBhcHBSb3V0ZXNcblx0XHRuZXdSb3V0ZXMgPSB7fVxuXHRcdEJhc2VVcmwgPSAnKDpsYW5nKSdcblx0XHRmb3Iga2V5LHZhbCBvZiByb3V0ZXNcblx0XHRcdGlmIGtleSBpcyAnJ1xuXHRcdFx0XHRuZXdSb3V0ZXNbQmFzZVVybF0gPSB2YWxcblx0XHRcdFx0bmV3Um91dGVzW0Jhc2VVcmwrJy8nXSA9IHZhbFxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRuZXdSb3V0ZXNbQmFzZVVybCsnLycra2V5XSA9IHZhbFxuXHRcdFx0XHRuZXdSb3V0ZXNbQmFzZVVybCsnLycra2V5KycvJ10gPSB2YWxcblxuXHRcdEBhcHBSb3V0ZXMgPSBuZXdSb3V0ZXNcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9zZXR0aW5nL3JvdXRlX3NldHRpbmcuY29mZmVlXG4gKiovIiwiZG8gLT5cbiAgc3luYyA9IEJhY2tib25lLnN5bmNcbiAgQmFja2JvbmUuc3luYyA9IChtZXRob2QsbW9kZWwsb3B0aW9ucyktPlxuICAgIGRhdGEgPSBvcHRpb25zLmRhdGEgb3Ige31cbiAgICBkYXRhLnNpZCA9IGFwcC51c2VyLnNpZFxuICAgIF8uZXh0ZW5kIG9wdGlvbnMsXG4gICAgICAgIGRhdGE6IGRhdGFcbiAgICAgICAgXG4gICAgc3luYy5hcHBseSB0aGlzLCBbbWV0aG9kLG1vZGVsLG9wdGlvbnNdXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL3NldHRpbmcvc3luY19yZXdyaXRlLmNvZmZlZVxuICoqLyIsImFwcC5tb2R1bGUgXCJIZWxwZXJzLkZpbHRlcnNcIiwgKEZpbHRlcnMsIGFwcCwgQmFja2JvbmUsIE1hcmlvbmV0dGUsICQsIF8pLT5cblx0Y2xhc3MgY29sbGVjdGlvbiBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXHRcdEZpbHRlcjogKG9wdGlvbnMpLT5cblx0XHRcdG9yaWdpbmFsID0gb3B0aW9ucy5jb2xsZWN0aW9uO1xuXHRcdFx0ZmlsdGVyZWQgPSBuZXcgb3JpZ2luYWwuY29uc3RydWN0b3IoKTtcblx0XHRcdGZpbHRlcmVkLmFkZChvcmlnaW5hbC5tb2RlbHMpO1xuXHRcdFx0ZmlsdGVyZWQuZmlsdGVyRnVuY3Rpb24gPSBvcHRpb25zLmZpbHRlckZ1bmN0aW9uO1xuXG5cdFx0XHRhcHBseUZpbHRlciA9IChmaWx0ZXJDcml0ZXJpb24sIGZpbHRlclN0cmF0ZWd5LCBjb2xsZWN0aW9uKS0+XG5cdFx0XHRcdGNvbGxlY3Rpb24gPSBjb2xsZWN0aW9uIHx8IG9yaWdpbmFsO1xuXHRcdFx0XHRjcml0ZXJpb247XG5cdFx0XHRcdGlmIGZpbHRlclN0cmF0ZWd5IGlzIFwiZmlsdGVyXCJcblx0XHRcdFx0XHRjcml0ZXJpb24gPSBmaWx0ZXJDcml0ZXJpb24udHJpbSgpO1xuXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRjcml0ZXJpb24gPSBmaWx0ZXJDcml0ZXJpb247XG5cblxuXHRcdFx0XHRpdGVtcyA9IFtdXG5cdFx0XHRcdGlmIGNyaXRlcmlvblxuXHRcdFx0XHRcdGlmKGZpbHRlclN0cmF0ZWd5ID09IFwiZmlsdGVyXCIpXG5cdFx0XHRcdFx0XHRpZiAhZmlsdGVyZWQuZmlsdGVyRnVuY3Rpb25cblx0XHRcdFx0XHRcdFx0dGhyb3coXCJBdHRlbXB0ZWQgdG8gdXNlICdmaWx0ZXInIGZ1bmN0aW9uLCBidXQgbm9uZSB3YXMgZGVmaW5lZFwiKTtcblxuXHRcdFx0XHRcdFx0ZmlsdGVyRnVuY3Rpb24gPSBmaWx0ZXJlZC5maWx0ZXJGdW5jdGlvbihjcml0ZXJpb24pO1xuXHRcdFx0XHRcdFx0aXRlbXMgPSBjb2xsZWN0aW9uLmZpbHRlcihmaWx0ZXJGdW5jdGlvbik7XG5cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRpdGVtcyA9IGNvbGxlY3Rpb24ud2hlcmUoY3JpdGVyaW9uKTtcblxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0aXRlbXMgPSBjb2xsZWN0aW9uLm1vZGVscztcblxuXG5cdFx0XHRcdGZpbHRlcmVkLl9jdXJyZW50Q3JpdGVyaW9uID0gY3JpdGVyaW9uO1xuXG5cdFx0XHRcdHJldHVybiBpdGVtcztcblxuXHRcdFx0ZmlsdGVyZWQuZmlsdGVyID0gKGZpbHRlckNyaXRlcmlvbiktPlxuXHRcdFx0XHRmaWx0ZXJlZC5fY3VycmVudEZpbHRlciA9IFwiZmlsdGVyXCI7XG5cdFx0XHRcdGl0ZW1zID0gYXBwbHlGaWx0ZXIoZmlsdGVyQ3JpdGVyaW9uLCBcImZpbHRlclwiKTtcblxuXHRcdFx0XHRmaWx0ZXJlZC5yZXNldChpdGVtcyk7XG5cdFx0XHRcdHJldHVybiBmaWx0ZXJlZDtcblxuXHRcdFx0ZmlsdGVyZWQud2hlcmUgPSAoZmlsdGVyQ3JpdGVyaW9uKS0+XG5cdFx0XHRcdGZpbHRlcmVkLl9jdXJyZW50RmlsdGVyID0gXCJ3aGVyZVwiO1xuXHRcdFx0XHRpdGVtcyA9IGFwcGx5RmlsdGVyKGZpbHRlckNyaXRlcmlvbiwgXCJ3aGVyZVwiKTtcblxuXHRcdFx0XHRmaWx0ZXJlZC5yZXNldChpdGVtcyk7XG5cdFx0XHRcdHJldHVybiBmaWx0ZXJlZDtcblxuXHRcdFx0ZmlsdGVyZWQuc3RvcExpc3RlbkNvbGxlY3Rpb24gPSAtPlxuXHRcdFx0XHRvcmlnaW5hbC5vZmYgJ2FkZCdcblx0XHRcdFx0b3JpZ2luYWwub2ZmICdyZXNldCdcblxuXHRcdFx0b3JpZ2luYWwub24gXCJyZXNldFwiLCAoKS0+XG5cdFx0XHRcdGl0ZW1zID0gYXBwbHlGaWx0ZXIoZmlsdGVyZWQuX2N1cnJlbnRDcml0ZXJpb24sIGZpbHRlcmVkLl9jdXJyZW50RmlsdGVyKTtcblx0XHRcdFx0ZmlsdGVyZWQucmVzZXQoaXRlbXMpO1xuXG5cblx0XHRcdG9yaWdpbmFsLm9uIFwiYWRkXCIsIChtb2RlbHMpLT5cblx0XHRcdFx0Y29sbCA9IG5ldyBvcmlnaW5hbC5jb25zdHJ1Y3RvcigpO1xuXHRcdFx0XHRjb2xsLmFkZChtb2RlbHMpO1xuXHRcdFx0XHRpdGVtcyA9IGFwcGx5RmlsdGVyKGZpbHRlcmVkLl9jdXJyZW50Q3JpdGVyaW9uLCBmaWx0ZXJlZC5fY3VycmVudEZpbHRlciwgY29sbCk7XG5cdFx0XHRcdGZpbHRlcmVkLmFkZChpdGVtcyk7XG5cblx0XHRcdHJldHVybiBmaWx0ZXJlZDtcblxuXHRGaWx0ZXJzLkNvbGxlY3Rpb24gPSBuZXcgY29sbGVjdGlvblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9oZWxwZXJzL2ZpbHRlcmVkX2NvbGxlY3Rpb24uY29mZmVlXG4gKiovIiwiQmFja2JvbmUuTW9kZWwuU2VsZWN0ZWQgPSBCYWNrYm9uZS5Nb2RlbC5leHRlbmQoe1xuXHRzZWxlY3Q6ICgpLT5cblx0XHRpZiB0aGlzLnNlbGVjdGVkXG5cdFx0XHRyZXR1cm5cblxuXHRcdHRoaXMuc2VsZWN0ZWQgPSB0cnVlO1xuXHRcdHRoaXMudHJpZ2dlcihcInNlbGVjdGVkXCIsIHRoaXMpO1xuXG5cdFx0aWYgdGhpcy5jb2xsZWN0aW9uXG5cdFx0XHR0aGlzLmNvbGxlY3Rpb24uc2VsZWN0KHRoaXMpO1xuXG5cblx0ZGVzZWxlY3Q6ICgpLT5cblx0XHRpZiAhdGhpcy5zZWxlY3RlZFxuXHRcdFx0cmV0dXJuXG5cblx0XHR0aGlzLnNlbGVjdGVkID0gZmFsc2U7XG5cdFx0dGhpcy50cmlnZ2VyKFwiZGVzZWxlY3RlZFwiLCB0aGlzKTtcblxuXHRcdGlmIHRoaXMuY29sbGVjdGlvblxuXHRcdFx0dGhpcy5jb2xsZWN0aW9uLmRlc2VsZWN0KHRoaXMpO1xuXG5cblx0dG9nZ2xlU2VsZWN0ZWQ6ICgpLT5cblx0XHRpZiB0aGlzLnNlbGVjdGVkXG5cdFx0XHR0aGlzLmRlc2VsZWN0KCk7XG5cdFx0ZWxzZVxuXHRcdFx0dGhpcy5zZWxlY3QoKTtcblxufSlcblxuQmFja2JvbmUuQ29sbGVjdGlvbi5TaW5nbGVTZWxlY3QgPSBCYWNrYm9uZS5Db2xsZWN0aW9uLmV4dGVuZCh7XG5cdHNlbGVjdDogKG1vZGVsKS0+XG5cdFx0aWYgbW9kZWwgJiYgdGhpcy5zZWxlY3RlZCBpcyBtb2RlbFxuXHRcdFx0cmV0dXJuO1xuXG5cdFx0dGhpcy5kZXNlbGVjdCgpO1xuXG5cdFx0dGhpcy5zZWxlY3RlZCA9IG1vZGVsO1xuXHRcdHRoaXMuc2VsZWN0ZWQuc2VsZWN0KCk7XG5cdFx0dGhpcy50cmlnZ2VyKFwic2VsZWN0Om9uZVwiLCBtb2RlbCk7XG5cblx0ZGVzZWxlY3Q6IChtb2RlbCktPlxuXHRcdGlmICF0aGlzLnNlbGVjdGVkXG5cdFx0XHRyZXR1cm5cblxuXHRcdG1vZGVsID0gbW9kZWwgfHwgdGhpcy5zZWxlY3RlZDtcblxuXHRcdGlmIHRoaXMuc2VsZWN0ZWQgaXNudCBtb2RlbFxuXHRcdFx0cmV0dXJuXG5cblx0XHR0aGlzLnNlbGVjdGVkLmRlc2VsZWN0KCk7XG5cdFx0dGhpcy50cmlnZ2VyKFwiZGVzZWxlY3Q6b25lXCIsIHRoaXMuc2VsZWN0ZWQpO1xuXHRcdGRlbGV0ZSB0aGlzLnNlbGVjdGVkO1xuXG59KVxuXG5CYWNrYm9uZS5Db2xsZWN0aW9uLk11bHRpU2VsZWN0ZWQgPSBCYWNrYm9uZS5Db2xsZWN0aW9uLmV4dGVuZCh7XG5cdGluaXRpYWxpemU6IC0+XG5cdFx0dGhpcy5zZWxlY3RlZCA9IHt9XG5cblx0c2VsZWN0OiAobW9kZWwpLT5cblx0XHRpZiB0aGlzLnNlbGVjdGVkW21vZGVsLmNpZF1cblx0XHRcdHJldHVyblxuXHRcdHRoaXMuc2VsZWN0ZWRbbW9kZWwuY2lkXSA9IG1vZGVsO1xuXHRcdG1vZGVsLnNlbGVjdCgpO1xuXHRcdEBjYWxjdWxhdGVTZWxlY3RlZExlbmd0aCh0aGlzKTtcblxuXHRkZXNlbGVjdDogKG1vZGVsKS0+XG5cdFx0aWYgIXRoaXMuc2VsZWN0ZWRbbW9kZWwuY2lkXVxuXHRcdFx0cmV0dXJuXG5cblx0XHRkZWxldGUgdGhpcy5zZWxlY3RlZFttb2RlbC5jaWRdO1xuXHRcdG1vZGVsLmRlc2VsZWN0KCk7XG5cdFx0QGNhbGN1bGF0ZVNlbGVjdGVkTGVuZ3RoKHRoaXMpO1xuXG5cblx0c2VsZWN0QWxsOiAoKS0+XG5cdFx0dGhpcy5lYWNoIChtb2RlbCktPlxuXHRcdFx0bW9kZWwuc2VsZWN0KCk7XG5cdFx0QGNhbGN1bGF0ZVNlbGVjdGVkTGVuZ3RoKHRoaXMpO1xuXG5cblx0c2VsZWN0Tm9uZTogKCktPlxuXHRcdGlmIHRoaXMuc2VsZWN0ZWRMZW5ndGggaXMgMFxuXHRcdFx0cmV0dXJuO1xuXHRcdHRoaXMuZWFjaCAobW9kZWwpLT5cblx0XHRcdG1vZGVsLmRlc2VsZWN0KCk7XG5cblx0XHRAY2FsY3VsYXRlU2VsZWN0ZWRMZW5ndGgodGhpcyk7XG5cblxuXHR0b2dnbGVTZWxlY3RBbGw6ICgpLT5cblx0XHRpZiB0aGlzLnNlbGVjdGVkTGVuZ3RoIGlzIHRoaXMubGVuZ3RoXG5cdFx0XHR0aGlzLnNlbGVjdE5vbmUoKTtcblx0XHRlbHNlXG5cdFx0XHR0aGlzLnNlbGVjdEFsbCgpO1xuXG5cblx0Y2FsY3VsYXRlU2VsZWN0ZWRMZW5ndGg6IChjb2xsZWN0aW9uKS0+XG5cdFx0Y29sbGVjdGlvbi5zZWxlY3RlZExlbmd0aCA9IF8uc2l6ZShjb2xsZWN0aW9uLnNlbGVjdGVkKTtcblxuXHRcdHNlbGVjdGVkTGVuZ3RoID0gY29sbGVjdGlvbi5zZWxlY3RlZExlbmd0aDtcblx0XHRsZW5ndGggPSBjb2xsZWN0aW9uLmxlbmd0aDtcblxuXHRcdGlmIHNlbGVjdGVkTGVuZ3RoIGlzIGxlbmd0aFxuXHRcdFx0Y29sbGVjdGlvbi50cmlnZ2VyKFwic2VsZWN0OmFsbFwiLCBjb2xsZWN0aW9uKTtcblx0XHRcdHJldHVybjtcblxuXHRcdGlmIHNlbGVjdGVkTGVuZ3RoIGlzIDBcblx0XHRcdGNvbGxlY3Rpb24udHJpZ2dlcihcInNlbGVjdDpub25lXCIsIGNvbGxlY3Rpb24pO1xuXHRcdFx0cmV0dXJuO1xuXG5cdFx0aWYgc2VsZWN0ZWRMZW5ndGggPiAwICYmIHNlbGVjdGVkTGVuZ3RoIDwgbGVuZ3RoXG5cdFx0XHRjb2xsZWN0aW9uLnRyaWdnZXIoXCJzZWxlY3Q6c29tZVwiLCBjb2xsZWN0aW9uKTtcblx0XHRcdHJldHVybjtcblxufSlcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9tb2RlbC9zZWxlY3QuY29mZmVlXG4gKiovIiwiZGVmaW5lIFsnLi4vY29uZmlnL2xvZycsJy4uL3BhcnRzL3BsYXllcicsJy4uL3BhcnRzL3NpZGViYXInXSwgKExvZ2dlcixQbGF5ZXIsU2lkZWJhciktPlxuXHRjbGFzcyBSb3V0ZXIgZXh0ZW5kcyBNYXJpb25ldHRlLkFwcFJvdXRlclxuXHRcdGFwcFJvdXRlczpcblx0XHRcdCd0cmFjay86aWQnOiAncGxheWVkVHJhY2snXG5cdFx0XHQnJzogJ3Nob3dSZWNvbWVuZCdcblx0XHRcdCduZXcnOiAnc2hvd05ldydcblx0XHRcdCdmZWVkJzogJ3Nob3dGZWVkJ1xuXHRcdFx0J3JlY29tZW5kJzogJ3Nob3dSZWNvbWVuZCdcblx0XHRcdCdwbGF5bGlzdHMnOiAnc2hvd1BsYXlsaXN0cydcblx0XHRcdCdwbGF5bGlzdC86aWQnOiAnc2hvd1BsYXlsaXN0J1xuXHRcdFx0J2lkOmlkJzogJ3Nob3dVc2VyUGFnZSdcblx0XHRcdCd2aWRlbyc6ICdzaG93VmlkZW8nXG5cdFx0XHQncHJvZmlsZSgvOnRhYiknOiAnc2hvd1Byb2ZpbGUnXG5cdFx0XHQndXNlci90cmFja3MnOiAnc2hvd1VzZXJUcmFja3MnXG5cdFx0XHQndXNlci9hbGJ1bXMnOiAnc2hvd1VzZXJBbGJ1bXMnXG5cdFx0XHQnYWxidW0vOmlkJzogJ3Nob3dBbGJ1bSdcblx0XHRcdCdhcnRpc3QvOmlkJzogJ3Nob3dBcnRpc3QnXG5cdFx0XHQnc2VhcmNoL3RyYWNrcyc6ICdzZWFyY2hUcmFja3MnXG5cdFx0XHQnc2VhcmNoL2FsYnVtcyc6ICdzZWFyY2hBbGJ1bXMnXG5cdFx0XHQnc2VhcmNoL2FydGlzdHMnOiAnc2VhcmNoQXJ0aXN0J1xuXG5cdGNsYXNzIEFQSSBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXHRcdHNob3dSZWNvbWVuZDogLT5cblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAocmVxdWlyZSktPlxuXHRcdFx0XHRSZWNvbWVuZCA9IHJlcXVpcmUoJy4vcmVjb21lbmQnKVxuXHRcdFx0XHQkKCcjaGVhZGVyIGgxJykudGV4dCBhcHAubG9jYWxlLm1lbnUucG9wdWxhclxuXHRcdFx0XHRkbyBSZWNvbWVuZC5Db250cm9sbGVyLnNob3dQYWdlXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzd2l0Y2g6cGFnZScsICdyZWNvbWVuZCdcblx0XHRcdFx0YXBwLkdBUGFnZUFjdGlvbiAncGFnZS1vcGVuJywn0JPQu9Cw0LLQvdCw0Y8nXG5cdFx0XHRcdExvZ2dlci5jcmVhdGVOZXdMb2cgYXBwLmdldFJlcXVlc3RVcmxTaWQoJy9nZW5lcmFsVjEvbG9nL29wZW5TZWN0aW9uJywnP3NlY3Rpb249bWFpbicpXG5cblx0XHRzaG93RmVlZDogLT5cblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAocmVxdWlyZSktPlxuXHRcdFx0XHRGZWVkID0gcmVxdWlyZSgnLi9mZWVkJylcblx0XHRcdFx0JCgnI2hlYWRlciBoMScpLnRleHQgYXBwLmxvY2FsZS5tZW51LmZlZWRcblx0XHRcdFx0ZG8gRmVlZC5Db250cm9sbGVyLnNob3dQYWdlXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzd2l0Y2g6cGFnZScsICdmZWVkJ1xuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwYWdlLW9wZW4nLCfQm9C10L3RgtCwJ1xuXHRcdFx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKCcvZ2VuZXJhbFYxL2xvZy9vcGVuU2VjdGlvbicsJz9zZWN0aW9uPXdhbGwnKVxuXG5cdFx0c2hvd1VzZXJQYWdlOiAobGFuZywgaWQpLT5cblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAocmVxdWlyZSktPlxuXHRcdFx0XHRVc2VyUGFnZSA9IHJlcXVpcmUoJy4vdXNlcl9wYWdlJylcblx0XHRcdFx0JCgnI2hlYWRlciBoMScpLnRleHQgJydcblx0XHRcdFx0VXNlclBhZ2UuQ29udHJvbGxlci5zaG93UGFnZSBpZFxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc3dpdGNoOnBhZ2UnLCAndXNlcidcblx0XHRcdFx0YXBwLkdBUGFnZUFjdGlvbiAncGFnZS1vcGVuJywn0J/QvtC70YzQt9C+0LLQsNGC0LXQu9GMLScraWRcblxuXHRcdHNob3dOZXc6IC0+XG5cdFx0XHRyZXF1aXJlLmVuc3VyZSBbXSwgKHJlcXVpcmUpLT5cblx0XHRcdFx0TmV3ID0gcmVxdWlyZSgnLi9uZXcnKVxuXHRcdFx0XHQkKCcjaGVhZGVyIGgxJykudGV4dCBhcHAubG9jYWxlLm1lbnUubmV3XG5cdFx0XHRcdGRvIE5ldy5Db250cm9sbGVyLnNob3dQYWdlXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzd2l0Y2g6cGFnZScsICduZXcnXG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ3BhZ2Utb3BlbicsJ9Cd0L7QstC40L3QutC4J1xuXHRcdFx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKCcvZ2VuZXJhbFYxL2xvZy9vcGVuU2VjdGlvbicsJz9zZWN0aW9uPW5ldycpXG5cblx0XHRzaG93UGxheWxpc3RzOiAtPlxuXHRcdFx0cmVxdWlyZS5lbnN1cmUgW10sIChyZXF1aXJlKS0+XG5cdFx0XHRcdFBsYXlsaXN0cyA9IHJlcXVpcmUoJy4vcGxheWxpc3RzJylcblx0XHRcdFx0JCgnI2hlYWRlciBoMScpLnRleHQgYXBwLmxvY2FsZS5tZW51LnBsYXlsaXN0c1xuXHRcdFx0XHRkbyBQbGF5bGlzdHMuQ29udHJvbGxlci5zaG93UGFnZVxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc3dpdGNoOnBhZ2UnLCAncGxheWxpc3RzJ1xuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwYWdlLW9wZW4nLCfQn9C70LXQudC70LjRgdGC0YsnXG5cblx0XHRzaG93VmlkZW86IC0+XG5cdFx0XHRyZXF1aXJlLmVuc3VyZSBbXSwgKHJlcXVpcmUpLT5cblx0XHRcdFx0VmlkZW8gPSByZXF1aXJlKCcuL3ZpZGVvJylcblx0XHRcdFx0JCgnI2hlYWRlciBoMScpLnRleHQgYXBwLmxvY2FsZS5tZW51LmNsaXBzXG5cdFx0XHRcdGRvIFZpZGVvLkNvbnRyb2xsZXIuc2hvd1BhZ2Vcblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3N3aXRjaDpwYWdlJywgJ3ZpZGVvJ1xuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwYWdlLW9wZW4nLCfQmtC70LjQv9GLJ1xuXG5cdFx0c2hvd1Byb2ZpbGU6IChsZW5nLHRhYj1cIm1haW5cIiktPlxuXHRcdFx0cmVxdWlyZS5lbnN1cmUgW10sIChyZXF1aXJlKS0+XG5cdFx0XHRcdFByb2ZpbGUgPSByZXF1aXJlKCcuL3Byb2ZpbGUnKVxuXHRcdFx0XHQkKCcjaGVhZGVyIGgxJykudGV4dCBhcHAubG9jYWxlLm1lbnUucHJvZmlsZVxuXHRcdFx0XHRQcm9maWxlLkNvbnRyb2xsZXIuc2hvd1BhZ2UgdGFiXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzd2l0Y2g6cGFnZScsICdwcm9maWxlJ1xuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwYWdlLW9wZW4nLCfQn9GA0L7RhNC40LvRjCdcblxuXHRcdHBsYXllZFRyYWNrOiAobGVuZyxpZCktPlxuXHRcdFx0YXBwLkdBUGFnZUFjdGlvbiAncGFnZS1vcGVuJywn0KjQsNGA0LjQvdCzINGC0YDQtdC60LAgLSAnK2lkXG5cdFx0XHRhcHAudHJpZ2dlciAnc2hvdzpyZWNvbWVuZCdcblx0XHRcdFBsYXllci5Db250cm9sbGVyLnBsYXlUcmFjayBpZFxuXG5cdFx0c2hvd0FsYnVtOiAobGVuZyxpZCxkYXRhLGRpcmVjdCktPlxuXHRcdFx0aWYgIWRpcmVjdFxuXHRcdFx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKFwiL2dlbmVyYWxWMS9sb2cvcmVsZWFzZVwiLFwiP2lkPSN7aWR9JnJlZmVyZXI9ZGlyZWN0TGlua1wiKVxuXHRcdFx0cmVxdWlyZS5lbnN1cmUgW10sIChyZXF1aXJlKS0+XG5cdFx0XHRcdEFsYnVtID0gcmVxdWlyZSgnLi9hbGJ1bScpXG5cdFx0XHRcdEFsYnVtLkNvbnRyb2xsZXIuc2hvd1BhZ2UgaWQsZGF0YVxuXG5cdFx0c2hvd0FydGlzdDogKGxlbmcsaWQsZGlyZWN0KS0+XG5cdFx0XHRpZiAhZGlyZWN0XG5cdFx0XHRcdExvZ2dlci5jcmVhdGVOZXdMb2cgYXBwLmdldFJlcXVlc3RVcmxTaWQoXCIvZ2VuZXJhbFYxL2xvZy9hcnRpc3RDcmVkaXRcIixcIj9pZD0je2lkfSZyZWZlcmVyPWRpcmVjdExpbmtcIilcblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAocmVxdWlyZSktPlxuXHRcdFx0XHRBcnRpc3QgPSByZXF1aXJlKCcuL2FydGlzdCcpXG5cdFx0XHRcdEFydGlzdC5Db250cm9sbGVyLnNob3dQYWdlIGlkXG5cblx0XHRzaG93UGxheWxpc3Q6IChsZW5nLGlkLGRpcmVjdCktPlxuXHRcdFx0aWYgIWRpcmVjdFxuXHRcdFx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKFwiL2dlbmVyYWxWMS9sb2cvcHVibGljUGxheWxpc3RcIixcIj9pZD0je2lkfSZyZWZlcmVyPWRpcmVjdExpbmtcIilcblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAocmVxdWlyZSktPlxuXHRcdFx0XHRQbGF5bGlzdCA9IHJlcXVpcmUoJy4vcGxheWxpc3QnKVxuXHRcdFx0XHRQbGF5bGlzdC5Db250cm9sbGVyLnNob3dQYWdlIGlkXG5cblx0XHRzaG93VXNlclRyYWNrczogLT5cblx0XHRcdHJlcXVpcmUuZW5zdXJlIFtdLCAtPlxuXHRcdFx0XHRVc2VyVHJhY2tzID0gcmVxdWlyZSgnLi91c2VyL3RyYWNrcycpXG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ3BhZ2Utb3BlbicsJ9Ci0YDQtdC60Lgg0Lgg0L/Qu9C10LnQu9C40YHRgtGLJ1xuXHRcdFx0XHQkKCcjaGVhZGVyIGgxJykudGV4dCBhcHAubG9jYWxlLm1lbnUudHJhY2tzQW5kUGxheWxpc3RzXG5cdFx0XHRcdGRvIFVzZXJUcmFja3MuQ29udHJvbGxlci5zaG93UGFnZVxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc3dpdGNoOnBhZ2UnLCAndHJhY2tzJ1xuXG5cdFx0c2hvd1VzZXJBbGJ1bXM6IC0+XG5cdFx0XHRyZXF1aXJlLmVuc3VyZSBbXSwgLT5cblx0XHRcdFx0VXNlckFsYnVtcyA9IHJlcXVpcmUoJy4vdXNlci9hbGJ1bXMnKVxuXHRcdFx0XHRhcHAuR0FQYWdlQWN0aW9uICdwYWdlLW9wZW4nLCfQmNC30LHRgNCw0L3QvdGL0LUg0LDQu9GM0LHQvtC80YsnXG5cdFx0XHRcdCQoJyNoZWFkZXIgaDEnKS50ZXh0IGFwcC5sb2NhbGUubWVudS5hbGJ1bXNcblx0XHRcdFx0ZG8gVXNlckFsYnVtcy5Db250cm9sbGVyLnNob3dQYWdlXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzd2l0Y2g6cGFnZScsICdhbGJ1bXMnXG5cblx0XHRzZWFyY2hUcmFja3M6IChsZW5nLHZhbCktPlxuXHRcdFx0YXBwLkdBUGFnZUFjdGlvbiAncGFnZS1vcGVuJywn0J/QvtC40YHQuiDRgtGA0LXQutCwINC/0L4g0LfQsNC/0YDQvtGB0YMgLSAnK3ZhbFxuXHRcdFx0U2lkZWJhci5Db250cm9sbGVyLnNob3dTZWFyY2hUcmFja3NQYWdlIGFwcC5wYXJzZVBhdGgodmFsKS5xXG5cdFx0XHRhcHAub3BlblNlYXJjaFBhZ2VGcm9tU2l0ZSA9IGZhbHNlXG5cblx0XHRzZWFyY2hBbGJ1bXM6IChsZW5nLHZhbCktPlxuXHRcdFx0YXBwLkdBUGFnZUFjdGlvbiAncGFnZS1vcGVuJywn0J/QvtC40YHQuiDQsNC70YzQsdC+0LzQsCDQv9C+INC30LDQv9GA0L7RgdGDIC0gJyt2YWxcblx0XHRcdFNpZGViYXIuQ29udHJvbGxlci5zaG93U2VhcmNoQWxidW1zUGFnZSBhcHAucGFyc2VQYXRoKHZhbCkucVxuXHRcdFx0YXBwLm9wZW5TZWFyY2hQYWdlRnJvbVNpdGUgPSBmYWxzZVxuXG5cdFx0c2VhcmNoQXJ0aXN0OiAobGVuZyx2YWwpLT5cblx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ3BhZ2Utb3BlbicsJ9Cf0L7QuNGB0Log0LDRgNGC0LjRgdGC0LAg0L/QviDQt9Cw0L/RgNC+0YHRgyAtICcrdmFsXG5cdFx0XHRTaWRlYmFyLkNvbnRyb2xsZXIuc2hvd1NlYXJjaEFydGlzdHNQYWdlIGFwcC5wYXJzZVBhdGgodmFsKS5xXG5cdFx0XHRhcHAub3BlblNlYXJjaFBhZ2VGcm9tU2l0ZSA9IGZhbHNlXG5cblxuXG5cblx0YXBpID0gbmV3IEFQSVxuXG5cdG5ldyBSb3V0ZXJcblx0XHRjb250cm9sbGVyOiBhcGlcblxuXG5cdGFwcC5vbiAnc2hvdzpuZXcnLCAtPlxuXHRcdGFwcC5uYXZpZ2F0ZSBcIi9uZXdcIlxuXHRcdGRvIGFwaS5zaG93TmV3XG5cblx0YXBwLm9uICdzaG93OnVzZXI6cGFnZScsIChpZCktPlxuXHRcdGFwcC5uYXZpZ2F0ZSBcIi9pZCN7aWR9XCJcblx0XHRhcGkuc2hvd1VzZXJQYWdlIG51bGwsIGlkXG5cblx0YXBwLm9uICdzaG93OnBsYXlsaXN0cycsIC0+XG5cdFx0YXBwLm5hdmlnYXRlIFwiL3BsYXlsaXN0c1wiXG5cdFx0ZG8gYXBpLnNob3dQbGF5bGlzdHNcblxuXHRhcHAub24gJ3Nob3c6ZmVlZCcsIC0+XG5cdFx0YXBwLm5hdmlnYXRlIFwiL2ZlZWRcIlxuXHRcdGRvIGFwaS5zaG93RmVlZFxuXG5cdGFwcC5vbiAnc2hvdzpjaGFydHMnLCAtPlxuXHRcdGFwcC5uYXZpZ2F0ZSBcIi9jaGFydHNcIlxuXHRcdGRvIGFwaS5zaG93Q2hhcnRzXG5cblx0YXBwLm9uICdzaG93OnJlY29tZW5kJywgLT5cblx0XHRhcHAubmF2aWdhdGUgXCIvXCJcblx0XHRkbyBhcGkuc2hvd1JlY29tZW5kXG5cblx0YXBwLm9uICdzaG93OnZpZGVvJywgLT5cblx0XHRhcHAubmF2aWdhdGUgXCIvdmlkZW9cIlxuXHRcdGRvIGFwaS5zaG93VmlkZW9cblxuXHRhcHAub24gJ3Nob3c6cHJvZmlsZScsICh0YWI9XCJtYWluXCIpLT5cblx0XHRhcHAubmF2aWdhdGUgXCIvcHJvZmlsZS8je3RhYn1cIlxuXHRcdGFwaS5zaG93UHJvZmlsZSBudWxsLHRhYlxuXG5cdGFwcC5vbiAnc2hvdzp1c2VyOnRyYWNrcycsIC0+XG5cdFx0YXBwLm5hdmlnYXRlIFwiL3VzZXIvdHJhY2tzXCJcblx0XHRkbyBhcGkuc2hvd1VzZXJUcmFja3NcblxuXHRhcHAub24gJ3Nob3c6dXNlcjphbGJ1bXMnLCAtPlxuXHRcdGFwcC5uYXZpZ2F0ZSBcIi91c2VyL2FsYnVtc1wiXG5cdFx0ZG8gYXBpLnNob3dVc2VyQWxidW1zXG5cblx0YXBwLm9uICdzaG93OmFsYnVtJywgKGlkLGRhdGEpLT5cblx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKFwiL2dlbmVyYWxWMS9sb2cvcmVsZWFzZVwiLFwiP2lkPSN7aWR9JnJlZmVyZXI9I3thcHAuZ2V0UmVmZXJlcigpfVwiKVxuXHRcdGFwcC5uYXZpZ2F0ZSBcIi9hbGJ1bS8je2lkfVwiXG5cdFx0YXBpLnNob3dBbGJ1bSBudWxsLGlkLGRhdGEsdHJ1ZVxuXG5cdGFwcC5vbiAnc2hvdzpwbGF5bGlzdCcsIChpZCxkYXRhKS0+XG5cdFx0TG9nZ2VyLmNyZWF0ZU5ld0xvZyBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9nZW5lcmFsVjEvbG9nL3B1YmxpY1BsYXlsaXN0XCIsXCI/aWQ9I3tpZH0mcmVmZXJlcj0je2FwcC5nZXRSZWZlcmVyKCl9XCIpXG5cdFx0YXBwLm5hdmlnYXRlIFwiL3BsYXlsaXN0LyN7aWR9XCJcblx0XHRhcGkuc2hvd1BsYXlsaXN0IG51bGwsaWQsdHJ1ZVxuXG5cdGFwcC5vbiAnc2hvdzphcnRpc3QnLCAoaWQpLT5cblx0XHRMb2dnZXIuY3JlYXRlTmV3TG9nIGFwcC5nZXRSZXF1ZXN0VXJsU2lkKFwiL2dlbmVyYWxWMS9sb2cvYXJ0aXN0Q3JlZGl0XCIsXCI/aWQ9I3tpZH0mcmVmZXJlcj0je2FwcC5nZXRSZWZlcmVyKCl9XCIpXG5cdFx0YXBwLnJlcXVlc3QgJ3N0b3A6YXJ0aXN0OnJlcXVlc3QnLCdhcnRpc3QnXG5cdFx0YXBwLm5hdmlnYXRlIFwiL2FydGlzdC8je2lkfVwiXG5cdFx0YXBpLnNob3dBcnRpc3QgbnVsbCxpZCx0cnVlXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFnZXMvaW5kZXguY29mZmVlXG4gKiovIiwiZGVmaW5lIFsnLi92aWV3LmNvZmZlZScsJy4uL3NpbXBsZV92aWV3cy9sb2FkZXInLCcuLi9zaW1wbGVfdmlld3MvdXBkYXRlJ10sIChTaWRlYmFyLExvYWRlcixVcGRhdGUpLT5cblx0cmVxdWlyZSgnLi9zdHlsZS5zdHlsJylcblxuXHRjbGFzcyBjb250cm9sbGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db250cm9sbGVyXG5cdFx0c2hvdzogLT5cblx0XHRcdHNlbGYgPSBAO1xuXHRcdFx0QGxheW91dCA9IG5ldyBTaWRlYmFyLkxheW91dFxuXG5cdFx0XHRAbGlzdGVuVG8gQGxheW91dCwgJ3Nob3cnLCAtPlxuXHRcdFx0XHQkKCcuc2lkZWJhci13cmFwcCcpLmNzcyAnbWluLWhlaWdodCcsICQod2luZG93KS5oZWlnaHQoKVxuXHRcdFx0XHQkKHdpbmRvdykub24gJ3Jlc2l6ZScsIC0+XG5cdFx0XHRcdFx0JCgnLnNpZGViYXItd3JhcHAnKS5jc3MgJ21pbi1oZWlnaHQnLCAkKHdpbmRvdykuaGVpZ2h0KClcblxuXHRcdFx0YXBwLnNpZGViYXIuc2hvdyBAbGF5b3V0XG5cdFx0XHRkbyBAc2hvd0JvZHlcblxuXHRcdFx0dXNlciA9IGFwcC5yZXF1ZXN0ICdnZXQ6dXNlcidcblx0XHRcdGlmIHVzZXIucHJvbWlzZVxuXHRcdFx0XHR1c2VyLmRvbmUgKHVzZXIpLT5cblx0XHRcdFx0XHRzZWxmLnNob3dVc2VyLmFwcGx5IHNlbGYsW3VzZXJdXG5cblx0XHRcdFx0dXNlci5mYWlsIChyZXMpLT5cblx0XHRcdFx0XHRpZiByZXMucmVhZHlTdGF0ZSBpcyAwIGFuZCByZXMuc3RhdHVzVGV4dCBpcyAnYWJvcnQnXG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyAn0L7RgtC80LXQvdCwJ1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGNvbnNvbGUubG9nICfQvtGI0LjQsdC60LAnXG5cdFx0XHRlbHNlXG5cdFx0XHRcdHNlbGYuc2hvd1VzZXIuYXBwbHkgc2VsZixbdXNlcl1cblxuXHRcdHNob3dCb2R5OiAtPlxuXHRcdFx0bWFpbk1lbnVMaXN0ID0gYXBwLnJlcXVlc3QgJ2dldDptZW51J1xuXHRcdFx0dXNlck1lbnVMaXN0ID0gYXBwLnJlcXVlc3QgJ2dldDp0YWJzJ1xuXG5cdFx0XHRAbWFpbk1lbnUgPSBuZXcgU2lkZWJhci5TaWRlTWVudU1haW5JdGVtc1xuXHRcdFx0XHRjb2xsZWN0aW9uOiBtYWluTWVudUxpc3RcblxuXHRcdFx0QHVzZXJNZW51ID0gbmV3IFNpZGViYXIuU2lkZU1lbnVVc2VySXRlbXNcblx0XHRcdFx0Y29sbGVjdGlvbjogdXNlck1lbnVMaXN0XG5cblx0XHRcdEBsYXlvdXQubWFpbk1lbnUuc2hvdyBAbWFpbk1lbnVcblx0XHRcdEBsYXlvdXQudXNlck1lbnUuc2hvdyBAdXNlck1lbnVcblxuXHRcdFx0aWYgIVNpZGViYXIudXJsSXNGaW5kXG5cdFx0XHRcdGlmICF+YXBwLmdldEN1cnJlbnRSb3V0ZSgpLmluZGV4T2YgJ3Byb2ZpbGUnXG5cdFx0XHRcdFx0JCgnLnNpZGViYXItbWVudS1pdGVtLnJlY29tZW5kJykuYWRkQ2xhc3MgJ2FjdGl2ZSdcblxuXHRcdFx0QGxpc3RlblRvIGFwcCwgJ3N3aXRjaDpwYWdlJywgKHBhZ2UpLT5cblx0XHRcdFx0JCgnI3NpZGViYXIgLnNpZGViYXItbWVudS1pdGVtLmFjdGl2ZScpLnJlbW92ZUNsYXNzICdhY3RpdmUnXG5cdFx0XHRcdCQoJyNzaWRlYmFyIC5zaWRlYmFyLW1lbnUtaXRlbS4nK3BhZ2UpLmFkZENsYXNzICdhY3RpdmUnXG5cdFx0XHRcdCMgYXBwLlBhcnRzLlVzZXJOb3RpZmljYXRpb24uQ29udHJvbGxlci5yZW1vdmUoKVxuXHRcdFx0XHQkKCcjc2lkZWJhcicpLnJlbW92ZUNsYXNzICdvcGVuJ1xuXG5cdFx0XHQkKCcjaGVhZGVyIC50aXRsZScpLm1vdXNlZW50ZXIgLT5cblx0XHRcdFx0JCgnI3NpZGViYXInKS5hZGRDbGFzcyAnb3BlbidcblxuXHRcdFx0JCgnLnNpZGViYXItb3ZlcmxheScpLmNsaWNrIC0+XG5cdFx0XHRcdCMgYXBwLlBhcnRzLlVzZXJOb3RpZmljYXRpb24uQ29udHJvbGxlci5yZW1vdmUoKVxuXHRcdFx0XHQkKCcjc2lkZWJhcicpLnJlbW92ZUNsYXNzICdvcGVuJ1xuXG5cdFx0c2hvd1NlYXJjaFJlc3VsdDogKHZhbCktPlxuXHRcdFx0aWYgJC50cmltKHZhbCkgaXMgJydcblx0XHRcdFx0aWYgQGlzU2VhcmNoTGF5b3V0U2hvd25cblx0XHRcdFx0XHRAaXNTZWFyY2hMYXlvdXRTaG93biA9IGZhbHNlXG5cdFx0XHRcdFx0ZG8gQHNlYXJjaExheW91dC5kZXN0cm95XG5cdFx0XHRcdFx0JCgnLnNpZGViYXItY29udGVudC1zY3JvbGwnKS5jdXN0b21TY3JvbGxiYXIoJ3Jlc2l6ZScsdHJ1ZSlcblxuXHRcdFx0XHRyZXR1cm5cblx0XHRcdGlmICFAaXNTZWFyY2hMYXlvdXRTaG93blxuXHRcdFx0XHRAc2VhcmNoTGF5b3V0ID0gbmV3IFNpZGViYXIuU2VhcmNoTGF5b3V0XG5cdFx0XHRcdEBsYXlvdXQuc2VhcmNoUmVzdWx0LnNob3cgQHNlYXJjaExheW91dFxuXHRcdFx0XHRAbGlzdGVuVG8gQHNlYXJjaExheW91dCwgJ2Rlc3Ryb3knLCAtPlxuXHRcdFx0XHRcdEBpc1NlYXJjaExheW91dFNob3duID0gZmFsc2VcblxuXHRcdFx0QGlzU2VhcmNoTGF5b3V0U2hvd24gPSB0cnVlXG5cdFx0XHRAU2VhcmNoVHJhY2tzIHZhbFxuXHRcdFx0QFNlYXJjaEFsYnVtcyB2YWxcblx0XHRcdEBTZWFyY2hBcnRpc3RzIHZhbFxuXG5cdFx0U2VhcmNoVHJhY2tzOiAodmFsKS0+XG5cdFx0XHRmZXRjaCA9IGFwcC5yZXF1ZXN0ICdzZWFyY2g6dHJhY2tzJyxcblx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdFx0aXRlbXNfb25fcGFnZTogM1xuXG5cdFx0XHRsb2FkZXIgPSBuZXcgTG9hZGVyLlZpZXdcblx0XHRcdFx0c3R5bGVzOlxuXHRcdFx0XHRcdHBvc2l0aW9uOiAnYWJzb2x1dGUnXG5cdFx0XHRcdFx0dG9wOiAnMzUlJ1xuXHRcdFx0XHRcdGxlZnQ6ICc0NSUnXG5cblx0XHRcdEBzZWFyY2hMYXlvdXQudHJhY2tzLnNob3cgbG9hZGVyXG5cblx0XHRcdGZldGNoLmRvbmUgKGNvbGxlY3Rpb24pPT5cblx0XHRcdFx0dmlldyA9IG5ldyBTaWRlYmFyLlNlYXJjaFRyYWNrc0xheW91dFxuXHRcdFx0XHRcdGNvbGxlY3Rpb246IGNvbGxlY3Rpb25cblxuXHRcdFx0XHRAc2VhcmNoTGF5b3V0LnRyYWNrcy5zaG93IHZpZXdcblxuXHRcdFx0ZmV0Y2guZmFpbCAocmVzKS0+XG5cblxuXHRcdFNlYXJjaEFydGlzdHM6ICh2YWwpLT5cblx0XHRcdGZldGNoID0gYXBwLnJlcXVlc3QgJ3NlYXJjaDphcnRpc3RzJyxcblx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdFx0aXRlbXNfb25fcGFnZTogM1xuXG5cdFx0XHRsb2FkZXIgPSBuZXcgTG9hZGVyLlZpZXdcblx0XHRcdFx0c3R5bGVzOlxuXHRcdFx0XHRcdHBvc2l0aW9uOiAnYWJzb2x1dGUnXG5cdFx0XHRcdFx0dG9wOiAnMzUlJ1xuXHRcdFx0XHRcdGxlZnQ6ICc0NSUnXG5cblx0XHRcdEBzZWFyY2hMYXlvdXQuYXJ0aXN0cy5zaG93IGxvYWRlclxuXG5cdFx0XHRmZXRjaC5kb25lIChjb2xsZWN0aW9uKT0+XG5cdFx0XHRcdHZpZXcgPSBuZXcgU2lkZWJhci5TZWFyY2hBcnRpc3RzTGF5b3V0XG5cdFx0XHRcdFx0Y29sbGVjdGlvbjogY29sbGVjdGlvblxuXG5cdFx0XHRcdEBzZWFyY2hMYXlvdXQuYXJ0aXN0cy5zaG93IHZpZXdcblxuXHRcdFx0ZmV0Y2guZmFpbCAocmVzKS0+XG5cblx0XHRTZWFyY2hBbGJ1bXM6ICh2YWwpLT5cblx0XHRcdGZldGNoID0gYXBwLnJlcXVlc3QgJ3NlYXJjaDphbGJ1bXMnLFxuXHRcdFx0XHRkYXRhOlxuXHRcdFx0XHRcdHE6IHZhbFxuXHRcdFx0XHRcdHBhZ2U6IDFcblx0XHRcdFx0XHRpdGVtc19vbl9wYWdlOiAzXG5cblxuXHRcdFx0bG9hZGVyID0gbmV3IExvYWRlci5WaWV3XG5cdFx0XHRcdHN0eWxlczpcblx0XHRcdFx0XHRwb3NpdGlvbjogJ2Fic29sdXRlJ1xuXHRcdFx0XHRcdHRvcDogJzM1JSdcblx0XHRcdFx0XHRsZWZ0OiAnNDUlJ1xuXG5cdFx0XHRAc2VhcmNoTGF5b3V0LmFsYnVtcy5zaG93IGxvYWRlclxuXG5cdFx0XHRmZXRjaC5kb25lIChjb2xsZWN0aW9uKT0+XG5cdFx0XHRcdHZpZXcgPSBuZXcgU2lkZWJhci5TZWFyY2hBbGJ1bXNMYXlvdXRcblx0XHRcdFx0XHRjb2xsZWN0aW9uOiBjb2xsZWN0aW9uXG5cblx0XHRcdFx0QHNlYXJjaExheW91dC5hbGJ1bXMuc2hvdyB2aWV3XG5cblx0XHRcdGZldGNoLmZhaWwgKHJlcyktPlxuXG5cblx0XHRzaG93U2VhcmNoVHJhY2tzUGFnZTogKHZhbCktPlxuXHRcdFx0bGlzdCA9IGFwcC5yZXF1ZXN0ICdzZWFyY2g6cGFnZScsJ3RyYWNrJyxcblx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdFx0aXRlbXNfb25fcGFnZTogNjBcblxuXHRcdFx0bG9hZGVyID0gbmV3IExvYWRlci5WaWV3XG5cdFx0XHRcdHN0eWxlczpcblx0XHRcdFx0XHQndG9wJzogJCh3aW5kb3cpLmhlaWdodCgpLzJcblxuXHRcdFx0YXBwLmNvbnRlbnQuc2hvdyBsb2FkZXJcblxuXHRcdFx0bGlzdC5kb25lIChzZWFyY2hDb2xsZWN0aW9uKS0+XG5cblx0XHRcdFx0JCh3aW5kb3cpLm9mZiAnLnNlYXJjaHBhZ2UnXG5cdFx0XHRcdGZldGNoTW9yZSA9IC0+XG5cdFx0XHRcdFx0JCh3aW5kb3cpLm9mZiAnLnNlYXJjaHBhZ2UnXG5cdFx0XHRcdFx0cGFnZSA9IE1hdGguY2VpbChzZWFyY2hDb2xsZWN0aW9uLmxlbmd0aC82MCkrMTtcblxuXHRcdFx0XHRcdHRyYWNrRmV0Y2ggPSBzZWFyY2hDb2xsZWN0aW9uLmZldGNoXG5cdFx0XHRcdFx0XHRkYXRhOlxuXHRcdFx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRcdFx0cGFnZTogcGFnZVxuXHRcdFx0XHRcdFx0XHRpdGVtc19vbl9wYWdlOiA5MFxuXG5cdFx0XHRcdFx0XHRyZW1vdmU6IGZhbHNlXG5cblx0XHRcdFx0XHR0cmFja0ZldGNoLmRvbmUgKG5ld2NvbGxlY3Rpb24pLT5cblxuXHRcdFx0XHRcdFx0ZG8gYXBwLmNoZWNrQWN0aXZlVHJhY2tcblx0XHRcdFx0XHRcdGlmIG5ld2NvbGxlY3Rpb24uc2VhcmNoZWQubGVuZ3RoXG5cdFx0XHRcdFx0XHRcdHdpbkhlaWdodCA9ICQoJ2JvZHknKS5oZWlnaHQoKTtcblx0XHRcdFx0XHRcdFx0JCh3aW5kb3cpLm9uICdzY3JvbGwuc2VhcmNocGFnZScsIC0+XG5cdFx0XHRcdFx0XHRcdFx0c2Nyb2xsID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpKyQod2luZG93KS5oZWlnaHQoKSo1XG5cdFx0XHRcdFx0XHRcdFx0aWYgd2luSGVpZ2h0IDwgc2Nyb2xsXG5cdFx0XHRcdFx0XHRcdFx0XHRmZXRjaE1vcmUoKTtcblxuXHRcdFx0XHRcdHRyYWNrRmV0Y2guZmFpbCAocmVzKS0+XG5cdFx0XHRcdFx0XHRpZiByZXMuc3RhdHVzIGlzIDQyMlxuXHRcdFx0XHRcdFx0XHRjb250YWN0cy5hZGQocmVzLnJlc3BvbnNlSlNPTik7XG5cdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKCfQn9GA0L7QuNC30L7RiNC70LAg0L7RiNC40LHQutCwJyk7XG5cblx0XHRcdFx0bW9kZWwgPSBuZXcgQmFja2JvbmUuTW9kZWxcblx0XHRcdFx0XHR0eXBlOiAndHJrJ1xuXHRcdFx0XHRcdHRpdGxlOiB2YWxcblxuXHRcdFx0XHRzZWFyY2hWaWV3ID0gbmV3IFNpZGViYXIuU2VhcmNoUGFnZVRyYWNrc1xuXHRcdFx0XHRcdG1vZGVsOiBtb2RlbFxuXHRcdFx0XHRcdGNvbGxlY3Rpb246IHNlYXJjaENvbGxlY3Rpb25cblxuXHRcdFx0XHRhcHAuY29udGVudC5zaG93IHNlYXJjaFZpZXdcblx0XHRcdFx0ZG8gYXBwLmNoZWNrQWN0aXZlVHJhY2tcblxuXHRcdFx0XHR3aW5IZWlnaHQgPSAkKCdib2R5JykuaGVpZ2h0KCk7XG5cdFx0XHRcdCQod2luZG93KS5vbiAnc2Nyb2xsLnNlYXJjaHBhZ2UnLCAtPlxuXHRcdFx0XHRcdHNjcm9sbCA9ICQod2luZG93KS5zY3JvbGxUb3AoKSskKHdpbmRvdykuaGVpZ2h0KCkqNVxuXHRcdFx0XHRcdGlmIHdpbkhlaWdodCA8IHNjcm9sbFxuXHRcdFx0XHRcdFx0ZmV0Y2hNb3JlKCk7XG5cblx0XHRzaG93U2VhcmNoQWxidW1zUGFnZTogKHZhbCktPlxuXHRcdFx0bGlzdCA9IGFwcC5yZXF1ZXN0ICdzZWFyY2g6cGFnZScsJ2FsYnVtJyxcblx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdFx0aXRlbXNfb25fcGFnZTogMzBcblxuXHRcdFx0bG9hZGVyID0gbmV3IExvYWRlci5WaWV3XG5cdFx0XHRcdHN0eWxlczpcblx0XHRcdFx0XHQndG9wJzogJCh3aW5kb3cpLmhlaWdodCgpLzJcblxuXHRcdFx0YXBwLmNvbnRlbnQuc2hvdyBsb2FkZXJcblxuXHRcdFx0bGlzdC5kb25lIChzZWFyY2hDb2xsZWN0aW9uKS0+XG5cblx0XHRcdFx0JCh3aW5kb3cpLm9mZiAnLnNlYXJjaHBhZ2UnXG5cdFx0XHRcdGZldGNoTW9yZSA9IC0+XG5cdFx0XHRcdFx0JCh3aW5kb3cpLm9mZiAnLnNlYXJjaHBhZ2UnXG5cdFx0XHRcdFx0cGFnZSA9IE1hdGguY2VpbChzZWFyY2hDb2xsZWN0aW9uLmxlbmd0aC8zMCkrMTtcblxuXHRcdFx0XHRcdHRyYWNrRmV0Y2ggPSBzZWFyY2hDb2xsZWN0aW9uLmZldGNoXG5cdFx0XHRcdFx0XHRkYXRhOlxuXHRcdFx0XHRcdFx0XHRxOiB2YWxcblx0XHRcdFx0XHRcdFx0cGFnZTogcGFnZVxuXHRcdFx0XHRcdFx0XHRpdGVtc19vbl9wYWdlOiAzMFxuXG5cdFx0XHRcdFx0XHRyZW1vdmU6IGZhbHNlXG5cblx0XHRcdFx0XHR0cmFja0ZldGNoLmRvbmUgKG5ld2NvbGxlY3Rpb24pLT5cblxuXHRcdFx0XHRcdFx0aWYgbmV3Y29sbGVjdGlvbi5zZWFyY2hlZC5sZW5ndGhcblx0XHRcdFx0XHRcdFx0d2luSGVpZ2h0ID0gJCgnYm9keScpLmhlaWdodCgpO1xuXHRcdFx0XHRcdFx0XHQkKHdpbmRvdykub24gJ3Njcm9sbC5zZWFyY2hwYWdlJywgLT5cblx0XHRcdFx0XHRcdFx0XHRzY3JvbGwgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkrJCh3aW5kb3cpLmhlaWdodCgpKjVcblx0XHRcdFx0XHRcdFx0XHRpZiB3aW5IZWlnaHQgPCBzY3JvbGxcblx0XHRcdFx0XHRcdFx0XHRcdGZldGNoTW9yZSgpO1xuXG5cdFx0XHRcdFx0dHJhY2tGZXRjaC5mYWlsIChyZXMpLT5cblx0XHRcdFx0XHRcdGlmIHJlcy5zdGF0dXMgaXMgNDIyXG5cdFx0XHRcdFx0XHRcdGNvbnRhY3RzLmFkZChyZXMucmVzcG9uc2VKU09OKTtcblx0XHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdFx0Y29uc29sZS5sb2coJ9Cf0YDQvtC40LfQvtGI0LvQsCDQvtGI0LjQsdC60LAnKTtcblxuXHRcdFx0XHRtb2RlbCA9IG5ldyBCYWNrYm9uZS5Nb2RlbFxuXHRcdFx0XHRcdHR5cGU6ICdhbGInXG5cdFx0XHRcdFx0dGl0bGU6IHZhbFxuXG5cdFx0XHRcdHNlYXJjaFZpZXcgPSBuZXcgU2lkZWJhci5TZWFyY2hQYWdlQWxidW1zXG5cdFx0XHRcdFx0bW9kZWw6IG1vZGVsXG5cdFx0XHRcdFx0Y29sbGVjdGlvbjogc2VhcmNoQ29sbGVjdGlvblxuXG5cdFx0XHRcdGFwcC5jb250ZW50LnNob3cgc2VhcmNoVmlld1xuXHRcdFx0XHRkbyBhcHAuY2hlY2tBY3RpdmVUcmFja1xuXG5cdFx0XHRcdHdpbkhlaWdodCA9ICQoJ2JvZHknKS5oZWlnaHQoKTtcblx0XHRcdFx0JCh3aW5kb3cpLm9uICdzY3JvbGwuc2VhcmNocGFnZScsIC0+XG5cdFx0XHRcdFx0c2Nyb2xsID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpKyQod2luZG93KS5oZWlnaHQoKSo1XG5cdFx0XHRcdFx0aWYgd2luSGVpZ2h0IDwgc2Nyb2xsXG5cdFx0XHRcdFx0XHRmZXRjaE1vcmUoKTtcblxuXHRcdHNob3dTZWFyY2hBcnRpc3RzUGFnZTogKHZhbCktPlxuXHRcdFx0bGlzdCA9IGFwcC5yZXF1ZXN0ICdzZWFyY2g6cGFnZScsJ2FydGlzdCcsXG5cdFx0XHRcdGRhdGE6XG5cdFx0XHRcdFx0cTogdmFsXG5cdFx0XHRcdFx0cGFnZTogMVxuXHRcdFx0XHRcdGl0ZW1zX29uX3BhZ2U6IDMwXG5cblx0XHRcdGxvYWRlciA9IG5ldyBMb2FkZXIuVmlld1xuXHRcdFx0XHRzdHlsZXM6XG5cdFx0XHRcdFx0J3RvcCc6ICQod2luZG93KS5oZWlnaHQoKS8yXG5cblx0XHRcdGFwcC5jb250ZW50LnNob3cgbG9hZGVyXG5cblx0XHRcdGxpc3QuZG9uZSAoc2VhcmNoQ29sbGVjdGlvbiktPlxuXG5cdFx0XHRcdCQod2luZG93KS5vZmYgJy5zZWFyY2hwYWdlJ1xuXHRcdFx0XHRmZXRjaE1vcmUgPSAtPlxuXHRcdFx0XHRcdCQod2luZG93KS5vZmYgJy5zZWFyY2hwYWdlJ1xuXHRcdFx0XHRcdHBhZ2UgPSBNYXRoLmNlaWwoc2VhcmNoQ29sbGVjdGlvbi5sZW5ndGgvMzApKzE7XG5cblx0XHRcdFx0XHR0cmFja0ZldGNoID0gc2VhcmNoQ29sbGVjdGlvbi5mZXRjaFxuXHRcdFx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRcdFx0cTogdmFsXG5cdFx0XHRcdFx0XHRcdHBhZ2U6IHBhZ2Vcblx0XHRcdFx0XHRcdFx0aXRlbXNfb25fcGFnZTogMzBcblxuXHRcdFx0XHRcdFx0cmVtb3ZlOiBmYWxzZVxuXG5cdFx0XHRcdFx0dHJhY2tGZXRjaC5kb25lIChuZXdjb2xsZWN0aW9uKS0+XG5cblx0XHRcdFx0XHRcdGlmIG5ld2NvbGxlY3Rpb24uc2VhcmNoZWQubGVuZ3RoXG5cdFx0XHRcdFx0XHRcdHdpbkhlaWdodCA9ICQoJ2JvZHknKS5oZWlnaHQoKTtcblx0XHRcdFx0XHRcdFx0JCh3aW5kb3cpLm9uICdzY3JvbGwuc2VhcmNocGFnZScsIC0+XG5cdFx0XHRcdFx0XHRcdFx0c2Nyb2xsID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpKyQod2luZG93KS5oZWlnaHQoKSo1XG5cdFx0XHRcdFx0XHRcdFx0aWYgd2luSGVpZ2h0IDwgc2Nyb2xsXG5cdFx0XHRcdFx0XHRcdFx0XHRmZXRjaE1vcmUoKTtcblxuXHRcdFx0XHRcdHRyYWNrRmV0Y2guZmFpbCAocmVzKS0+XG5cdFx0XHRcdFx0XHRpZiByZXMuc3RhdHVzIGlzIDQyMlxuXHRcdFx0XHRcdFx0XHRjb250YWN0cy5hZGQocmVzLnJlc3BvbnNlSlNPTik7XG5cdFx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRcdGNvbnNvbGUubG9nKCfQn9GA0L7QuNC30L7RiNC70LAg0L7RiNC40LHQutCwJyk7XG5cblx0XHRcdFx0bW9kZWwgPSBuZXcgQmFja2JvbmUuTW9kZWxcblx0XHRcdFx0XHR0eXBlOiAnYXJ0J1xuXHRcdFx0XHRcdHRpdGxlOiB2YWxcblxuXHRcdFx0XHRzZWFyY2hWaWV3ID0gbmV3IFNpZGViYXIuU2VhcmNoUGFnZUFydGlzdHNcblx0XHRcdFx0XHRtb2RlbDogbW9kZWxcblx0XHRcdFx0XHRjb2xsZWN0aW9uOiBzZWFyY2hDb2xsZWN0aW9uXG5cblx0XHRcdFx0YXBwLmNvbnRlbnQuc2hvdyBzZWFyY2hWaWV3XG5cdFx0XHRcdGRvIGFwcC5jaGVja0FjdGl2ZVRyYWNrXG5cblx0XHRcdFx0d2luSGVpZ2h0ID0gJCgnYm9keScpLmhlaWdodCgpO1xuXHRcdFx0XHQkKHdpbmRvdykub24gJ3Njcm9sbC5zZWFyY2hwYWdlJywgLT5cblx0XHRcdFx0XHRzY3JvbGwgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkrJCh3aW5kb3cpLmhlaWdodCgpKjVcblx0XHRcdFx0XHRpZiB3aW5IZWlnaHQgPCBzY3JvbGxcblx0XHRcdFx0XHRcdGZldGNoTW9yZSgpO1xuXG5cblx0XHRjbG9zZVNlYXJjaFBhZ2U6IC0+XG5cdFx0XHRpZiBhcHAub3BlblNlYXJjaFBhZ2VGcm9tU2l0ZVxuXHRcdFx0XHRoaXN0b3J5LmJhY2soKVxuXG5cdFx0XHRlbHNlXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzaG93OnJlY29tZW5kJ1xuXG5cblxuXG5cdFNpZGViYXIuQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cblx0Xy5leHRlbmQgU2lkZWJhci5Db250cm9sbGVyLFxuXHRcdHNob3dVc2VyOiAodXNlciktPlxuXHRcdFx0dXNlciA9IG5ldyBTaWRlYmFyLlVzZXJcblx0XHRcdFx0bW9kZWw6IHVzZXJcblxuXHRcdFx0QGxheW91dC51c2VyLnNob3cgdXNlclxuXG5cdHJldHVybiBTaWRlYmFyXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvc2lkZWJhci9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuLi8uLi9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3RyYWNrJywnLi4vLi4vY29uZmlnL2N1c3RvbV9lbGVtZW50cy9hbGJ1bScsJy4uLy4uL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYXJ0aXN0LycsJy4uLy4uL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvcGxheWxpc3QnLCcuLi9wbGF5ZXInXSAsIChUcmFjayxBbGJ1bSxBcnRpc3QsUGxheWxpc3QsUGxheWVyKS0+XG5cdFNpZGViYXIgPSB7fVxuXG5cdGNsYXNzIFNpZGViYXIuTGF5b3V0IGV4dGVuZHMgTWFyaW9uZXR0ZS5MYXlvdXRWaWV3XG5cdFx0dGVtcGxhdGU6IC0+XG5cdFx0XHRyZXR1cm4gcmVxdWlyZSgnLi90ZW1wbGF5dHMvbGF5b3V0LXRlbXBsYXRlLmphZGUnKSgpXG5cdFx0Y2xhc3NOYW1lOiAnc2lkZWJhci13cmFwcCdcblx0XHRyZWdpb25zOlxuXHRcdFx0J3VzZXInOiAnLnByb2ZpbGUnXG5cdFx0XHQnbWFpbk1lbnUnOiAnLm1haW4tbmF2J1xuXHRcdFx0J3VzZXJNZW51JzogJy51c2VyLW5hdidcblx0XHRcdCdzZWFyY2hSZXN1bHQnOiAnLnNpZGViYXItc2VhcmNoLWNvbnRhaW5lcidcblxuXHRcdG9uU2hvdzogLT5cblx0XHRcdHNpZGViYXJTY3JvbGxDb250ZW50ID0gJCgnLnNpZGViYXItY29udGVudC1zY3JvbGwnKVxuXHRcdFx0c2lkZWJhclNjcm9sbENvbnRlbnQuaGVpZ2h0ICQod2luZG93KS5oZWlnaHQoKS0xMTZcblx0XHRcdCQod2luZG93KS5vbiAncmVzaXplLnVzZXJUcmFja3MnLCAtPlxuXHRcdFx0XHRzaWRlYmFyU2Nyb2xsQ29udGVudC5oZWlnaHQgJCh3aW5kb3cpLmhlaWdodCgpLTExNlxuXHRcdFx0c2lkZWJhclNjcm9sbENvbnRlbnQuY3VzdG9tU2Nyb2xsYmFyKHtcblx0XHRcdFx0dXBkYXRlT25XaW5kb3dSZXNpemU6IHRydWVcblx0XHRcdH0pO1xuXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2tleXVwIC5zZWFyY2ggaW5wdXQnOiAtPlxuXHRcdFx0XHR2YWwgPSBAJCgnLnNlYXJjaCBpbnB1dCcpLnZhbCgpXG5cblx0XHRcdFx0aWYgJC50cmltKHZhbCkgaXMgJydcblx0XHRcdFx0XHQkKCcuc2lkZWJhci1ib2R5IC5zZWFyY2gnKS5yZW1vdmVDbGFzcyAnYWN0aXZlJ1xuXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHQkKCcuc2lkZWJhci1ib2R5IC5zZWFyY2gnKS5hZGRDbGFzcyAnYWN0aXZlJ1xuXG5cdFx0XHRcdGlmIEBzZWFyY2hUaW1lclxuXHRcdFx0XHRcdGNsZWFyVGltZW91dCBAc2VhcmNoVGltZXJcblxuXHRcdFx0XHRAc2VhcmNoVGltZXIgPSBzZXRUaW1lb3V0ID0+XG5cdFx0XHRcdFx0U2lkZWJhci5Db250cm9sbGVyLnNob3dTZWFyY2hSZXN1bHQgdmFsXG5cdFx0XHRcdCwzMDBcblxuXHRcdFx0J2NsaWNrIC5jbG9zZSc6IC0+XG5cdFx0XHRcdEAkKCcuc2VhcmNoIGlucHV0JykudmFsICcnXG5cdFx0XHRcdFx0LmtleXVwKClcblxuXHRjbGFzcyBTaWRlYmFyLlVzZXIgZXh0ZW5kcyBNYXJpb25ldHRlLkxheW91dFZpZXdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLXVzZXItdHBsJ1xuXHRcdHJlZ2lvbnM6XG5cdFx0XHQnbm90aWZ5JzogJy5ub3RpZnktcmVnaW9uJ1xuXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrIC5leGl0JzogKGUpLT5cblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKVxuXHRcdFx0XHQkLmdldCAnL2V4aXQnLCAtPlxuXHRcdFx0XHRcdGxvY2F0aW9uLmhyZWYgPSAnJ1xuXG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzpwcm9maWxlJ1xuXG5cdFx0bW9kZWxFdmVudHM6XG5cdFx0XHQnY2hhbmdlJzogLT5cblx0XHRcdFx0ZG8gQHJlbmRlclxuXG5cdGNsYXNzIFNpZGViYXIuU2lkZU1lbnVNYWluSXRlbSBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLW1lbnUtaXRlbS10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnc2lkZWJhci1tZW51LWl0ZW0nXG5cdFx0dGFnTmFtZTogJ2xpJ1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayc6IC0+XG5cdFx0XHRcdGFwcC50cmlnZ2VyIEBtb2RlbC5nZXQgJ3RyaWdnZXInXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0b25SZW5kZXI6IC0+XG5cdFx0XHRAJGVsLmFkZENsYXNzIEBtb2RlbC5nZXQgJ2NsYXNzJ1xuXHRcdFx0aHJlZiA9IGFwcC5nZXRDdXJyZW50Um91dGUoKS5zcGxpdCgnLycpWzFdIG9yICcnXG5cdFx0XHRyb3V0ZSA9IEBtb2RlbC5nZXQgJ2hyZWYnXG5cblx0XHRcdGlmIHJvdXRlIGlzIGhyZWZcblx0XHRcdFx0QCRlbC5hZGRDbGFzcyAnYWN0aXZlJ1xuXHRcdFx0XHRTaWRlYmFyLnVybElzRmluZCA9IHRydWVcblxuXG5cblx0Y2xhc3MgU2lkZWJhci5TaWRlTWVudU1haW5JdGVtcyBleHRlbmRzIE1hcmlvbmV0dGUuQ29sbGVjdGlvblZpZXdcblx0XHRjaGlsZFZpZXc6IFNpZGViYXIuU2lkZU1lbnVNYWluSXRlbVxuXHRcdHRhZ05hbWU6ICd1bCdcblxuXHRjbGFzcyBTaWRlYmFyLlNpZGVNZW51VXNlckl0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6ICcjc2lkZWJhci1tZW51LWl0ZW0tdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3NpZGViYXItbWVudS1pdGVtJ1xuXHRcdHRhZ05hbWU6ICdsaSdcblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRhcHAudHJpZ2dlciBAbW9kZWwuZ2V0ICd0cmlnZ2VyJ1xuXHRcdFx0XHRyZXR1cm4gZmFsc2Vcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEAkZWwuYWRkQ2xhc3MgQG1vZGVsLmdldCAnY2xhc3MnXG5cdFx0XHRjdXJyZW50Um91dGUgPSBhcHAuZ2V0Q3VycmVudFJvdXRlKCkuc3BsaXQoJy8nKVxuXHRcdFx0aHJlZiA9IGN1cnJlbnRSb3V0ZVsxXSsnLycrY3VycmVudFJvdXRlWzJdXG5cdFx0XHRyb3V0ZSA9IEBtb2RlbC5nZXQgJ2hyZWYnXG5cblx0XHRcdGlmIHJvdXRlIGlzIGhyZWZcblx0XHRcdFx0QCRlbC5hZGRDbGFzcyAnYWN0aXZlJ1xuXHRcdFx0XHRTaWRlYmFyLnVybElzRmluZCA9IHRydWVcblxuXG5cdGNsYXNzIFNpZGViYXIuU2lkZU1lbnVVc2VySXRlbXMgZXh0ZW5kcyBNYXJpb25ldHRlLkNvbXBvc2l0ZVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLW1lbnUtdXNlci10cGwnXG5cdFx0Y2hpbGRWaWV3OiBTaWRlYmFyLlNpZGVNZW51VXNlckl0ZW1cblx0XHRjaGlsZFZpZXdDb250YWluZXI6ICcudXNlci1tZW51J1xuXG5cdGNsYXNzIFNpZGViYXIuU2VhcmNoTGF5b3V0IGV4dGVuZHMgTWFyaW9uZXR0ZS5MYXlvdXRWaWV3XG5cdFx0dGVtcGxhdGU6ICcjc2lkZWJhci1zZWFyY2gtbGF5b3V0LXRwbCdcblx0XHRjbGFzc05hbWU6ICdzY3JvbGxhYmxlJ1xuXHRcdHJlZ2lvbnM6XG5cdFx0XHR0cmFja3M6ICcudHJhY2tzLXdyYXBwJ1xuXHRcdFx0YWxidW1zOiAnLmFsYnVtcy13cmFwcCdcblx0XHRcdGFydGlzdHM6ICcuYXJ0aXN0cy13cmFwcCdcblxuXHRcdG9uU2hvdzogLT5cblx0XHRcdCQoJy5zaWRlYmFyLWNvbnRlbnQtd3JhcHAnKS5oaWRlKClcblx0XHRcdCQoJy5zaWRlYmFyLWNvbnRlbnQtc2Nyb2xsJykuY3VzdG9tU2Nyb2xsYmFyKCdyZXNpemUnLHRydWUpXG5cblx0XHRvbkRlc3Ryb3k6IC0+XG5cdFx0XHQkKCcuc2lkZWJhci1jb250ZW50LXdyYXBwJykuc2hvdygpXG5cblx0Y2xhc3MgU2lkZWJhci5TZWFyY2hUcmFjayBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLXNlYXJjaC10cmFjay10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnc2VhcmNoLXRyYWNrJ1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayc6IC0+XG5cdFx0XHRcdFBsYXllci5Db250cm9sbGVyLnBsYXlUcmFjayBAbW9kZWwuZ2V0KCdpZCcpLEBtb2RlbC50b0pTT04oKVxuXG5cdGNsYXNzIFNpZGViYXIuU2VhcmNoQXJ0aXN0IGV4dGVuZHMgTWFyaW9uZXR0ZS5JdGVtVmlld1xuXHRcdHRlbXBsYXRlOiAnI3NpZGViYXItc2VhcmNoLWFydGlzdC10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnc2VhcmNoLWFydGlzdCdcblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphcnRpc3QnLCBAbW9kZWwuZ2V0ICdhcnRpc3RpZCdcblx0XHRcdFx0JCgnI3NpZGViYXInKS5yZW1vdmVDbGFzcyAnb3BlbidcblxuXHRjbGFzcyBTaWRlYmFyLlNlYXJjaEFsYnVtIGV4dGVuZHMgTWFyaW9uZXR0ZS5JdGVtVmlld1xuXHRcdHRlbXBsYXRlOiAnI3NpZGViYXItc2VhcmNoLWFsYnVtLXRwbCdcblx0XHRjbGFzc05hbWU6ICdzZWFyY2gtYWxidW0nXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YWxidW0nLCBAbW9kZWwuZ2V0KCdpZCcpLCBAbW9kZWxcblx0XHRcdFx0JCgnI3NpZGViYXInKS5yZW1vdmVDbGFzcyAnb3BlbidcblxuXHRjbGFzcyBTaWRlYmFyLlNlYXJjaFRyYWNrc0VtcHR5IGV4dGVuZHMgTWFyaW9uZXR0ZS5JdGVtVmlld1xuXHRcdHRlbXBsYXRlOiAnI3NpZGViYXItc2VhcmNoLXRyYWNrcy1lbXB0eS10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnc2VhcmNoLWVtcHR5LW1lc3NhZ2UnXG5cblx0Y2xhc3MgU2lkZWJhci5TZWFyY2hBcnRpc3RzRW1wdHkgZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6ICcjc2lkZWJhci1zZWFyY2gtYXJ0aXN0cy1lbXB0eS10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnc2VhcmNoLWVtcHR5LW1lc3NhZ2UnXG5cblx0Y2xhc3MgU2lkZWJhci5TZWFyY2hBbGJ1bXNFbXB0eSBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLXNlYXJjaC1hcnRpc3RzLWVtcHR5LXRwbCdcblx0XHRjbGFzc05hbWU6ICdzZWFyY2gtZW1wdHktbWVzc2FnZSdcblxuXHRjbGFzcyBTaWRlYmFyLlNlYXJjaFRyYWNrc0xheW91dCBleHRlbmRzIE1hcmlvbmV0dGUuQ29tcG9zaXRlVmlld1xuXHRcdGNoaWxkVmlldzogU2lkZWJhci5TZWFyY2hUcmFja1xuXHRcdGNoaWxkVmlld0NvbnRhaW5lcjogJy5zZWFyY2gtYmxvY2stY29udGVudCdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLXNlYXJjaC1yZXN1bHQtbGF5b3V0LXRwbCdcblx0XHRjbGFzc05hbWU6ICdzZWFyY2gtcmVzdWx0J1xuXHRcdGVtcHR5VmlldzogU2lkZWJhci5TZWFyY2hUcmFja3NFbXB0eVxuXHRcdG9uUmVuZGVyOiAtPlxuXHRcdFx0aWYgQGNvbGxlY3Rpb24ubGVuZ3RoIGlzIDBcblx0XHRcdFx0QCQoJy5zaG93LW1vcmUnKS5oaWRlKClcblx0XHRcdGVsc2Vcblx0XHRcdFx0QCQoJy5zaG93LW1vcmUnKS5zaG93KClcblxuXHRcdGV2ZW50czpcblx0XHQgJ2NsaWNrIC5zaG93LW1vcmUnOiAtPlxuXHRcdFx0IHZhbCA9ICQoJy5zaWRlYmFyLWJvZHkgLnNlYXJjaCBpbnB1dCcpLnZhbCgpXG5cdFx0XHQgYXBwLm5hdmlnYXRlICcvc2VhcmNoL3RyYWNrcy8/cT0nK3ZhbFxuXHRcdFx0IFNpZGViYXIuQ29udHJvbGxlci5zaG93U2VhcmNoVHJhY2tzUGFnZSB2YWxcblx0XHRcdCAkKCcjc2lkZWJhcicpLnJlbW92ZUNsYXNzICdvcGVuJ1xuXHRcdFx0IGFwcC5vcGVuU2VhcmNoUGFnZUZyb21TaXRlID0gdHJ1ZVxuXG5cblx0Y2xhc3MgU2lkZWJhci5TZWFyY2hBcnRpc3RzTGF5b3V0IGV4dGVuZHMgTWFyaW9uZXR0ZS5Db21wb3NpdGVWaWV3XG5cdFx0Y2hpbGRWaWV3OiBTaWRlYmFyLlNlYXJjaEFydGlzdFxuXHRcdGNoaWxkVmlld0NvbnRhaW5lcjogJy5zZWFyY2gtYmxvY2stY29udGVudCdcblx0XHR0ZW1wbGF0ZTogJyNzaWRlYmFyLXNlYXJjaC1yZXN1bHQtbGF5b3V0LXRwbCdcblx0XHRjbGFzc05hbWU6ICdzZWFyY2gtcmVzdWx0J1xuXHRcdGVtcHR5VmlldzogU2lkZWJhci5TZWFyY2hBcnRpc3RzRW1wdHlcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdGlmIEBjb2xsZWN0aW9uLmxlbmd0aCBpcyAwXG5cdFx0XHRcdEAkKCcuc2hvdy1tb3JlJykuaGlkZSgpXG5cdFx0XHRlbHNlXG5cdFx0XHRcdEAkKCcuc2hvdy1tb3JlJykuc2hvdygpXG5cblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2sgLnNob3ctbW9yZSc6IC0+XG5cdFx0XHRcdHZhbCA9ICQoJy5zaWRlYmFyLWJvZHkgLnNlYXJjaCBpbnB1dCcpLnZhbCgpXG5cdFx0XHRcdGFwcC5uYXZpZ2F0ZSAnL3NlYXJjaC9hcnRpc3RzLz9xPScrdmFsXG5cdFx0XHRcdFNpZGViYXIuQ29udHJvbGxlci5zaG93U2VhcmNoQXJ0aXN0c1BhZ2UgdmFsXG5cdFx0XHRcdCQoJyNzaWRlYmFyJykucmVtb3ZlQ2xhc3MgJ29wZW4nXG5cdFx0XHRcdGFwcC5vcGVuU2VhcmNoUGFnZUZyb21TaXRlID0gdHJ1ZVxuXG5cdGNsYXNzIFNpZGViYXIuU2VhcmNoQWxidW1zTGF5b3V0IGV4dGVuZHMgTWFyaW9uZXR0ZS5Db21wb3NpdGVWaWV3XG5cdFx0Y2hpbGRWaWV3OiBTaWRlYmFyLlNlYXJjaEFsYnVtXG5cdFx0Y2hpbGRWaWV3Q29udGFpbmVyOiAnLnNlYXJjaC1ibG9jay1jb250ZW50J1xuXHRcdHRlbXBsYXRlOiAnI3NpZGViYXItc2VhcmNoLXJlc3VsdC1sYXlvdXQtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3NlYXJjaC1yZXN1bHQnXG5cdFx0ZW1wdHlWaWV3OiBTaWRlYmFyLlNlYXJjaEFsYnVtc0VtcHR5XG5cdFx0b25SZW5kZXI6IC0+XG5cdFx0XHRpZiBAY29sbGVjdGlvbi5sZW5ndGggaXMgMFxuXHRcdFx0XHRAJCgnLnNob3ctbW9yZScpLmhpZGUoKVxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRAJCgnLnNob3ctbW9yZScpLnNob3coKVxuXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrIC5zaG93LW1vcmUnOiAtPlxuXHRcdFx0XHR2YWwgPSAkKCcuc2lkZWJhci1ib2R5IC5zZWFyY2ggaW5wdXQnKS52YWwoKVxuXHRcdFx0XHRhcHAubmF2aWdhdGUgJy9zZWFyY2gvYWxidW1zLz9xPScrdmFsXG5cdFx0XHRcdFNpZGViYXIuQ29udHJvbGxlci5zaG93U2VhcmNoQWxidW1zUGFnZSB2YWxcblx0XHRcdFx0JCgnI3NpZGViYXInKS5yZW1vdmVDbGFzcyAnb3Blbidcblx0XHRcdFx0YXBwLm9wZW5TZWFyY2hQYWdlRnJvbVNpdGUgPSB0cnVlXG5cblxuXHRjbGFzcyBTaWRlYmFyLlNlYXJjaFBhZ2VUcmFjayBleHRlbmRzIFRyYWNrLkl0ZW1cblx0XHR0ZW1wbGF0ZTogJyN0cmFjay10cGwnXG5cdFx0Y2xhc3NOYW1lOiAndHJhY2staXRlbSdcblxuXHQjIGNsYXNzIFNpZGViYXIuU2VhcmNoUGFnZUFydGlzdCBleHRlbmRzIEFydGlzdC5JdGVtXG5cdCMgXHR0ZW1wbGF0ZTogJyNhcnRpc3QtaXRlbS10cGwnXG5cdCMgXHRjbGFzc05hbWU6ICdhcnRpc3QtaXRlbSdcblx0IyBcdG9uUmVuZGVyOiAtPlxuXHQjIFx0XHRpbWcgPSBuZXcgSW1hZ2Vcblx0IyBcdFx0JChpbWcpLm9uICdsb2FkLmltZycsID0+XG5cdCMgXHRcdFx0JChpbWcpLmFkZENsYXNzICdhcnRpc3QtYmcnXG5cdCMgXHRcdFx0LmhpZGUoKVxuXHQjIFx0XHRcdC5hcHBlbmRUbyBAJCAnLmNvbnRyb2xsJ1xuXHQjIFx0XHRcdC5mYWRlSW4gMzAwXG5cblx0IyBcdFx0XHQkKGltZykub2ZmICdsb2FkLmltZydcblx0IyBcdFx0bGluayA9IGFwcC5jcmVhdGVJbWdMaW5rKEBtb2RlbC5nZXQoJ2ltZycpLCczMDB4MzAwJylcblx0IyBcdFx0aW1nLnNyYyA9IGxpbmtcblxuXHQjIGNsYXNzIFNpZGViYXIuU2VhcmNoUGFnZUFsYnVtIGV4dGVuZHMgQWxidW0uSXRlbVxuXHQjIFx0dGVtcGxhdGU6ICcjYWxidW0taXRlbS10cGwnXG5cdCMgXHRjbGFzc05hbWU6ICdhbGJ1bS1pdGVtJ1xuXHQjIFx0b25SZW5kZXI6IC0+XG5cdCMgXHRcdGltZyA9IG5ldyBJbWFnZVxuXHQjIFx0XHQkKGltZykub24gJ2xvYWQuaW1nJywgPT5cblxuXHQjIFx0XHRcdCQoaW1nKS5hZGRDbGFzcyAnYWxidW0tYmcnXG5cdCMgXHRcdFx0LmhpZGUoKVxuXHQjIFx0XHRcdC5hcHBlbmRUbyBAJCAnLmNvbnRyb2xsJ1xuXHQjIFx0XHRcdC5mYWRlSW4gMzAwXG5cblx0IyBcdFx0XHQkKGltZykub2ZmICdsb2FkLmltZydcblx0IyBcdFx0bGluayA9IGFwcC5jcmVhdGVJbWdMaW5rKEBtb2RlbC5nZXQoJ2ltZycpLCczMDB4MzAwJylcblx0IyBcdFx0aW1nLnNyYyA9IGxpbmtcblxuXG5cdGNsYXNzIFNpZGViYXIuU2VhcmNoUGFnZVRyYWNrcyBleHRlbmRzIE1hcmlvbmV0dGUuQ29tcG9zaXRlVmlld1xuXHRcdHRlbXBsYXRlOiAnI3NpZGViYXItc2VhcmNoLXBhZ2UtbGF5b3V0LXRwbCdcblx0XHRjbGFzc05hbWU6ICdzZWFyY2gtcGFnZSB0cmFja3Mtd3JhcHBlcidcblx0XHRjaGlsZFZpZXc6IFNpZGViYXIuU2VhcmNoUGFnZVRyYWNrXG5cdFx0Y2hpbGRWaWV3Q29udGFpbmVyOiAnLnNlYXJjaC1jb250ZW50J1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAuY2xvc2UnOiAtPlxuXHRcdFx0XHRTaWRlYmFyLkNvbnRyb2xsZXIuY2xvc2VTZWFyY2hQYWdlKClcblxuXHRjbGFzcyBTaWRlYmFyLlNlYXJjaFBhZ2VBcnRpc3RzIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db21wb3NpdGVWaWV3XG5cdFx0dGVtcGxhdGU6ICcjc2lkZWJhci1zZWFyY2gtcGFnZS1sYXlvdXQtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3NlYXJjaC1wYWdlIGFydGlzdHMtd3JhcHBlcidcblx0XHRjaGlsZFZpZXc6IFNpZGViYXIuU2VhcmNoUGFnZUFydGlzdFxuXHRcdGNoaWxkVmlld0NvbnRhaW5lcjogJy5zZWFyY2gtY29udGVudCdcblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2sgLmNsb3NlJzogLT5cblx0XHRcdFx0U2lkZWJhci5Db250cm9sbGVyLmNsb3NlU2VhcmNoUGFnZSgpXG5cdFx0b25CZWZvcmVSZW5kZXJDb2xsZWN0aW9uOiAtPlxuXHRcdFx0QGF0dGFjaEh0bWwgPSAoY29sbGVjdGlvblZpZXcsIGl0ZW1WaWV3LCBpbmRleCktPlxuXHRcdFx0XHRjb2xsZWN0aW9uVmlldy4kZWwuYXBwZW5kIGl0ZW1WaWV3LmVsXG5cdFx0XHRcdC5hcHBlbmQgJyAnXG5cblx0Y2xhc3MgU2lkZWJhci5TZWFyY2hQYWdlQWxidW1zIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db21wb3NpdGVWaWV3XG5cdFx0dGVtcGxhdGU6ICcjc2lkZWJhci1zZWFyY2gtcGFnZS1sYXlvdXQtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3NlYXJjaC1wYWdlIGFsYnVtcy13cmFwcGVyJ1xuXHRcdGNoaWxkVmlldzogU2lkZWJhci5TZWFyY2hQYWdlQWxidW1cblx0XHRjaGlsZFZpZXdDb250YWluZXI6ICcuc2VhcmNoLWNvbnRlbnQnXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrIC5jbG9zZSc6IC0+XG5cdFx0XHRcdFNpZGViYXIuQ29udHJvbGxlci5jbG9zZVNlYXJjaFBhZ2UoKVxuXHRcdG9uQmVmb3JlUmVuZGVyQ29sbGVjdGlvbjogLT5cblx0XHRcdEBhdHRhY2hIdG1sID0gKGNvbGxlY3Rpb25WaWV3LCBpdGVtVmlldywgaW5kZXgpLT5cblx0XHRcdFx0Y29sbGVjdGlvblZpZXcuJGVsLmFwcGVuZCBpdGVtVmlldy5lbFxuXHRcdFx0XHQuYXBwZW5kICcgJ1xuXG5cdHJldHVybiBTaWRlYmFyXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvc2lkZWJhci92aWV3LmNvZmZlZVxuICoqLyIsImRlZmluZSBbJy4vdmlldy5jb2ZmZWUnLCcuL21vZGVsLmNvZmZlZScsJy4uLy4uLy4uL3BhcnRzL25vdGlmaWNhdGlvbiddLCAoVHJhY2ssTW9kZWwsTm90aWZpY2F0aW9uKS0+XG5cdHJlcXVpcmUoJy4vc3R5bGUuc3R5bCcpXG5cdFxuXHRjbGFzcyBjb250cm9sbGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db250cm9sbGVyXG5cblx0XHRvcGVuT3B0aW9uczogKGl0ZW0pLT5cblx0XHRcdGlmIGl0ZW0ubW9kZWwuaGFzICdvcHRpb25zTGlzdCdcblx0XHRcdFx0aWYgaXRlbS5tb2RlbC5oYXMgJ3BsYXlsaXN0VHJhY2snXG5cdFx0XHRcdFx0bGlzdCA9IFtcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0dGl0bGU6ICfRg9C00LDQu9C40YLRjCDQuNC3INC/0LvQtdC50LvQuNGB0YLQsCdcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdkZWxldGUtZnJvbS1wbGF5bGlzdCdcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRdXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRpZiBpdGVtLm1vZGVsLmdldCAnaW5GYXZvcml0ZSdcblx0XHRcdFx0XHRcdGxpc3QgPSBbXG5cdFx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0XHR0aXRsZTogJ9GD0LTQsNC70LjRgtGMINC40Lcg0LzQtdC00LjQsNGC0LXQutC4J1xuXHRcdFx0XHRcdFx0XHRcdGNsYXNzOiAnZGVsZXRlJ1xuXHRcdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XHRdXG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0bGlzdCA9IFtcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHRpdGxlOiAn0LTQvtCx0LDQstC40YLRjCDQsiDQvNC10LTQuNCw0YLQtdC60YMnXG5cdFx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdhZGQnXG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdF1cblxuXHRcdFx0XHRfLmVhY2ggaXRlbS5tb2RlbC5nZXQoJ29wdGlvbnNMaXN0JyksIChpdGVtKS0+XG5cdFx0XHRcdFx0bGlzdC5wdXNoIGl0ZW1cblxuXHRcdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24gbGlzdFxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb25cblxuXHRcdFx0aWYgaXRlbS5tb2RlbC5nZXQgJ2FydGlzdF9pc192YSdcblx0XHRcdFx0b3B0cyA9IGNvbGxlY3Rpb24uZmluZFdoZXJlXG5cdFx0XHRcdFx0YXJ0aXN0OiB0cnVlXG5cdFx0XHRcdGlmIG9wdHNcblx0XHRcdFx0XHRvcHRzLnJlbW92ZVxuXHRcdFx0XHRcdFx0c2lsZW50OnRydWVcblxuXHRcdFx0bGF5b3V0ID0gbmV3IFRyYWNrLk9wdGlvbnNMYXlvdXRcblx0XHRcdFx0bW9kZWw6IGl0ZW0ubW9kZWxcblx0XHRcdFx0Y29sbGVjdGlvbjogY29sbGVjdGlvblxuXG5cdFx0XHRpdGVtLm9wdGlvbnNSZWdpb24uc2hvdyBsYXlvdXRcblxuXG5cdFx0b3BlblBsYXlsaXN0c0xpc3Q6IChpdGVtKS0+XG5cblx0XHRcdHBsYXlsaXN0ID0gYXBwLnJlcXVlc3QgJ2dldDpmYXZvcml0ZTpwbGF5bGlzdHMnXG5cdFx0XHRpZiAhcGxheWxpc3QucHJvbWlzZVxuXHRcdFx0XHR2aWV3ID0gbmV3IFRyYWNrLlBsYXlsaXN0SXRlbXNcblx0XHRcdFx0XHRtb2RlbDogaXRlbS5tb2RlbFxuXHRcdFx0XHRcdGNvbGxlY3Rpb246IHBsYXlsaXN0XG5cblx0XHRcdFx0aXRlbS5vcHRpb25zUmVnaW9uLnNob3cgdmlld1xuXG5cdFx0c2VsZWN0VGV4dDogKHRhcmdldCktPlxuXHRcdFx0aWYgZG9jdW1lbnQuY3JlYXRlUmFuZ2Vcblx0XHRcdFx0cm5nID0gZG9jdW1lbnQuY3JlYXRlUmFuZ2UoKTtcblx0XHRcdFx0dGV4dE5vZGUgPSB0YXJnZXQuZmlyc3RDaGlsZDtcblxuXHRcdFx0XHRybmcuc2VsZWN0Tm9kZSggdGFyZ2V0ICk7XG5cblx0XHRcdFx0cm5nLnNldFN0YXJ0KCB0ZXh0Tm9kZSwgMCApO1xuXHRcdFx0XHRybmcuc2V0RW5kKCB0ZXh0Tm9kZSwgdGV4dE5vZGUubGVuZ3RoICk7XG5cblx0XHRcdFx0c2VsID0gd2luZG93LmdldFNlbGVjdGlvbigpO1xuXHRcdFx0XHRzZWwucmVtb3ZlQWxsUmFuZ2VzKCk7XG5cdFx0XHRcdHNlbC5hZGRSYW5nZSggcm5nICk7XG5cblxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRybmcgPSBkb2N1bWVudC5ib2R5LmNyZWF0ZVRleHRSYW5nZSgpO1xuXHRcdFx0XHRybmcubW92ZVRvRWxlbWVudFRleHQoIHRhcmdldCApO1xuXHRcdFx0XHRybmcuc2VsZWN0KCk7XG5cblx0XHRyZW1vdmVUcmFja0Zyb21GYXZvcml0ZTogKGlkKS0+XG5cdFx0XHRhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnRyYWNrcydcblx0XHRcdC5kb25lIChmYXZvcml0ZXMpLT5cblx0XHRcdFx0dHJhY2sgPSBmYXZvcml0ZXMuZmluZFdoZXJlXG5cdFx0XHRcdFx0aWQ6IGlkXG5cblx0XHRcdFx0aWYgdHJhY2tcblx0XHRcdFx0XHRkbyB0cmFjay5kZXN0cm95XG5cblx0XHRcdFx0XHROb3RpZmljYXRpb24uQ29udHJvbGxlci5zaG93KHttZXNzYWdlOiBcItCi0YDQtdC6IMKrI3t0cmFjay5nZXQgJ3RpdGxlJyB9wrsg0YPQtNCw0LvQtdC9INC80LXQtNC40LDRgtC10LrQuFwiLHR5cGU6ICdzdWNjZXNzJyx0aW1lOiAzMDAwfSlcblxuXHRcdGFkZFRyYWNrVG9GYXZvcml0ZTogKHRyYWNrKS0+XG5cdFx0XHRhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnRyYWNrcydcblx0XHRcdC5kb25lIChmYXZvcml0ZXMpLT5cblx0XHRcdFx0bW9kZWwgPSB0cmFjay50b0pTT04oKVxuXHRcdFx0XHRpZiAhbW9kZWwuZmF2b3VyaXRlcyBvciBtb2RlbC5mYXZvdXJpdGVzLnNvcnQgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdFx0bW9kZWwuZmF2b3VyaXRlcyA9IHt9XG5cdFx0XHRcdFx0bW9kZWwuZmF2b3VyaXRlcy5zb3J0ID0gMTAwMDBcblxuXHRcdFx0XHRmYXZvcml0ZXMuY3JlYXRlIG1vZGVsXG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ9CU0L7QsdCw0LLQu9C10L3QuNC1INGC0YDQtdC60LAg0LIg0LjQt9Cx0YDQsNC90L3QvtC1JywgdHJhY2suZ2V0KCd0aXRsZScpKycgLSAnK2xvY2F0aW9uLmhyZWZcblx0XHRcdFx0Tm90aWZpY2F0aW9uLkNvbnRyb2xsZXIuc2hvdyh7bWVzc2FnZTogXCLQotGA0LXQuiDCqyN7dHJhY2suZ2V0ICd0aXRsZScgfcK7INC00L7QsdCw0LLQu9C10L0g0LzQtdC00LjQsNGC0LXQutGDXCIsdHlwZTogJ3N1Y2Nlc3MnLHRpbWU6IDMwMDB9KVxuXG5cdFx0Y3JlYXRlUGxheWxpc3Q6ICh0aXRsZSx0cmFjayktPlxuXHRcdFx0cGxheWxpc3QgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnBsYXlsaXN0cydcblx0XHRcdHBsYXlsaXN0Q3JlYXRlID0gcGxheWxpc3QuY3JlYXRlIHsndGl0bGUnOiB0aXRsZSx0cmFja3M6IFt0cmFjay5tb2RlbC50b0pTT04oKV19LHt3YWl0OnRydWV9XG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyIC5iYWNrLXRvLW9wdGlvbnMnKS5oaWRlKClcblx0XHRcdCQoJy5vcHRpb25zLXdyYXBwZXIgLm9wdGlvbnMtcGxheWxpc3Qtc2Nyb2xsLXdyYXBwJykuaGlkZSgpXG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyJykuY3NzKCdtaW4taGVpZ2h0JywgMzAwKydweCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxvYWRlclwiIHN0eWxlPVwidG9wOjEzMHB4XCI+PC9kaXY+Jyk7XG5cblx0XHRcdHBsYXlsaXN0Q3JlYXRlLm9uY2UgJ3N5bmMnLCAocGxheWxpc3QpPT5cblx0XHRcdFx0QGFkZFRyYWNrVG9QbGF5bGlzdCBwbGF5bGlzdC5pZCx0cmFjay5tb2RlbFxuXG5cdFx0YWRkVHJhY2tUb1BsYXlsaXN0OiAocGxheWxpc3RJZCx0cmFjayktPlxuXHRcdFx0cGxheWxpc3RJZCA9ICtwbGF5bGlzdElkXG5cdFx0XHRwbGF5bGlzdHMgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOnBsYXlsaXN0cydcblxuXHRcdFx0cGxheWxpc3QgPSBwbGF5bGlzdHMuZmluZFdoZXJlXG5cdFx0XHRcdGlkOiBwbGF5bGlzdElkXG5cblx0XHRcdHdpbmRvdy50ZXN0ID0gcGxheWxpc3RcblxuXHRcdFx0c3VjY2VzcyA9IChkYXRhKS0+XG5cdFx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gXCLQlNC+0LHQsNCy0LvQtdC90LjQtSDRgtGA0LXQutCwINCyINC/0LvQtdC50LvQuNGB0YIgI3twbGF5bGlzdElkfVwiLCB0cmFjay5nZXQoJ2FydGlzdCcpKycgLSAnK3RyYWNrLmdldCgndGl0bGUnKVxuXG5cdFx0XHRcdHRyYWNrRGF0YSA9IHRyYWNrLnRvSlNPTigpXG5cdFx0XHRcdHRyYWNrRGF0YS5wbGF5bGlzdCA9IGRhdGEucGxheWxpc3RcblxuXHRcdFx0XHRpZiBwbGF5bGlzdC5oYXMgJ3RyYWNrcydcblx0XHRcdFx0XHRjb2xsZWN0aW9uID0gXy51bmlvbiBwbGF5bGlzdC5nZXQoJ3RyYWNrcycpLFt0cmFja0RhdGFdXG5cdFx0XHRcdFx0Y291bnQgPSBjb2xsZWN0aW9uLmxlbmd0aFxuXHRcdFx0XHRcdHBsYXlsaXN0LnNldFxuXHRcdFx0XHRcdFx0dHJhY2tzOiBjb2xsZWN0aW9uXG5cdFx0XHRcdFx0XHRpdGVtc19jb3VudDogY291bnRcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHBsYXlsaXN0LnNldFxuXHRcdFx0XHRcdFx0dHJhY2tzOiBbdHJhY2tEYXRhXVxuXHRcdFx0XHRcdFx0aXRlbXNfY291bnQ6IDFcblxuXHRcdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyJykucmVtb3ZlKClcblx0XHRcdFx0Tm90aWZpY2F0aW9uLkNvbnRyb2xsZXIuc2hvdyh7bWVzc2FnZTogXCLQotGA0LXQuiDCqyN7dHJhY2suZ2V0ICd0aXRsZScgfcK7INC00L7QsdCw0LLQu9C10L0g0L/Qu9C10LnQu9C40YHRglwiLHR5cGU6ICdzdWNjZXNzJyx0aW1lOiAzMDAwfSlcblxuXHRcdFx0aWYgcGxheWxpc3Rcblx0XHRcdFx0JCgnLm9wdGlvbnMtd3JhcHBlcicpLmNzcygnbWluLWhlaWdodCcsIDMwMCsncHgnKS5lbXB0eSgpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImxvYWRlclwiIHN0eWxlPVwidG9wOjEzMHB4XCI+PC9kaXY+Jyk7XG5cdFx0XHRcdCQuYWpheFxuXHRcdFx0XHRcdHR5cGU6IFwiR0VUXCIsXG5cdFx0XHRcdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9wbGF5bGlzdC9hZGRpdGVtXCIsXCI/Y29udGVudGlkPSN7dHJhY2suZ2V0KCdpZCcpfSZwbGF5bGlzdGlkPSN7cGxheWxpc3RJZH1cIilcblx0XHRcdFx0XHRzdWNjZXNzOiBzdWNjZXNzXG5cblxuXG5cdFx0cmVtb3ZlVHJhY2tGcm9tUGxheWxpc3Q6ICh0cmFjayxwYXJlbnQpLT5cblx0XHRcdHBhcmVudFRyYWNrTW9kZWwgPSBwYXJlbnQub3B0aW9ucy5wYXJlbnRUcmFjay5tb2RlbFxuXHRcdFx0b2xkQ29sbGVjdGlvbiA9IHBhcmVudFRyYWNrTW9kZWwuZ2V0ICd0cmFja3MnXG5cdFx0XHRuZXdDb2xsZWN0aW9uID0gXy5maWx0ZXIgb2xkQ29sbGVjdGlvbiwgKG9iaiktPlxuXHRcdFx0XHRyZXR1cm4gdHJhY2subW9kZWwuZ2V0KCdpZCcpIGlzbnQgb2JqLmlkXG5cblx0XHRcdHBhcmVudFRyYWNrTW9kZWwuc2V0ICd0cmFja3MnLCBuZXdDb2xsZWN0aW9uXG5cdFx0XHQjXHRcdFx0dmlldy4kZWwuYWRkQ2xhc3MgJ2Rpc2FibGUnXG5cdFx0XHQjXHRcdFx0dmlldy4kKCcucmVtb3ZlLWZyb20tZnZydCcpLnJlbW92ZUNsYXNzKCdyZW1vdmUtZnJvbS1mdnJ0JykuYWRkQ2xhc3MoJ2FkZC10by1mdnJ0JylcblxuXHRcdFx0JC5hamF4XG5cdFx0XHRcdHR5cGU6IFwiR0VUXCIsXG5cdFx0XHRcdHVybDogYXBwLmdldFJlcXVlc3RVcmxTaWQoXCIvcGxheWxpc3QvcmVtb3ZlaXRlbVwiLFwiP2l0ZW1pZD0je3RyYWNrLm1vZGVsLmdldCgncGxheWxpc3QnKS5pdGVtaWR9XCIpXG5cblx0VHJhY2suQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cblx0Xy5leHRlbmQgVHJhY2ssIE1vZGVsXG5cblx0cmV0dXJuIFRyYWNrXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjay9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuLi8uLi8uLi9wYXJ0cy9wbGF5ZXInXSwgKFBsYXllciktPlxuXHRUcmFjayA9IHt9XG5cdGNsYXNzIFRyYWNrLkl0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkxheW91dFZpZXdcblx0XHR0ZW1wbGF0ZTogJyN0cmFjay10cGwnXG5cdFx0Y2xhc3NOYW1lOiAndHJhY2staXRlbSdcblx0XHRyZWdpb25zOlxuXHRcdFx0b3B0aW9uc1JlZ2lvbjogJy5vcHRpb25zLWNvbnRlbnQnXG5cblx0XHRvblNob3c6IC0+XG5cdFx0XHRpZiBAbW9kZWwuaGFzICdpc19kZWxldGVkJ1xuXHRcdFx0XHRAJGVsLmFkZENsYXNzICdkaXNhYmxlJ1xuXHRcdGlkOiAtPlxuXHRcdFx0cmV0dXJuIEBtb2RlbC5nZXQgJ3VuaXEnXG5cblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRpZiBAbW9kZWwuaGFzICdpc19kZWxldGVkJ1xuXHRcdFx0XHRcdHJldHVyblxuXG5cdFx0XHRcdGlmIEAkZWwuaGFzQ2xhc3MgJ2FjdGl2ZSdcblx0XHRcdFx0XHRpZiBAJGVsLmhhc0NsYXNzICdwbGF5J1xuXHRcdFx0XHRcdFx0ZG8gUGxheWVyLkNvbnRyb2xsZXIucGxheVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnBhdXNlXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRhcHAucGxheWVkQWxidW0gPSBudWxsXG5cdFx0XHRcdFx0UGxheWVyLkNvbnRyb2xsZXIucGxheUJ5Q29sbGVjdGlvbiBAbW9kZWwuY29sbGVjdGlvbixAbW9kZWwuZ2V0KCdpZCcpLEJhY2tib25lLmhpc3RvcnkubG9jYXRpb24uaHJlZlxuXG5cdFx0XHQnbW91c2VlbnRlcic6IC0+XG5cdFx0XHRcdGZhdm9yaXRlcyA9IGFwcC5yZXF1ZXN0ICdnZXQ6ZmF2b3JpdGU6dHJhY2tzJ1xuXG5cdFx0XHRcdGlmIGZhdm9yaXRlcy5wcm9taXNlXG5cdFx0XHRcdFx0ZmF2b3JpdGVzLmRvbmUgKGNvbGxlY3Rpb24pPT5cblx0XHRcdFx0XHRcdGl0ZW0gPSBjb2xsZWN0aW9uLnNvbWUgKGl0ZW0pPT5cblx0XHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW0uZ2V0KCdpZCcpIGlzIEBtb2RlbC5nZXQoJ2lkJylcblx0XHRcdFx0XHRcdGlmIGl0ZW1cblx0XHRcdFx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIHRydWVcblx0XHRcdFx0XHRcdFx0QCQoJy50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdhZGQnKS5hZGRDbGFzcygnZGVsZXRlJylcblx0XHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIGZhbHNlXG5cdFx0XHRcdFx0XHRcdEAkKCcudHJhY2stY29udHJvbGwnKS5yZW1vdmVDbGFzcygnZGVsZXRlJykuYWRkQ2xhc3MoJ2FkZCcpXG5cblx0XHRcdFx0XHRmYXZvcml0ZXMuZmFpbCAoZXJyKS0+XG5cdFx0XHRcdFx0XHRjb25zb2xlLmxvZyBhcmd1bWVudHNcblxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0aXRlbSA9IGZhdm9yaXRlcy5zb21lIChpdGVtKT0+XG5cdFx0XHRcdFx0XHRyZXR1cm4gaXRlbS5nZXQoJ2lkJykgaXMgQG1vZGVsLmdldCgnaWQnKVxuXHRcdFx0XHRcdGlmIGl0ZW1cblx0XHRcdFx0XHRcdEBtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCB0cnVlXG5cdFx0XHRcdFx0XHRAJCgnLnRyYWNrLWNvbnRyb2xsJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdEBtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCBmYWxzZVxuXHRcdFx0XHRcdFx0QCQoJy50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdkZWxldGUnKS5hZGRDbGFzcygnYWRkJylcblxuXHRcdFx0J2NsaWNrIC5wbGF5bGlzdC10cmFjay1jb250cm9sbCc6IC0+XG5cdFx0XHRcdEB0cmlnZ2VyICdwbGF5bGlzdDp0cmFjazpkZWxldGUnXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnY2xpY2sgLnRyYWNrLWNvbnRyb2xsLmFkZCc6IC0+XG5cdFx0XHRcdFRyYWNrLkNvbnRyb2xsZXIuYWRkVHJhY2tUb0Zhdm9yaXRlIEBtb2RlbFxuXHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZVxuXHRcdFx0XHRAJCgnLnRyYWNrLWNvbnRyb2xsJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0XHRhcHAudHJpZ2dlciAnYWRkOnRvOmZhdm9yaXRlJ1xuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC50cmFjay1jb250cm9sbC5kZWxldGUnOiAtPlxuXHRcdFx0XHRUcmFjay5Db250cm9sbGVyLnJlbW92ZVRyYWNrRnJvbUZhdm9yaXRlIEBtb2RlbC5nZXQgJ2lkJ1xuXHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgZmFsc2Vcblx0XHRcdFx0QCQoJy50cmFjay1jb250cm9sbCcpLnJlbW92ZUNsYXNzKCdkZWxldGUnKS5hZGRDbGFzcygnYWRkJylcblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3JlbW92ZTpmcm9tOmZhdm9yaXRlJ1xuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5vcHRpb25zLXdyYXBwZXInOiAtPlxuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5tb3JlLW9wdGlvbnMnOiAtPlxuXHRcdFx0XHRUcmFjay5Db250cm9sbGVyLm9wZW5PcHRpb25zIEBcblx0XHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRcdCdjbGljayAuYWRkLXRvLXBsYXlsaXN0JzogLT5cblx0XHRcdFx0VHJhY2suQ29udHJvbGxlci5vcGVuUGxheWxpc3RzTGlzdCBAXG5cdFx0XHRcdHJldHVybiBmYWxzZVxuXG5cdFx0XHQnY2xpY2sgLmJhY2stdG8tb3B0aW9ucyc6IC0+XG5cdFx0XHRcdFRyYWNrLkNvbnRyb2xsZXIub3Blbk9wdGlvbnMgQFxuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5vcHRpb25zLXBsYXlsaXN0LWl0ZW0nOiAoZSktPlxuXHRcdFx0XHRUcmFjay5Db250cm9sbGVyLmFkZFRyYWNrVG9QbGF5bGlzdCAkKGUudGFyZ2V0KS5hdHRyKCdkYXRhLWlkJyksQG1vZGVsXG5cblx0XHRcdCdjbGljayAub3B0aW9ucy1vcGVuLWFsYnVtJzogKGUpLT5cblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKVxuXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphbGJ1bScsIEBtb2RlbC5nZXQoJ3JlbGVhc2VfaWQnKVxuXG5cdFx0XHQnY2xpY2sgLm9wdGlvbnMtb3Blbi1hcnRpc3QnOiAoZSktPlxuXHRcdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpXG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzaG93OmFydGlzdCcsIEBtb2RlbC5nZXQoJ2FydGlzdGlkJylcblxuXHRcdFx0J2tleXByZXNzIC5uZXctcGxheWxpc3QgaW5wdXQnOiAoZSktPlxuXG5cdFx0XHRcdGlmIGUua2V5Q29kZSBpcyAxM1xuXHRcdFx0XHRcdFRyYWNrLkNvbnRyb2xsZXIuY3JlYXRlUGxheWxpc3QgJCgnLm5ldy1wbGF5bGlzdCBpbnB1dCcpLnZhbCgpLEBcblxuXHRcdFx0J2NsaWNrIC5hcnRpc3Qgc3Bhbic6IChlKS0+XG5cdFx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKClcblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YXJ0aXN0JywgQG1vZGVsLmdldCgnYXJ0aXN0aWQnKVxuXG5cblxuXG5cblx0Y2xhc3MgVHJhY2suT3B0aW9uc0l0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6ICcjdHJhY2stb3B0aW9ucy1saXN0LWl0ZW0tdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3RyYWNrLW9wdGlvbnMtbGlzdC1pdGVtJ1xuXHRcdHRhZ05hbWU6ICdsaSdcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEAkZWwuYWRkQ2xhc3MgQG1vZGVsLmdldCAnY2xhc3MnXG5cblx0Y2xhc3MgVHJhY2suT3B0aW9uc0xheW91dCBleHRlbmRzIE1hcmlvbmV0dGUuQ29tcG9zaXRlVmlld1xuXHRcdHRlbXBsYXRlOiAnI29wdGlvbnMtbGlzdC10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnb3B0aW9ucy13cmFwcGVyJ1xuXHRcdGNoaWxkVmlldzogVHJhY2suT3B0aW9uc0l0ZW1cblx0XHRjaGlsZFZpZXdDb250YWluZXI6ICcub3B0aW9ucy1saXN0J1xuXHRcdG9uU2hvdzogLT5cblx0XHRcdHRhcmdldCA9IEAkKCcudGV4dGFyZWEnKVswXVxuXHRcdFx0VHJhY2suQ29udHJvbGxlci5zZWxlY3RUZXh0IHRhcmdldFxuXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrJzogLT5cblx0XHRcdFx0dGFyZ2V0ID0gQCQoJy50ZXh0YXJlYScpWzBdXG5cdFx0XHRcdFRyYWNrLkNvbnRyb2xsZXIuc2VsZWN0VGV4dCB0YXJnZXRcblxuXHRcdFx0J2NsaWNrIC5kZWxldGUnOiAoZSktPlxuXHRcdFx0XHRUcmFjay5Db250cm9sbGVyLnJlbW92ZVRyYWNrRnJvbUZhdm9yaXRlIEBtb2RlbC5nZXQgJ2lkJ1xuXHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgZmFsc2UsIHtzaWxlbnQ6IHRydWV9XG5cdFx0XHRcdCQoZS50YXJnZXQpLmNsb3Nlc3QoJy5vcHRpb25zJykuZmluZCgnLnRyYWNrLWNvbnRyb2xsJykucmVtb3ZlQ2xhc3MoJ2RlbGV0ZScpLmFkZENsYXNzKCdhZGQnKVxuXHRcdFx0XHRAJGVsLmZpbmQoJy5kZWxldGUnKS5yZW1vdmVDbGFzcygnZGVsZXRlJykuYWRkQ2xhc3MoJ2FkZCcpLnRleHQoJ9CU0L7QsdCw0LLQuNGC0Ywg0LIg0LzQtdC00LjQsNGC0LXQutGDJylcblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3JlbW92ZTpmcm9tOmZhdm9yaXRlJ1xuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J2NsaWNrIC5hZGQnOiAtPlxuXHRcdFx0XHRUcmFjay5Db250cm9sbGVyLmFkZFRyYWNrVG9GYXZvcml0ZSBAbW9kZWxcblx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIHRydWUsIHtzaWxlbnQ6IHRydWV9XG5cdFx0XHRcdEAkZWwuY2xvc2VzdCgnLm9wdGlvbnMnKS5maW5kKCcudHJhY2stY29udHJvbGwnKS5yZW1vdmVDbGFzcygnYWRkJykuYWRkQ2xhc3MoJ2RlbGV0ZScpXG5cdFx0XHRcdEAkZWwuZmluZCgnLmFkZCcpLnJlbW92ZUNsYXNzKCdhZGQnKS5hZGRDbGFzcygnZGVsZXRlJykudGV4dCgn0KPQtNCw0LvQuNGC0Ywg0LjQtyDQvNC10LTQuNCw0YLQtdC60LgnKVxuXHRcdFx0XHRhcHAudHJpZ2dlciAnYWRkOnRvOmZhdm9yaXRlJ1xuXHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0J21vdXNlbGVhdmUnOiAnY2xvc2VPcHRpb25zJ1xuXHRcdFx0J2NsaWNrIC5vcHRpb25zLW92ZXJsYXknOiAnY2xvc2VPcHRpb25zJ1xuXG5cdFx0Y2xvc2VPcHRpb25zOiAoZSktPlxuXHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKVxuXHRcdFx0JCgnLm9wdGlvbnMtd3JhcHBlcicpLnJlbW92ZSgpXG5cblx0Y2xhc3MgVHJhY2suUGxheWxpc3RJdGVtIGV4dGVuZHMgTWFyaW9uZXR0ZS5JdGVtVmlld1xuXHRcdHRlbXBsYXRlOiAnI29wdGlvbnMtcGxheWxpc3QtaXRlbS10cGwnXG5cdFx0Y2xhc3NOYW1lOiAnb3B0aW9ucy1wbGF5bGlzdC1pdGVtJ1xuXHRcdHRhZ25hbWU6ICdsaSdcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEAkZWwuYXR0ciAnZGF0YS1pZCcsIEBtb2RlbC5nZXQgJ2lkJ1xuXG5cdGNsYXNzIFRyYWNrLlBsYXlsaXN0SXRlbXMgZXh0ZW5kcyBNYXJpb25ldHRlLkNvbXBvc2l0ZVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNvcHRpb25zLXBsYXlsaXN0LXdpbmRvdy10cGwnXG5cdFx0Y2hpbGRWaWV3OiBUcmFjay5QbGF5bGlzdEl0ZW1cblx0XHRjaGlsZFZpZXdDb250YWluZXI6ICcuY29udGFpbmVyJ1xuXHRcdGNsYXNzTmFtZTogJ29wdGlvbnMtd3JhcHBlcidcblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2sgLm5ldy1wbGF5bGlzdCBpbnB1dCc6IC0+XG5cdFx0XHRcdCQoJy5uZXctcGxheWxpc3QgaW5wdXQnKS5zZWxlY3QoKVxuXHRcdFx0J2JsdXIgLm5ldy1wbGF5bGlzdCBpbnB1dCc6IC0+XG5cdFx0XHRcdCQoJy5uZXctcGxheWxpc3QgaW5wdXQnKS52YWwgJysg0J3QvtCy0YvQuSDQv9C70LXQudC70LjRgdGCJ1xuXG5cdFx0b25TaG93OiAtPlxuXHRcdFx0JCgnLm9wdGlvbnMtd3JhcHBlciAub3B0aW9ucy1wbGF5bGlzdC1zY3JvbGwtd3JhcHAnKS5jdXN0b21TY3JvbGxiYXIoKTtcblxuXHRjbGFzcyBUcmFjay5MaXN0VmlldyBleHRlbmRzIE1hcmlvbmV0dGUuQ29sbGVjdGlvblZpZXdcblx0XHRjaGlsZFZpZXc6IFRyYWNrLkl0ZW1cblx0XHRjbGFzc05hbWU6ICd0cmFja3Mtd3JhcHBlcidcblxuXHRyZXR1cm4gVHJhY2tcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3RyYWNrL3ZpZXcuY29mZmVlXG4gKiovIiwiZGVmaW5lIFtdLCAtPlxuICAgIFRyYWNrID0ge31cblxuICAgIGNsYXNzIFRyYWNrLk1vZGVsIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcbiAgICAgICAgdXJsUm9vdDogJy9mYXZvcml0ZXNUcmFja3MnXG4gICAgICAgIGRlZmF1bHRzOlxuICAgICAgICAgIFwiZHVyYXRpb25cIjogXCJcIlxuICAgICAgICAgIFwiYXJ0aXN0XCI6IFwiXCJcbiAgICAgICAgICBcInRpdGxlXCI6IFwiXCJcblxuICAgIGNsYXNzIFRyYWNrLkNvbGxlY3Rpb24gZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG4gICAgICAgIG1vZGVsOiBUcmFjay5Nb2RlbFxuXG4gICAgcmV0dXJuIFRyYWNrXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjay9tb2RlbC5jb2ZmZWVcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjay9zdHlsZS5zdHlsXG4gKiogbW9kdWxlIGlkID0gMjVcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsImRlZmluZSBbJy4vdmlldy5jb2ZmZWUnLCAnLi9tb2RlbC5jb2ZmZWUnLCcuLi8uLi8uLi9wYXJ0cy9wbGF5ZXInLCcuLi8uLi8uLi9wYXJ0cy9ub3RpZmljYXRpb24nXSwgKEFsYnVtLCBNb2RlbCwgUGxheWVyLCBOb3RpZmljYXRpb24pLT5cblx0cmVxdWlyZSgnLi9zdHlsZS5zdHlsJylcblxuXHRjbGFzcyBjb250cm9sbGVyIGV4dGVuZHMgTWFyaW9uZXR0ZS5Db250cm9sbGVyXG5cdFx0UGxheTogKGFsYnVtTW9kZWwpLT5cblx0XHRcdGFwcC5wbGF5ZWRBbGJ1bSA9IGFsYnVtTW9kZWwuZ2V0ICdpZCdcblxuXHRcdFx0aWYgYWxidW1Nb2RlbC5oYXMgJ3RyYWNrcydcblx0XHRcdFx0Y29sbGVjdGlvbiA9IG5ldyBCYWNrYm9uZS5Db2xsZWN0aW9uIGFsYnVtTW9kZWwuZ2V0ICd0cmFja3MnXG5cdFx0XHRcdGNvbGxlY3Rpb24uZWFjaCAoaXRlbSktPlxuXHRcdFx0XHRcdGl0ZW0uc2V0ICd1bmlxJywgJ2FsYnVtJytpdGVtLmlkXG5cdFx0XHRcdFx0aXRlbS5zZXQgJ29wdGlvbnNMaXN0JywgW1xuXHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5vcHRpb25zLmFkZFRQbFxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ2FkZC10by1wbGF5bGlzdCdcblx0XHRcdFx0XHRcdH0se1xuXHRcdFx0XHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5vcHRpb25zLnRvQXJ0XG5cdFx0XHRcdFx0XHRcdGNsYXNzOiAnb3B0aW9ucy1vcGVuLWFydGlzdCdcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRdXG5cdFx0XHRcdFBsYXllci5Db250cm9sbGVyLnBsYXlCeUNvbGxlY3Rpb24gY29sbGVjdGlvblxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRhbGJ1bVRyYWNrcyA9IGRvIGFsYnVtTW9kZWwuZmV0Y2hcblxuXHRcdFx0XHRhbGJ1bVRyYWNrcy5kb25lIC0+XG5cdFx0XHRcdFx0Y29sbGVjdGlvbiA9IG5ldyBCYWNrYm9uZS5Db2xsZWN0aW9uIGFsYnVtTW9kZWwuZ2V0ICd0cmFja3MnXG5cdFx0XHRcdFx0Y29sbGVjdGlvbi5lYWNoIChpdGVtKS0+XG5cdFx0XHRcdFx0XHRpdGVtLnNldCAndW5pcScsICdhbGJ1bScraXRlbS5pZFxuXHRcdFx0XHRcdFx0aXRlbS5zZXQgJ29wdGlvbnNMaXN0JywgW1xuXHRcdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy5hZGRUUGxcblx0XHRcdFx0XHRcdFx0XHRjbGFzczogJ2FkZC10by1wbGF5bGlzdCdcblx0XHRcdFx0XHRcdFx0fSx7XG5cdFx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy50b0FydFxuXHRcdFx0XHRcdFx0XHRcdGNsYXNzOiAnb3B0aW9ucy1vcGVuLWFydGlzdCdcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XVxuXG5cdFx0XHRcdFx0aHJlZiA9IEJhY2tib25lLmhpc3RvcnkubG9jYXRpb24ub3JpZ2luKycvJytCYWNrYm9uZS5oaXN0b3J5LmxvY2F0aW9uLnBhdGhuYW1lLnNwbGl0KCcvJylbMV0rJy9hbGJ1bS8nK2FsYnVtTW9kZWwuZ2V0ICdpZCdcblx0XHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5wbGF5QnlDb2xsZWN0aW9uIGNvbGxlY3Rpb24sbnVsbCxocmVmXG5cblx0XHRcdFx0YWxidW1UcmFja3MuZmFpbCAoZXJyKS0+XG5cdFx0XHRcdFx0Y29uc29sZS5sb2cgZXJyXG5cblx0XHRyZW1vdmVBbGJ1bUZyb21GYXZvcml0ZTogKGlkKS0+XG5cdFx0XHRmYXZvcml0ZXMgPSBhcHAucmVxdWVzdCAnZ2V0OmZhdm9yaXRlOmFsYnVtcydcblx0XHRcdGFsYnVtID0gZmF2b3JpdGVzLmZpbmRXaGVyZVxuXHRcdFx0XHRpZDogaWRcblxuXHRcdFx0aWYgYWxidW1cblx0XHRcdFx0ZG8gYWxidW0uZGVzdHJveVxuXG5cdFx0XHRcdE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3coe21lc3NhZ2U6IFwi0JDQu9GM0LHQvtC8IMKrI3thbGJ1bS5nZXQgJ3RpdGxlJyB9wrsg0YPQtNCw0LvQtdC9INC40Lcg0LzQtdC00LjQsNGC0LXQutC4XCIsdHlwZTogJ3N1Y2Nlc3MnLHRpbWU6IDMwMDB9KVxuXG5cblx0XHRhZGRBbGJ1bVRvRmF2b3JpdGU6IChhbGJ1bSktPlxuXHRcdFx0ZmF2b3JpdGVzID0gYXBwLnJlcXVlc3QgJ2dldDpmYXZvcml0ZTphbGJ1bXMnXG5cdFx0XHRtb2RlbCA9IGFsYnVtLnRvSlNPTigpXG5cblx0XHRcdGlmICFtb2RlbC5mYXZvdXJpdGVzIG9yIG1vZGVsLmZhdm91cml0ZXMuc29ydCBpcyB1bmRlZmluZWRcblx0XHRcdFx0bW9kZWwuZmF2b3VyaXRlcyA9IHt9XG5cdFx0XHRcdG1vZGVsLmZhdm91cml0ZXMuc29ydCA9IDEwMDAwXG5cblx0XHRcdGZhdm9yaXRlcy5jcmVhdGUgbW9kZWxcblx0XHRcdGFwcC5HQVBhZ2VBY3Rpb24gJ9CU0L7QsdCw0LLQu9C10L3QuNC1INCw0LvRjNCx0L7QvNCwINCyINC40LfQsdGA0LDQvdC90L7QtScsIGFsYnVtLmdldCgndGl0bGUnKSsnIC0gJytsb2NhdGlvbi5ocmVmXG5cdFx0XHROb3RpZmljYXRpb24uQ29udHJvbGxlci5zaG93KHttZXNzYWdlOiBcItCQ0LvRjNCx0L7QvCDCqyN7YWxidW0uZ2V0KCd0aXRsZScpIH3CuyDQtNC+0LHQsNCy0LvQtdC9INCyINC80LXQtNC40LDRgtC10LrRg1wiLHR5cGU6ICdzdWNjZXNzJyx0aW1lOiAzMDAwfSlcblxuXHRcdG9wZW5PcHRpb25zOiAoaXRlbSktPlxuXHRcdFx0dmlldyA9IG5ldyBBbGJ1bS5PcHRpb25zXG5cdFx0XHRcdG1vZGVsOiBpdGVtLm1vZGVsXG5cdFx0XHRpZiBpdGVtLm9wdGlvbnNSZWdpb25cblx0XHRcdFx0aXRlbS5vcHRpb25zUmVnaW9uLnNob3cgdmlld1xuXG5cblx0QWxidW0uQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cblx0Xy5leHRlbmQgQWxidW0sIE1vZGVsXG5cblx0cmV0dXJuIEFsYnVtXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9hbGJ1bS9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuLi8uLi8uLi9wYXJ0cy9wbGF5ZXInLCcuLi8uLi8uLi9wYXJ0cy9zaW1wbGVfdmlld3MvdXBkYXRlJywnLi4vLi4vLi4vcGFydHMvc2ltcGxlX3ZpZXdzL2xvYWRlcicsJy4uL3RyYWNrJ10sIChQbGF5ZXIsVXBkYXRlLExvYWRlcixUcmFjayktPlxuXHRBbGJ1bSA9IHt9XG5cblx0Y2xhc3MgQWxidW0uSXRlbSBleHRlbmRzIE1hcmlvbmV0dGUuTGF5b3V0Vmlld1xuXHRcdHRlbXBsYXRlOiAnI2FsYnVtLWl0ZW0tdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ2FsYnVtLWl0ZW0nXG5cdFx0b25SZW5kZXI6IC0+XG5cdFx0XHRpbWcgPSBuZXcgSW1hZ2Vcblx0XHRcdCQoaW1nKS5vbiAnbG9hZC5pbWcnLCA9PlxuXG5cdFx0XHRcdCQoaW1nKS5hZGRDbGFzcyAnYWxidW0tYmcnXG5cdFx0XHRcdC5oaWRlKClcblx0XHRcdFx0LmFwcGVuZFRvIEAkICcuY29udHJvbGwnXG5cdFx0XHRcdC5mYWRlSW4gMzAwXG5cblxuXHRcdFx0XHQkKGltZykub2ZmICdsb2FkLmltZydcblx0XHRcdGxpbmsgPSBhcHAuY3JlYXRlSW1nTGluayhAbW9kZWwuZ2V0KCdpbWcnKSwnMzAweDMwMCcpXG5cdFx0XHRpbWcuc3JjID0gbGlua1xuXG5cdFx0cmVnaW9uczpcblx0XHRcdG9wdGlvbnNSZWdpb246ICcub3B0aW9ucydcblxuXHRcdGlkOiAtPlxuXHRcdFx0cmV0dXJuIEBtb2RlbC5nZXQgJ2lkJ1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAucGxheWVyLWNvbnRyb2xsJzogKGUpLT5cblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKVxuXHRcdFx0XHRpZiBAJGVsLmhhc0NsYXNzICdhY3RpdmUnXG5cdFx0XHRcdFx0aWYgQCRlbC5oYXNDbGFzcyAncGxheSdcblx0XHRcdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnBsYXlcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRkbyBQbGF5ZXIuQ29udHJvbGxlci5wYXVzZVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0QWxidW0uQ29udHJvbGxlci5QbGF5IEBtb2RlbFxuXG5cdFx0XHQnbW91c2VlbnRlcic6IC0+XG5cdFx0XHRcdGZhdm9yaXRlcyA9IGFwcC5yZXF1ZXN0ICdnZXQ6ZmF2b3JpdGU6YWxidW1zJ1xuXG5cdFx0XHRcdGlmIGZhdm9yaXRlcy5wcm9taXNlXG5cdFx0XHRcdFx0ZmF2b3JpdGVzLmRvbmUgKGNvbGxlY3Rpb24pPT5cblx0XHRcdFx0XHRcdGl0ZW0gPSBjb2xsZWN0aW9uLnNvbWUgKGl0ZW0pPT5cblx0XHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW0uZ2V0KCdpZCcpIGlzIEBtb2RlbC5nZXQoJ2lkJylcblx0XHRcdFx0XHRcdGlmIGl0ZW1cblx0XHRcdFx0XHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIHRydWVcblx0XHRcdFx0XHRcdFx0QCQoJy5tb3ZlJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgZmFsc2Vcblx0XHRcdFx0XHRcdFx0QCQoJy5tb3ZlJykucmVtb3ZlQ2xhc3MoJ2RlbGV0ZScpLmFkZENsYXNzKCdhZGQnKVxuXG5cdFx0XHRcdFx0ZmF2b3JpdGVzLmZhaWwgKGVyciktPlxuXHRcdFx0XHRcdFx0Y29uc29sZS5sb2cgYXJndW1lbnRzXG5cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdGl0ZW0gPSBmYXZvcml0ZXMuc29tZSAoaXRlbSk9PlxuXHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW0uZ2V0KCdpZCcpIGlzIEBtb2RlbC5nZXQoJ2lkJylcblx0XHRcdFx0XHRpZiBpdGVtXG5cdFx0XHRcdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZVxuXHRcdFx0XHRcdFx0QCQoJy5tb3ZlJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdEBtb2RlbC5zZXQgJ2luRmF2b3JpdGUnLCBmYWxzZVxuXHRcdFx0XHRcdFx0QCQoJy5tb3ZlJykucmVtb3ZlQ2xhc3MoJ2RlbGV0ZScpLmFkZENsYXNzKCdhZGQnKVxuXG5cdFx0XHQnY2xpY2sgLmNvbnRyb2xsJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YWxidW0nLCBAbW9kZWwuZ2V0KCdpZCcpLCBAbW9kZWxcblxuXHRcdFx0J2NsaWNrIC5pbmZvIC50aXRsZSc6IC0+XG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzaG93OmFsYnVtJywgQG1vZGVsLmdldCgnaWQnKSwgQG1vZGVsXG5cblx0XHRcdCdjbGljayAuaW5mbyAuYXJ0aXN0JzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6YXJ0aXN0JywgQG1vZGVsLmdldCAnYXJ0aXN0aWQnXG5cblx0XHRcdCdjbGljayAubW92ZSc6IChlKS0+XG5cdFx0XHRcdGRvIGUuc3RvcFByb3BhZ2F0aW9uXG5cblx0XHRcdCdjbGljayAub3B0aW9ucyc6IChlKS0+XG5cdFx0XHRcdGRvIGUuc3RvcFByb3BhZ2F0aW9uXG5cdFx0XHRcdEFsYnVtLkNvbnRyb2xsZXIub3Blbk9wdGlvbnMgQFxuXG5cdFx0XHQnY2xpY2sgLmFkZCc6IC0+XG5cdFx0XHRcdGRvIEBhZGRUb0FsYnVtXG5cblx0XHRcdCdjbGljayAuZGVsZXRlJzogLT5cblx0XHRcdFx0ZG8gQHJlbW92ZUZyb21BbGJ1bVxuXG5cdFx0XHQnY2xpY2sgLnRvLWFydGlzdCc6IC0+XG5cdFx0XHRcdGFwcC50cmlnZ2VyICdzaG93OmFydGlzdCcsIEBtb2RlbC5nZXQgJ2FydGlzdGlkJ1xuXG5cdFx0YWRkVG9BbGJ1bTogLT5cblx0XHRcdCQoJy5vcHRpb25zLXdyYXBwZXIgLmFkZCcpLnJlbW92ZUNsYXNzKCdhZGQnKS5hZGRDbGFzcygnZGVsZXRlJykudGV4dCgn0KPQtNCw0LvQuNGC0YwnKVxuXHRcdFx0QWxidW0uQ29udHJvbGxlci5hZGRBbGJ1bVRvRmF2b3JpdGUgQG1vZGVsXG5cdFx0XHRAbW9kZWwuc2V0ICdpbkZhdm9yaXRlJywgdHJ1ZVxuXHRcdFx0QCQoJy5tb3ZlJykucmVtb3ZlQ2xhc3MoJ2FkZCcpLmFkZENsYXNzKCdkZWxldGUnKVxuXHRcdFx0cmV0dXJuIGZhbHNlXG5cblx0XHRyZW1vdmVGcm9tQWxidW06IC0+XG5cdFx0XHQkKCcub3B0aW9ucy13cmFwcGVyIC5kZWxldGUnKS5yZW1vdmVDbGFzcygnZGVsZXRlJykuYWRkQ2xhc3MoJ2FkZCcpLnRleHQoJ9Ch0L7RhdGA0LDQvdC40YLRjCcpXG5cdFx0XHRBbGJ1bS5Db250cm9sbGVyLnJlbW92ZUFsYnVtRnJvbUZhdm9yaXRlIEBtb2RlbC5nZXQgJ2lkJ1xuXHRcdFx0QG1vZGVsLnNldCAnaW5GYXZvcml0ZScsIGZhbHNlXG5cdFx0XHRAJCgnLm1vdmUnKS5yZW1vdmVDbGFzcygnZGVsZXRlJykuYWRkQ2xhc3MoJ2FkZCcpXG5cdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRjbGFzcyBBbGJ1bS5JbmxpbmVJdGVtIGV4dGVuZHMgTWFyaW9uZXR0ZS5MYXlvdXRWaWV3XG5cdFx0dGVtcGxhdGU6ICcjaW5saW5lLWFsYnVtLXRwbCdcblx0XHRjbGFzc05hbWU6ICdhbGJ1bS1pdGVtJ1xuXHRcdHJlZ2lvbnM6XG5cdFx0XHRjb250ZW50OiAnLmJvZHkgLmFsYnVtcy10cmFja3MnXG5cdFx0ZXZlbnRzOlxuXHRcdFx0J2NsaWNrIC5ib2R5JzogKGUpLT5cblx0XHRcdFx0ZS5pc1Byb3BhZ2F0aW9uU3RvcHBlZCgpXG5cblx0XHRcdCdjbGljayAuc2h1ZmZsZS10cmFja3MnOiAtPlxuXHRcdFx0XHR0cmFja0NvbnRhaW5lciA9IEAkKCcudHJhY2tzLXdyYXBwZXInKVxuXHRcdFx0XHR0cmFja3MgPSB0cmFja0NvbnRhaW5lci5maW5kKCcudHJhY2staXRlbScpLm5vdCgnLmRpc2FibGUnKVxuXHRcdFx0XHR0cmFja3NMZW5ndGggPSB0cmFja3MubGVuZ3RoXG5cblx0XHRcdFx0cmFuZG9tID0gYXBwLlJhbmRJbnQoMCx0cmFja3NMZW5ndGgtMSlcblxuXHRcdFx0XHR0cmFja3MuZXEocmFuZG9tKS5jbGljaygpXG5cblx0XHRcdCdjbGljayc6ICdzaG93QWxidW1Db250ZW50J1xuXG5cdFx0c2hvd0FsYnVtQ29udGVudDogLT5cblx0XHRcdGFsYnVtID0gQFxuXG5cdFx0XHRvcGVuQWxidW0gPSAoYWxidW0sdHJhY2tzKS0+XG5cdFx0XHRcdGFsYnVtLiRlbC5yZW1vdmVDbGFzcygnZGlzYWJsZScpLmFkZENsYXNzKCdvcGVuJylcblxuXHRcdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24gdHJhY2tzXG5cblx0XHRcdFx0Y29sbGVjdGlvbi5lYWNoIChpdGVtKS0+XG5cdFx0XHRcdFx0aXRlbS5zZXQgJ3VuaXEnLCBhbGJ1bS5tb2RlbC5nZXQoJ2lkJykraXRlbS5pZFxuXHRcdFx0XHRcdGl0ZW0uc2V0ICdvcHRpb25zTGlzdCcsIFtcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy5hZGRUUGxcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdhZGQtdG8tcGxheWxpc3QnXG5cdFx0XHRcdFx0XHR9LHtcblx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy50b0FsYlxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hbGJ1bSdcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRdXG5cblx0XHRcdFx0dHJhY2tzVmlldyA9IG5ldyBUcmFjay5MaXN0Vmlld1xuXHRcdFx0XHRcdGNvbGxlY3Rpb246IGNvbGxlY3Rpb25cblxuXHRcdFx0XHRhbGJ1bS5jb250ZW50LnNob3cgdHJhY2tzVmlld1xuXHRcdFx0XHRkbyBhcHAuY2hlY2tBY3RpdmVUcmFja1xuXG5cdFx0XHRpZiAhYWxidW0uJGVsLmhhc0NsYXNzICdkaXNhYmxlJ1xuXG5cdFx0XHRcdGlmIGFsYnVtLiRlbC5oYXNDbGFzcyAnb3Blbidcblx0XHRcdFx0XHRkbyBhbGJ1bS5jb250ZW50LmVtcHR5XG5cdFx0XHRcdFx0YWxidW0uJGVsLnJlbW92ZUNsYXNzICdvcGVuJ1xuXHRcdFx0XHRcdHJldHVyblxuXG5cdFx0XHRcdGFsYnVtLiRlbC5hZGRDbGFzcyAnZGlzYWJsZSdcblx0XHRcdFx0bG9hZGVyID0gbmV3IExvYWRlci5WaWV3XG5cdFx0XHRcdFx0c3R5bGVzOlxuXHRcdFx0XHRcdFx0J21hcmdpbic6ICcyMHB4IGF1dG8nXG5cdFx0XHRcdGFsYnVtLmNvbnRlbnQuc2hvdyBsb2FkZXJcblxuXHRcdFx0XHRpZiBhbGJ1bS5tb2RlbC5oYXMgJ3RyYWNrcydcblx0XHRcdFx0XHRvcGVuQWxidW0gYWxidW0sYWxidW0ubW9kZWwuZ2V0ICd0cmFja3MnXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRmZXRjaFRyYWNrcyA9IGFsYnVtLm1vZGVsLmZldGNoKClcblxuXHRcdFx0XHRcdGZldGNoVHJhY2tzLmRvbmUgLT5cblx0XHRcdFx0XHRcdG9wZW5BbGJ1bSBhbGJ1bSwgYWxidW0ubW9kZWwuZ2V0ICd0cmFja3MnXG5cblx0XHRcdFx0XHRmZXRjaFRyYWNrcy5mYWlsIC0+XG5cdFx0XHRcdFx0XHRhbGJ1bS4kZWwucmVtb3ZlQ2xhc3MoJ2Rpc2FibGUnKS5hZGRDbGFzcygnb3BlbicpXG5cdFx0XHRcdFx0XHRjYWxsYmFjayA9IC0+XG5cdFx0XHRcdFx0XHRcdHNob3dBbGJ1bUNvbnRlbnQgYWxidW1cblxuXHRcdFx0XHRcdFx0dXBkYXRlID0gbmV3IFVwZGF0ZS5WaWV3XG5cdFx0XHRcdFx0XHRcdHN0eWxlczpcblx0XHRcdFx0XHRcdFx0XHRtYXJnaW46ICcyMHB4IGF1dG8nXG5cdFx0XHRcdFx0XHRcdGNhbGxiYWNrOiBjYWxsYmFja1xuXG5cdFx0XHRcdFx0XHRhbGJ1bS5jb250ZW50LnNob3cgdXBkYXRlXG5cblx0Y2xhc3MgQWxidW0uT3B0aW9ucyBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogJyNhbGJ1bS1pdGVtLW9wdGlvbnMtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ29wdGlvbnMtd3JhcHBlcidcblx0XHRldmVudHM6XG5cdFx0XHQnbW91c2VsZWF2ZSc6ICdjbG9zZU9wdGlvbnMnXG5cdFx0XHQnY2xpY2sgLm9wdGlvbnMtb3ZlcmxheSc6ICdjbG9zZU9wdGlvbnMnXG5cblx0XHRjbG9zZU9wdGlvbnM6IChlKS0+XG5cdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpXG5cdFx0XHRkbyBAZGVzdHJveVxuXG5cdGNsYXNzIEFsYnVtLkxpc3RWaWV3IGV4dGVuZHMgTWFyaW9uZXR0ZS5Db2xsZWN0aW9uVmlld1xuXHRcdGNoaWxkVmlldzogQWxidW0uSXRlbVxuXHRcdGNsYXNzTmFtZTogJ2FsYnVtcy13cmFwcGVyJ1xuXHRcdG9uQmVmb3JlUmVuZGVyQ29sbGVjdGlvbjogLT5cblx0XHRcdEBhdHRhY2hIdG1sID0gKGNvbGxlY3Rpb25WaWV3LCBpdGVtVmlldywgaW5kZXgpLT5cblx0XHRcdFx0Y29sbGVjdGlvblZpZXcuJGVsLmFwcGVuZCBpdGVtVmlldy5lbFxuXHRcdFx0XHQuYXBwZW5kICcgJ1xuXG5cdGNsYXNzIEFsYnVtLklubGluZUxpc3RWaWV3IGV4dGVuZHMgTWFyaW9uZXR0ZS5Db2xsZWN0aW9uVmlld1xuXHRcdGNoaWxkVmlldzogQWxidW0uSW5saW5lSXRlbVxuXHRcdGNsYXNzTmFtZTogJ2lubGluZS1hbGJ1bXMtd3JhcHBlcidcblx0XHRvblJlbmRlcjogLT5cblx0XHRcdGlmIEBjb2xsZWN0aW9uLmxlbmd0aCBpc250IDBcblx0XHRcdFx0JCgnLmFydGlzdC1sYXlvdXQtdGl0bGUuYWxidW1zLXRpdGxlJykuc2hvdygpXG5cblx0cmV0dXJuIEFsYnVtXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9hbGJ1bS92aWV3LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuXHRyZXF1aXJlKCcuL3N0eWxlLnN0eWwnKTtcblx0VXBkYXRlID0ge31cblxuXHRjbGFzcyBVcGRhdGUuVmlldyBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHR0ZW1wbGF0ZTogZmFsc2Vcblx0XHRjbGFzc05hbWU6IC0+XG5cdFx0XHRyZXR1cm4gaWYgQG9wdGlvbnMuY29udGFpbmVyIHRoZW4gQG9wdGlvbnMuY29udGFpbmVyIGVsc2UgJ3VwZGF0ZS1lbGVtZW50J1xuXG5cdFx0b25SZW5kZXI6IC0+XG5cdFx0XHRAb3B0aW9ucy5zdHlsZXMgPSBAb3B0aW9ucy5zdHlsZXMgb3Ige31cblx0XHRcdGlmIEBvcHRpb25zLmNvbnRhaW5lclxuXHRcdFx0XHRjb250YWluZXIgPSAkKCc8ZGl2IGNsYXNzPVwidXBkYXRlLWVsZW1lbnRcIj48L2Rpdj4nKVxuXHRcdFx0XHRAJGVsLmFwcGVuZCBjb250YWluZXJcblxuXHRcdFx0aWYgQG9wdGlvbnMuY29udGFpbmVyXG5cdFx0XHRcdEAkKCcudXBkYXRlLWVsZW1lbnQnKS5jc3MgQG9wdGlvbnMuc3R5bGVzXG5cdFx0XHRlbHNlXG5cdFx0XHRcdEAkZWwuY3NzIEBvcHRpb25zLnN0eWxlc1xuXG5cblx0XHRldmVudHM6XG5cdFx0XHQnY2xpY2snOiAtPlxuXHRcdFx0XHRkbyBAb3B0aW9ucy5jYWxsYmFja1xuXG5cdHJldHVybiBVcGRhdGVcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaW1wbGVfdmlld3MvdXBkYXRlL2luZGV4LmNvZmZlZVxuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Zyb250ZW5kL2FwcC9wYXJ0cy9zaW1wbGVfdmlld3MvdXBkYXRlL3N0eWxlLnN0eWxcbiAqKiBtb2R1bGUgaWQgPSAyOVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiZGVmaW5lIC0+XG5cdHJlcXVpcmUoJy4vc3R5bGUuc3R5bCcpXG5cdExvYWRlciA9IHt9XG5cblx0Y2xhc3MgTG9hZGVyLlZpZXcgZXh0ZW5kcyBNYXJpb25ldHRlLkl0ZW1WaWV3XG5cdFx0dGVtcGxhdGU6IGZhbHNlXG5cdFx0Y2xhc3NOYW1lOiAtPlxuXHRcdFx0cmV0dXJuIGlmIEBvcHRpb25zLmNvbnRhaW5lciB0aGVuIEBvcHRpb25zLmNvbnRhaW5lciBlbHNlICdsb2FkZXInXG5cblx0XHRvblJlbmRlcjogLT5cblx0XHRcdEBvcHRpb25zLnN0eWxlcyA9IEBvcHRpb25zLnN0eWxlcyBvciB7fVxuXHRcdFx0aWYgQG9wdGlvbnMuY29udGFpbmVyXG5cdFx0XHRcdGNvbnRhaW5lciA9ICQoJzxkaXYgY2xhc3M9XCJsb2FkZXJcIj48L2Rpdj4nKVxuXHRcdFx0XHRAJGVsLmFwcGVuZCBjb250YWluZXJcblxuXHRcdFx0aWYgQG9wdGlvbnMuY29udGFpbmVyXG5cdFx0XHRcdEAkKCcubG9hZGVyJykuY3NzIEBvcHRpb25zLnN0eWxlc1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHRAJGVsLmNzcyBAb3B0aW9ucy5zdHlsZXNcblxuXHRyZXR1cm4gTG9hZGVyXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvc2ltcGxlX3ZpZXdzL2xvYWRlci9pbmRleC5jb2ZmZWVcbiAqKi8iLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvc2ltcGxlX3ZpZXdzL2xvYWRlci9zdHlsZS5zdHlsXG4gKiogbW9kdWxlIGlkID0gMzFcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsImRlZmluZSAtPlxuICBBbGJ1bSA9IHt9XG5cbiAgY2xhc3MgQWxidW0uTW9kZWwgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuICAgIHVybFJvb3Q6IC0+XG4gICAgICByZXR1cm4gYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9jYXRhbG9ndWUvZ2V0QWxidW1cIixcIj9hbGJ1bWlkPSN7QGlkfVwiKVxuICAgICAgXG4gICAgcGFyc2U6IChtb2RlbCktPlxuICAgICAgaWYgbW9kZWwuYWxidW1cbiAgICAgICAgbW9kZWwuYWxidW0uYWxidW0gPSB0cnVlXG4gICAgICAgIHJldHVybiBtb2RlbC5hbGJ1bVxuICAgICAgZWxzZVxuICAgICAgICBtb2RlbC5hbGJ1bSA9IHRydWVcbiAgICAgICAgcmV0dXJuIG1vZGVsXG4gICAgZGVmYXVsdHM6XG4gICAgICBcImR1cmF0aW9uXCI6IFwiXCJcbiAgICAgIFwiYXJ0aXN0XCI6IFwiXCJcbiAgICAgIFwidGl0bGVcIjogXCJcIlxuICAgICAgXCJ0cmFja19jb3VudFwiOiBcIlwiXG4gICAgICBcImltZ1wiOiBcIlwiXG5cbiAgY2xhc3MgQWxidW0uQ29sbGVjdGlvbiBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb25cbiAgICBtb2RlbDogQWxidW0uTW9kZWxcblxuICByZXR1cm4gQWxidW1cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FsYnVtL21vZGVsLmNvZmZlZVxuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FsYnVtL3N0eWxlLnN0eWxcbiAqKiBtb2R1bGUgaWQgPSAzM1xuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiZGVmaW5lIFsnLi92aWV3LmNvZmZlZSddLCAoVmlldyktPlxuICAgIHJlcXVpcmUoJy4vc3R5bGUuc3R5bCcpXG4gICAgY2xhc3MgY29udHJvbGxlciBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXG5cblxuICAgIFZpZXcuQ29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyXG5cbiAgICByZXR1cm4gVmlld1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYXJ0aXN0L2luZGV4LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuICAgIEFydGlzdCA9IHt9XG4gICAgY2xhc3MgQXJ0aXN0Lkl0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkxheW91dFZpZXdcbiAgICAgICAgZXZlbnRzOlxuICAgICAgICAgICAgJ2NsaWNrJzogLT5cbiAgICAgICAgICAgICAgICBhcHAudHJpZ2dlciAnc2hvdzphcnRpc3QnLCBAbW9kZWwuZ2V0ICdpZCdcblxuICAgIHJldHVybiBBcnRpc3RcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FydGlzdC92aWV3LmNvZmZlZVxuICoqLyIsIi8vIHJlbW92ZWQgYnkgZXh0cmFjdC10ZXh0LXdlYnBhY2stcGx1Z2luXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL2FydGlzdC9zdHlsZS5zdHlsXG4gKiogbW9kdWxlIGlkID0gMzZcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsImRlZmluZSBbJy4vdmlldy5jb2ZmZWUnLCcuL21vZGVsLmNvZmZlZScsJy4uLy4uLy4uL3BhcnRzL3BsYXllcicsJy4uLy4uLy4uL3BhcnRzL25vdGlmaWNhdGlvbiddLCAoUGxheWxpc3QsTW9kZWwsUGxheWVyLE5vdGlmaWNhdGlvbiktPlxuXHRyZXF1aXJlKCcuL3N0eWxlLnN0eWwnKVxuXHRcblx0Y2xhc3MgY29udHJvbGxlciBleHRlbmRzIE1hcmlvbmV0dGUuQ29udHJvbGxlclxuXHRcdFBsYXk6IChwbGF5bGlzdE1vZGVsKS0+XG5cdFx0XHRhcHAucGxheWVkQWxidW0gPSAncGxheWxpc3QtJytwbGF5bGlzdE1vZGVsLmdldCAnaWQnXG5cblx0XHRcdGlmIHBsYXlsaXN0TW9kZWwuaGFzICd0cmFja3MnXG5cdFx0XHRcdGlmIHBsYXlsaXN0TW9kZWwuZ2V0KCd0cmFja3MnKS5sZW5ndGggaXMgMFxuXHRcdFx0XHRcdE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3coe21lc3NhZ2U6ICfQn9C70LXQudC70LjRgdGCINC/0YPRgdGC0L7QuScsdHlwZTogJ2Vycm9yJyx0aW1lOiAzMDAwfSlcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2VcblxuXHRcdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24gcGxheWxpc3RNb2RlbC5nZXQgJ3RyYWNrcydcblx0XHRcdFx0Y29sbGVjdGlvbi5lYWNoIChpdGVtKS0+XG5cdFx0XHRcdFx0aXRlbS5zZXQgJ3VuaXEnLCAnYWxidW0nK2l0ZW0uaWRcblx0XHRcdFx0XHRpdGVtLnNldCAnb3B0aW9uc0xpc3QnLCBbXG5cdFx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMuYWRkVFBsXG5cdFx0XHRcdFx0XHRcdGNsYXNzOiAnYWRkLXRvLXBsYXlsaXN0J1xuXHRcdFx0XHRcdFx0fSx7XG5cdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BcnRcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdvcHRpb25zLW9wZW4tYXJ0aXN0J1xuXHRcdFx0XHRcdFx0fSx7XG5cdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BbGJcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdvcHRpb25zLW9wZW4tYWxidW0nXG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0XVxuXHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5wbGF5QnlDb2xsZWN0aW9uIGNvbGxlY3Rpb25cblx0XHRcdGVsc2Vcblx0XHRcdFx0cGxheWxpc3RUcmFja3MgPSBkbyBwbGF5bGlzdE1vZGVsLmZldGNoXG5cblx0XHRcdFx0cGxheWxpc3RUcmFja3MuZG9uZSAtPlxuXHRcdFx0XHRcdGNvbGxlY3Rpb24gPSBuZXcgQmFja2JvbmUuQ29sbGVjdGlvbiBwbGF5bGlzdE1vZGVsLmdldCAndHJhY2tzJ1xuXHRcdFx0XHRcdGNvbGxlY3Rpb24uZWFjaCAoaXRlbSktPlxuXHRcdFx0XHRcdFx0aXRlbS5zZXQgJ3VuaXEnLCAnYWxidW0nK2l0ZW0uaWRcblx0XHRcdFx0XHRcdGl0ZW0uc2V0ICdvcHRpb25zTGlzdCcsIFtcblx0XHRcdFx0XHRcdFx0e1xuXHRcdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMuYWRkVFBsXG5cdFx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdhZGQtdG8tcGxheWxpc3QnXG5cdFx0XHRcdFx0XHRcdH0se1xuXHRcdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BcnRcblx0XHRcdFx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hcnRpc3QnXG5cdFx0XHRcdFx0XHRcdH0se1xuXHRcdFx0XHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BbGJcblx0XHRcdFx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hbGJ1bSdcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0XVxuXG5cdFx0XHRcdFx0aHJlZiA9IEJhY2tib25lLmhpc3RvcnkubG9jYXRpb24ub3JpZ2luKycvJytCYWNrYm9uZS5oaXN0b3J5LmxvY2F0aW9uLnBhdGhuYW1lLnNwbGl0KCcvJylbMV0rJy9wbGF5bGlzdC8nK3BsYXlsaXN0TW9kZWwuZ2V0ICdpZCdcblx0XHRcdFx0XHRQbGF5ZXIuQ29udHJvbGxlci5wbGF5QnlDb2xsZWN0aW9uIGNvbGxlY3Rpb24sbnVsbCxocmVmXG5cblx0XHRcdFx0cGxheWxpc3RUcmFja3MuZmFpbCAoZXJyKS0+XG5cdFx0XHRcdFx0Y29uc29sZS5sb2cgZXJyXG5cblxuXHRQbGF5bGlzdC5Db250cm9sbGVyID0gbmV3IGNvbnRyb2xsZXJcblxuXHRfLmV4dGVuZCBQbGF5bGlzdCwgTW9kZWxcblx0cmV0dXJuIFBsYXlsaXN0XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvY29uZmlnL2N1c3RvbV9lbGVtZW50cy9wbGF5bGlzdC9pbmRleC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgWycuLi8uLi8uLi9wYXJ0cy9wbGF5ZXInLCcuLi8uLi8uLi9wYXJ0cy9zaW1wbGVfdmlld3MvbG9hZGVyJywnLi4vLi4vLi4vcGFydHMvc2ltcGxlX3ZpZXdzL3VwZGF0ZScsJy4uL3RyYWNrJ10sIChQbGF5ZXIsTG9hZGVyLFVwZGF0ZSxUcmFjayktPlxuXHRQbGF5bGlzdCA9IHt9XG5cblx0Y2xhc3MgUGxheWxpc3QuSXRlbSBleHRlbmRzIE1hcmlvbmV0dGUuSXRlbVZpZXdcblx0XHRpZDogLT5cblx0XHRcdHJldHVybiAncGxheWxpc3QtJytAbW9kZWwuZ2V0ICdpZCdcblxuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAucGxheWVyLWNvbnRyb2xsJzogKGUpLT5cblx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKVxuXHRcdFx0XHRpZiBAJGVsLmhhc0NsYXNzICdhY3RpdmUnXG5cdFx0XHRcdFx0aWYgQCRlbC5oYXNDbGFzcyAncGxheSdcblx0XHRcdFx0XHRcdGRvIFBsYXllci5Db250cm9sbGVyLnBsYXlcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRkbyBQbGF5ZXIuQ29udHJvbGxlci5wYXVzZVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0UGxheWxpc3QuQ29udHJvbGxlci5QbGF5IEBtb2RlbFxuXG5cdFx0XHQnY2xpY2sgLmNvbnRyb2xsJzogLT5cblx0XHRcdFx0YXBwLnRyaWdnZXIgJ3Nob3c6cGxheWxpc3QnLCBAbW9kZWwuZ2V0KCdpZCcpLCBAbW9kZWxcblxuXHRcdFx0J2NsaWNrIC5pbmZvIC50aXRsZSc6IC0+XG4jXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphbGJ1bScsIEBtb2RlbC5nZXQoJ2lkJyksIEBtb2RlbFxuXG5cdFx0XHQnY2xpY2sgLmluZm8gLmFydGlzdCc6IC0+XG4jXHRcdFx0XHRhcHAudHJpZ2dlciAnc2hvdzphcnRpc3QnLCBAbW9kZWwuZ2V0ICdhcnRpc3RpZCdcblxuXG5cdGNsYXNzIFBsYXlsaXN0LklubGluZUl0ZW0gZXh0ZW5kcyBNYXJpb25ldHRlLkxheW91dFZpZXdcblx0XHR0ZW1wbGF0ZTogJyNpbmxpbmUtcGxheWxpc3QtdHBsJ1xuXHRcdGNsYXNzTmFtZTogJ3BsYXlsaXN0LWl0ZW0nXG5cdFx0cmVnaW9uczpcblx0XHRcdGNvbnRlbnQ6ICcuYm9keSAucGxheWxpc3QtdHJhY2tzJ1xuXHRcdGV2ZW50czpcblx0XHRcdCdjbGljayAuYm9keSc6IChlKS0+XG5cdFx0XHRcdGUuaXNQcm9wYWdhdGlvblN0b3BwZWQoKVxuXG5cdFx0XHQnY2xpY2sgLnNodWZmbGUtdHJhY2tzJzogLT5cblx0XHRcdFx0dHJhY2tDb250YWluZXIgPSBAJCgnLnRyYWNrcy13cmFwcGVyJylcblx0XHRcdFx0dHJhY2tzID0gdHJhY2tDb250YWluZXIuZmluZCgnLnRyYWNrLWl0ZW0nKS5ub3QoJy5kaXNhYmxlJylcblx0XHRcdFx0dHJhY2tzTGVuZ3RoID0gdHJhY2tzLmxlbmd0aFxuXG5cdFx0XHRcdHJhbmRvbSA9IGFwcC5SYW5kSW50KDAsdHJhY2tzTGVuZ3RoLTEpXG5cblx0XHRcdFx0dHJhY2tzLmVxKHJhbmRvbSkuY2xpY2soKVxuXG5cdFx0XHQnY2xpY2snOiAnc2hvd0NvbnRlbnQnXG5cblx0XHRzaG93Q29udGVudDogLT5cblx0XHRcdHBsYXlsaXN0ID0gQFxuXG5cdFx0XHRvcGVuUGxheWxpc3QgPSAocGxheWxpc3QsdHJhY2tzKS0+XG5cdFx0XHRcdHBsYXlsaXN0LiRlbC5yZW1vdmVDbGFzcygnZGlzYWJsZScpLmFkZENsYXNzKCdvcGVuJylcblxuXHRcdFx0XHRjb2xsZWN0aW9uID0gbmV3IEJhY2tib25lLkNvbGxlY3Rpb24gdHJhY2tzXG5cblx0XHRcdFx0Y29sbGVjdGlvbi5lYWNoIChpdGVtKS0+XG5cdFx0XHRcdFx0aXRlbS5zZXQgJ3VuaXEnLCBwbGF5bGlzdC5tb2RlbC5nZXQoJ2lkJykraXRlbS5pZFxuXHRcdFx0XHRcdGl0ZW0uc2V0ICdvcHRpb25zTGlzdCcsIFtcblx0XHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy5hZGRUUGxcblx0XHRcdFx0XHRcdFx0Y2xhc3M6ICdhZGQtdG8tcGxheWxpc3QnXG5cdFx0XHRcdFx0XHR9LHtcblx0XHRcdFx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUub3B0aW9ucy50b0FsYlxuXHRcdFx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hbGJ1bSdcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRdXG5cblx0XHRcdFx0dHJhY2tzVmlldyA9IG5ldyBUcmFjay5MaXN0Vmlld1xuXHRcdFx0XHRcdGNvbGxlY3Rpb246IGNvbGxlY3Rpb25cblxuXHRcdFx0XHRwbGF5bGlzdC5jb250ZW50LnNob3cgdHJhY2tzVmlld1xuXHRcdFx0XHRkbyBhcHAuY2hlY2tBY3RpdmVUcmFja1xuXG5cdFx0XHRpZiAhcGxheWxpc3QuJGVsLmhhc0NsYXNzICdkaXNhYmxlJ1xuXG5cdFx0XHRcdGlmIHBsYXlsaXN0LiRlbC5oYXNDbGFzcyAnb3Blbidcblx0XHRcdFx0XHRkbyBwbGF5bGlzdC5jb250ZW50LmVtcHR5XG5cdFx0XHRcdFx0cGxheWxpc3QuJGVsLnJlbW92ZUNsYXNzICdvcGVuJ1xuXHRcdFx0XHRcdHJldHVyblxuXG5cdFx0XHRcdHBsYXlsaXN0LiRlbC5hZGRDbGFzcyAnZGlzYWJsZSdcblx0XHRcdFx0bG9hZGVyID0gbmV3IExvYWRlci5WaWV3XG5cdFx0XHRcdFx0c3R5bGVzOlxuXHRcdFx0XHRcdFx0J21hcmdpbic6ICcyMHB4IGF1dG8nXG5cdFx0XHRcdHBsYXlsaXN0LmNvbnRlbnQuc2hvdyBsb2FkZXJcblxuXHRcdFx0XHRpZiBwbGF5bGlzdC5tb2RlbC5oYXMgJ3RyYWNrcydcblx0XHRcdFx0XHRvcGVuUGxheWxpc3QgcGxheWxpc3QscGxheWxpc3QubW9kZWwuZ2V0ICd0cmFja3MnXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRmZXRjaFRyYWNrcyA9IHBsYXlsaXN0Lm1vZGVsLmZldGNoKClcblxuXHRcdFx0XHRcdGZldGNoVHJhY2tzLmRvbmUgLT5cblx0XHRcdFx0XHRcdG9wZW5QbGF5bGlzdCBwbGF5bGlzdCwgcGxheWxpc3QubW9kZWwuZ2V0ICd0cmFja3MnXG5cblx0XHRcdFx0XHRmZXRjaFRyYWNrcy5mYWlsIC0+XG5cdFx0XHRcdFx0XHRwbGF5bGlzdC4kZWwucmVtb3ZlQ2xhc3MoJ2Rpc2FibGUnKS5hZGRDbGFzcygnb3BlbicpXG5cdFx0XHRcdFx0XHRjYWxsYmFjayA9IC0+XG5cdFx0XHRcdFx0XHRcdHNob3dBbGJ1bUNvbnRlbnQgcGxheWxpc3RcblxuXHRcdFx0XHRcdFx0dXBkYXRlID0gbmV3IFVwZGF0ZS5WaWV3XG5cdFx0XHRcdFx0XHRcdHN0eWxlczpcblx0XHRcdFx0XHRcdFx0XHRtYXJnaW46ICcyMHB4IGF1dG8nXG5cdFx0XHRcdFx0XHRcdGNhbGxiYWNrOiBjYWxsYmFja1xuXG5cdFx0XHRcdFx0XHRwbGF5bGlzdC5jb250ZW50LnNob3cgdXBkYXRlXG5cblxuXHRjbGFzcyBQbGF5bGlzdC5MaXN0VmlldyBleHRlbmRzIE1hcmlvbmV0dGUuQ29sbGVjdGlvblZpZXdcblx0XHRjaGlsZFZpZXc6IFBsYXlsaXN0Lkl0ZW1cblxuXG5cdGNsYXNzIFBsYXlsaXN0LklubGluZUxpc3RWaWV3IGV4dGVuZHMgTWFyaW9uZXR0ZS5Db2xsZWN0aW9uVmlld1xuXHRcdGNoaWxkVmlldzogUGxheWxpc3QuSW5saW5lSXRlbVxuXHRcdGNsYXNzTmFtZTogJ2lubGluZS1wbGF5bGlzdHMtd3JhcHBlcidcblxuXHRyZXR1cm4gUGxheWxpc3RcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3BsYXlsaXN0L3ZpZXcuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG4gICAgUGxheWxpc3QgPSB7fVxuXG4gICAgY2xhc3MgUGxheWxpc3QuTW9kZWwgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuICAgICAgICB1cmxSb290OiAtPlxuICAgICAgICAgICAgYXBwLmdldFJlcXVlc3RVcmwoXCIvcGxheWxpc3QvZ2V0P3BsYXlsaXN0aWQ9I3tAaWR9Jml0ZW1zX29uX3BhZ2U9MTAwMFwiKVxuICAgICAgICBkZWZhdWx0czpcbiAgICAgICAgICBcImR1cmF0aW9uXCI6IFwiXCJcbiAgICAgICAgICBcImFydGlzdFwiOiBcIlwiXG4gICAgICAgICAgXCJ0aXRsZVwiOiBcIlwiXG5cbiAgICBjbGFzcyBQbGF5bGlzdC5Db2xsZWN0aW9uIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuICAgICAgICBtb2RlbDogUGxheWxpc3QuTW9kZWxcblxuICAgIHJldHVybiBQbGF5bGlzdFxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvcGxheWxpc3QvbW9kZWwuY29mZmVlXG4gKiovIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvcGxheWxpc3Qvc3R5bGUuc3R5bFxuICoqIG1vZHVsZSBpZCA9IDQwXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCJ2YXIgamFkZSA9IHJlcXVpcmUoXCIvVXNlcnMvZW1pbC93b3JrX3Byb2plY3Qvbm9kZS5tdXNpYy9ub2RlX21vZHVsZXMvamFkZS9saWIvcnVudGltZS5qc1wiKTtcblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiB0ZW1wbGF0ZShsb2NhbHMpIHtcbnZhciBidWYgPSBbXTtcbnZhciBqYWRlX21peGlucyA9IHt9O1xudmFyIGphZGVfaW50ZXJwO1xuO3ZhciBsb2NhbHNfZm9yX3dpdGggPSAobG9jYWxzIHx8IHt9KTsoZnVuY3Rpb24gKGFwcCkge1xuYnVmLnB1c2goXCI8ZGl2IGNsYXNzPVxcXCJwcm9maWxlXFxcIj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJzaWRlYmFyLWJvZHlcXFwiPjxkaXYgY2xhc3M9XFxcInNlYXJjaFxcXCI+PGlucHV0IHR5cGU9XFxcInRleHRcXFwiXCIgKyAoamFkZS5hdHRyKFwicGxhY2Vob2xkZXJcIiwgXCJcIiArIChhcHAubG9jYWxlLnNlYXJjaC5zZWFyY2gpICsgXCJcIiwgdHJ1ZSwgdHJ1ZSkpICsgXCI+PGkgY2xhc3M9XFxcImljb25cXFwiPjwvaT48aSBjbGFzcz1cXFwiY2xvc2VcXFwiPtC+0YLQvNC10L3QsDwvaT48L2Rpdj48ZGl2IGNsYXNzPVxcXCJzaWRlYmFyLWNvbnRlbnQtc2Nyb2xsXFxcIj48ZGl2IGNsYXNzPVxcXCJzaWRlYmFyLXNlYXJjaC1jb250YWluZXJcXFwiPjwvZGl2PjxkaXYgY2xhc3M9XFxcInNpZGViYXItY29udGVudC13cmFwcFxcXCI+PGRpdiBjbGFzcz1cXFwibWFpbi1uYXZcXFwiPjwvZGl2PjxkaXYgY2xhc3M9XFxcInVzZXItbmF2XFxcIj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJhcHBzXFxcIj48cCBjbGFzcz1cXFwidGl0bGVcXFwiPlwiICsgKGphZGUuZXNjYXBlKChqYWRlX2ludGVycCA9IGFwcC5sb2NhbGUubWVudS5hcHBzKSA9PSBudWxsID8gJycgOiBqYWRlX2ludGVycCkpICsgXCI8L3A+PHVsPjxsaSBjbGFzcz1cXFwiYXBwLWxpbmtcXFwiPjxhIHRhcmdldD1cXFwiX2JsYW5rXFxcIiBocmVmPVxcXCJodHRwczovL2l0dW5lcy5hcHBsZS5jb20vcnUvYXBwL2JpbGFqbi5tdXp5a2EvaWQ3MDI3NzAyNjg/bXQ9OFxcXCI+SU9TPC9hPjwvbGk+PGxpIGNsYXNzPVxcXCJhcHAtbGlua1xcXCI+PGEgdGFyZ2V0PVxcXCJfYmxhbmtcXFwiIGhyZWY9XFxcImh0dHBzOi8vcGxheS5nb29nbGUuY29tL3N0b3JlL2FwcHMvZGV0YWlscz9pZD1ydS5iZWVvbmxpbmUubXVzaWMmYW1wO2hsPXJ1XFxcIj5BbmRyb2lkPC9hPjwvbGk+PC91bD48L2Rpdj48L2Rpdj48L2Rpdj48L2Rpdj48ZGl2IGNsYXNzPVxcXCJzaWRlYmFyLW92ZXJsYXlcXFwiPjwvZGl2PlwiKTt9LmNhbGwodGhpcyxcImFwcFwiIGluIGxvY2Fsc19mb3Jfd2l0aD9sb2NhbHNfZm9yX3dpdGguYXBwOnR5cGVvZiBhcHAhPT1cInVuZGVmaW5lZFwiP2FwcDp1bmRlZmluZWQpKTs7cmV0dXJuIGJ1Zi5qb2luKFwiXCIpO1xufVxuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogLi9mcm9udGVuZC9hcHAvcGFydHMvc2lkZWJhci90ZW1wbGF5dHMvbGF5b3V0LXRlbXBsYXRlLmphZGVcbiAqKiBtb2R1bGUgaWQgPSA0MVxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiLy8gcmVtb3ZlZCBieSBleHRyYWN0LXRleHQtd2VicGFjay1wbHVnaW5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL3BhcnRzL3NpZGViYXIvc3R5bGUuc3R5bFxuICoqIG1vZHVsZSBpZCA9IDQyXG4gKiogbW9kdWxlIGNodW5rcyA9IDBcbiAqKi8iLCJkZWZpbmUgWycuLi9jb25maWcvY3VzdG9tX2VsZW1lbnRzL3RyYWNrJ10sIChDdXN0b21UcmFjayktPlxuXHRUcmFja0xpc3QgPSB7fVxuXHRjbGFzcyBUcmFja0xpc3QuSXRlbSBleHRlbmRzIEN1c3RvbVRyYWNrLk1vZGVsXG5cdFx0cGFyc2U6IChpdGVtKS0+XG5cdFx0XHRpdGVtLnVuaXEgPSAnYXJ0aXN0LScraXRlbS5pZFxuXHRcdFx0aXRlbS5vcHRpb25zTGlzdCA9IFtcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMuYWRkVFBsXG5cdFx0XHRcdFx0Y2xhc3M6ICdhZGQtdG8tcGxheWxpc3QnXG5cdFx0XHRcdH0se1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BcnRcblx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hcnRpc3QnXG5cdFx0XHRcdFx0YXJ0aXN0OiB0cnVlXG5cdFx0XHRcdH0se1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BbGJcblx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hbGJ1bSdcblx0XHRcdFx0fVxuXHRcdFx0XVxuXHRcdFx0cmV0dXJuIGl0ZW1cblx0XHR1cmxSb290OiAnL3RyYWNrJ1xuXG5cblx0Y2xhc3MgVHJhY2tMaXN0Lkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBUcmFja0xpc3QuSXRlbVxuXHRcdHVybDogYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9jYXRhbG9ndWUvZ2V0QmVlbGluZUNvbnRlbnRcIilcblx0XHRwYXJzZTogKGNvbGxlY3Rpb24pLT5cblx0XHRcdHJldHVybiBjb2xsZWN0aW9uLml0ZW1zXG5cblx0aW5pdGlhbGl6ZU5ld1RyYWNrcyA9IC0+XG5cdFx0aWYgVHJhY2tMaXN0Lk5ld0xpc3RJc0xvYWRcblx0XHRcdHJldHVybiBUcmFja0xpc3QuTmV3TGlzdERlZmVyLnByb21pc2UoKVxuXG5cdFx0VHJhY2tMaXN0Lk5ld0xpc3RJc0xvYWQgPSB0cnVlXG5cblx0XHR0cmFja0xpc3QgPSBuZXcgVHJhY2tMaXN0Lkl0ZW1zKCk7XG5cdFx0VHJhY2tMaXN0Lk5ld0xpc3REZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2hUcmFja3MgPSB0cmFja0xpc3QuZmV0Y2hcblx0XHRcdGRhdGE6XG5cdFx0XHRcdHR5cGU6ICd0cmFja3MnXG5cdFx0XHRcdHNvcnQ6ICdkYXRlJ1xuXHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdGNvdW50aXRlbXM6IDkwXG5cblx0XHRpZiBmZWNoVHJhY2tzXG5cdFx0XHRmZWNoVHJhY2tzLmRvbmUgKCktPlxuXHRcdFx0XHRUcmFja0xpc3QuTmV3TGlzdCA9IHRyYWNrTGlzdFxuXHRcdFx0XHRUcmFja0xpc3QuTmV3TGlzdERlZmVyLnJlc29sdmVXaXRoIGZlY2hUcmFja3MsW1RyYWNrTGlzdC5OZXdMaXN0XVxuXHRcdFx0ZmVjaFRyYWNrcy5mYWlsIC0+XG5cdFx0XHRcdFRyYWNrTGlzdC5OZXdMaXN0SXNMb2FkID0gZmFsc2Vcblx0XHRcdFx0VHJhY2tMaXN0Lk5ld0xpc3REZWZlci5yZWplY3RXaXRoIGZlY2hUcmFja3MsYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBUcmFja0xpc3QuTmV3TGlzdERlZmVyLnByb21pc2UoKVxuXG5cblxuXHRBUEkgPVxuXHRcdGdldE5ld1RyYWNrczogLT5cblx0XHRcdGlmIFRyYWNrTGlzdC5OZXdMaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZU5ld1RyYWNrcygpXG5cdFx0XHRyZXR1cm4gVHJhY2tMaXN0Lk5ld0xpc3RcblxuXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6bmV3OnRyYWNrcycsIC0+XG5cdFx0cmV0dXJuIEFQSS5nZXROZXdUcmFja3MoKVxuXG5cdHJldHVybiBUcmFja0xpc3RcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3RyYWNrX2xpc3QuY29mZmVlXG4gKiovIiwidmFyIG1hcCA9IHtcblx0XCIuL2FncmVlbWVudF9tb2RlbFwiOiA5MSxcblx0XCIuL2FncmVlbWVudF9tb2RlbC5jb2ZmZWVcIjogOTEsXG5cdFwiLi9hbGJ1bV9saXN0XCI6IDkyLFxuXHRcIi4vYWxidW1fbGlzdC5jb2ZmZWVcIjogOTIsXG5cdFwiLi9hcnRpc3RfbW9kZWxcIjogOTMsXG5cdFwiLi9hcnRpc3RfbW9kZWwuY29mZmVlXCI6IDkzLFxuXHRcIi4vY2hhcnRzX2xpc3RcIjogOTQsXG5cdFwiLi9jaGFydHNfbGlzdC5jb2ZmZWVcIjogOTQsXG5cdFwiLi9jbGlwTGlzdFwiOiA5NSxcblx0XCIuL2NsaXBMaXN0LmNvZmZlZVwiOiA5NSxcblx0XCIuL2Zhdm9yaXRlX2FsYnVtc1wiOiA5Nixcblx0XCIuL2Zhdm9yaXRlX2FsYnVtcy5jb2ZmZWVcIjogOTYsXG5cdFwiLi9mYXZvcml0ZV9wbGF5X2xpc3RcIjogOTcsXG5cdFwiLi9mYXZvcml0ZV9wbGF5X2xpc3QuY29mZmVlXCI6IDk3LFxuXHRcIi4vZmF2b3JpdGVfdHJhY2tfbGlzdFwiOiA5OCxcblx0XCIuL2Zhdm9yaXRlX3RyYWNrX2xpc3QuY29mZmVlXCI6IDk4LFxuXHRcIi4vZmVlZFwiOiAxMDEsXG5cdFwiLi9mZWVkLmNvZmZlZVwiOiAxMDEsXG5cdFwiLi9sb2NhbFwiOiAxMDIsXG5cdFwiLi9sb2NhbC5jb2ZmZWVcIjogMTAyLFxuXHRcIi4vbG9nXCI6IDcsXG5cdFwiLi9sb2cuY29mZmVlXCI6IDcsXG5cdFwiLi9tZW51X2xpc3RcIjogMTAzLFxuXHRcIi4vbWVudV9saXN0LmNvZmZlZVwiOiAxMDMsXG5cdFwiLi9wbGF5bGlzdHNfbGlzdFwiOiAxMDQsXG5cdFwiLi9wbGF5bGlzdHNfbGlzdC5jb2ZmZWVcIjogMTA0LFxuXHRcIi4vcHJvZmlsZV9tZW51XCI6IDEwNSxcblx0XCIuL3Byb2ZpbGVfbWVudS5jb2ZmZWVcIjogMTA1LFxuXHRcIi4vcmVjb21lbmRfcGFnZVwiOiAxMDYsXG5cdFwiLi9yZWNvbWVuZF9wYWdlLmNvZmZlZVwiOiAxMDYsXG5cdFwiLi9zZWFyY2hfbGlzdFwiOiAxMDcsXG5cdFwiLi9zZWFyY2hfbGlzdC5jb2ZmZWVcIjogMTA3LFxuXHRcIi4vc2xpZGVyX2xpc3RcIjogMTA4LFxuXHRcIi4vc2xpZGVyX2xpc3QuY29mZmVlXCI6IDEwOCxcblx0XCIuL3RhYl9saXN0XCI6IDEwOSxcblx0XCIuL3RhYl9saXN0LmNvZmZlZVwiOiAxMDksXG5cdFwiLi90cmFja19saXN0XCI6IDY1LFxuXHRcIi4vdHJhY2tfbGlzdC5jb2ZmZWVcIjogNjUsXG5cdFwiLi91c2VyLW5vdGlmeVwiOiAxMTAsXG5cdFwiLi91c2VyLW5vdGlmeS5jb2ZmZWVcIjogMTEwLFxuXHRcIi4vdXNlcl9tb2RlbFwiOiAxMTEsXG5cdFwiLi91c2VyX21vZGVsLmNvZmZlZVwiOiAxMTFcbn07XG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18od2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkpO1xufTtcbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0cmV0dXJuIG1hcFtyZXFdIHx8IChmdW5jdGlvbigpIHsgdGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJy5cIikgfSgpKTtcbn07XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gOTA7XG5cblxuXG4vKioqKioqKioqKioqKioqKipcbiAqKiBXRUJQQUNLIEZPT1RFUlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvLiokXG4gKiogbW9kdWxlIGlkID0gOTBcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsImRlZmluZSAtPlxuXHRNb2RlbCA9IHt9XG5cdGNsYXNzIE1vZGVsLkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6ICcvYWdyZWVtZW50J1xuXG5cdFx0cGFyc2U6IChtb2RlbCktPlxuXHRcdFx0cmV0dXJuIG1vZGVsXG5cblxuXHRpbml0aWFsaXplVXNlciA9IC0+XG5cdFx0TW9kZWwuQWdyZWVtZW50ID0gbmV3IE1vZGVsLkl0ZW0oKTtcblx0XHRkZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2hNb2RlbCA9IE1vZGVsLkFncmVlbWVudC5mZXRjaCgpO1xuXG5cdFx0aWYgZmVjaE1vZGVsXG5cdFx0XHRmZWNoTW9kZWwuZG9uZSAocmVzKS0+XG5cdFx0XHRcdGRlZmVyLnJlc29sdmVXaXRoIGZlY2hNb2RlbCxbTW9kZWwuQWdyZWVtZW50XVxuXG5cdFx0XHRmZWNoTW9kZWwuZmFpbCAtPlxuXHRcdFx0XHRkZWZlci5yZWplY3RXaXRoIGZlY2hNb2RlbCxhcmd1bWVudHNcblxuXHRcdFx0cmV0dXJuIGRlZmVyLnByb21pc2UoKVxuXG5cblxuXG5cblx0QVBJID1cblx0XHRnZXRBZ3JlZW1lbnQ6IC0+XG5cdFx0XHRpZiBNb2RlbC5BZ3JlZW1lbnQgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdHJldHVybiBpbml0aWFsaXplVXNlcigpXG5cdFx0XHRyZXR1cm4gTW9kZWwuQWdyZWVtZW50XG5cblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmFncmVlbWVudCcsIC0+XG5cdFx0cmV0dXJuIEFQSS5nZXRBZ3JlZW1lbnQoKVxuXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9hZ3JlZW1lbnRfbW9kZWwuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG5cdEFsYnVtTGlzdCA9IHt9XG5cblx0Y2xhc3MgQWxidW1MaXN0Lkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6IC0+XG5cdFx0XHRyZXR1cm4gYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9jYXRhbG9ndWUvZ2V0QWxidW1cIixcIj9hbGJ1bWlkPSN7QGlkfVwiKVxuXHRcdFxuXHRcdHNwZWNpZmllZElkS2V5OiAnYWxidW1pZCdcblx0XHRwYXJzZTogKG1vZGVsKS0+XG5cdFx0XHRpZiBtb2RlbC5hbGJ1bVxuXHRcdFx0XHRtb2RlbC5hbGJ1bS5hbGJ1bSA9IHRydWVcblx0XHRcdFx0cmV0dXJuIG1vZGVsLmFsYnVtXG5cdFx0XHRlbHNlXG5cdFx0XHRcdG1vZGVsLmFsYnVtID0gdHJ1ZVxuXHRcdFx0XHRyZXR1cm4gbW9kZWxcblxuXHRjbGFzcyBBbGJ1bUxpc3QuSXRlbXMgZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG5cdFx0bW9kZWw6IEFsYnVtTGlzdC5JdGVtXG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybChcIi9jb21wYXRpYmlsaXR5L2NhdGFsb2d1ZS9nZXRCZWVsaW5lQ29udGVudFwiKVxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24uaXRlbXNcblx0XHRmZXRjaDogKG9wdHMpLT5cblx0XHRcdGdlbnJlID0gYXBwLnJlcXVlc3QgJ2dldDpsb2NhbDpnZW5yaWVzJ1xuXHRcdFx0aWYgZ2VucmUubGVuZ3RoXG5cdFx0XHRcdG9wdHMuZGF0YSA9IG9wdHMuZGF0YSBvciB7fVxuXHRcdFx0XHRvcHRzLmRhdGEuZ2VucmUgPSBnZW5yZS50b0pTT04oKVxuXG5cdFx0XHRCYWNrYm9uZS5Db2xsZWN0aW9uLnByb3RvdHlwZS5mZXRjaC5hcHBseSBALFtvcHRzXVxuXG5cblx0aW5pdGlhbGl6ZU5ld0FsYnVtcyA9IC0+XG5cdFx0aWYgQWxidW1MaXN0Lk5ld0xpc3RJc0xvYWRcblx0XHRcdHJldHVybiBBbGJ1bUxpc3QuTmV3TGlzdERlZmVyLnByb21pc2UoKVxuXG5cdFx0QWxidW1MaXN0Lk5ld0xpc3RJc0xvYWQgPSB0cnVlXG5cblx0XHRhbGJ1bXMgPSBuZXcgQWxidW1MaXN0Lkl0ZW1zKCk7XG5cdFx0QWxidW1MaXN0Lk5ld0xpc3REZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2hBbGJ1bXMgPSBhbGJ1bXMuZmV0Y2hcblx0XHRcdGRhdGE6XG5cdFx0XHRcdHR5cGU6ICdhbGJ1bXMnXG5cdFx0XHRcdHNvcnQ6ICdkYXRlJ1xuXHRcdFx0XHRwYWdlOiAxXG5cdFx0XHRcdGNvdW50aXRlbXM6IDYwXG5cblx0XHRpZiBmZWNoQWxidW1zXG5cdFx0XHRmZWNoQWxidW1zLmRvbmUgKCktPlxuXHRcdFx0XHRBbGJ1bUxpc3QuTmV3TGlzdCA9IGFsYnVtc1xuXHRcdFx0XHRBbGJ1bUxpc3QuTmV3TGlzdC5UeXBlID0gJ25ldydcblx0XHRcdFx0QWxidW1MaXN0Lk5ld0xpc3REZWZlci5yZXNvbHZlV2l0aCBmZWNoQWxidW1zLFtBbGJ1bUxpc3QuTmV3TGlzdF1cblx0XHRcdGZlY2hBbGJ1bXMuZmFpbCAtPlxuXHRcdFx0XHRBbGJ1bUxpc3QuTmV3TGlzdElzTG9hZCA9IGZhbHNlXG5cdFx0XHRcdEFsYnVtTGlzdC5OZXdMaXN0RGVmZXIucmVqZWN0V2l0aCBmZWNoQWxidW1zLGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gQWxidW1MaXN0Lk5ld0xpc3REZWZlci5wcm9taXNlKClcblxuXHRpbml0aWFsaXplU2luZ2xlQWxidW0gPSAoaWQpLT5cblx0XHRBbGJ1bUxpc3QuU2luZ2xlQWxidW0gPSBuZXcgQWxidW1MaXN0Lkl0ZW1cblx0XHRcdGlkOiBpZFxuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0ZmVjaEFsYnVtID0gQWxidW1MaXN0LlNpbmdsZUFsYnVtLmZldGNoKClcblxuXHRcdGlmIGZlY2hBbGJ1bVxuXHRcdFx0ZmVjaEFsYnVtLmRvbmUgKHJlcyktPlxuXHRcdFx0XHRpZiByZXMuY29kZSBpcyAwXG5cdFx0XHRcdFx0QWxidW1MaXN0LkFsYnVtSWQgPSBpZFxuXHRcdFx0XHRcdGRlZmVyLnJlc29sdmVXaXRoIGZlY2hBbGJ1bSxbQWxidW1MaXN0LlNpbmdsZUFsYnVtXVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0ZGVmZXIucmVqZWN0V2l0aCBmZWNoQWxidW0sW3Jlc11cblx0XHRcdGZlY2hBbGJ1bS5mYWlsIC0+XG5cdFx0XHRcdGRlZmVyLnJlamVjdFdpdGggZmVjaEFsYnVtLGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblxuXG5cdEFQSSA9XG5cdFx0Z2V0TmV3QWxidW1zOiAtPlxuXHRcdFx0aWYgQWxidW1MaXN0Lk5ld0xpc3QgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdHJldHVybiBpbml0aWFsaXplTmV3QWxidW1zKClcblx0XHRcdHJldHVybiBBbGJ1bUxpc3QuTmV3TGlzdFxuXG5cdFx0Z2V0QWxidW06IChpZCktPlxuXHRcdFx0aWYgQWxidW1MaXN0LkFsYnVtSWQgaXNudCBpZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVNpbmdsZUFsYnVtIGlkXG5cblx0XHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0XHRzZXRJbW1lZGlhdGUgLT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZSBBbGJ1bUxpc3QuU2luZ2xlQWxidW1cblxuXHRcdFx0cmV0dXJuIGRlZmVyLnByb21pc2UoKTtcblxuXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6bmV3OmFsYnVtcycsIC0+XG5cdFx0cmV0dXJuIEFQSS5nZXROZXdBbGJ1bXMoKVxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnNpbmdsZTphbGJ1bScsIChpZCktPlxuXHRcdHJldHVybiBBUEkuZ2V0QWxidW0gaWRcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDp0ZW1wOmFsYnVtcycsIChjb2xsZWN0aW9uKS0+XG5cdFx0aWYgY29sbGVjdGlvbj9cblx0XHRcdEFsYnVtTGlzdC5UZW1wQWxidW1zID0gbmV3IEFsYnVtTGlzdC5JdGVtcyBjb2xsZWN0aW9uLnRvSlNPTigpXG5cblx0XHRcdGlmIEFsYnVtTGlzdC5UZW1wQ29sbGVjdGlvbj9cblx0XHRcdFx0QWxidW1MaXN0LlRlbXBDb2xsZWN0aW9uLm9mZiAncmVzZXQnXG5cblx0XHRcdEFsYnVtTGlzdC5UZW1wQ29sbGVjdGlvbiA9IGNvbGxlY3Rpb25cblxuXHRcdFx0QWxidW1MaXN0LlRlbXBDb2xsZWN0aW9uLm9uICdyZXNldCcsIC0+XG5cdFx0XHRcdGlmIEFsYnVtTGlzdC5UZW1wQ29sbGVjdGlvbi5UeXBlIGlzICduZXcnXG5cdFx0XHRcdFx0YXBwLk1haW4uQ29udGVudC5OZXcuQ29udHJvbGxlci5zaG93QWxidW1zT25lIEFsYnVtTGlzdC5UZW1wQ29sbGVjdGlvblxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0YXBwLk1haW4uQ29udGVudC5SZWNvbWVuZC5Db250cm9sbGVyLnNob3dBbGJ1bXNPbmUgQWxidW1MaXN0LlRlbXBDb2xsZWN0aW9uXG5cblx0XHRyZXR1cm4gQWxidW1MaXN0LlRlbXBBbGJ1bXNcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ3JlOmluaXRpYWxpemU6YWxidW1zJywgLT5cblx0XHRpZiBBbGJ1bUxpc3QuTmV3TGlzdFxuXHRcdFx0QWxidW1MaXN0Lk5ld0xpc3QuZmV0Y2hcblx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHR0eXBlOiAnbmV3J1xuXHRcdFx0XHRcdHBhZ2U6IDFcblx0XHRcdFx0XHRjb3VudDogNjBcblx0XHRcdFx0cmVzZXQ6IHRydWVcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2FsYnVtX2xpc3QuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG5cdE1vZGVsID0ge31cblx0XG5cdGNsYXNzIE1vZGVsLkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6IC0+XG5cdFx0XHRyZXR1cm4gYXBwLmdldFJlcXVlc3RVcmwoXCIvYXJ0aXN0L2dldFwiKTtcblxuXHRcdHBhcnNlOiAobW9kZWwpLT5cblx0XHRcdHJldHVybiBtb2RlbC5hcnRpc3RcblxuXHRjbGFzcyBNb2RlbC5UcmFjayBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cdFx0cGFyc2U6IChpdGVtKS0+XG5cdFx0XHRpdGVtLnVuaXEgPSAnYXJ0aXN0LScraXRlbS5pZFxuXHRcdFx0cmV0dXJuIGl0ZW1cblxuXHRjbGFzcyBNb2RlbC5UcmFja3MgZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG5cdFx0bW9kZWw6IE1vZGVsLlRyYWNrXG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybChcIi9jb21wYXRpYmlsaXR5L2NhdGFsb2d1ZS9nZXRCZWVsaW5lQ29udGVudFwiLFwiP3R5cGU9dHJhY2tzJnNvcnQ9cmF0aW5nJmNvdW50aXRlbXM9MTBcIilcblx0XHRwYXJzZTogKGNvbGxlY3Rpb24pLT5cblx0XHRcdHJldHVybiBjb2xsZWN0aW9uLml0ZW1zXG5cblxuXHRjbGFzcyBNb2RlbC5BbGJ1bSBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cdFx0dXJsUm9vdDogLT5cblx0XHRcdHJldHVybiBhcHAuZ2V0UmVxdWVzdFVybChcIi9jb21wYXRpYmlsaXR5L2NhdGFsb2d1ZS9nZXRBbGJ1bVwiLFwiP2FsYnVtaWQ9I3tAaWR9XCIpXG5cdFx0cGFyc2U6IChtb2RlbCktPlxuXHRcdFx0aWYgbW9kZWwuYWxidW1cblx0XHRcdFx0bW9kZWwuYWxidW0uYWxidW0gPSB0cnVlXG5cdFx0XHRcdHJldHVybiBtb2RlbC5hbGJ1bVxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRtb2RlbC5hbGJ1bSA9IHRydWVcblx0XHRcdFx0cmV0dXJuIG1vZGVsXG5cblx0Y2xhc3MgTW9kZWwuQWxidW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBNb2RlbC5BbGJ1bVxuXHRcdHVybDogYXBwLmdldFJlcXVlc3RVcmwoXCIvYmVlbGluZU11c2ljVjEvY2F0YWxvZy9nZXRBbGJ1bXNcIixcIj9zb3J0PXJhdGluZ1wiKVxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24uaXRlbXNcblxuXHRjbGFzcyBNb2RlbC5BcnRpc3RWaWRlb01vZGVsIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcblx0XHRwYXJzZTogKGl0ZW0pLT5cblx0XHRcdGl0ZW0uaWQgPSBpdGVtWydpZCddWyd2aWRlb0lkJ11cblx0XHRcdGl0ZW0uc25pcHBldC5yZXNvdXJjZUlkID0ge31cblx0XHRcdGl0ZW0uc25pcHBldC5yZXNvdXJjZUlkLnZpZGVvSWQgPSBpdGVtLmlkXG5cdFx0XHRyZXR1cm4gaXRlbVxuXG5cblx0Y2xhc3MgTW9kZWwuQXJ0aXN0VmlkZW9JdGVtcyBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb25cblx0XHRtb2RlbDogTW9kZWwuQXJ0aXN0VmlkZW9Nb2RlbFxuXHRcdHVybDogJy9hcnRpc3RWaWRlbydcblxuXHRpbml0aWFsaXplQXJ0aXN0VmlkZW8gPSAocSktPlxuXHRcdGlmIE1vZGVsLlZpZGVvTG9hZFByb2Nlc3Ncblx0XHRcdE1vZGVsLlZpZGVvTG9hZFByb2Nlc3MuYWJvcnQoKVxuXG5cdFx0TW9kZWwuQXJ0aXN0VmlkZW9MaXN0ID0gbmV3IE1vZGVsLkFydGlzdFZpZGVvSXRlbXMoKTtcblx0XHRkZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdE1vZGVsLlZpZGVvUHJvY2VzcyA9IGZlY2ggPSBNb2RlbC5BcnRpc3RWaWRlb0xpc3QuZmV0Y2hcblx0XHRcdGRhdGE6XG5cdFx0XHRcdHE6IHFcblx0XHRpZiBmZWNoXG5cdFx0XHRmZWNoLmRvbmUgLT5cblx0XHRcdFx0TW9kZWwuQXJ0aXN0cSA9IHFcblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggZmVjaCxbTW9kZWwuQXJ0aXN0VmlkZW9MaXN0XVxuXG5cdFx0XHRmZWNoLmZhaWwgLT5cblx0XHRcdFx0ZGVmZXIucmVqZWN0V2l0aCBmZWNoLGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblxuXHRpbml0aWFsaXplQXJ0aXN0VHJhY2tzID0gKG9wdGlvbnM9e30pLT5cblx0XHRNb2RlbC5BcnRpc3RUcmFja3MgPSBuZXcgTW9kZWwuVHJhY2tzXG5cdFx0ZGVmZXIgPSAkLkRlZmVycmVkKClcblx0XHRNb2RlbC5UcmFja3NMb2FkUHJvY2VzcyA9IGZlY2hNb2RlbCA9IE1vZGVsLkFydGlzdFRyYWNrcy5mZXRjaCBfLm9taXQob3B0aW9ucywnc3VjY2VzcycsJ2Vycm9yJylcblxuXHRcdGlmIGZlY2hNb2RlbFxuXHRcdFx0ZmVjaE1vZGVsLmRvbmUgPT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggZmVjaE1vZGVsLFtNb2RlbC5BcnRpc3RUcmFja3NdXG5cdFx0XHRcdE1vZGVsLmFydGlzdFRyYWNrSWQgPSBvcHRpb25zLmRhdGEuYXJ0aXN0XG5cblx0XHRcdGZlY2hNb2RlbC5mYWlsIC0+XG5cdFx0XHRcdGRlZmVyLnJlamVjdFdpdGggZmVjaE1vZGVsLGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblx0aW5pdGlhbGl6ZUFydGlzdEFsYnVtcyA9IChvcHRpb25zPXt9KS0+XG5cdFx0QXJ0aXN0QWxidW1zID0gbmV3IE1vZGVsLkFsYnVtc1xuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0TW9kZWwuQWxidW1zTG9hZFByb2Nlc3MgPSBmZWNoTW9kZWwgPSBBcnRpc3RBbGJ1bXMuZmV0Y2ggXy5vbWl0KG9wdGlvbnMsJ3N1Y2Nlc3MnLCdlcnJvcicpXG5cblx0XHRpZiBmZWNoTW9kZWxcblx0XHRcdGZlY2hNb2RlbC5kb25lID0+XG5cdFx0XHRcdGRlZmVyLnJlc29sdmVXaXRoIGZlY2hNb2RlbCxbQXJ0aXN0QWxidW1zXVxuXHRcdFx0XHRNb2RlbC5hcnRpc3RBbGJ1bUlkID0gb3B0aW9ucy5kYXRhLmFydGlzdFxuXG5cdFx0XHRmZWNoTW9kZWwuZmFpbCAtPlxuXHRcdFx0XHRkZWZlci5yZWplY3RXaXRoIGZlY2hNb2RlbCxhcmd1bWVudHNcblxuXHRcdFx0cmV0dXJuIGRlZmVyLnByb21pc2UoKVxuXG5cdGluaXRpYWxpemVBcnRpc3QgPSAoaWQpLT5cblx0XHRNb2RlbC5BcnRpc3QgPSBuZXcgTW9kZWwuSXRlbVxuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0TW9kZWwuTG9hZFByb2Nlc3MgPSBmZWNoTW9kZWwgPSBNb2RlbC5BcnRpc3QuZmV0Y2hcblx0XHRcdGRhdGE6XG5cdFx0XHRcdCdhcnRpc3RpZCc6IGlkXG5cblx0XHRpZiBmZWNoTW9kZWxcblx0XHRcdGZlY2hNb2RlbC5kb25lIChyZXMpPT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggZmVjaE1vZGVsLFtNb2RlbC5BcnRpc3RdXG5cdFx0XHRcdE1vZGVsLmFydGlzdElkID0gaWRcblxuXHRcdFx0ZmVjaE1vZGVsLmZhaWwgLT5cblx0XHRcdFx0ZGVmZXIucmVqZWN0V2l0aCBmZWNoTW9kZWwsYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBkZWZlci5wcm9taXNlKClcblxuXG5cblx0QVBJID1cblx0XHRnZXRBcnRpc3Q6IChpZCktPlxuXHRcdFx0aWYgTW9kZWwuYXJ0aXN0SWQgYW5kIE1vZGVsLmFydGlzdElkIGlzIGlkXG5cdFx0XHRcdHJldHVybiBNb2RlbC5BcnRpc3Rcblx0XHRcdGVsc2Vcblx0XHRcdFx0cmV0dXJuIGluaXRpYWxpemVBcnRpc3QgaWRcblxuXHRcdGdldEFydGlzdFRyYWNrczogKG9wdGlvbnMpLT5cblx0XHRcdGlmIE1vZGVsLmFydGlzdFRyYWNrSWQgYW5kIE1vZGVsLmFydGlzdFRyYWNrSWQgaXMgb3B0aW9ucy5kYXRhLmFydGlzdFxuXHRcdFx0XHRyZXR1cm4gTW9kZWwuQXJ0aXN0VHJhY2tzXG5cdFx0XHRlbHNlXG5cdFx0XHRcdHJldHVybiBpbml0aWFsaXplQXJ0aXN0VHJhY2tzIG9wdGlvbnNcblxuXHRcdGdldEFydGlzdEFsYnVtczogKG9wdGlvbnMpLT5cblx0XHRcdHJldHVybiBpbml0aWFsaXplQXJ0aXN0QWxidW1zIG9wdGlvbnNcblxuXHRcdGdldEFydGlzdFZpZGVvczogKHEpLT5cblx0XHRcdGlmIE1vZGVsLkFydGlzdHEgaXNudCBxXG5cdFx0XHRcdHJldHVybiBpbml0aWFsaXplQXJ0aXN0VmlkZW8gcVxuXHRcdFx0cmV0dXJuIE1vZGVsLkFydGlzdFZpZGVvTGlzdFxuXG5cblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDphcnRpc3QnLCAoaWQpLT5cblx0XHRyZXR1cm4gQVBJLmdldEFydGlzdChpZClcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDphcnRpc3Q6dHJhY2tzJywgKG9wdGlvbnMpLT5cblx0XHRyZXR1cm4gQVBJLmdldEFydGlzdFRyYWNrcyBvcHRpb25zXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6YXJ0aXN0OmFsYnVtcycsIChvcHRpb25zKS0+XG5cdFx0cmV0dXJuIEFQSS5nZXRBcnRpc3RBbGJ1bXMgb3B0aW9uc1xuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmFydGlzdDp2aWRlb3MnLCAocSktPlxuXHRcdHJldHVybiBBUEkuZ2V0QXJ0aXN0VmlkZW9zIHFcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ3N0b3A6YXJ0aXN0OnJlcXVlc3QnLCAodHlwZSktPlxuXHRcdGlmIHR5cGVcblx0XHRcdHN3aXRjaCB0eXBlXG5cdFx0XHRcdHdoZW4gJ3RyYWNrcydcblx0XHRcdFx0XHRpZiBNb2RlbC5UcmFja3NMb2FkUHJvY2Vzc1xuXHRcdFx0XHRcdFx0TW9kZWwuVHJhY2tzTG9hZFByb2Nlc3MuYWJvcnQoKVxuXG5cdFx0XHRcdHdoZW4gJ2FsYnVtcydcblx0XHRcdFx0XHRpZiBNb2RlbC5BbGJ1bXNMb2FkUHJvY2Vzc1xuXHRcdFx0XHRcdFx0TW9kZWwuQWxidW1zTG9hZFByb2Nlc3MuYWJvcnQoKVxuXHRcdFx0XHR3aGVuICdhcnRpc3QnXG5cdFx0XHRcdFx0aWYgTW9kZWwuTG9hZFByb2Nlc3Ncblx0XHRcdFx0XHRcdE1vZGVsLkxvYWRQcm9jZXNzLmFib3J0KClcblx0XHRcdFx0d2hlbiAndmlkZW8nXG5cdFx0XHRcdFx0aWYgTW9kZWwuVmlkZW9Mb2FkUHJvY2Vzc1xuXHRcdFx0XHRcdFx0TW9kZWwuVmlkZW9Mb2FkUHJvY2Vzcy5hYm9ydCgpXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9hcnRpc3RfbW9kZWwuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG4gIENoYXJ0cyA9IHt9XG4gIFxuICBjbGFzcyBDaGFydHMuSXRlbSBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cbiAgY2xhc3MgQ2hhcnRzLkxpc3QgZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG4gICAgbW9kZWw6IENoYXJ0cy5JdGVtXG4gICAgdXJsOiAnL2NoYXJ0cy9saXN0J1xuICAgIHBhcnNlOiAoY29sbGVjdGlvbiktPlxuICAgICAgcmV0dXJuIGNvbGxlY3Rpb24uY2hhcnRzXG5cbiAgaW5pdGlhbGl6ZUNoYXJ0cyA9IC0+XG4gICAgaWYgQ2hhcnRzLkxpc3RJc0xvYWRcbiAgICAgIHJldHVybiBDaGFydHMuTGlzdERlZmVyLnByb21pc2UoKVxuXG4gICAgQ2hhcnRzLkxpc3RJc0xvYWQgPSB0cnVlXG5cbiAgICBmZWVkTGlzdCA9IG5ldyBDaGFydHMuTGlzdFxuICAgIENoYXJ0cy5MaXN0RGVmZXIgPSAkLkRlZmVycmVkKClcbiAgICBmZXRjaCA9IGZlZWRMaXN0LmZldGNoKClcblxuICAgIGlmIGZldGNoXG4gICAgICBmZXRjaC5kb25lICgpLT5cbiAgICAgICAgQ2hhcnRzLk5ld0xpc3QgPSBmZWVkTGlzdFxuICAgICAgICBDaGFydHMuTGlzdERlZmVyLnJlc29sdmVXaXRoIGZldGNoLFtDaGFydHMuTmV3TGlzdF1cbiAgICAgIGZldGNoLmZhaWwgLT5cbiAgICAgICAgQ2hhcnRzLkxpc3RJc0xvYWQgPSBmYWxzZVxuICAgICAgICBDaGFydHMuTGlzdERlZmVyLnJlamVjdFdpdGggZmV0Y2gsYXJndW1lbnRzXG5cbiAgICAgIHJldHVybiBDaGFydHMuTGlzdERlZmVyLnByb21pc2UoKVxuXG5cbiAgQVBJID1cbiAgICBnZXRDaGFydHM6IC0+XG4gICAgICBpZiBDaGFydHMuTmV3TGlzdCBpcyB1bmRlZmluZWRcbiAgICAgICAgcmV0dXJuIGluaXRpYWxpemVDaGFydHMoKVxuICAgICAgcmV0dXJuIENoYXJ0cy5OZXdMaXN0XG5cblxuXG4gIGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmNoYXJ0cycsIC0+XG4gICAgcmV0dXJuIEFQSS5nZXRDaGFydHMoKVxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL2NoYXJ0c19saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuXHRWaWRlbyA9IHt9XG5cdFxuXHRjbGFzcyBWaWRlby5JdGVtcyBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb25cblx0XHR1cmw6ICcvY2xpcHMnXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gY29sbGVjdGlvbi5pdGVtc1xuXG5cblx0aW5pdGlhbGl6ZVZpZGVvcyA9IC0+XG5cdFx0aWYgVmlkZW8uaXNMb2FkXG5cdFx0XHRyZXR1cm4gVmlkZW8uZGVmZXIucHJvbWlzZSgpXG5cblx0XHR2aWRlb0xpc3QgPSBuZXcgVmlkZW8uSXRlbXMoKTtcblx0XHRWaWRlby5kZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2ggPSB2aWRlb0xpc3QuZmV0Y2goKTtcblx0XHRpZiBmZWNoXG5cdFx0XHRmZWNoLmRvbmUgKHJlcyktPlxuXHRcdFx0XHRWaWRlby5MaXN0ID0gdmlkZW9MaXN0XG5cdFx0XHRcdFZpZGVvLmRlZmVyLnJlc29sdmVXaXRoIGZlY2gsW1ZpZGVvLkxpc3RdXG5cdFx0XHRcdGlmIHJlcy5uZXh0UGFnZVRva2VuXG5cdFx0XHRcdFx0VmlkZW8uTGlzdC5mZXRjaFxuXHRcdFx0XHRcdFx0ZGF0YTpcblx0XHRcdFx0XHRcdFx0dG9rZW46IHJlcy5uZXh0UGFnZVRva2VuXG5cdFx0XHRcdFx0XHRyZW1vdmU6IGZhbHNlXG5cblx0XHRcdGZlY2guZmFpbCAtPlxuXHRcdFx0XHRWaWRlby5kZWZlci5yZWplY3RXaXRoIGZlY2gsYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBWaWRlby5kZWZlci5wcm9taXNlKClcblxuXG5cblxuXG5cblx0QVBJID1cblx0XHRnZXRWaWRlb3M6IC0+XG5cdFx0XHRpZiBWaWRlby5MaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVZpZGVvcygpXG5cdFx0XHRyZXR1cm4gVmlkZW8uTGlzdFxuXG5cblxuXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6dmlkZW9zJywgLT5cblx0XHRyZXR1cm4gQVBJLmdldFZpZGVvcygpXG5cblx0ZG8gQVBJLmdldFZpZGVvc1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvY2xpcExpc3QuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG5cdEZhdm9yaXRlQWxidW1zID0ge31cblx0XG5cdGNsYXNzIEZhdm9yaXRlQWxidW1zLkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6IC0+XG5cdFx0XHRyZXR1cm4gYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9jYXRhbG9ndWUvZ2V0QWxidW1cIixcIj9hbGJ1bWlkPSN7QGlkfVwiKVxuXHRcdFxuXHRcdHVwZGF0ZVVybDogLT5cblx0XHRcdHJldHVybiBhcHAuZ2V0UmVxdWVzdFVybChcIi9jb21wYXRpYmlsaXR5L2Zhdm91cml0ZXMvYWRkSXRlbVwiLFwiP2NvbnRlbnR0eXBlPUFMQk0mY29udGVudGlkPSN7QGlkfVwiKVxuXHRcdGRlbGV0ZVVybDogLT5cblx0XHRcdHJldHVybiBhcHAuZ2V0UmVxdWVzdFVybChcIi9jb21wYXRpYmlsaXR5L2Zhdm91cml0ZXMvcmVtb3ZlSXRlbVwiLFwiP2NvbnRlbnR0eXBlPUFMQk0mY29udGVudGlkPSN7QGlkfVwiKVxuXHRcdGRlZmF1bHRzOlxuXHRcdFx0XCJ0aXRsZVwiOiBcIlwiXG5cdFx0XHRcImltZ1wiOiBcIlwiXG5cdFx0cGFyc2U6IChtb2RlbCktPlxuXHRcdFx0aWYgbW9kZWwuaXNfZGVsZXRlZFxuXHRcdFx0XHRtb2RlbC5mYXZvdXJpdGVzLnNvcnQgPSArbW9kZWwuZmF2b3VyaXRlcy5zb3J0IC0gMTAwMDAwXG5cblx0XHRcdGlmIG1vZGVsLmFsYnVtXG5cdFx0XHRcdHJldHVybiBtb2RlbC5hbGJ1bVxuXHRcdFx0ZWxzZVxuXHRcdFx0XHRyZXR1cm4gbW9kZWxcblxuXG5cdGNsYXNzIEZhdm9yaXRlQWxidW1zLkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBGYXZvcml0ZUFsYnVtcy5JdGVtXG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybChcIi9mYXZvdXJpdGVzL2dldGl0ZW1zXCIsXCI/dXNlcmlkPSN7YXBwLnVzZXIudWlkfSZjb250ZW50dHlwZT1BTEJNXCIpXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gY29sbGVjdGlvbi5hbGJ1bXNcblxuXHRcdGNvbXBhcmF0b3I6IChpdGVtKS0+XG5cdFx0XHRyZXR1cm4gaXRlbS5nZXQoJ2Zhdm91cml0ZXMnKS5zb3J0XG5cblxuXHRpbml0aWFsaXplQWxidW1zID0gLT5cblx0XHRpZiBGYXZvcml0ZUFsYnVtcy5pc0xvYWRcblx0XHRcdHJldHVybiBGYXZvcml0ZUFsYnVtcy5kZWZlci5wcm9taXNlKClcblxuXHRcdEZhdm9yaXRlQWxidW1zLmlzTG9hZCA9IHRydWVcblxuXHRcdGZhdm9yaXRlQWxidW0gPSBuZXcgRmF2b3JpdGVBbGJ1bXMuSXRlbXMoKTtcblx0XHRGYXZvcml0ZUFsYnVtcy5kZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2hBbGJ1bXMgPSBmYXZvcml0ZUFsYnVtLmZldGNoKCk7XG5cdFx0aWYgZmVjaEFsYnVtc1xuXHRcdFx0ZmVjaEFsYnVtcy5kb25lIChyZXMpLT5cblx0XHRcdFx0RmF2b3JpdGVBbGJ1bXMuTGlzdCA9IGZhdm9yaXRlQWxidW1cblx0XHRcdFx0aWYgcmVzLmNvZGUgaXMgMFxuXHRcdFx0XHRcdEZhdm9yaXRlQWxidW1zLmRlZmVyLnJlc29sdmVXaXRoIGZlY2hBbGJ1bXMsW0Zhdm9yaXRlQWxidW1zLkxpc3RdXG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRGYXZvcml0ZUFsYnVtcy5pc0xvYWQgPSBmYWxzZVxuXHRcdFx0XHRcdEZhdm9yaXRlQWxidW1zLmRlZmVyLnJlamVjdFdpdGggZmVjaEFsYnVtcyxhcmd1bWVudHNcblx0XHRcdGZlY2hBbGJ1bXMuZmFpbCAtPlxuXHRcdFx0XHRGYXZvcml0ZUFsYnVtcy5pc0xvYWQgPSBmYWxzZVxuXHRcdFx0XHRGYXZvcml0ZUFsYnVtcy5kZWZlci5yZWplY3RXaXRoIGZlY2hBbGJ1bXMsYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBGYXZvcml0ZUFsYnVtcy5kZWZlci5wcm9taXNlKClcblxuXG5cblx0QVBJID1cblx0XHRnZXRBbGJ1bXM6IC0+XG5cdFx0XHRpZiBGYXZvcml0ZUFsYnVtcy5MaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZUFsYnVtcygpXG5cblx0XHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0XHRzZXRJbW1lZGlhdGUgLT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZSBGYXZvcml0ZUFsYnVtcy5MaXN0XG5cblx0XHRcdHJldHVybiBkZWZlci5wcm9taXNlKCk7XG5cblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmZhdm9yaXRlOmFsYnVtcycsIC0+XG5cdFx0cmV0dXJuIEFQSS5nZXRBbGJ1bXMoKVxuXG5cblx0QVBJLmdldEFsYnVtcygpXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvZmF2b3JpdGVfYWxidW1zLmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuICAgIEZhdm9yaXRlUGxheWxpc3QgPSB7fVxuICAgIGNsYXNzIEZhdm9yaXRlUGxheWxpc3QuSXRlbSBleHRlbmRzIEJhY2tib25lLk1vZGVsXG4gICAgICAgdXJsUm9vdDogLT5cbiAgICAgICAgIGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2NvbXBhdGliaWxpdHkvcGxheWxpc3QvZ2V0XCIsXCI/cGxheWxpc3RpZD0je0BpZH0maXRlbXNfb25fcGFnZT0xMDAwXCIpXG4gICAgICAgXG4gICAgICAgY3JlYXRlVXJsOiAtPlxuICAgICAgICAgYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9wbGF5bGlzdC9jcmVhdGVcIixcIj90aXRsZT0je0BnZXQoJ3RpdGxlJyl9XCIpO1xuXG4gICAgICAgZGVsZXRlVXJsOiAtPlxuICAgICAgICAgYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9wbGF5bGlzdC9yZW1vdmVcIixcIj9wbGF5bGlzdGlkPSN7QGlkfVwiKTtcbiAgICAgICBkZWZhdWx0czpcbiAgICAgICAgIFwidGl0bGVcIjogXCJcIlxuICAgICAgICAgXCJpbWFnZVwiOiBcIlwiXG4gICAgICAgICBcImltYWdlX2JsYWNrXCI6IFwiXCJcblxuICAgICAgIHBhcnNlOiAoaXRlbSktPlxuICAgICAgICAgaWYgaXRlbS5wbGF5bGlzdERhdGFcbiAgICAgICAgICByZXR1cm4gaXRlbS5wbGF5bGlzdERhdGFcbiAgICAgICAgIHJldHVybiBpdGVtXG5cbiAgICAgICB2YWxpZGF0ZTogKGF0dHJzLCBvcHRpb25zKS0+XG4gICAgICAgICBpZiAkLnRyaW0oYXR0cnMudGl0bGUpIGlzIFwiXCJcbiAgICAgICAgICByZXR1cm4gXCJ0aXRsZVwiO1xuXG4gICAgICAgICBpZiBfLmlzQXJyYXkoYXR0cnMudHJhY2tzKSBhbmQgYXR0cnMudHJhY2tzLmxlbmd0aCBpcyAwXG4gICAgICAgICAgcmV0dXJuIFwi0J3QtdGCINGC0YDQtdC60L7QslwiO1xuXG5cbiAgICBjbGFzcyBGYXZvcml0ZVBsYXlsaXN0Lkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuICAgICAgIG1vZGVsOiBGYXZvcml0ZVBsYXlsaXN0Lkl0ZW1cbiAgICAgICB1cmw6IGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2NvbXBhdGliaWxpdHkvcGxheWxpc3RzL2dldGJ5dXNlcnNpZFwiKVxuXG4gICAgICAgcGFyc2U6IChjb2xsZWN0aW9uKS0+XG4gICAgICAgICByZXR1cm4gY29sbGVjdGlvbi5wbGF5bGlzdHNcblxuXG4gICAgaW5pdGlhbGl6ZVBsYXlsaXN0cyA9IC0+XG4gICAgICAgaWYgRmF2b3JpdGVQbGF5bGlzdC5pc0xvYWRcbiAgICAgICAgIHJldHVybiBGYXZvcml0ZVBsYXlsaXN0LmRlZmVyLnByb21pc2UoKVxuXG4gICAgICAgRmF2b3JpdGVQbGF5bGlzdC5pc0xvYWQgPSB0cnVlXG5cbiAgICAgICBmYXZvcml0ZVBsID0gbmV3IEZhdm9yaXRlUGxheWxpc3QuSXRlbXMoKTtcbiAgICAgICBGYXZvcml0ZVBsYXlsaXN0LmRlZmVyID0gJC5EZWZlcnJlZCgpXG4gICAgICAgZmVjaFBsID0gZmF2b3JpdGVQbC5mZXRjaCgpO1xuXG4gICAgICAgZmVjaFBsLmRvbmUgKHJlcyktPlxuICAgICAgICAgaWYgcmVzLmNvZGUgaXMgMFxuICAgICAgICAgIEZhdm9yaXRlUGxheWxpc3QuTGlzdCA9IGZhdm9yaXRlUGxcbiAgICAgICAgICBGYXZvcml0ZVBsYXlsaXN0LmRlZmVyLnJlc29sdmVXaXRoIGZlY2hQbCxbRmF2b3JpdGVQbGF5bGlzdC5MaXN0XVxuICAgICAgICAgZWxzZVxuICAgICAgICAgIEZhdm9yaXRlUGxheWxpc3QuaXNMb2FkID0gZmFsc2VcbiAgICAgICAgICBGYXZvcml0ZVBsYXlsaXN0LmRlZmVyLnJlamVjdFdpdGggZmVjaFBsLGFyZ3VtZW50c1xuICAgICAgIGZlY2hQbC5mYWlsIC0+XG4gICAgICAgICBGYXZvcml0ZVBsYXlsaXN0LmlzTG9hZCA9IGZhbHNlXG4gICAgICAgICBGYXZvcml0ZVBsYXlsaXN0LmRlZmVyLnJlamVjdFdpdGggZmVjaFBsLGFyZ3VtZW50c1xuXG4gICAgICAgcmV0dXJuIEZhdm9yaXRlUGxheWxpc3QuZGVmZXIucHJvbWlzZSgpXG5cblxuXG4gICAgQVBJID1cbiAgICAgICBnZXRQbGF5bGlzdDogLT5cbiAgICAgICAgIGlmIEZhdm9yaXRlUGxheWxpc3QuTGlzdCBpcyB1bmRlZmluZWRcbiAgICAgICAgICByZXR1cm4gaW5pdGlhbGl6ZVBsYXlsaXN0cygpXG4gICAgICAgICByZXR1cm4gRmF2b3JpdGVQbGF5bGlzdC5MaXN0XG5cblxuXG4gICAgYXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6ZmF2b3JpdGU6cGxheWxpc3RzJywgLT5cbiAgICAgICByZXR1cm4gQVBJLmdldFBsYXlsaXN0KClcblxuXG4gICAgZG8gQVBJLmdldFBsYXlsaXN0XG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvZmF2b3JpdGVfcGxheV9saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSBbJy4uL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvdHJhY2snXSwgKEN1c3RvbVRyYWNrKS0+XG5cdEZhdm9yaXRlVHJhY2tzID0ge31cblx0Y2xhc3MgRmF2b3JpdGVUcmFja3MuSXRlbSBleHRlbmRzIEN1c3RvbVRyYWNrLk1vZGVsXG5cdFx0ZGVsZXRlVXJsOiAtPlxuXHRcdFx0YXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9mYXZvdXJpdGVzL3JlbW92ZWl0ZW1cIixcIj9jb250ZW50aWQ9I3tAaWR9JmNvbnRlbnR0eXBlPVRSQ0tcIilcblxuXHRcdHVwZGF0ZVVybDogLT5cblx0XHRcdGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2NvbXBhdGliaWxpdHkvZmF2b3VyaXRlcy9hZGRpdGVtXCIsXCI/Y29udGVudGlkPSN7QGlkfSZjb250ZW50dHlwZT1UUkNLXCIpXG5cdFx0ZGVmYXVsdHM6XG5cdFx0XHRkdXJhdGlvbjogJydcblx0XHRwYXJzZTogKG1vZGVsKS0+XG5cdFx0XHRkYXRhID0gbW9kZWxcblx0XHRcdGlmIGRhdGEuaXNfZGVsZXRlZFxuXHRcdFx0XHRkYXRhLmZhdm91cml0ZXMuc29ydCA9ICtkYXRhLmZhdm91cml0ZXMuc29ydCAtIDEwMDAwMFxuXG5cdFx0XHRkYXRhLnVuaXEgPSAndXNlci0nK21vZGVsLmlkXG5cblx0XHRcdGRhdGEub3B0aW9uc0xpc3QgPSBbXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5vcHRpb25zLmFkZFRQbFxuXHRcdFx0XHRcdGNsYXNzOiAnYWRkLXRvLXBsYXlsaXN0J1xuXHRcdFx0XHR9LHtcblx0XHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5vcHRpb25zLnRvQXJ0XG5cdFx0XHRcdFx0Y2xhc3M6ICdvcHRpb25zLW9wZW4tYXJ0aXN0J1xuXHRcdFx0XHR9LHtcblx0XHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5vcHRpb25zLnRvQWxiXG5cdFx0XHRcdFx0Y2xhc3M6ICdvcHRpb25zLW9wZW4tYWxidW0nXG5cdFx0XHRcdH1cblx0XHRcdF1cblxuXHRcdFx0cmV0dXJuIGRhdGFcblxuXHRjbGFzcyBGYXZvcml0ZVRyYWNrcy5JdGVtcyBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb25cblx0XHRtb2RlbDogRmF2b3JpdGVUcmFja3MuSXRlbVxuXHRcdHVybDogYXBwLmdldFJlcXVlc3RVcmwoXCIvY29tcGF0aWJpbGl0eS9mYXZvdXJpdGVzL2dldGl0ZW1zXCIsXCI/dXNlcmlkPSN7YXBwLnVzZXIudWlkfSZjb250ZW50dHlwZT1UUkNLJlwiKVxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24udHJhY2tzXG5cblx0XHRjb21wYXJhdG9yOiAoaXRlbSktPlxuXHRcdFx0cmV0dXJuIC1pdGVtLmdldCgnZmF2b3VyaXRlcycpLnNvcnRcblxuXG5cdGluaXRpYWxpemVUcmFja3MgPSAtPlxuXHRcdGlmIEZhdm9yaXRlVHJhY2tzLmlzTG9hZGVkXG5cdFx0XHRyZXR1cm4gRmF2b3JpdGVUcmFja3MuZGVmZXIucHJvbWlzZSgpXG5cblx0XHRGYXZvcml0ZVRyYWNrcy5pc0xvYWRlZCA9IHRydWVcblxuXHRcdGZhdm9yaXRlVEwgPSBuZXcgRmF2b3JpdGVUcmFja3MuSXRlbXMoKTtcblx0XHRGYXZvcml0ZVRyYWNrcy5kZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZlY2hUcmFja3MgPSBmYXZvcml0ZVRMLmZldGNoKCk7XG5cdFx0aWYgZmVjaFRyYWNrc1xuXHRcdFx0ZmVjaFRyYWNrcy5kb25lIChyZXMpLT5cblx0XHRcdFx0aWYgcmVzLmNvZGUgaXMgMFxuXHRcdFx0XHRcdEZhdm9yaXRlVHJhY2tzLkxpc3QgPSBmYXZvcml0ZVRMXG5cdFx0XHRcdFx0RmF2b3JpdGVUcmFja3MuZGVmZXIucmVzb2x2ZVdpdGggZmVjaFRyYWNrcyxbRmF2b3JpdGVUcmFja3MuTGlzdF1cblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdEZhdm9yaXRlVHJhY2tzLmlzTG9hZGVkID0gZmFsc2Vcblx0XHRcdFx0XHRGYXZvcml0ZVRyYWNrcy5kZWZlci5yZWplY3RXaXRoIGZlY2hUcmFja3MsIGFyZ3VtZW50c1xuXG5cdFx0XHRmZWNoVHJhY2tzLmZhaWwgLT5cblx0XHRcdFx0RmF2b3JpdGVUcmFja3MuaXNMb2FkZWQgPSBmYWxzZVxuXHRcdFx0XHRGYXZvcml0ZVRyYWNrcy5kZWZlci5yZWplY3RXaXRoIGZlY2hUcmFja3MsYXJndW1lbnRzXG5cblx0XHRcdHJldHVybiBGYXZvcml0ZVRyYWNrcy5kZWZlci5wcm9taXNlKClcblxuXG5cblx0QVBJID1cblx0XHRnZXRUcmFja3M6IC0+XG5cdFx0XHRpZiBGYXZvcml0ZVRyYWNrcy5MaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVRyYWNrcygpXG5cdFx0XHRkZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdFx0c2V0SW1tZWRpYXRlIC0+XG5cdFx0XHRcdGRlZmVyLnJlc29sdmUgRmF2b3JpdGVUcmFja3MuTGlzdFxuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xuXG5cblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpmYXZvcml0ZTp0cmFja3MnLCAtPlxuXHRcdHJldHVybiBBUEkuZ2V0VHJhY2tzKClcblxuXG5cdGRvIEFQSS5nZXRUcmFja3NcblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9mYXZvcml0ZV90cmFja19saXN0LmNvZmZlZVxuICoqLyIsInZhciBuZXh0VGljayA9IHJlcXVpcmUoJ3Byb2Nlc3MvYnJvd3Nlci5qcycpLm5leHRUaWNrO1xudmFyIGFwcGx5ID0gRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5O1xudmFyIHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlO1xudmFyIGltbWVkaWF0ZUlkcyA9IHt9O1xudmFyIG5leHRJbW1lZGlhdGVJZCA9IDA7XG5cbi8vIERPTSBBUElzLCBmb3IgY29tcGxldGVuZXNzXG5cbmV4cG9ydHMuc2V0VGltZW91dCA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4gbmV3IFRpbWVvdXQoYXBwbHkuY2FsbChzZXRUaW1lb3V0LCB3aW5kb3csIGFyZ3VtZW50cyksIGNsZWFyVGltZW91dCk7XG59O1xuZXhwb3J0cy5zZXRJbnRlcnZhbCA9IGZ1bmN0aW9uKCkge1xuICByZXR1cm4gbmV3IFRpbWVvdXQoYXBwbHkuY2FsbChzZXRJbnRlcnZhbCwgd2luZG93LCBhcmd1bWVudHMpLCBjbGVhckludGVydmFsKTtcbn07XG5leHBvcnRzLmNsZWFyVGltZW91dCA9XG5leHBvcnRzLmNsZWFySW50ZXJ2YWwgPSBmdW5jdGlvbih0aW1lb3V0KSB7IHRpbWVvdXQuY2xvc2UoKTsgfTtcblxuZnVuY3Rpb24gVGltZW91dChpZCwgY2xlYXJGbikge1xuICB0aGlzLl9pZCA9IGlkO1xuICB0aGlzLl9jbGVhckZuID0gY2xlYXJGbjtcbn1cblRpbWVvdXQucHJvdG90eXBlLnVucmVmID0gVGltZW91dC5wcm90b3R5cGUucmVmID0gZnVuY3Rpb24oKSB7fTtcblRpbWVvdXQucHJvdG90eXBlLmNsb3NlID0gZnVuY3Rpb24oKSB7XG4gIHRoaXMuX2NsZWFyRm4uY2FsbCh3aW5kb3csIHRoaXMuX2lkKTtcbn07XG5cbi8vIERvZXMgbm90IHN0YXJ0IHRoZSB0aW1lLCBqdXN0IHNldHMgdXAgdGhlIG1lbWJlcnMgbmVlZGVkLlxuZXhwb3J0cy5lbnJvbGwgPSBmdW5jdGlvbihpdGVtLCBtc2Vjcykge1xuICBjbGVhclRpbWVvdXQoaXRlbS5faWRsZVRpbWVvdXRJZCk7XG4gIGl0ZW0uX2lkbGVUaW1lb3V0ID0gbXNlY3M7XG59O1xuXG5leHBvcnRzLnVuZW5yb2xsID0gZnVuY3Rpb24oaXRlbSkge1xuICBjbGVhclRpbWVvdXQoaXRlbS5faWRsZVRpbWVvdXRJZCk7XG4gIGl0ZW0uX2lkbGVUaW1lb3V0ID0gLTE7XG59O1xuXG5leHBvcnRzLl91bnJlZkFjdGl2ZSA9IGV4cG9ydHMuYWN0aXZlID0gZnVuY3Rpb24oaXRlbSkge1xuICBjbGVhclRpbWVvdXQoaXRlbS5faWRsZVRpbWVvdXRJZCk7XG5cbiAgdmFyIG1zZWNzID0gaXRlbS5faWRsZVRpbWVvdXQ7XG4gIGlmIChtc2VjcyA+PSAwKSB7XG4gICAgaXRlbS5faWRsZVRpbWVvdXRJZCA9IHNldFRpbWVvdXQoZnVuY3Rpb24gb25UaW1lb3V0KCkge1xuICAgICAgaWYgKGl0ZW0uX29uVGltZW91dClcbiAgICAgICAgaXRlbS5fb25UaW1lb3V0KCk7XG4gICAgfSwgbXNlY3MpO1xuICB9XG59O1xuXG4vLyBUaGF0J3Mgbm90IGhvdyBub2RlLmpzIGltcGxlbWVudHMgaXQgYnV0IHRoZSBleHBvc2VkIGFwaSBpcyB0aGUgc2FtZS5cbmV4cG9ydHMuc2V0SW1tZWRpYXRlID0gdHlwZW9mIHNldEltbWVkaWF0ZSA9PT0gXCJmdW5jdGlvblwiID8gc2V0SW1tZWRpYXRlIDogZnVuY3Rpb24oZm4pIHtcbiAgdmFyIGlkID0gbmV4dEltbWVkaWF0ZUlkKys7XG4gIHZhciBhcmdzID0gYXJndW1lbnRzLmxlbmd0aCA8IDIgPyBmYWxzZSA6IHNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKTtcblxuICBpbW1lZGlhdGVJZHNbaWRdID0gdHJ1ZTtcblxuICBuZXh0VGljayhmdW5jdGlvbiBvbk5leHRUaWNrKCkge1xuICAgIGlmIChpbW1lZGlhdGVJZHNbaWRdKSB7XG4gICAgICAvLyBmbi5jYWxsKCkgaXMgZmFzdGVyIHNvIHdlIG9wdGltaXplIGZvciB0aGUgY29tbW9uIHVzZS1jYXNlXG4gICAgICAvLyBAc2VlIGh0dHA6Ly9qc3BlcmYuY29tL2NhbGwtYXBwbHktc2VndVxuICAgICAgaWYgKGFyZ3MpIHtcbiAgICAgICAgZm4uYXBwbHkobnVsbCwgYXJncyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmbi5jYWxsKG51bGwpO1xuICAgICAgfVxuICAgICAgLy8gUHJldmVudCBpZHMgZnJvbSBsZWFraW5nXG4gICAgICBleHBvcnRzLmNsZWFySW1tZWRpYXRlKGlkKTtcbiAgICB9XG4gIH0pO1xuXG4gIHJldHVybiBpZDtcbn07XG5cbmV4cG9ydHMuY2xlYXJJbW1lZGlhdGUgPSB0eXBlb2YgY2xlYXJJbW1lZGlhdGUgPT09IFwiZnVuY3Rpb25cIiA/IGNsZWFySW1tZWRpYXRlIDogZnVuY3Rpb24oaWQpIHtcbiAgZGVsZXRlIGltbWVkaWF0ZUlkc1tpZF07XG59O1xuXG5cbi8qKioqKioqKioqKioqKioqKlxuICoqIFdFQlBBQ0sgRk9PVEVSXG4gKiogKHdlYnBhY2spL34vbm9kZS1saWJzLWJyb3dzZXIvfi90aW1lcnMtYnJvd3NlcmlmeS9tYWluLmpzXG4gKiogbW9kdWxlIGlkID0gOTlcbiAqKiBtb2R1bGUgY2h1bmtzID0gMFxuICoqLyIsIi8vIHNoaW0gZm9yIHVzaW5nIHByb2Nlc3MgaW4gYnJvd3NlclxuXG52YXIgcHJvY2VzcyA9IG1vZHVsZS5leHBvcnRzID0ge307XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBzZXRUaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBjbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBzZXRUaW1lb3V0KGRyYWluUXVldWUsIDApO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuXG5cblxuLyoqKioqKioqKioqKioqKioqXG4gKiogV0VCUEFDSyBGT09URVJcbiAqKiAod2VicGFjaykvfi9ub2RlLWxpYnMtYnJvd3Nlci9+L3Byb2Nlc3MvYnJvd3Nlci5qc1xuICoqIG1vZHVsZSBpZCA9IDEwMFxuICoqIG1vZHVsZSBjaHVua3MgPSAwXG4gKiovIiwiZGVmaW5lIC0+XG5cdEZlZWQgPSB7fVxuXHRjbGFzcyBGZWVkLkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdGRlbGV0ZVVybDogLT5cblx0XHRcdHJldHVybiBhcHAuZ2V0UmVxdWVzdFVybFNpZChcIi9iZWVsaW5lTXVzaWNWMS93YWxsL2RlbGV0ZVJlY29yZFwiLFwiP3JlY29yZElkPSN7QGlkfVwiKVxuXHRcdHBhcnNlOiAoaXRlbSktPlxuXHRcdFx0aXRlbS5jdXJyZW50VXNlciA9IEZlZWQuY3VycmVudFVzZXIudG9KU09OKClcblx0XHRcdHJldHVybiBpdGVtXG5cblx0Y2xhc3MgRmVlZC5MaXN0IGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBGZWVkLkl0ZW1cblx0XHR1cmw6IGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2JlZWxpbmVNdXNpY1YxL3dhbGwvZ2V0QWxsUmVjb3Jkc1wiKVxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24ud2FsbFJlY29yZHNcblxuXHRcdGNvbXBhcmF0b3I6IChhLGIpLT5cblx0XHRcdGRhdGVBID0gbmV3IERhdGUoYS5nZXQoJ2RhdGVfY3JlYXRlJykqMTAwMClcblx0XHRcdGRhdGVCID0gbmV3IERhdGUoYi5nZXQoJ2RhdGVfY3JlYXRlJykqMTAwMClcblx0XHRcdHJldHVybiBpZiBkYXRlQSA8IGRhdGVCIHRoZW4gMSBlbHNlIC0xXG5cblxuXHRpbml0aWFsaXplRmVlZCA9IC0+XG5cdFx0IyBpZiBGZWVkLkxpc3RJc0xvYWRcblx0XHQjIFx0cmV0dXJuIEZlZWQuTGlzdERlZmVyLnByb21pc2UoKVxuXG5cdFx0RmVlZC5MaXN0SXNMb2FkID0gdHJ1ZVxuXG5cdFx0ZmVlZExpc3QgPSBuZXcgRmVlZC5MaXN0XG5cdFx0RmVlZC5MaXN0RGVmZXIgPSAkLkRlZmVycmVkKClcblx0XHRhcHAucmVxdWVzdCgnZ2V0OnVzZXInKS5kb25lICh1c2VyKS0+XG5cdFx0XHRGZWVkLmN1cnJlbnRVc2VyID0gdXNlclxuXHRcdFx0ZmV0Y2ggPSBmZWVkTGlzdC5mZXRjaFxuXHRcdFx0XHRsaW1pdDogMzBcblxuXHRcdFx0aWYgZmV0Y2hcblx0XHRcdFx0ZmV0Y2guZG9uZSAoKS0+XG5cdFx0XHRcdFx0RmVlZC5OZXdMaXN0ID0gZmVlZExpc3Rcblx0XHRcdFx0XHRGZWVkLkxpc3REZWZlci5yZXNvbHZlV2l0aCBmZXRjaCxbRmVlZC5OZXdMaXN0XVxuXHRcdFx0XHRmZXRjaC5mYWlsIC0+XG5cdFx0XHRcdFx0RmVlZC5MaXN0SXNMb2FkID0gZmFsc2Vcblx0XHRcdFx0XHRGZWVkLkxpc3REZWZlci5yZWplY3RXaXRoIGZldGNoLGFyZ3VtZW50c1xuXG5cdFx0cmV0dXJuIEZlZWQuTGlzdERlZmVyLnByb21pc2UoKVxuXG5cdGluaXRpYWxpemVQb3N0TGlzdCA9IC0+XG5cdFx0cmV0dXJuIEZlZWQuUG9zdExpc3QgPSBuZXcgQmFja2JvbmUuQ29sbGVjdGlvblxuXG5cdGluaXRpYWxpemVOZXdNb2RlbCA9IC0+XG5cdFx0Y2xhc3MgbW9kZWwgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdFx0ZGVmYXVsdHM6XG5cdFx0XHRcdG1lc3NhZ2U6ICcnXG5cblx0XHRyZXR1cm4gRmVlZC5OZXdNb2RlbCA9IG5ldyBtb2RlbFxuXG5cblxuXHRBUEkgPVxuXHRcdGdldEZlZWRzOiAoY3VycmVudCktPlxuXHRcdFx0IyBpZiBGZWVkLk5ld0xpc3QgaXMgdW5kZWZpbmVkXG5cdFx0XHRpZiBjdXJyZW50XG5cdFx0XHRcdHJldHVybiBGZWVkLk5ld0xpc3RcblxuXHRcdFx0cmV0dXJuIGluaXRpYWxpemVGZWVkKClcblxuXHRcdGdldEZlZWRQb3N0SXRlbXM6IC0+XG5cdFx0XHRpZiBGZWVkLlBvc3RMaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVBvc3RMaXN0KClcblx0XHRcdHJldHVybiBGZWVkLlBvc3RMaXN0XG5cblx0XHRnZXROZXdGZWVkTW9kZWw6IC0+XG5cdFx0XHRpZiBGZWVkLk5ld01vZGVsIGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZU5ld01vZGVsKClcblx0XHRcdHJldHVybiBGZWVkLk5ld01vZGVsXG5cblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmZlZWRzJywgKGN1cnJlbnQpLT5cblx0XHRyZXR1cm4gQVBJLmdldEZlZWRzKGN1cnJlbnQpXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6ZmVlZDpwb3N0Oml0ZW1zJywgLT5cblx0XHRyZXR1cm4gQVBJLmdldEZlZWRQb3N0SXRlbXMoKVxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0Om5ldzpmZWVkOm1vZGVsJywgLT5cblx0XHRyZXR1cm4gQVBJLmdldE5ld0ZlZWRNb2RlbCgpXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9mZWVkLmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuXHRMb2NhbCA9IHt9XG5cdGNsYXNzIExvY2FsLkdlbnJlIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcblxuXHRjbGFzcyBMb2NhbC5HZW5yaWVzIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBMb2NhbC5HZW5yZVxuXHRcdGxvY2FsU3RvcmFnZTogbmV3IEJhY2tib25lLkxvY2FsU3RvcmFnZSBcIkdlbnJpZXNcIlxuXHRcdHNhdmU6IC0+XG5cdFx0XHRAZWFjaCAobW9kZWwpLT5cblx0XHRcdFx0QmFja2JvbmUubG9jYWxTeW5jIFwidXBkYXRlXCIsIG1vZGVsXG5cblx0XG5cdGluaXRpYWxpemVHZW5yZSA9IC0+XG5cdFx0TG9jYWwuR2Vucmllc0xpc3QgPSBuZXcgTG9jYWwuR2Vucmllc1xuXG5cdFx0ZG8gTG9jYWwuR2Vucmllc0xpc3QuZmV0Y2hcblxuXG5cdEFQSSA9XG5cdFx0Z2V0R2VucmllczogLT5cblx0XHRcdGlmIExvY2FsLkdlbnJpZXNMaXN0IGlzIHVuZGVmaW5lZFxuXHRcdFx0XHRpbml0aWFsaXplR2VucmUoKVxuXG5cdFx0XHRyZXR1cm4gTG9jYWwuR2Vucmllc0xpc3RcblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OmxvY2FsOmdlbnJpZXMnLCAtPlxuXHRcdHJldHVybiBBUEkuZ2V0R2VucmllcygpXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9sb2NhbC5jb2ZmZWVcbiAqKi8iLCJkZWZpbmUgW10sIC0+XG5cdE1lbnUgPSB7fVxuXHRjbGFzcyBNZW51Lkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbC5TZWxlY3RlZFxuXG5cdGNsYXNzIE1lbnUuQ29sbGVjdGlvbiBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb24uU2luZ2xlU2VsZWN0XG5cdFx0bW9kZWw6IE1lbnUuSXRlbVxuXG5cdGNsYXNzIE1lbnUuR2VucmUgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbC5TZWxlY3RlZFxuXG5cdGNsYXNzIE1lbnUuR2VucmllcyBleHRlbmRzIEJhY2tib25lLkNvbGxlY3Rpb24uTXVsdGlTZWxlY3RlZFxuXHRcdG1vZGVsOiBNZW51LkdlbnJlXG5cdFx0dXJsOiAnL2dlbnJpZXMnXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gXy53aGVyZSBjb2xsZWN0aW9uLmdlbnJlcywgJ2lzX3BhcmVudCc6IHRydWVcblxuXHRpbml0aWFsaXplTWVudSA9IC0+XG5cdFx0TWVudS5MaXN0ID0gbmV3IE1lbnUuQ29sbGVjdGlvbihbXG5cdFx0XHQjIHtcblx0XHRcdCMgXHR0aXRsZTogJ9Cg0LDQtNC40L4nLFxuXHRcdFx0IyBcdGhyZWY6ICdyYWRpbycsXG5cdFx0XHQjIFx0Y2xhc3M6ICdtZW51LXJhZGlvJyxcblx0XHRcdCMgXHR0cmlnZ2VyOiAnc2hvdzpyYWRpbydcblx0XHRcdCMgfSxcblx0XHRcdHtcblx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUubWVudS5mZWVkLFxuXHRcdFx0XHRocmVmOiAnZmVlZCcsXG5cdFx0XHRcdGNsYXNzOiAnZmVlZCcsXG5cdFx0XHRcdHRyaWdnZXI6ICdzaG93OmZlZWQnXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5tZW51Lm5ldyxcblx0XHRcdFx0aHJlZjogJ25ldycsXG5cdFx0XHRcdGNsYXNzOiAnbmV3Jyxcblx0XHRcdFx0dHJpZ2dlcjogJ3Nob3c6bmV3J1xuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUubWVudS5wb3B1bGFyLFxuXHRcdFx0XHRocmVmOiAnJyxcblx0XHRcdFx0Y2xhc3M6ICdyZWNvbWVuZCcsXG5cdFx0XHRcdHRyaWdnZXI6ICdzaG93OnJlY29tZW5kJ1xuXHRcdFx0fSxcblx0XHRcdHtcblx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUubWVudS5wbGF5bGlzdHMsXG5cdFx0XHRcdGhyZWY6ICdwbGF5bGlzdHMnLFxuXHRcdFx0XHRjbGFzczogJ3BsYXlsaXN0cycsXG5cdFx0XHRcdHRyaWdnZXI6ICdzaG93OnBsYXlsaXN0cydcblx0XHRcdH0sXG4jXHRcdFx0IHtcbiNcdFx0XHQgXHR0aXRsZTogYXBwLmxvY2FsZS5tZW51LmNoYXJ0cyxcbiNcdFx0XHQgXHRocmVmOiAnY2hhcnRzJyxcbiNcdFx0XHQgXHRjbGFzczogJ2NoYXJ0cycsXG4jXHRcdFx0IFx0dHJpZ2dlcjogJ3Nob3c6Y2hhcnRzJ1xuI1x0XHRcdCB9LFxuXHRcdFx0e1xuXHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5tZW51LmNsaXBzLFxuXHRcdFx0XHRocmVmOiAndmlkZW8nLFxuXHRcdFx0XHRjbGFzczogJ3ZpZGVvJyxcblx0XHRcdFx0dHJpZ2dlcjogJ3Nob3c6dmlkZW8nXG5cdFx0XHR9XG5cdFx0XSk7XG5cblxuXG5cblxuXG5cdEFQSSA9XG5cdFx0Z2V0TWVudXM6IC0+XG5cdFx0XHRpZiBNZW51Lkxpc3QgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdGluaXRpYWxpemVNZW51KClcblx0XHRcdHJldHVybiBNZW51Lkxpc3RcblxuXHRcdGdldEdlbnJpZXM6IC0+XG5cdFx0XHRnYW5yaWVzID0gbmV3IE1lbnUuR2Vucmllc1xuXG5cdFx0XHRkZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdFx0ZmV0Y2ggPSBkbyBnYW5yaWVzLmZldGNoXG5cblx0XHRcdGZldGNoLmRvbmUgLT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggZmV0Y2gsIFtnYW5yaWVzXVxuXG5cdFx0XHRmZXRjaC5mYWlsIC0+XG5cdFx0XHRcdGRlZmVyLnJlamVjdFdpdGggZmV0Y2gsIGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDptZW51JywgLT5cblx0XHRyZXR1cm4gQVBJLmdldE1lbnVzKClcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpnZW5yaWVzJywgLT5cblx0XHRyZXR1cm4gQVBJLmdldEdlbnJpZXMoKVxuXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy9tZW51X2xpc3QuY29mZmVlXG4gKiovIiwiZGVmaW5lIC0+XG4gICAgUGxheWxpc3RzTGlzdCA9IHt9XG4gICAgY2xhc3MgUGxheWxpc3RzTGlzdC5JdGVtIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcbiAgICAgICAgdXJsUm9vdDogLT5cbiAgICAgICAgICAgIGFwcC5nZXRSZXF1ZXN0VXJsKFwiL3BsYXlsaXN0L2dldFwiLFwiP3BsYXlsaXN0aWQ9I3tAaWR9Jml0ZW1zX29uX3BhZ2U9MTAwMFwiKVxuXG4gICAgICAgIHBhcnNlOiAobW9kZWwpLT5cbiAgICAgICAgICAgIGlmIG1vZGVsLmFsYnVtXG4gICAgICAgICAgICAgICAgbW9kZWwuYWxidW0uYWxidW0gPSB0cnVlXG4gICAgICAgICAgICAgICAgcmV0dXJuIG1vZGVsLmFsYnVtXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgbW9kZWwuYWxidW0gPSB0cnVlXG4gICAgICAgICAgICAgICAgcmV0dXJuIG1vZGVsXG5cbiAgICBjbGFzcyBQbGF5bGlzdHNMaXN0Lkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuICAgICAgICBtb2RlbDogUGxheWxpc3RzTGlzdC5JdGVtXG4gICAgICAgIHVybDogYXBwLmdldFJlcXVlc3RVcmwoXCIvYmVlbGluZU11c2ljVjEvcGxheWxpc3QvdG9wXCIpXG4gICAgICAgIHBhcnNlOiAoY29sbGVjdGlvbiktPlxuICAgICAgICAgICAgcmV0dXJuIGNvbGxlY3Rpb24uc2VhcmNoZWRcblxuXG4gICAgaW5pdGlhbGl6ZVBsYXlsaXN0cyA9IC0+XG4gICAgICAgIGlmIFBsYXlsaXN0c0xpc3QuTGlzdElzTG9hZFxuICAgICAgICAgICAgcmV0dXJuIFBsYXlsaXN0c0xpc3QuTGlzdERlZmVyLnByb21pc2UoKVxuXG4gICAgICAgIFBsYXlsaXN0c0xpc3QuTGlzdElzTG9hZCA9IHRydWVcblxuICAgICAgICBwbGF5bGlzdHMgPSBuZXcgUGxheWxpc3RzTGlzdC5JdGVtcygpO1xuICAgICAgICBQbGF5bGlzdHNMaXN0Lkxpc3REZWZlciA9ICQuRGVmZXJyZWQoKVxuICAgICAgICBmZXRjaCA9IHBsYXlsaXN0cy5mZXRjaFxuICAgICAgICAgICAgZGF0YTpcbiAgICAgICAgICAgICAgICBjb3VudDogNjBcbiAgICAgICAgICAgICAgICBvZmZzZXQ6IDBcblxuICAgICAgICBpZiBmZXRjaFxuICAgICAgICAgICAgZmV0Y2guZG9uZSAoKS0+XG4gICAgICAgICAgICAgICAgUGxheWxpc3RzTGlzdC5MaXN0ID0gcGxheWxpc3RzXG4gICAgICAgICAgICAgICAgUGxheWxpc3RzTGlzdC5MaXN0RGVmZXIucmVzb2x2ZVdpdGggZmV0Y2gsW1BsYXlsaXN0c0xpc3QuTGlzdF1cbiAgICAgICAgICAgIGZldGNoLmZhaWwgLT5cbiAgICAgICAgICAgICAgICBQbGF5bGlzdHNMaXN0Lkxpc3RJc0xvYWQgPSBmYWxzZVxuICAgICAgICAgICAgICAgIFBsYXlsaXN0c0xpc3QuTGlzdERlZmVyLnJlamVjdFdpdGggZmV0Y2gsYXJndW1lbnRzXG5cbiAgICAgICAgICAgIHJldHVybiBQbGF5bGlzdHNMaXN0Lkxpc3REZWZlci5wcm9taXNlKClcblxuXG4gICAgaW5pdGlhbGl6ZVNpbmdsZVBsYXlsaXN0ID0gKGlkKS0+XG4gICAgICAgIFBsYXlsaXN0c0xpc3QuU2luZ2xlUGxheWxpc3QgPSBuZXcgUGxheWxpc3RzTGlzdC5JdGVtXG4gICAgICAgICAgICBpZDogaWRcbiAgICAgICAgZGVmZXIgPSAkLkRlZmVycmVkKClcbiAgICAgICAgZmV0Y2ggPSBQbGF5bGlzdHNMaXN0LlNpbmdsZVBsYXlsaXN0LmZldGNoKClcblxuICAgICAgICBpZiBmZXRjaFxuICAgICAgICAgICAgZmV0Y2guZG9uZSAocmVzKS0+XG4gICAgICAgICAgICAgICAgaWYgcmVzLmNvZGUgaXMgMFxuICAgICAgICAgICAgICAgICAgICBQbGF5bGlzdHNMaXN0LlBsYXlsaXN0SWQgPSBpZFxuICAgICAgICAgICAgICAgICAgICBkZWZlci5yZXNvbHZlV2l0aCBmZXRjaCxbUGxheWxpc3RzTGlzdC5TaW5nbGVQbGF5bGlzdF1cbiAgICAgICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgICAgIGRlZmVyLnJlamVjdFdpdGggZmV0Y2gsW3Jlc11cbiAgICAgICAgICAgIGZldGNoLmZhaWwgLT5cbiAgICAgICAgICAgICAgICBkZWZlci5yZWplY3RXaXRoIGZldGNoLGFyZ3VtZW50c1xuXG4gICAgICAgICAgICByZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblxuXG4gICAgQVBJID1cbiAgICAgICAgZ2V0UGxheWxpc3RzOiAtPlxuICAgICAgICAgICAgaWYgUGxheWxpc3RzTGlzdC5MaXN0IGlzIHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgIHJldHVybiBpbml0aWFsaXplUGxheWxpc3RzKClcbiAgICAgICAgICAgIHJldHVybiBQbGF5bGlzdHNMaXN0Lkxpc3RcblxuICAgICAgICBnZXRQbGF5bGlzdDogKGlkKS0+XG4gICAgICAgICAgICBpZiBQbGF5bGlzdHNMaXN0LlBsYXlsaXN0SWQgaXMgaWRcbiAgICAgICAgICAgICAgICByZXR1cm4gUGxheWxpc3RzTGlzdC5TaW5nbGVQbGF5bGlzdFxuXG4gICAgICAgICAgICBlbHNlXG4gICAgICAgICAgICAgICAgcmV0dXJuIGluaXRpYWxpemVTaW5nbGVQbGF5bGlzdCBpZFxuXG4gICAgICAgIGdldFRlbXBQbGF5bGlzdHM6IChjb2xsZWN0aW9uKS0+XG4gICAgICAgICAgICByZXR1cm4gbmV3IFBsYXlsaXN0c0xpc3QuSXRlbXMgY29sbGVjdGlvblxuXG5cblxuICAgIGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnBsYXlsaXN0cycsIC0+XG4gICAgICAgIHJldHVybiBBUEkuZ2V0UGxheWxpc3RzKClcblxuICAgIGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnBsYXlsaXN0JywgKGlkKS0+XG4gICAgICAgIHJldHVybiBBUEkuZ2V0UGxheWxpc3QgaWRcblxuICAgIGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnRlbXA6cGxheWxpc3RzJywgKGNvbGxlY3Rpb24pLT5cbiAgICAgICAgcmV0dXJuIEFQSS5nZXRUZW1wUGxheWxpc3RzIGNvbGxlY3Rpb24udG9KU09OKClcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3BsYXlsaXN0c19saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuICAgIE1lbnUgPSB7fVxuICAgIGNsYXNzIE1lbnUuSXRlbSBleHRlbmRzIEJhY2tib25lLk1vZGVsLlNlbGVjdGVkXG5cbiAgICBjbGFzcyBNZW51LkNvbGxlY3Rpb24gZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uLlNpbmdsZVNlbGVjdFxuICAgICAgICBtb2RlbDogTWVudS5JdGVtXG5cbiAgICBpbml0aWFsaXplTWVudSA9IC0+XG4gICAgICAgIE1lbnUuTGlzdCA9IG5ldyBNZW51LkNvbGxlY3Rpb24oW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIHRpdGxlOiBhcHAubG9jYWxlLnByb2ZpbGUubWVudS5tYWluXG4gICAgICAgICAgICAgICAgdGFiOiAnbWFpbidcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGl0bGU6IGFwcC5sb2NhbGUucHJvZmlsZS5tZW51LmFjY2Vzc1xuICAgICAgICAgICAgICAgIHRhYjogJ3N1YnNjcmliZSdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgdGl0bGU6IGFwcC5sb2NhbGUucHJvZmlsZS5tZW51LnByb21vY29kZVxuICAgICAgICAgICAgICAgIHRhYjogJ3Byb21vJ1xuICAgICAgICAgICAgfSx7XG4gICAgICAgICAgICAgICAgdGl0bGU6IGFwcC5sb2NhbGUucHJvZmlsZS5tZW51LmZvbGxvd2Vyc1xuICAgICAgICAgICAgICAgIHRhYjogJ2ZvbGxvd2VycydcbiAgICAgICAgICAgIH0se1xuICAgICAgICAgICAgICAgIHRpdGxlOiBhcHAubG9jYWxlLnByb2ZpbGUubWVudS5mb2xsb3dlZFxuICAgICAgICAgICAgICAgIHRhYjogJ2ZvbGxvd2VkJ1xuICAgICAgICAgICAgfVxuICAgICAgICBdKTtcblxuXG5cblxuICAgIEFQSSA9XG4gICAgICAgIGdldE1lbnVzOiAtPlxuICAgICAgICAgICAgaWYgTWVudS5MaXN0IGlzIHVuZGVmaW5lZFxuICAgICAgICAgICAgICAgIGluaXRpYWxpemVNZW51KClcbiAgICAgICAgICAgIHJldHVybiBNZW51Lkxpc3RcblxuXG5cbiAgICBhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpwcm9maWxlOm1lbnUnLCAtPlxuICAgICAgICByZXR1cm4gQVBJLmdldE1lbnVzKClcblxuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvcHJvZmlsZV9tZW51LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuICBSZWNvbWVuZCA9IHt9XG4gIGNsYXNzIFJlY29tZW5kLk1haW4gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuICAgIHVybFJvb3Q6IGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2JlZWxpbmVNdXNpY1YxL21haW4tcGFnZS9nZXRNYWluQmxvY2tcIilcbiAgICBwYXJzZTogKG1vZGVsKS0+XG4gICAgICBpZiBtb2RlbC50b2RheV9iZXN0P1xuICAgICAgICByZXR1cm4gbW9kZWwudG9kYXlfYmVzdFxuICAgICAgZWxzZVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGlzRW1wdHk6IHRydWVcbiAgICAgICAgfVxuICAgICAgXG4gIGNsYXNzIFJlY29tZW5kLkJsb2NrcyBleHRlbmRzIEJhY2tib25lLk1vZGVsXG4gICAgdXJsUm9vdDogYXBwLmdldFJlcXVlc3RVcmwoXCIvYmVlbGluZU11c2ljVjEvbWFpbi1wYWdlL2dldEFkZGl0aW9uYWxCbG9ja3NcIilcbiAgICBwYXJzZTogKG1vZGVsKS0+XG4gICAgICByZXR1cm4gbW9kZWwuYmxvY2tzXG5cbiAgaW5pdGlhbGl6ZU1haW4gPSAtPlxuICAgIGlmIFJlY29tZW5kLk1haW5CbG9ja0lzTG9hZFxuICAgICAgcmV0dXJuIFJlY29tZW5kLk1haW5CbG9ja0RlZmVyLnByb21pc2UoKVxuXG4gICAgUmVjb21lbmQuTWFpbkJsb2NrSXNMb2FkID0gdHJ1ZVxuXG4gICAgbWFpbiA9IG5ldyBSZWNvbWVuZC5NYWluKCk7XG4gICAgUmVjb21lbmQuTWFpbkJsb2NrRGVmZXIgPSAkLkRlZmVycmVkKClcbiAgICBmZXRjaE1haW4gPSBtYWluLmZldGNoKCk7XG5cbiAgICBmZXRjaE1haW4uZG9uZSAtPlxuICAgICAgUmVjb21lbmQuTWFpbkJsb2NrID0gbWFpblxuICAgICAgUmVjb21lbmQuTWFpbkJsb2NrRGVmZXIucmVzb2x2ZVdpdGggZmV0Y2hNYWluLFtSZWNvbWVuZC5NYWluQmxvY2tdXG5cblxuICAgIGZldGNoTWFpbi5mYWlsIC0+XG4gICAgICBSZWNvbWVuZC5NYWluQmxvY2tEZWZlci5yZWplY3RXaXRoIGZldGNoTWFpbixhcmd1bWVudHNcbiAgICAgIFJlY29tZW5kLk1haW5CbG9ja0lzTG9hZCA9IGZhbHNlXG5cbiAgICByZXR1cm4gUmVjb21lbmQuTWFpbkJsb2NrRGVmZXIucHJvbWlzZSgpXG4gICAgXG4gIGluaXRpYWxpemVCbG9ja3MgPSAtPlxuICAgIGlmIFJlY29tZW5kLkJsb2Nrc0lzTG9hZFxuICAgICAgcmV0dXJuIFJlY29tZW5kLkJsb2Nrc0RlZmVyLnByb21pc2UoKVxuXG4gICAgUmVjb21lbmQuQmxvY2tzSXNMb2FkID0gdHJ1ZVxuXG4gICAgYmxvY2tzID0gbmV3IFJlY29tZW5kLkJsb2NrcygpO1xuICAgIFJlY29tZW5kLkJsb2Nrc0RlZmVyID0gJC5EZWZlcnJlZCgpXG4gICAgZmV0Y2ggPSBibG9ja3MuZmV0Y2goKTtcblxuICAgIGZldGNoLmRvbmUgLT5cbiAgICAgIFJlY29tZW5kLkJsb2Nrc0l0ZW0gPSBibG9ja3NcbiAgICAgIFJlY29tZW5kLkJsb2Nrc0RlZmVyLnJlc29sdmVXaXRoIGZldGNoLFtSZWNvbWVuZC5CbG9ja3NJdGVtXVxuXG5cbiAgICBmZXRjaC5mYWlsIC0+XG4gICAgICBSZWNvbWVuZC5CbG9ja3NEZWZlci5yZWplY3RXaXRoIGZldGNoLGFyZ3VtZW50c1xuICAgICAgUmVjb21lbmQuQmxvY2tzSXNMb2FkID0gZmFsc2VcblxuICAgIHJldHVybiBSZWNvbWVuZC5CbG9ja3NEZWZlci5wcm9taXNlKClcblxuXG5cblxuXG4gIEFQSSA9XG4gICAgZ2V0TWFpbjogLT5cbiAgICAgIGlmIFJlY29tZW5kLk1haW5CbG9jayBpcyB1bmRlZmluZWRcbiAgICAgICAgcmV0dXJuIGluaXRpYWxpemVNYWluKClcbiAgICAgIHJldHVybiBSZWNvbWVuZC5NYWluQmxvY2tcblxuICAgIGdldEJsb2NrczogLT5cbiAgICAgIGlmIFJlY29tZW5kLkJsb2Nrc0l0ZW0gaXMgdW5kZWZpbmVkXG4gICAgICAgIHJldHVybiBpbml0aWFsaXplQmxvY2tzKClcbiAgICAgIHJldHVybiBSZWNvbWVuZC5CbG9ja3NJdGVtXG5cbiAgYXBwLnJlcXJlcy5zZXRIYW5kbGVyICdnZXQ6cmVjb21lbmQ6bWFpbicsIC0+XG4gICAgcmV0dXJuIEFQSS5nZXRNYWluKClcblxuICBhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpyZWNvbWVuZDpibG9ja3MnLCAtPlxuICAgIHJldHVybiBBUEkuZ2V0QmxvY2tzKClcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3JlY29tZW5kX3BhZ2UuY29mZmVlXG4gKiovIiwiZGVmaW5lIFsnLi4vY29uZmlnL2N1c3RvbV9lbGVtZW50cy90cmFjaycsJy4uL2NvbmZpZy9jdXN0b21fZWxlbWVudHMvYWxidW0nXSwgKEN1c3RvbVRyYWNrLEN1c3RvbUFsYnVtKS0+XG5cdFNlYXJjaExpc3QgPSB7fVxuXHRcblx0Y2xhc3MgU2VhcmNoTGlzdC5UcmFjayBleHRlbmRzIEN1c3RvbVRyYWNrLk1vZGVsXG5cdFx0cGFyc2U6IChpdGVtKS0+XG5cdFx0XHRpdGVtLnVuaXEgPSAnc2VhcmNoLScraXRlbS5pZFxuXHRcdFx0aXRlbS5vcHRpb25zTGlzdCA9IFtcblx0XHRcdFx0e1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMuYWRkVFBsXG5cdFx0XHRcdFx0Y2xhc3M6ICdhZGQtdG8tcGxheWxpc3QnXG5cdFx0XHRcdH0se1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BcnRcblx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hcnRpc3QnXG5cdFx0XHRcdFx0YXJ0aXN0OiB0cnVlXG5cdFx0XHRcdH0se1xuXHRcdFx0XHRcdHRpdGxlOiBhcHAubG9jYWxlLm9wdGlvbnMudG9BbGJcblx0XHRcdFx0XHRjbGFzczogJ29wdGlvbnMtb3Blbi1hbGJ1bSdcblx0XHRcdFx0fVxuXHRcdFx0XVxuXHRcdFx0cmV0dXJuIGl0ZW1cblxuXHRjbGFzcyBTZWFyY2hMaXN0LkFsYnVtIGV4dGVuZHMgQ3VzdG9tQWxidW0uTW9kZWxcblx0XHR1cmxSb290OiAtPlxuXHRcdFx0cmV0dXJuIGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2NvbXBhdGliaWxpdHkvY2F0YWxvZ3VlL2dldEFsYnVtXCIsXCI/YWxidW1pZD0je0BpZH1cIilcblx0XHRwYXJzZTogKG1vZGVsKS0+XG5cdFx0XHRpZiBtb2RlbC5hbGJ1bVxuXHRcdFx0XHRyZXR1cm4gbW9kZWwuYWxidW1cblx0XHRcdGVsc2Vcblx0XHRcdFx0cmV0dXJuIG1vZGVsXG5cblx0Y2xhc3MgU2VhcmNoTGlzdC5BcnRpc3QgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6ICcvYXJ0aXN0J1xuXG5cdGNsYXNzIFNlYXJjaExpc3QuUGxheWxpc3QgZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6ICcvcGxheWxpc3QnXG5cblx0Y2xhc3MgU2VhcmNoTGlzdC5UcmFja3NDb2xsZWN0aW9uIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBTZWFyY2hMaXN0LlRyYWNrXG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybCgnL3NlYXJjaC90cmFja3MnKVxuXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gY29sbGVjdGlvbi5zZWFyY2hlZFxuXG5cblx0Y2xhc3MgU2VhcmNoTGlzdC5BbGJ1bXNDb2xsZWN0aW9uIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBTZWFyY2hMaXN0LkFsYnVtXG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybCgnL3NlYXJjaC9hbGJ1bXMnKVxuXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gY29sbGVjdGlvbi5zZWFyY2hlZFxuXG5cdGNsYXNzIFNlYXJjaExpc3QuQXJ0aXN0c0NvbGxlY3Rpb24gZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG5cdFx0bW9kZWw6IFNlYXJjaExpc3QuQXJ0aXN0XG5cdFx0dXJsOiBhcHAuZ2V0UmVxdWVzdFVybCgnL3NlYXJjaC9hcnRpc3RzJylcblxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24uc2VhcmNoZWRcblxuXHRjbGFzcyBTZWFyY2hMaXN0LlBsYXlsaXN0c0NvbGxlY3Rpb24gZXh0ZW5kcyBCYWNrYm9uZS5Db2xsZWN0aW9uXG5cdFx0bW9kZWw6IFNlYXJjaExpc3QuUGxheWxpc3Rcblx0XHR1cmw6IHVybDogYXBwLmdldFJlcXVlc3RVcmwoJy9zZWFyY2gvdHJhY2tzJylcblxuXHRcdHBhcnNlOiAoY29sbGVjdGlvbiktPlxuXHRcdFx0cmV0dXJuIGNvbGxlY3Rpb24uc2VhcmNoZWRcblxuXG5cdGluaXRpYWxpemVUcmFja3MgPSAob3B0aW9ucyktPlxuXHRcdGlmIFNlYXJjaExpc3QuZmV0Y2hUcmFja3NDb2xsZWN0aW9uXG5cdFx0XHRTZWFyY2hMaXN0LmZldGNoVHJhY2tzQ29sbGVjdGlvbi5hYm9ydCgpXG5cblx0XHRTZWFyY2hMaXN0LlRyYWNrcyA9IG5ldyBTZWFyY2hMaXN0LlRyYWNrc0NvbGxlY3Rpb25cblx0XHRvcHRpb25zIHx8IChvcHRpb25zID0ge30pO1xuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpO1xuXHRcdFNlYXJjaExpc3QuZmV0Y2hUcmFja3NDb2xsZWN0aW9uID0gU2VhcmNoTGlzdC5UcmFja3MuZmV0Y2goXy5vbWl0KG9wdGlvbnMsJ3N1Y2Nlc3MnLCdlcnJvcicpKTtcblx0XHRTZWFyY2hMaXN0LmZldGNoVHJhY2tzQ29sbGVjdGlvbi5kb25lIC0+XG5cdFx0XHRpZiBTZWFyY2hMaXN0LmZldGNoVHJhY2tzQ29sbGVjdGlvbi5yZXNwb25zZUpTT05cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggU2VhcmNoTGlzdC5mZXRjaFRyYWNrc0NvbGxlY3Rpb24sW1NlYXJjaExpc3QuVHJhY2tzLFNlYXJjaExpc3QuZmV0Y2hUcmFja3NDb2xsZWN0aW9uLnJlc3BvbnNlSlNPTi5jb3VudF1cblxuXHRcdFNlYXJjaExpc3QuZmV0Y2hUcmFja3NDb2xsZWN0aW9uLmZhaWwgLT5cblx0XHRcdGRlZmVyLnJlamVjdFdpdGggU2VhcmNoTGlzdC5mZXRjaFRyYWNrc0NvbGxlY3Rpb24sYXJndW1lbnRzXG5cblx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xuXG5cdGluaXRpYWxpemVBbGJ1bXMgPSAob3B0aW9ucyktPlxuXHRcdGlmIFNlYXJjaExpc3QuZmV0Y2hBbGJ1bXNDb2xsZWN0aW9uXG5cdFx0XHRTZWFyY2hMaXN0LmZldGNoQWxidW1zQ29sbGVjdGlvbi5hYm9ydCgpXG5cblx0XHRTZWFyY2hMaXN0LkFsYnVtcyA9IG5ldyBTZWFyY2hMaXN0LkFsYnVtc0NvbGxlY3Rpb25cblx0XHRvcHRpb25zIHx8IChvcHRpb25zID0ge30pO1xuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpO1xuXHRcdFNlYXJjaExpc3QuZmV0Y2hBbGJ1bXNDb2xsZWN0aW9uID0gU2VhcmNoTGlzdC5BbGJ1bXMuZmV0Y2goXy5vbWl0KG9wdGlvbnMsJ3N1Y2Nlc3MnLCdlcnJvcicpKTtcblx0XHRTZWFyY2hMaXN0LmZldGNoQWxidW1zQ29sbGVjdGlvbi5kb25lIC0+XG5cdFx0XHRpZiBTZWFyY2hMaXN0LmZldGNoQWxidW1zQ29sbGVjdGlvbi5yZXNwb25zZUpTT05cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggU2VhcmNoTGlzdC5mZXRjaEFsYnVtc0NvbGxlY3Rpb24sW1NlYXJjaExpc3QuQWxidW1zLFNlYXJjaExpc3QuZmV0Y2hBbGJ1bXNDb2xsZWN0aW9uLnJlc3BvbnNlSlNPTi5jb3VudF1cblxuXHRcdFNlYXJjaExpc3QuZmV0Y2hBbGJ1bXNDb2xsZWN0aW9uLmZhaWwgLT5cblx0XHRcdGRlZmVyLnJlamVjdFdpdGggU2VhcmNoTGlzdC5mZXRjaEFsYnVtc0NvbGxlY3Rpb24sYXJndW1lbnRzXG5cblx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xuXG5cdGluaXRpYWxpemVBcnRpc3RzID0gKG9wdGlvbnMpLT5cblx0XHRpZiBTZWFyY2hMaXN0LmZldGNoQXJ0aXN0c0NvbGxlY3Rpb25cblx0XHRcdFNlYXJjaExpc3QuZmV0Y2hBcnRpc3RzQ29sbGVjdGlvbi5hYm9ydCgpXG5cblx0XHRTZWFyY2hMaXN0LkFydGlzdHMgPSBuZXcgU2VhcmNoTGlzdC5BcnRpc3RzQ29sbGVjdGlvblxuXHRcdG9wdGlvbnMgfHwgKG9wdGlvbnMgPSB7fSk7XG5cdFx0ZGVmZXIgPSAkLkRlZmVycmVkKCk7XG5cdFx0U2VhcmNoTGlzdC5mZXRjaEFydGlzdHNDb2xsZWN0aW9uID0gU2VhcmNoTGlzdC5BcnRpc3RzLmZldGNoKF8ub21pdChvcHRpb25zLCdzdWNjZXNzJywnZXJyb3InKSk7XG5cdFx0U2VhcmNoTGlzdC5mZXRjaEFydGlzdHNDb2xsZWN0aW9uLmRvbmUgLT5cblx0XHRcdGlmIFNlYXJjaExpc3QuZmV0Y2hBcnRpc3RzQ29sbGVjdGlvbi5yZXNwb25zZUpTT05cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggU2VhcmNoTGlzdC5mZXRjaEFydGlzdHNDb2xsZWN0aW9uLFtTZWFyY2hMaXN0LkFydGlzdHMsU2VhcmNoTGlzdC5mZXRjaEFydGlzdHNDb2xsZWN0aW9uLnJlc3BvbnNlSlNPTi5jb3VudF1cblxuXHRcdFNlYXJjaExpc3QuZmV0Y2hBcnRpc3RzQ29sbGVjdGlvbi5mYWlsIC0+XG5cdFx0XHRkZWZlci5yZWplY3RXaXRoIFNlYXJjaExpc3QuZmV0Y2hBcnRpc3RzQ29sbGVjdGlvbixhcmd1bWVudHNcblxuXHRcdHJldHVybiBkZWZlci5wcm9taXNlKCk7XG5cblx0aW5pdGlhbGl6ZVBsYXlsaXN0cyA9IChvcHRpb25zKS0+XG5cdFx0aWYgU2VhcmNoTGlzdC5mZXRjaFBsYXlsaXN0c0NvbGxlY3Rpb25cblx0XHRcdFNlYXJjaExpc3QuZmV0Y2hQbGF5bGlzdHNDb2xsZWN0aW9uLmFib3J0KClcblxuXHRcdFNlYXJjaExpc3QuUGxheWxpc3RzID0gbmV3IFNlYXJjaExpc3QuUGxheWxpc3RzQ29sbGVjdGlvblxuXHRcdG9wdGlvbnMgfHwgKG9wdGlvbnMgPSB7fSk7XG5cdFx0ZGVmZXIgPSAkLkRlZmVycmVkKCk7XG5cdFx0U2VhcmNoTGlzdC5mZXRjaFBsYXlsaXN0c0NvbGxlY3Rpb24gPSBTZWFyY2hMaXN0LlBsYXlsaXN0cy5mZXRjaChfLm9taXQob3B0aW9ucywnc3VjY2VzcycsJ2Vycm9yJykpO1xuXHRcdFNlYXJjaExpc3QuZmV0Y2hQbGF5bGlzdHNDb2xsZWN0aW9uLmRvbmUgLT5cblx0XHRcdGlmIFNlYXJjaExpc3QuZmV0Y2hQbGF5bGlzdHNDb2xsZWN0aW9uLnJlc3BvbnNlSlNPTlxuXHRcdFx0XHRkZWZlci5yZXNvbHZlV2l0aCBTZWFyY2hMaXN0LmZldGNoUGxheWxpc3RzQ29sbGVjdGlvbixbU2VhcmNoTGlzdC5QbGF5bGlzdHMsU2VhcmNoTGlzdC5mZXRjaFBsYXlsaXN0c0NvbGxlY3Rpb24ucmVzcG9uc2VKU09OLmNvdW50XVxuXG5cdFx0U2VhcmNoTGlzdC5mZXRjaFBsYXlsaXN0c0NvbGxlY3Rpb24uZmFpbCAtPlxuXHRcdFx0ZGVmZXIucmVqZWN0V2l0aCBTZWFyY2hMaXN0LmZldGNoUGxheWxpc3RzQ29sbGVjdGlvbixhcmd1bWVudHNcblxuXHRcdHJldHVybiBkZWZlci5wcm9taXNlKCk7XG5cblxuXHRpbml0aWFsaXplU2VhcmNoUGFnZSA9ICh0eXBlLG9wdGlvbnMpLT5cblx0XHRpZiBTZWFyY2hMaXN0LmZldGNoUGFnZUNvbGxlY3Rpb25cblx0XHRcdFNlYXJjaExpc3QuZmV0Y2hQYWdlQ29sbGVjdGlvbi5hYm9ydCgpXG5cblx0XHRzd2l0Y2hcdHR5cGVcblx0XHRcdHdoZW4gJ2FydGlzdCdcblx0XHRcdFx0U2VhcmNoTGlzdC5QYWdlID0gbmV3IFNlYXJjaExpc3QuQXJ0aXN0c0NvbGxlY3Rpb25cblx0XHRcdHdoZW4gJ2FsYnVtJ1xuXHRcdFx0XHRTZWFyY2hMaXN0LlBhZ2UgPSBuZXcgU2VhcmNoTGlzdC5BbGJ1bXNDb2xsZWN0aW9uXG5cdFx0XHR3aGVuICd0cmFjaydcblx0XHRcdFx0U2VhcmNoTGlzdC5QYWdlID0gbmV3IFNlYXJjaExpc3QuVHJhY2tzQ29sbGVjdGlvblxuXHRcdFx0d2hlbiAncGxheWxpc3QnXG5cdFx0XHRcdFNlYXJjaExpc3QuUGFnZSA9IG5ldyBTZWFyY2hMaXN0LlBsYXlsaXN0c0NvbGxlY3Rpb25cblxuXG5cdFx0b3B0aW9ucyB8fCAob3B0aW9ucyA9IHt9KTtcblx0XHRkZWZlciA9ICQuRGVmZXJyZWQoKTtcblx0XHRTZWFyY2hMaXN0LmZldGNoUGFnZUNvbGxlY3Rpb24gPSBTZWFyY2hMaXN0LlBhZ2UuZmV0Y2goXy5vbWl0KG9wdGlvbnMsJ3N1Y2Nlc3MnLCdlcnJvcicpKTtcblx0XHRTZWFyY2hMaXN0LmZldGNoUGFnZUNvbGxlY3Rpb24uZG9uZSAtPlxuXHRcdFx0aWYgU2VhcmNoTGlzdC5mZXRjaFBhZ2VDb2xsZWN0aW9uLnJlc3BvbnNlSlNPTlxuXHRcdFx0XHRkZWZlci5yZXNvbHZlV2l0aCBTZWFyY2hMaXN0LmZldGNoUGFnZUNvbGxlY3Rpb24sW1NlYXJjaExpc3QuUGFnZSxTZWFyY2hMaXN0LmZldGNoUGFnZUNvbGxlY3Rpb24ucmVzcG9uc2VKU09OLmNvdW50XVxuXG5cdFx0U2VhcmNoTGlzdC5mZXRjaFBhZ2VDb2xsZWN0aW9uLmZhaWwgLT5cblx0XHRcdGRlZmVyLnJlamVjdFdpdGggU2VhcmNoTGlzdC5mZXRjaFBhZ2VDb2xsZWN0aW9uLGFyZ3VtZW50c1xuXG5cdFx0cmV0dXJuIGRlZmVyLnByb21pc2UoKTtcblxuXG5cdEFQSSA9XG5cdFx0Z2V0VHJhY2tzQ29sbGVjdGlvbnM6IChvcHRpb25zKS0+XG5cdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVRyYWNrcyBvcHRpb25zXG5cblx0XHRnZXRBbGJ1bXNDb2xsZWN0aW9uczogKG9wdGlvbnMpLT5cblx0XHRcdHJldHVybiBpbml0aWFsaXplQWxidW1zIG9wdGlvbnNcblxuXHRcdGdldEFydGlzdHNDb2xsZWN0aW9uczogKG9wdGlvbnMpLT5cblx0XHRcdHJldHVybiBpbml0aWFsaXplQXJ0aXN0cyBvcHRpb25zXG5cblx0XHRnZXRQbGF5bGlzdHNDb2xsZWN0aW9uczogKG9wdGlvbnMpLT5cblx0XHRcdHJldHVybiBpbml0aWFsaXplUGxheWxpc3RzIG9wdGlvbnNcblxuXHRcdGdldFNlYXJjaFBhZ2VDb2xsZWN0aW9uczogKHR5cGUsb3B0aW9ucyktPlxuXHRcdFx0cmV0dXJuIGluaXRpYWxpemVTZWFyY2hQYWdlIHR5cGUsb3B0aW9uc1xuXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdzZWFyY2g6dHJhY2tzJywgKG9wdGlvbnMpLT5cblx0XHRBUEkuZ2V0VHJhY2tzQ29sbGVjdGlvbnMgb3B0aW9uc1xuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnc2VhcmNoOmFsYnVtcycsIChvcHRpb25zKS0+XG5cdFx0QVBJLmdldEFsYnVtc0NvbGxlY3Rpb25zIG9wdGlvbnNcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ3NlYXJjaDphcnRpc3RzJywgKG9wdGlvbnMpLT5cblx0XHRBUEkuZ2V0QXJ0aXN0c0NvbGxlY3Rpb25zIG9wdGlvbnNcblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ3NlYXJjaDpwbGF5bGlzdHMnLCAob3B0aW9ucyktPlxuXHRcdEFQSS5nZXRQbGF5bGlzdHNDb2xsZWN0aW9ucyBvcHRpb25zXG5cblx0YXBwLnJlcXJlcy5zZXRIYW5kbGVyICdzZWFyY2g6cGFnZScsICh0eXBlLG9wdGlvbnMpLT5cblx0XHRBUEkuZ2V0U2VhcmNoUGFnZUNvbGxlY3Rpb25zIHR5cGUsb3B0aW9uc1xuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3NlYXJjaF9saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuXHRTbGlkZXIgPSB7fVxuXHRcblx0Y2xhc3MgU2xpZGVyLkl0ZW0gZXh0ZW5kcyBCYWNrYm9uZS5Nb2RlbFxuXHRcdHVybFJvb3Q6ICcvc2xpZGVyJ1xuXG5cblx0Y2xhc3MgU2xpZGVyLkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBTbGlkZXIuSXRlbVxuXHRcdHVybDogYXBwLmdldFJlcXVlc3RVcmwoJy9nZW5lcmFsVjEvY3AvZ2V0QmFubmVycycpXG5cdFx0cGFyc2U6IChjb2xsZWN0aW9uKS0+XG5cdFx0XHRyZXR1cm4gY29sbGVjdGlvbi5iYW5uZXJzXG5cblxuXHRpbml0aWFsaXplU2xpZGVzID0gLT5cblx0XHRzbGlkZXJMaXN0ID0gbmV3IFNsaWRlci5JdGVtcygpO1xuXHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0ZmV0Y2ggPSBzbGlkZXJMaXN0LmZldGNoKCk7XG5cdFx0aWYgZmV0Y2hcblx0XHRcdGZldGNoLmRvbmUgKHJlcyktPlxuXHRcdFx0XHRpZiByZXMuY29kZSBpcyAwXG5cdFx0XHRcdFx0U2xpZGVyLkxpc3QgPSBzbGlkZXJMaXN0XG5cdFx0XHRcdFx0ZGVmZXIucmVzb2x2ZVdpdGggZmV0Y2gsW1NsaWRlci5MaXN0XVxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0ZGVmZXIucmVqZWN0V2l0aCBmZXRjaCxbcmVzXVxuXHRcdFx0ZmV0Y2guZmFpbCAtPlxuXHRcdFx0XHRkZWZlci5yZWplY3RXaXRoIGZldGNoLGFyZ3VtZW50c1xuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpXG5cblxuXG5cdEFQSSA9XG5cdFx0Z2V0U2xpZGVzOiAtPlxuXHRcdFx0aWYgU2xpZGVyLkxpc3QgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdHJldHVybiBpbml0aWFsaXplU2xpZGVzKClcblx0XHRcdHJldHVybiBTbGlkZXIuTGlzdFxuXG5cblxuXHRhcHAucmVxcmVzLnNldEhhbmRsZXIgJ2dldDpzbGlkZXMnLCAtPlxuXHRcdHJldHVybiBBUEkuZ2V0U2xpZGVzKClcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3NsaWRlcl9saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuXHRUYWIgPSB7fVxuXHRjbGFzcyBUYWIuSXRlbSBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cblx0XHRzZWxlY3Q6IC0+XG5cdFx0XHRALmNvbGxlY3Rpb24uZWFjaCAobW9kZWwpLT5cblx0XHRcdFx0bW9kZWwuc2VsZWN0ZWQgPSBmYWxzZVxuXG5cdFx0XHRALnNlbGVjdGVkID0gdHJ1ZVxuXG5cdGNsYXNzIFRhYi5Db2xsZWN0aW9uIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuXHRcdG1vZGVsOiBUYWIuSXRlbVxuXG5cdGluaXRpYWxpemVUYWIgPSAtPlxuXHRcdFRhYi5MaXN0ID0gbmV3IFRhYi5Db2xsZWN0aW9uKFtcblx0XHRcdHtcblx0XHRcdFx0dGl0bGU6IGFwcC5sb2NhbGUubWVudS50cmFja3NBbmRQbGF5bGlzdHMsXG5cdFx0XHRcdGNsYXNzOiAndHJhY2tzJyxcblx0XHRcdFx0c2VsZWN0ZWQ6ICdzZWxlY3RlZCcsXG5cdFx0XHRcdGhyZWY6ICd1c2VyL3RyYWNrcydcblx0XHRcdFx0dHJpZ2dlcjogJ3Nob3c6dXNlcjp0cmFja3MnXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHR0aXRsZTogYXBwLmxvY2FsZS5tZW51LmFsYnVtcyxcblx0XHRcdFx0Y2xhc3M6ICdhbGJ1bXMnLFxuXHRcdFx0XHRocmVmOiAndXNlci9hbGJ1bXMnXG5cdFx0XHRcdHRyaWdnZXI6ICdzaG93OnVzZXI6YWxidW1zJ1xuXHRcdFx0fVxuXHRcdFx0IyAse1xuXHRcdFx0IyBcdHRpdGxlOiAn0KDQsNC00LjQvicsXG5cdFx0XHQjIFx0Y2xhc3M6ICdyYWRpbycsXG5cdFx0XHQjIFx0dHJpZ2dlcjogJ3Nob3c6dHJhY2tzJ1xuXHRcdFx0IyB9XG5cdFx0XSk7XG5cblxuXG5cblx0QVBJID1cblx0XHRnZXRUYWJzOiAtPlxuXHRcdFx0aWYgVGFiLkxpc3QgaXMgdW5kZWZpbmVkXG5cdFx0XHRcdGluaXRpYWxpemVUYWIoKVxuXHRcdFx0cmV0dXJuIFRhYi5MaXN0XG5cblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnRhYnMnLCAtPlxuXHRcdHJldHVybiBBUEkuZ2V0VGFicygpXG5cblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vZnJvbnRlbmQvYXBwL2VudGl0aWVzL3RhYl9saXN0LmNvZmZlZVxuICoqLyIsImRlZmluZSAtPlxuICBOb3RpZnkgPSB7fVxuICBjbGFzcyBOb3RpZnkuSXRlbSBleHRlbmRzIEJhY2tib25lLk1vZGVsXG5cbiAgY2xhc3MgTm90aWZ5Lkl0ZW1zIGV4dGVuZHMgQmFja2JvbmUuQ29sbGVjdGlvblxuICAgIG1vZGVsOiBOb3RpZnkuSXRlbVxuICAgIGxvY2FsU3RvcmFnZTogbmV3IEJhY2tib25lLkxvY2FsU3RvcmFnZSBcIk5vdGlmeVwiXG4gICAgc2F2ZTogLT5cbiAgICAgIEBlYWNoIChtb2RlbCktPlxuICAgICAgICBCYWNrYm9uZS5sb2NhbFN5bmMgXCJ1cGRhdGVcIiwgbW9kZWxcblxuXG4gIGluaXRpYWxpemVOb3RpZnkgPSAtPlxuICAgIE5vdGlmeS5MaXN0ID0gbmV3IE5vdGlmeS5JdGVtc1xuXG4gICAgZG8gTm90aWZ5Lkxpc3QuZmV0Y2hcblxuXG4gIEFQSSA9XG4gICAgZ2V0Tm90aWZ5OiAtPlxuICAgICAgaWYgTm90aWZ5Lkxpc3QgaXMgdW5kZWZpbmVkXG4gICAgICAgIGluaXRpYWxpemVOb3RpZnkoKVxuXG4gICAgICByZXR1cm4gTm90aWZ5Lkxpc3RcblxuXG4gIGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnVzZXI6bm90aWZpY2F0aW9uJywgLT5cbiAgICByZXR1cm4gQVBJLmdldE5vdGlmeSgpXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogLi9mcm9udGVuZC9hcHAvZW50aXRpZXMvdXNlci1ub3RpZnkuY29mZmVlXG4gKiovIiwiZGVmaW5lIFsnLi4vcGFydHMvbm90aWZpY2F0aW9uJ10sIChOb3RpZmljYXRpb24pLT5cblx0TW9kZWwgPSB7fVxuXHRjbGFzcyBNb2RlbC5JdGVtIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcblx0XHR1cmxSb290OiBhcHAuZ2V0UmVxdWVzdFVybCgnL2NvbXBhdGliaWxpdHkvdXNlci9HZXRCeVNpZCcpXG5cdFx0cGFyc2U6IChtb2RlbCktPlxuXHRcdFx0aWYgbW9kZWwudXNlcnNcblx0XHRcdFx0cmV0dXJuIG1vZGVsLnVzZXJzWzBdXG5cdFx0XHRlbHNlXG5cdFx0XHRcdHJldHVybiBtb2RlbC51c2VyXG5cblx0Y2xhc3MgTW9kZWwuSWRJdGVtIGV4dGVuZHMgQmFja2JvbmUuTW9kZWxcblx0XHR1cmxSb290OiAtPlxuXHRcdFx0cmV0dXJuIGFwcC5nZXRSZXF1ZXN0VXJsKFwiL2JlZWxpbmVNdXNpY1YxL3NvY2lldHkvZ2V0UHVibGljUHJvZmlsZXNcIik7XG5cdFx0cGFyc2U6IChtb2RlbCktPlxuXHRcdFx0aWYgbW9kZWwudXNlcnNcblx0XHRcdFx0cmV0dXJuIG1vZGVsLnVzZXJzWzBdXG5cdFx0XHRlbHNlXG5cdFx0XHRcdHJldHVybiBtb2RlbC51c2VyXG5cblxuXHRpbml0aWFsaXplVXNlciA9IC0+XG5cdFx0aWYgTW9kZWwuaXNMb2FkXG5cdFx0XHRyZXR1cm4gTW9kZWwuZGVmZXIucHJvbWlzZSgpXG5cblx0XHRNb2RlbC5pc0xvYWQgPSB0cnVlXG5cblx0XHR1c2VyID0gbmV3IE1vZGVsLkl0ZW0oKTtcblx0XHRNb2RlbC5kZWZlciA9ICQuRGVmZXJyZWQoKVxuXHRcdGZldGNoVXNlciA9IHVzZXIuZmV0Y2goKTtcblx0XHRpZiAhTW9kZWwuVXNlclRpbWVyXG5cdFx0XHRNb2RlbC5Vc2VyVGltZXIgPSBzZXRJbnRlcnZhbCAtPlxuXHRcdFx0XHRkbyBBUEkuZmV0Y2hVc2VyXG5cdFx0XHQsNjAwMDBcblxuXHRcdGlmIGZldGNoVXNlclxuXHRcdFx0ZmV0Y2hVc2VyLmRvbmUgKHJlcyktPlxuXHRcdFx0XHRpZiByZXMuY29kZSBpcyAwXG5cdFx0XHRcdFx0TW9kZWwuVXNlciA9IHVzZXJcblx0XHRcdFx0XHRNb2RlbC5kZWZlci5yZXNvbHZlV2l0aCBmZXRjaFVzZXIsW01vZGVsLlVzZXJdXG5cblx0XHRcdFx0XHRpZiBNb2RlbC5Vc2VyLmdldCgnc3Vic2NyaXB0aW9uJykgaXMgJ0RJU0FCTEVEJ1xuXHRcdFx0XHRcdFx0ZG8gTm90aWZpY2F0aW9uLkNvbnRyb2xsZXIuc2hvd1N1YnNjcmliZVxuXHRcdFx0XHRcdGVsc2UgaWYgTW9kZWwuVXNlci5nZXQoJ3N1YnNjcmlwdGlvbicpIGlzICdPVEhFUidcblx0XHRcdFx0XHRcdGRvIE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3dTdWJzY3JpYmVcblxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0aWYgcmVzLmNvZGUgaXMgLTRcblx0XHRcdFx0XHRcdCQuZ2V0ICcvZXhpdCcsIC0+XG5cdFx0XHRcdFx0XHRcdGxvY2F0aW9uLmhyZWYgPSAnJ1xuXHRcdFx0XHRcdE1vZGVsLmRlZmVyLnJlamVjdFdpdGggZmV0Y2hVc2VyLGFyZ3VtZW50c1xuXHRcdFx0XHRcdE1vZGVsLmlzTG9hZCA9IGZhbHNlXG5cblx0XHRcdGZldGNoVXNlci5mYWlsIC0+XG5cdFx0XHRcdE1vZGVsLmRlZmVyLnJlamVjdFdpdGggZmV0Y2hVc2VyLGFyZ3VtZW50c1xuXHRcdFx0XHRNb2RlbC5pc0xvYWQgPSBmYWxzZVxuXG5cdFx0XHRyZXR1cm4gTW9kZWwuZGVmZXIucHJvbWlzZSgpXG5cblx0aW5pdGlhbGl6ZVVzZXJCeUlkID0gKGlkKS0+XG5cdFx0aWYgTW9kZWwuaXNMb2FkQnlJZFxuXHRcdFx0cmV0dXJuIE1vZGVsLmRlZmVyQnlJZC5wcm9taXNlKClcblxuXHRcdE1vZGVsLmlzTG9hZEJ5SWQgPSB0cnVlXG5cblx0XHR1c2VyID0gbmV3IE1vZGVsLklkSXRlbSgpO1xuXHRcdE1vZGVsLmRlZmVyQnlJZCA9ICQuRGVmZXJyZWQoKVxuXHRcdGZldGNoVXNlciA9IHVzZXIuZmV0Y2hcblx0XHRcdGRhdGE6XG5cdFx0XHRcdHVzZXJJZHM6IFtpZF1cblxuXHRcdGlmIGZldGNoVXNlclxuXHRcdFx0ZmV0Y2hVc2VyLmRvbmUgKHJlcyktPlxuXHRcdFx0XHRpZiByZXMuY29kZSBpcyAwXG5cdFx0XHRcdFx0aWYgcmVzLnVzZXJzWzBdWydub3RfZm91bmQnXVxuXHRcdFx0XHRcdFx0TW9kZWwuZGVmZXJCeUlkLnJlamVjdFdpdGggZmV0Y2hVc2VyLGFyZ3VtZW50c1xuXHRcdFx0XHRcdGVsc2Vcblx0XHRcdFx0XHRcdE1vZGVsLlVzZXJCeUlkID0gdXNlclxuXHRcdFx0XHRcdFx0TW9kZWwuZGVmZXJCeUlkLnJlc29sdmVXaXRoIGZldGNoVXNlcixbTW9kZWwuVXNlckJ5SWRdXG5cdFx0XHRcdFx0XHRNb2RlbC5hY3R1YWxVc2VySWQgPSBpZFxuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0TW9kZWwuZGVmZXJCeUlkLnJlamVjdFdpdGggZmV0Y2hVc2VyLGFyZ3VtZW50c1xuXG5cdFx0XHRcdE1vZGVsLmlzTG9hZEJ5SWQgPSBmYWxzZVxuXG5cdFx0XHRmZXRjaFVzZXIuZmFpbCAtPlxuXHRcdFx0XHRNb2RlbC5kZWZlckJ5SWQucmVqZWN0V2l0aCBmZXRjaFVzZXIsYXJndW1lbnRzXG5cdFx0XHRcdE1vZGVsLmlzTG9hZEJ5SWQgPSBmYWxzZVxuXG5cdFx0XHRyZXR1cm4gTW9kZWwuZGVmZXJCeUlkLnByb21pc2UoKVxuXG5cblxuXG5cblx0QVBJID1cblx0XHRnZXRVc2VyOiAtPlxuXHRcdFx0aWYgTW9kZWwuVXNlciBpcyB1bmRlZmluZWRcblx0XHRcdFx0cmV0dXJuIGluaXRpYWxpemVVc2VyKClcblx0XHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0XHRzZXRJbW1lZGlhdGUgLT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZSBNb2RlbC5Vc2VyXG5cblx0XHRcdHJldHVybiBkZWZlci5wcm9taXNlKCk7XG5cblx0XHRnZXRVc2VyQnlJZDogKGlkKS0+XG5cdFx0XHRpZiBNb2RlbC5hY3R1YWxVc2VySWQgaXNudCBpZFxuXHRcdFx0XHRyZXR1cm4gaW5pdGlhbGl6ZVVzZXJCeUlkIGlkXG5cblx0XHRcdGRlZmVyID0gJC5EZWZlcnJlZCgpXG5cdFx0XHRzZXRJbW1lZGlhdGUgLT5cblx0XHRcdFx0ZGVmZXIucmVzb2x2ZSBNb2RlbC5Vc2VyQnlJZFxuXG5cdFx0XHRyZXR1cm4gZGVmZXIucHJvbWlzZSgpO1xuXG5cdFx0ZmV0Y2hVc2VyOiAtPlxuXHRcdFx0aWYgTW9kZWwuVXNlclxuXHRcdFx0XHRmZXRjaCA9IE1vZGVsLlVzZXIuZmV0Y2goKVxuXG5cdFx0XHRcdGZldGNoLmRvbmUgLT5cblx0XHRcdFx0XHRzdGF0dXMgPSBNb2RlbC5Vc2VyLmdldCgnc3Vic2NyaXB0aW9uJylcblx0XHRcdFx0XHRpZiBzdGF0dXMgaXNudCAnRElTQUJMRUQnIGFuZCBzdGF0dXMgaXNudCAnT1RIRVInXG5cdFx0XHRcdFx0XHRkbyBOb3RpZmljYXRpb24uQ29udHJvbGxlci5jbG9zZVN1YnNjcmliZVxuXHRcdFx0XHRcdGVsc2UgaWYgc3RhdHVzIGlzICdPVEhFUidcblx0XHRcdFx0XHRcdGRvIE5vdGlmaWNhdGlvbi5Db250cm9sbGVyLnNob3dTdWJzY3JpYmVcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRkbyBOb3RpZmljYXRpb24uQ29udHJvbGxlci5zaG93U3Vic2NyaWJlXG5cblxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZ2V0OnVzZXInLCAoaWQpLT5cblx0XHRpZiBpZD9cblx0XHRcdHJldHVybiBBUEkuZ2V0VXNlckJ5SWQgaWRcblx0XHRlbHNlXG5cdFx0XHRyZXR1cm4gQVBJLmdldFVzZXIoKVxuXG5cdGFwcC5yZXFyZXMuc2V0SGFuZGxlciAnZmV0Y2g6dXNlcicsIC0+XG5cdFx0cmV0dXJuIEFQSS5mZXRjaFVzZXIoKVxuXG5cdHJldHVybiBNb2RlbFxuXG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2Zyb250ZW5kL2FwcC9lbnRpdGllcy91c2VyX21vZGVsLmNvZmZlZVxuICoqLyJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7OztBQ3pGQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFFQTs7QUFBQTs7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOztBQU5BO0FBUUE7QUFkQTtBQWdCQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFMQTtBQUFBO0FBT0E7O0FBUkE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUZBO0FBR0E7QUFOQTtBQVFBOztBQVRBO0FBV0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBSEE7QUFNQTtBQUNBO0FBQUE7QUFDQTtBQUNBOztBQUhBO0FBS0E7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUhBO0FBS0E7QUFEQTtBQUpBO0FBT0E7QUFEQTtBQU5BO0FBU0E7QUFDQTtBQVZBO0FBWUE7QUFaQTs7QUFhQTtBQXBCQTtBQXVCQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBS0E7O0FBUEE7QUFTQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7O0FBTkE7QUFRQTtBQWRBO0FBZ0JBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBREE7QUFHQTtBQURBO0FBR0E7QUFEQTtBQUdBO0FBREE7QUFHQTtBQURBO0FBR0E7QUFEQTtBQUdBOztBQUNBO0FBQ0E7O0FBcEJBO0FBc0JBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBTEE7QUFRQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFMQTtBQVNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUFBOztBQUVBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7O0FBRUE7QUFBQTtBQUFBO0FBQ0E7O0FBYkE7QUFlQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFBQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBREE7QUFHQTs7QUFHQTtBQUNBO0FBQUE7O0FBQ0E7QUFGQTtBQXBCQTtBQUFBOztBQXJDQTtBQThEQTtBQUNBO0FBREE7QUFHQTtBQXhQQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7Ozs7Ozs7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7O0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7O0FBSEE7QUFyQkE7QUFDQTtBQTJCQTtBQUNBO0FBQUE7QUFFQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBRUE7QUFDQTtBQUpBO0FBTUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUNBOztBQVBBO0FBU0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBL0JBO0FBSEE7QUFDQTtBQW1DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFOQTtBQUNBO0FBT0E7QUFDQTtBQUFBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFMQTtBQUFBO0FBT0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFWQTtBQUNBO0FBYUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFMQTtBQUFBOztBQWhCQTtBQUNBO0FBd0JBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQVBBOztBQVNBO0FBQ0E7QUFiQTtBQUNBO0FBY0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBTkE7QUFBQTtBQVFBO0FBWEE7O0FBYUE7QUFDQTtBQURBO0FBSUE7O0FBcEJBO0FBQ0E7QUF1QkE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFmQTtBQUNBO0FBZ0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBZEE7QUFDQTtBQWlCQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUVBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFkQTtBQUNBO0FBZUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBSkE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTs7QUFHQTtBQU5BO0FBQ0E7QUFPQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUdBOztBQUdBO0FBQ0E7QUFUQTs7QUFEQTtBQUNBO0FBV0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFOQTs7QUFRQTtBQUNBO0FBWEE7O0FBREE7QUFDQTtBQWFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQVFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQVJBO0FBUkE7O0FBdUJBO0FBQ0E7QUFEQTtBQUdBO0FBM0JBO0FBNkJBOztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFKQTs7QUFPQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBM0NBO0FBQ0E7QUE2Q0E7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQVJBO0FBQ0E7QUFTQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFQQTtBQUZBO0FBQ0E7QUFVQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVJBO0FBRkE7QUFDQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFQQTtBQUNBO0FBU0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSEE7QUFPQTtBQUNBO0FBQ0E7QUFGQTs7QUFJQTtBQUdBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFyQkE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7O0FBckNBO0FBQ0E7QUE0Q0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUlBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFQQTtBQVNBO0FBZkE7QUFDQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7QUFOQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQUNBO0FBYUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQURBO0FBQ0E7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBOztBQUVBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFHQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFmQTtBQUFBO0FBYkE7QUFDQTtBQTZCQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFHQTtBQXBCQTtBQUNBO0FBcUJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFMQTs7QUFPQTtBQUNBO0FBdEJBO0FBQ0E7Ozs7Ozs7QUFwZ0JBO0FBNGhCQTtBQUNBO0FBaGlCQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUhBOztBQURBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBOztBQUZBO0FBU0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUVBO0FBQ0E7O0FBSEE7QUFLQTtBQUNBOzs7QUFFQTtBQUNBO0FBREE7QUFDQTs7O0FBckdBO0FBdUdBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUxBO0FBT0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUNBOzs7QUFoQ0E7QUFtQ0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTs7O0FBTEE7QUFPQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFEQTs7O0FBR0E7QUFDQTtBQURBO0FBQ0E7OztBQVpBO0FBY0E7QUF6S0E7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDclBBOzs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQU1BO0FBQ0E7QUFDQTs7QUFUQTtBQVdBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUF0QkE7QUEyQkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBOztBQW5DQTtBQUNBO0FBdUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFEQTtBQUdBOztBQUVBO0FBQ0E7QUFEQTtBQVZBO0FBQ0E7QUFlQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFaQTtBQUNBOzs7QUE3REE7QUEwRUE7QUE3RUE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7O0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFBQTtBQUtBO0FBQ0E7O0FBQ0E7QUFDQTtBQVRBO0FBQ0E7QUFVQTtBQUNBO0FBREE7QUFDQTs7O0FBbEJBO0FBb0JBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBQ0E7OztBQUpBO0FBUUE7QUFDQTtBQUVBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUpBOztBQU9BO0FBQ0E7QUFEQTtBQTVDQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7QUFEQTtBQVFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUVBO0FBQ0E7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUlBO0FBekJBO0FBQ0E7QUEwQkE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBTEE7QUFDQTtBQU1BO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBWEE7QUFDQTtBQVlBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7OztBQWpEQTtBQW9EQTtBQUVBO0FBekRBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTs7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7OztBQUdBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQ0E7OztBQWxCQTtBQW9CQTtBQXZCQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUNaQTs7Ozs7O0FDQUE7Ozs7OztBQ0FBOzs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFBQTtBQUtBO0FBQ0E7QUFQQTtBQVNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUpBOztBQU1BO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFKQTs7QUFNQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBSkE7O0FBTUE7QUFHQTtBQXRCQTtBQVZBOzs7Ozs7O0FDQUE7OztBQUFBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7QUFOQTtBQVFBO0FBWkE7QUFDQTs7O0FBRkE7Ozs7Ozs7QUNBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBTkE7QUFGQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUxBO0FBUUE7QUFUQTtBQUFBO0FBWUE7O0FBR0E7QUFFQTtBQTVCQTtBQThCQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFNQTtBQWpFQTtBQUNBOzs7QUFGQTtBQW9FQTtBQXJFQTs7Ozs7OztBQ0FBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUNBOztBQVJBO0FBV0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUNBOztBQVJBO0FBV0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQXZCQTtBQUNBO0FBOEJBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUNBO0FBUkE7QUFVQTtBQUNBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQVhBO0FBWEE7QUFDQTtBQXlCQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQU9BO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFOQTtBQVNBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFIQTtBQU1BO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBREE7QUFHQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQU9BO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBZkE7QUExQ0E7Ozs7Ozs7QUN6REE7OztBQUFBO0FBQ0E7QUFBQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFsQkE7QUFvQkE7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQURBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQURBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFEQTtBQUNBO0FBUUE7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBQ0E7QUFPQTs7QUFBQTs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBREE7QUFDQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFDQTtBQUlBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBSEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQURBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFEQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBOzs7QUFqSEE7QUF3SEE7QUFFQTtBQUNBO0FBREE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQUFBOztBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQXJNQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUZBO0FBS0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOztBQUpBO0FBSkE7QUFVQTs7QUF2QkE7QUFDQTtBQXdCQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTs7QUFJQTtBQUNBO0FBQ0E7QUFFQTtBQUpBO0FBTUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUZBO0FBMUJBO0FBQ0E7QUE2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQWpCQTtBQUNBO0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBTUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBQUE7QUFNQTtBQXJCQTtBQUNBO0FBdUJBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBTUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBQUE7QUFNQTtBQXJCQTtBQUNBO0FBc0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQU9BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBTUE7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBQUE7QUFNQTtBQXRCQTtBQUNBO0FBd0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQU1BO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFJQTtBQUVBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFOQTtBQVFBO0FBRUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOztBQUhBOztBQUxBO0FBVUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQXRCQTtBQTRCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7QUFIQTtBQTNDQTtBQWJBO0FBQ0E7QUE0REE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUlBO0FBRUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQU5BO0FBUUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOztBQUhBOztBQUpBO0FBU0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQXJCQTtBQTJCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7QUFIQTtBQTFDQTtBQWJBO0FBQ0E7QUEyREE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBTUE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUlBO0FBRUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQU5BO0FBUUE7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBOztBQUhBOztBQUpBO0FBU0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQXJCQTtBQTJCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7QUFIQTtBQTFDQTtBQWJBO0FBQ0E7QUE0REE7QUFDQTtBQUNBO0FBREE7QUFJQTs7QUFMQTtBQUNBOzs7QUExVUE7QUFtVkE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFKQTtBQURBO0FBT0E7QUEvVkE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFEQTtBQUxBO0FBQ0E7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFEQTtBQUlBOztBQUVBO0FBQ0E7O0FBRUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQVpBO0FBZ0JBO0FBQ0E7QUFEQTs7Ozs7QUFwQ0E7QUF3Q0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRkE7QUFLQTtBQUNBO0FBREE7OztBQUdBO0FBQ0E7QUFDQTtBQURBOzs7OztBQWZBO0FBa0JBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTs7O0FBSUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFQQTtBQUNBOzs7QUFWQTtBQW9CQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFIQTtBQUlBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTs7O0FBR0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOztBQVJBO0FBQ0E7OztBQVRBO0FBbUJBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7QUFKQTtBQUtBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUdBO0FBQ0E7QUFEQTtBQUNBOzs7QUFiQTtBQWVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTs7Ozs7QUFKQTtBQU9BOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUZBOzs7OztBQUpBO0FBUUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7Ozs7O0FBSkE7QUFRQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFIQTtBQUlBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUhBO0FBSUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTs7O0FBSEE7QUFJQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBOzs7OztBQWJBO0FBcUJBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOztBQUpBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7Ozs7O0FBYkE7QUFvQkE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7O0FBSkE7QUFDQTtBQUtBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFMQTs7Ozs7QUFiQTtBQXFCQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFIQTtBQW9DQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTs7Ozs7QUFOQTtBQVNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBQ0E7OztBQVRBO0FBYUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7OztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFDQTs7O0FBVEE7QUFhQTtBQXBTQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQURBO0FBQUE7QUFRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFEQTtBQUFBO0FBUUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQVJBO0FBUkE7O0FBdUJBO0FBQ0E7QUFEQTtBQUdBO0FBM0JBO0FBNkJBOztBQUVBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFKQTs7QUFPQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBM0NBO0FBQ0E7QUE2Q0E7QUFFQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQVJBO0FBQ0E7QUFTQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBWEE7QUFlQTtBQUNBO0FBQ0E7O0FBbEJBO0FBQ0E7QUFtQkE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBUEE7QUFGQTtBQUNBO0FBVUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFSQTtBQUZBO0FBQ0E7QUFXQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBUEE7QUFDQTtBQVNBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFIQTtBQU9BO0FBQ0E7QUFDQTtBQUZBOztBQUlBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWxCQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTs7QUEvQkE7QUFDQTtBQXFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFWQTtBQUNBOzs7QUF0SkE7QUFtS0E7QUFFQTtBQUVBO0FBMUtBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7QUFFQTtBQUNBO0FBQ0E7O0FBRkE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUpBO0FBQUE7QUFNQTtBQUNBOztBQVhBO0FBYUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7O0FBUkE7QUFBQTtBQVVBO0FBQ0E7QUFEQTtBQVhBO0FBZUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQXRCQTs7QUFIQTtBQTJCQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFPQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFFQTtBQUNBOztBQUhBO0FBS0E7QUFDQTtBQUNBO0FBRkE7Ozs7O0FBdEdBO0FBOEdBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUxBO0FBT0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFGQTtBQUNBO0FBR0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7OztBQWxDQTtBQXFDQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBOzs7QUFMQTtBQU9BOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFDQTtBQURBOzs7QUFHQTtBQUNBO0FBREE7QUFDQTs7O0FBWkE7QUFjQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBOzs7QUFIQTtBQUlBO0FBckxBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUxBO0FBT0E7Ozs7Ozs7QUFDQTtBQUNBOzs7QUFGQTtBQUdBO0FBYkE7Ozs7Ozs7QUNBQTs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQU5BO0FBQUE7QUFGQTtBQVdBO0FBYkE7QUFlQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQU5BO0FBQUE7QUFGQTtBQVlBO0FBQ0E7QUFmQTtBQWlCQTtBQUNBO0FBREE7O0FBckNBO0FBQ0E7QUF1Q0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBUkE7QUFDQTtBQVVBO0FBQ0E7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVkE7QUFDQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBOztBQUpBO0FBQ0E7OztBQWpFQTtBQXVFQTtBQUVBO0FBRUE7QUE5RUE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFFQTtBQU1BO0FBUkE7QUFBQTtBQVNBO0FBQ0E7QUFaQTtBQUNBO0FBYUE7QUFDQTs7O0FBRUE7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBSkE7QUFBQTtBQU1BOztBQVJBO0FBVUE7QUFDQTtBQUFBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7O0FBUkE7QUFBQTtBQVVBO0FBQ0E7QUFEQTtBQVhBO0FBZUE7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQXRCQTs7QUFIQTtBQTJCQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBQ0E7OztBQTdGQTtBQW1HQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQVBBO0FBU0E7OztBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBTkE7QUFBQTtBQUZBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQXJCQTtBQXVCQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUdBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBSEE7QUFLQTtBQVZBO0FBckJBOztBQTFCQTtBQUNBOzs7QUFyQkE7QUErRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFDQTs7O0FBUkE7QUFXQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUNBOzs7QUFKQTtBQVFBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7O0FBRkE7QUFDQTs7O0FBSkE7QUFPQTtBQS9NQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7O0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBREE7QUFHQTs7QUFUQTtBQUNBO0FBV0E7QUFDQTtBQUNBO0FBREE7Ozs7O0FBbEJBO0FBcUJBO0FBekJBOzs7Ozs7O0FDQUE7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7O0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBREE7QUFHQTs7QUFUQTtBQUNBOzs7QUFOQTtBQWdCQTtBQXBCQTs7Ozs7OztBQ0FBOzs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOztBQU5BO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBaEJBO0FBa0JBOzs7Ozs7O0FBQ0E7QUFDQTs7O0FBRkE7QUFHQTtBQXhCQTs7Ozs7OztBQ0FBOzs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7OztBQUFBO0FBSUE7QUFFQTtBQVJBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7Ozs7O0FBRkE7QUFLQTtBQVBBOzs7Ozs7O0FDQUE7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFOQTtBQVFBO0FBQ0E7QUFUQTtBQUFBO0FBRkE7QUFjQTtBQXBCQTtBQXNCQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQU5BO0FBUUE7QUFDQTtBQVRBO0FBQUE7QUFGQTtBQWVBO0FBQ0E7QUFsQkE7QUFvQkE7QUFDQTtBQURBOztBQS9DQTtBQUNBOzs7QUFGQTtBQW9EQTtBQUVBO0FBQ0E7QUExREE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFKQTtBQUFBO0FBTUE7O0FBUkE7QUFVQTtBQUNBO0FBREE7QUFHQTtBQUdBOzs7OztBQXJCQTtBQXlCQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFFQTtBQVBBO0FBU0E7OztBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBQ0E7QUFBQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBTkE7QUFBQTtBQUZBO0FBWUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQXJCQTtBQXVCQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUdBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFFQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFEQTtBQUVBO0FBSEE7QUFLQTtBQVZBO0FBckJBOztBQTFCQTtBQUNBOzs7QUFyQkE7QUFnRkE7Ozs7Ozs7QUFDQTtBQUNBOzs7QUFGQTtBQUlBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUhBO0FBSUE7QUFwSEE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQU5BO0FBUUE7Ozs7Ozs7QUFDQTtBQUNBOzs7QUFGQTtBQUdBO0FBZEE7Ozs7Ozs7QUNBQTs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7QUNSQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBVkE7O0FBYUE7QUFmQTtBQUNBO0FBZUE7QUFDQTs7O0FBbEJBO0FBb0JBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTs7O0FBSkE7QUFNQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUdBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7O0FBdkJBO0FBMkJBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7O0FBT0E7QUFDQTtBQURBO0FBR0E7QUFsRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDdkRBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTs7O0FBSkE7QUFPQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTs7QUFaQTtBQWtCQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBOztBQU9BO0FBQ0E7QUFEQTtBQW5DQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOztBQU5BO0FBQ0E7OztBQU5BO0FBYUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBTkE7QUFDQTs7O0FBTkE7QUFjQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQURBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBSUE7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUF4QkE7QUEwQkE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFMQTtBQU1BO0FBQ0E7QUFEQTtBQUdBOztBQWhCQTtBQW9CQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQVJBOztBQVlBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOztBQUpBOztBQU1BO0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUlBO0FBTEE7O0FBRkE7QUFySEE7Ozs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTs7O0FBTEE7QUFPQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBQ0E7OztBQUZBO0FBS0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBOzs7QUFKQTtBQU9BOzs7Ozs7O0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTs7QUFOQTtBQUNBOzs7QUFKQTtBQVdBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFDQTs7O0FBSkE7QUFNQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFKQTtBQUNBOzs7QUFGQTtBQVFBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7OztBQUhBO0FBSUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7O0FBakJBO0FBb0JBO0FBQ0E7O0FBREE7O0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFJQTtBQUNBO0FBREE7QUFHQTs7QUFiQTtBQWVBO0FBQ0E7O0FBREE7O0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBQUE7QUFJQTtBQUNBO0FBREE7QUFHQTs7QUFiQTtBQWVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUlBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFGQTtBQUFBO0FBSUE7QUFDQTtBQURBO0FBR0E7O0FBZkE7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOztBQUpBO0FBTUE7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQU1BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBOztBQU9BO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUFBO0FBRUE7QUFDQTs7QUFGQTtBQURBO0FBTUE7QUFDQTs7QUFGQTtBQUxBO0FBU0E7QUFDQTs7QUFGQTtBQVJBO0FBWUE7QUFDQTs7QUFiQTs7QUFGQTtBQTNKQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7Ozs7O0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUpBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBR0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFsQkE7QUFxQkE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFIQTs7QUFPQTtBQUNBO0FBREE7QUF4Q0E7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUhBO0FBTUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBRUE7QUFIQTs7QUFKQTtBQVNBO0FBQ0E7QUFEQTtBQUdBOztBQXBCQTtBQTJCQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBOztBQVNBO0FBQ0E7QUFEQTtBQUdBO0FBakRBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQURBO0FBR0E7O0FBUEE7QUFDQTs7O0FBWkE7QUFxQkE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQ0E7OztBQVBBO0FBVUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBOztBQU5BO0FBT0E7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFyQkE7QUF5QkE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBUkE7O0FBWUE7QUFDQTtBQURBO0FBSUE7QUE1RUE7Ozs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFMQTtBQUNBOzs7QUFwQkE7QUEyQkE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBOzs7QUFMQTtBQVFBO0FBQ0E7QUFBQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7O0FBTkE7QUFPQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBckJBO0FBeUJBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7O0FBT0E7QUFDQTtBQURBO0FBSUE7QUExRUE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFLQTtBQUNBO0FBTkE7QUFRQTtBQUNBO0FBVEE7O0FBYUE7QUFwQkE7QUFDQTs7O0FBVEE7QUE4QkE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBQ0E7OztBQVBBO0FBVUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOztBQU5BO0FBUUE7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUF0QkE7QUEwQkE7QUFDQTtBQUNBO0FBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBUEE7O0FBV0E7QUFDQTtBQURBO0FBSUE7QUFwRkE7Ozs7Ozs7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7O0FDNUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7O0FDMUZBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7OztBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTs7O0FBSkE7QUFPQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFFQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBOztBQUhBO0FBQ0E7OztBQVBBO0FBWUE7QUFJQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFHQTtBQUNBO0FBQ0E7QUFGQTs7QUFUQTtBQWFBO0FBckJBO0FBdUJBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFBQTs7Ozs7OztBQUNBO0FBQ0E7Ozs7O0FBRkE7QUFJQTtBQUxBO0FBU0E7QUFDQTtBQUVBO0FBQ0E7O0FBRUE7QUFMQTtBQU9BO0FBQ0E7QUFDQTs7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7O0FBT0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBbEZBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7Ozs7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQURBO0FBQ0E7OztBQUpBO0FBUUE7QUFDQTtBQUVBO0FBSEE7QUFNQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUpBOztBQU9BO0FBQ0E7QUFEQTtBQTFCQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7Ozs7O0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7OztBQUZBO0FBR0E7Ozs7Ozs7OztBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBREE7QUFDQTs7O0FBSkE7QUFNQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQVhBO0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFqQkE7QUFvQkE7QUFDQTtBQUNBO0FBQ0E7QUF2QkE7QUEwQkE7QUFDQTtBQUNBO0FBQ0E7QUE3QkE7QUFzQ0E7QUFDQTtBQUNBO0FBQ0E7QUF6Q0E7QUFBQTtBQURBO0FBbURBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFaQTs7QUFlQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUExRkE7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7QUFDQTtBQUNBO0FBREE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBOztBQU5BO0FBQ0E7OztBQUxBO0FBWUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUNBOzs7QUFKQTtBQU9BO0FBQ0E7QUFBQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFEQTtBQUtBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFHQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQXJCQTtBQXdCQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQUxBO0FBTUE7QUFDQTtBQURBO0FBR0E7O0FBaEJBO0FBb0JBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7QUFLQTtBQUNBO0FBQ0E7QUFEQTtBQUlBOztBQUxBO0FBT0E7QUFDQTtBQURBOztBQUtBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQXpGQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTs7Ozs7Ozs7O0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7OztBQUZBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBTUE7QUFDQTtBQVBBO0FBVUE7QUFDQTtBQVhBO0FBYUE7QUFDQTtBQWRBO0FBZ0JBO0FBQ0E7QUFqQkE7QUFBQTtBQURBO0FBeUJBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBSEE7O0FBT0E7QUFDQTtBQURBO0FBeENBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUpBOztBQURBO0FBQ0E7OztBQUhBO0FBVUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUhBO0FBS0E7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFuQkE7QUFxQkE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFGQTtBQUtBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFuQkE7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFDQTs7QUFDQTtBQUhBOztBQUtBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFEQTtBQTdFQTs7Ozs7OztBQ0FBOzs7QUFBQTtBQUNBO0FBQUE7QUFFQTs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBS0E7QUFDQTtBQUNBO0FBUEE7QUFTQTtBQUNBO0FBVkE7O0FBYUE7QUFmQTtBQUNBOzs7QUFGQTtBQWtCQTs7Ozs7OztBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQUNBOzs7QUFKQTtBQVNBOzs7Ozs7O0FBQ0E7QUFDQTs7O0FBRkE7QUFHQTs7Ozs7OztBQUNBO0FBQ0E7OztBQUZBO0FBR0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUNBOzs7QUFMQTtBQVFBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFDQTs7O0FBTEE7QUFPQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBQ0E7OztBQUxBO0FBT0E7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFBQTs7O0FBRUE7QUFDQTtBQURBO0FBQ0E7OztBQUxBO0FBUUE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQWZBO0FBaUJBO0FBQ0E7QUFBQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUFmQTtBQWlCQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFGQTtBQUlBO0FBQ0E7QUFEQTtBQUdBO0FBZkE7QUFpQkE7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQWZBO0FBa0JBO0FBQ0E7QUFBQTtBQUNBOztBQUVBO0FBQUE7QUFFQTtBQURBO0FBREE7QUFJQTtBQURBO0FBSEE7QUFNQTtBQURBO0FBTEE7QUFRQTtBQVJBO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUZBO0FBSUE7QUFDQTtBQURBO0FBR0E7QUF6QkE7QUE0QkE7QUFDQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBREE7O0FBSUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBaE1BOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUVBOzs7Ozs7O0FBQ0E7QUFDQTs7O0FBRkE7QUFJQTs7Ozs7OztBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQURBO0FBQ0E7OztBQUpBO0FBT0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOztBQUxBO0FBTUE7QUFDQTtBQURBO0FBR0E7O0FBZEE7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFIQTs7QUFPQTtBQUNBO0FBREE7QUF4Q0E7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBSkE7QUFDQTs7O0FBSEE7QUFRQTs7Ozs7OztBQUNBO0FBQ0E7OztBQUZBO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBU0E7QUFDQTtBQUNBO0FBQ0E7QUFaQTtBQUFBO0FBREE7QUF5QkE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFIQTs7QUFPQTtBQUNBO0FBREE7QUE5Q0E7Ozs7Ozs7QUNBQTs7O0FBQUE7QUFDQTtBQUFBO0FBQ0E7Ozs7Ozs7OztBQUFBO0FBRUE7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBREE7QUFDQTs7O0FBSkE7QUFRQTtBQUNBO0FBRUE7QUFIQTtBQU1BO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBSkE7O0FBT0E7QUFDQTtBQURBO0FBMUJBOzs7Ozs7O0FDQUE7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOzs7Ozs7O0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQURBO0FBR0E7O0FBSkE7QUFDQTs7O0FBSEE7QUFRQTs7Ozs7OztBQUNBO0FBQ0E7QUFEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFHQTs7QUFKQTtBQUNBOzs7QUFKQTtBQVVBO0FBQ0E7QUFBQTtBQUNBOztBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFEQTtBQUdBO0FBUEE7QUFBQTtBQVVBO0FBQ0E7QUFDQTtBQURBOztBQUVBO0FBQ0E7O0FBZkE7QUFpQkE7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUFwQ0E7QUFzQ0E7QUFDQTtBQUFBO0FBQ0E7O0FBRUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFEQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFRQTs7QUFFQTtBQVhBO0FBYUE7QUFDQTtBQUNBO0FBRkE7QUFJQTs7QUE5QkE7QUFvQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBUEE7QUFTQTtBQUNBO0FBQUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBUkE7QUFVQTtBQUNBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFEQTtBQUdBO0FBREE7QUFHQTs7QUFQQTs7QUFKQTs7QUFlQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOztBQUpBO0FBTUE7QUFDQTtBQURBO0FBR0E7QUExSUE7Ozs7OyIsInNvdXJjZVJvb3QiOiIifQ==