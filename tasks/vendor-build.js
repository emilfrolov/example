var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var rename = require('gulp-rename')

var libFiles = [
  './frontend/packages/jquery/dist/jquery.js',
  './frontend/packages/scrollTo/scrollTo.min.js',
  './frontend/packages/jPlayer/dist/jplayer/jquery.jplayer.js',
  './frontend/packages/underscore/underscore.js',
  './frontend/packages/backbone/backbone.js',
  './frontend/packages/backbone.marionette/lib/backbone.marionette.js',
  './frontend/packages/backbone.localStorage/backbone.localStorage.js',
  './frontend/packages/Clamp.js-master/clamp.js',
  './frontend/packages/autoresize/dist/autosize.js',
  './frontend/packages/jquery-scrollbar/jquery.custom-scrollbar.min.js'
]

module.exports = function() {

  return function(callback) {

      var stream = gulp.src(libFiles)
        .pipe(concat('index.js'))
        .pipe(gulp.dest('./public/pack/scripts/vendor/'))
        .pipe(uglify())
        .pipe(rename('index.min.js'))
        .pipe(gulp.dest('./public/pack/scripts/vendor/'))

      
        return stream;
  };
};