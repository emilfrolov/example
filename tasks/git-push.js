var gulp = require('gulp')
var git = require('gulp-git')
const runSequence = require('run-sequence');

module.exports = function(commitName) {

  commitName = commitName ? commitName : 'defaultCommit'

  return function(callback) {

      gulp.task('git:add', function (callback) {
        return gulp.src('./*')
              .pipe(git.add());
      })

      gulp.task('git:commit', function (callback) {
        return gulp.src('./*')
              .pipe(git.commit(commitName))
              .on('data',function(data) {
                console.log(data);
              });
      })

      gulp.task('git:push', function (callback) {
        git.push('origin', 'master', {args: " -f"}, function (err) {
          if (err) throw err;
          callback()
        });
      })
      

      runSequence("git:add","git:commit","git:push",callback)

  };
};